 
 /* Restaurant DB*/
INSERT INTO [dbo].[Restaurant_Details]
	--([Restaurant_ID],
	 ([Restaurant_Name],
	 [Restaurant_Description],
	 [Address_Street],
	 [Address_Street2],
	 [Address_City],
	 [Address_Region],
	 [Address_Country],
	 [Address_Code],
	 [Contact_Telephone],
	 [Contact_Fax],
	 [Contact_Email],
	 [Contact_Website],
	 [Star_Rating],
	 [Status_Partnered],
	 [Creation_Date])
VALUES 
	( 'Estuary Cafe', 'Best Indian', 'MG Road','','Panaji','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',4,0, GETDATE()),
	( 'Cafe Lilliput', 'Best Indian', 'MG Road','','Margao','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0, GETDATE()),
	( 'Viva Panjim', 'Best Indian', 'MG Road','','Anjuna','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',3,0, GETDATE()),
	( 'Mosaic Restaurant', 'Best Indian', 'MG Road','','Ponda','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0, GETDATE()),
	( 'AZ.U.R - The Marriott', 'Best Indian', 'MG Road','','Miramar','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0, GETDATE())
	
	 
INSERT INTO [dbo].[Restaurant_Types]
     --([Restaurant_Type_ID],
     ([Restaurant_Type],
      [Valid_Type],
      [Creation_Date])
VALUES
	  ('VEG',0,GETDATE()),
	  ('VEG',0,GETDATE()),
	  ('NON-VEG',0,GETDATE()),
	  ('VEG',0,GETDATE()),
	  ('NON-VEG',0,GETDATE())
	 
	 
INSERT INTO [dbo].[Restaurants]
	  --([Restaurants_ID],
     ([Restaurant_ID],
      [Restaurant_Type_ID],
      [Restaurant_Valid],
      [Creation_Date])
VALUES
	  (1,1,0,GETDATE()),
	  (2,2,0,GETDATE()),
	  (3,3,0,GETDATE()),
	  (4,4,0,GETDATE()),
	  (5,5,0,GETDATE())
	  
	  
INSERT INTO [dbo].[Restaurant_Menu_Types]
      --([Restaurant_Menu_Type_ID],
      ([Restaurant_Menu_Type_Description],
      [Restaurant_Menu_Type_Valid],
      [Creation_Date])
 VALUES
	  ('Starter',0,GETDATE()),
	  ('Main Course',0,GETDATE()),
	  ('Dessert',0,GETDATE()),
	  ('Main Course',0,GETDATE()),
	  ('Starter',0,GETDATE())
	  
	  
INSERT INTO [dbo].[Restaurant_Menu_Items]
      --([Restaurant_Menu_Item_ID],
      ([Restaurant_Menu_Item_Name],
      [Restaurant_Menu_Item_Description],
      [Restaurant_Menu_Item_Image],
      [Restaurant_Menu_Item_Valid],
      [Creation_Date])
VALUES
	  ('Starter1','Starters item Description','',0,GETDATE()),
	  ('Main Course1','Main Course item Description','',0,GETDATE()),
	  ('Dessert1','Dessert item Description','',0,GETDATE()),
	  ('Main Course2','Main Course item Description','',0,GETDATE()),
	  ('Starter2','Starters item Description','',0,GETDATE())
	  
	  
INSERT INTO [dbo].[Restaurant_Menu_Items_Customisation]
      --([Restaurant_Menu_Items_Customisation_ID],
      ([Restaurant_Menu_Items_Customisation_Name],
      [Restaurant_Menu_Items_Customisation_Valid],
      [Creation_Date])
VALUES
	  ('Starter1',0,GETDATE()),
	  ('Main Course1',0,GETDATE()),
	  ('Dessert1',0,GETDATE()),
	  ('Main Course2',0,GETDATE()),
	  ('Starter2',0,GETDATE())
	  
	  
INSERT INTO [dbo].[Restaurant_Menu_Items_Customisations]
	  --([Restaurant_Menu_Items_Customisations_ID],
      ([Restaurant_Menu_Item_ID],
      [Restaurant_Menu_Items_Customisation_ID],
      [Restaurant_Menu_Items_Customisations_Icon],
      [Creation_Date])
VALUES
	  (1,1,'',GETDATE()),
	  (2,2,'',GETDATE()),
	  (3,3,'',GETDATE()),
	  (4,4,'',GETDATE()),
	  (5,5,'',GETDATE())


INSERT INTO [dbo].[Restaurant_Menus]
      --([Restaurant_Menus_ID],
      ([Restaurants_ID],
      [Restaurant_Menu_Item_ID],
      [Restaurant_Menu_Type_ID],
      [Restaurant_Menus_Valid],
      [Creation_Date])
VALUES
	  (1,1,1,0,GETDATE()),
	  (2,2,2,0,GETDATE()),
	  (3,3,3,0,GETDATE()),
	  (4,4,4,0,GETDATE()),
	  (5,5,5,0,GETDATE())
	  
  
  /* Hotel DB*/
  
  
INSERT INTO [dbo].[Hotel_Details]
	  --([Hotel_ID],
      ([Hotel_Name],
      [Hotel_Description],
      [Address_Street],
      [Address_Street2],
      [Address_City],
      [Address_Region],
      [Address_Country],
      [Address_Code],
      [Contact_Telephone],
      [Contact_Fax],
      [Contact_Email],
      [Contact_Website],
      [Star_Rating],
      [Status_Partnered],
      [Creation_Date])
VALUES 
	( 'Le Meridien Kochi', 'very good hotel for relaxing', 'Maradu','','Panaji','Kerala','India','682304','0484 270 5777','234324','ka@asdf.com','http://www.lemeridienkochi.com/',4,0, GETDATE()),
	( 'THE CROWN GOA', 'Best Indian', 'MG Road','','Panaji','Goa','India','403001','0832 24 00 000','234324','rajan@associatehotelsindia.com','http://www.thecrowngoa.com/',5,0, GETDATE()),
	( 'The Oberoi Grand Kolkata', 'Best Indian', 'Sham Nath Marg,','','Anjuna','Delhi','India','110 054','91-11-2389 0606','91-11-2389 0500','reservations@oberoigroup.com','https://www.oberoihotels.com',3,0, GETDATE()),
	( 'Taj Falaknuma Palace', 'Best Indian', 'Barrister Rajni Patel Marg Nariman Point','','Mumbai','Maharashtra ','India','400 021','9102261371637','234324','ka@asdf.com','http://www.tajhotels.com',5,0, GETDATE()),
	( ' The Taj Lake Palace', 'Best Indian', 'Barrister Rajni Patel Marg Nariman Point','','Mumbai','Maharashtra ','India','400 021','9102261371637','234324','ka@asdf.com','http://www.tajhotels.com',5,0, GETDATE())
	

INSERT INTO [dbo].[Hotel_Types]
	  --([Hotel_Type_ID],
      ([Hotel_Type],
      [Valid_Type],
      [Creation_Date])
VALUES
	  ('Hotel',0,GETDATE()),
	  ('Hostel',0,GETDATE()),
	  ('Apartments',0,GETDATE()),
	  ('Hotel',0,GETDATE()),
	  ('Hostel',0,GETDATE())
	  

INSERT INTO [dbo].[Hotels]
	  --([Hotels_ID],
	  ([Hotel_ID],
      [Hotel_Type_ID],
      [Hotel_Valid],
      [Creation_Date])
VALUES
	  (1,1,0,GETDATE()),
	  (2,2,0,GETDATE()),
	  (3,3,0,GETDATE()),
	  (4,4,0,GETDATE()),
	  (5,5,0,GETDATE())
	
	
INSERT INTO [dbo].[Hotel_Rooms]
	  --([Room_ID],
      ([Room_Description],
      [Hotels_ID],
      [Creation_Date])
VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())
	  
	  
INSERT INTO [dbo].[Hotel_Customisation_Types]
      --([Hotel_Customisation_Type_ID],
      ([Hotel_Customisation_Type_Description],
      [Hotel_Customisation_Type_Valid],
      [Creation_Date])
VALUES
	  ('No Pillows',0,GETDATE()),
	  ('Double Pillows',0,GETDATE()),
	  ('Air Con On',0,GETDATE()),
	  ('Air Con Off',0,GETDATE()),
	  ('Double Pillows',0,GETDATE()) 
	  

INSERT INTO [dbo].[Hotel_Customisations]
      --([Hotel_Customisation_ID],
      ([Hotel_Customisation_Type_ID],
      [Room_ID],
      [Hotel_Customisation_Valid],
      [Creation_Date])
VALUES
	  (1,1,0,GETDATE()),
	  (2,2,0,GETDATE()),
	  (3,3,0,GETDATE()),
	  (4,4,0,GETDATE()),
	  (5,5,0,GETDATE())
	
	
	
	/*Customer Details*/  
	
INSERT INTO [dbo].[Customer_Details]
      --([Customer_ID],
      ([Customer_ForeName],
      [Customer_SurName],
      [Customer_DOB],
      [Address_Street],
      [Address_Street2],
      [Address_City],
      [Address_Region],
      [Address_Country],
      [Address_Code],
      [Contact_Telephone],
      [Contact_Email],
      [Password],
      [Status_Active],
      [ResetPasswordCode],
      [ActivationCode],  
	  [LastUpdated],  
      [Creation_Date])
VALUES
	   ('rest','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,7895682547845,'rest@gmail.com','pass1234',0,'','',GETDATE(),GETDATE()),
	   ('Andrew','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,7895682547845,'andrew@gmail.com','pass1234',0,'','',GETDATE(),GETDATE()),
	   ('Maria','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,7895682547845,'maria@gmail.com','pass1234',0,'','',GETDATE(),GETDATE()),
	   ('Steven','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,7895682547845,'steven@gmail.com','pass1234',0,'','',GETDATE(),GETDATE()),
	   ('Mike','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,7895682547845,'mike@gmail.com','pass1234',0,'','',GETDATE(),GETDATE())
	   
	    
 	
INSERT INTO [dbo].[Customer_Restaurant_Orders]
	  --([Customer_Restaurant_Order_ID],
      ([Customer_ID],
      [Restaurant_ID],
      [Customer_Table_Number],
      [Creation_Date])
VALUES
	  (1,1,7,GETDATE()),
	  (2,2,34,GETDATE()),
	  (3,3,17,GETDATE()),
	  (4,4,21,GETDATE()),
	  (5,5,9,GETDATE())
	  
	  
INSERT INTO [dbo].[Customer_Restaurant_Order_Items]
       --([Customer_restaurant_Order_Item_ID],
       ([Customer_Restaurant_Order_ID],
       [Restaurant_Menu_Item_ID],
       [Creation_Date])
VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())
	  
	  
INSERT INTO [dbo].[Customer_Restaurant_Order_Item_customisation]
      --([Customer_Restaurant_Order_Item_customisation_ID],
      ([Customer_restaurant_Order_Item_ID],
      [Restaurant_Menu_Items_Customisations_ID],
      [Creation_Date])
VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())
	  
	  
	  
INSERT INTO [dbo].[Customer_Hotel_Bookings]
      --([Customer_Hotel_Booking_ID],
      ([Customer_ID],
      [Hotel_ID],
      [Room_ID],
      [Creation_Date])
VALUES
	  (1,1,1,GETDATE()),
	  (2,2,2,GETDATE()),
	  (3,3,3,GETDATE()),
	  (4,4,4,GETDATE()),
	  (5,5,5,GETDATE())
	  
	  
INSERT INTO [dbo].[Customer_Hotel_Booking_Customisations]
      --([Customer_Hotel_Booking_Customisations_ID],
      ([Customer_Hotel_Booking_ID],
      [Hotel_Customisation_ID],
      [Creation_Date])
VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())
	  
	  
	  
INSERT INTO [dbo].[Customer_Hotel_Preferences]
      --([Customer_Hotel_Preference_ID],
      ([Customer_ID],
      [Hotel_Customisation_Type_ID],
      [Is_Valid],
      [Creation_Date])
VALUES
	  (1,1,1,GETDATE()),
	  (2,2,1,GETDATE()),
	  (3,3,1,GETDATE()),
	  (4,4,1,GETDATE()),
	  (5,5,1,GETDATE())
	  
	  
	  
INSERT INTO [dbo].[Customer_Restaurant_Preferences]
	  --([Customer_Restaurant_Preference_ID],
      ([Customer_ID],
      [Restaurant_Menu_Items_Customisation_ID],
      [Is_Valid],
      [Creation_Date])
VALUES
	  (1,1,1,GETDATE()),
	  (2,2,1,GETDATE()),
	  (3,3,1,GETDATE()),
	  (4,4,1,GETDATE()),
	  (5,5,1,GETDATE())
	
			  
INSERT INTO [dbo].[Role]
      --([Role_Id],
      ([Name])
VALUES
	  ('Admin'),
	  ('RestaurantOwner'),
	  ('RestaurantUser'),
	  ('Customer') 
	
	
INSERT INTO [dbo].[Customer_Role]
      ([Customer_Id],
      [Role_Id])
VALUES
	  (1,1 ),
	  (2,2 ),
	  (3,3 ),
	  (4,4 ),
	  (5,1 )  
		
	
INSERT INTO [dbo].[Hotel_Customer_Reviews]
      --([Review_ID],
      ([Customer_Hotel_Booking_ID],
      [Review_Score],
      [Creation_Date])
VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())  
	  
	  
INSERT INTO [dbo].[Restaurant_Customer_Reviews]
	  --([Review_ID],
      ([Customer_Restaurant_Order_ID],
      [Review_Score],
      [Creation_Date])
  
  VALUES
	  (1,1,GETDATE()),
	  (2,2,GETDATE()),
	  (3,3,GETDATE()),
	  (4,4,GETDATE()),
	  (5,5,GETDATE())