﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using System.Threading;
using System.Web.Http.Description;
using System.Security.Claims;

namespace ToOrdr.Api.Controllers
{
    /// <summary>
    /// Parent for all the controller classes
    /// </summary>
    public class BaseController : ApiController
    {
        private readonly ICustomerService _customerService = DependencyResolver.Current.GetService<ICustomerService>();

        [ApiExplorerSettings(IgnoreApi = true)]
        public Customer GetAuthenticatedCustomer()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.Identity is ClaimsIdentity)
                {
                    var claims = User.Identity as ClaimsIdentity;

                    var claim = claims.Claims.FirstOrDefault(c => c.Type == "Provider");
                    if (claim != null)
                    {
                        var provideName = claim.Value;
                        return _customerService.GetAllCustomers(c => String.Equals(c.ProviderKey, User.Identity.Name, StringComparison.OrdinalIgnoreCase) && c.ActiveStatus == true).FirstOrDefault();
                    }

                    return _customerService.GetAllCustomers(c => String.Equals(c.Email, User.Identity.Name, StringComparison.OrdinalIgnoreCase) && c.ActiveStatus == true).FirstOrDefault();
                }
            }
            return null;


        }
    }
}
