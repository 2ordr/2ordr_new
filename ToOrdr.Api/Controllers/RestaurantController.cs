﻿using AutoMapper;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI;
using ToOrdr.Api.App_Start;
using ToOrdr.Api.DTO;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Exceptions;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;

namespace ToOrdr.Api.Controllers
{

#if !DEBUG
    [Authorize]
#endif

    public class RestaurantController : BaseController
    {
        IRestaurantService _restaurantService;
        IPaymentService _paymentService;
        ICustomerService _customerService;

        public RestaurantController(IRestaurantService restaurantService, IPaymentService paymentService, ICustomerService customerService)
        {
            _restaurantService = restaurantService;
            _paymentService = paymentService;
            _customerService = customerService;
        }


        #region restaurant

        /// <summary>
        /// Search restaurants based on different criterias
        /// </summary>
        /// <remarks>
        /// Search resturants based on different criterias, if found than returns 200 status with "success" meassage and if not found then return 404 status with "no content" message
        /// </remarks>
        /// <param name="lat">Enter latitude of place</param>
        /// <param name="lng">Enter longitude of place</param>
        /// <param name="radius_min">Enter radius in meters</param>
        /// <param name="radius_max">Enter radius in meters</param>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="Ids">Enter cuisine ids</param>
        /// <param name="rating_min">Minimum user rating</param>
        /// <param name="rating_max">Maximum user rating</param>
        /// <param name="price_min">Minimum Night price</param>
        /// <param name="price_max">Maximum Night price</param>
        /// <param name="page">Enter page number</param>
        /// <param name="limit">Enter the page size</param>
        /// <param name="sort">Select sort by condition</param>
        /// <param name="order">Enter how to order "asc" or "desc"</param>
        /// <param name="openingHour">Enter opening time in hours(24hr format)</param>
        /// <param name="closingHour">Enter closing time in hours(24hr format)</param>
        /// <param name="starRating">Number of star rating</param>
        /// <param name="priceRange">Price range</param>
        /// <param name="minUserRating">Min user rating average</param>
        /// <param name="maxUserRating">Max user rating average</param>
        /// <param name="considerMyPreferences">My preference</param>
        /// <param name="selection">Selections</param>
        /// <param name="groupBy">Select group by</param>
        /// <returns>Returns list of restaurants </returns>
        [HttpGet]
        [Route("restaurants/search", Name = "SearchRestaurants")]

        [ResponseType(typeof(List<RestaurantDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Restaurants not found")]
        [SwaggerResponse(HttpStatusCode.NoContent, Description = "no content")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]

        public IHttpActionResult SearchRestaurants(double? lat = null, double? lng = null, double? radius_min = 0, double? radius_max = 20000, string searchText = "", [FromUri]  int[] Ids = null, double? rating_min = null, double? rating_max = null, double? price_min = null, double? price_max = null, int page = 1, int limit = 20, Selection selection = Selection.All, RestaurantSort sort = RestaurantSort.Popularity, SortOrder order = SortOrder.Asc, bool considerMyPreferences = false, float? openingHour = null, float? closingHour = null, int? starRating = null, int? priceRange = null, int? minUserRating = null, int? maxUserRating = null, GroupBy groupBy = GroupBy.None)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            int Id = user != null ? user.Id : 0;
            TimeSpan? openingTime = null;
            TimeSpan? closingTime = null;
            int total;

            if (openingHour.HasValue) openingTime = TimeSpan.FromHours(openingHour.Value);
            if (closingHour.HasValue) closingTime = TimeSpan.FromHours(closingHour.Value);

            var restaurants = _restaurantService.Search(out total, searchText, lat, lng, radius_min, radius_max, Ids, rating_min, rating_max, price_min, price_max, page, limit, selection, sort, order, considerMyPreferences, SearchTextType.Everywhere, Id, openingTime, closingTime, starRating, priceRange, minUserRating, maxUserRating, groupBy);

            if (restaurants.Any())
            {
                var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
                var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantDTO>>(restaurants);
                restaurantsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
                if (user != null)
                {
                    if (favRestaurants != null && favRestaurants.Count() > 0)
                    {
                        foreach (var res in restaurantsDTO)
                        {
                            if (favRestaurants.Any(r => r.Id == res.Id))
                                res.IsFavourite = true;
                        }
                    }
                }

                switch (groupBy)
                {
                    case GroupBy.None:
                        break;
                    case GroupBy.UserRating:
                        int[] Averages = { 2, 4, 6, 8, 10 };
                        var restUserRatingAvgGroups = restaurantsDTO.Where(r => r.Rating_Average > 0).GroupBy(avg => Averages.FirstOrDefault(r => r >= avg.Rating_Average)).OrderByDescending(o => o.Key).Select(gr => new RestaurantGroupByUserRatingDTO { UserRatingAvg = gr.Key, Restaurant = gr.Take(5).ToList() });
                        var restUserRatingAvgGroupsDTO = Mapper.Map<List<RestaurantGroupByUserRatingDTO>>(restUserRatingAvgGroups);
                        foreach (var defaultUserRatingAvgGrp in restUserRatingAvgGroupsDTO)
                        {
                            if (defaultUserRatingAvgGrp.UserRatingAvg == 0)
                            {
                                restUserRatingAvgGroupsDTO.Remove(defaultUserRatingAvgGrp);
                                break;
                            }
                        }
                        return Content(HttpStatusCode.OK, new { Message = "success", Result = restUserRatingAvgGroupsDTO });
                    case GroupBy.PriceRange:
                        var restPriceRangeGroups = restaurantsDTO.GroupBy(r => r.PriceRange).OrderByDescending(o => o.Key).Select(gr => new RestaurantGroupByPriceRangeDTO { PriceRange = gr.Key, Restaurant = gr.Take(5).ToList() });
                        return Content(HttpStatusCode.OK, new { Message = "success", Result = restPriceRangeGroups });
                    case GroupBy.Radius:
                        if (lat.HasValue && lng.HasValue)
                        {
                            int[] ranges = { 5000, 10000, 20000, 30000, 100000 };
                            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", lng.Value, lat.Value);
                            var sourcePoint = DbGeography.PointFromText(text, 4326);
                            var restRadiusGroups = restaurants.GroupBy(loc => ranges.FirstOrDefault(r => r > loc.Location.Distance(sourcePoint))).OrderBy(o => o.Key).Select(gr => new RestaurantGroupByRadius { Distance = gr.Key, Restaurant = gr.Take(5).ToList() });
                            var restRadiusGroupsDTO = Mapper.Map<IEnumerable<RestaurantGroupByRadius>, List<RestaurantGroupByRadiusDTO>>(restRadiusGroups);
                            foreach (var defaultRadiusGrp in restRadiusGroupsDTO)
                            {
                                if (defaultRadiusGrp.Distance == 0)
                                {
                                    restRadiusGroupsDTO.Remove(defaultRadiusGrp);
                                    break;
                                }
                            }
                            return Content(HttpStatusCode.OK, new { Message = "success", Result = restRadiusGroupsDTO });
                        }
                        break;
                }
                var mod = total % limit;
                var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { Message = "success", total_items = total, total_pages = totalPageCount, current_page = page, result = restaurantsDTO });

            }

            // search didn't return content, so send suggestions

            var suggestions = _restaurantService.Search(out total, null, null, null, radius_min, radius_max, null, null, null, null, null, page, limit, Selection.All, RestaurantSort.Popularity, SortOrder.Desc);
            var suggestionsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantDTO>>(suggestions);
            suggestionsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();

            if (user != null)
            {
                var sugRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
                if (sugRestaurants != null || sugRestaurants.Count() > 0)
                {
                    foreach (var sugRes in suggestionsDTO)
                    {
                        if (sugRestaurants.Any(r => r.Id == sugRes.Id))
                            sugRes.IsFavourite = true;
                    }
                }
            }
            var suggMod = total % limit;
            var suggTotalPageCount = (total / limit) + (suggMod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { Message = "no content", total_items = total, total_pages = suggTotalPageCount, current_page = page, result = (string)null, Suggestions = suggestionsDTO });

        }

        /// <summary>
        /// Search restaurants based on restauran name
        /// </summary>
        /// <remarks>
        /// Search restaurants based restaurant name
        /// </remarks>
        /// <param name="Name">Enter restaurant name</param>
        /// <param name="page">Enter the number</param>
        /// <param name="limit">Enter page size</param>
        /// <returns>Return list of restaurants</returns>
        [HttpGet]
        [Route("restaurants/searchbyname", Name = "GetRestaurantByName")]
        [ResponseType(typeof(RestaurantDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]

        public IHttpActionResult GetRestaurantByName(string Name, int? page = 1, int? limit = 20)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            int total;
            var restaurants = _restaurantService.Search(out total, Name, null, null, null, null, null, null, null, null, null, Convert.ToInt32(page), Convert.ToInt32(limit), Selection.All, RestaurantSort.Popularity, SortOrder.Desc, false, SearchTextType.RestaurantName);
            if (restaurants.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantDTO>>(restaurants);
            restaurantsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
            if (user != null)
            {
                var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
                if (favRestaurants != null && favRestaurants.Count() > 0)
                {
                    foreach (var res in restaurantsDTO)
                    {
                        if (favRestaurants.Any(r => r.Id == res.Id))
                            res.IsFavourite = true;
                    }
                }
            }
            var mod = total % limit;
            var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = restaurantsDTO });
        }

        /// <summary>
        /// Group restaurants based on star rating
        /// </summary>
        /// <remarks>
        /// Group restaurants based on star rating
        /// </remarks>
        /// <returns>Return list of restaurants groups</returns>
        [HttpGet]
        [Route("restaurants/groupbystarrating", Name = "GetRestaurantGroupByStarRating")]
        [ResponseType(typeof(RestaurantGroupByUserRatingDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantGroupByStarRating()
        {
            var user = GetAuthenticatedCustomer();
            var restaurantsGroups = _restaurantService.GetRestaurantGroupsByStarRating();
            if (restaurantsGroups.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantsGroupsDTO = Mapper.Map<List<RestaurantGroupByUserRatingDTO>>(restaurantsGroups);
            var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);

            foreach (var eachStarRatingGrp in restaurantsGroupsDTO)
            {
                eachStarRatingGrp.Restaurant.Select(r => { r.IsFavourite = false; return r; }).ToList();
                foreach (var restaurant in eachStarRatingGrp.Restaurant)
                {
                    if (!string.IsNullOrWhiteSpace(restaurant.BackgroundImage))
                        restaurant.BackgroundImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Background/" + restaurant.BackgroundImage;

                    if (!string.IsNullOrWhiteSpace(restaurant.LogoImage))
                        restaurant.LogoImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Logo/" + restaurant.LogoImage;

                    if (favRestaurants != null && favRestaurants.Count() > 0)
                    {
                        if (favRestaurants.Any(r => r.Id == restaurant.Id))
                            restaurant.IsFavourite = true;
                    }
                }
            }
            return Content(HttpStatusCode.OK, restaurantsGroupsDTO);
        }

        /// <summary>
        /// Get all restaurants based on star rating
        /// </summary>
        /// <remarks>
        /// Get all restaurants based on star rating
        /// </remarks>
        /// <param name="rating">Star rating</param>
        /// <param name="page">Page number</param>
        /// <param name="limit">Page size</param>
        /// <returns>Return list of restaurants </returns>
        [HttpGet]
        [Route("restaurants/starrating/{rating}")]
        [ResponseType(typeof(RestaurantGroupDetailDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantsByStarRating(int rating, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;
            var user = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetRestaurantByStarRating(rating, page.Value, limit.Value, out total);
            if (restaurants.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantGroupDetailDTO>>(restaurants);
            var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
            restaurantsDTO.Select(r => { r.IsFavourite = "False"; return r; }).ToList();
            foreach (var restaurant in restaurantsDTO)
            {
                if (!string.IsNullOrWhiteSpace(restaurant.BackgroundImage))
                    restaurant.BackgroundImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Background/" + restaurant.BackgroundImage;

                if (!string.IsNullOrWhiteSpace(restaurant.LogoImage))
                    restaurant.LogoImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Logo/" + restaurant.LogoImage;

                if (favRestaurants != null && favRestaurants.Count() > 0)
                {
                    if (favRestaurants.Any(r => r.Id == restaurant.Id))
                        restaurant.IsFavourite = "True";
                }
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_pages = totalPageCount, current_page = page.Value, result = restaurantsDTO });
        }

        /// <summary>
        /// Group restaurants based on Price range
        /// </summary>
        /// <remarks>
        /// Group restaurants based on price range
        /// </remarks>
        /// <returns>Return list of restaurants groups</returns>
        [HttpGet]
        [Route("restaurants/groupbypricerange", Name = "GetRestaurantGroupsByPriceRange")]
        [ResponseType(typeof(RestaurantGroupByPriceRangeDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantGroupsByPriceRange()
        {
            var user = GetAuthenticatedCustomer();
            var restPriceRangeGroups = _restaurantService.GetRestaurantGroupsByPriceRange();
            if (restPriceRangeGroups.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restPriceRangeGroupsDTO = Mapper.Map<IEnumerable<RestaurantGroupByPriceRange>, List<RestaurantGroupByPriceRangeDTO>>(restPriceRangeGroups);
            var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);

            foreach (var eachPriceRangeGrp in restPriceRangeGroupsDTO)
            {
                eachPriceRangeGrp.Restaurant.Select(r => { r.IsFavourite = false; return r; }).ToList();
                foreach (var restaurant in eachPriceRangeGrp.Restaurant)
                {
                    if (!string.IsNullOrWhiteSpace(restaurant.BackgroundImage))
                        restaurant.BackgroundImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Background/" + restaurant.BackgroundImage;

                    if (!string.IsNullOrWhiteSpace(restaurant.LogoImage))
                        restaurant.LogoImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Logo/" + restaurant.LogoImage;

                    if (favRestaurants != null && favRestaurants.Count() > 0)
                    {
                        if (favRestaurants.Any(r => r.Id == restaurant.Id))
                            restaurant.IsFavourite = true;
                    }
                }
            }
            return Content(HttpStatusCode.OK, restPriceRangeGroupsDTO);
        }

        /// <summary>
        /// Get all restaurants based on price range
        /// </summary>
        /// <remarks>
        /// Get all restaurants based on price range
        /// </remarks>
        /// <param name="price">price range</param>
        /// <param name="page">Page number</param>
        /// <param name="limit">Page size</param>
        /// <returns>Return list of restaurants </returns>
        [HttpGet]
        [Route("restaurants/pricerange/{price}")]
        [ResponseType(typeof(RestaurantGroupDetailDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantsByPriceRange(int price, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;
            var user = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetRestaurantsByPriceRange(price, page.Value, limit.Value, out total);
            if (restaurants.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantGroupDetailDTO>>(restaurants);
            var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
            restaurantsDTO.Select(r => { r.IsFavourite = "False"; return r; }).ToList();
            foreach (var restaurant in restaurantsDTO)
            {
                if (!string.IsNullOrWhiteSpace(restaurant.BackgroundImage))
                    restaurant.BackgroundImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Background/" + restaurant.BackgroundImage;

                if (!string.IsNullOrWhiteSpace(restaurant.LogoImage))
                    restaurant.LogoImage = Url.Content("/Images/Restaurant/") + restaurant.Id.ToString() + "/Logo/" + restaurant.LogoImage;

                if (favRestaurants != null && favRestaurants.Count() > 0)
                {
                    if (favRestaurants.Any(r => r.Id == restaurant.Id))
                        restaurant.IsFavourite = "True";
                }
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_pages = totalPageCount, current_page = page.Value, result = restaurantsDTO });
        }

        /// <summary>
        /// Returns restaurant by Id
        /// </summary>
        /// <remarks>
        /// Get detailed restaurant information using youORDR restaurant ID. Authorization Required.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <returns>Restaurant Details</returns>
        [HttpGet]
        [Route("restaurants/{id}", Name = "GetRestaurantById")]

        [ResponseType(typeof(RestaurantDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]

        public IHttpActionResult GetRestaurantById(int id)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var restaurant = _restaurantService.GetRestaurantById(id);
            if (restaurant == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantDTO = Mapper.Map<RestaurantDTO>(restaurant);
            restaurantDTO.IsFavourite = false;

            if (user != null)
            {
                var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
                if (favRestaurants != null && favRestaurants.Count() > 0)
                {
                    if (favRestaurants.Any(r => r.Id == restaurantDTO.Id))
                        restaurantDTO.IsFavourite = true;
                }
            }

            return Content(HttpStatusCode.OK, restaurantDTO);
        }

        /// <summary>
        /// This method returns a fully qualified absolute server Url which includes
        /// the protocol, server, port in addition to the server relative Url.
        /// 
        /// Works like Control.ResolveUrl including support for ~ syntax
        /// but returns an absolute URL.
        /// </summary>
        /// <param name="serverUrl">Any Url, either App relative or fully qualified</param>
        /// <param name="forceHttps">if true forces the url to use https</param>
        /// <returns></returns>
        public static string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            if (serverUrl.IndexOf("://") > -1)
                return serverUrl;

            string newUrl = serverUrl;

            Uri originalUri = System.Web.HttpContext.Current.Request.Url;
            newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                "://" + originalUri.Authority + newUrl;

            return newUrl;
        }



        /// <summary>
        /// Creates new Restaurant
        /// </summary>
        /// <remarks>
        /// Adds new resturant in the backend
        /// </remarks>
        /// <param name="restaurantDTO">Restaurant Details</param>
        /// <returns>Returns newly created restaurant details</returns>
        [HttpPost]
        [Route("Restaurants")]

        [ResponseType(typeof(RestaurantDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]

        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult CreateRestaurant(CreateRestaurantDTO restaurantDTO)
        {
            if (restaurantDTO != null)
                ModelState["restaurantDTO.Location.Geography"].Errors.Clear();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                    "POINT({0} {1})", restaurantDTO.Longitude, restaurantDTO.Latitude);
            restaurantDTO.Location = DbGeography.PointFromText(text, 4326);
            var entity = Mapper.Map<Restaurant>(restaurantDTO);

            entity = _restaurantService.CreateRestaurant(entity);

            return Created(Url.Link("GetRestaurantById", new { id = entity.Id }), Mapper.Map<RestaurantDTO>(entity));

        }

        /// <summary>
        /// Modifies existing restaurant details
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the resturant.
        /// </remarks>
        /// <param name="restaurantDTO">Restaurant details</param>
        /// <returns>returns modifed restaurant details</returns>
        [Route("restaurants")]
        [HttpPut]

        [ResponseType(typeof(RestaurantDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ModifyRestaurant(ModifyRestaurantDTO restaurantDTO)
        {
            if (restaurantDTO != null)
                ModelState["restaurantDTO.Location.Geography"].Errors.Clear();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                   "POINT({0} {1})", restaurantDTO.Longitude, restaurantDTO.Latitude);
            restaurantDTO.Location = DbGeography.PointFromText(text, 4326);

            var entity = Mapper.Map<Restaurant>(restaurantDTO);

            entity = _restaurantService.ModifyRestaurant(entity);

            return Ok(Mapper.Map<RestaurantDTO>(entity));

        }

        /// <summary>
        /// Delete Restaurant by id
        /// </summary>
        /// <remarks>
        /// Delete restaurant details based on restaurant id.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <returns>Success</returns>
        [Route("restaurants/{id}")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Restaurants" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteRestaurant(int id)
        {

            _restaurantService.DeleteRestaurant(id);

            return Ok();

        }

        #endregion

        #region Menu Item Offers
        /// <summary>
        /// Returns all virtual/non virtual restaurants menu items containing offers
        /// </summary>
        /// <remarks>
        /// Get a detailed list of virtual/non virtual restaurants menu items containing offers.
        /// </remarks>
        /// <param name="hotelId">Pass hotelId to get menu item offers of virtual restaurants</param>
        /// <returns>List of virtual/non virtual restaurants menu items containing offers.</returns>
        [HttpGet]
        [Route("restaurants/menuItem_offers", Name = "GetMenuItemsOffers")]
        [AllowAnonymous]
        [ResponseType(typeof(List<MenuItemOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Offers Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Restaurants Menu Items Offers" })]

        public IHttpActionResult GetMenuItemsOffers(int? hotelId = null)
        {
            var offersMenuItems = _restaurantService.GetAllRestOffersMenuItems(hotelId);
            if (offersMenuItems == null || offersMenuItems.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.MenuItemOffers_NotFound } });

            var menuItemOffersDTO = Mapper.Map<IEnumerable<MenuItemOffer>, List<MenuItemOffersDTO>>(offersMenuItems);

            foreach (var menuItemOffer in menuItemOffersDTO)
            {
                var imageUrl = Url.Content("/Images/");
                BuildRestaurantMenuItemsImagesInfo(menuItemOffer.RestaurantMenuItem, imageUrl);
            }
            return Content(HttpStatusCode.OK, menuItemOffersDTO);
        }


        private static RestaurantMenuItemInfoDTO BuildRestaurantMenuItemsImagesInfo(RestaurantMenuItemInfoDTO restaurantMenuItems, string imageUrl)
        {
            if (restaurantMenuItems.SoldOut == true)
            {
                restaurantMenuItems.RestaurantMenuItemImages = Enumerable.Empty<RestaurantMenuItemImagesDTO>().ToList<RestaurantMenuItemImagesDTO>();
                var img = new RestaurantMenuItemImagesDTO()
                {
                    RestaurantMenuItemId = restaurantMenuItems.Id,
                    Image = imageUrl + "sold-out.png",
                    IsPrimary = true,
                    IsActive = true
                };
                restaurantMenuItems.RestaurantMenuItemImages.Add(img);
            }
            else
            {
                foreach (var itemImage in restaurantMenuItems.RestaurantMenuItemImages)
                {
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                        itemImage.Image = imageUrl + "MenuItem/" + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                }
            }
            return restaurantMenuItems;
        }

        #endregion Menu Item Offers

        #region restaurant menu

        /// <summary>
        /// Returns restaurant's daily menus
        /// </summary>
        /// <remarks>
        /// Get a detailed list of restaurant's daily menus using restaurant Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <param name="languageId">Language Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of Restaurant daily menus</returns>

        [HttpGet]
        [Route("restaurants/{id}/Menu")]

        [ResponseType(typeof(List<RestaurantMenusDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Menu found this restaurant")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetDailyMenu(int id, int? languageId = null, int? page = 1, int? limit = null)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
                limit = limit ?? ToOrderConfigs.DefaultPageSize;
                int total;

                var dailyMenus = _restaurantService.GetDailyMenuLanguageWise(id, languageId, page.Value, limit.Value, out total);
                if (dailyMenus == null || dailyMenus.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

                var dailyMenusDTO = Mapper.Map<IEnumerable<RestaurantMenu>, List<RestaurantMenusDTO>>(dailyMenus);
                var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = dailyMenus.Select(m => m.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

                foreach (var menu in dailyMenusDTO)
                {
                    menu.PreferredCurrencySymbol = user.Currency.Symbol;
                    foreach (var group in menu.RestaurantMenuGroups)
                    {
                        group.RestaurantMenuItems.Select(i => { i.IsFavourite = favMenuItems.Any(r => r.Id == i.Id) ? true : false; return i; }).ToList();
                        if (baseCurrencyRate != 0)
                            group.RestaurantMenuItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

                        foreach (var restMenuItes in group.RestaurantMenuItems)
                        {
                            var imageUrl = Url.Content("/Images/");
                            BuildRestaurantMenuItemsImages(restMenuItes, imageUrl);
                        }
                    }
                }
                var mod = total % limit.Value;
                var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = dailyMenusDTO });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { arEx.Message } });
            }
        }



        private static RestaurantMenuItemsDTO BuildRestaurantMenuItemsImages(RestaurantMenuItemsDTO restaurantMenuItems, string imageUrl)
        {
            if (restaurantMenuItems.SoldOut == true)
            {
                restaurantMenuItems.RestaurantMenuItemImages = Enumerable.Empty<RestaurantMenuItemImagesDTO>().ToList<RestaurantMenuItemImagesDTO>();
                var img = new RestaurantMenuItemImagesDTO()
                {
                    RestaurantMenuItemId = restaurantMenuItems.Id,
                    Image = imageUrl + "sold-out.png",
                    IsPrimary = true,
                    IsActive = true
                };
                restaurantMenuItems.RestaurantMenuItemImages.Add(img);
            }
            else
            {
                foreach (var itemImage in restaurantMenuItems.RestaurantMenuItemImages)
                {
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                        itemImage.Image = imageUrl + "MenuItem/" + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                }
            }
            return restaurantMenuItems;
        }


        /// <summary>
        /// Returns Details of restaurant's daily menu
        /// </summary>
        /// <remarks>
        /// Get a detailed of restaurant's daily menu using menu Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Menu Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Details of Restaurant daily menu</returns>

        [HttpGet]
        [Route("restaurants/Menu/{id}")]

        [ResponseType(typeof(RestaurantMenusDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Menu found this restaurant")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetRestaurantMenu(int id, int? languageId = null)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var menuDetails = _restaurantService.GetRestaurantMenuByIdLanguageWise(id, languageId);
                if (menuDetails == null)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Menufound } });

                var dailyMenusDTO = Mapper.Map<RestaurantMenusDTO>(menuDetails);
                var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = menuDetails.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                dailyMenusDTO.PreferredCurrencySymbol = user.Currency.Symbol;
                foreach (var group in dailyMenusDTO.RestaurantMenuGroups)
                {
                    group.RestaurantMenuItems.Select(i => { i.IsFavourite = favMenuItems.Any(r => r.Id == i.Id) ? true : false; return i; }).ToList();
                    if (baseCurrencyRate != 0)
                        group.RestaurantMenuItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

                    foreach (var restMenuItes in group.RestaurantMenuItems)
                    {
                        var imageUrl = Url.Content("/Images/");
                        BuildRestaurantMenuItemsImages(restMenuItes, imageUrl);
                    }
                }
                return Content(HttpStatusCode.OK, dailyMenusDTO);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Returns list of Menu Items using menu groupId
        /// </summary>
        /// <remarks>
        /// Get list of the Menu Items.
        /// </remarks>
        /// <param name="id">Menu group Id</param>
        /// <param name="languageId">Language Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Detailed list of Information of Menu Items</returns>

        [HttpGet]
        [Route("restaurants/menuGroup/{id}")]

        [ResponseType(typeof(List<RestaurantMenuItemDetailedDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Item Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItemByGroupId(int id, int? languageId = null, int? page = 1, int? limit = null)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
                limit = limit ?? ToOrderConfigs.DefaultPageSize;
                int total;

                var menuItems = _restaurantService.GetMenuItemByGroupId(id, ord => ord.Id, page.Value, limit.Value, out total, languageId);
                if (menuItems == null || menuItems.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.MenuItem_NotFound } });

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = menuItems.Select(i => i.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(ex => ex.ToCurrency == user.CurrencyId).Select(e => e.BaseRate).FirstOrDefault()).FirstOrDefault();

                foreach (var menuItem in menuItems)
                {
                    foreach (var menuAddElements in menuItem.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
                    {
                        foreach (var addElement in menuAddElements)
                        {
                            if (!string.IsNullOrWhiteSpace(addElement.Image))
                                if (addElement.Image.Contains("http"))
                                    continue;
                                else
                                    addElement.Image = Url.Content("/Images/MenuAdditionalGroups/") + addElement.MenuAdditionalGroupId.ToString() + "/MenuAdditionalElements/" + addElement.Id.ToString() + "/" + addElement.Image;
                        }
                    }
                }

                var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);
                var menuItemDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestaurantMenuItemDetailedDTO>>(menuItems);
                menuItemDTO.Select(m => { m.IsFavourite = favMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
                menuItemDTO.Select(m => { m.PreferredCurrencySymbol = user.Currency.Symbol; return m; }).ToList();

                foreach (var menuItem in menuItemDTO)
                {
                    var imageUrl = Url.Content("/Images/");
                    BuildRestaurantMenuItemsImages(menuItem, imageUrl);

                    if (baseCurrencyRate != 0)
                    {
                        menuItem.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(menuItem.Price) * baseCurrencyRate), 2);
                        foreach (var menuAddElements in menuItem.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
                        {
                            menuAddElements.Select(i => { i.PreferredCurrencyAdditionalCost = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.AdditionalCost) * baseCurrencyRate), 2); return i; }).ToList();
                        }
                    }
                }

                var mod = total % limit.Value;
                var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemDTO });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Returns Detailed information of Menu Item
        /// </summary>
        /// <remarks>
        /// Get detailed information of the Menu Item.
        /// </remarks>
        /// <param name="id">Menu Item Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Detailed Information of Menu Item</returns>

        [HttpGet]
        [Route("restaurants/menuItem/{id}")]

        [ResponseType(typeof(List<RestaurantMenuItemDetailedDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Item Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItem(int id, int? languageId = null)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var menuItem = _restaurantService.GetMenuItem(id, languageId);
            if (menuItem == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_RestMenuItem } });

            foreach (var menuAddElements in menuItem.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
            {
                foreach (var addElement in menuAddElements)
                {
                    if (!string.IsNullOrWhiteSpace(addElement.Image))
                        addElement.Image = Url.Content("/Images/MenuAdditionalGroups/") + addElement.MenuAdditionalGroupId.ToString() + "/MenuAdditionalElements/" + addElement.Id.ToString() + "/" + addElement.Image;
                }
            }

            double? baseCurrencyRate = 0;
            if (user.CurrencyId != null && user.CurrencyId > 0)
                baseCurrencyRate = menuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var favRestaurants = _customerService.GetCustomerFavoriteMenuItems(user.Id);
            var menuItemDTO = Mapper.Map<RestaurantMenuItemDetailedDTO>(menuItem);

            menuItemDTO.IsFavourite = favRestaurants.Any(r => r.Id == menuItemDTO.Id) ? true : false;
            menuItemDTO.PreferredCurrencySymbol = user.Currency.Symbol;

            if (baseCurrencyRate != 0)
            {
                menuItemDTO.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(menuItemDTO.Price) * baseCurrencyRate), 2);
                foreach (var menuAddElements in menuItemDTO.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
                {
                    menuAddElements.Select(i => { i.PreferredCurrencyAdditionalCost = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.AdditionalCost) * baseCurrencyRate), 2); return i; }).ToList();
                }
            }

            var imageUrl = Url.Content("/Images/");
            BuildRestaurantMenuItemsImages(menuItemDTO, imageUrl);

            return Content(HttpStatusCode.OK, menuItemDTO);
        }

        /// <summary>
        /// Returns Menu Item Suggestions
        /// </summary>
        /// <remarks>
        /// Get Menu Item Suggestions.
        /// </remarks>
        /// <param name="id">Menu Item Id</param>
        /// <param name="languageId">Language Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Suggestions of Menu Item</returns>

        [HttpGet]
        [Route("restaurants/menuItem/{id}/Suggestions")]

        [ResponseType(typeof(List<RestaurantMenuItemsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Item Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItemSuggestions(int id, int? languageId = null, int? page = 1, int? limit = null)
        {
            try
            {
                page = page <= 0 ? 1 : page;
                limit = limit ?? ToOrderConfigs.DefaultPageSize;
                int total;
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
                var menuItems = _restaurantService.GetMenuItemSuggestions(id, page.Value, limit.Value, out total, languageId);

                if (menuItems == null || menuItems.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.MenuItem_NotFound } });

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = menuItems.Select(mi => mi.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

                var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);
                var menuItemDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestaurantMenuItemsDTO>>(menuItems);
                menuItemDTO.Select(m => { m.IsFavourite = favMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();

                foreach (var menuItem in menuItemDTO)
                {
                    var imageUrl = Url.Content("/Images/");
                    BuildRestaurantMenuItemsImages(menuItem, imageUrl);
                }

                if (baseCurrencyRate != 0)
                    menuItemDTO.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

                var mod = total % limit.Value;
                var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemDTO });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Returns MenuItem Ingredient Nutrients
        /// </summary>
        /// <remarks>
        /// Get MenuItem Ingredient Nutrients.
        /// </remarks>
        /// <param name="id">Menu Item Id</param>
        /// <returns>Suggestions of Menu Item</returns>

        [HttpGet]
        [Route("restaurants/menuItem/{id}/Nutrients")]

        [ResponseType(typeof(List<MenuItemIngredientInfoDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "MenuItem Ingredient Nutrients Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItemIngredientNutrients(int id)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var menuItemsIngredient = _restaurantService.GetItemIngredient(id);
                if (menuItemsIngredient == null || menuItemsIngredient.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Ingredient_Nutrient_Not_found } });

                var menuItemsIngredientDTO = Mapper.Map<IEnumerable<MenuItemIngredient>, List<MenuItemIngredientInfoDTO>>(menuItemsIngredient);

                return Content(HttpStatusCode.OK, new { result = menuItemsIngredientDTO });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Returns restaurant's menus based on Table Number
        /// </summary>
        /// <remarks>
        /// Get a detailed list of restaurant menus using Table Number. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Table Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>List of Restaurant menus</returns>

        [HttpGet]
        [Route("restaurants/tableMenu/{id}")]

        [ResponseType(typeof(List<RestaurantMenusDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Menu Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetRestaurantMenuByTableNumber(int id, int? languageId = null)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var tableMenus = _restaurantService.GetRestaurantMenuByTableId(id, languageId);
                if (tableMenus == null || tableMenus.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Active_Menu } });


                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = tableMenus.Select(m => m.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

                var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);
                var tableMenusDTO = Mapper.Map<IEnumerable<RestaurantMenu>, List<RestaurantMenusDTO>>(tableMenus);
                tableMenusDTO.Select(t => { t.PreferredCurrencySymbol = user.Currency.Symbol; return t; }).ToList();

                foreach (var menu in tableMenusDTO)
                {
                    foreach (var group in menu.RestaurantMenuGroups)
                    {
                        group.RestaurantMenuItems.Select(m => { m.IsFavourite = favMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
                        if (baseCurrencyRate != 0)
                            group.RestaurantMenuItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

                        foreach (var menuItem in group.RestaurantMenuItems)
                        {
                            var imageUrl = Url.Content("/Images/");
                            BuildRestaurantMenuItemsImages(menuItem, imageUrl);
                        }
                    }
                }
                return Content(HttpStatusCode.OK, tableMenusDTO);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Search dishes based on different criterias
        /// </summary>
        /// <remarks>
        /// Search dishes based on different criterias, if found than returns 200 status with "success" meassage and if not found then return 404 status with "no content" message
        /// </remarks>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="restaurant_Id">Enter restaurant id</param>
        /// <param name="page">Enter page number</param>
        /// <param name="limit">Enter the page size</param>
        /// <returns>Returns list of restaurants dishes </returns>

        [HttpGet]
        [Route("restaurants/menuItem/search", Name = "SearchMenuItems")]

        [ResponseType(typeof(List<RestaurantMenuItemDetailedDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Items Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult SearchMenuItems(string searchText = "", int? restaurant_Id = null, int page = 1, int limit = 10)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            int total;
            var menuItems = _restaurantService.SearchMenuItems(out total, searchText, restaurant_Id, page, limit);
            if (menuItems.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { Message = new string[] { Resources.No_Found_Item } });

            foreach (var menuItem in menuItems)
            {
                foreach (var menuAddElements in menuItem.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
                {
                    foreach (var addElement in menuAddElements)
                    {
                        if (!string.IsNullOrWhiteSpace(addElement.Image))
                            if (addElement.Image.Contains("http"))
                                continue;
                            else
                                addElement.Image = Url.Content("/Images/MenuAdditionalGroups/") + addElement.MenuAdditionalGroupId.ToString() + "/MenuAdditionalElements/" + addElement.Id.ToString() + "/" + addElement.Image;
                    }
                }
            }

            double? baseCurrencyRate = 0;
            if (user.CurrencyId != null && user.CurrencyId > 0)
                baseCurrencyRate = menuItems.Select(m => m.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favRestaurants = _customerService.GetCustomerFavoriteMenuItems(user.Id);
            var restaurantMenuItemDetailedDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestaurantMenuItemDetailedDTO>>(menuItems);

            restaurantMenuItemDetailedDTO.Select(m => { m.IsFavourite = favRestaurants.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
            restaurantMenuItemDetailedDTO.Select(m => { m.PreferredCurrencySymbol = user.Currency.Symbol; return m; }).ToList();

            foreach (var resItem in restaurantMenuItemDetailedDTO)
            {
                var imageUrl = Url.Content("/Images/");
                BuildRestaurantMenuItemsImages(resItem, imageUrl);

                if (baseCurrencyRate != 0)
                {
                    resItem.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(resItem.Price) * baseCurrencyRate), 2);
                    foreach (var menuAddElements in resItem.MenuItemAdditionals.Select(a => a.MenuAdditionalGroup.MenuAdditionalElements))
                    {
                        menuAddElements.Select(i => { i.PreferredCurrencyAdditionalCost = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.AdditionalCost) * baseCurrencyRate), 2); return i; }).ToList();
                    }
                }
            }

            var mod = total % limit;
            var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = restaurantMenuItemDetailedDTO });
        }

        /// <summary>
        /// Create new reviews for menu item
        /// </summary>
        /// <remarks>
        /// Adds new reviews for menu item in the backend
        /// </remarks>
        /// <param name="reviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of menu item reviews</returns>

        [Route("restaurants/menuItemReviews")]
        [HttpPost]
        [ResponseType(typeof(MenuItemReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]
        public IHttpActionResult CreateMenuItemReview(CreateMenuItemReviewDTO reviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var menuItem = _restaurantService.GetMenuItem(reviewDTO.RestaurantMenuItemId, null);
            if (menuItem == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.MenuItemId_NotFound } });

            var entity = Mapper.Map<MenuItemReview>(reviewDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            entity = _restaurantService.CreateMenuItemReview(entity);

            var menuItemReviewDTO = Mapper.Map<MenuItemReviewDTO>(entity);
            return Content(HttpStatusCode.OK, menuItemReviewDTO);
        }

        /// <summary>
        /// Returns list of Menu Item reviews
        /// </summary>
        /// <remarks>
        /// Get list of the Menu Item reviews.
        /// </remarks>
        /// <param name="id">Menu Item Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Limit number of reviews on each page</param>
        /// <returns>List of Menu Item Reviews</returns>
        /// 

        [Route("restaurants/menuItem/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<MenuItemReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItemReviews(int id, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var menuItemReviews = _restaurantService.GetMenuItemReviews(id, page.Value, limit.Value, out total);
            if (menuItemReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var menuItemReviewDTO = Mapper.Map<IEnumerable<MenuItemReview>, List<MenuItemReviewDTO>>(menuItemReviews);
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemReviewDTO });
        }

        /// <summary>
        /// Returns restaurant menus
        /// </summary>
        /// <remarks>
        /// Get a detailed list of restaurant menus using restaurant Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <param name="considerMyPreferences">My preference</param>
        /// <param name="languageId">Language Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of Restaurant daily menus</returns>

        [HttpGet]
        [Route("restaurants/{id}/Menus")]

        [ResponseType(typeof(List<ConcreteRestaurantMenusDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Menu found this restaurant")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetRestaurantMenus(int id, bool considerMyPreferences = false, int? languageId = null, int? page = 1, int? limit = null)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var restaurantMenus = _restaurantService.GetDailyMenu(id, user.Id, languageId, ord => ord.Id, page.Value, limit.Value, out total, considerMyPreferences);
            if (restaurantMenus == null || restaurantMenus.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Menufound } });

            var restaurantMenusDTO = Mapper.Map<IEnumerable<RestaurantMenu>, List<ConcreteRestaurantMenusDTO>>(restaurantMenus);
            foreach (var menu in restaurantMenusDTO)
            {
                if (!string.IsNullOrWhiteSpace(menu.Image))
                    menu.Image = Url.Content("/Images/Restaurant/") + menu.RestaurantId.ToString() + "/Menu/" + menu.Id.ToString() + "/" + menu.Image;
            }
            if (considerMyPreferences)
            {
                restaurantMenusDTO.Select(m => { m.MyPreference = true; return m; }).ToList();
            }
            else
            {
                restaurantMenusDTO.Select(m => { m.MyPreference = false; return m; }).ToList();
                var preferredMenuIds = _restaurantService.GetCustomerPreferredMenus(user.Id, id).Select(m => m.Id).Distinct();
                if (preferredMenuIds.Any())
                {
                    foreach (var restMenu in restaurantMenusDTO)
                    {
                        if (preferredMenuIds.Contains(restMenu.Id))
                            restMenu.MyPreference = true;
                    }
                }
            }

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = restaurantMenusDTO });
        }

        /// <summary>
        /// Return all menu groups of concrete menu
        /// </summary>
        /// <remarks>
        /// Return all menu groups of concrete menu. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Menu Id</param>
        /// <param name="considerMyPreferences">My Preference</param>
        /// <param name="languageId">LanguageId</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Return menu groups of concrete menu</returns>
        [HttpGet]
        [Route("restaurants/Menu/{id}/menuGroups")]
        [ResponseType(typeof(RestMenuGroupsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Menu Groups not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetRestaurantMenuGroups(int id, bool considerMyPreferences = false, int? languageId = null, int? page = 1, int? limit = null)
        {
            try
            {
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                limit = limit ?? ToOrderConfigs.DefaultPageSize;
                int total;
                var menuGroups = _restaurantService.GetMenuGroupsByMenuId(id, user.Id, page.Value, limit.Value, out total, languageId, considerMyPreferences);
                if (menuGroups.Count() <= 0)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.MenuGroups_NotFound } });

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = menuGroups.Select(m => m.RestaurantMenuItems.Select(i => i.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(ex => ex.ToCurrency == user.CurrencyId).Select(br => br.BaseRate).FirstOrDefault()).FirstOrDefault()).FirstOrDefault();

                var menuGroupsDTO = Mapper.Map<IEnumerable<RestaurantMenuGroupsOfMenu>, List<RestMenuGroupsDTO>>(menuGroups);
                menuGroupsDTO.Select(m => { m.MyPreference = false; return m; }).ToList();

                var favRestaurants = _customerService.GetCustomerFavoriteMenuItems(user.Id);
                var preferedMenuItems = _customerService.GetCustomerPreferredMenuItems(user.Id);

                foreach (var menuItems in menuGroupsDTO.Select(m => m.RestaurantMenuItems))
                {
                    menuItems.Select(m => { m.MyPreference = preferedMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
                    menuItems.Select(m => { m.IsFavourite = favRestaurants.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();

                    if (baseCurrencyRate != 0)
                        menuItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();
                    menuItems.Select(i => { i.PreferredCurrencySymbol = user.Currency.Symbol; return i; }).ToList();

                    foreach (var menuItem in menuItems)
                    {
                        if (menuItem.SoldOut == true)
                        {
                            menuItem.RestaurantMenuItemImages = Enumerable.Empty<RestaurantMenuItemImagesDTO>().ToList<RestaurantMenuItemImagesDTO>();
                            var img = new RestaurantMenuItemImagesDTO()
                            {
                                RestaurantMenuItemId = menuItem.Id,
                                Image = Url.Content("/Images/sold-out.png"),
                                IsPrimary = true,
                                IsActive = true
                            };
                            menuItem.RestaurantMenuItemImages.Add(img);
                        }
                        else
                        {
                            foreach (var itemImage in menuItem.RestaurantMenuItemImages)
                            {
                                if (!string.IsNullOrWhiteSpace(itemImage.Image))
                                    itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                            }
                        }
                    }
                }
                var menuGrpsId = _restaurantService.GetCustomerPreferredMenuGroups(user.Id, id).Select(mg => mg.Id).ToList();
                foreach (var menuGroup in menuGroupsDTO)
                {
                    if (menuGrpsId.Contains(menuGroup.Id))
                        menuGroup.MyPreference = true;
                }
                var mod = total % limit.Value;
                var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuGroupsDTO });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

        }

        /// <summary>
        /// Returns list of Menu Items using menu groupId
        /// </summary>
        /// <remarks>
        /// Get list of Menu Items.
        /// </remarks>
        /// <param name="id">Menu group Id</param>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="considerMyPreferences">My preference</param>
        /// <param name="languageId">Language Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Detailed list of Menu Items</returns>
        [HttpGet]
        [Route("restaurants/menuGroup/{id}/menuItems")]
        [ResponseType(typeof(List<RestMenuItemsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Item Not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Menu" })]

        public IHttpActionResult GetMenuItemsByGroupId(int id, string searchText = "", bool considerMyPreferences = false, int? languageId = null, int? page = 1, int? limit = null)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var menuItems = _restaurantService.GetMenuItemsByGroupIdAndSearchText(id, user.Id, searchText, page.Value, limit.Value, out total, languageId, considerMyPreferences);
            if (menuItems == null || menuItems.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_RestMenuItem } });

            double? baseCurrencyRate = 0;
            if (user.CurrencyId != null && user.CurrencyId > 0)
                baseCurrencyRate = menuItems.Select(m => m.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(user.Id);
            var preferedMenuItems = _customerService.GetCustomerPreferredMenuItems(user.Id).Where(i => i.RestaurantMenuGroupId == id);

            var menuItemDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestMenuItemsDTO>>(menuItems);
            menuItemDTO.Select(m => { m.MyPreference = preferedMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
            menuItemDTO.Select(m => { m.IsFavourite = favMenuItems.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();

            foreach (var menuItem in menuItemDTO)
            {
                if (menuItem.SoldOut == true)
                {
                    menuItem.RestaurantMenuItemImages = Enumerable.Empty<RestaurantMenuItemImagesDTO>().ToList<RestaurantMenuItemImagesDTO>();
                    var img = new RestaurantMenuItemImagesDTO()
                    {
                        RestaurantMenuItemId = menuItem.Id,
                        Image = Url.Content("/Images/sold-out.png"),
                        IsPrimary = true,
                        IsActive = true
                    };
                    menuItem.RestaurantMenuItemImages.Add(img);
                }
                else
                {
                    foreach (var itemImage in menuItem.RestaurantMenuItemImages)
                    {
                        if (!string.IsNullOrWhiteSpace(itemImage.Image))
                            itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                    }
                }
            }

            if (baseCurrencyRate != 0)
                menuItemDTO.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();
            menuItemDTO.Select(i => { i.PreferredCurrencySymbol = user.Currency.Symbol; return i; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemDTO });
        }

        #endregion

        #region restaurant reviews

        /// <summary>
        /// Returns restaurant's reviews
        /// </summary>
        /// <remarks>
        /// Get a restaurant's customer reviews using restaurant Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Limit number of reviews on each page</param>
        /// <returns>returns reviews</returns>

        [HttpGet]
        [Route("restaurants/{id}/reviews")]

        [ResponseType(typeof(List<RestaurantReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No reviews Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Reviews" })]
        public IHttpActionResult GetRestaurantReviews(int id, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;
            var reviews = _restaurantService.GetRestaurantReviews(id, page.Value, limit.Value, out total);

            if (reviews == null || reviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var reviewsDTO = Mapper.Map<IEnumerable<RestaurantReview>, List<RestaurantReviewDTO>>(reviews);
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = reviewsDTO });
        }

        /// <summary>
        /// Returns Review by Id
        /// </summary>
        /// <remarks>
        /// Get a detailed of customer restaurant reviews using Review Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Restaurant Customer Reviews</returns>
        [HttpGet]
        [Route("restaurants/RestaurantReviews/{id}", Name = "GetReviewById")]

        [ResponseType(typeof(RestaurantReviewDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Reviews" })]

        public IHttpActionResult GetReviewById(int id)
        {
            var review = _restaurantService.GetReviewById(id);

            if (review == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewDTO = Mapper.Map<RestaurantReviewDTO>(review);

            return Content(HttpStatusCode.OK, reviewDTO);
        }


        /// <summary>
        /// Create a new customer restaurant reviews
        /// </summary>
        /// <remarks>
        /// Adds new customer restaurant reviews in the backend
        /// </remarks>
        /// <param name="reviewDTO">Review Details in Request body</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("restaurants/RestaurantReviews")]
        [HttpPost]
        [ResponseType(typeof(RestaurantReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Reviews" })]
        public IHttpActionResult CreateRestaurantReview(CreateRestaurantReviewDTO reviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var entity = Mapper.Map<RestaurantReview>(reviewDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            entity = _restaurantService.CreateRestaurantReview(entity);

            return Created(Url.Link("GetReviewById", new { id = entity.Id }), Mapper.Map<RestaurantReviewDTO>(entity));

        }

        /// <summary>
        /// Delete Review by id
        /// </summary>
        /// <remarks>
        /// Delete Review based on Review id.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Success</returns>
        [Route("restaurants/RestaurantReviews/{id}")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Restaurant Reviews" })]
        public IHttpActionResult DeleteReview(int id)
        {
            var review = _restaurantService.GetReviewById(id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }

            _restaurantService.DeleteRestaurantReview(id);

            return Ok();

        }


        #endregion

        #region Table Reservation

        ///// <summary>
        ///// Return Customer's reserved Table details based on id
        ///// </summary> 
        ///// <remarks>
        ///// Get a detailed  of customer's reserved Table details using youORDR Customer_Table_Reservation_ID. Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">Customer Table Reservation ID</param>
        ///// <returns>Customer Table Reservations Details</returns>
        //[Route("restaurants/tableReservation/{id}", Name = "GetCustomerReservedTable")]
        //[HttpGet]
        //[ResponseType(typeof(CustomerTableReservationsDTO))]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "No Reservation Found")]
        //[SwaggerOperation(Tags = new[] { "Restaurants" })]
        //public IHttpActionResult GetCustomerReservedTableById(int id)
        //{
        //    var reservedTable = _restaurantService.GetCustomerReservedTableById(id);

        //    if (reservedTable == null)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

        //    var reservedTableDTO = Mapper.Map<CustomerTableReservationsDTO>(reservedTable);

        //    return Content(HttpStatusCode.OK, reservedTableDTO);
        //}


        ///// <summary>
        ///// Customer table Reservation
        ///// </summary>
        ///// <remarks>
        ///// Create new table Reservation for restuarnat.
        ///// </remarks>
        ///// <param name="customerTableReservationsDTO">Customer Table Reservations</param>
        ///// <returns>returns Customer Table Reservations</returns>
        //[Route("restaurants/tableReservation")]
        //[HttpPost]

        //[ResponseType(typeof(CustomerTableReservationsDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Tables not found")]
        //[SwaggerOperation(Tags = new[] { "Restaurants" })]

        //public IHttpActionResult CreateReservation(CreateCustomerTableReservationsDTO customerTableReservationsDTO)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    var entity = Mapper.Map<Customer_Table_Reservations>(customerTableReservationsDTO);

        //    var availabilTable = _restaurantService.ReserveTable(entity.No_Of_Seats, entity.Reservation_DateTime);

        //    if (availabilTable.Count() == 0)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });
        //    else
        //        entity = _restaurantService.CreateReservation(entity, availabilTable.ToList());

        //    return Created(Url.Link("GetCustomerReservedTable", new { id = entity.Customer_Table_Reservation_ID }), Mapper.Map<CustomerTableReservationsDTO>(entity));

        //}

        #endregion

        #region Restaurants Allergens/Ingredient Details
        /// <summary>
        /// Returns all available allergen categories along with allergens 
        /// </summary>
        /// <remarks>
        /// Returns all available allergen categories along with allergens. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of allergen categories along with allergens </returns>
        [HttpGet]
        [Route("restaurants/allergen_categories")]

        [ResponseType(typeof(List<AllergenCategoriesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Allergen Categories Not Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Ingredient/Allergens Details" })]

        public IHttpActionResult GetAllergenCategories()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var allergenCategories = _restaurantService.GetAllAllergenCategories();
            if (allergenCategories == null || allergenCategories.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.AllergenCat_NotFound } });

            var allergenCatDTO = Mapper.Map<IEnumerable<AllergenCategory>, List<AllergenCategoriesDTO>>(allergenCategories);

            return Content(HttpStatusCode.OK, allergenCatDTO);

        }

        /// <summary>
        /// Returns all available ingredient categories along with ingredients 
        /// </summary>
        /// <remarks>
        /// Returns all available ingredient categories along with ingredient. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of ingredient categories along with ingredient </returns>
        [HttpGet]
        [Route("restaurants/ingredient_categories")]

        [ResponseType(typeof(List<IngredientCategoriesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Ingredient Categories Not Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Ingredient/Allergens Details" })]

        public IHttpActionResult GetIngredientCategories()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var ingredientCategories = _restaurantService.GetAllIngredientCategories();
            if (ingredientCategories == null || ingredientCategories.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.IngredientCat_NotFound } });

            var ingredientCatDTO = Mapper.Map<IEnumerable<IngredientCategory>, List<IngredientCategoriesDTO>>(ingredientCategories);

            return Content(HttpStatusCode.OK, ingredientCatDTO);

        }
        /// <summary>
        /// Returns all available food profiles 
        /// </summary>
        /// <remarks>
        /// Returns all available food profile. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of food profiles </returns>
        [HttpGet]
        [Route("restaurants/food_profiles")]

        [ResponseType(typeof(List<FoodProfileDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Food Profiles Not Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Ingredient/Allergens Details" })]

        public IHttpActionResult GetFoodProfiles()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var foodProfiles = _restaurantService.GetAllFoodProfiles();
            if (foodProfiles == null || foodProfiles.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.FoodProfile_NotFound } });

            var foodProfilesDTO = Mapper.Map<IEnumerable<FoodProfile>, List<FoodProfileDTO>>(foodProfiles);

            return Content(HttpStatusCode.OK, foodProfilesDTO);

        }
        #endregion Restaurants Allergens/Ingredient Details

        /*Restaurant List Based on User Preferences*/

        /// <summary>
        /// Returs all restaurants Based on User Preferences
        /// </summary>
        /// <remarks>
        /// Get a detailed list of preference restaurants. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of restaurants</returns>
        [HttpGet]
        [Route("restaurants/preferences", Name = "GetPreferences")]

        [ResponseType(typeof(List<GetRestaurantsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurants Preferences Found")]
        [SwaggerOperation(Tags = new[] { "Preference Restaurants" })]

        public IHttpActionResult GetAllPreferenceRestaurants()
        {
            var user = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetAllRestaurants();
            if (restaurants == null || restaurants.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<GetRestaurantsDTO>>(restaurants);
            restaurantsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
            if (user != null)
            {
                var favRestaurants = _customerService.GetFavoriteRestaurants(user.Id);
                if (favRestaurants != null && favRestaurants.Count() > 0)
                {
                    foreach (var res in restaurantsDTO)
                    {
                        if (favRestaurants.Any(r => r.Id == res.Id))
                            res.IsFavourite = true;
                    }
                }
            }
            return Content(HttpStatusCode.OK, restaurantsDTO);
        }


        #region Cuisines

        /// <summary>
        /// Returs all available Cuisines
        /// </summary>
        /// <remarks>
        /// Get a detailed list of available Cuisines. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of Cuisines</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("restaurants/cuisines")]

        [ResponseType(typeof(List<CuisineDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Cuisines Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Cuisines" })]

        public IHttpActionResult GetCuisines()
        {
            var cuisines = _restaurantService.GetAllCuisines();
            if (cuisines == null || cuisines.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var cuisinesDTO = Mapper.Map<IEnumerable<Cuisine>, List<CuisineDTO>>(cuisines);

            return Content(HttpStatusCode.OK, cuisinesDTO);

        }


        /// <summary>
        /// Returs Cuisines Served by Restaurant
        /// </summary>
        /// <remarks>
        /// List of cuisines served by restaurant.
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <returns>List of Cuisines</returns>
        [HttpGet]
        [Route("restaurants/{id}/cuisines")]

        [ResponseType(typeof(List<RestCuisinesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No cuisines Found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Cuisines" })]
        public IHttpActionResult GetRestaurantCuisines(int id)
        {

            var cuisines = _restaurantService.GetRestaurantCuisines(id);

            if (cuisines == null || cuisines.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Not_Found_Cuisines } });

            var cuisinesDTO = Mapper.Map<IEnumerable<RestaurantCuisine>, List<RestCuisinesDTO>>(cuisines);

            return Content(HttpStatusCode.OK, cuisinesDTO);
        }

        #endregion Cuisines

        #region Restaurant Cart

        /// <summary>
        /// Returs the Restaurant Cart of specified customer
        /// </summary>
        /// <remarks>
        /// Returs the Restaurant Cart of specified customer
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <returns>Returns Cart Details</returns>
        [HttpGet]
        [Route("restaurants/{id}/cart", Name = "GetCart")]

        [ResponseType(typeof(RestaurantCartDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Cart Not Available for restaurant")]
        [SwaggerOperation(Tags = new[] { "Restaurant Cart" })]
        public IHttpActionResult GetRestaurantCart(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var cart = _restaurantService.GetCustomerCart(customer.Id, id);
            if (cart == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cart_IsEmpty } });

            cart.RestaurantCartItems = cart.RestaurantCartItems.OrderBy(c => c.ServeLevel).ToList();
            var restaurantCartDTO = Mapper.Map<RestaurantCart, RestaurantCartDTO>(cart);
            return Content(HttpStatusCode.OK, restaurantCartDTO);
        }


        /// <summary>
        /// Creates new cart /  basket
        /// </summary>
        /// <remarks>
        /// Creates customer restaurant cart /  basket
        /// </remarks>
        /// <param name="createCartDTO">item Details in Request body</param>
        /// <param name="orderType">Choose order type</param>
        /// <param name="table_room_Id">Table/Room Id</param>
        /// <param name="flush">Flushes old cart and creates new one</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("restaurants/cart")]
        [HttpPost]
        [ResponseType(typeof(RestaurantCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error or Cart Already exists")]
        [SwaggerOperation(Tags = new[] { "Restaurant Cart" })]
        public IHttpActionResult CreateRestaurantCart(CreateCartDTO createCartDTO, OrderType orderType = OrderType.Table, int? table_room_Id = null, bool flush = false)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            int restaurantId = 0;
            if (createCartDTO.RestaurantCartItems != null && createCartDTO.RestaurantCartItems.Any())
            {
                foreach (var item in createCartDTO.RestaurantCartItems)
                {
                    if (item.RestaurantMenuItemId == 0)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID" } });

                    var menuItem = _restaurantService.GetMenuItem(item.RestaurantMenuItemId, null);
                    if (menuItem == null)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                    var restaurantID = menuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId;
                    if (restaurantId == 0)
                        restaurantId = restaurantID;

                    if (restaurantId != restaurantID)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                    if (item.Qty == null)
                        item.Qty = 1;

                    if (item.RestaurantCartItemAdditionals != null)
                        foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                        {
                            if (additionalItem.MenuAdditionalElementId == 0)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ADDITIONAL_ELEMENT_ID" } });

                            var details = _restaurantService.GetMenuAdditionalElementById(additionalItem.MenuAdditionalElementId);
                            if (details == null)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId } });

                            if (details.MenuAdditionalGroup.HasQuantity)
                            {
                                if (additionalItem.Qty == null)
                                    //additionalItem.Qty = 1;
                                    additionalItem.Qty = item.Qty;
                                else if (additionalItem.Qty < 1)
                                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "ADDITIONAL_ELEMENT_QUANTITY_SHOULDN'T_BE_ZERO_OR_NEGATIVE_FOR_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId } });
                            }
                            else
                                if (additionalItem.Qty == null)
                                    additionalItem.Qty = 0;
                                else if (additionalItem.Qty != 0)
                                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "YOU_CAN'T_SET_QUANTITY_FOR_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId + " SINCE_HASQUANTITY_PARAMETER_HAS_BEEN_SET_TO_FALSE" } });
                        }

                    var additionals = menuItem.MenuItemAdditionals;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.MenuAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if ((!addGroup.MinimumSelected.HasValue || addGroup.MinimumSelected == 0) && (!addGroup.MaximumSelected.HasValue || addGroup.MaximumSelected == 0))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (item.RestaurantCartItemAdditionals != null && item.RestaurantCartItemAdditionals.Any())
                                foreach (var cItemAdditional in item.RestaurantCartItemAdditionals)
                                {
                                    if (addGroup.MenuAdditionalElements.Any(i => i.Id == cItemAdditional.MenuAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            if (addGroup.MinimumSelected.HasValue && cartAdditionalsCount < addGroup.MinimumSelected.Value)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "MINIMUM_ITEM_ADDITIONAL_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinimumSelected.Value.ToString() + " FOR_RESTAURANT_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                            if (addGroup.MaximumSelected.HasValue && cartAdditionalsCount > addGroup.MaximumSelected.Value)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "MAXIMUM_ITEM_ADDITIONAL_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaximumSelected.Value.ToString() + " FOR_RESTAURANT_MENU_ITEM_ID " + item.RestaurantMenuItemId } });
                        }
                    }

                }
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<RestaurantCart>(createCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            entity.RestaurantId = restaurantId;
            try
            {
                entity = _restaurantService.CreateRestaurantCart(entity, orderType, table_room_Id, flush);
                entity.Restaurant = _restaurantService.GetRestaurantById(entity.RestaurantId);

                var cartDTO = Mapper.Map<RestaurantCart, RestaurantCartDTO>(entity);
                return Created(Url.Link("GetCart", new { id = cartDTO.RestaurantId }), new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing item id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }


        /// <summary>
        /// Updates restaurant cart /  basket
        /// </summary>
        /// <remarks>
        /// Updates customer restaurant cart. Ex To update qty just send new quantity of item.
        /// </remarks>
        /// <param name="updateCartDTO">Just specify the new properties</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("restaurants/cart")]
        [HttpPut]
        [ResponseType(typeof(RestaurantCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Restaurant Cart" })]
        public IHttpActionResult UpdateRestaurantCart(UpdateCartDTO updateCartDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            int restaurantId = 0;
            if (updateCartDTO.RestaurantCartItems != null && updateCartDTO.RestaurantCartItems.Any())
            {
                foreach (var item in updateCartDTO.RestaurantCartItems)
                {
                    if (item.RestaurantMenuItemId == 0)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID" } });

                    var menuItem = _restaurantService.GetMenuItem(item.RestaurantMenuItemId, null);
                    if (menuItem == null)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                    var restaurantID = menuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId;
                    if (restaurantId == 0)
                        restaurantId = restaurantID;

                    if (restaurantId != restaurantID)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                    var additionals = menuItem.MenuItemAdditionals;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.MenuAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            // if (!addGroup.MinimumSelected.HasValue && (!addGroup.MaximumSelected.HasValue))
                            if ((!addGroup.MinimumSelected.HasValue || addGroup.MinimumSelected == 0) && (!addGroup.MaximumSelected.HasValue || addGroup.MaximumSelected == 0))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (item.RestaurantCartItemAdditionals != null && item.RestaurantCartItemAdditionals.Any())
                                //    cartAdditionalsCount = item.RestaurantCartItemAdditionals.Count();

                                foreach (var cItemAdditional in item.RestaurantCartItemAdditionals)
                                {
                                    if (addGroup.MenuAdditionalElements.Any(i => i.Id == cItemAdditional.MenuAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }

                            if (addGroup.MinimumSelected.HasValue && cartAdditionalsCount < addGroup.MinimumSelected.Value)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "MINIMUM_ITEM_ADDITIONAL_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinimumSelected.Value.ToString() + " FOR_RESTAURANT_MENU_ITEM_ID " + item.RestaurantMenuItemId } });

                            if (addGroup.MaximumSelected.HasValue && cartAdditionalsCount > addGroup.MaximumSelected.Value)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "MAXIMUM_ITEM_ADDITIONAL_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaximumSelected.Value.ToString() + " FOR_RESTAURANT_MENU_ITEM_ID " + item.RestaurantMenuItemId } });
                        }
                    }

                    if (item.Qty == null)
                        item.Qty = 1;
                    if (item.ServeLevel == null)
                        item.ServeLevel = menuItem.RestaurantMenuGroup.ServeLevel;

                    if (item.RestaurantCartItemAdditionals != null)
                        foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                        {
                            if (additionalItem.MenuAdditionalElementId == 0)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ADDITIONAL_ELEMENT_ID" } });

                            var details = _restaurantService.GetMenuAdditionalElementById(additionalItem.MenuAdditionalElementId);
                            if (details == null)
                                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId } });

                            if (details.MenuAdditionalGroup.HasQuantity)
                            {
                                if (additionalItem.Qty == null)
                                    //additionalItem.Qty = 1;
                                    additionalItem.Qty = item.Qty;
                                else if (additionalItem.Qty < 1)
                                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "ADDITIONAL_ELEMENT_QUANTITY_SHOULDN'T_BE_ZERO_OR_NEGATIVE_FOR_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId } });
                            }
                            else
                                if (additionalItem.Qty == null)
                                    additionalItem.Qty = 0;
                                else if (additionalItem.Qty != 0)
                                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "YOU_CAN'T_SET_QUANTITY_FOR_MENU_ADDITIONAL_ELEMENT_ID " + additionalItem.MenuAdditionalElementId + " SINCE_HASQUANTITY_PARAMETER_HAS_BEEN_SET_TO_FALSE" } });
                        }
                }
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var entity = Mapper.Map<RestaurantCart>(updateCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            entity.RestaurantId = restaurantId;
            try
            {
                entity = _restaurantService.UpdateRestaurantCart(authenticatedCustomer.Id, entity);
                entity.Restaurant = _restaurantService.GetRestaurantById(entity.RestaurantId);

                var cartDTO = Mapper.Map<RestaurantCart, RestaurantCartDTO>(entity);
                return Content(HttpStatusCode.OK, new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (ZeroItemQtyException zeroQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Zero_Menu_Item_Qty } });
            }
            catch (NegativeItemQtyException negativeQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Negative_Item_Qty } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing item id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        #endregion Restaurant Cart

        #region Restaurant Order

        /// <summary>
        /// Returs the Restaurant Order details
        /// </summary>
        /// <remarks>
        /// Returs the Restaurant Order details
        /// </remarks>
        /// <returns>Returns Cart Details</returns>
        [HttpGet]
        [Route("restaurants/order", Name = "GetOrder")]

        [ResponseType(typeof(RestaurantOrderDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult GetRestaurantOrder(int orderId)
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _restaurantService.GetOrdersDetailsById(orderId);

            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            order.RestaurantOrderItems = order.RestaurantOrderItems.OrderBy(o => o.ServeLevel).ToList();

            if (order.CustomerId != customer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var restaurantOrderDTO = Mapper.Map<RestaurantOrder, RestaurantOrderDTO>(order);

            restaurantOrderDTO.ReceiptNumber = "#" + order.Id;
            Status_Type ordrStatus = (Status_Type)order.OrderStatus;
            restaurantOrderDTO.OrderStatus = ordrStatus.ToString();

            return Content(HttpStatusCode.OK, restaurantOrderDTO);
        }

        /// <summary>
        /// Place order for items in basket
        /// </summary>
        /// <remarks>
        /// Place order for cart items
        /// </remarks>
        /// <param name="scheduleDate">Order schedule date time</param>
        /// <param name="cardId"> select customer Card</param>
        /// <param name="restaurantId">Restautant Id</param>
        /// <returns>Returns details of newly created order</returns>

        [Route("restaurants/order")]
        [HttpPost]
        [ResponseType(typeof(RestaurantOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult PlaceRestaurantOrderFromCart(int restaurantId, DateTime scheduleDate, int? cardId = null)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            RestaurantOrder entity;
            try
            {
                entity = _restaurantService.CreateRestaurantOrderFromCart(authenticatedCustomer.Id, cardId, restaurantId, scheduleDate);

                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();

                var result = Mapper.Map<RestaurantOrder, RestaurantOrderDTO>(entity);
                result.ReceiptNumber = "#" + entity.Id;
                Status_Type ordrStatus = (Status_Type)entity.OrderStatus;
                result.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = result });
            }
            catch (CartEmptyException empty)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Cart_Empty } });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_ScheduleDateTime } });
            }
            catch (CardSpendingLimitException limitEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_Card_SpendingLimit } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing item id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Schedule your  order
        /// </summary>
        /// <remarks>
        /// Schedule your  order
        /// </remarks>
        /// <param name="orderId">Enter order Id</param>
        /// <param name="scheduleDate">Order schedule date time</param>
        /// <returns>Returns details of scheduled order</returns>

        [Route("restaurants/order_schedule")]
        [HttpPut]
        [ResponseType(typeof(RestaurantOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult ScheduleRestaurantOrder(int orderId, DateTime scheduleDate)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            RestaurantOrder entity;
            try
            {
                entity = _restaurantService.ScheduleRestaurantOrder(authenticatedCustomer.Id, orderId, scheduleDate);

                var result = Mapper.Map<RestaurantOrder, RestaurantOrderDTO>(entity);
                result.ReceiptNumber = "#" + entity.Id;
                Status_Type ordrStatus = (Status_Type)entity.OrderStatus;
                result.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = result });
            }
            catch (OrderNotFoundException orderNotFound)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_ScheduleDateTime } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }


        /// <summary>
        /// Create new reviews for Restaurant order
        /// </summary>
        /// <remarks>
        /// Adds new reviews for restaurant order in the backend
        /// </remarks>
        /// <param name="orderReviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of menu item reviews</returns>

        [Route("restaurants/order/Review")]
        [HttpPost]
        [ResponseType(typeof(RestaurantOrderReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult CreateRestaurantOrderReview(CreateRestaurantOrderReviewDTO orderReviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var order = _restaurantService.GetOrdersDetailsById(orderReviewDTO.RestaurantOrderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });
            if (order.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var entity = Mapper.Map<RestaurantOrderReview>(orderReviewDTO);
            entity = _restaurantService.CreateRestaurantOrderReview(entity);
            var orderReviewsDTO = Mapper.Map<RestaurantOrderReviewsDTO>(entity);

            return Content(HttpStatusCode.OK, orderReviewsDTO);
        }

        /// <summary>
        /// Returns list of order reviews
        /// </summary>
        /// <remarks>
        /// Get list of order reviews.
        /// </remarks>
        /// <param name="id">Restaurant Order Id</param>
        /// <returns>List of order Reviews</returns>
        /// 

        [Route("restaurants/order/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<RestaurantOrderReviewsDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]

        public IHttpActionResult GetOrderReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var order = _restaurantService.GetOrdersDetailsById(id);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });
            if (order.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var orderReviews = _restaurantService.GetOrderReviews(id);
            if (orderReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var orderReviewDTO = Mapper.Map<IEnumerable<RestaurantOrderReview>, List<RestaurantOrderReviewsDTO>>(orderReviews);
            return Content(HttpStatusCode.OK, orderReviewDTO);
        }

        /// <summary>
        /// Pay the order using the card
        /// </summary>
        /// <param name="payDTO">Request data</param>
        /// <returns>Status</returns>
        [Route("restaurants/order/Pay")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult PayOrder(CreateRestaurantPayDTO payDTO)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.CardPayment(payDTO.OrderId, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, payDTO.CardId, authenticatedCustomer.Id);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (CardSpendingLimitException limitEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_Card_SpendingLimit } });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        /// <summary>
        /// Cancel the restaurant order and release the amount reserved
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns order status</returns>
        [Route("restaurants/order/{orderId}/Cancel")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult CancelOrder(int orderId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                var restOrder = _restaurantService.GetOrdersDetailsById(orderId);
                if (restOrder == null)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Order Id Not found " } });

                if (restOrder.OrderStatus != 0)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "This order cannot be cancel, since it is already processing" } });

                //if (restOrder.CardId != null)
                //{
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.CancelOrderAndReleaseAmount(restOrder, ToOrderConfigs.QuickPayAuthToken, quickPayUrl);
                return Content(HttpStatusCode.OK, string.Format(response));
                //}
                //else
                //{
                //    var paypalBaseUrl = ConfigurationManager.AppSettings["PaypalBaseURL"];
                //    var token = ConfigurationManager.AppSettings["PaypalAuthorizationKey"];

                //    var response = _paymentService.CancelOrderAndReleasePaypalAmount(restOrder, paypalBaseUrl, token);
                //    return Content(HttpStatusCode.OK, string.Format(response));
                //}

            }
            catch (NotFoundPaypalPaymentId ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (NotCapturevoided ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Failed" } });
            }

        }

        /// <summary>
        /// Pay the order tips using the card
        /// </summary>
        /// <param name="payDTO">Request data</param>
        /// <returns>Status</returns>
        [Route("restaurants/order_tip/Pay")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Tips paid successfully")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult PayOrderTips(CreateRestaurantTipPayDTO payDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.TipsPaymentUsingCard(payDTO.OrderId, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, payDTO.TipAmount, payDTO.Comment, payDTO.CardId, authenticatedCustomer.Id);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        ///// <summary>
        ///// Pay the order using the paypal
        ///// </summary>
        ///// <param name="payPalDTO">Request data</param>        
        ///// <returns>Status</returns>
        //[Route("restaurants/order/Paypal")]
        //[HttpPost]
        //[ResponseType(typeof(string))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        //public IHttpActionResult PaymentUsingPaypal(CreateRestaurantPaypalPayDTO payPalDTO)
        //{
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }
        //    try
        //    {
        //        var payment = _paymentService.PaypalPayment(authenticatedCustomer.Id, payPalDTO.PaypalEmailId, payPalDTO.OrderId, payPalDTO.PaypalCardId);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Invalid Paypal Email" } });
        //    }
        //    return Content(HttpStatusCode.OK, string.Format("success {0}", payPalDTO.OrderId));
        //}
        [Route("restaurants/order/PaypalToken")]
        [HttpGet]
        [ResponseType(typeof(Object))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]

        public IHttpActionResult GetPaypalToken()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                var token = ConfigurationManager.AppSettings["PaypalAuthorizationKey"];
                var baseAddress = ConfigurationManager.AppSettings["PaypalBaseURL"];
                var response = _paymentService.GenerateFullPaypalToken(token, baseAddress);

                return Ok(response);
            }
            catch (Exception ex)
            {
                dynamic responseJSON = JsonConvert.DeserializeObject(ex.Message);
                return Content(HttpStatusCode.BadRequest, new { messages = responseJSON });
            }
        }


        /// <summary>
        /// Get paypal payment approval
        /// </summary>
        /// <param name="orderId">Enter order Id</param>
        /// <param name="paymentID">Payment Id</param>
        /// <returns>Status</returns>
        [Route("restaurants/order/Paypal")]
        [HttpPost]
        [ResponseType(typeof(Object))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult PaymentUsingPaypal(int orderId, string paymentID)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _restaurantService.GetOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            try
            {
                var token = ConfigurationManager.AppSettings["PaypalAuthorizationKey"];
                var baseAddress = ConfigurationManager.AppSettings["PaypalBaseURL"];
                dynamic responseKey = _paymentService.GeneratePaypalToken(token, baseAddress);

                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + responseKey);

                    client.BaseAddress = baseAddress;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                    var response = client.DownloadString("/v1/payments/payment/" + paymentID);
                    dynamic result = JsonConvert.DeserializeObject(response);
                    var state = result["state"];

                    //want to Authorized payment
                    if (state == "approved")
                    {
                        var authorizationId = result["transactions"][0]["related_resources"][0]["authorization"]["id"];
                        var authorizationState = result["transactions"][0]["related_resources"][0]["authorization"]["state"];
                        if (authorizationState == "authorized")
                        {
                            response = client.DownloadString("/v1/payments/authorization/" + authorizationId);
                            result = JsonConvert.DeserializeObject(response);
                            state = result["state"];
                        }
                        else
                        {
                            return Content(HttpStatusCode.Forbidden, new { messages = "Pass 'intent' parameter as 'authorize' to make this payment" });
                        }
                    }
                    else
                    {
                        return Content(HttpStatusCode.Forbidden, new { messages = "Paypal payment was not Approved" });
                    }

                    order.PaymentId = paymentID;
                    _restaurantService.ModifyOrders(order);
                    return Content(HttpStatusCode.OK, new { message = "Paypal payment Approved successfully" });
                };
            }
            catch (WebException webEx)
            {
                var statusCode = ((HttpWebResponse)webEx.Response).StatusCode;
                var body = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                dynamic responseJSON = JsonConvert.DeserializeObject(body);
                string message = responseJSON["message"];
                return Content(HttpStatusCode.BadRequest, new { messages = message });
            }
        }


        #endregion Restaurant Order

        [AllowAnonymous]
        [Route("restaurants/UploadImage")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "upload" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult UploadImage(string imgPath)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            var uploadFile = System.Web.HttpContext.Current.Request.Files[0];
            string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
            if (!Directory.Exists(FolderPathOnServer))
            {
                Directory.CreateDirectory(FolderPathOnServer);
            }
            var Filename = Path.GetFileName(uploadFile.FileName);
            var path = Path.Combine(FolderPathOnServer, Filename);
            uploadFile.SaveAs(path);
            return Content(HttpStatusCode.OK, new { status = true });
        }


        [AllowAnonymous]
        [Route("restaurants/DeleteImage")]
        [HttpDelete]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "delete" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteImage(string imgPath)
        {
            string imagePath = HttpContext.Current.Server.MapPath(imgPath);

            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);
                return Content(HttpStatusCode.OK, new { status = true });
            }
            else
            {
                return Content(HttpStatusCode.NotFound, new { status = false });
            }
        }

        #region GeneratePDF

        /// <summary>
        /// Send order receipt to email
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <remarks>
        /// Returs the Restaurant Order details
        /// </remarks>
        /// <returns>Send order receipt to email</returns>
        [HttpPost]
        [Route("restaurants/orderReceipt")]

        [ResponseType(typeof(object))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Restaurant Order" })]
        public IHttpActionResult SendOrderReceipt(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _restaurantService.GetOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var restaurantOrderDTO = Mapper.Map<RestaurantOrder, RestaurantOrderPdfDTO>(order);
            restaurantOrderDTO.ReceiptNumber = "#" + order.Id;
            Status_Type ordrStatus = (Status_Type)order.OrderStatus;
            restaurantOrderDTO.OrderStatus = ordrStatus.ToString();

            var r = GeneratePDF(restaurantOrderDTO);
            var task = r.Content.ReadAsStreamAsync();
            task.Wait();
            Stream requestStream = task.Result;
            try
            {
                Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/PDF/" + "RestOrderReceipt" + restaurantOrderDTO.ReceiptNumber + ".pdf"));
                requestStream.CopyTo(fileStream);
                fileStream.Close();
                requestStream.Close();

                SendReceipt(customer.Email, restaurantOrderDTO.ReceiptNumber);
                return Content(HttpStatusCode.OK, new { messages = new string[] { "Mail send Successfullly" } });
            }
            catch (IOException)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PDF already generated or send" } });
            }
        }

        private HttpResponseMessage GeneratePDF(RestaurantOrderPdfDTO request)
        {
            try
            {
                var stream = CreatePdf(request);
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(System.Net.Mime.MediaTypeNames.Application.Pdf),
                            ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "PdfFileName.pdf" }
                        }
                    }
                };
                return response;
            }
            catch (ApplicationException ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private Stream CreatePdf(RestaurantOrderPdfDTO request)
        {
            using (var document = new Document(PageSize.A4, 50f, 50f, 100f, 100f))
            {
                var output = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, output);

                var page = new ITextEvents();
                page.Restaurant = request.Restaurant;
                writer.PageEvent = page;

                writer.CloseStream = false;
                // set standard font type and size
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                PdfPTable receiptTable = new PdfPTable(2);
                receiptTable.HorizontalAlignment = 1;

                PdfPCell seatNo = new PdfPCell(new Phrase("Seat No.", _standardFont));
                seatNo.Border = 0;
                PdfPCell seatNum = new PdfPCell(new Phrase(request.Id.ToString(), _standardFont));
                seatNum.Border = 0;

                PdfPCell booking = new PdfPCell(new Phrase("Booking", _standardFont));
                booking.Border = 0;
                PdfPCell bookingRe = new PdfPCell(new Phrase(request.OrderDate.ToString(), _standardFont));
                bookingRe.Border = 0;

                PdfPCell receiptNo = new PdfPCell(new Phrase("Receipt No.", _standardFont));
                receiptNo.Border = 0;
                PdfPCell receiptNum = new PdfPCell(new Phrase(request.ReceiptNumber.ToString(), _standardFont));
                receiptNum.Border = 0;

                receiptTable.AddCell(seatNo);
                receiptTable.AddCell(seatNum);

                receiptTable.AddCell(booking);
                receiptTable.AddCell(bookingRe);

                receiptTable.AddCell(receiptNo);
                receiptTable.AddCell(receiptNum);

                PdfPTable mainTable = new PdfPTable(5);
                mainTable.HorizontalAlignment = 1;

                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;

                // here we will add table headers for report details
                PdfPCell itemName = new PdfPCell(new Phrase("Item Name", _standardFont));
                itemName.Border = 0;
                PdfPCell price = new PdfPCell(new Phrase("Price", _standardFont));
                price.Border = 0;
                PdfPCell qty = new PdfPCell(new Phrase("Qty", _standardFont));
                qty.Border = 0;
                PdfPCell offerPrice = new PdfPCell(new Phrase("OfferPrice", _standardFont));
                offerPrice.Border = 0;
                PdfPCell amount = new PdfPCell(new Phrase("Amount", _standardFont));
                amount.Border = 0;

                mainTable.AddCell(itemName);
                mainTable.AddCell(price);
                mainTable.AddCell(qty);
                mainTable.AddCell(offerPrice);
                mainTable.AddCell(amount);

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                foreach (var item in request.RestaurantOrderItems)
                {
                    mainTable.AddCell(item.ItemName);
                    mainTable.AddCell(item.Price.ToString());
                    mainTable.AddCell(item.Qty.ToString());
                    mainTable.AddCell(item.OfferPrice.ToString());
                    mainTable.AddCell(item.OrderItemTotal.ToString());

                    if (item.RestaurantOrderItemAdditionals.Any())
                    {

                        PdfPTable mainTable1 = new PdfPTable(4);
                        mainTable1.HorizontalAlignment = 1;

                        mainTable1.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell itemName1 = new PdfPCell(new Phrase("Additional", _standardFont));
                        itemName1.Border = 0;
                        PdfPCell price1 = new PdfPCell(new Phrase("Price", _standardFont));
                        price1.Border = 0;
                        PdfPCell qty1 = new PdfPCell(new Phrase("Qty", _standardFont));
                        qty1.Border = 0;
                        PdfPCell amount1 = new PdfPCell(new Phrase("Amount", _standardFont));
                        amount1.Border = 0;

                        mainTable1.AddCell(itemName1);
                        mainTable1.AddCell(price1);
                        mainTable1.AddCell(qty1);
                        mainTable1.AddCell(amount1);

                        foreach (var item1 in item.RestaurantOrderItemAdditionals)
                        {
                            mainTable1.AddCell(item1.MenuAdditionalElement.Additional_Element_Name);
                            mainTable1.AddCell(item1.MenuAdditionalElement.Additional_Cost.ToString());
                            mainTable1.AddCell(item1.Qty.ToString());
                            mainTable1.AddCell(item1.Total.ToString());
                        }

                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable1)) { Colspan = 4, Border = 0 });
                    }
                }
                PdfPCell totalToPay = new PdfPCell(new Phrase("Total to Pay", _standardFont));
                totalToPay.Border = 0;
                PdfPCell totalToPays = new PdfPCell(new Phrase(request.OrderTotal.ToString(), _standardFont));
                totalToPays.Border = 0;

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                mainTable.AddCell(totalToPay);
                mainTable.AddCell("");
                mainTable.AddCell("");
                mainTable.AddCell(" ");
                mainTable.AddCell(totalToPays);

                document.Open();

                Rectangle rect = new Rectangle(577, 825, 18, 15);
                rect.EnableBorderSide(1);
                rect.EnableBorderSide(2);
                rect.EnableBorderSide(4);
                rect.EnableBorderSide(8);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = 1;
                document.Add(rect);

                Paragraph p = new Paragraph();
                document.Add(p);

                document.Add(Chunk.NEWLINE);
                document.Add(receiptTable);

                document.Add(Chunk.NEWLINE);
                document.Add(mainTable);

                document.Close();
                output.Seek(0, SeekOrigin.Begin);
                return output;
            }
        }

        private void SendReceipt(string emailId, string receiptNumber)
        {
            try
            {
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", emailId))
                {
                    mm.Subject = "Order Receipt";
                    mm.IsBodyHtml = false;
                    string filename = HttpContext.Current.Server.MapPath("~/PDF/" + "RestOrderReceipt" + receiptNumber + ".pdf");
                    mm.Attachments.Add(new Attachment(filename));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }

        public class ITextEvents : PdfPageEventHelper
        {

            // This is the contentbyte object of the writer  
            PdfContentByte cb;

            // we will put the final number of pages in a template  
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer  
            BaseFont bf = null;

            // This keeps track of the creation time  
            DateTime PrintTime = DateTime.Now;

            #region Fields
            private string _header;

            public RestaurantInfoPdfDTO _restaurant;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }

            public virtual RestaurantInfoPdfDTO Restaurant
            {
                get { return _restaurant; }
                set { _restaurant = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                }
                catch (System.IO.IOException ioe)
                {
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);
                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                Phrase p1Header = new Phrase(Restaurant.Name, baseFontNormal);

                iTextSharp.text.Image logo = null;
                if (!string.IsNullOrWhiteSpace(Restaurant.LogoImage))
                {
                    logo = iTextSharp.text.Image.GetInstance(Restaurant.LogoImage);

                    logo.ScaleAbsolute(50f, 50f);
                }

                //Create PdfTable object  
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings  
                //Row 1  
                PdfPCell pdfCell1 = null;
                if (logo != null)
                    pdfCell1 = new PdfPCell(logo);
                else
                    pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell(p1Header);
                PdfPCell pdfCell3 = new PdfPCell();

                //Row 2  
                PdfPCell pdfCell4 = new PdfPCell(new Phrase(Restaurant.Description, baseFontNormal));
                PdfPCell pdfCell5 = new PdfPCell(new Phrase(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig)));

                //Row 3   
                PdfPCell pdfCell6 = new PdfPCell();
                PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                //set the alignment of all three cells and set border to 0  
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell1.Rowspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                pdfCell4.Border = 0;
                pdfCell5.Border = 0;
                pdfCell6.Border = 0;
                pdfCell7.Border = 0;

                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                pdfTab.AddCell(pdfCell4);
                pdfTab.AddCell(pdfCell5);
                pdfTab.AddCell(pdfCell6);
                pdfTab.AddCell(pdfCell7);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable  
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write  
                //Third and fourth param is x and y position to start writing  
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
                //set pdfContent value  

                //Move the pointer and draw line to separate header section from rest of page  
                cb.MoveTo(40, document.PageSize.Height - 100);
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                cb.Stroke();

                //Create PdfooTable object for Footer
                PdfPTable pdfooTab = new PdfPTable(1);

                //We will have to create separate cells to include image logo and 2 separate strings  
                PdfPCell pdfooCell1 = new PdfPCell(new Phrase(Restaurant.Name, baseFontNormal));
                PdfPCell pdfooCell2 = new PdfPCell(new Phrase(Restaurant.Street == null ? Restaurant.Street2 : Restaurant.Street, baseFontNormal));
                PdfPCell pdfooCell3 = new PdfPCell(new Phrase(Restaurant.City, baseFontNormal));
                PdfPCell pdfooCell4 = new PdfPCell(new Phrase(Restaurant.Region, baseFontNormal));
                PdfPCell pdfooCell5 = new PdfPCell(new Phrase(Restaurant.Telephone != null ? Restaurant.Telephone : "", baseFontNormal));

                pdfooCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell5.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfooCell1.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell4.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell5.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfooCell1.Border = 0;
                pdfooCell2.Border = 0;
                pdfooCell3.Border = 0;
                pdfooCell4.Border = 0;
                pdfooCell5.Border = 0;

                pdfooTab.AddCell(pdfooCell1);
                pdfooTab.AddCell(pdfooCell2);
                pdfooTab.AddCell(pdfooCell3);
                pdfooTab.AddCell(pdfooCell4);
                pdfooTab.AddCell(pdfooCell5);

                pdfooTab.TotalWidth = document.PageSize.Width - 80f;
                pdfooTab.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable  
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write  
                //Third and fourth param is x and y position to start writing  
                pdfooTab.WriteSelectedRows(0, -1, 40, document.PageSize.GetBottom(100), writer.DirectContent);

                //Move the pointer and draw line to separate footer section from rest of page  
                cb.MoveTo(40, document.PageSize.GetBottom(100));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(100));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }
        }

        #endregion GeneratePDF

    }
}
