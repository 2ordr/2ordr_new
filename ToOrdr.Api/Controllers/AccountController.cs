﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using ToOrdr.Api.Models;
using ToOrdr.Api.Providers;
using ToOrdr.Api.Results;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Core.Entities;
using System.Web.Http.Description;
using ToOrdr.Api.DTO;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Linq;
using System.Net.Mail;
using ToOrdr.Api.App_Start;
using ToOrdr.Localization;
using Newtonsoft.Json.Linq;
using ToOrdr.Core.Util;
using System.Configuration;
using Newtonsoft.Json;
using ToOrdr.Core.Exceptions;

namespace ToOrdr.Api.Controllers
{
    [Authorize]


    public class AccountController : BaseController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private readonly ICustomerService _userService = System.Web.Mvc.DependencyResolverExtensions.GetService<ICustomerService>(System.Web.Mvc.DependencyResolver.Current);

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        //GET api/Account/EmailPhoneVerification

        /// <summary>
        /// Check if the email or telephone number already exist
        /// </summary>
        /// <remarks>
        /// Check if the email or telephone number already exist.
        /// </remarks>
        /// <param name="emailId">Email Id</param>
        /// <param name="telephone">Telephone</param>
        /// <returns>Success</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("Account/EmailPhoneVerification")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validations Error")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public IHttpActionResult EmailPhoneVerification(string emailId = "", string telephone = "")
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(emailId))
                {
                    var alreadyExist = _userService.GetCustomerByEmailId(emailId);
                    if (alreadyExist != null)
                        return Json(new { messages = new string[] { Resources.Email_AlreadyExt } });
                }
                if (!string.IsNullOrWhiteSpace(telephone))
                {
                    var alreadyExist = _userService.GetCustomerByTelephone(telephone);
                    if (alreadyExist != null)
                        return Json(new { messages = new string[] { Resources.Telephone_AlreadyExist } });
                }

                return Json(new { messages = new string[] { "Success" } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }


        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Account/Register")]

        [ResponseType(typeof(List<CreateCustomerDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validations Error")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            var user = new Customer()
            {
                Email = model.Email,
                Password = Hashing.HashPassword(model.Password),
                FirstName = model.FirstName,
                LastName = model.LastName,
                CreationDate = DateTime.Now,
                BirthDate = DateTime.MinValue,
                Telephone = model.Telephone,
                CountryId = model.CountryId,
                PhoneCode = model.PhoneCode,
                CurrencyId = model.CurrencyId
            };

            string activationCode = Guid.NewGuid().ToString();
            user.ActivationCode = activationCode;

            try
            {
                if (user.CountryId > 0)
                {
                    var country = _userService.GetCountryById(Convert.ToInt32(user.CountryId));
                    if (country == null)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Invalid Country Id" } });
                }

                if (user.CurrencyId > 0)
                {
                    var currency = _userService.GetCurrencyById(user.CurrencyId);
                    if (currency == null)
                        return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Invalid Currency Id" } });
                }

                user = _userService.CreateCustomer(user);
                var userDTO = AutoMapper.Mapper.Map<CreateCustomerDTO>(user);

                SendActivationEmail(user);
                SendSMS(user.PhoneCode, user.Telephone);

                var msg = string.Format(Resources.Msg_Registration_Success, user.Email, user.Telephone);
                return Created(this.Url.Link("GetCustomerDetails", null), new { messages = new string[] { msg } }); //new { Controller = "Customers", Action = "Get", id = user.Id }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        // POST api/Account/ResendOTP
        [AllowAnonymous]
        [Route("Account/ResendOTP")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validations Error")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public async Task<IHttpActionResult> ResendOTP(ResendOTPBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }
            try
            {
                SendSMS(model.PhoneCode, model.Telephone);
                return Json(new { messages = new string[] { "Verification code will be resend to your phone " + model.PhoneCode + " " + model.Telephone } }); //new { Controller = "Customers", Action = "Get", id = user.Id }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        // POST api/Account/Verification
        [AllowAnonymous]
        [Route("Account/Verification")]

        [ResponseType(typeof(List<CreateCustomerDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validations Error")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public async Task<IHttpActionResult> Verification(OTPVerificationBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Authorization", "Bearer WP0hfCh5y9muj0yES3mXSUUgltpsCQGcpwULtiA6");
                    client.BaseAddress = @"https://api.smsapi.com";

                    string phoneNumber = model.PhoneCode.Replace("+", "") + model.Telephone;
                    var response = client.UploadValues(string.Format("/mfa/codes/verifications"), "POST", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"code",model.OTP},
                        {"phone_number",phoneNumber}
                    });
                    string responseString = System.Text.Encoding.UTF8.GetString(response);
                    dynamic responseJSON = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                }
                var user = _userService.GetCustomerByTelephone(model.Telephone);
                user.ActiveStatus = true;
                _userService.ModifyCustomer(user);

                return Json(new { messages = new string[] { "Your account has been activated successfully" } }); //new { Controller = "Customers", Action = "Get", id = user.Id }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }


        }


        private IHttpActionResult ReturnValidationErrors()
        {
            List<string> messages = new List<string>();

            foreach (var error in ModelState.Values)
            {
                var x = error.Errors.Select(e => e.ErrorMessage);
                messages.AddRange(x);
            }

            return Content(HttpStatusCode.BadRequest, new { messages = messages });
        }



        // POST api/Account/TokenExternal
        [AllowAnonymous]
        [Route("Account/TokenExternal")]

        [ResponseType(typeof(Object))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validations Error")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public async Task<IHttpActionResult> ExternalToken(ExternalTokenModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            if (string.IsNullOrWhiteSpace(model.Email) && string.IsNullOrWhiteSpace(model.ProviderKey))
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Identity_ProviderKey_Required } });
            }
            try
            {
                switch (model.Provider)
                {
                    case "Google":
                        using (HttpClient client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + model.ProviderKey);
                            client.BaseAddress = new Uri("https://www.googleapis.com");

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                            var response = client.GetAsync("/oauth2/v1/tokeninfo").Result;
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                            model.ProviderKey = responseJSON.user_id;
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                if (!_userService.ValidateExternalLogin(model.ProviderKey, model.Provider))  //customer not found, add customer
                                {
                                    var newCustomer = new Customer
                                    {
                                        FirstName = model.FirstName,
                                        LastName = model.LastName,
                                        Password = string.Empty,
                                        ProviderKey = model.ProviderKey,
                                        ProviderName = model.Provider,
                                        Email = string.IsNullOrWhiteSpace(model.Email) ? "email_not_available@2ordr.com" : model.Email,
                                        CurrencyId = 1,
                                        ActiveStatus = true
                                    };
                                    _userService.CreateExternalCustomer(newCustomer);
                                }
                            }
                            else
                            {
                                if (response.StatusCode == HttpStatusCode.BadRequest)
                                {
                                    var ErrMsg = GenerateActualErroMsg(responseJSON);
                                    throw new BadRequestException(ErrMsg);
                                }
                                else
                                {
                                    var ErrMsg = GenerateActualErroMsg(responseJSON);
                                    throw new ForbiddenException(ErrMsg);
                                }
                            }
                        }
                        break;

                    case "Facebook":
                        using (HttpClient client1 = new HttpClient())
                        {
                            client1.BaseAddress = new Uri("https://graph.facebook.com");
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                            var response1 = client1.GetAsync(string.Format("/me?fields={0}&access_token={1}", "id,name,email", model.ProviderKey)).Result;
                            string responseString1 = response1.Content.ReadAsStringAsync().Result;
                            dynamic responseJSON1 = JsonConvert.DeserializeObject(responseString1);

                            model.ProviderKey = responseJSON1.id;
                            model.Email = responseJSON1.email;
                            if (response1.StatusCode == HttpStatusCode.OK)
                            {
                                if (!_userService.ValidateExternalLogin(model.ProviderKey, model.Provider))  //customer not found, add customer
                                {
                                    var newCustomer = new Customer
                                    {
                                        FirstName = model.FirstName,
                                        LastName = model.LastName,
                                        Password = string.Empty,
                                        ProviderKey = model.ProviderKey,
                                        ProviderName = model.Provider,
                                        Email = string.IsNullOrWhiteSpace(model.Email) ? responseJSON1.email : model.Email,
                                        CurrencyId = 1,
                                        ActiveStatus = true
                                    };
                                    _userService.CreateExternalCustomer(newCustomer);
                                }
                            }
                            else
                            {
                                if (response1.StatusCode == HttpStatusCode.BadRequest)
                                {
                                    var ErrMsg = GenerateActualErroMsg(responseJSON1);
                                    throw new BadRequestException(ErrMsg);
                                }
                                else
                                {
                                    var ErrMsg = GenerateActualErroMsg(responseJSON1);
                                    throw new ForbiddenException(ErrMsg);
                                }
                            }
                        }
                        break;
                    default:
                        throw new NotFoundException("Please Provide Valid Provide Name");
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            return CreateToken(model);
        }


        private IHttpActionResult CreateToken(ExternalTokenModel model)
        {
            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);

            if (!string.IsNullOrWhiteSpace(model.Email)) oAuthIdentity.AddClaim(new Claim(ClaimTypes.Email, model.Email));
            if (!string.IsNullOrWhiteSpace(model.ProviderKey)) oAuthIdentity.AddClaim(new Claim("ProviderKey", model.ProviderKey));
            if (!string.IsNullOrWhiteSpace(model.Provider)) oAuthIdentity.AddClaim(new Claim("Provider", model.Provider));


            oAuthIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, model.ProviderKey == null ? "Email" : "Provider_Key"));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, model.ProviderKey == null ? model.Email : model.ProviderKey));

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());

            DateTime currentUtc = DateTime.UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromDays(14));

            string accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
            Request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);



            // Create the response building a JSON object that mimics exactly the one issued by the default /Token endpoint
            JObject token = new JObject(
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
                new JProperty("expires_in", TimeSpan.FromDays(14).TotalSeconds.ToString()),
                new JProperty(".issued", ticket.Properties.IssuedUtc.Value.ToUniversalTime()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.Value.ToUniversalTime())

            );

            return Ok(token);
        }

        public static string GenerateActualErroMsg(dynamic responseJSON)
        {
            var actualErrMsg = "";
            if (responseJSON.errors != null)
            {
                JObject obj = JObject.Parse(responseJSON.errors.ToString());
                foreach (var pair in obj)
                {
                    actualErrMsg = ": " + pair.Key + " " + pair.Value.FirstOrDefault().ToString();
                }
            }
            var fullActualErrMsg = responseJSON.message.ToString() + actualErrMsg;
            return fullActualErrMsg;
        }


        private void SendActivationEmail(Customer customer)
        {
            try
            {
                //todo: make it asynchronnious call and template based

                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", customer.Email.ToString()))
                {

                    mm.Subject = "Account Activation";
                    string body = "Hello " + customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to activate your account";
                    body += "<br /><a href = '" + Url.Link("ActivationUrl", new { ActivationCode = customer.ActivationCode, referenceId = customer.Id }) + "'>Click here to activate your account.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }


        }


        private void SendSMS(string PhoneCode, string Telephone)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    var token = ConfigurationManager.AppSettings["SMSAPIToken"];
                    var url = ConfigurationManager.AppSettings["SMSAPIUrl"];
                    client.Headers.Add("Authorization", token);
                    client.BaseAddress = url;

                    string phoneNumber = PhoneCode.Replace("+", "") + Telephone;
                    var response = client.UploadValues(string.Format("/mfa/codes"), "POST", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"phone_number",phoneNumber}
                    });
                    string responseString = System.Text.Encoding.UTF8.GetString(response);
                    dynamic responseJSON = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }



        [AllowAnonymous]
        [Route("Account/Activate", Name = "ActivationUrl")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public IHttpActionResult Activate(string ActivationCode, string referenceId)
        {
            var customer = _userService.GetCustomerById(Convert.ToInt32(referenceId));

            if (customer == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_DETAILS" } });


            if (customer.ActivationCode == ActivationCode)
            {
                customer.ActiveStatus = true;
                _userService.ModifyCustomer(customer);
                return Content(HttpStatusCode.OK, "Your email verified successfully");
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "INVALID_ACTIVATION_CODE" } });
            }

        }


        /// <param name="emailId">Email Id</param>
        [AllowAnonymous]
        [Route("Account/ActivateAccount")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Invalid email Address")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public IHttpActionResult ActivateAccount(string emailId)
        {
            try
            {
                var user = _userService.GetCustomerByEmailId(emailId);

                if (user == null)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Email_Not_Found } });

                if (!user.ActiveStatus.HasValue)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Valid_EmailVerification } });

                if (user.ActiveStatus.Value == true)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Already_ActiveAccount } });

                string activationCode = Guid.NewGuid().ToString();
                user.ActivationCode = activationCode;

                user = _userService.ModifyCustomer(user);
                SendActivationEmail(user);
                SendSMS(user.PhoneCode, user.Telephone);

                var msg = string.Format(Resources.Account_ReActive, user.Email, user.Telephone);
                return Created(this.Url.Link("GetCustomerDetails", null), new { messages = new string[] { msg } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }



        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        // POST api/Account/Logout
        [Route("Account/Logout")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [HttpPost]
        [Route("Account/ChangePassword")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Account" })]


        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Not_Authorized } });

            if (!Hashing.ValidatePassword(model.OldPassword, authenticatedCustomer.Password))
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Valid_OldPassword } });

            try
            {
                authenticatedCustomer.Password = Hashing.HashPassword(model.NewPassword);
                _userService.UpdatePassword(authenticatedCustomer);  //todo: send informative email to user
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new { messages = new string[] { "INTERNAL_SERVER_ERROR" } });
            }

            return Content(HttpStatusCode.OK, new { messages = new string[] { Resources.Change_Password_Success } });
        }

        // POST api/Account/ChangeEmail
        [HttpPost]
        [Route("Account/ChangeEmail")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Account" })]
        public async Task<IHttpActionResult> ChangeEmail(ChangeEmailBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Not_Authorized } });

            if (authenticatedCustomer.Email != model.OldEmail)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Valid_OldEmail } });

            try
            {
                authenticatedCustomer.Email = model.NewEmail;
                _userService.UpdateEmail(authenticatedCustomer);  //todo: send informative email to user
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new { messages = new string[] { ex.Message } });
            }

            return Content(HttpStatusCode.OK, new { messages = new string[] { Resources.EmailChange_Success } });
        }

        /// <summary>
        /// Delete User account
        /// </summary>
        /// <remarks>
        /// Delete User account. Authorization Required.
        /// </remarks>
        /// <param name="password">Password</param>
        /// <returns>Success</returns>
        [HttpDelete]
        [Route("Account/DeleteAccount")]
        //[ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Account" })]
        public IHttpActionResult DeleteAccount(string password)
        {
            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Unauthorized, new { messages = new string[] { Resources.Not_Authorized } });

            if (!Hashing.ValidatePassword(password, authenticatedCustomer.Password))
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Password provided is Incorrect" } });
            try
            {
                authenticatedCustomer.ActiveStatus = false;
                _userService.DeleteAccount(authenticatedCustomer);

                Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
                return Content(HttpStatusCode.OK, new { messages = new string[] { Resources.AccountDeleted } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new { messages = new string[] { ex.Message } });
            }

        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        [AllowAnonymous]
        [Route("Account/ForgotPassword")]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Invalid Email")]
        [SwaggerOperation(Tags = new[] { "Account" })]

        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordModel model)
        {

            if (!ModelState.IsValid)
            {
                return ReturnValidationErrors();
            }

            var customer = _userService.GetAllCustomers(c => c.Email == model.Email && c.ActiveStatus == true).FirstOrDefault();
            if (customer == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Email_Not_Found } });


            string resetCode = Guid.NewGuid().ToString();
            customer.ResetPasswordCode = resetCode;
            try
            {
                _userService.UpdatePassword(customer);
                SendResetEmail(customer);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new { messages = new string[] { Resources.Internal_Server_error } });
            }

            return Content(HttpStatusCode.OK, new { messages = new string[] { Resources.Forgot_Password_Email_Sent } });

        }

        private void SendResetEmail(Customer Customer)
        {
            string resetUrl = ToOrderConfigs.BackOfficeUrl + "/Account/ResetPassword";

            try
            {
                //todo: make it asynchronnious call and template based
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", Customer.Email.ToString()))
                {

                    mm.Subject = "Reset Password";
                    string body = "Hello " + Customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to reset your password";
                    body += "<br /><a href = '" + resetUrl + "?ResetPasswordCode=" + Customer.ResetPasswordCode + "&Id=" + Customer.Id.ToString() + "'>Click here to reset your password.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }






        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }






        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
