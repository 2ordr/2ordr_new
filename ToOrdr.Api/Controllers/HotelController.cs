﻿using AutoMapper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using ToOrdr.Api.App_Start;
using ToOrdr.Api.DTO;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Exceptions;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;

namespace ToOrdr.Api.Controllers
{

#if !DEBUG
    [Authorize]
#endif

    public class HotelController : BaseController
    {
        IHotelService _hotelService;
        ICustomerService _customerService;
        IPaymentService _paymentService;

        public HotelController(IHotelService hotelService, ICustomerService customerService, IPaymentService paymentService)
        {
            _hotelService = hotelService;
            _customerService = customerService;
            _paymentService = paymentService;
        }

        #region Hotels

        /// <summary>
        /// Search hotels based on different criterias 
        /// </summary>
        /// <remarks>
        /// Search hotels based on different criterias, if found than returns 200 status with "success" meassage and if not found then return 404 status with "no content" message
        /// </remarks>
        /// <param name="lat">Enter latitude of place</param>
        /// <param name="lng">Enter longitude of place</param>
        /// <param name="radius_min">Enter min radius in meters</param>
        /// <param name="radius_max">Enter max radius in meters</param>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="Ids">Hotel services Ids</param>
        /// <param name="starRating_min">Minimum star rating</param>
        /// <param name="starRating_max">Maximum star rating</param>
        /// <param name="price_min">Minimum Night price</param>
        /// <param name="price_max">Maximum Night price</param>
        /// <param name="userRating_min">User min rating</param>
        /// <param name="userRating_max">User max rating</param>
        /// <param name="page">Enter page number</param>
        /// <param name="limit">Enter the page size</param>
        /// <param name="sort">Select sort by condition</param>
        /// <param name="order">Enter how to order "asc" or "desc"</param>
        /// <param name="selection">Selection</param>
        /// <param name="checkInDate">Check in date</param>
        /// <param name="checkOutDate">Check out date</param>
        /// <param name="starRating">Number of star rating</param>
        /// <param name="priceRange">Price range</param>
        /// <param name="hotelGrpBy">Choose group by</param>
        /// <returns>Returns list of hotels </returns>
        [HttpGet]
        [Route("hotels/search", Name = "SearchHotels")]
        [ResponseType(typeof(List<HotelDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Hotels not found")]
        [SwaggerResponse(HttpStatusCode.NoContent, Description = "No content")]
        [SwaggerOperation(Tags = new[] { "Hotels" })]
        public IHttpActionResult SearchHotels(double? lat = null, double? lng = null, double? radius_min = 0, double? radius_max = 20000, string searchText = "", [FromUri]  int[] Ids = null, double? starRating_min = null, double? starRating_max = null, double? price_min = null, double? price_max = null, double? userRating_min = null, double? userRating_max = null, int page = 1, int limit = 20, Selection selection = Selection.All, string checkInDate = "", string checkOutDate = "", int? starRating = null, int? priceRange = null, HotelGroupBy hotelGrpBy = HotelGroupBy.None)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            int Id = user != null ? user.Id : 0;

            int total;
            var hotels = _hotelService.Search(out total, searchText, checkInDate, checkOutDate, lat, lng, radius_min, radius_max, Ids, starRating_min, starRating_max, price_min, price_max, userRating_min, userRating_max, page, limit, SearchTextTypes.Everywhere, Id, selection, starRating, priceRange);
            var favHotels = _customerService.GetFavoriteHotels(user.Id);
            if (hotels.Any())
            {
                var hotelsDTO = Mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(hotels);
                hotelsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
                if (favHotels != null && favHotels.Count() > 0)
                {
                    foreach (var res in hotelsDTO)
                    {
                        if (favHotels.Any(r => r.Id == res.Id))
                            res.IsFavourite = true;
                    }
                }
                switch (hotelGrpBy)
                {
                    case HotelGroupBy.None:
                        break;
                    case HotelGroupBy.StarRating:
                        var hotelStarRatingGroupsDTO = hotelsDTO.GroupBy(h => h.StarRating).OrderByDescending(o => o.Key).Select(gr => new HotelGroupByStarRatingDTO { StarRating = gr.Key, Hotel = gr.Take(5).ToList() });
                        return Content(HttpStatusCode.OK, new { Message = "success", Result = hotelStarRatingGroupsDTO });
                    case HotelGroupBy.PriceRange:
                        var hotelPriceRangeGroupsDTO = hotelsDTO.GroupBy(h => h.PriceRange).OrderByDescending(o => o.Key).Select(gr => new HotelGroupByPriceRangeDTO { PriceRange = gr.Key, Hotel = gr.Take(5).ToList() });
                        return Content(HttpStatusCode.OK, new { Message = "success", Result = hotelPriceRangeGroupsDTO });
                    case HotelGroupBy.Radius:
                        if (lat.HasValue && lng.HasValue)
                        {
                            int[] ranges = { 5000, 10000, 20000, 30000, 100000 };
                            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", lng.Value, lat.Value);
                            var sourcePoint = DbGeography.PointFromText(text, 4326);
                            var hotelRadiusGroups = hotels.GroupBy(loc => ranges.FirstOrDefault(r => r > loc.Location.Distance(sourcePoint))).OrderBy(o => o.Key).Select(gr => new HotelsGroupByRadius { Distance = gr.Key, Hotel = gr.Take(5).ToList() });
                            var hotelRadiusGroupsDTO = Mapper.Map<IEnumerable<HotelsGroupByRadius>, List<HotelGroupByRadiusDTO>>(hotelRadiusGroups);
                            foreach (var defaultRadiusGrp in hotelRadiusGroupsDTO)
                            {
                                if (defaultRadiusGrp.Distance == 0)
                                {
                                    hotelRadiusGroupsDTO.Remove(defaultRadiusGrp);
                                    break;
                                }
                                defaultRadiusGrp.Hotel.Select(r => { r.IsFavourite = false; return r; }).ToList();
                                if (favHotels != null && favHotels.Count() > 0)
                                {
                                    foreach (var res in defaultRadiusGrp.Hotel)
                                    {
                                        if (favHotels.Any(r => r.Id == res.Id))
                                            res.IsFavourite = true;
                                    }
                                }
                            }
                            return Content(HttpStatusCode.OK, new { Message = "success", Result = hotelRadiusGroupsDTO });
                        }
                        break;
                }

                var mod = total % limit;
                var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { Message = "success", total_items = total, total_pages = totalPageCount, current_page = page, result = hotelsDTO });
            }

            // search didn't return content, so send suggestions

            var suggestions = _hotelService.Search(out total, null, null, null, null, null, radius_min, radius_max, null, null, null, null, null, page, limit);
            var suggestionsDTO = Mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(suggestions);
            suggestionsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
            if (favHotels != null && favHotels.Count() > 0)
            {
                foreach (var res in suggestionsDTO)
                {
                    if (favHotels.Any(r => r.Id == res.Id))
                        res.IsFavourite = true;
                }
            }
            var suggetionMod = total % limit;
            var suggTotalPageCount = (total / limit) + (suggetionMod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { Message = "no content", total_items = total, total_pages = suggTotalPageCount, current_page = page, result = (string)null, Suggestions = suggestionsDTO });
        }


        /// <summary>
        /// Search hotels based on hotel name
        /// </summary>
        /// <remarks>
        /// Search hotels based hotels name
        /// </remarks>
        /// <param name="Name">Enter hotel name</param>
        /// <param name="page">Enter the number</param>
        /// <param name="limit">Enter page size</param>
        /// <returns>Return list of hotels</returns>
        [HttpGet]
        [Route("hotels/searchbyname", Name = "GetHotelByName")]
        [ResponseType(typeof(HotelDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Hotel Found")]
        [SwaggerOperation(Tags = new[] { "Hotels" })]

        public IHttpActionResult GetHotelByName(string Name, int? page = 1, int? limit = 20)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            int total;
            var hotels = _hotelService.Search(out total, Name, "", "", null, null, null, null, null, null, null, null, null, null, null, Convert.ToInt32(page), Convert.ToInt32(limit), SearchTextTypes.HotelName);
            if (hotels.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            var hotelsDTO = Mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(hotels);
            var favHotels = _customerService.GetFavoriteHotels(user.Id);
            hotelsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
            if (favHotels != null && favHotels.Count() > 0)
            {
                foreach (var res in hotelsDTO)
                {
                    if (favHotels.Any(r => r.Id == res.Id))
                        res.IsFavourite = true;
                }
            }
            var Mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (Mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = hotelsDTO });
        }

        /// <summary>
        /// Returns all hotels
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotels based on latitude and longitude. Authorization required to access this method.
        /// </remarks>
        /// <param name="lat">Enter latitude of place</param>
        /// <param name="lng">Enter longitude of place</param>
        /// <param name="range">Enter radius in meters</param>
        /// <returns>List of hotels</returns>
        [HttpGet]
        [Route("hotel/{lat=lat}/{lng=lng}/{range=range}")]

        [ResponseType(typeof(List<HotelDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Hotels Found")]
        [SwaggerOperation(Tags = new[] { "Hotels" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetAllHotels(double lat, double lng, double range, int? page = null)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            string orderBy = "asce";
            var hotels = _hotelService.GetAllHotels(lat, lng, range, orderBy, pageNumber, pageSize, out total);

            if (hotels == null || hotels.Count() < 1)
                return NotFound();

            var hotelsDTO = Mapper.Map<IEnumerable<Hotel>, List<GetHotelDTO>>(hotels);

            return Content(HttpStatusCode.OK, hotelsDTO);
        }


        /// <summary>
        /// Returns hotel by Id
        /// </summary>
        /// <remarks>
        /// Get detailed hotel information using youORDR hotel ID. Authorization Required.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Hotel Details</returns>
        [HttpGet]
        [Route("hotels/{id}", Name = "GetHotelById")]

        [ResponseType(typeof(HotelDetailWithExcursionPriceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Hotel Found")]
        [SwaggerOperation(Tags = new[] { "Hotels" })]

        public IHttpActionResult GetHotelById(int id)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            var hotel = _hotelService.GetHotelById(id);

            if (hotel == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            var hotelDTO = Mapper.Map<HotelDetailWithExcursionPriceDTO>(hotel);
            var favHotels = _customerService.GetFavoriteHotels(user.Id);
            hotelDTO.IsFavourite = false;

            if (favHotels != null && favHotels.Count() > 0)
            {
                if (favHotels.Any(r => r.Id == hotelDTO.Id))
                    hotelDTO.IsFavourite = true;
            }
            var excPriceMinAndMax = _hotelService.GetExcursionPriceMinAndMaxOfHotel(id);
            if (excPriceMinAndMax.Excursion_Price_Min == null || excPriceMinAndMax.Excursion_Price_Max == null)
            {
                hotelDTO.Excursion_Price_Min = 0;
                hotelDTO.Excursion_Price_Max = 0;
            }
            else
            {
                hotelDTO.Excursion_Price_Min = Convert.ToDouble(excPriceMinAndMax.Excursion_Price_Min);
                hotelDTO.Excursion_Price_Max = Convert.ToDouble(excPriceMinAndMax.Excursion_Price_Max);
            }

            return Content(HttpStatusCode.OK, hotelDTO);
        }


        /// <summary>
        /// Creates new Hotel
        /// </summary>
        /// <remarks>
        /// Adds new hotel in the backend
        /// </remarks>
        /// <param name="hotelDTO">Hotel Details</param>
        /// <returns>Returns newly created hotel details</returns>
        //[HttpPost]
        //[Route("hotels")]

        //[ResponseType(typeof(HotelDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerOperation(Tags = new[] { "Hotels" })]
        //public IHttpActionResult CreateHotel(CreateHotelDTO hotelDTO)
        //{
        //    if (hotelDTO != null)
        //        ModelState["hotelDTO.Location.Geography"].Errors.Clear();

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);
        //    var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
        //                            "POINT({0} {1})", hotelDTO.Longitude, hotelDTO.Latitude);
        //    hotelDTO.Location = DbGeography.PointFromText(text, 4326);
        //    var entity = Mapper.Map<Hotel_Details>(hotelDTO);

        //    entity = _hotelService.CreateHotel(entity);

        //    //return Created(Url.Link("GetHotelById", new { id = entity.Hotel_ID }), Mapper.Map<HotelDTO>(entity));
        //    return null;

        //}


        /// <summary>
        /// Modifies existing hotel details
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the hotel.
        /// </remarks>
        /// <param name="hotelDTO">Hotel details</param>
        /// <returns>returns modifed hotel details</returns>
        //[Route("hotels")]
        //[HttpPut]

        //[ResponseType(typeof(HotelDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerOperation(Tags = new[] { "Hotels" })]

        //public IHttpActionResult ModifyHotel(ModifyHotelDTO hotelDTO)
        //{
        //    if (hotelDTO != null)
        //        ModelState["hotelDTO.Location.Geography"].Errors.Clear();

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);
        //    var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
        //                           "POINT({0} {1})", hotelDTO.Longitude, hotelDTO.Latitude);
        //    hotelDTO.Location = DbGeography.PointFromText(text, 4326);

        //    var entity = Mapper.Map<Hotel_Details>(hotelDTO);

        //    entity = _hotelService.ModifyHotel(entity);

        //    return Ok(Mapper.Map<HotelDTO>(entity));

        //}

        /// <summary>
        /// Delete Hotel by id
        /// </summary>
        /// <remarks>
        /// Delete hotel details based on hotel id.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Success</returns>
        [Route("hotels/{id}")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotels" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteHotel(int id)
        {

            _hotelService.DeleteHotel(id);

            return Ok();

        }

        #endregion

        #region Hotel Virtual Restaurants
        /// <summary>
        /// Returns hotel virtual restaurants 
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotel virtual restaurants 
        /// </remarks>
        /// <returns>List of hotel virtual restaurants </returns>
        [HttpGet]
        [Route("hotel/{id}/virtual_restaurants")]

        [ResponseType(typeof(List<HotelVirtualRestaurantsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Hotel Virtual Restaurant Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Virtual Restaurants" })]

        public IHttpActionResult GetHotelVirtualRestaurants(int id)
        {
            var hotel = _hotelService.GetHotelById(id);
            if (hotel == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            if (hotel.HotelRestaurants.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HotelVirtualRest_NotFound } });

            var hotelVirtualRestsDTO = Mapper.Map<Hotel, HotelVirtualRestaurantsDTO>(hotel);
            return Content(HttpStatusCode.OK, hotelVirtualRestsDTO);

        }
        #endregion Hotel Virtual Restaurants

        #region Hotel Near By Places
        /// <summary>
        /// Returns list of hotel near by places
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of all hotel near by places using Hotel Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of hotel near by places</returns>
        [Route("hotels/{id}/nearByPlaces", Name = "GetHotelNearByPlaces")]
        [HttpGet]

        [ResponseType(typeof(HotelNearByPlacesDTO))]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Hotel Near By Places Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Near By Places" })]
        public IHttpActionResult GetHotelNearByPlaces(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelNearByPlaces = _hotelService.GetAllHotelNearByPlaces(id);
            if (hotelNearByPlaces == null || hotelNearByPlaces.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HotelNrByPlaces_NotFound } });

            var hotelNrByPlacesDTO = Mapper.Map<IEnumerable<HotelsNearByPlace>, List<HotelNearByPlacesDTO>>(hotelNearByPlaces);
            return Content(HttpStatusCode.OK, hotelNrByPlacesDTO);
        }
        #endregion Hotel Near By Places

        #region hotel reviews

        /// <summary>
        /// Returns list of customer's hotel reviews
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of all customer's hotel reviews using Hotel Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of reviews</returns>
        [Route("hotels/{id}/reviews", Name = "GetHotelReviews")]
        [HttpGet]

        [ResponseType(typeof(HotelReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Reviews Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Reviews" })]
        public IHttpActionResult GetHotelReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var reviews = _hotelService.GetHotelReviews(id, page.Value, limit.Value, out total);

            if (reviews == null || reviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewsDTO = Mapper.Map<IEnumerable<HotelReview>, List<HotelReviewsDTO>>(reviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = reviewsDTO });
        }

        /// <summary>
        /// Returns Review by Id
        /// </summary>
        /// <remarks>
        /// Get a detailed of customer hotel reviews using Review Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Hotel Review</returns>
        [HttpGet]
        [Route("hotels/HotelReview/{id}", Name = "GetHotelReviewById")]

        [ResponseType(typeof(HotelReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Review Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Reviews" })]

        public IHttpActionResult GetHotelReviewById(int id)
        {
            var review = _hotelService.GetReviewById(id);

            if (review == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewDTO = Mapper.Map<HotelReviewsDTO>(review);

            return Content(HttpStatusCode.OK, reviewDTO);
        }

        /// <summary>
        /// Creates new Hotel reviews
        /// </summary>
        /// <remarks>
        /// Adds new hotel review in the backend
        /// </remarks>
        /// <param name="hotelReviewsDTO">Hotel Review Details</param>
        /// <returns>Returns newly created hotel review details</returns>
        [HttpPost]
        [Route("hotels/review", Name = "CreateHotelReview")]

        [ResponseType(typeof(HotelReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Reviews" })]
        public IHttpActionResult CreateHotelReview(CreateHotelReviewsDTO hotelReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var entity = Mapper.Map<HotelReview>(hotelReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            entity = _hotelService.CreateHotelReviews(entity);

            return Created(Url.Link("GetHotelReviewById", new { id = entity.Id }), Mapper.Map<HotelReviewsDTO>(entity));
        }

        /// <summary>
        /// Deletes hotel reviews
        /// </summary>
        /// <remarks>
        /// Delete hotel review details based on review id.
        /// </remarks>
        /// <param name="Id">Review Id</param>
        /// <returns></returns>
        [Route("hotels/review", Name = "DeleteHotelReviews")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Reviews" })]
        public IHttpActionResult DeleteHotelReviews(int Id)
        {
            var review = _hotelService.GetReviewById(Id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }
            _hotelService.DeleteHotelReview(Id);
            return Ok();
        }


        #endregion

        #region hotel services
        /// <summary>
        /// Returns all available services
        /// </summary>
        /// <remarks>
        /// Get a detailed list of available services
        /// </remarks>
        /// <returns>List of services</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("hotels/services")]

        [ResponseType(typeof(List<ServicesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Services not Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Services" })]

        public IHttpActionResult GetServices()
        {
            var services = _hotelService.GetAllServices();
            if (services == null || services.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Service_Not_Found } });

            var servicesDTO = Mapper.Map<IEnumerable<Services>, List<ServicesDTO>>(services);
            return Content(HttpStatusCode.OK, servicesDTO);

        }

        /// <summary>
        /// Return hotel services by HotelId
        /// </summary>
        /// <remarks>
        /// Get hotel services using youORDR hotel ID. Authorization Required.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns> List Hotel Services</returns>
        [HttpGet]
        [Route("hotels/{id}/services", Name = "GetHotelServices")]

        [ResponseType(typeof(List<HotelServicesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Hotel Services not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Services" })]

        public IHttpActionResult GetHotelServices(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelServices = _hotelService.GetHotelServices(id);
            if (hotelServices == null || hotelServices.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Service_Not_Found } });

            var hotelServicesDTO = Mapper.Map<IEnumerable<HotelServices>, List<HotelServicesDTO>>(hotelServices);

            return Content(HttpStatusCode.OK, hotelServicesDTO);
        }


        /// <summary>
        /// Return hotel service by ServiceId
        /// </summary>
        /// <remarks>
        /// Get hotel service using youORDR serviceId. Authorization Required.
        /// </remarks>
        /// <param name="id">Service Id</param>
        /// <returns> Details Hotel Service</returns>
        [HttpGet]
        [Route("hotels/services/{id}", Name = "GetHotelServicesByServiceId")]

        [ResponseType(typeof(HotelServicesDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Hotel Services Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Services" })]

        public IHttpActionResult GetHotelServicesByServiceId(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelServices = _hotelService.GetActiveHotelServicesByServiceId(id);
            if (hotelServices == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Service_Not_Found } });

            var hotelServicesDTO = Mapper.Map<HotelServices, HotelServicesDTO>(hotelServices);

            return Content(HttpStatusCode.OK, hotelServicesDTO);
        }

        #endregion hotel services

        #region Concierge
        /// <summary>
        /// Returns all available hotel concierge groups with details 
        /// </summary>
        /// <remarks>
        /// Returns all available hotel concierge groups with details . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>List of concierge groups with details </returns>
        [HttpGet]
        [Route("hotels/{id}/concierge_groups")]

        [ResponseType(typeof(List<ConciergeGroupDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Concierge Groups Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Concierge Details" })]

        public IHttpActionResult GetConciergeGroups(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            var conciergeGroups = _hotelService.GetAllConciergeGroups(id);

            if (conciergeGroups == null || conciergeGroups.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ConciergeGroup_NotFound } });

            var conciergeGroupDTO = Mapper.Map<IEnumerable<ConciergeGroup>, List<ConciergeGroupDTO>>(conciergeGroups);

            return Content(HttpStatusCode.OK, conciergeGroupDTO);

        }
        #endregion Concierge

        #region hotel spa service

        /// <summary>
        /// Returns list of spa services with their details available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of spa services with their details available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of spa services</returns>
        [Route("hotels/{id}/spa_services", Name = "GetHotelSpaServices")]
        [HttpGet]

        [ResponseType(typeof(SpaServiceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa service not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelSpaServices(int id, int? page = 1, int? limit = null)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var Spaservices = _hotelService.GetHotelSpaServices(id, page.Value, limit.Value, out total);
            if (Spaservices == null || Spaservices.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAService_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = Spaservices.Select(s => s.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var spaServiceDTO = Mapper.Map<IEnumerable<SpaService>, List<SpaServiceDTO>>(Spaservices);

            if (baseCurrencyRate != 0)
            {
                foreach (var spaService in spaServiceDTO)
                {
                    spaService.SpaServiceDetail.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                    spaService.SpaServiceDetail.Select(s => { s.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return s; }).ToList();
                }
            }

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceDTO });
        }

        /// <summary>
        /// Returns list of spa services available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of spa services available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of spa services</returns>
        [Route("hotels/{id}/spa_services_list", Name = "GetHotelSpaServicesList")]
        [HttpGet]

        [ResponseType(typeof(SpaServicesListDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa service not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelSpaServicesList(int id, int? page = 1, int? limit = null)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var Spaservices = _hotelService.GetHotelSpaServices(id, page.Value, limit.Value, out total);
            if (Spaservices == null || Spaservices.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAService_NotFound } });

            var spaServiceDTO = Mapper.Map<IEnumerable<SpaService>, List<SpaServicesListDTO>>(Spaservices);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceDTO });
        }
        /// <summary>
        /// Returns spa service details
        /// </summary> 
        /// <remarks>
        /// Get a detailed  of spa service available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service Id</param>
        /// <param name="considerMyPreferences">My Preference</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of spa service</returns>
        [Route("hotels/spa_services/{id}", Name = "GetHotelSpaServiceById")]
        [HttpGet]

        [ResponseType(typeof(SpaServiceDetailWithImagesDTO))]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa service details not found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelSpaServiceById(int id, bool considerMyPreferences = false, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaServiceDetails = _hotelService.GetSpaServiceDetails(id, authenticatedCustomer.Id, page.Value, limit.Value, out   total, considerMyPreferences);
            if (spaServiceDetails.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaServiceDetail_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaServiceDetails.Select(s => s.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favSpaServiceDtl = _customerService.GetCustomerFavoriteSpaServiceDetails(authenticatedCustomer.Id);

            var spaServiceDTO = Mapper.Map<IEnumerable<SpaServiceDetail>, List<SpaServiceDetailWithImagesDTO>>(spaServiceDetails);
            spaServiceDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();
            spaServiceDTO.Select(m => { m.IsFavourite = favSpaServiceDtl.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();

            if (baseCurrencyRate != 0)
                spaServiceDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceDTO });
        }

        /// <summary>
        /// Returns spa service details with its all images
        /// </summary> 
        /// <remarks>
        /// Get Spa service details with its all images. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service detail Id</param>
        /// <returns>Details of spa service</returns>
        [Route("hotels/spa_service_details/{id}", Name = "GetSpaServiceDetailsById")]
        [HttpGet]

        [ResponseType(typeof(SpaServiceDetailWithOfferDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa service details not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaServiceDetailsById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaServiceDetails = _hotelService.GetSpaServiceDetailsById(id);
            if (spaServiceDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaServiceDetail_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaServiceDetails.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            bool IsFavourite = spaServiceDetails.CustomerFavoriteSpaService.Any(c => c.CustomerId == authenticatedCustomer.Id);
            double customerRating = 0;
            if (spaServiceDetails.SpaServiceReview != null && spaServiceDetails.SpaServiceReview.Count() > 0)
                customerRating = spaServiceDetails.SpaServiceReview.Average(rv => rv.Score);

            var spaServiceDetailDTO = Mapper.Map<SpaServiceDetail, SpaServiceDetailWithOfferDTO>(spaServiceDetails);
            if (IsFavourite)
                spaServiceDetailDTO.IsFavourite = IsFavourite;
            spaServiceDetailDTO.CustomerRating = customerRating;
            if (baseCurrencyRate != 0)
            {
                spaServiceDetailDTO.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(spaServiceDetailDTO.Price * baseCurrencyRate), 2);
                spaServiceDetailDTO.SpaServiceOffers.Select(o => { o.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.DiscountPrice) * baseCurrencyRate), 2); return o; }).ToList();
                spaServiceDetailDTO.SpaSuggestions.Select(o => { o.SpaServiceDetail1.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.SpaServiceDetail1.Price) * baseCurrencyRate), 2); return o; }).ToList();

                if (spaServiceDetailDTO.SpaDetailAdditional.Any())
                    spaServiceDetailDTO.SpaDetailAdditional.Select(s => { s.SpaAdditionalGroup.SpaAdditionalElement.Select(o => { o.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(o.Price * baseCurrencyRate), 2); return o; }).ToList(); return s; }).ToList();


                if (spaServiceDetailDTO.ExtraTimes != null && spaServiceDetailDTO.ExtraTimes.Count() > 0)
                    spaServiceDetailDTO.ExtraTimes.Select(o => { o.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.Price) * baseCurrencyRate), 2); return o; }).ToList();

                if (spaServiceDetailDTO.ExtraProcedures != null && spaServiceDetailDTO.ExtraProcedures.Count() > 0)
                    spaServiceDetailDTO.ExtraProcedures.Select(o => { o.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.Price) * baseCurrencyRate), 2); return o; }).ToList();
            }

            spaServiceDetailDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
            spaServiceDetailDTO.SpaSuggestions.Select(s => { s.SpaServiceDetail1.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return s; }).ToList();

            return Content(HttpStatusCode.OK, spaServiceDetailDTO);
        }

        /// <summary>
        /// Search Spa Service detail based on name
        /// </summary>
        /// <remarks>
        ///Search Spa Service detail based on name
        /// </remarks>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="hotelId">Enter Hotel Id</param>
        /// <param name="page">Enter page number</param>
        /// <param name="limit">Enter the page size</param>
        /// <returns>Returns list of hotel spa service details</returns>

        [HttpGet]
        [Route("hotels/spa_service_details/search", Name = "SearchSpaServiceDetails")]

        [ResponseType(typeof(List<SpaServiceDetailWithImagesDTO>))]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa service details not found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]

        public IHttpActionResult SearchSpaServiceDetails(int hotelId, string searchText = "", int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            int total;
            var spaServiceDetail = _hotelService.SearchSpaServiceDetailByName(searchText, hotelId, page.Value, limit.Value, out total);
            if (spaServiceDetail.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { Message = new string[] { Resources.SpaServiceDetail_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaServiceDetail.Select(s => s.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var spaServiceDetailDTO = Mapper.Map<IEnumerable<SpaServiceDetail>, List<SpaServiceDetailWithImagesDTO>>(spaServiceDetail);

            spaServiceDetailDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
                spaServiceDetailDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();

            var mod = total % limit;
            var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceDetailDTO });
        }




        /// <summary>
        /// Returns spa employee details list who provides these spa service detail
        /// </summary> 
        /// <remarks>
        /// Get spa employee details list who provides these spa service detail. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <param name="gender">Select Gender</param>
        /// <returns>Details list of spa service Employee</returns>
        [Route("hotels/spa_service_details/{id}/spaEmployees")]
        [HttpGet]

        [ResponseType(typeof(List<SpaEmployeeDetailsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa employee not found who provides this spa service detail")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaEmployeeListByDetailId(int id, Gender gender = Gender.None, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaEmployeeList = _hotelService.GetSpaEmployeeDetailsBySpaDetailId(id, page.Value, limit.Value, out total, gender);
            if (spaEmployeeList.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaEmp_NotFound } });

            var spaEmployeeDetailsDTO = Mapper.Map<IEnumerable<SpaEmployeeDetails>, List<SpaEmployeeDetailsDTO>>(spaEmployeeList);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaEmployeeDetailsDTO });
        }

        /// <summary>
        /// Returns spa service details Suggestions
        /// </summary>
        /// <remarks>
        /// Get spa service details Suggestions.
        /// </remarks>
        /// <param name="id">Spa Service Detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Suggestions of Spa service detail</returns>

        [HttpGet]
        [Route("hotels/spa_service_details/{id}/Suggestions")]

        [ResponseType(typeof(List<SpaServiceDetailWithImagesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Service Details Suggestion Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]

        public IHttpActionResult GetSpaServiceDetailSuggestions(int id, int? page = 1, int? limit = null)
        {
            try
            {
                page = page <= 0 ? 1 : page;
                limit = limit ?? ToOrderConfigs.DefaultPageSize;
                int total;
                var user = GetAuthenticatedCustomer();
                if (user == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var spaServiceDetSugg = _hotelService.GetSpaServiceDetailSuggestions(id, page.Value, limit.Value, out total);

                if (spaServiceDetSugg == null || spaServiceDetSugg.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaServDetSugg_NotFound } });

                double? baseCurrencyRate = 0;
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = spaServiceDetSugg.Select(mi => mi.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

                var favSpaServiceDtl = _customerService.GetCustomerFavoriteSpaServiceDetails(user.Id);
                var spaServiceSuggDTO = Mapper.Map<IEnumerable<SpaServiceDetail>, List<SpaServiceDetailWithImagesDTO>>(spaServiceDetSugg);
                spaServiceSuggDTO.Select(m => { m.IsFavourite = favSpaServiceDtl.Any(r => r.Id == m.Id) ? true : false; return m; }).ToList();
                spaServiceSuggDTO.Select(m => { m.PreferredCurrencySymbol = user.Currency.Symbol; return m; }).ToList();

                if (baseCurrencyRate != 0)
                    spaServiceSuggDTO.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

                var mod = total % limit.Value;
                var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = spaServiceSuggDTO });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
        }



        /// <summary>
        /// Returns spa employee details time slot
        /// </summary> 
        /// <remarks>
        /// Get Spa employee details available time slot. Authorization required to access this method.
        /// </remarks>
        /// <param name='id'>Spa Employee Details Id</param>
        /// <param name="extratimeId">Extratime Id</param>
        /// <param name="Ids">Enter extra procedure ids</param>
        ///<param name="date">Datetime</param>
        /// <returns>Details of spa service Employee timeslot</returns>
        [Route("hotels/spa_employee_details/{id}/timeslot")]
        [HttpGet]

        [ResponseType(typeof(List<SpaEmployeeAvailableTimeSlotDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "No time slot found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaEmployeeDetailsTimeSlotById(int id, DateTime date, int? extratimeId = null, [FromUri] int[] Ids = null)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            if (extratimeId == null)
                extratimeId = 0;
            else if (extratimeId <= 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { "Extra time id should not zero or negative" } });
            try
            {
                var spaEmployeeDetailTimeSlot = _hotelService.GetSpaEmployeeDetailsTimeSlotById(id, date, Ids, extratimeId);
                if (spaEmployeeDetailTimeSlot == null || spaEmployeeDetailTimeSlot.Count() < 1)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { "time slot not found" } });

                var spaEmployeeDetailTimeSlotDTO = Mapper.Map<IEnumerable<SpaEmployeeAvailableTimeSlot>, List<SpaEmployeeAvailableTimeSlotDTO>>(spaEmployeeDetailTimeSlot);

                return Content(HttpStatusCode.OK, new { result = spaEmployeeDetailTimeSlotDTO });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }



        /// <summary>
        /// Returns extra times for spa service detail
        /// </summary> 
        /// <remarks>
        /// Get list of extra times for this spa service detail. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details list of extra times for spa service detail</returns>
        [Route("hotels/spa_service_details/{id}/extraTime")]
        [HttpGet]

        [ResponseType(typeof(List<ExtraTimeDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Extra time not provides for this spa service detail")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaServiceDetsExtratimes(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaDtlExtratime = _hotelService.GetSpaServiceDetsExtratimes(id, page.Value, limit.Value, out total);
            if (spaDtlExtratime.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Extratime } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaDtlExtratime.Select(e => e.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var spaDetailExtratimesDTO = Mapper.Map<IEnumerable<ExtraTime>, List<ExtraTimeDTO>>(spaDtlExtratime);
            if (baseCurrencyRate != 0)
                spaDetailExtratimesDTO.Select(o => { o.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.Price) * baseCurrencyRate), 2); return o; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaDetailExtratimesDTO });
        }


        /// <summary>
        /// Returns extra procedures for spa service detail
        /// </summary> 
        /// <remarks>
        /// Get list of extra procedures for this spa service detail. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details list of extra procedures for spa service detail</returns>
        [Route("hotels/spa_service_details/{id}/extraProcedure")]
        [HttpGet]

        [ResponseType(typeof(List<ExtraProcedureDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Extra procedure not provides for this spa service detail")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaServiceDetsExtraprocedures(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaDtlExtraprocedures = _hotelService.GetSpaServiceDetsExtraprocedures(id, page.Value, limit.Value, out total);
            if (spaDtlExtraprocedures.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Extraprocedure } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaDtlExtraprocedures.Select(e => e.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var spaDetailExtraproceduresDTO = Mapper.Map<IEnumerable<ExtraProcedure>, List<ExtraProcedureDTO>>(spaDtlExtraprocedures);
            if (baseCurrencyRate != 0)
                spaDetailExtraproceduresDTO.Select(o => { o.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.Price) * baseCurrencyRate), 2); return o; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaDetailExtraproceduresDTO });
        }

        ///// <summary>
        ///// Returns  spa service booking details by id 
        ///// </summary> 
        ///// <remarks>
        ///// Get SPA service booking details by booking id . Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">SPA Service Booking Id</param>
        ///// <returns>Details of spa service booking</returns>
        //[Route("hotels/spa_serviceBooking/{id}", Name = "GetHotelSPAServiceBooking")]
        //[HttpGet]

        //[ResponseType(typeof(SPAServiceBookingDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, "No SPA Service Booking Found")]
        //[SwaggerOperation(Tags = new[] { "Hotel SPA Service" })]
        //[ApiExplorerSettings(IgnoreApi = false)]
        //public IHttpActionResult GetHotelSPAServiceBooking(int id)
        //{
        //    var customer = GetAuthenticatedCustomer();
        //    if (customer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var spaServiceBooking = _hotelService.GetHotelSPAServiceBooking(id);
        //    if (spaServiceBooking == null)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAServiceBooking_NotFound } });

        //    if (spaServiceBooking.CustomerId != customer.Id)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var spaServiceBookingDTO = Mapper.Map<SPAServiceBooking, SPAServiceBookingDTO>(spaServiceBooking);
        //    return Content(HttpStatusCode.OK, spaServiceBookingDTO);
        //}


        ///// <summary>
        ///// Creates Hotel SPA service bookings
        ///// </summary>
        ///// <remarks>
        ///// Create new SPA service bookings
        ///// </remarks>
        ///// <param name="SPAServiceBookingDTO">SPA Service booking Details</param>
        ///// <returns>Returns newly created hotel SPA service booking details</returns>
        //[HttpPost]
        //[Route("hotel/spa_serviceBooking", Name = "CreateSPAServiceBooking")]

        //[ResponseType(typeof(SPAServiceBookingDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Service not available during this time")]
        //[SwaggerOperation(Tags = new[] { "Hotel SPA Service" })]
        //public IHttpActionResult CreateSPAServiceBooking(CreateSPAServiceBookingDTO SPAServiceBookingDTO)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        List<string> messages = new List<string>();
        //        foreach (var error in ModelState.Values)
        //        {
        //            var x = error.Errors.Select(e => e.ErrorMessage);
        //            messages.AddRange(x);
        //        }
        //        return Content(HttpStatusCode.BadRequest, new { messages = messages });
        //    }

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }

        //    var entity = Mapper.Map<SPAServiceBooking>(SPAServiceBookingDTO);
        //    entity.CustomerId = authenticatedCustomer.Id;

        //    try
        //    {
        //        entity = _hotelService.CreateHotelSPAServiceBooking(entity);
        //    }
        //    catch (ArgumentException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }

        //    return Created(Url.Link("GetHotelSPAServiceBooking", new { id = entity.Id }), Mapper.Map<SPAServiceBookingDTO>(entity));

        //}

        ///// <summary>
        ///// Returns all SPA Service bookings place by customer
        ///// </summary>
        ///// <remarks>
        ///// Returns all  SPA Service bookings place by customer
        ///// </remarks>
        ///// <returns>Returns all  SPA Service bookings place by customer</returns>
        //[HttpGet]
        //[Route("hotel/spa_serviceBooking", Name = "GetSPAServiceBooking")]

        //[ResponseType(typeof(List<SPAServiceBookingDTO>))]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "No SPA Service Booking Found ")]
        //[SwaggerOperation(Tags = new[] { "Hotel SPA Service" })]
        //public IHttpActionResult GetSPAServiceBooking()
        //{
        //    var customer = GetAuthenticatedCustomer();
        //    if (customer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var spaServiceBooking = _hotelService.GetCustomerSPAServiceBooking(customer.Id);
        //    if (spaServiceBooking == null || spaServiceBooking.Count() == 0)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.SPAServiceBooking_NotFound } });

        //    var spaServiceBookingDTO = Mapper.Map<IEnumerable<SPAServiceBooking>, List<SPAServiceBookingDTO>>(spaServiceBooking);
        //    return Content(HttpStatusCode.OK, spaServiceBookingDTO);
        //}

        ///// <summary>
        ///// Deletes Spa Service Booking
        ///// </summary>
        ///// <remarks>
        ///// Delete hotel service booking details based on spa service booking id.
        ///// </remarks>
        ///// <param name="Id">SPA Service Booking Id</param>
        ///// <returns></returns>
        //[Route("hotels/spa_serviceBooking/{id}", Name = "DeleteSpaServiceBooking")]
        //[HttpDelete]
        //[SwaggerResponse(HttpStatusCode.OK, "Success")]
        //[SwaggerOperation(Tags = new[] { "Hotel SPA Service" })]
        //public IHttpActionResult DeleteSpaServiceBooking(int Id)
        //{
        //    var spaServiceBooking = _hotelService.GetHotelSPAServiceBooking(Id);
        //    if (spaServiceBooking == null)
        //    {
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAServiceBooking_NotFound } });
        //    }
        //    _hotelService.DeleteSpaServiceBooking(Id);
        //    return Ok();
        //}


        ///// <summary>
        ///// Update Hotel SPA service bookings
        ///// </summary>
        ///// <remarks>
        ///// Update SPA service bookings
        ///// </remarks>
        ///// <param name="SPAServiceBookingDTO">SPA Service booking Details</param>
        ///// <returns>Returns updated hotel SPA service booking details</returns>
        //[HttpPut]
        //[Route("hotel/spa_serviceBooking", Name = "UpdateSPAServiceBooking")]
        //[ResponseType(typeof(SPAServiceBookingDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Service not available during this time")]
        //[SwaggerOperation(Tags = new[] { "Hotel SPA Service" })]
        //public IHttpActionResult UpdateSPAServiceBooking(UpdateSPAServiceBookingDTO SPAServiceBookingDTO)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        List<string> messages = new List<string>();
        //        foreach (var error in ModelState.Values)
        //        {
        //            var x = error.Errors.Select(e => e.ErrorMessage);
        //            messages.AddRange(x);
        //        }
        //        return Content(HttpStatusCode.BadRequest, new { messages = messages });
        //    }

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }

        //    var entity = Mapper.Map<SPAServiceBooking>(SPAServiceBookingDTO);
        //    entity.CustomerId = authenticatedCustomer.Id;

        //    try
        //    {
        //        entity = _hotelService.ModifySPAServiceBooking(authenticatedCustomer.Id, entity);
        //    }
        //    catch (ArgumentException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }
        //    catch (InvalidOperationException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }

        //    return Created(Url.Link("GetHotelSPAServiceBooking", new { id = entity.Id }), Mapper.Map<SPAServiceBookingDTO>(entity));

        //}

        #endregion

        #region Spa Service Offers

        /// <summary>
        /// Returns hotel spa service details offers
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotel spa service details containing offers
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="considerMyPreferences">My Preference</param>
        /// <returns>List of hotel spa service details containing offers</returns>
        [HttpGet]
        [Route("hotels/{id}/spa_services_offers", Name = "GetHotelSpaServiceOffers")]
        [AllowAnonymous]
        [ResponseType(typeof(List<HotelSpaServiceOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Offers Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Spa Service Offers" })]

        public IHttpActionResult GetHotelSpaServiceOffers(int id, bool considerMyPreferences = false)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotel = _hotelService.GetHotelById(id);
            if (hotel == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            var hotelSpaServiceDetailOffer = _hotelService.GetHotelSpaServiceDetailOffer(id, customer.Id, considerMyPreferences);
            if (hotelSpaServiceDetailOffer == null || hotelSpaServiceDetailOffer.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaServiceOffer_NotFound } });

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = hotelSpaServiceDetailOffer.Select(s => s.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favSpaServiceDetails = _customerService.GetCustomerFavoriteSpaServiceDetails(customer.Id);
            var spaServiceDetOffersDTO = Mapper.Map<IEnumerable<SpaServiceOffer>, List<HotelSpaServiceOffersDTO>>(hotelSpaServiceDetailOffer);

            spaServiceDetOffersDTO.Select(s => { s.IsFavourite = favSpaServiceDetails.Any(r => r.Id == s.SpaServiceDetailId) ? true : false; return s; }).ToList();
            spaServiceDetOffersDTO.Select(i => { i.PreferredCurrencySymbol = customer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
            {
                spaServiceDetOffersDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                spaServiceDetOffersDTO.Select(s => { s.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.DiscountPrice * baseCurrencyRate), 2); return s; }).ToList();
            }
            return Content(HttpStatusCode.OK, spaServiceDetOffersDTO);
        }


        /// <summary>
        /// Returns hotel spa service details containing offers of particular category
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotel spa service details containing offers of particular category
        /// </remarks>
        /// <param name="id">Spa Service Id</param>
        /// <returns>List of hotel spa service details containing offers of particular category</returns>
        [HttpGet]
        [Route("hotels/spa_services/{id}/offers", Name = "GetSpaServiceOffers")]
        [AllowAnonymous]
        [ResponseType(typeof(List<HotelSpaServiceOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Offers Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Spa Service Offers" })]

        public IHttpActionResult GetSpaServiceOffers(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaService = _hotelService.GetHotelSpaServiceById(id);
            if (spaService == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAService_NotFound } });

            var spaServiceDetailOffer = _hotelService.GetSpaServiceDetailOfferBySpaServiceId(id);
            if (spaServiceDetailOffer == null || spaServiceDetailOffer.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaServiceOffer_NotFound } });

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = spaServiceDetailOffer.Select(s => s.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favSpaServiceDetails = _customerService.GetCustomerFavoriteSpaServiceDetails(customer.Id);
            var spaServiceDetOffersDTO = Mapper.Map<IEnumerable<SpaServiceOffer>, List<HotelSpaServiceOffersDTO>>(spaServiceDetailOffer);

            spaServiceDetOffersDTO.Select(s => { s.IsFavourite = favSpaServiceDetails.Any(r => r.Id == s.SpaServiceDetailId) ? true : false; return s; }).ToList();
            spaServiceDetOffersDTO.Select(i => { i.PreferredCurrencySymbol = customer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
            {
                spaServiceDetOffersDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                spaServiceDetOffersDTO.Select(s => { s.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.DiscountPrice * baseCurrencyRate), 2); return s; }).ToList();
            }
            return Content(HttpStatusCode.OK, spaServiceDetOffersDTO);
        }



        #endregion Spa Service Offers

        #region Spa Cart

        /// <summary>
        /// Returns the Spa Cart details of specified customer
        /// </summary>
        /// <remarks>
        /// Returs the Spa Cart of specified customer
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Returns Cart Details</returns>
        [HttpGet]
        [Route("hotels/{id}/cart", Name = "GetSpaCart")]

        [ResponseType(typeof(SpaCartDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Cart Not Available for hotel")]
        [SwaggerOperation(Tags = new[] { "Spa Cart" })]
        public IHttpActionResult GetSpaCart(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var cart = _hotelService.GetCustomerSpaCart(customer.Id, id);
            if (cart == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cart_IsEmpty } });

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = cart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            cart.SpaCartItems = cart.SpaCartItems.OrderBy(c => c.ServeLevel).ToList();
            var spaCartDTO = Mapper.Map<SpaCart, SpaCartDTO>(cart);

            if (baseCurrencyRate != 0)
            {
                foreach (var SpaCartItem in spaCartDTO.SpaCartItems)
                {
                    if (SpaCartItem.ExtraTime != null)
                        SpaCartItem.ExtraTime.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(SpaCartItem.ExtraTime.Price * baseCurrencyRate), 2);
                }
                if (spaCartDTO.SpaCartItems.Any(c => c.SpaCartExtraProcedures.Count() > 0))
                    spaCartDTO.SpaCartItems.Select(c => c.SpaCartExtraProcedures.Select(p => { p.ExtraProcedure.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(p.ExtraProcedure.Price * baseCurrencyRate), 2); return p; }).ToList()).ToList();
            }
            return Content(HttpStatusCode.OK, spaCartDTO);
        }

        /// <summary>
        /// Creates new Spa cart /  basket
        /// </summary>
        /// <remarks>
        /// Creates customer Spa cart /  basket
        /// </remarks>
        /// <param name="createSpaCartDTO">Spa Details in Request body</param>
        /// <param name="flush">Flushes old cart and creates new one</param>
        /// <returns>Returns newly created Spa details in response</returns>

        [Route("hotel/Spa/cart")]
        [HttpPost]
        [ResponseType(typeof(SpaCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error or Cart Already exists")]
        [SwaggerOperation(Tags = new[] { "Spa Cart" })]
        public IHttpActionResult CreateSpaCart(CreateSpaCartDTO createSpaCartDTO, bool flush = false)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            foreach (var item in createSpaCartDTO.SpaCartItems)
            {
                item.Qty = 1;

                if (item.SpaCartItemsAdditionals != null)
                    foreach (var additional in item.SpaCartItemsAdditionals)
                    {
                        if (additional.Qty == null)
                            additional.Qty = 1;
                    }
            }

            var entity = Mapper.Map<SpaCart>(createSpaCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateSpaCart(entity, flush);
                entity.Hotel = _hotelService.GetHotelById(entity.HotelId);

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<SpaCart, SpaCartDTO>(entity);

                if (baseCurrencyRate != 0)
                {
                    foreach (var SpaCartItem in cartDTO.SpaCartItems)
                    {
                        if (SpaCartItem.ExtraTime != null)
                            SpaCartItem.ExtraTime.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(SpaCartItem.ExtraTime.Price * baseCurrencyRate), 2);
                    }
                    if (cartDTO.SpaCartItems.Any(c => c.SpaCartExtraProcedures.Count() > 0))
                        cartDTO.SpaCartItems.Select(c => c.SpaCartExtraProcedures.Select(p => { p.ExtraProcedure.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(p.ExtraProcedure.Price * baseCurrencyRate), 2); return p; }).ToList()).ToList();
                }
                return Created(Url.Link("GetSpaCart", new { id = cartDTO.HotelId }), new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing Spa service detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Updates spa cart /  basket
        /// </summary>
        /// <remarks>
        /// Updates customer spa cart. Ex To update qty just send new quantity of item.
        /// </remarks>
        /// <param name="updateSpaCartDTO">Just specify the new properties</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("hotel/Spa/cart")]
        [HttpPut]
        [ResponseType(typeof(SpaCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Spa Cart" })]
        public IHttpActionResult UpdateSpaCart(UpdateSpaCartDTO updateSpaCartDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            foreach (var item in updateSpaCartDTO.SpaCartItems)
            {
                if (item.SpaCartItemsAdditionals != null)
                    foreach (var additional in item.SpaCartItemsAdditionals)
                    {
                        if (additional.Qty == null)
                            additional.Qty = 1;
                    }
            }

            var entity = Mapper.Map<SpaCart>(updateSpaCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.UpdateSpaCart(authenticatedCustomer.Id, entity);
                entity.Hotel = _hotelService.GetHotelById(entity.HotelId);

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<SpaCart, SpaCartDTO>(entity);

                if (baseCurrencyRate != 0)
                {
                    foreach (var SpaCartItem in cartDTO.SpaCartItems)
                    {
                        if (SpaCartItem.ExtraTime != null)
                            SpaCartItem.ExtraTime.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(SpaCartItem.ExtraTime.Price * baseCurrencyRate), 2);
                    }
                    if (cartDTO.SpaCartItems.Any(c => c.SpaCartExtraProcedures.Count() > 0))
                        cartDTO.SpaCartItems.Select(c => c.SpaCartExtraProcedures.Select(p => { p.ExtraProcedure.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(p.ExtraProcedure.Price * baseCurrencyRate), 2); return p; }).ToList()).ToList();
                }

                return Content(HttpStatusCode.OK, new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (ZeroItemQtyException zeroQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Zero_Spa_Qty } });
            }
            catch (NegativeItemQtyException negativeQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Negative_Spa_Qty } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing spa detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        #endregion Spa Cart

        #region Spa Order

        /// <summary>
        /// Returs the Spa Order details
        /// </summary>
        /// <remarks>
        /// Returs the Spa Order details
        /// </remarks>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns Spa Order Details</returns>
        [HttpGet]
        [Route("hotel/Spa/order", Name = "GetSpaOrder")]

        [ResponseType(typeof(SpaOrderDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Spa Order" })]
        public IHttpActionResult GetSpaOrder(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetSpaOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaOrder_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            order.SpaOrderItems = order.SpaOrderItems.OrderBy(o => o.ServeLevel).ToList();

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = order.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var spaOrderDTO = Mapper.Map<SpaOrder, SpaOrderDTO>(order);
            if (baseCurrencyRate != 0)
            {
                foreach (var SpaOrderItem in spaOrderDTO.SpaOrderItems)
                {
                    if (SpaOrderItem.ExtraTime != null)
                        SpaOrderItem.ExtraTime.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(SpaOrderItem.ExtraTime.Price * baseCurrencyRate), 2);
                }

                if (spaOrderDTO.SpaOrderItems.Any(c => c.SpaOrderExtraProcedures.Count() > 0))
                    spaOrderDTO.SpaOrderItems.Select(c => c.SpaOrderExtraProcedures.Select(p => { p.ExtraProcedure.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(p.ExtraProcedure.Price * baseCurrencyRate), 2); return p; }).ToList()).ToList();
            }

            spaOrderDTO.ReceiptNumber = "#" + order.Id;
            SpaOrderStatus ordrStatus = (SpaOrderStatus)order.OrderStatus;
            spaOrderDTO.OrderStatus = ordrStatus.ToString();

            return Content(HttpStatusCode.OK, spaOrderDTO);
        }


        /// <summary>
        /// Place order for spa items in basket
        /// </summary>
        /// <remarks>
        /// Place order for cart spa items
        /// </remarks>
        /// <param name="cardId"> select customer Card</param>
        /// <param name="hotelId">Hotel Id</param>
        /// <returns>Returns details of newly created spa order</returns>

        [Route("hotel/Spa/order")]
        [HttpPost]
        [ResponseType(typeof(SpaOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Spa Order" })]
        public IHttpActionResult PlaceSpaOrderFromCart(int cardId, int hotelId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            SpaOrder entity;
            try
            {
                entity = _hotelService.CreateSpaOrderFromCart(authenticatedCustomer.Id, cardId, hotelId);

                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var result = Mapper.Map<SpaOrder, SpaOrderDTO>(entity);
                if (baseCurrencyRate != 0)
                {
                    foreach (var SpaOrderItem in result.SpaOrderItems)
                    {
                        if (SpaOrderItem.ExtraTime != null)
                            SpaOrderItem.ExtraTime.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(SpaOrderItem.ExtraTime.Price * baseCurrencyRate), 2);
                    }

                    if (result.SpaOrderItems.Any(c => c.SpaOrderExtraProcedures.Count() > 0))
                        result.SpaOrderItems.Select(c => c.SpaOrderExtraProcedures.Select(p => { p.ExtraProcedure.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(p.ExtraProcedure.Price * baseCurrencyRate), 2); return p; }).ToList()).ToList();
                }

                result.ReceiptNumber = "#" + entity.Id;
                SpaOrderStatus ordrStatus = (SpaOrderStatus)entity.OrderStatus;
                result.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = result });
            }
            catch (CartEmptyException empty)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Cart_Empty } });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { invalidDateTime.Message } });
            }
            catch (CardSpendingLimitException limitEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_Card_SpendingLimit } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing spa item id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Schedule your spa order
        /// </summary>
        /// <remarks>
        /// Schedule your spa order
        /// </remarks> 
        /// <param name="orderId"> Enter spa order Id</param>
        /// <param name="scheduleDate">spa schedule date time</param>       
        /// <returns>Returns details of scheduled spa order</returns>

        //[Route("hotel/Spa/order_schedule")]
        //[HttpPut]
        //[ResponseType(typeof(SpaOrderDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        //[SwaggerOperation(Tags = new[] { "Spa Order" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult ScheduleSpaOrder(int orderId, DateTime scheduleDate)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }
        //    SpaOrder entity;
        //    try
        //    {
        //        entity = _hotelService.ScheduleSpaOrder(authenticatedCustomer.Id, orderId, scheduleDate);
        //    }
        //    catch (OrderNotFoundException orderNotFound)
        //    {
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { orderNotFound.Message } });
        //    }
        //    catch (InvalidScheduleDateTimeException invalidDateTime)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { invalidDateTime.Message } });
        //    }
        //    catch (ArgumentException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }

        //    var result = Mapper.Map<SpaOrder, SpaOrderDTO>(entity);

        //    result.ReceiptNumber = "#" + entity.Id;
        //    // result.OrderDate = (entity.ScheduledDate == null) ? entity.CreationDate : entity.ScheduledDate;
        //    SpaOrderStatus ordrStatus = (SpaOrderStatus)entity.OrderStatus;
        //    result.OrderStatus = ordrStatus.ToString();

        //    return Content(HttpStatusCode.Created, new { status = true, data = result });
        //}

        /// <summary>
        /// Create new reviews for Spa order
        /// </summary>
        /// <remarks>
        /// Adds new reviews for spa order in the backend
        /// </remarks>
        /// <param name="orderReviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of spa reviews</returns>

        [Route("hotels/spaorder/Review")]
        [HttpPost]
        [ResponseType(typeof(SpaOrderReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Spa Order" })]
        public IHttpActionResult CreateSpaOrderReview(CreateSpaOrderReviewDTO orderReviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var order = _hotelService.GetSpaOrdersDetailsById(orderReviewDTO.SpaOrderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaOrder_NotFound } });
            if (order.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var entity = Mapper.Map<SpaOrderReview>(orderReviewDTO);
            entity = _hotelService.CreateSpaOrderReview(entity);
            var orderReviewsDTO = Mapper.Map<SpaOrderReviewDTO>(entity);

            return Content(HttpStatusCode.OK, orderReviewsDTO);
        }

        /// <summary>
        /// Returns list of spa order reviews
        /// </summary>
        /// <remarks>
        /// Get list of spa order reviews.
        /// </remarks>
        /// <param name="id">Spa Order Id</param>
        /// <returns>List of spa order Reviews</returns>
        /// 

        [Route("hotels/spaorder/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<SpaOrderReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Spa Order" })]

        public IHttpActionResult GetSpaOrderReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var order = _hotelService.GetSpaOrdersDetailsById(id);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaOrder_NotFound } });
            if (order.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var orderReviews = _hotelService.GetSpaOrderReviews(id);
            if (orderReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var orderReviewDTO = Mapper.Map<IEnumerable<SpaOrderReview>, List<SpaOrderReviewDTO>>(orderReviews);
            return Content(HttpStatusCode.OK, orderReviewDTO);
        }

        #endregion Spa Order

        #region Hotel Services Is Late Status
        /// <summary>
        /// Sets the IsLate status for the hotel services order and returns the status
        /// </summary>
        /// <remarks>
        /// Sets the IsLate status for the hotel services order and returns the status
        /// </remarks>
        /// <param name="id">Order Id</param>
        /// <returns>Sets the IsLate status for the hotel services order and returns the status</returns>
        [HttpPut]
        [Route("hotel/services/order/{id}/IsLateStatus", Name = "SetHotelServiceOrderIsLateStatus")]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "I am Late status is already set")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Services Is Late Status" })]
        public IHttpActionResult SetHotelServiceOrderIsLateStatus(int id, HotelServicesIsLateEnum hoteleServicesIsLate)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            switch (hoteleServicesIsLate)
            {
                case HotelServicesIsLateEnum.Spa:
                    var order = _hotelService.GetSpaOrdersDetailsById(id);
                    if (order == null)
                        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaOrder_NotFound } });

                    if (order.CustomerId != customer.Id)
                        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                    if (order.IsLate)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.HotelServicesOrderIsLate_AlreadySets } });

                    order.IsLate = true;
                    _hotelService.UpdateSpaOrderStatus(order);
                    break;
                case HotelServicesIsLateEnum.Excursion:
                    var excursionOrder = _hotelService.GetExcursionOrdersDetailsById(id);
                    if (excursionOrder == null)
                        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionOrder_NotFound } });

                    if (excursionOrder.CustomerId != customer.Id)
                        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                    if (excursionOrder.IsLate)
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.HotelServicesOrderIsLate_AlreadySets } });

                    excursionOrder.IsLate = true;
                    _hotelService.ModifyExcursionOrder(excursionOrder);
                    break;
            }

            Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();
            Hubs.NotificationHub.NotifyStatusChangeToKitchenAdmin();
            return Content(HttpStatusCode.OK, "Success");
        }
        #endregion Hotel Services Is Late Status

        #region Hotel Services Payment
        /// <summary>
        /// Pay the hotel service order amount using the card
        /// </summary>
        /// <param name="payDTO">Request data</param>
        /// <param name="hotelServiceType">Select Appropriate hotel service</param>
        /// <returns>Status</returns>

        [Route("hotels/services/order/Pay")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Services Payment" })]
        public IHttpActionResult PayOrder(CreateHotelPayDTO payDTO, HotelServiceType hotelServiceType)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.HotelServiceOrderCardPayment(payDTO.OrderId, payDTO.Currency, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, payDTO.CardId, authenticatedCustomer.Id, hotelServiceType);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        /// <summary>
        /// Cancel the hotel services order and release amount reserved for the order
        /// </summary>
        /// <remarks>
        /// Cancel the hotel services order and release amount reserved for the order
        /// </remarks>
        /// <param name="orderId">Hotel Service Order Id</param>
        /// <param name="hotelServiceType">Select Appropriate hotel service</param>
        /// <returns>Returns Status</returns>

        [HttpPost]
        [Route("hotel/services/order/{orderId}/cancel", Name = "CancelHotelServiceOrder")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Succes Status")]
        [SwaggerOperation(Tags = new[] { "Hotel Services Payment" })]
        public IHttpActionResult CancelHotelServiceOrder(int orderId, HotelServiceType hotelServiceType)
        {
            try
            {
                var customer = GetAuthenticatedCustomer();
                if (customer == null)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.CancelHotelServiceOrderAndReleaseAmount(orderId, customer.Id, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, hotelServiceType);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (NotFoundException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Failed" } });
            }
        }

        /// <summary>
        /// Pay the order tips using the card
        /// </summary>
        /// <param name="payDTO">Request data</param>
        /// <param name="hotelServiceType">Select Appropriate hotel service</param>
        /// <returns>Status</returns>
        [Route("hotels/services/order_tip/Pay")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Tips paid successfully")]
        [SwaggerOperation(Tags = new[] { "Hotel Services Payment" })]
        public IHttpActionResult PayOrderTips(CreateHotelTipPayDTO payDTO, HotelServiceType hotelServiceType)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.HotelServiceTipsPaymentUsingCard(payDTO.OrderId, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, payDTO.TipAmount, payDTO.Comment, payDTO.CardId, authenticatedCustomer.Id, hotelServiceType);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        #endregion Hotel Services Payment

        #region Hotel Trip
        /// <summary>
        /// Returns list of available hotel Trips
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of available hotel trips . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of available hotel trips</returns>
        [Route("hotels/{id}/trips", Name = "GetHotelTrips")]
        [HttpGet]
        [ResponseType(typeof(HotelTripsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Hotel Trips Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult GetHotelTrips(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var trips = _hotelService.GetHotelTrips(id);

            if (trips == null || trips.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Trips_NotFound } });

            var tripsDTO = Mapper.Map<IEnumerable<Trip>, List<HotelTripsDTO>>(trips);

            return Content(HttpStatusCode.OK, tripsDTO);
        }

        /// <summary>
        /// Returns hotel Trips details
        /// </summary> 
        /// <remarks>
        /// Get a list of hotel Trips details . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Trip Id</param>
        /// <returns>Details of available hotel trip details</returns>
        [Route("hotels/trip_details/{id}", Name = "GetHotelTripDetails")]
        [HttpGet]
        [ResponseType(typeof(HotelTripDetailsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Hotel Trips Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult GetHotelTripDetails(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var tripDetails = _hotelService.GetHotelTripByID(id);

            if (tripDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Trips_NotFound } });

            var tripDetailsDTO = Mapper.Map<Trip, HotelTripDetailsDTO>(tripDetails);

            return Content(HttpStatusCode.OK, tripDetailsDTO);
        }


        /// <summary>
        /// Returns  trip booking details by id 
        /// </summary> 
        /// <remarks>
        /// Get trip booking details by booking id . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Trip Booking Id</param>
        /// <returns>Details of Trip booking</returns>
        [Route("hotels/trip_booking/{id}", Name = "GetHotelTripBooking")]
        [HttpGet]

        [ResponseType(typeof(TripBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Hotel trip booking Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult GetHotelTripBooking(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var tripBooking = _hotelService.GetHotelTripBookingByID(id);
            if (tripBooking == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TripBooking_NotFound } });

            if (tripBooking.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var tripBookingkingDTO = Mapper.Map<TripBookingDTO>(tripBooking);
            return Content(HttpStatusCode.OK, tripBookingkingDTO);
        }


        /// <summary>
        /// Returns all Trips bookings place by customer
        /// </summary>
        /// <remarks>
        /// Returns all  Trips bookings place by customer
        /// </remarks>
        /// <returns>Returns all  Trips bookings place by customer</returns>
        [HttpGet]
        [Route("hotels/trip_booking", Name = "GetAllTripBookingOfCustomer")]
        [ResponseType(typeof(List<TripBookingDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Trip Booking Found ")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult GetAllTripBookingOfCustomer()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var tripBooking = _hotelService.GetAllTripBookingOfCustomer(customer.Id);
            if (tripBooking == null || tripBooking.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.TripBooking_NotFound } });

            var tripBookingDTO = Mapper.Map<IEnumerable<TripBooking>, List<TripBookingDTO>>(tripBooking);
            return Content(HttpStatusCode.OK, tripBookingDTO);
        }

        /// <summary>
        /// Create hotel trip booking
        /// </summary>
        /// <remarks>
        /// Adds new hotel trip booking. Authorization required to access this method.
        /// </remarks>
        /// <param name="hotelTripBookingDTO">Hotel Trip Booking Details</param>
        /// <returns>Returns newly created trip bookings details</returns>
        [HttpPost]
        [Route("hotels/trip_booking")]
        [ResponseType(typeof(TripBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult CreateHotelTripBooking(CreateTripBookingDTO tripBookingDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var entity = Mapper.Map<TripBooking>(tripBookingDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateHotelTripBookings(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            return Created(Url.Link("GetHotelTripBooking", new { id = entity.Id }), Mapper.Map<TripBookingDTO>(entity));
        }

        /// <summary>
        /// Update hotel trip booking
        /// </summary>
        /// <remarks>
        /// Modify hotel trip booking. Authorization required to access this method.
        /// </remarks>
        /// <param name="updateTripBookingDTO">Hotel Trip Booking Details</param>
        /// <returns>Returns modified trip bookings details</returns>
        [HttpPut]
        [Route("hotels/trip_booking")]
        [ResponseType(typeof(TripBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult UpdateHotelTripBooking(UpdateTripBookingDTO updateTripBookingDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var entity = Mapper.Map<TripBooking>(updateTripBookingDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.ModifyHotelTripBooking(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            return Created(Url.Link("GetHotelTripBooking", new { id = entity.Id }), Mapper.Map<TripBookingDTO>(entity));
        }


        /// <summary>
        /// Deletes Trip Booking
        /// </summary>
        /// <remarks>
        /// Delete hotel trip booking details based on trip booking id.
        /// </remarks>
        /// <param name="Id">Trip Booking Id</param>
        /// <returns></returns>
        [Route("hotels/trip_Booking/{id}", Name = "DeleteTripBooking")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Trips" })]
        public IHttpActionResult DeleteTripBooking(int Id)
        {
            var tripBooking = _hotelService.GetHotelTripBookingByID(Id);
            if (tripBooking == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SPAServiceBooking_NotFound } });
            }
            _hotelService.DeleteTripBooking(Id);
            return Ok();
        }

        /// <summary>
        /// Returns list of trip reviews
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of trip reviews. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Trip Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Returns reviews of particular trip</returns>
        [Route("hotels/trip/{id}/tripreview", Name = "GetTripReviews")]
        [HttpGet]

        [ResponseType(typeof(List<TripReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Trip Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Trip Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetTripReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var tripReviews = _hotelService.GetTripReviews(id, page.Value, limit.Value, out total);
            if (tripReviews == null || tripReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var tripReviewsDTO = Mapper.Map<IEnumerable<TripReview>, List<TripReviewDTO>>(tripReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = tripReviewsDTO });
        }

        /// <summary>
        /// Returns trip Review by Id
        /// </summary>
        /// <remarks>
        /// Get trip Review by Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Returns trip Review by reviewId</returns>
        [HttpGet]
        [Route("hotels/tripreview/{id}", Name = "GetTripReviewById")]

        [ResponseType(typeof(TripReviewDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Trip review not found")]
        [SwaggerOperation(Tags = new[] { "Trip Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetTripReviewById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var review = _hotelService.GetTripReviewById(id);

            if (review == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewDTO = Mapper.Map<TripReviewDTO>(review);

            return Content(HttpStatusCode.OK, reviewDTO);
        }

        /// <summary>
        /// Add new trip reviews
        /// </summary>
        /// <remarks>
        /// Add new trip reviews
        /// </remarks>
        /// <param name="tripReviewsDTO">Trip Review Details</param>
        /// <returns>Returns newly created trip review details</returns>
        [HttpPost]
        [Route("hotels/tripreview", Name = "CreateTripReview")]

        [ResponseType(typeof(TripReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Trip Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateTripReview(CreateTripReviewDTO tripReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<TripReview>(tripReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateTripReviews(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            return Created(Url.Link("GetTripReviewById", new { id = entity.Id }), Mapper.Map<TripReviewDTO>(entity));
        }


        /// <summary>
        /// Deletes trip review
        /// </summary>
        /// <remarks>
        /// Delete trip review details based on review id.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns></returns>
        [Route("hotels/tripreview", Name = "DeleteTripReview")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Trip Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult DeleteTripReview(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var review = _hotelService.GetTripReviewById(id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }
            _hotelService.DeleteTripReview(id);
            return Ok();
        }
        #endregion Hotel Trip

        #region Hotel Laundry

        /// <summary>
        /// Returns list of available Laundry Services
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of available laundry services . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of available hotel laundry services</returns>
        [Route("hotels/{id}/laundry", Name = "GetHotelLaundryServices")]
        [HttpGet]

        [ResponseType(typeof(LaundryServiceDetailsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Hotel Laundry Services Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Laundry Services" })]
        public IHttpActionResult GetHotelLaundryServices(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryServices = _hotelService.GetHotelLaundryServices(id);

            if (laundryServices.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.LaundryService_NotFound } });

            var laundryDTO = Mapper.Map<IEnumerable<Laundry>, List<LaundryServiceDetailsDTO>>(laundryServices);

            return Content(HttpStatusCode.OK, laundryDTO);
        }

        /// <summary>
        /// Returns list of available laundry Details
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of available laundry Details . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Laundry Id</param>
        /// <returns>Details of available hotel laundry details</returns>
        [Route("hotels/laundry_service/{id}", Name = "GetLaundryServiceById")]
        [HttpGet]
        [ResponseType(typeof(LaundryDetailsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Hotel Laundry Services Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Laundry Services" })]
        public IHttpActionResult GetLaundryServiceById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryDetails = _hotelService.GetHotelLaundryDetailsByLaundryId(id);

            if (laundryDetails.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.LaundryDetails_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = laundryDetails.Select(s => s.Laundry.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var laundryDetailsDTO = Mapper.Map<IEnumerable<LaundryDetail>, List<LaundryDetailsDTO>>(laundryDetails);
            laundryDetailsDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
            {
                laundryDetailsDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                laundryDetailsDTO.Select(s => { s.LaundryDetailAdditional.Select(i => { i.LaundryAdditionalGroup.LaundryAdditionalElement.Select(k => { k.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(k.Price * baseCurrencyRate), 2); return k; }).ToList(); return i; }).ToList(); return s; }).ToList();
            }
            return Content(HttpStatusCode.OK, laundryDetailsDTO);
        }


        //Laundry reviews

        /// <summary>
        /// Returns laundry review by id 
        /// </summary> 
        /// <remarks>
        /// Get laundry review by review id . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Laundry review Id</param>
        /// <returns>Details of Laundry reviews</returns>
        [Route("hotels/laundryReview/{id}", Name = "GetLaundryReviewById")]
        [HttpGet]

        [ResponseType(typeof(LaundryReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Laundry Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Laundry Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetLaundryReview(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryReviews = _hotelService.GetLaundryReviewById(id);
            if (laundryReviews == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            if (laundryReviews.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryReviewsDTO = Mapper.Map<LaundryReview, LaundryReviewsDTO>(laundryReviews);
            return Content(HttpStatusCode.OK, laundryReviewsDTO);
        }



        /// <summary>
        /// Returns list of laundry reviews
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of laundry reviews. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Laundry Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Returns reviews of particular Laundry</returns>
        [Route("hotels/Laundry/{id}/laundryreview", Name = "GetLaundryReviews")]
        [HttpGet]

        [ResponseType(typeof(List<LaundryReviewsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Laundry Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Laundry Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetLaundryReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryReviews = _hotelService.GetLaundryReviews(id, page.Value, limit.Value, out total);
            if (laundryReviews == null || laundryReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var laundryReviewsDTO = Mapper.Map<IEnumerable<LaundryReview>, List<LaundryReviewsDTO>>(laundryReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = laundryReviewsDTO });
        }

        /// <summary>
        /// Deletes Laundry review
        /// </summary>
        /// <remarks>
        /// Delete Laundry review details based on review id.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns></returns>
        [Route("hotels/laundryreview", Name = "DeleteLaundryReview")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Laundry Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult DeleteLaundryReview(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var review = _hotelService.GetLaundryReviewById(id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }
            _hotelService.DeleteLaundryReview(id);
            return Ok();
        }

        /// <summary>
        /// Add new Laundry reviews
        /// </summary>
        /// <remarks>
        /// Add new laundry reviews
        /// </remarks>
        /// <param name="laundryReviewsDTO">Laundry Review Details</param>
        /// <returns>Returns newly created laundry review details</returns>
        [HttpPost]
        [Route("hotels/laundryreview", Name = "CreateLaundryReview")]

        [ResponseType(typeof(LaundryReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Laundry Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateLaundryReview(CreateLaundryReviewDTO laundryReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<LaundryReview>(laundryReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateLaundryReviews(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            return Created(Url.Link("GetLaundryReviewById", new { id = entity.Id }), Mapper.Map<LaundryReviewsDTO>(entity));
        }

        #endregion Hotel Laundry

        #region Laundry Cart

        /// <summary>
        /// Returns the Laundry Cart details of specified customer
        /// </summary>
        /// <remarks>
        /// Returs the Laundry Cart of specified customer
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Returns Laundry Cart Details</returns>
        [HttpGet]
        [Route("hotels/{id}/laundryCart", Name = "GetLaundryCart")]

        [ResponseType(typeof(LaundryCartDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Laundry Cart Not Available for hotel")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Laundry Cart" })]
        public IHttpActionResult GetLaundryCart(int id)
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryCart = _hotelService.GetCustomerLaundryCart(customer.Id, id);

            if (laundryCart == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cart_IsEmpty } });

            laundryCart.LaundryCartItems = laundryCart.LaundryCartItems.OrderBy(c => c.ScheduledDate).ToList();
            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = laundryCart.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var laundryCartDTO = Mapper.Map<LaundryCart, LaundryCartDTO>(laundryCart);
            laundryCartDTO.PreferredCurrencySymbol = customer.Currency.Symbol;

            if (baseCurrencyRate != 0)
            {
                laundryCartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(laundryCartDTO.Total * baseCurrencyRate), 2);
                laundryCartDTO.LaundryCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                laundryCartDTO.LaundryCartItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();

                laundryCartDTO.LaundryCartItems.Select(s => { s.LaundryCartItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
            }

            return Content(HttpStatusCode.OK, laundryCartDTO);
        }


        /// <summary>
        /// Creates new Laundry cart /  basket
        /// </summary>
        /// <remarks>
        /// Creates customer Laundry cart /  basket
        /// </remarks>
        /// <param name="createLaundryCartDTO">Laundry Details in Request body</param>
        /// <param name="flush">Flushes old cart and creates new one</param>
        /// <returns>Returns newly created Laundry details in response</returns>

        [Route("hotel/Laundry/Cart")]
        [HttpPost]
        [ResponseType(typeof(LaundryCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error or Cart Already exists")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "Cart Created")]
        [SwaggerOperation(Tags = new[] { "Laundry Cart" })]
        public IHttpActionResult CreateLaundryCart(CreateLaundryCartDTO createLaundryCartDTO, bool flush = false)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<LaundryCart>(createLaundryCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateLaundryCart(entity, flush);
                var hotelID = entity.Room.HotelId;
                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<LaundryCart, LaundryCartDTO>(entity);
                cartDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    cartDTO.LaundryCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                    cartDTO.LaundryCartItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();

                    cartDTO.LaundryCartItems.Select(s => { s.LaundryCartItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
                }
                return Created(Url.Link("GetLaundryCart", new { id = hotelID }), new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing Laundry detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Updates laundry cart /  basket
        /// </summary>
        /// <remarks>
        /// Updates customer laundry cart. Ex To update qty just send new quantity of item.
        /// </remarks>
        /// <param name="updateLaundryCartDTO">Just specify the new properties</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("hotel/Laundry/cart")]
        [HttpPut]
        [ResponseType(typeof(LaundryCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Laundry Cart Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Laundry Cart" })]
        public IHttpActionResult UpdateLaundryCart(UpdateLaundryCartDTO updateLaundryCartDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<LaundryCart>(updateLaundryCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.UpdateLaundryCart(authenticatedCustomer.Id, entity);
                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<LaundryCart, LaundryCartDTO>(entity);
                cartDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    cartDTO.LaundryCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                    cartDTO.LaundryCartItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();

                    cartDTO.LaundryCartItems.Select(s => { s.LaundryCartItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
                }
                return Content(HttpStatusCode.OK, new { status = true, data = cartDTO });
            }
            catch (InvalidOperationException cartNotFoundEx)
            {
                return Content(HttpStatusCode.NotFound, new { messages = cartNotFoundEx.Message });
            }
            catch (NegativeItemQtyException negativeQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Negative_Spa_Qty } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing laundry detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = arEx.Message });
            }
        }

        /// <summary>
        /// Allows to upload new laundry item image (Note: Please test this route on Postman)
        /// </summary>
        /// <remarks>
        /// Uploads customer new laundry cart item images.
        /// </remarks>
        /// <param name="roomId">Room Id</param>
        /// <param name="laundryDetailId">laundry Detail Id</param>
        /// <returns>Returns success</returns>

        [Route("hotel/Laundry/cart/itemImages")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerOperation(Tags = new[] { "Laundry Cart" })]
        public IHttpActionResult UploadLaundryCartItemImages(int roomId, int laundryDetailId)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Content(HttpStatusCode.UnsupportedMediaType, new { messages = new string[] { Resources.Invalid_MediaType } });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            // Check for any uploaded file 
            if (HttpContext.Current.Request.Files.Count > 0)
            {
                //Loop through uploaded files  
                for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
                {
                    HttpPostedFile uploadedFile = HttpContext.Current.Request.Files[i];
                    if (uploadedFile.ContentLength <= 2000000)
                    {
                        var allowedExtensions = new[] { ".jpg", ".png" };
                        var filename = Path.GetFileName(uploadedFile.FileName);
                        var ext = Path.GetExtension(uploadedFile.FileName);
                        if (allowedExtensions.Contains(ext))
                        {
                            try
                            {
                                var imgPath = "~/Images/Customers/" + authenticatedCustomer.Id.ToString() + "/Room/" + roomId.ToString() + "/Laundry/" + laundryDetailId.ToString() + "/";
                                string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
                                if (!Directory.Exists(FolderPathOnServer))
                                {
                                    Directory.CreateDirectory(FolderPathOnServer);
                                }
                                var path = Path.Combine(FolderPathOnServer, filename);
                                uploadedFile.SaveAs(path);
                            }
                            catch (Exception ex)
                            {
                                return Content(HttpStatusCode.BadRequest, new { messages = ex.Message });
                            }
                        }
                        else
                        {
                            return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_ImageExtension + " for " + filename } });
                        }
                    }
                    else
                    {
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.ImageUploadSizeExceeded + " for " + uploadedFile.FileName } });
                    }
                }
                return Ok(HttpStatusCode.Created);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Req_Image } });
            }
        }

        #endregion Laundry Cart

        #region Laundry Order

        /// <summary>
        /// Returs the Laundry Order details
        /// </summary>
        /// <remarks>
        /// Returs the Laundry Order details
        /// </remarks>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns Laundry Order Details</returns>
        [HttpGet]
        [Route("hotel/Laundry/order", Name = "GetLaundryOrder")]

        [ResponseType(typeof(LaundryOrderDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Laundry Order" })]
        public IHttpActionResult GetLaundryOrder(int orderId)
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetLaundryOrderById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.LaundryOrder_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            order.LaundryOrderItems = order.LaundryOrderItems.OrderBy(o => o.ScheduledDate).ToList();

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = order.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var laundryOrderDTO = Mapper.Map<LaundryOrder, LaundryOrderDTO>(order);
            laundryOrderDTO.PreferredCurrencySymbol = customer.Currency.Symbol;
            if (baseCurrencyRate != 0)
            {
                laundryOrderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(laundryOrderDTO.OrderTotal * baseCurrencyRate), 2);
                laundryOrderDTO.LaundryOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                laundryOrderDTO.LaundryOrderItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();

                laundryOrderDTO.LaundryOrderItems.Select(s => { s.LaundryOrderItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
            }
            laundryOrderDTO.ReceiptNumber = "#" + order.Id;
            SpaOrderStatus ordrStatus = (SpaOrderStatus)order.OrderStatus;
            laundryOrderDTO.OrderStatus = ordrStatus.ToString();

            return Content(HttpStatusCode.OK, laundryOrderDTO);
        }

        /// <summary>
        /// Place order for laundry items in basket
        /// </summary>
        /// <remarks>
        /// Place order for cart laundry items
        /// </remarks>
        /// <param name="cardId"> select customer Card</param>
        /// <param name="roomId">Room Id</param>
        /// <returns>Returns details of newly created laundry order</returns>

        [Route("hotel/Laundry/order")]
        [HttpPost]
        [ResponseType(typeof(LaundryOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Laundry Order" })]
        public IHttpActionResult PlaceLaundryOrderFromCart(int cardId, int roomId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            LaundryOrder entity;
            try
            {
                entity = _hotelService.CreateLaundryOrderFromCart(authenticatedCustomer.Id, cardId, roomId);

                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var orderDTO = Mapper.Map<LaundryOrder, LaundryOrderDTO>(entity);
                orderDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    orderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(orderDTO.OrderTotal * baseCurrencyRate), 2);
                    orderDTO.LaundryOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                    orderDTO.LaundryOrderItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();

                    orderDTO.LaundryOrderItems.Select(s => { s.LaundryOrderItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
                }

                orderDTO.ReceiptNumber = "#" + entity.Id;
                SpaOrderStatus ordrStatus = (SpaOrderStatus)entity.OrderStatus;
                orderDTO.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = orderDTO });
            }
            catch (CartEmptyException empty)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Cart_Empty } });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { invalidDateTime.Message } });
            }
            catch (CardSpendingLimitException limitEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_Card_SpendingLimit } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing laundry item id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Create new reviews for Laundry order
        /// </summary>
        /// <remarks>
        /// Adds new reviews for laundry order in the backend
        /// </remarks>
        /// <param name="laundryOrderReviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of laundry reviews</returns>

        [Route("hotels/laundryOrder/Review")]
        [HttpPost]
        [ResponseType(typeof(LaundryOrderReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Laundry Order" })]
        public IHttpActionResult CreateLaundryOrderReview(CreateLaundryOrderReviewDTO laundryOrderReviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetLaundryOrderById(laundryOrderReviewDTO.LaundryOrderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.LaundryOrder_NotFound } });

            if (order.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var entity = Mapper.Map<LaundryOrderReview>(laundryOrderReviewDTO);
            entity = _hotelService.CreateLaundryOrderReview(entity);
            var orderReviewsDTO = Mapper.Map<LaundryOrderReviewDTO>(entity);

            return Content(HttpStatusCode.OK, orderReviewsDTO);
        }


        /// <summary>
        /// Returns list of laundry order reviews
        /// </summary>
        /// <remarks>
        /// Get list of laundry order reviews.
        /// </remarks>
        /// <param name="id">Laundry Order Id</param>
        /// <returns>List of laundry order Reviews</returns>

        [Route("hotels/laundryOrder/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<LaundryOrderReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Laundry Order" })]

        public IHttpActionResult GetLaundryOrderReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var laundryOrder = _hotelService.GetLaundryOrderById(id);
            if (laundryOrder == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.LaundryOrder_NotFound } });

            if (laundryOrder.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var orderReviews = _hotelService.GetLaundryOrderReviews(id);
            if (orderReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var orderReviewDTO = Mapper.Map<IEnumerable<LaundryOrderReview>, List<LaundryOrderReviewDTO>>(orderReviews);
            return Content(HttpStatusCode.OK, orderReviewDTO);
        }

        #endregion Laundry Order

        #region Hotel Taxis
        /// <summary>
        /// Returns list of taxis available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of taxis available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of taxi services</returns>
        [Route("hotels/{id}/taxi", Name = "GetHotelTaxiDetails")]
        [HttpGet]

        [ResponseType(typeof(TaxiDetailDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Taxi Service Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelTaxiDetails(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiDetail = _hotelService.GetHotelTaxiDetails(id);
            if (taxiDetail == null || taxiDetail.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Taxi_NotFound } });

            var taxiDetailDTO = Mapper.Map<IEnumerable<TaxiDetail>, List<TaxiDetailDTO>>(taxiDetail);
            return Content(HttpStatusCode.OK, taxiDetailDTO);
        }

        /// <summary>
        /// Returns taxi detail
        /// </summary> 
        /// <remarks>
        /// Get a detailed  of taxi available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Taxi Id</param>
        /// <returns>Details of taxi</returns>
        [Route("hotels/taxi/{id}", Name = "GetHotelTaxiDetailById")]
        [HttpGet]

        [ResponseType(typeof(TaxiDetailDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No taxi Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelTaxiDetailById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiDetail = _hotelService.GetHotelTaxiDetailById(id);
            if (taxiDetail == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Taxi_NotFound } });

            var taxiDetailDTO = Mapper.Map<TaxiDetail, TaxiDetailDTO>(taxiDetail);
            return Content(HttpStatusCode.OK, taxiDetailDTO);
        }

        /// <summary>
        /// Returns list of predefine taxi destaination available in hotel
        /// </summary> 
        /// <remarks>
        /// Returns list of predefine taxi destaination available in hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of taxi destination</returns>
        [Route("hotels/{id}/taxi_destination", Name = "GetHotelTaxiDestinations")]
        [HttpGet]

        [ResponseType(typeof(TaxiDestinationDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Taxi destination not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelTaxiDestinations(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiDestination = _hotelService.GetHotelTaxiDestination(id);
            if (taxiDestination == null || taxiDestination.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TaxiDest_NotFound } });

            var taxiDestinationDTO = Mapper.Map<IEnumerable<TaxiDestination>, List<TaxiDestinationDTO>>(taxiDestination);
            return Content(HttpStatusCode.OK, taxiDestinationDTO);
        }

        /// <summary>
        /// Returns  Taxi booking details by id 
        /// </summary> 
        /// <remarks>
        /// Get Taxi booking details by booking id . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Taxi Booking Id</param>
        /// <returns>Details of Taxi booking</returns>
        [Route("hotels/taxiBooking/{id}", Name = "GetHotelTaxiBooking")]
        [HttpGet]

        [ResponseType(typeof(TaxiBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "No Taxi Booking Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelTaxiBooking(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiBooking = _hotelService.GetHotelTaxiBooking(id);
            if (taxiBooking == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TaxiBooking_NotFound } });

            if (taxiBooking.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiBookingDTO = Mapper.Map<TaxiBooking, TaxiBookingDTO>(taxiBooking);
            TaxiBookingStatus ordrStatus = (TaxiBookingStatus)taxiBooking.OrderStatus;
            taxiBookingDTO.OrderStatus = ordrStatus.ToString();
            return Content(HttpStatusCode.OK, taxiBookingDTO);
        }

        /// <summary>
        /// Creates Hotel Taxi bookings
        /// </summary>
        /// <remarks>
        /// Create Taxi bookings
        /// </remarks>
        /// <param name="TaxiBookingDTO">Taxi booking Details</param>
        /// <returns>Returns newly created Taxi booking details</returns>
        [HttpPost]
        [Route("hotels/taxiBooking", Name = "CreateTaxiBooking")]

        [ResponseType(typeof(TaxiBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi not available for this scheduled date time")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        public IHttpActionResult CreateTaxiBooking(CreateTaxiBookingDTO TaxiBookingDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<TaxiBooking>(TaxiBookingDTO);
            entity.CustomerId = authenticatedCustomer.Id;

            try
            {
                entity = _hotelService.CreateHotelTaxiBooking(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            var result = Mapper.Map<TaxiBooking, TaxiBookingDTO>(entity);
            TaxiBookingStatus ordrStatus = (TaxiBookingStatus)entity.OrderStatus;
            result.OrderStatus = ordrStatus.ToString();
            return Content(HttpStatusCode.Created, new { status = true, data = result });
        }


        /// <summary>
        /// Schedule hotel taxi Booking
        /// </summary>
        /// <remarks>
        /// Schedule hotel taxi Booking
        /// </remarks>
        /// <param name="taxiBookingId">Enter Taxi bookingId</param>
        /// <param name="scheduleDate">Schedule Datetime</param>
        /// <returns>Returns details of scheduled taxi Booking</returns>
        [HttpPut]
        [Route("hotel/taxiBooking_schedule", Name = "ScheduleTaxiBooking")]

        [ResponseType(typeof(TaxiBookingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi not available between this time")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        public IHttpActionResult ScheduleTaxiBooking(int taxiBookingId, DateTime scheduleDate)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            TaxiBooking entity;
            try
            {
                entity = _hotelService.ScheduleTaxiBooking(authenticatedCustomer.Id, taxiBookingId, scheduleDate);
            }
            catch (BookingNotFoundException bookingNotFound)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { bookingNotFound.Message } });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { invalidDateTime.Message } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            var result = Mapper.Map<TaxiBooking, TaxiBookingDTO>(entity);

            return Content(HttpStatusCode.Created, new { status = true, data = result });
        }


        /// <summary>
        /// Returns all taxi bookings place by customer
        /// </summary>
        /// <remarks>
        /// Returns all taxi bookings place by customer
        /// </remarks>
        /// <returns>Returns all taxi bookings place by customer</returns>
        [HttpGet]
        [Route("hotels/taxiBooking", Name = "GetAllTaxiBookings")]

        [ResponseType(typeof(List<TaxiBookingDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi Booking Not Found ")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        public IHttpActionResult GetAllTaxiBookings()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiBooking = _hotelService.GetAllTaxiBookingByCustomer(customer.Id);
            if (taxiBooking == null || taxiBooking.Count() <= 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.TaxiBooking_NotFound } });

            var taxiBookingDTO = Mapper.Map<IEnumerable<TaxiBooking>, List<TaxiBookingDTO>>(taxiBooking);
            foreach (var eachBooking in taxiBookingDTO)
            {
                TaxiBookingStatus ordrStatus = (TaxiBookingStatus)Convert.ToInt32(eachBooking.OrderStatus);
                eachBooking.OrderStatus = ordrStatus.ToString();
            }
            return Content(HttpStatusCode.OK, taxiBookingDTO);
        }

        //Taxi Booking Reviews

        /// <summary>
        /// Create new reviews for taxi bookings
        /// </summary>
        /// <remarks>
        /// Adds new reviews for taxi booking in the backend
        /// </remarks>
        /// <param name="orderReviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of newly createsd taxi booking review</returns>

        [Route("hotels/taxiBooking/Review")]
        [HttpPost]
        [ResponseType(typeof(TaxiBookingReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        public IHttpActionResult CreateTaxiBookingReview(CreateTaxiBookingReviewDTO orderReviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var taxiBooking = _hotelService.GetHotelTaxiBooking(orderReviewDTO.TaxiBookingId);
            if (taxiBooking == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TaxiBooking_NotFound } });
            if (taxiBooking.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var entity = Mapper.Map<TaxiBookingReview>(orderReviewDTO);
            entity = _hotelService.CreateTaxiBookingReview(entity);
            var taxiBookingReviewsDTO = Mapper.Map<TaxiBookingReviewDTO>(entity);

            return Content(HttpStatusCode.OK, taxiBookingReviewsDTO);
        }

        /// <summary>
        /// Returns list of taxi booking reviews
        /// </summary>
        /// <remarks>
        /// Get list of taxi booking reviews.
        /// </remarks>
        /// <param name="id">Taxi Booking Id</param>
        /// <returns>List of taxi booking Reviews</returns>
        /// 

        [Route("hotels/taxiBooking/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<TaxiBookingReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]

        public IHttpActionResult GetTaxiBookingReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var taxiBooking = _hotelService.GetHotelTaxiBooking(id);
            if (taxiBooking == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TaxiBooking_NotFound } });
            if (taxiBooking.CustomerId != authenticatedCustomer.Id)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });
            }

            var bookingReviews = _hotelService.GetTaxiBookingReviewByBookingId(id);
            if (bookingReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var taxiBookingReviewDTO = Mapper.Map<IEnumerable<TaxiBookingReview>, List<TaxiBookingReviewDTO>>(bookingReviews);
            return Content(HttpStatusCode.OK, taxiBookingReviewDTO);
        }


        /// <summary>
        /// Deletes Taxi Booking
        /// </summary>
        /// <remarks>
        /// Delete taxi booking details based on taxi booking id.
        /// </remarks>
        /// <param name="id">Taxi Booking Id</param>
        /// <returns></returns>
        //[Route("hotels/taxiBooking/{id}", Name = "DeleteTaxiBooking")]
        //[HttpDelete]
        //[SwaggerResponse(HttpStatusCode.OK, "Success")]
        //[SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        //public IHttpActionResult DeleteTaxiBooking(int id)
        //{
        //    var taxiBooking = _hotelService.GetHotelTaxiBooking(id);
        //    if (taxiBooking == null)
        //    {
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.TaxiBooking_NotFound } });
        //    }
        //    _hotelService.DeleteTaxiBooking(id);
        //    return Ok();
        //}

        /// <summary>
        /// Updates Hotel Taxi bookings
        /// </summary>
        /// <remarks>
        /// Updates Taxi bookings
        /// </remarks>
        /// <param name="TaxiBookingDTO">Taxi booking Details</param>
        /// <returns>Returns newly updated Taxi booking details</returns>
        //[HttpPut]
        //[Route("hotel/taxiBooking", Name = "UpdateTaxiBooking")]

        //[ResponseType(typeof(TaxiBookingDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi not available between this time")]
        //[SwaggerOperation(Tags = new[] { "Hotel Taxi Services" })]
        //public IHttpActionResult UpdateTaxiBooking(UpdateTaxiBookingDTO TaxiBookingDTO)
        //{
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }

        //    var entity = Mapper.Map<TaxiBooking>(TaxiBookingDTO);
        //    entity.CustomerId = authenticatedCustomer.Id;

        //    try
        //    {
        //        entity = _hotelService.ModifyTaxiBooking(entity);
        //    }
        //    catch (ArgumentException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }
        //    catch (InvalidOperationException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }

        //    return Created(Url.Link("GetHotelTaxiBooking", new { id = entity.Id }), Mapper.Map<TaxiBookingDTO>(entity));
        //}

        // Taxi Reviews

        /// <summary>
        /// Returns list of taxi reviews
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of taxi reviews. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Taxi Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Returns reviews of particular taxi</returns>
        [Route("hotels/taxi/{id}/taxireview", Name = "GetTaxiReviews")]
        [HttpGet]

        [ResponseType(typeof(List<TaxiReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Taxi Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Taxi Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetTaxiReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var taxiReviews = _hotelService.GetTaxiReviews(id, page.Value, limit.Value, out total);
            if (taxiReviews == null || taxiReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var taxiReviewsDTO = Mapper.Map<IEnumerable<TaxiReview>, List<TaxiReviewDTO>>(taxiReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = taxiReviewsDTO });
        }

        /// <summary>
        /// Returns taxi Review by Id
        /// </summary>
        /// <remarks>
        /// Get taxi Review by Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Returns taxi Review by reviewId</returns>
        [HttpGet]
        [Route("hotels/taxireview/{id}", Name = "GetTaxiReviewById")]

        [ResponseType(typeof(TaxiReviewDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi review not found")]
        [SwaggerOperation(Tags = new[] { "Taxi Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetTaxiReviewById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var review = _hotelService.GetTaxiReviewById(id);

            if (review == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewDTO = Mapper.Map<TaxiReviewDTO>(review);

            return Content(HttpStatusCode.OK, reviewDTO);
        }

        /// <summary>
        /// Add new taxi reviews
        /// </summary>
        /// <remarks>
        /// Add new taxi reviews
        /// </remarks>
        /// <param name="taxiReviewsDTO">Taxi Review Details</param>
        /// <returns>Returns newly created taxi review details</returns>
        [HttpPost]
        [Route("hotels/taxireview", Name = "CreateTaxiReview")]

        [ResponseType(typeof(TaxiReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Taxi Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateTaxiReview(CreateTaxiReviewDTO taxiReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<TaxiReview>(taxiReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateTaxiReviews(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            return Created(Url.Link("GetTaxiReviewById", new { id = entity.Id }), Mapper.Map<TaxiReviewDTO>(entity));
        }


        /// <summary>
        /// Deletes taxi review
        /// </summary>
        /// <remarks>
        /// Delete taxi review details based on review id.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns></returns>
        [Route("hotels/taxireview", Name = "DeleteTaxiReview")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Taxi Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult DeleteTaxiReview(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var review = _hotelService.GetTaxiReviewById(id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }
            _hotelService.DeleteTaxiReview(id);
            return Ok();
        }
        #endregion Hotel Taxis

        #region Customer HotelRoom
        /// <summary>
        /// Returns room details available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detail of room available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="roomNumber">Room Number</param>
        /// <returns>Details of hotel room</returns>
        [Route("hotels/{id}/room", Name = "GetHotelRooms")]
        [HttpGet]
        [ResponseType(typeof(RoomDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel room not Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelRooms(int id, string roomNumber)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelRoom = _hotelService.GetAllHotelRooomsById(id, roomNumber);
            if (hotelRoom == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Rooms_NotFound } });

            var roomDTO = Mapper.Map<Room, RoomDTO>(hotelRoom);
            return Content(HttpStatusCode.OK, roomDTO);
        }

        /// <summary>
        /// Returns hotel customer room details
        /// </summary> 
        /// <remarks>
        /// Get hotel customer room details. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of customer hotel room</returns>
        [Route("hotels/{id}/customerRoom", Name = "GetHotelRoomDetailsById")]
        [HttpGet]
        [ResponseType(typeof(CustomerHotelRoomDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel room not found")]
        [SwaggerOperation(Tags = new[] { "Customer HotelRoom" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetCustomerHotelRoomDetails(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelRoomDetails = _hotelService.GetCustomerRooomDetailsById(id, authenticatedCustomer.Id);
            if (hotelRoomDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Rooms_NotFound } });

            var roomDetailsDTO = Mapper.Map<CustomerHotelRoom, CustomerHotelRoomDTO>(hotelRoomDetails);
            return Content(HttpStatusCode.OK, roomDetailsDTO);
        }

        /// <summary>
        /// Returns hotel customer room details
        /// </summary> 
        /// <remarks>
        /// Create request to save the RoomId for Customer after scan the QRcode of the Room. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Room Id</param>
        /// <returns>Details of hotel customer room</returns>
        [Route("hotels/customerRoom/{id}")]
        [HttpPost]
        [ResponseType(typeof(CustomerHotelRoomDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel room not found")]
        [SwaggerOperation(Tags = new[] { "Customer HotelRoom" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateCustomerRooom(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (authenticatedCustomer.CustomerHotelRooms.Select(r => r.IsActive == true).LastOrDefault())
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { "CUSTOMER_ALREADY_ACTIVE_IN_ANOTHER_ROOM" } });

            CustomerHotelRoom custHotelRoom;
            try
            {
                custHotelRoom = _hotelService.CreateCustomerHotelRooom(authenticatedCustomer.Id, id);

                var result = Mapper.Map<CustomerHotelRoom, CustomerHotelRoomDTO>(custHotelRoom);
                return Content(HttpStatusCode.Created, new { status = true, data = result });
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        /// <summary>
        /// Returns list of hotel room reviews
        /// </summary>
        /// <remarks>
        /// Get list of the hotel room reviews.
        /// </remarks>
        /// <param name="id">Room Id</param>
        /// <returns>List of hotel room Reviews</returns>
        [Route("hotels/room/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<RoomReviewsDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Room reviews not found")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult GetRoomReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomReviews = _hotelService.GetReviewsByRoomId(id);
            if (roomReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var roomReviewDTO = Mapper.Map<IEnumerable<RoomReview>, List<RoomReviewsDTO>>(roomReviews);
            return Content(HttpStatusCode.OK, roomReviewDTO);
        }

        /// <summary>
        /// Create new hotel room Reviews
        /// </summary>
        /// <remarks>
        /// Adds new hotel room reviews
        /// </remarks>
        /// <param name="reviewDTO">Review Details in Request body</param>
        /// <returns>Returns newly created room review</returns>

        [Route("hotels/roomReviews")]
        [HttpPost]
        [ResponseType(typeof(RoomReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateRoomReview(CreateRoomReviewDTO reviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var entity = Mapper.Map<RoomReview>(reviewDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateRoomReview(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            var roomReviewDTO = Mapper.Map<RoomReviewsDTO>(entity);
            return Content(HttpStatusCode.OK, roomReviewDTO);
        }

        /// <summary>
        /// Returns  room booking details by id 
        /// </summary> 
        /// <remarks>
        /// Get hotel room booking details by booking id . Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Room Booking Id</param>
        /// <returns>Details of room booking</returns>
        [Route("hotels/room_booking/{id}", Name = "GetHotelRoomBooking")]
        [HttpGet]

        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel room booking not Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetHotelRoomBooking(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomBooking = _hotelService.GetHotelRoomBookingByID(id);
            if (roomBooking == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.RoomBooking_NotFound } });

            if (roomBooking.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomBookingkingDTO = Mapper.Map<HotelBookingsDTO>(roomBooking);
            return Content(HttpStatusCode.OK, roomBookingkingDTO);
        }

        /// <summary>
        /// Create hotel room booking
        /// </summary>
        /// <remarks>
        /// Adds new hotel room booking. Authorization required to access this method.
        /// </remarks>
        /// <param name="hotelBookingDTO">Hotel Room Booking Details</param>
        /// <returns>Returns newly created room bookings details</returns>
        [HttpPost]
        [Route("hotels/room_booking")]
        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateHotelBooking(CreateHotelBookingDTO hotelBookingDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            if (hotelBookingDTO.From < DateTime.UtcNow || hotelBookingDTO.To < DateTime.UtcNow)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Booking Date shouldn't be less than today date" } });
            }

            if (hotelBookingDTO.From > hotelBookingDTO.To)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "From date must be less than To date" } });
            }

            var entity = Mapper.Map<CustomerHotelBooking>(hotelBookingDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateHotelBookings(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            return Created(Url.Link("GetHotelRoomBooking", new { id = entity.Id }), Mapper.Map<HotelBookingsDTO>(entity));
        }

        /// <summary>
        /// Update hotel room booking
        /// </summary>
        /// <remarks>
        /// Update hotel room booking. Authorization required to access this method.
        /// </remarks>
        /// <param name="hotelBookingDTO">Hotel Room Booking Details</param>
        /// <returns>Returns newly updated room bookings details</returns>
        [HttpPut]
        [Route("hotels/room_booking")]
        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult UpdateHotelBooking(ModifyHotelBookingDTO hotelBookingDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            if (hotelBookingDTO.From < DateTime.UtcNow || hotelBookingDTO.To < DateTime.UtcNow)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "Booking Date shouldn't be less than today date" } });
            }

            if (hotelBookingDTO.From > hotelBookingDTO.To)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "From date must be less than To date" } });
            }

            var entity = Mapper.Map<CustomerHotelBooking>(hotelBookingDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.ModifyHotelBookings(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (InvalidOperationException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            return Created(Url.Link("GetHotelRoomBooking", new { id = entity.Id }), Mapper.Map<HotelBookingsDTO>(entity));
        }


        /// <summary>
        /// Returns all room bookings place by customer
        /// </summary>
        /// <remarks>
        /// Returns all room bookings place by customer
        /// </remarks>
        /// <returns>Returns all room bookings place by customer</returns>
        [HttpGet]
        [Route("hotels/room_booking")]

        [ResponseType(typeof(List<HotelBookingsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Room Booking Not Found ")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetAllHotelRoomBookings()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomBooking = _hotelService.GetAllRoomBookingByCustomer(customer.Id);
            if (roomBooking == null || roomBooking.Count() <= 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.RoomBooking_NotFound } });

            var roomBookingDTO = Mapper.Map<IEnumerable<CustomerHotelBooking>, List<HotelBookingsDTO>>(roomBooking);
            return Content(HttpStatusCode.OK, roomBookingDTO);
        }

        /// <summary>
        /// Deletes hotel room Booking
        /// </summary>
        /// <remarks>
        /// Delete hotel room booking details based on room booking id.
        /// </remarks>
        /// <param name="Id">Room Booking Id</param>
        /// <returns></returns>
        [Route("hotels/room_booking/{id}", Name = "DeleteRoomBooking")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteRoomBooking(int Id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomBooking = _hotelService.GetHotelRoomBookingByID(Id);
            if (roomBooking == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.RoomBooking_NotFound } });
            }
            _hotelService.DeleteRoomBooking(Id);
            return Ok();
        }

        /// <summary>
        /// Returns list of Housekeeping facilities available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of Housekeeping facilities available in the hotel. Authorization required to access this method.
        /// </remarks>       
        /// <returns>Details of Housekeeping facilities</returns>
        [Route("hotels/room/housekeeping", Name = "GetHotelHousekeepingFacilities")]
        [HttpGet]
        [ResponseType(typeof(GetHouseKeepingFacilitiesDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel Housekeeping facilities not Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetHotelHousekeepingFacilities()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelHouseKeeping = _hotelService.GetAllHousekeepingFacilities();
            if (hotelHouseKeeping == null || hotelHouseKeeping.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HouseKeepingfacilities_NotFound } });

            var housekeepingFacilitiesDTO = Mapper.Map<IEnumerable<HouseKeepingFacility>, List<GetHouseKeepingFacilitiesDTO>>(hotelHouseKeeping);
            return Content(HttpStatusCode.OK, housekeepingFacilitiesDTO);
        }

        /// <summary>
        /// Returns list of housekeeping facilities of room
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of housekeeping facilities of room. Authorization required to access this method.
        /// </remarks> 
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of room housekeeping facilities</returns>
        [Route("hotels/{id}/customerRoom/housekeeping", Name = "GetRoomHousekeepingFacilities")]
        [HttpGet]
        [ResponseType(typeof(CustomerHotelRoomInfoDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Hotel Room Housekeeping facilities not Found")]
        [SwaggerOperation(Tags = new[] { "Customer HotelRoom" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetRoomHousekeepingFacilities(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomHouseKeeping = _hotelService.GetCustomerRooomDetailsById(id, authenticatedCustomer.Id);
            if (roomHouseKeeping == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HouseKeepingfacilities_NotFound } });

            var roomHousekeepingFacDTO = Mapper.Map<CustomerHotelRoom, CustomerHotelRoomInfoDTO>(roomHouseKeeping);
            return Content(HttpStatusCode.OK, roomHousekeepingFacDTO);
        }

        /// <summary>
        /// Returns hotel house keeping information
        /// </summary> 
        /// <remarks>
        /// Get a hotel house keeping information. Authorization required to access this method.
        /// </remarks> 
        /// <param name="id">Hotel Id</param>
        /// <returns>Details of hotel house keeping information</returns>
        [Route("hotels/{id}/call_housekeeping", Name = "CallHotelHouseKeeping")]
        [HttpGet]
        [ResponseType(typeof(HotelHouseKeepingInfoDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Hotel Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Rooms" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CallHotelHouseKeeping(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotel = _hotelService.GetHotelById(id);
            if (hotel == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            var hotelHouseKeepingInfo = _hotelService.GetHotelHouseKeepingInfoByHotelId(id);
            if (hotelHouseKeepingInfo == null)
            {
                HotelHouseKeepingInfo emptyHouseKeepingInfo = new HotelHouseKeepingInfo();
                var hotelHouseKeepingInfoEmptyDTO = Mapper.Map<HotelHouseKeepingInfo, HotelHouseKeepingInfoDTO>(emptyHouseKeepingInfo);
                return Content(HttpStatusCode.OK, new { CallHouseKeeper = false, Result = hotelHouseKeepingInfoEmptyDTO });
            }
            var hotelHouseKeepingInfoDTO = Mapper.Map<HotelHouseKeepingInfo, HotelHouseKeepingInfoDTO>(hotelHouseKeepingInfo);
            return Content(HttpStatusCode.OK, new { CallHouseKeeper = true, Result = hotelHouseKeepingInfoDTO });
        }

        #endregion Customer HotelRoom

        #region hotel spa service review

        /// <summary>
        /// Returns list of spa service reviews available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of spa service reviews available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of spa service reviews</returns>
        [Route("hotels/{id}/spa_service_reviews", Name = "GetSpaServiceReviewsByHotelId")]
        [HttpGet]

        [ResponseType(typeof(List<HotelSPAServiceReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Spa Service reviews not Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetSpaServiceReviewsByHotelId(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaServiceReviews = _hotelService.GetSpaServiceReviewsByHotelId(id, page.Value, limit.Value, out total);
            if (spaServiceReviews == null || spaServiceReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var spaServiceReviewsDTO = Mapper.Map<IEnumerable<SpaServiceReview>, List<HotelSPAServiceReviewDTO>>(spaServiceReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceReviewsDTO });
        }

        /// <summary>
        /// Returns list of spa service reviews for the particular hotel service
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of spa service for particular hotel service. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Spa Service Detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>spa service reviews</returns>
        [Route("hotels/spa_service_details/{id}/reviews", Name = "GetHotelSpaServiceReviews")]
        [HttpGet]

        [ResponseType(typeof(List<HotelSPAServiceReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "No SPA Service reviews Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelSpaServiceReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var SPAserviceReviews = _hotelService.GetHotelSpaServiceReviews(id, page.Value, limit.Value, out total);
            if (SPAserviceReviews == null || SPAserviceReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var spaServiceReviewsDTO = Mapper.Map<IEnumerable<SpaServiceReview>, List<HotelSPAServiceReviewDTO>>(SPAserviceReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = spaServiceReviewsDTO });
        }

        /// <summary>
        /// Returns spa service Review by Id
        /// </summary>
        /// <remarks>
        /// Get a detailed of customer hotel spa service reviews using Review Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns>Hotel Spa Service Review</returns>
        [HttpGet]
        [Route("hotels/spa_service_review/{id}", Name = "GetHotelSPAServiceReviewById")]

        [ResponseType(typeof(HotelSPAServiceReviewDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Review Found")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelSpaServiceReviewById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var review = _hotelService.GetHotelSpaServiceReviewById(id);

            if (review == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var reviewDTO = Mapper.Map<HotelSPAServiceReviewDTO>(review);

            return Content(HttpStatusCode.OK, reviewDTO);
        }

        /// <summary>
        /// Creates new Hotel Spa Service review
        /// </summary>
        /// <remarks>
        /// Add new hotel SPA service review in the backend
        /// </remarks>
        /// <param name="hotelSPAServiceReviewsDTO">Hotel SPA service Review Details</param>
        /// <returns>Returns newly created hotel SPA service review details</returns>
        [HttpPost]
        [Route("hotels/spa_service_review", Name = "CreateHotelSpaServiceReview")]

        [ResponseType(typeof(HotelSPAServiceReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateHotelSpaServiceReview(CreateHotelSPAServiceReviewsDTO hotelSPAServiceReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var entity = Mapper.Map<SpaServiceReview>(hotelSPAServiceReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateHotelSpaServiceReview(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            return Created(Url.Link("GetHotelSPAServiceReviewById", new { id = entity.Id }), Mapper.Map<HotelSPAServiceReviewDTO>(entity));
        }

        /// <summary>
        /// Deletes hotel Spa service review
        /// </summary>
        /// <remarks>
        /// Delete hotel Spa service review details based on review id.
        /// </remarks>
        /// <param name="id">Review Id</param>
        /// <returns></returns>
        [Route("hotels/spa_service_review", Name = "DeleteHotelSpaServiceReview")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Spa Service Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult DeleteHotelSpaServiceReview(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var review = _hotelService.GetHotelSpaServiceReviewById(id);
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
            }
            _hotelService.DeleteHotelSpaServiceReview(id);
            return Ok();
        }

        #endregion hotel spa service review

        #region Housekeeping Cart

        /// <summary>
        /// Returns the Housekeeping Cart details of specified customer
        /// </summary>
        /// <remarks>
        /// Returs the Housekeeping Cart of specified customer
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Returns Housekeeping Cart Details</returns>
        [HttpGet]
        [Route("hotels/{id}/Housekeeping/Cart", Name = "GetHousekeepingCart")]

        [ResponseType(typeof(HousekeepingCartDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Housekeeping Cart Not Available for hotel")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Cart" })]
        public IHttpActionResult GetHousekeepingCart(int id)
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var housekeepingCart = _hotelService.GetCustomerHousekeepingCart(customer.Id, id);

            if (housekeepingCart == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cart_IsEmpty } });

            housekeepingCart.HousekeepingCartItems = housekeepingCart.HousekeepingCartItems.OrderBy(c => c.ScheduleDate).ToList();

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = housekeepingCart.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var housekeepingCartDTO = Mapper.Map<HousekeepingCart, HousekeepingCartDTO>(housekeepingCart);
            housekeepingCartDTO.PreferredCurrencySymbol = customer.Currency.Symbol;

            if (baseCurrencyRate != 0)
            {
                housekeepingCartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(housekeepingCartDTO.Total * baseCurrencyRate), 2);
                if (housekeepingCartDTO.HousekeepingCartItems != null && housekeepingCartDTO.HousekeepingCartItems.Count() > 0)
                {
                    housekeepingCartDTO.HousekeepingCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                    housekeepingCartDTO.HousekeepingCartItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                }
            }

            foreach (var cartItem in housekeepingCartDTO.HousekeepingCartItems)
            {
                var roomDetails = housekeepingCart.Room.RoomDetails.Where(i => i.HouseKeeping.Select(k => k.HouseKeepingFacility.HouseKeepingFacilityDetails.Any(l => cartItem.HouseKeepingFacilityDetail.Id == l.Id)).FirstOrDefault()).FirstOrDefault();
                var image = roomDetails.Image;
                if (image != null)
                    image = Url.Content("/Images/Hotel/Rooms/") + housekeepingCartDTO.RoomId.ToString() + "/RoomDetails/" + roomDetails.Id.ToString() + "/" + image.ToString();
                cartItem.HouseKeepingFacilityDetail.Image = image != null ? image : null;
            }

            return Content(HttpStatusCode.OK, housekeepingCartDTO);
        }

        /// <summary>
        /// Creates new Housekeeping cart /  basket
        /// </summary>
        /// <remarks>
        /// Creates customer Housekeeping cart /  basket
        /// </remarks>
        /// <param name="createHousekeepingCartDTO">Housekeeping Details in Request body</param>
        /// <param name="flush">Flushes old cart and creates new one</param>
        /// <returns>Returns newly created Housekeeping details in response</returns>

        [Route("hotel/Housekeeping/Cart")]
        [HttpPost]
        [ResponseType(typeof(HousekeepingCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error or Cart Already exists")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "Cart Created")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Cart" })]
        public IHttpActionResult CreateHousekeepingCart(CreateHousekeepingCartDTO createHousekeepingCartDTO, bool flush = false)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<HousekeepingCart>(createHousekeepingCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateHousekeepingCart(entity, flush);

                var hotelID = entity.Room.HotelId;
                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<HousekeepingCart, HousekeepingCartDTO>(entity);
                cartDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    if (cartDTO.HousekeepingCartItems != null && cartDTO.HousekeepingCartItems.Count() > 0)
                    {
                        cartDTO.HousekeepingCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                        cartDTO.HousekeepingCartItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                    }
                }

                foreach (var cartItem in cartDTO.HousekeepingCartItems)
                {
                    var roomDetails = entity.Room.RoomDetails.Where(i => i.HouseKeeping.Select(k => k.HouseKeepingFacility.HouseKeepingFacilityDetails.Any(l => cartItem.HouseKeepingFacilityDetail.Id == l.Id)).FirstOrDefault()).FirstOrDefault();
                    var image = roomDetails.Image;
                    if (image != null)
                        image = Url.Content("/Images/Hotel/Rooms/") + cartDTO.RoomId.ToString() + "/RoomDetails/" + roomDetails.Id.ToString() + "/" + image.ToString();
                    cartItem.HouseKeepingFacilityDetail.Image = image != null ? image : null;
                }

                return Created(Url.Link("GetHousekeepingCart", new { id = hotelID }), new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing Housingkeeping facility detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

        }

        /// <summary>
        /// Updates housekeeping cart /  basket
        /// </summary>
        /// <remarks>
        /// Updates customer housekeeping cart. Ex To update qty just send new quantity of item.
        /// </remarks>
        /// <param name="updateHousekeepingCartDTO">Just specify the new properties</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("hotel/Housekeeping/cart")]
        [HttpPut]
        [ResponseType(typeof(HousekeepingCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Housekeeping Cart Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Cart" })]
        public IHttpActionResult UpdateHousekeepingCart(UpdateHousekeepingCartDTO updateHousekeepingCartDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<HousekeepingCart>(updateHousekeepingCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.UpdateHousekeepingCart(authenticatedCustomer.Id, entity);
                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<HousekeepingCart, HousekeepingCartDTO>(entity);
                cartDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    if (cartDTO.HousekeepingCartItems != null && cartDTO.HousekeepingCartItems.Count() > 0)
                    {
                        cartDTO.HousekeepingCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                        cartDTO.HousekeepingCartItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                    }
                }

                foreach (var cartItem in cartDTO.HousekeepingCartItems)
                {
                    var roomDetails = entity.Room.RoomDetails.Where(i => i.HouseKeeping.Select(k => k.HouseKeepingFacility.HouseKeepingFacilityDetails.Any(l => cartItem.HouseKeepingFacilityDetail.Id == l.Id)).FirstOrDefault()).FirstOrDefault();
                    var image = roomDetails.Image;
                    if (image != null)
                        image = Url.Content("/Images/Hotel/Rooms/") + cartDTO.RoomId.ToString() + "/RoomDetails/" + roomDetails.Id.ToString() + "/" + image.ToString();
                    cartItem.HouseKeepingFacilityDetail.Image = image != null ? image : null;
                }

                return Content(HttpStatusCode.OK, new { status = true, data = cartDTO });
            }
            catch (InvalidOperationException cartNotFoundEx)
            {
                return Content(HttpStatusCode.NotFound, new { messages = cartNotFoundEx.Message });
            }
            catch (NegativeItemQtyException negativeQtyEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { negativeQtyEx.Message } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing Housingkeeping facility detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = arEx.Message });
            }
        }

        #endregion Housekeeping Cart

        #region Housekeeping Order

        /// <summary>
        /// Returs the Housekeeping Order details
        /// </summary>
        /// <remarks>
        /// Returs the Housekeeping Order details
        /// </remarks>
        /// <param name="orderId">Housekeeping Order Id</param>
        /// <returns>Returns Housekeeping Order Details</returns>
        [HttpGet]
        [Route("hotel/Housekeeping/order", Name = "GetHousekeepingOrder")]

        [ResponseType(typeof(HousekeepingOrderDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Order" })]
        public IHttpActionResult GetHousekeepingOrder(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetHousekeepingOrderById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HousekeepingOrder_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            order.HousekeepingOrderItems = order.HousekeepingOrderItems.OrderBy(o => o.ScheduleDate).ToList();
            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = order.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var housekeepingOrderDTO = Mapper.Map<HousekeepingOrder, HousekeepingOrderDTO>(order);
            if (baseCurrencyRate != 0)
            {
                housekeepingOrderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(housekeepingOrderDTO.OrderTotal * baseCurrencyRate), 2);
                if (housekeepingOrderDTO.HousekeepingOrderItems != null && housekeepingOrderDTO.HousekeepingOrderItems.Count() > 0)
                {
                    housekeepingOrderDTO.HousekeepingOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                    housekeepingOrderDTO.HousekeepingOrderItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                }
            }

            housekeepingOrderDTO.ReceiptNumber = "#" + order.Id;
            SpaOrderStatus ordrStatus = (SpaOrderStatus)order.OrderStatus;
            housekeepingOrderDTO.OrderStatus = ordrStatus.ToString();

            return Content(HttpStatusCode.OK, housekeepingOrderDTO);
        }

        /// <summary>
        /// Place order for housekeeping items in basket
        /// </summary>
        /// <remarks>
        /// Place order for cart housekeeping items
        /// </remarks>
        /// <param name="cardId"> select customer Card</param>
        /// <param name="roomId">Room Id</param>
        /// <returns>Returns details of newly created housekeeping order</returns>

        [Route("hotel/Housekeeping/order")]
        [HttpPost]
        [ResponseType(typeof(HousekeepingOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Order" })]
        public IHttpActionResult PlaceHousekeepingOrderFromCart(int cardId, int roomId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            HousekeepingOrder entity;
            try
            {
                entity = _hotelService.CreateHousekeepingOrderFromCart(authenticatedCustomer.Id, cardId, roomId);

                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var orderDTO = Mapper.Map<HousekeepingOrder, HousekeepingOrderDTO>(entity);
                orderDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                if (baseCurrencyRate != 0)
                {
                    orderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(orderDTO.OrderTotal * baseCurrencyRate), 2);
                    if (orderDTO.HousekeepingOrderItems != null && orderDTO.HousekeepingOrderItems.Count() > 0)
                    {
                        orderDTO.HousekeepingOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                        orderDTO.HousekeepingOrderItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                    }
                }

                orderDTO.ReceiptNumber = "#" + entity.Id;
                SpaOrderStatus ordrStatus = (SpaOrderStatus)entity.OrderStatus;
                orderDTO.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = orderDTO });
            }
            catch (CartEmptyException empty)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Cart_Empty } });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (InvalidScheduleDateTimeException invalidDateTime)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { invalidDateTime.Message } });
            }
            catch (CardSpendingLimitException limitEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_Card_SpendingLimit } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing housekeeping facility details id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        #endregion Housekeeping Order

        #region Hotel Services Reviews
        ///// <summary>
        ///// Returns hotel service Review by Id
        ///// </summary>
        ///// <remarks>
        ///// Get hotel service reviews using Review Id. Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">Review Id</param>
        ///// <returns>Returns Hotel service reviews</returns>
        //[HttpGet]
        //[Route("hotels/hotelServicesReviews/{id}", Name = "GetHotelServiceReviewById")]

        //[ResponseType(typeof(HotelServicesReviewsDTO))]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Review not Found")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = true)]

        //public IHttpActionResult GetHotelServiceReviewById(int id)
        //{
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var review = _hotelService.GetHotelServicesReviewsById(id);

        //    if (review == null)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

        //    var reviewDTO = Mapper.Map<HotelServicesReviewsDTO>(review);

        //    return Content(HttpStatusCode.OK, reviewDTO);
        //}

        ///// <summary>
        ///// Returns list of each service reviews 
        ///// </summary> 
        ///// <remarks>
        ///// Returns list of each service reviews. Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">Hotel Service Id</param>
        ///// <param name="page">Page Number</param>
        ///// <param name="limit">Page Size</param>
        ///// <returns>Returns list of each service reviews</returns>
        //[Route("hotels/hotelServicesReviews/hotelservice/{id}", Name = "GetEachHotelServiceReviews")]
        //[HttpGet]

        //[ResponseType(typeof(List<HotelServicesReviewsDTO>))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Service reviews not Found")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult GetEachHotelServiceReviews(int id, int? page = 1, int? limit = null)
        //{
        //    limit = limit ?? ToOrderConfigs.DefaultPageSize;
        //    int total;

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var serviceReviews = _hotelService.GetEachHotelServiceReviews(id, page.Value, limit.Value, out total);
        //    if (serviceReviews == null || serviceReviews.Count() < 1)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

        //    var serviceReviewsDTO = Mapper.Map<IEnumerable<HotelServicesReviews>, List<HotelServicesReviewsDTO>>(serviceReviews);

        //    var mod = total % limit.Value;
        //    var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
        //    return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = serviceReviewsDTO });
        //}

        ///// <summary>
        ///// Returns list of services reviews by hotel Id  
        ///// </summary> 
        ///// <remarks>
        ///// Returns list of services reviews by hotel Id  . Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">Hotel Id</param>
        ///// <param name="page">Page Number</param>
        ///// <param name="limit">Page Size</param>
        ///// <returns>Returns list of services reviews by hotel Id  </returns>
        //[Route("hotels/{id}/hotelServicesReviews", Name = "GetHotelAllServicesReviews")]
        //[HttpGet]

        //[ResponseType(typeof(List<HotelServicesReviewsDTO>))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Services reviews not Found")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult GetHotelAllServicesReviews(int id, int? page = 1, int? limit = null)
        //{
        //    limit = limit ?? ToOrderConfigs.DefaultPageSize;
        //    int total;

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var serviceReviews = _hotelService.GetHotelAllServicesReviews(id, page.Value, limit.Value, out total);
        //    if (serviceReviews == null || serviceReviews.Count() < 1)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

        //    var serviceReviewsDTO = Mapper.Map<IEnumerable<HotelServicesReviews>, List<HotelServicesReviewsDTO>>(serviceReviews);

        //    var mod = total % limit.Value;
        //    var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
        //    return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = serviceReviewsDTO });
        //}
        ///// <summary>
        ///// Creates new Hotel services reviews
        ///// </summary>
        ///// <remarks>
        ///// Add new hotel service review in the backend
        ///// </remarks>
        ///// <param name="hotelServicesReviewsDTO">Hotel service Review Details</param>
        ///// <returns>Returns newly created hotel service review details</returns>
        //[HttpPost]
        //[Route("hotels/hotelServicesReviews", Name = "CreateHotelServicesReview")]

        //[ResponseType(typeof(HotelServicesReviewsDTO))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult CreateHotelServicesReview(CreateHotelServicesReviewsDTO hotelServicesReviewsDTO)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        List<string> messages = new List<string>();

        //        foreach (var error in ModelState.Values)
        //        {
        //            var x = error.Errors.Select(e => e.ErrorMessage);
        //            messages.AddRange(x);
        //        }
        //        return Content(HttpStatusCode.BadRequest, new { messages = messages });
        //    }
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }
        //    var entity = Mapper.Map<HotelServicesReviews>(hotelServicesReviewsDTO);
        //    entity.CustomerId = authenticatedCustomer.Id;
        //    try
        //    {
        //        entity = _hotelService.CreateHotelServicesReviews(entity);
        //    }
        //    catch (ArgumentException arEx)
        //    {
        //        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
        //    }

        //    return Created(Url.Link("GetHotelServiceReviewById", new { id = entity.Id }), Mapper.Map<HotelServicesReviewsDTO>(entity));
        //}

        ///// <summary>
        ///// Deletes hotel services review
        ///// </summary>
        ///// <remarks>
        ///// Delete hotel services review details based on review id.
        ///// </remarks>
        ///// <param name="id">Review Id</param>
        ///// <returns></returns>
        //[Route("hotels/hotelServiceReview", Name = "DeleteHotelServicesReview")]
        //[HttpDelete]
        //[SwaggerResponse(HttpStatusCode.OK, "Success")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult DeleteHotelServicesReview(int id)
        //{
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }
        //    var review = _hotelService.GetHotelServicesReviewsById(id);
        //    if (review == null)
        //    {
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });
        //    }
        //    _hotelService.DeleteHotelServicesReview(id);
        //    return Ok();
        //}

        ///// <summary>
        ///// Returns list of all services reviews 
        ///// </summary> 
        ///// <remarks>
        ///// Returns list of all services reviews. Authorization required to access this method.
        ///// </remarks>
        ///// <param name="id">Hotel Id</param>
        ///// <param name="page">Page Number</param>
        ///// <param name="limit">Page Size</param>
        ///// <returns>Returns list of all services reviews</returns>
        //[Route("hotels/{id}/allServicesReviews", Name = "GetAllServicesReviews")]
        //[HttpGet]

        //[ResponseType(typeof(List<AllHotelServicesReviewsDTO>))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Service reviews not Found")]
        //[SwaggerOperation(Tags = new[] { "Hotel Services Review" })]
        //[ApiExplorerSettings(IgnoreApi = false)]
        //public IHttpActionResult GetAllServicesReviews(int id, int? page = 1, int? limit = null)
        //{
        //    limit = limit ?? ToOrderConfigs.DefaultPageSize;
        //    int total;

        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

        //    var serviceReviews = _hotelService.GetAllHotelServicesReviews(id, page.Value, limit.Value, out total);
        //    if (serviceReviews == null || serviceReviews.Count() < 1)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

        //    var serviceReviewsDTO = Mapper.Map<IEnumerable<VW_ServicesReviews>, List<AllHotelServicesReviewsDTO>>(serviceReviews);

        //    var mod = total % limit.Value;
        //    var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
        //    return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = serviceReviewsDTO });
        //}
        #endregion Hotel Services reviews

        #region Excursion Search
        /// <summary>
        /// Search hotel excursion details based on different criterias 
        /// </summary>
        /// <remarks>
        /// Search hotel excursion details based on different criterias, if found than returns 200 status with "success" meassage
        /// </remarks>
        /// <param name="hotelId">Hotel Id</param>
        /// <param name="searchText">Enter search keyword</param>
        /// <param name="Ids">Hotel Excursion Ids</param>
        /// <param name="price_min">Minimum excursion price</param>
        /// <param name="price_max">Maximum excursion price</param>
        /// <param name="minUserRating">Minimum user rating</param>
        /// <param name="maxUserRating">Maximum user rating</param>
        /// <param name="duration_min">Minimum duration in days</param>
        /// <param name="duration_max">Maximum duration in days</param>
        /// <param name="page">Enter page number</param>
        /// <param name="limit">Enter the page size</param>
        /// <returns>Returns list of hotel excursion details </returns>
        [HttpGet]
        [Route("hotels/excursion/search", Name = "SearchExcursion")]
        [ResponseType(typeof(List<ExcursionDetailsDTO>))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion not found")]
        [SwaggerResponse(HttpStatusCode.NoContent, Description = "No content")]
        [SwaggerOperation(Tags = new[] { "Excursion Search" })]
        public IHttpActionResult SearchExcursion(int hotelId, string searchText = "", [FromUri] int[] Ids = null, double? minUserRating = null, double? maxUserRating = null, double? price_min = null, double? price_max = null, int? duration_min = null, int? duration_max = null, int page = 1, int limit = 20)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            int total;
            var excursionDetails = _hotelService.ExcursionSearch(out total, hotelId, searchText, Ids, minUserRating, maxUserRating, price_min, price_max, duration_min, duration_max, page, limit);
            var favHotelExcursion = _customerService.GetCustomerFavoriteExcursionDetails(user.Id);
            double? baseCurrencyRate = 0;
            if (excursionDetails.Any())
            {
                if (user.CurrencyId != null && user.CurrencyId > 0)
                    baseCurrencyRate = excursionDetails.Select(s => s.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

                var excursionDetailsDTO = Mapper.Map<IEnumerable<ExcursionDetail>, List<ExcursionDetailsDTO>>(excursionDetails);
                excursionDetailsDTO.Select(r => { r.IsFavourite = false; return r; }).ToList();
                if (favHotelExcursion != null && favHotelExcursion.Count() > 0)
                {
                    foreach (var res in excursionDetailsDTO)
                    {
                        if (favHotelExcursion.Any(r => r.Id == res.Id))
                            res.IsFavourite = true;
                        res.PreferredCurrencySymbol = user.Currency.Symbol;
                        if (baseCurrencyRate != 0)
                            res.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(res.Price * baseCurrencyRate), 2);
                    }
                }

                var mod = total % limit;
                var totalPageCount = (total / limit) + (mod == 0 ? 0 : 1);
                return Content(HttpStatusCode.OK, new { Message = "success", total_items = total, total_pages = totalPageCount, current_page = page, result = excursionDetailsDTO });
            }

            return Content(HttpStatusCode.NotFound, new { Message = new string[] { "Result Not Found" } });
        }
        #endregion Excursion Search

        #region Hotel Excursion
        /// <summary>
        /// Returns list of excursion services available in hotel
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of excursion services available in the hotel. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Details of excursion services</returns>
        [Route("hotels/{id}/excursion_list", Name = "GetHotelExcursionList")]
        [HttpGet]

        [ResponseType(typeof(HotelExcursionDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Excursion service not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelExcursionList(int id, int? page = 1, int? limit = null)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var hotelExcServices = _hotelService.GetHotelExcursionServices(id, page.Value, limit.Value, out total);
            if (hotelExcServices == null || hotelExcServices.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HotelExcService_NotFound } });

            var hotelExcDTO = Mapper.Map<IEnumerable<HotelExcursion>, List<HotelExcursionDTO>>(hotelExcServices);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = hotelExcDTO });
        }

        /// <summary>
        /// Returns excursion details of particular excursion
        /// </summary> 
        /// <remarks>
        /// Get a excursion details of particular excursion. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Hotel Excursion Id</param>
        /// <param name="considerMyPreferences">My Preference</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns> Returns excursion details of particular excursion</returns>
        [Route("hotels/excursion/{id}", Name = "GetHotelExcursionById")]
        [HttpGet]

        [ResponseType(typeof(ExcursionDetailsDTO))]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Excursion details not found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelExcursionById(int id, bool considerMyPreferences = false, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionDetails = _hotelService.GetExcursionDetailsByHotelExcId(id, authenticatedCustomer.Id, page.Value, limit.Value, out   total, considerMyPreferences);
            if (excursionDetails.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetail_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = excursionDetails.Select(s => s.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favExcursionDetails = _customerService.GetCustomerFavoriteExcursionDetails(authenticatedCustomer.Id);

            var excDetailDTO = Mapper.Map<IEnumerable<ExcursionDetail>, List<ExcursionDetailsDTO>>(excursionDetails);
            excDetailDTO.Select(s => { s.IsFavourite = favExcursionDetails.Any(r => r.Id == s.Id) ? true : false; return s; }).ToList();
            excDetailDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
                excDetailDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = excDetailDTO });
        }

        /// <summary>
        /// Returns excursion details by id with its reviews, offers, additional elements,suggestion etc.
        /// </summary> 
        /// <remarks>
        ///  Get excursion details by id with its reviews, offers, additional elements,suggestion etc.
        /// </remarks>
        /// <param name="id">Excursion detail Id</param>
        /// <returns> Returns excursion details by id with its reviews, offers, additional elements,suggestion etc.</returns>
        [Route("hotels/excursion_details/{id}", Name = "GetExcursionDetailsById")]
        [HttpGet]

        [ResponseType(typeof(ExcursionDetailsWithFullInfoDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Excursion details not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetExcursionDetailsById(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionDetails = _hotelService.GetExcursionDetailById(id);
            if (excursionDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetail_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = excursionDetails.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            bool IsFavourite = excursionDetails.CustomerFavoriteExcursion.Any(c => c.CustomerId == authenticatedCustomer.Id);
            double customerRating = 0;
            if (excursionDetails.ExcursionReviews != null && excursionDetails.ExcursionReviews.Count() > 0)
                customerRating = excursionDetails.ExcursionReviews.Average(rv => rv.Score);

            var favExcursionDetails = _customerService.GetCustomerFavoriteExcursionDetails(authenticatedCustomer.Id);
            var excursionDetailDTO = Mapper.Map<ExcursionDetail, ExcursionDetailsWithFullInfoDTO>(excursionDetails);
            if (IsFavourite)
                excursionDetailDTO.IsFavourite = IsFavourite;
            excursionDetailDTO.CustomerRating = customerRating;
            excursionDetailDTO.ExcursionSuggestions.Select(e => { e.ExcursionDetail1.IsFavourite = favExcursionDetails.Any(r => r.Id == e.ExcursionDetail1.Id) ? true : false; return e; }).ToList();
            if (baseCurrencyRate != 0)
            {
                excursionDetailDTO.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(excursionDetailDTO.Price * baseCurrencyRate), 2);
                excursionDetailDTO.ExcursionOffers.Select(o => { o.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.DiscountPrice) * baseCurrencyRate), 2); return o; }).ToList();
                excursionDetailDTO.ExcursionSuggestions.Select(o => { o.ExcursionDetail1.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(o.ExcursionDetail1.Price) * baseCurrencyRate), 2); return o; }).ToList();

            }

            excursionDetailDTO.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
            excursionDetailDTO.ExcursionSuggestions.Select(s => { s.ExcursionDetail1.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return s; }).ToList();

            return Content(HttpStatusCode.OK, excursionDetailDTO);
        }

        /// <summary>
        /// Returns list of available date time slots for the month of particular excursion details
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of available date time slots for the month of particular excursion details. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Excursion Detail Id</param>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <returns>list of available date time slots for the month of particular excursion details</returns>
        [Route("hotels/excursion_detail/{id}/dateTimeSlotsForMonth", Name = "GetExcursionDetailsDateTimeSlots")]
        [HttpGet]

        [ResponseType(typeof(List<string>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Excursion details not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Service" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetExcursionDetailsDateTimeSlots(int id, int year, int month)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelExcDetails = _hotelService.GetExcursionDetailById(id);
            if (hotelExcDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetail_NotFound } });

            var date = new DateTime(year, month, 1);

            if ((int)date.DayOfWeek != hotelExcDetails.WeekDay)
            {
                int daysUntilDayOfWeek = ((int)hotelExcDetails.WeekDay - (int)date.DayOfWeek + 7) % 7;
                date = date.AddDays(daysUntilDayOfWeek);
            }

            List<string> days = new List<string>();
            CultureInfo ci = new CultureInfo("en-US");

            while (date.Month == month)
            {
                var excursionDateTime = date + hotelExcDetails.StartTime;
                if (excursionDateTime > DateTime.Now)
                {
                    var dat = date.ToString("dd MMM yyyy,", ci) + hotelExcDetails.StartTime.ToString("hh\\:mm");
                    days.Add(dat);
                }
                date = date.AddDays(7);
            }

            return Content(HttpStatusCode.OK, days);
        }
        #endregion Hotel Excursion

        #region Excursion Cart
        /// <summary>
        /// Creates new excursion cart /  basket
        /// </summary>
        /// <remarks>
        /// Creates customer excursion cart /  basket
        /// </remarks>
        /// <param name="createExcursionCartDTO">Excursion Details in Request body</param>
        /// <param name="flush">Flushes old cart and creates new one</param>
        /// <returns>Returns newly created excursion details in response</returns>

        [Route("hotel/excursion_cart")]
        [HttpPost]
        [ResponseType(typeof(ExcursionCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error or Cart Already exists")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion details not found")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Cart" })]
        public IHttpActionResult CreateExcursionCart(CreateExcursionCartDTO createExcursionCartDTO, bool flush = false)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });


            var entity = Mapper.Map<ExcursionCart>(createExcursionCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateExcursionCart(entity, flush);
                entity.Hotel = _hotelService.GetHotelById(entity.HotelId);

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<ExcursionCart, ExcursionCartDTO>(entity);
                if (baseCurrencyRate > 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    cartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.Price * baseCurrencyRate), 2); return i; }).ToList();
                    cartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                    cartDTO.ExcursionCartItems.Select(i => { i.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(i.OfferPrice * baseCurrencyRate), 2); return i; }).ToList();
                }

                return Content(HttpStatusCode.Created, new { status = true, data = cartDTO });
            }
            catch (CartExistsException existEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = existEx.Message, details = "Issue request with flush=true to create new cart" });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing excursion detail id etc" });
            }
            catch (NotFoundException arEx)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { arEx.Message } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Updates excursion cart /  basket
        /// </summary>
        /// <remarks>
        /// Updates customer excursion cart.
        /// </remarks>
        /// <param name="updateExcursionCartDTO">Excursion details request parameters</param>
        /// <returns>Returns updated excursion cart details</returns>

        [Route("hotel/excursion_cart")]
        [HttpPut]
        [ResponseType(typeof(ExcursionCartDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error ")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion cart not found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Cart" })]
        public IHttpActionResult UpdateExcursionCart(UpdateExcursionCartDTO updateExcursionCartDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<ExcursionCart>(updateExcursionCartDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.UpdateExcursionCart(authenticatedCustomer.Id, entity);
                //entity.Hotel = _hotelService.GetHotelById(entity.HotelId);

                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var cartDTO = Mapper.Map<ExcursionCart, ExcursionCartDTO>(entity);

                if (baseCurrencyRate != 0)
                {
                    cartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(cartDTO.Total * baseCurrencyRate), 2);
                    cartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.Price * baseCurrencyRate), 2); return i; }).ToList();
                    cartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                    cartDTO.ExcursionCartItems.Select(i => { i.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(i.OfferPrice * baseCurrencyRate), 2); return i; }).ToList();
                }

                return Content(HttpStatusCode.OK, new { status = true, data = cartDTO });
            }
            catch (NotFoundException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
            catch (ArgumentNullException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (DbUpdateException dbEx)
            {
                return Content(HttpStatusCode.BadRequest, new { message = "INCORRECT_DATA", details = "Incorrect data such as non existing excursion detail id etc" });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Returns the excursion cart details of authorized customer of particular hotel
        /// </summary>
        /// <remarks>
        /// Returns the excursion cart details of authorized customer of particular hotel
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <returns>Returns Excursion Cart Details</returns>
        [HttpGet]
        [Route("hotels/{id}/excursion_cart", Name = "GetExcursionCart")]

        [ResponseType(typeof(ExcursionCartDTO))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Cart Is Empty")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Cart" })]
        public IHttpActionResult GetExcursionCart(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var cart = _hotelService.GetCustomerExcursionCart(customer.Id, id);
            if (cart == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cart_IsEmpty } });

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = cart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            cart.ExcursionCartItems = cart.ExcursionCartItems.OrderBy(c => c.Id).ToList();
            var excurionCartDTO = Mapper.Map<ExcursionCart, ExcursionCartDTO>(cart);

            if (baseCurrencyRate != 0)
            {
                excurionCartDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(excurionCartDTO.Total * baseCurrencyRate), 2);
                excurionCartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.Price * baseCurrencyRate), 2); return i; }).ToList();
                excurionCartDTO.ExcursionCartItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                excurionCartDTO.ExcursionCartItems.Select(i => { i.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(i.OfferPrice * baseCurrencyRate), 2); return i; }).ToList();
            }
            return Content(HttpStatusCode.OK, excurionCartDTO);
        }
        /// <summary>
        /// Deletes the excursion cart item from the cart based on excursionCartItemId
        /// </summary>
        /// <remarks>
        /// Deletes the excursion cart item from the cart based on excursionCartItemId
        /// </remarks>
        /// <param name="Id">Excursion cart item Id</param>
        /// <returns>Returns status</returns>
        [Route("hotels/excursion_cart_item/{id}", Name = "DeleteExcursionCartItem")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Cart Item Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Cart" })]
        public IHttpActionResult DeleteExcursionCartItem(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionCartItem = _hotelService.GetExcursionCartItemById(id);
            if (excursionCartItem == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcCartItem_NotFound } });
            try
            {
                var custExcursionCart = excursionCartItem.ExcursionCart;
                custExcursionCart.Total -= excursionCartItem.Total;
                _hotelService.DeleteExcursionCartItem(id);
                _hotelService.ModifyExcursionCartTotal(custExcursionCart);
                return Content(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        #endregion Excursion Cart

        #region Excursion Order

        /// <summary>
        /// Returns the excursion order details
        /// </summary>
        /// <remarks>
        /// Returns the excursion order details
        /// </remarks>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns Excursion Order Details</returns>
        [HttpGet]
        [Route("hotel/excursion_order", Name = "GetExcursionOrder")]

        [ResponseType(typeof(ExcursionOrderDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Order" })]
        public IHttpActionResult GetExcursionOrder(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetExcursionOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionOrder_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            order.ExcursionOrderItems = order.ExcursionOrderItems.OrderBy(o => o.ScheduleDate).ToList();

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = order.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

            var excursionOrderDTO = Mapper.Map<ExcursionOrder, ExcursionOrderDTO>(order);
            if (baseCurrencyRate != 0)
            {
                excursionOrderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(excursionOrderDTO.Total * baseCurrencyRate), 2);
                excursionOrderDTO.ExcursionOrderItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.Price * baseCurrencyRate), 2); return i; }).ToList();
                excursionOrderDTO.ExcursionOrderItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                excursionOrderDTO.ExcursionOrderItems.Select(i => { i.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(i.OfferPrice * baseCurrencyRate), 2); return i; }).ToList();
            }

            excursionOrderDTO.ReceiptNumber = "#" + order.Id;
            ExcursionOrderStatus ordrStatus = (ExcursionOrderStatus)order.OrderStatus;
            excursionOrderDTO.OrderStatus = ordrStatus.ToString();

            return Content(HttpStatusCode.OK, excursionOrderDTO);
        }


        /// <summary>
        /// Place order for excursion items in basket
        /// </summary>
        /// <remarks>
        /// Place order for excursion cart items
        /// </remarks>
        /// <param name="cardId"> Select customer card</param>
        /// <param name="hotelId">Hotel Id</param>
        /// <returns>Returns details of newly created excursion order</returns>

        [Route("hotel/excursion_order")]
        [HttpPost]
        [ResponseType(typeof(ExcursionOrderDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Excursion Order" })]
        public IHttpActionResult PlaceExcursionOrderFromCart(int cardId, int hotelId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            ExcursionOrder entity;
            try
            {
                entity = _hotelService.CreateExcursionOrderFromCart(authenticatedCustomer.Id, cardId, hotelId);
                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();
                double? baseCurrencyRate = 0;
                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                    baseCurrencyRate = entity.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                var excursionOrderDTO = Mapper.Map<ExcursionOrder, ExcursionOrderDTO>(entity);
                if (baseCurrencyRate != 0)
                {
                    excursionOrderDTO.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(excursionOrderDTO.Total * baseCurrencyRate), 2);
                    excursionOrderDTO.ExcursionOrderItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.Price * baseCurrencyRate), 2); return i; }).ToList();
                    excursionOrderDTO.ExcursionOrderItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                    excursionOrderDTO.ExcursionOrderItems.Select(i => { i.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(i.OfferPrice * baseCurrencyRate), 2); return i; }).ToList();
                }

                excursionOrderDTO.ReceiptNumber = "#" + entity.Id;
                ExcursionOrderStatus ordrStatus = (ExcursionOrderStatus)entity.OrderStatus;
                excursionOrderDTO.OrderStatus = ordrStatus.ToString();

                return Content(HttpStatusCode.Created, new { status = true, data = excursionOrderDTO });
            }
            catch (CartEmptyException empty)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Cart_Empty } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Create new reviews for Excursion order
        /// </summary>
        /// <remarks>
        /// Adds new reviews for excursion order in the backend
        /// </remarks>
        /// <param name="excursionOrderReviewDTO">Review Details in Request body</param>
        /// <returns>Returns details of excursion reviews</returns>

        [Route("hotels/excursion_order/Review")]
        [HttpPost]
        [ResponseType(typeof(ExcursionOrderReviewDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Excursion Order" })]
        public IHttpActionResult CreateExcursionOrderReview(CreateExcursionOrderReviewDTO excursionOrderReviewDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetExcursionOrdersDetailsById(excursionOrderReviewDTO.ExcursionOrderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionOrder_NotFound } });

            if (order.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var entity = Mapper.Map<ExcursionOrderReview>(excursionOrderReviewDTO);
            entity = _hotelService.CreateExcursionOrderReview(entity);
            var orderReviewsDTO = Mapper.Map<ExcursionOrderReviewDTO>(entity);

            return Content(HttpStatusCode.OK, orderReviewsDTO);
        }

        /// <summary>
        /// Returns list of excursion order reviews
        /// </summary>
        /// <remarks>
        /// Get list of excursion order reviews.
        /// </remarks>
        /// <param name="id">Excursion Order Id</param>
        /// <returns>List of excursion order Reviews</returns>

        [Route("hotels/excursion_order/{id}/Reviews")]
        [HttpGet]
        [ResponseType(typeof(List<ExcursionOrderReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Reviews not found")]
        [SwaggerOperation(Tags = new[] { "Excursion Order" })]

        public IHttpActionResult GetExcursionOrderReviews(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionOrder = _hotelService.GetExcursionOrdersDetailsById(id);
            if (excursionOrder == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionOrder_NotFound } });

            if (excursionOrder.CustomerId != authenticatedCustomer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var orderReviews = _hotelService.GetExcursionOrderReviews(id);
            if (orderReviews.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var orderReviewDTO = Mapper.Map<IEnumerable<ExcursionOrderReview>, List<ExcursionOrderReviewDTO>>(orderReviews);
            return Content(HttpStatusCode.OK, orderReviewDTO);
        }

        #endregion Excursion Order

        #region Excursion Reviews

        /// <summary>
        /// Returns list of excursion reviews by excursion details Id
        /// </summary> 
        /// <remarks>
        /// Returns list of excursion reviews by excursion details Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Excursion Detail Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Excursion reviews</returns>
        [Route("hotels/excursion_details/{id}/reviews", Name = "GetExcursionReviews")]
        [HttpGet]

        [ResponseType(typeof(List<ExcursionReviewsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Excursion reviews not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetExcursionReviews(int id, int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionReviews = _hotelService.GetAllExcursionReviewsByExcDetId(id, page.Value, limit.Value, out total);
            if (excursionReviews == null || excursionReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var excursionReviewsDTO = Mapper.Map<IEnumerable<ExcursionReview>, List<ExcursionReviewsDTO>>(excursionReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = excursionReviewsDTO });
        }
        /// <summary>
        /// Creates new hotel excursion review
        /// </summary>
        /// <remarks>
        /// Add new hotel excursion review in the backend
        /// </remarks>
        /// <param name="excursionReviewsDTO">Excursion Review Details</param>
        /// <returns>Returns newly created excursion review details</returns>
        [HttpPost]
        [Route("hotels/excursion_review", Name = "CreateExcursionReview")]

        [ResponseType(typeof(ExcursionReviewsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.Created, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotel Excursion Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult CreateExcursionReview(CreateExcursionReviewDTO excursionReviewsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var entity = Mapper.Map<ExcursionReview>(excursionReviewsDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _hotelService.CreateExcursionReview(entity);
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            return Created(Url.Link("GetExcursionReviews", new { id = entity.Id }), Mapper.Map<ExcursionReviewsDTO>(entity));
        }

        #endregion Excursion Reviews

        #region Excursion Offers

        /// <summary>
        /// Returns hotel excursion details offers
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotel excursion details containing offers
        /// </remarks>
        /// <param name="id">Hotel Id</param>
        /// <param name="considerMyPreferences">My Preference</param>
        /// <returns>List of hotel excursion details containing offers</returns>
        [HttpGet]
        [Route("hotels/{id}/excursion_offers", Name = "GetExcursionOffersByHotelId")]
        [AllowAnonymous]
        [ResponseType(typeof(List<ExcursionOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Offers Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Excursion Offers" })]

        public IHttpActionResult GetExcursionOffersByHotelId(int id, bool considerMyPreferences = false)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotel = _hotelService.GetHotelById(id);
            if (hotel == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });

            var excursionOffers = _hotelService.GetExcursionOfferByHotelId(id, customer.Id, considerMyPreferences);
            if (excursionOffers == null || excursionOffers.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetailOffer_NotFound } });


            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = excursionOffers.Select(s => s.ExcursionDetail.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var favExcursionDetails = _customerService.GetCustomerFavoriteExcursionDetails(customer.Id);
            var excursionDetOfferDTO = Mapper.Map<IEnumerable<ExcursionOffer>, List<ExcursionOffersDTO>>(excursionOffers);

            excursionDetOfferDTO.Select(s => { s.IsFavourite = favExcursionDetails.Any(r => r.Id == s.ExcursionDetailId) ? true : false; return s; }).ToList();
            excursionDetOfferDTO.Select(i => { i.PreferredCurrencySymbol = customer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
            {
                excursionDetOfferDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                excursionDetOfferDTO.Select(s => { s.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.DiscountPrice * baseCurrencyRate), 2); return s; }).ToList();
            }
            return Content(HttpStatusCode.OK, excursionDetOfferDTO);
        }


        /// <summary>
        /// Returns hotel excursion details containing offers of particular hotel excursion
        /// </summary>
        /// <remarks>
        /// Get a detailed list of hotel excursion details containing offers of particular hotel excursion
        /// </remarks>
        /// <param name="id">Hotel Excursion Id</param>
        /// <returns>List of hotel excursion details containing offers of particular hotel excursion</returns>
        [HttpGet]
        [Route("hotels/excursion/{id}/offers", Name = "GetExcursionOffersHotelExcursionId")]
        [AllowAnonymous]
        [ResponseType(typeof(List<ExcursionOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Offers Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Excursion Offers" })]

        public IHttpActionResult GetExcursionOffersHotelExcursionId(int id)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelExcursion = _hotelService.GetHotelExcursionById(id);
            if (hotelExcursion == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.HotelExcursion_NotFound } });

            var hotelExcursionOffer = _hotelService.GetExcursionOfferByHotelExcursionId(id);
            if (hotelExcursionOffer == null || hotelExcursionOffer.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetailOffer_NotFound } });

            double? baseCurrencyRate = 0;
            if (customer.CurrencyId != null && customer.CurrencyId > 0)
                baseCurrencyRate = hotelExcursionOffer.Select(s => s.ExcursionDetail.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            //var favSpaServiceDetails = _customerService.GetCustomerFavoriteSpaServiceDetails(customer.Id);
            var excursionDetOfferDTO = Mapper.Map<IEnumerable<ExcursionOffer>, List<ExcursionOffersDTO>>(hotelExcursionOffer);

            //spaServiceDetOffersDTO.Select(s => { s.IsFavourite = favSpaServiceDetails.Any(r => r.Id == s.SpaServiceDetailId) ? true : false; return s; }).ToList();
            excursionDetOfferDTO.Select(i => { i.PreferredCurrencySymbol = customer.Currency.Symbol; return i; }).ToList();

            if (baseCurrencyRate != 0)
            {
                excursionDetOfferDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                excursionDetOfferDTO.Select(s => { s.DiscountPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.DiscountPrice * baseCurrencyRate), 2); return s; }).ToList();
            }

            return Content(HttpStatusCode.OK, excursionDetOfferDTO);
        }

        #endregion Excursion Offers

        #region Hotels Services Offers

        /// <summary>
        /// Returns list of hotels services offers
        /// </summary> 
        /// <remarks>
        /// Returns list of hotels services offers. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of hotels services offers</returns>
        [Route("hotel/servicesOffers")]
        [HttpGet]

        [ResponseType(typeof(List<AllHotelServicesOffersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Hotel Services offers Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Hotels Services Offers" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelsServicesOffers()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelsServicesOffers = _hotelService.GetAllHotelServicesOffers();
            if (hotelsServicesOffers == null || hotelsServicesOffers.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Offers_Found } });

            var hotelsServicesOffersDTO = Mapper.Map<IEnumerable<AllHotelServicesOffers>, List<AllHotelServicesOffersDTO>>(hotelsServicesOffers);
            return Content(HttpStatusCode.OK, hotelsServicesOffersDTO);
        }

        #endregion Hotels Services Offers

        #region Snooze

        /// <summary>
        /// Returns Success if snooze
        /// </summary> 
        /// <remarks>
        /// Returns Success. Authorization required to access this method.
        /// </remarks>
        /// <param name="token">firebase client token</param>
        /// <param name="orderId">Spa Order Id</param>    
        /// <returns>Returns Success</returns>
        [Route("hotel/snooze")]
        [HttpGet]

        [ResponseType(typeof(Object))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Snooze" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult CreateSnooze(string token, int orderId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                var order = _hotelService.GetSpaOrdersDetailsById(orderId);
                if (order == null)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaOrder_NotFound } });
                var data = new
                {
                    to = token.ToString(),
                    data = new
                    {
                        message = "Spa Order start Time is " + order.SpaOrderItems.Select(s => s.StartDateTime).FirstOrDefault(),
                        name = order.SpaOrderItems.Select(s => s.SpaServiceDetail.Name).FirstOrDefault()
                    }
                };
                string dt = SendNotification(data).ToString();
                dynamic responseJSON = JsonConvert.DeserializeObject(dt);

                int acceptedStatus = responseJSON.success;

                if (acceptedStatus == 1)
                    return Content(HttpStatusCode.OK, new { messages = "success" });
                else
                    return Content(HttpStatusCode.OK, new { messages = "failure" });
            }
            catch (Exception ex)
            {
                dynamic responseJSON = JsonConvert.DeserializeObject(ex.Message);
                return Content(HttpStatusCode.BadRequest, new { messages = responseJSON });
            }
        }

        public Object SendNotification(object data)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            return SendNotification(byteArray);
        }

        public Object SendNotification(Byte[] byteArray)
        {
            try
            {
                string server_api_key = ConfigurationManager.AppSettings["SERVER_API_KEY"];
                string server_id = ConfigurationManager.AppSettings["SENDER_ID"];

                HttpWebRequest tRequest = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "POST";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add("Authorization", "key=" + server_api_key);
                tRequest.Headers.Add("Sender", "id=" + server_id);

                tRequest.ContentLength = byteArray.Length;
                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tresponse = tRequest.GetResponse();
                dataStream = tresponse.GetResponseStream();
                StreamReader tReader = new StreamReader(dataStream);

                string sResponseFromServer = tReader.ReadToEnd();

                tReader.Close();
                dataStream.Close();

                return sResponseFromServer;

            }
            catch (Exception ex)
            {
                dynamic responseJSON = JsonConvert.DeserializeObject(ex.Message);
                return ex.Message;
            }

        }

        #endregion Snooze

        #region GeneratePDF

        /// <summary>
        /// Send order receipt to email
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <remarks>
        /// Returs the Spa Order details
        /// </remarks>
        /// <returns>Send order receipt to email</returns>
        [HttpPost]
        [Route("hotel/spaOrderReceipt")]

        [ResponseType(typeof(object))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Spa Order" })]
        public IHttpActionResult SendSpaOrderReceipt(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetSpaOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var spaOrderDTO = Mapper.Map<SpaOrder, SpaOrderInfoPdfDTO>(order);
            spaOrderDTO.ReceiptNumber = "#" + order.Id;

            var r = GenerateSpaOrderPDF(spaOrderDTO);
            var task = r.Content.ReadAsStreamAsync();
            task.Wait();
            Stream requestStream = task.Result;
            try
            {
                Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/PDF/" + "SpaOrderReceipt" + spaOrderDTO.ReceiptNumber + ".pdf"));
                requestStream.CopyTo(fileStream);
                fileStream.Close();
                requestStream.Close();

                SendReceipt(customer.Email, spaOrderDTO.ReceiptNumber, "SpaOrderReceipt");
                return Content(HttpStatusCode.OK, new { messages = new string[] { "Mail send Successfullly" } });
            }
            catch (IOException)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PDF already generated or send" } });
            }
        }

        private HttpResponseMessage GenerateSpaOrderPDF(SpaOrderInfoPdfDTO request)
        {
            try
            {
                var stream = CreateSpaOrderPdf(request);
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(System.Net.Mime.MediaTypeNames.Application.Pdf),
                            ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "PdfFileName.pdf" }
                        }
                    }
                };
                return response;
            }
            catch (ApplicationException ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private Stream CreateSpaOrderPdf(SpaOrderInfoPdfDTO request)
        {
            using (var document = new Document(PageSize.A4, 50f, 50f, 100f, 100f))
            {
                var output = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, output);

                var page = new ITextEvents();
                page.Hotel = request.Hotel;
                writer.PageEvent = page;

                writer.CloseStream = false;
                // set standard font type and size
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                PdfPTable receiptTable = new PdfPTable(2);
                receiptTable.HorizontalAlignment = 1;

                PdfPCell seatNo = new PdfPCell(new Phrase("Seat No.", _standardFont));
                seatNo.Border = 0;
                PdfPCell seatNum = new PdfPCell(new Phrase(request.Id.ToString(), _standardFont));
                seatNum.Border = 0;

                PdfPCell booking = new PdfPCell(new Phrase("Booking", _standardFont));
                booking.Border = 0;
                PdfPCell bookingRe = new PdfPCell(new Phrase(request.OrderDate.ToString(), _standardFont));
                bookingRe.Border = 0;

                PdfPCell receiptNo = new PdfPCell(new Phrase("Receipt No.", _standardFont));
                receiptNo.Border = 0;
                PdfPCell receiptNum = new PdfPCell(new Phrase(request.ReceiptNumber.ToString(), _standardFont));
                receiptNum.Border = 0;

                receiptTable.AddCell(seatNo);
                receiptTable.AddCell(seatNum);

                receiptTable.AddCell(booking);
                receiptTable.AddCell(bookingRe);

                receiptTable.AddCell(receiptNo);
                receiptTable.AddCell(receiptNum);

                PdfPTable mainTable = new PdfPTable(4);
                mainTable.HorizontalAlignment = 1;

                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;

                // here we will add table headers for report details
                PdfPCell serviceName = new PdfPCell(new Phrase("Service Name", _standardFont));
                serviceName.Border = 0;
                PdfPCell price = new PdfPCell(new Phrase("Price", _standardFont));
                price.Border = 0;
                PdfPCell qty = new PdfPCell(new Phrase("Qty", _standardFont));
                qty.Border = 0;
                PdfPCell amount = new PdfPCell(new Phrase("Amount", _standardFont));
                amount.Border = 0;

                mainTable.AddCell(serviceName);
                mainTable.AddCell(price);
                mainTable.AddCell(qty);
                mainTable.AddCell(amount);

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                foreach (var item in request.SpaOrderItems)
                {
                    mainTable.AddCell(item.SpaServiceDetailName);
                    mainTable.AddCell(item.Price.ToString());
                    mainTable.AddCell(item.Qty.ToString());
                    mainTable.AddCell(item.Total.ToString());

                    //Check for Additional
                    if (item.SpaOrderItemsAdditionals.Any())
                    {

                        PdfPTable mainTable1 = new PdfPTable(4);
                        mainTable1.HorizontalAlignment = 1;

                        mainTable1.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell itemName1 = new PdfPCell(new Phrase("Additional", _standardFont));
                        itemName1.Border = 0;
                        PdfPCell price1 = new PdfPCell(new Phrase("Price", _standardFont));
                        price1.Border = 0;
                        PdfPCell qty1 = new PdfPCell(new Phrase("Qty", _standardFont));
                        qty1.Border = 0;
                        PdfPCell amount1 = new PdfPCell(new Phrase("Amount", _standardFont));
                        amount1.Border = 0;

                        mainTable1.AddCell(itemName1);
                        mainTable1.AddCell(price1);
                        mainTable1.AddCell(qty1);
                        mainTable1.AddCell(amount1);

                        foreach (var item1 in item.SpaOrderItemsAdditionals)
                        {
                            mainTable1.AddCell(item1.SpaAdditionalElement.Name);
                            mainTable1.AddCell(item1.SpaAdditionalElement.Price.ToString());
                            mainTable1.AddCell(item1.Qty.ToString());
                            mainTable1.AddCell(item1.Total.ToString());
                        }
                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable1)) { Colspan = 3, Border = 0 });
                    }

                    //Check for ExtraTime
                    if (item.ExtraTime != null)
                    {
                        PdfPTable mainTable2 = new PdfPTable(4);
                        mainTable2.HorizontalAlignment = 1;

                        mainTable2.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell ExtraTime = new PdfPCell(new Phrase("ExtraTime", _standardFont));
                        ExtraTime.Border = 0;
                        PdfPCell Duration = new PdfPCell(new Phrase("Duration", _standardFont));
                        Duration.Border = 0;
                        PdfPCell ex = new PdfPCell(new Phrase("", _standardFont));
                        ex.Border = 0;
                        PdfPCell Price = new PdfPCell(new Phrase("Price", _standardFont));
                        Price.Border = 0;

                        mainTable2.AddCell(ExtraTime);
                        mainTable2.AddCell(Duration);
                        mainTable2.AddCell(ex);
                        mainTable2.AddCell(Price);

                        mainTable2.AddCell(item.ExtraTime.Name);
                        mainTable2.AddCell(item.ExtraTime.Duration.ToString());
                        mainTable2.AddCell("");
                        mainTable2.AddCell(item.ExtraTime.Price.ToString());

                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable2)) { Colspan = 3, Border = 0 });
                    }

                    //Check for Extra Procedure
                    if (item.SpaOrderExtraProcedures.Any())
                    {

                        PdfPTable mainTable3 = new PdfPTable(4);
                        mainTable3.HorizontalAlignment = 1;

                        mainTable3.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell proName = new PdfPCell(new Phrase("Procdure", _standardFont));
                        proName.Border = 0;
                        PdfPCell procDuration = new PdfPCell(new Phrase("Duration", _standardFont));
                        procDuration.Border = 0;
                        PdfPCell procEmty = new PdfPCell(new Phrase("", _standardFont));
                        procEmty.Border = 0;
                        PdfPCell ProcPrice = new PdfPCell(new Phrase("Price", _standardFont));
                        ProcPrice.Border = 0;

                        mainTable3.AddCell(proName);
                        mainTable3.AddCell(procDuration);
                        mainTable3.AddCell(procEmty);
                        mainTable3.AddCell(ProcPrice);

                        foreach (var item3 in item.SpaOrderExtraProcedures)
                        {
                            mainTable3.AddCell(item3.ExtraProcedure.Name);
                            mainTable3.AddCell(item3.ExtraProcedure.Duration.ToString());
                            mainTable3.AddCell("");
                            mainTable3.AddCell(item3.ExtraProcedure.Price.ToString());
                        }
                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable3)) { Colspan = 3, Border = 0 });
                    }
                }
                PdfPCell totalToPay = new PdfPCell(new Phrase("Total to Pay", _standardFont));
                totalToPay.Border = 0;
                PdfPCell totalToPays = new PdfPCell(new Phrase(request.OrderTotal.ToString(), _standardFont));
                totalToPays.Border = 0;

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                mainTable.AddCell(totalToPay);
                mainTable.AddCell("");
                mainTable.AddCell("");
                mainTable.AddCell(totalToPays);

                document.Open();

                Rectangle rect = new Rectangle(577, 825, 18, 15);
                rect.EnableBorderSide(1);
                rect.EnableBorderSide(2);
                rect.EnableBorderSide(4);
                rect.EnableBorderSide(8);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = 1;
                document.Add(rect);

                Paragraph p = new Paragraph();
                document.Add(p);

                document.Add(Chunk.NEWLINE);
                document.Add(receiptTable);

                document.Add(Chunk.NEWLINE);
                document.Add(mainTable);
                document.Close();

                output.Seek(0, SeekOrigin.Begin);
                return output;
            }
        }


        /// <summary>
        /// Send order receipt to email
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <remarks>
        /// Returs the Laundry Order details
        /// </remarks>
        /// <returns>Send order receipt to email</returns>
        [HttpPost]
        [Route("hotel/laundryOrderReceipt")]

        [ResponseType(typeof(object))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Laundry Order" })]
        public IHttpActionResult SendLaundryOrderReceipt(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetLaundryOrderById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var laundryOrderDTO = Mapper.Map<LaundryOrder, LaundryOrderInfoDTO>(order);
            laundryOrderDTO.ReceiptNumber = "#" + order.Id;

            var r = GenerateLaundryOrderPDF(laundryOrderDTO);
            var task = r.Content.ReadAsStreamAsync();
            task.Wait();
            Stream requestStream = task.Result;
            try
            {
                Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/PDF/" + "LaundryOrderReceipt" + laundryOrderDTO.ReceiptNumber + ".pdf"));
                requestStream.CopyTo(fileStream);
                fileStream.Close();
                requestStream.Close();

                SendReceipt(customer.Email, laundryOrderDTO.ReceiptNumber, "LaundryOrderReceipt");
                return Content(HttpStatusCode.OK, new { messages = new string[] { "Mail send Successfullly" } });
            }
            catch (IOException)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PDF already generated or send" } });
            }
        }

        private HttpResponseMessage GenerateLaundryOrderPDF(LaundryOrderInfoDTO request)
        {
            try
            {
                var stream = CreateLaundryOrderPdf(request);
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(System.Net.Mime.MediaTypeNames.Application.Pdf),
                            ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "PdfFileName.pdf" }
                        }
                    }
                };
                return response;
            }
            catch (ApplicationException ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private Stream CreateLaundryOrderPdf(LaundryOrderInfoDTO request)
        {
            using (var document = new Document(PageSize.A4, 50f, 50f, 100f, 100f))
            {
                var output = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, output);

                var page = new ITextEvents();
                page.Hotel = request.Hotel;
                writer.PageEvent = page;

                writer.CloseStream = false;
                // set standard font type and size
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                PdfPTable receiptTable = new PdfPTable(2);
                receiptTable.HorizontalAlignment = 1;

                PdfPCell seatNo = new PdfPCell(new Phrase("Seat No.", _standardFont));
                seatNo.Border = 0;
                PdfPCell seatNum = new PdfPCell(new Phrase(request.Id.ToString(), _standardFont));
                seatNum.Border = 0;

                PdfPCell booking = new PdfPCell(new Phrase("Booking", _standardFont));
                booking.Border = 0;
                PdfPCell bookingRe = new PdfPCell(new Phrase(request.OrderDate.ToString(), _standardFont));
                bookingRe.Border = 0;

                PdfPCell receiptNo = new PdfPCell(new Phrase("Receipt No.", _standardFont));
                receiptNo.Border = 0;
                PdfPCell receiptNum = new PdfPCell(new Phrase(request.ReceiptNumber.ToString(), _standardFont));
                receiptNum.Border = 0;

                receiptTable.AddCell(seatNo);
                receiptTable.AddCell(seatNum);

                receiptTable.AddCell(booking);
                receiptTable.AddCell(bookingRe);

                receiptTable.AddCell(receiptNo);
                receiptTable.AddCell(receiptNum);

                PdfPTable mainTable = new PdfPTable(4);
                mainTable.HorizontalAlignment = 1;

                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;

                // here we will add table headers for report details
                PdfPCell serviceName = new PdfPCell(new Phrase("Service Name", _standardFont));
                serviceName.Border = 0;
                PdfPCell price = new PdfPCell(new Phrase("Price", _standardFont));
                price.Border = 0;
                PdfPCell qty = new PdfPCell(new Phrase("Qty", _standardFont));
                qty.Border = 0;
                PdfPCell amount = new PdfPCell(new Phrase("Amount", _standardFont));
                amount.Border = 0;

                mainTable.AddCell(serviceName);
                mainTable.AddCell(price);
                mainTable.AddCell(qty);
                mainTable.AddCell(amount);

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                foreach (var item in request.LaundryOrderItems)
                {
                    mainTable.AddCell(item.LaundryDetail.Garments.Name);
                    mainTable.AddCell(item.LaundryDetail.Price.ToString());
                    mainTable.AddCell(item.Quantity.ToString());
                    mainTable.AddCell(item.Total.ToString());

                    if (item.LaundryOrderItemsAdditional.Any())
                    {

                        PdfPTable mainTable1 = new PdfPTable(4);
                        mainTable1.HorizontalAlignment = 1;

                        mainTable1.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell itemName1 = new PdfPCell(new Phrase("Additional", _standardFont));
                        itemName1.Border = 0;
                        PdfPCell price1 = new PdfPCell(new Phrase("Price", _standardFont));
                        price1.Border = 0;
                        PdfPCell qty1 = new PdfPCell(new Phrase("Qty", _standardFont));
                        qty1.Border = 0;
                        PdfPCell amount1 = new PdfPCell(new Phrase("Amount", _standardFont));
                        amount1.Border = 0;

                        mainTable1.AddCell(itemName1);
                        mainTable1.AddCell(price1);
                        mainTable1.AddCell(qty1);
                        mainTable1.AddCell(amount1);

                        foreach (var item1 in item.LaundryOrderItemsAdditional)
                        {
                            mainTable1.AddCell(item1.LaundryAdditionalElement.Name);
                            mainTable1.AddCell(item1.LaundryAdditionalElement.Price.ToString());
                            mainTable1.AddCell(item1.Qty.ToString());
                            mainTable1.AddCell(item1.Total.ToString());
                        }

                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable1)) { Colspan = 3, Border = 0 });
                    }
                }
                PdfPCell totalToPay = new PdfPCell(new Phrase("Total to Pay", _standardFont));
                totalToPay.Border = 0;
                PdfPCell totalToPays = new PdfPCell(new Phrase(request.OrderTotal.ToString(), _standardFont));
                totalToPays.Border = 0;

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                mainTable.AddCell(totalToPay);
                mainTable.AddCell("");
                mainTable.AddCell("");
                mainTable.AddCell(totalToPays);

                document.Open();

                Rectangle rect = new Rectangle(577, 825, 18, 15);
                rect.EnableBorderSide(1);
                rect.EnableBorderSide(2);
                rect.EnableBorderSide(4);
                rect.EnableBorderSide(8);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = 1;
                document.Add(rect);
               
                Paragraph p = new Paragraph();
                document.Add(p);

                document.Add(Chunk.NEWLINE);
                document.Add(receiptTable);

                document.Add(Chunk.NEWLINE);
                document.Add(mainTable);
                document.Close();

                output.Seek(0, SeekOrigin.Begin);
                return output;
            }
        }


        /// <summary>
        /// Send order receipt to email
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <remarks>
        /// Returs the Housekeeping Order details
        /// </remarks>
        /// <returns>Send order receipt to email</returns>
        [HttpPost]
        [Route("hotel/housekeepingOrderReceipt")]

        [ResponseType(typeof(object))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Housekeeping Order" })]
        public IHttpActionResult SendHousekeepingOrderReceipt(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetHousekeepingOrderById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var housekeepingOrderDTO = Mapper.Map<HousekeepingOrder, HousekeepingOrderPdfDTO>(order);
            housekeepingOrderDTO.ReceiptNumber = "#" + order.Id;

            var r = GenerateHousekeepingOrderPDF(housekeepingOrderDTO);
            var task = r.Content.ReadAsStreamAsync();
            task.Wait();
            Stream requestStream = task.Result;
            try
            {
                Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/PDF/" + "HousekeepingOrderReceipt" + housekeepingOrderDTO.ReceiptNumber + ".pdf"));
                requestStream.CopyTo(fileStream);
                fileStream.Close();
                requestStream.Close();

                SendReceipt(customer.Email, housekeepingOrderDTO.ReceiptNumber, "HousekeepingOrderReceipt");
                return Content(HttpStatusCode.OK, new { messages = new string[] { "Mail send Successfullly" } });
            }
            catch (IOException)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PDF already generated or send" } });
            }
        }

        private HttpResponseMessage GenerateHousekeepingOrderPDF(HousekeepingOrderPdfDTO request)
        {
            try
            {
                var stream = CreateHousekeepingOrderPdf(request);
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(System.Net.Mime.MediaTypeNames.Application.Pdf),
                            ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "PdfFileName.pdf" }
                        }
                    }
                };
                return response;
            }
            catch (ApplicationException ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private Stream CreateHousekeepingOrderPdf(HousekeepingOrderPdfDTO request)
        {
            using (var document = new Document(PageSize.A4, 50f, 50f, 100f, 100f))
            {
                var output = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, output);

                var page = new ITextEvents();
                page.Hotel = request.Hotel;
                writer.PageEvent = page;

                writer.CloseStream = false;
                // set standard font type and size
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                PdfPTable receiptTable = new PdfPTable(2);
                receiptTable.HorizontalAlignment = 1;

                PdfPCell seatNo = new PdfPCell(new Phrase("Seat No.", _standardFont));
                seatNo.Border = 0;
                PdfPCell seatNum = new PdfPCell(new Phrase(request.Id.ToString(), _standardFont));
                seatNum.Border = 0;

                PdfPCell booking = new PdfPCell(new Phrase("Booking", _standardFont));
                booking.Border = 0;
                PdfPCell bookingRe = new PdfPCell(new Phrase(request.OrderDate.ToString(), _standardFont));
                bookingRe.Border = 0;

                PdfPCell receiptNo = new PdfPCell(new Phrase("Receipt No.", _standardFont));
                receiptNo.Border = 0;
                PdfPCell receiptNum = new PdfPCell(new Phrase(request.ReceiptNumber.ToString(), _standardFont));
                receiptNum.Border = 0;

                receiptTable.AddCell(seatNo);
                receiptTable.AddCell(seatNum);

                receiptTable.AddCell(booking);
                receiptTable.AddCell(bookingRe);

                receiptTable.AddCell(receiptNo);
                receiptTable.AddCell(receiptNum);

                PdfPTable mainTable = new PdfPTable(4);
                mainTable.HorizontalAlignment = 1;

                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;

                // here we will add table headers for report details
                PdfPCell serviceName = new PdfPCell(new Phrase("Service Name", _standardFont));
                serviceName.Border = 0;
                PdfPCell price = new PdfPCell(new Phrase("Price", _standardFont));
                price.Border = 0;
                PdfPCell qty = new PdfPCell(new Phrase("Qty", _standardFont));
                qty.Border = 0;
                PdfPCell amount = new PdfPCell(new Phrase("Amount", _standardFont));
                amount.Border = 0;

                mainTable.AddCell(serviceName);
                mainTable.AddCell(price);
                mainTable.AddCell(qty);
                mainTable.AddCell(amount);

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                foreach (var item in request.HousekeepingOrderItems)
                {
                    mainTable.AddCell(item.HouseKeepingFacilityDetail.Name);
                    mainTable.AddCell(item.HouseKeepingFacilityDetail.Price.ToString());
                    mainTable.AddCell(item.Quantity.ToString());
                    mainTable.AddCell(item.Total.ToString());

                    if (item.HousekeepingOrderItemAdditionals.Any())
                    {

                        PdfPTable mainTable1 = new PdfPTable(4);
                        mainTable1.HorizontalAlignment = 1;

                        mainTable1.DefaultCell.Border = Rectangle.NO_BORDER;

                        // here we will add table headers for report details
                        PdfPCell itemName1 = new PdfPCell(new Phrase("Additional", _standardFont));
                        itemName1.Border = 0;
                        PdfPCell price1 = new PdfPCell(new Phrase("Price", _standardFont));
                        price1.Border = 0;
                        PdfPCell qty1 = new PdfPCell(new Phrase("Qty", _standardFont));
                        qty1.Border = 0;
                        PdfPCell amount1 = new PdfPCell(new Phrase("Amount", _standardFont));
                        amount1.Border = 0;

                        mainTable1.AddCell(itemName1);
                        mainTable1.AddCell(price1);
                        mainTable1.AddCell(qty1);
                        mainTable1.AddCell(amount1);

                        foreach (var item1 in item.HousekeepingOrderItemAdditionals)
                        {
                            mainTable1.AddCell(item1.HousekeepingAdditionalElement.Name);
                            mainTable1.AddCell(item1.HousekeepingAdditionalElement.Price.ToString());
                            mainTable1.AddCell(item1.Qty.ToString());
                            mainTable1.AddCell(item1.Total.ToString());
                        }

                        mainTable.AddCell("");
                        mainTable.AddCell(new PdfPCell(new PdfPTable(mainTable1)) { Colspan = 3, Border = 0 });
                    }
                }
                PdfPCell totalToPay = new PdfPCell(new Phrase("Total to Pay", _standardFont));
                totalToPay.Border = 0;
                PdfPCell totalToPays = new PdfPCell(new Phrase(request.OrderTotal.ToString(), _standardFont));
                totalToPays.Border = 0;

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                mainTable.AddCell(totalToPay);
                mainTable.AddCell("");
                mainTable.AddCell("");
                mainTable.AddCell(totalToPays);

                document.Open();

                Rectangle rect = new Rectangle(577, 825, 18, 15);
                rect.EnableBorderSide(1);
                rect.EnableBorderSide(2);
                rect.EnableBorderSide(4);
                rect.EnableBorderSide(8);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = 1;
                document.Add(rect);

                Paragraph p = new Paragraph();
                document.Add(p);

                document.Add(Chunk.NEWLINE);
                document.Add(receiptTable);

                document.Add(Chunk.NEWLINE);
                document.Add(mainTable);
                document.Close();

                output.Seek(0, SeekOrigin.Begin);
                return output;
            }
        }



        /// <summary>
        /// Send order receipt to email
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <remarks>
        /// Returs the Excursion Order details
        /// </remarks>
        /// <returns>Send order receipt to email</returns>
        [HttpPost]
        [Route("hotel/excursionOrderReceipt")]

        [ResponseType(typeof(object))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Order not found")]
        [SwaggerOperation(Tags = new[] { "Excursion Order" })]
        public IHttpActionResult SendExcursionOrderReceipt(int orderId)
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var order = _hotelService.GetExcursionOrdersDetailsById(orderId);
            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (order.CustomerId != customer.Id)
                return Content(HttpStatusCode.Forbidden, new { messages = Resources.Not_Authorized });

            var excursionOrderDTO = Mapper.Map<ExcursionOrder, ExcursionOrderInfoPdfDTO>(order);
            excursionOrderDTO.ReceiptNumber = "#" + order.Id;

            var r = GenerateExcursionOrderPDF(excursionOrderDTO);
            var task = r.Content.ReadAsStreamAsync();
            task.Wait();
            Stream requestStream = task.Result;
            try
            {
                Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/PDF/" + "ExcursionOrderReceipt" + excursionOrderDTO.ReceiptNumber + ".pdf"));
                requestStream.CopyTo(fileStream);
                fileStream.Close();
                requestStream.Close();

                SendReceipt(customer.Email, excursionOrderDTO.ReceiptNumber, "ExcursionOrderReceipt");
                return Content(HttpStatusCode.OK, new { messages = new string[] { "Mail send Successfullly" } });
            }
            catch (IOException)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PDF already generated or send" } });
            }
        }

        private HttpResponseMessage GenerateExcursionOrderPDF(ExcursionOrderInfoPdfDTO request)
        {
            try
            {
                var stream = CreateExcursionOrderPdf(request);
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(System.Net.Mime.MediaTypeNames.Application.Pdf),
                            ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "PdfFileName.pdf" }
                        }
                    }
                };
                return response;
            }
            catch (ApplicationException ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private Stream CreateExcursionOrderPdf(ExcursionOrderInfoPdfDTO request)
        {
            using (var document = new Document(PageSize.A4, 50f, 50f, 100f, 100f))
            {
                var output = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, output);

                var page = new ITextEvents();
                page.Hotel = request.Hotel;
                writer.PageEvent = page;

                writer.CloseStream = false;
                // set standard font type and size
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                PdfPTable receiptTable = new PdfPTable(2);
                receiptTable.HorizontalAlignment = 1;

                PdfPCell seatNo = new PdfPCell(new Phrase("Seat No.", _standardFont));
                seatNo.Border = 0;
                PdfPCell seatNum = new PdfPCell(new Phrase(request.Id.ToString(), _standardFont));
                seatNum.Border = 0;

                PdfPCell booking = new PdfPCell(new Phrase("Booking", _standardFont));
                booking.Border = 0;
                PdfPCell bookingRe = new PdfPCell(new Phrase(request.OrderDate.ToString(), _standardFont));
                bookingRe.Border = 0;

                PdfPCell receiptNo = new PdfPCell(new Phrase("Receipt No.", _standardFont));
                receiptNo.Border = 0;
                PdfPCell receiptNum = new PdfPCell(new Phrase(request.ReceiptNumber.ToString(), _standardFont));
                receiptNum.Border = 0;

                receiptTable.AddCell(seatNo);
                receiptTable.AddCell(seatNum);

                receiptTable.AddCell(booking);
                receiptTable.AddCell(bookingRe);

                receiptTable.AddCell(receiptNo);
                receiptTable.AddCell(receiptNum);

                PdfPTable mainTable = new PdfPTable(5);
                mainTable.HorizontalAlignment = 1;

                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;

                // here we will add table headers for report details
                PdfPCell serviceName = new PdfPCell(new Phrase("Service Name", _standardFont));
                serviceName.Border = 0;
                PdfPCell price = new PdfPCell(new Phrase("Price", _standardFont));
                price.Border = 0;
                PdfPCell NoOfPerson = new PdfPCell(new Phrase("NoOfPerson", _standardFont));
                NoOfPerson.Border = 0;
                PdfPCell OfferPrice = new PdfPCell(new Phrase("OfferPrice", _standardFont));
                OfferPrice.Border = 0;
                PdfPCell amount = new PdfPCell(new Phrase("Amount", _standardFont));
                amount.Border = 0;

                mainTable.AddCell(serviceName);
                mainTable.AddCell(price);
                mainTable.AddCell(NoOfPerson);
                mainTable.AddCell(OfferPrice);
                mainTable.AddCell(amount);

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                foreach (var item in request.ExcursionOrderItems)
                {
                    mainTable.AddCell(item.ExcursionDetailName);
                    mainTable.AddCell(item.Price.ToString());
                    mainTable.AddCell(item.NoOfPerson.ToString());
                    mainTable.AddCell(item.OfferPrice.ToString());
                    mainTable.AddCell(item.Total.ToString());
                }
                PdfPCell totalToPay = new PdfPCell(new Phrase("Total to Pay", _standardFont));
                totalToPay.Border = 0;
                PdfPCell totalToPays = new PdfPCell(new Phrase(request.Total.ToString(), _standardFont));
                totalToPays.Border = 0;

                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");
                mainTable.AddCell(" ");

                mainTable.AddCell(totalToPay);
                mainTable.AddCell("");
                mainTable.AddCell("");
                mainTable.AddCell(" ");
                mainTable.AddCell(totalToPays);

                document.Open();

                Rectangle rect = new Rectangle(577, 825, 18, 15);
                rect.EnableBorderSide(1);
                rect.EnableBorderSide(2);
                rect.EnableBorderSide(4);
                rect.EnableBorderSide(8);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = 1;
                document.Add(rect);
                
                Paragraph p = new Paragraph();
                document.Add(p);

                document.Add(Chunk.NEWLINE);
                document.Add(receiptTable);

                document.Add(Chunk.NEWLINE);
                document.Add(mainTable);
                document.Close();

                output.Seek(0, SeekOrigin.Begin);
                return output;
            }
        }

        private void SendReceipt(string emailId, string receiptNumber, string type)
        {
            try
            {
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", emailId))
                {
                    mm.Subject = "Order Receipt";
                    mm.IsBodyHtml = false;
                    string filename = HttpContext.Current.Server.MapPath("~/PDF/" + type + receiptNumber + ".pdf");
                    mm.Attachments.Add(new Attachment(filename));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }

        public class ITextEvents : PdfPageEventHelper
        {

            // This is the contentbyte object of the writer  
            PdfContentByte cb;

            // we will put the final number of pages in a template  
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer  
            BaseFont bf = null;

            // This keeps track of the creation time  
            DateTime PrintTime = DateTime.Now;

            #region Fields
            private string _header;

            public HotelPdfDTO _hotel;
            #endregion

            #region Properties
            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            public virtual HotelPdfDTO Hotel
            {
                get { return _hotel; }
                set { _hotel = value; }
            }
            #endregion

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                }
                catch (System.IO.IOException ioe)
                {
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);
                iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                Phrase p1Header = new Phrase(Hotel.Name, baseFontNormal);
                iTextSharp.text.Image logo = null;
                if (!string.IsNullOrWhiteSpace(Hotel.LogoImage))
                {
                    logo = iTextSharp.text.Image.GetInstance(Hotel.LogoImage);

                    logo.ScaleAbsolute(50f, 50f);
                }

                //Create PdfTable object  
                PdfPTable pdfTab = new PdfPTable(3);

                //We will have to create separate cells to include image logo and 2 separate strings  
                //Row 1  
                PdfPCell pdfCell1 = null;
                if (logo != null)
                    pdfCell1 = new PdfPCell(logo);
                else
                    pdfCell1 = new PdfPCell();
                PdfPCell pdfCell2 = new PdfPCell(p1Header);
                PdfPCell pdfCell3 = new PdfPCell();

                //Row 2  
                PdfPCell pdfCell4 = new PdfPCell();
                PdfPCell pdfCell5 = new PdfPCell(new Phrase(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig)));

                //Row 3   
                PdfPCell pdfCell6 = new PdfPCell();
                PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

                //set the alignment of all three cells and set border to 0  
                pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
                pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
                pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfCell1.Rowspan = 3;

                pdfCell1.Border = 0;
                pdfCell2.Border = 0;
                pdfCell3.Border = 0;
                pdfCell4.Border = 0;
                pdfCell5.Border = 0;
                pdfCell6.Border = 0;
                pdfCell7.Border = 0;

                pdfTab.AddCell(pdfCell1);
                pdfTab.AddCell(pdfCell2);
                pdfTab.AddCell(pdfCell3);
                pdfTab.AddCell(pdfCell4);
                pdfTab.AddCell(pdfCell5);
                pdfTab.AddCell(pdfCell6);
                pdfTab.AddCell(pdfCell7);

                pdfTab.TotalWidth = document.PageSize.Width - 80f;
                pdfTab.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable  
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write  
                //Third and fourth param is x and y position to start writing  
                pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
                //set pdfContent value  

                //Move the pointer and draw line to separate header section from rest of page  
                cb.MoveTo(40, document.PageSize.Height - 100);
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                cb.Stroke();

                //Create PdfooTable object for Footer
                PdfPTable pdfooTab = new PdfPTable(1);

                //We will have to create separate cells to include image logo and 2 separate strings  
                PdfPCell pdfooCell1 = new PdfPCell(new Phrase(Hotel.Name, baseFontNormal));
                PdfPCell pdfooCell2 = new PdfPCell(new Phrase(Hotel.Street == null ? Hotel.Street2 : Hotel.Street, baseFontNormal));
                PdfPCell pdfooCell3 = new PdfPCell(new Phrase(Hotel.City, baseFontNormal));
                PdfPCell pdfooCell4 = new PdfPCell(new Phrase(Hotel.Region, baseFontNormal));
                PdfPCell pdfooCell5 = new PdfPCell(new Phrase(Hotel.Telephone != null ? Hotel.Telephone : "", baseFontNormal));

                pdfooCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfooCell5.HorizontalAlignment = Element.ALIGN_CENTER;

                pdfooCell1.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell4.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfooCell5.VerticalAlignment = Element.ALIGN_MIDDLE;

                pdfooCell1.Border = 0;
                pdfooCell2.Border = 0;
                pdfooCell3.Border = 0;
                pdfooCell4.Border = 0;
                pdfooCell5.Border = 0;

                pdfooTab.AddCell(pdfooCell1);
                pdfooTab.AddCell(pdfooCell2);
                pdfooTab.AddCell(pdfooCell3);
                pdfooTab.AddCell(pdfooCell4);
                pdfooTab.AddCell(pdfooCell5);

                pdfooTab.TotalWidth = document.PageSize.Width - 80f;
                pdfooTab.WidthPercentage = 70;

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable  
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write  
                //Third and fourth param is x and y position to start writing  
                pdfooTab.WriteSelectedRows(0, -1, 40, document.PageSize.GetBottom(100), writer.DirectContent);

                //Move the pointer and draw line to separate footer section from rest of page  
                cb.MoveTo(40, document.PageSize.GetBottom(100));
                cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(100));
                cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

                footerTemplate.BeginText();
                footerTemplate.SetFontAndSize(bf, 12);
                footerTemplate.SetTextMatrix(0, 0);
                footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                footerTemplate.EndText();
            }
        }

        #endregion GeneratePDF
    }
}
