﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Api.Models;

namespace ToOrdr.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return RedirectToAction("Index", "Help", new { Areas = "HelpPage" });
        }


        public ActionResult Documentation()
        {

            List<EndPointDetails> detailsList = new List<EndPointDetails> 
            {
                new EndPointDetails()
            };

            return View(detailsList);
        }

        public ActionResult Inx()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
