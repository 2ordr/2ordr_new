﻿using AutoMapper;
using NLog;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ToOrdr.Api.App_Start;
using ToOrdr.Api.DTO;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Exceptions;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;

namespace ToOrdr.Api.Controllers
{
#if !DEBUG
    [Authorize]
#endif

    public class CustomersController : BaseController
    {
        ICustomerService _customerService;
        ICustomer_Restaurant_PreferencesService _customer_Restaurant_PreferencesService;
        ICustomer_Hotel_PreferencesService _customer_Hotel_PreferencesService;
        IRestaurantService _restaurantService;
        IHotelService _hotelService;
        IAboutYouOrdrService _aboutYouOrdrService;
        IPaymentService _paymentService;

        private static Logger logger = LogManager.GetCurrentClassLogger();


        public CustomersController(ICustomerService customerService,
                                   ICustomer_Restaurant_PreferencesService customer_Restaurant_PreferencesService,
                                   ICustomer_Hotel_PreferencesService customer_Hotel_PreferencesService,
                                   IRestaurantService restaurantService,
                                   IHotelService hotelService,
                                   IAboutYouOrdrService aboutYouOrdrService,
                                   IPaymentService paymentService)
        {
            _customerService = customerService;
            _customer_Restaurant_PreferencesService = customer_Restaurant_PreferencesService;
            _customer_Hotel_PreferencesService = customer_Hotel_PreferencesService;
            _restaurantService = restaurantService;
            _hotelService = hotelService;
            _aboutYouOrdrService = aboutYouOrdrService;
            _paymentService = paymentService;

            logger.Info("Customer controller initialized");

        }

        #region About YouOrdr
        /// <summary>
        /// Returns about youOrdr description based on language Id
        /// </summary>
        /// <remarks>
        /// Gets about youOrdr description based on language Id
        /// </remarks>
        /// <param name="id">Language Id</param>
        /// <returns>Returns about youOrdr description based on language Id</returns>
        [HttpGet]
        [Route("customer/aboutYouOrdr/{id}", Name = "GetAboutYouOrdrDescByLangId")]
        [AllowAnonymous]
        [ResponseType(typeof(AboutYouOrdrDescriptionDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "YouOrdr Description Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Issue")]
        [SwaggerOperation(Tags = new[] { "About YouOrdr" })]

        public IHttpActionResult GetAboutYouOrdrDescByLangId(int id)
        {
            try
            {
                var abtYouOrdrDescInSpecifiedLang = _aboutYouOrdrService.GetYouOrdrDescription(id);

                if (abtYouOrdrDescInSpecifiedLang == null)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.AboutYouOrdrDesc_NotFound } });

                var abtYouOrdrDescInSpecifiedLangDTO = Mapper.Map<AboutYouOrdrDescriptionDTO>(abtYouOrdrDescInSpecifiedLang);

                return Content(HttpStatusCode.OK, abtYouOrdrDescInSpecifiedLangDTO);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "YouOrdr Description Not Found");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }
        #endregion About YouOrdr

        #region customers
        /// <summary>
        /// Returns Authenticated Customer details 
        /// </summary>
        /// <remarks>
        /// Returns Authenticated Customer details.Authorisation required to access this method
        /// </remarks>
        /// <returns>Returns Authenticated customer details</returns>

        [HttpGet]
        [Route("customer", Name = "GetCustomerDetails")]
        [ResponseType(typeof(CustomerDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Customer Not Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer" })]
        public IHttpActionResult GetCustomerDetails()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var DTO = AutoMapper.Mapper.Map<CustomerDTO>(authenticatedCustomer);
            return Content(HttpStatusCode.OK, DTO);
        }

        /// <summary>
        /// Modifies existing customer details
        /// </summary>
        /// <param name="customerDTO">Customer details</param>
        /// <returns>returns modifed customer details</returns>
        [Route("customer", Name = "ModifyCustomer")]
        [HttpPut]

        [ResponseType(typeof(CustomerDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer" })]

        public IHttpActionResult ModifyCustomer(ModifyCustomerDTO customerDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var newCustomer = Mapper.Map<Customer>(customerDTO);

            authenticatedCustomer.City = newCustomer.City;
            authenticatedCustomer.Pincode = newCustomer.Pincode;
            authenticatedCustomer.Street = newCustomer.Street;
            authenticatedCustomer.Street2 = newCustomer.Street2;

            if (newCustomer.CountryId > 0)
            {
                var country = _customerService.GetCountryById(Convert.ToInt32(newCustomer.CountryId));
                if (country != null)
                    authenticatedCustomer.CountryId = newCustomer.CountryId;
                else
                    return Content(HttpStatusCode.BadRequest, new { messages = "Invalid Country Id" });
            }
            if (!string.IsNullOrEmpty(newCustomer.Region))
                authenticatedCustomer.Region = newCustomer.Region;

            if (!string.IsNullOrEmpty(newCustomer.Telephone))
                authenticatedCustomer.Telephone = newCustomer.Telephone;

            if (newCustomer.BirthDate.HasValue)
                authenticatedCustomer.BirthDate = newCustomer.BirthDate;

            if (!string.IsNullOrEmpty(newCustomer.FirstName))
                authenticatedCustomer.FirstName = newCustomer.FirstName;

            if (!string.IsNullOrEmpty(newCustomer.LastName))
                authenticatedCustomer.LastName = newCustomer.LastName;

            if (!string.IsNullOrEmpty(newCustomer.Gender))
                authenticatedCustomer.Gender = newCustomer.Gender;

            if (!string.IsNullOrWhiteSpace(newCustomer.PhoneCode))
                authenticatedCustomer.PhoneCode = newCustomer.PhoneCode;

            if (newCustomer.CurrencyId > 0)
            {
                var currency = _customerService.GetCurrencyById(newCustomer.CurrencyId);
                if (currency != null)
                    authenticatedCustomer.CurrencyId = newCustomer.CurrencyId;
                else
                    return Content(HttpStatusCode.BadRequest, new { messages = "Invalid Currency Id" });
            }
            try
            {
                newCustomer = _customerService.ModifyCustomer(authenticatedCustomer);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = ex.Message });
            }

            var newCustomerDTO = Mapper.Map<CustomerDTO>(newCustomer);
            return Ok(newCustomerDTO);
        }

        /// <summary>
        /// Allows to upload profile image (Note: Please test this route on Postman)
        /// </summary>
        /// <returns>returns modifed customer details</returns>
        [ResponseType(typeof(CustomerDTO))]
        [Route("customer/profilepicture")]
        [SwaggerOperation(Tags = new[] { "Customer" })]
        public IHttpActionResult UploadProfilePicture()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Content(HttpStatusCode.UnsupportedMediaType, new { messages = new string[] { Resources.Invalid_MediaType } });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            if (System.Web.HttpContext.Current.Request.Files.Count > 0)
            {
                var uploadedFile = System.Web.HttpContext.Current.Request.Files[0];
                if (uploadedFile.ContentLength <= 2000000)
                {
                    var allowedExtensions = new[] { ".jpg", ".png" };
                    var filename = Path.GetFileName(uploadedFile.FileName);
                    var ext = Path.GetExtension(uploadedFile.FileName);
                    if (allowedExtensions.Contains(ext))
                    {
                        try
                        {
                            var imgPath = "~/Images/Customers/" + authenticatedCustomer.Id.ToString() + "/";
                            string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
                            if (!Directory.Exists(FolderPathOnServer))
                            {
                                Directory.CreateDirectory(FolderPathOnServer);
                            }
                            var path = Path.Combine(FolderPathOnServer, filename);
                            uploadedFile.SaveAs(path);

                            authenticatedCustomer.ProfileImage = filename;
                            var updatedCustomer = _customerService.ModifyCustomer(authenticatedCustomer);
                            var updatedCustomerDTO = Mapper.Map<CustomerDTO>(updatedCustomer);
                            return Ok(updatedCustomerDTO);
                        }
                        catch (Exception ex)
                        {
                            return Content(HttpStatusCode.BadRequest, new { messages = ex.Message });
                        }
                    }
                    else
                    {
                        return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_ImageExtension } });
                    }
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.ImageUploadSizeExceeded } });
                }
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_ProfileImage } });
            }
        }

        #endregion customer

        #region Customer Preferred Food Profile
        /// <summary>
        /// Add preferred food profile 
        /// </summary> 
        /// <remarks>
        /// Add preferred food profile
        /// </remarks>
        ///<param name="FoodProfileId">Food Profile ID</param>
        /// <returns>Preferred food profile </returns>
        [Route("customer/prferred_food_profile")]
        [HttpPost]

        [ResponseType(typeof(CustomerPreferredFoodProfileDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Food Profile Not Found")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "Sets Preferred Food Profile")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Preferred Food Profile" })]

        public IHttpActionResult AddPreferredFoodProfile(int FoodProfileId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var foodProfile = _restaurantService.GetFoodProfileById(FoodProfileId);
            if (foodProfile == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.FoodProfile_NotFound } });

            var existsPrefFoodProfile = _customerService.GetCustomerPreferredFoodProfile(authenticatedCustomer.Id);
            if (existsPrefFoodProfile != null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Exists_PreferredFoodProfile } });

            CustomerPreferredFoodProfile prefFoodProfile = new CustomerPreferredFoodProfile()
            {
                CustomerId = authenticatedCustomer.Id,
                FoodProfileId = FoodProfileId
            };
            var addedPreferredFoodProfile = _customerService.CreateCustomerFoodProfile(prefFoodProfile);
            var prefFoodProfileDTO = Mapper.Map<CustomerPreferredFoodProfileDTO>(addedPreferredFoodProfile);
            return Content(HttpStatusCode.Created, prefFoodProfileDTO);

        }

        /// <summary>
        /// Update preferred food profile 
        /// </summary> 
        /// <remarks>
        /// Update preferred food profile
        /// </remarks>
        /// <param name="FoodProfileId">Food Profile ID</param>
        /// <returns>Updated preferred food profile </returns>
        [Route("customer/prferred_food_profile")]
        [HttpPut]

        [ResponseType(typeof(CustomerPreferredFoodProfileDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Food Profile Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Updated Preferred Food Profile")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Preferred Food Profile" })]

        public IHttpActionResult UpdatePreferredFoodProfile(int FoodProfileId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var foodProfile = _restaurantService.GetFoodProfileById(FoodProfileId);
            if (foodProfile == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.FoodProfile_NotFound } });

            var existsPrefFoodProfile = _customerService.GetCustomerPreferredFoodProfile(authenticatedCustomer.Id);
            if (existsPrefFoodProfile.FoodProfileId == FoodProfileId)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Exists_PreferredFoodProfile } });

            existsPrefFoodProfile.FoodProfileId = FoodProfileId;
            var updatedPreferredFoodProfile = _customerService.UpdateCustPreferredFoodProfile(existsPrefFoodProfile);
            var prefFoodProfileDTO = Mapper.Map<CustomerPreferredFoodProfileDTO>(updatedPreferredFoodProfile);
            return Ok(prefFoodProfileDTO);
        }

        /// <summary>
        /// Returns customer preffered food profile 
        /// </summary>
        /// <remarks>
        /// Returns customer preffered food profile . Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns customer preffered food profile  </returns>
        [HttpGet]
        [Route("customer/prferred_food_profile")]

        [ResponseType(typeof(List<CustomerPreferredFoodProfileDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Food Profile Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Returns Preferred Food Profile")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Preferred Food Profile" })]

        public IHttpActionResult GetPreferredFoodProfile()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var prefFoodProfile = _customerService.GetCustomerPreferredFoodProfile(customer.Id);
            if (prefFoodProfile == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PrefFoodProfile_NotFound } });

            var prefFoodProfileDTO = Mapper.Map<CustomerPreferredFoodProfileDTO>(prefFoodProfile);

            return Content(HttpStatusCode.OK, prefFoodProfileDTO);

        }
        #endregion Customer Preffered Food Profile

        #region bookings
        /// <summary>
        /// Returns authenticated customer all bookings
        /// </summary>
        /// <remarks>
        /// Get a authenticated customer all bookings. Authorization required to access this method.
        /// </remarks>
        /// <returns>Return list of bookings</returns>

        [HttpGet]
        [Route("customers/bookings", Name = "GetBookings")]

        [ResponseType(typeof(List<HotelBookingsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Bookings Found or No Authorisations")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Bookings" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetBookings()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return NotFound();
            }
            var bookings = _customerService.GetBookings(authenticatedCustomer.Id);

            if (bookings == null || bookings.Count() < 1)
                return NotFound();

            var bookingsDTO = Mapper.Map<IEnumerable<Customer_Hotel_Bookings>, List<HotelBookingsDTO>>(bookings);

            return Content(HttpStatusCode.OK, bookingsDTO);
        }


        /// <summary>
        /// Returns Booking by id
        /// </summary>
        /// <remarks>
        /// Get detailed Customer booking information using youORDR Booking ID. Authorization Required.
        /// </remarks>
        /// <param name="id">Booking Id</param>
        /// <returns>Booking details</returns>

        [HttpGet]
        [Route("customers/bookings/{id}", Name = "GetBookingById")]

        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Booking Found")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Bookings" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetBookingById(int id)
        {
            var booking = _customerService.GetBookingById(id);

            if (booking == null)
                return NotFound();

            var bookingsDTO = Mapper.Map<HotelBookingsDTO>(booking);

            return Content(HttpStatusCode.OK, bookingsDTO);
        }



        /// <summary>
        /// Creates a new Hotel booking
        /// </summary>  
        /// <remarks>
        /// Adds new customer Booking in the backend
        /// </remarks>
        /// <param name="hotelBooking">Details of new hotel booking</param>
        /// <returns>Returns url of the new booking in header, and details in the body</returns>
        [HttpPost]
        [Route("customers/bookings")]

        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Bookings" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateBooking(CreateHotelBookingDTO hotelBooking)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_Hotel_Bookings>(hotelBooking);

            entity = _customerService.CreateBooking(entity);

            //return Created(Url.Link("GetBookingById", new { id = entity.Customer_Hotel_Booking_ID }), Mapper.Map<CustomerHotelBookingsDTO>(entity));
            return null;

        }



        /// <summary>
        /// Modifies existing hotel booking
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the customer Booking.
        /// </remarks>
        /// <param name="hotelBooking">Details of booking to be modified</param>
        /// <returns>Details after modification</returns>
        [Route("customers/bookings")]
        [HttpPut]

        [ResponseType(typeof(HotelBookingsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Bookings" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ModifyBooking(ModifyHotelBookingDTO hotelBooking)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_Hotel_Bookings>(hotelBooking);

            entity = _customerService.ModifyBooking(entity);

            return Ok(Mapper.Map<HotelBookingsDTO>(entity));

        }

        /// <summary>
        /// Deletes a hotel booking by id
        /// </summary>
        /// <remarks>
        /// Delete hotel booking details based on booking id.
        /// </remarks>
        /// <param name="hotelBooking">Booking id</param>
        /// <returns>Status Ok on success</returns>

        [Route("customers/bookings/{id}")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Bookings" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteBooking(int id)
        {

            _customerService.DeleteBooking(id);

            return Ok();

        }

        #endregion bookings

        #region restaurant preferences

        /// <summary>
        /// Returns authenticated customer food ingredient preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer food ingredient preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of food Ingredient Preferences</returns>
        [Route("customer/food_ingredient_preferences")]
        [HttpGet]

        [ResponseType(typeof(IngredientCategoriesPrefDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Ingredient Categories Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetFoodIngredientPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            var activeIngredientCategories = _restaurantService.GetAllIngredientCategoriesByType(false);
            if (activeIngredientCategories == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.IngredientCat_NotFound } });
            var prefIngredientDTO = Mapper.Map<IEnumerable<IngredientCategory>, List<IngredientCategoriesPrefDTO>>(activeIngredientCategories);

            var custPrefFoodProfile = _customerService.GetCustomerPreferredFoodProfile(authenticatedCustomer.Id);
            var custIngredientPref = _customerService.GetCustomerIngredientPreferenceByType(authenticatedCustomer.Id, false);

            if (custPrefFoodProfile != null)
            {
                foreach (var ingredientCatDTO in prefIngredientDTO)
                {
                    if (ingredientCatDTO.Ingredients.Count() > 0)
                    {
                        foreach (var eachIngredientDTO in ingredientCatDTO.Ingredients)
                        {

                            foreach (var prefFPIngredient in custPrefFoodProfile.FoodProfile.IngredientFoodProfiles)
                            {
                                if (prefFPIngredient.IngredientId == eachIngredientDTO.Id)
                                    eachIngredientDTO.Include = true;
                                else
                                    continue;
                            }
                            foreach (var eachCustIngredientPref in custIngredientPref)
                            {
                                if (eachCustIngredientPref.IngredientId == eachIngredientDTO.Id)
                                    eachIngredientDTO.Include = eachCustIngredientPref.Include;
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, prefIngredientDTO);
        }

        /// <summary>
        /// Returns authenticated customer drink ingredient preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer drink ingredient preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of drink Ingredient Preferences</returns>
        [Route("customer/drink_ingredient_preferences")]
        [HttpGet]

        [ResponseType(typeof(DrinkGroupsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Ingredient Categories Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetDrinkIngredientPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeIngredientCategories = _restaurantService.GetAllIngredientCategoriesByType(true);
            if (activeIngredientCategories == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.IngredientCat_NotFound } });

            var allGroups = activeIngredientCategories.GroupBy(g => g.DrinkType).OrderBy(o => o.Key).Select(gr => new DrinkGroups { DrinkType = gr.Key, IngredientCategory = gr.ToList() });
            var drinkGroupsDTO = Mapper.Map<IEnumerable<DrinkGroups>, List<DrinkGroupsDTO>>(allGroups).Where(gr => gr.DrinkType != "None");

            var custIngredientPref = _customerService.GetCustomerIngredientPreferenceByType(authenticatedCustomer.Id, true);
            foreach (var eachDrinkGroup in drinkGroupsDTO)
            {
                foreach (var ingredientCatDTO in eachDrinkGroup.IngredientCategory)
                {
                    if (ingredientCatDTO.Ingredients.Count() > 0)
                    {
                        foreach (var eachIngredientDTO in ingredientCatDTO.Ingredients)
                        {
                            foreach (var eachCustIngredientPref in custIngredientPref)
                            {
                                if (eachCustIngredientPref.IngredientId == eachIngredientDTO.Id)
                                    eachIngredientDTO.Include = eachCustIngredientPref.Include;
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, drinkGroupsDTO);
        }

        /// <summary>
        /// Modifies ingredient preference
        /// </summary> 
        /// <remarks>
        /// Modifies any of the information of ingredient preference.
        /// </remarks>
        /// <param name="preferenceDTO">Ingredinet preference</param>
        /// <returns>Returns success</returns>
        [Route("customer/ingredient_preferences")]
        [HttpPut]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Ingredient Not Found")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult ModifyIngredientPreference(ModifyIngredientPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            var ingredient = _restaurantService.GetIngredient(preferenceDTO.IngredientId);
            if (ingredient == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Ingredient_NotFound } });

            try
            {
                var entity = Mapper.Map<CustomerIngredientPreference>(preferenceDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                var result = _customerService.ModifyIngredientPreference(entity);
                var custPrefAllergenConflictList = _customerService.GetCustAllergenByPrefIngredient(authenticatedCustomer.Id, ingredient.Id).ToList();
                if (custPrefAllergenConflictList.Count() > 0 && preferenceDTO.Include != false)
                {
                    string allergenNames = "";
                    foreach (var prefAllergen in custPrefAllergenConflictList)
                    {
                        allergenNames += prefAllergen.Allergen.Name + ",";
                        _customerService.ModifyAllergenPreference(prefAllergen, false);
                    }
                    return Ok("Your preferred " + allergenNames.TrimEnd(',') + " allergen is removed because you have set " + ingredient.Name + " as preferred ingredient");
                }
                else
                {
                    return Ok("Success");
                }
            }
            catch (ArgumentNullException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }


        /// <summary>
        /// Returns authenticated customer allergen preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer allergen preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of allergen preferences of customer</returns>
        [Route("customer/allergen_preferences")]
        [HttpGet]

        [ResponseType(typeof(AllergenCategoriesPrefDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Allergen Categories Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetAllergenPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeAllergenCategories = _restaurantService.GetAllAllergenCategories();
            if (activeAllergenCategories == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.AllergenCat_NotFound } });

            var custAllergenPreferences = _customerService.GetCustomerAllergenPrefernecs(authenticatedCustomer.Id);
            var prefAllergenCatDTO = Mapper.Map<IEnumerable<AllergenCategory>, List<AllergenCategoriesPrefDTO>>(activeAllergenCategories);
            foreach (var eachAllergenCatDTO in prefAllergenCatDTO)
            {
                if (eachAllergenCatDTO.Allergens.Count() > 0)
                {
                    foreach (var eachAllergenDTO in eachAllergenCatDTO.Allergens)
                    {
                        foreach (var allergenPref in custAllergenPreferences)
                        {
                            if (allergenPref.AllergenId == eachAllergenDTO.Id)
                                eachAllergenDTO.Include = true;
                            else
                                continue;
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, prefAllergenCatDTO);
        }

        /// <summary>
        /// Modifies allergen preference
        /// </summary> 
        /// <remarks>
        /// Modifies any of the information of allergen preference.
        /// </remarks>
        /// <param name="preferenceDTO">allergen preference</param>
        /// <returns>Returns success</returns>
        [Route("customer/allergen_preferences")]
        [HttpPut]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Allergen Not Found")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult ModifyAllergenPreference(ModifyAllergenPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            var allergen = _restaurantService.GetAllergenById(preferenceDTO.AllergenId);
            if (allergen == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Allergen_NotFound } });

            try
            {
                var entity = Mapper.Map<CustomerAllergenPreference>(preferenceDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                var result = _customerService.ModifyAllergenPreference(entity, preferenceDTO.Include);
                var allergicIngredients = allergen.AllergenIngredients.Select(a => a.IngredientID).ToList();
                var custPrefIngrConflictList = _customerService.GetCustIngredPrefByAllergicIngredient(authenticatedCustomer.Id, allergicIngredients).ToList();
                if (custPrefIngrConflictList.Count() > 0)
                {
                    string ingredientNames = "";
                    foreach (var prefIngredient in custPrefIngrConflictList)
                    {
                        ingredientNames += prefIngredient.Ingredient.Name + ",";
                        prefIngredient.Include = false;
                        _customerService.ModifyIngredientPreference(prefIngredient);
                    }
                    return Ok("Your like to " + ingredientNames.TrimEnd(',') + " ingredient is removed because you have the allergy to it");
                }
                else
                {
                    return Ok("Success");
                }

            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        /// <summary>
        /// Returns authenticated customer restaurant preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer restaurant preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of Preferences</returns>
        [Route("customer/restaurant_preferences")]
        [HttpGet]

        [ResponseType(typeof(RestaurantPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Preferences Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult GetRestaurantPreferences()
        {
            var imageUrl = Url.Content("/Images/Preferences/");

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var preference = _customerService.GetCustomerRestaurantPreference(authenticatedCustomer.Id);

            if (preference == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });
            RestaurantPreferenceDTO DTO = BuildCustomerRestaurantPreference(authenticatedCustomer, preference, imageUrl);
            return Content(HttpStatusCode.OK, DTO);
        }

        private static RestaurantPreferenceDTO BuildCustomerRestaurantPreference(Customer authenticatedCustomer, Customer_Restaurant_Preference preference, string imageUrl)
        {

            RestaurantPreferenceDTO DTO = new RestaurantPreferenceDTO()
            {
                Groups = new List<RestaurantPreferenceGroupDTO>()
            };

            DTO.Id = authenticatedCustomer.Id;
            RestaurantPreferenceGroupDTO allergensGroup = new RestaurantPreferenceGroupDTO()
            {
                GroupName = "Allergens",
                Description = "I'm allergic to following, so please use this inform while ordering food."
            };
            allergensGroup.SubGroups.AddRange(preference.Allergen_Preference.Select(ap =>
            {
                var newGroup = new RestaurantPreferenceSubGroupDTO
                {
                    SubGroupId = ap.Allergen_ID,
                    Description = ap.Allergen_Desc,
                    SubGroupName = ap.Allergen_Name,
                    Image = ap.Allergen_Image != null ? imageUrl + "Allergens/" + ap.Allergen_ID.ToString() + "/" + ap.Allergen_Image : null
                };

                newGroup.PreferenceItems.Add(new RestaurantPreferenceItemDTO
                {
                    Id = string.Format("{0},{1}", "A", ap.Allergen_ID),
                    ItemName = ap.Allergen_Name,
                    ItemValue = (ap.Id != null) ? true : false,
                    ItemImage = ap.Allergen_Image != null ? imageUrl + "Allergens/" + ap.Allergen_ID.ToString() + "/" + ap.Allergen_Image : null
                });

                //newGroup.PreferenceItems.AddRange(ap.Allergen.Allergen_Ingredient.Select(ai => new RestaurantPreferenceItemDTO { Id = string.Format("{0},{1}", "A", ai.Ingredient.Ingredient_ID), ItemName = ai.Ingredient.Ingredient_Name, ItemValue = true }));

                return newGroup;

            }));

            RestaurantPreferenceGroupDTO ingredientsGroup = new RestaurantPreferenceGroupDTO()
            {
                GroupName = "Ingredients Preferences",
                Description = "My preferences for ingredients",
                SubGroups = new List<RestaurantPreferenceSubGroupDTO>()
            };

            preference.Ingredient_Preference.ToList().ForEach(p =>
            {
                if (p.IsGroup)
                {
                    var newGroup = new RestaurantPreferenceSubGroupDTO
                    {
                        SubGroupId = p.Ingredient_Id,
                        SubGroupName = p.Ingredient_Name,
                        Description = p.Ingredient_Desc,
                        Image = p.Ingredient_Image != null ? imageUrl + "Ingredients/" + p.Ingredient_Id.ToString() + "/" + p.Ingredient_Image : null
                    };

                    newGroup.PreferenceItems.Add(new RestaurantPreferenceItemDTO { Id = string.Format("{0},{1}", "ING", p.Ingredient_Id), ItemName = p.Ingredient_Name, ItemValue = p.Include, ItemImage = p.Ingredient_Image != null ? imageUrl + "Ingredients/" + p.Ingredient_Id.ToString() + "/" + p.Ingredient_Image : null });
                    ingredientsGroup.SubGroups.Add(newGroup);
                }
                else
                {
                    var group = ingredientsGroup.SubGroups.FirstOrDefault(g => g.SubGroupId == p.Parent_Id);
                    if (group == null)  //if this group not created yet
                    {
                        var parentGroup = preference.Ingredient_Preference.FirstOrDefault(pg => pg.Ingredient_Id == p.Parent_Id);

                        if (parentGroup == null)
                            return;

                        var newGroup = new RestaurantPreferenceSubGroupDTO
                        {
                            SubGroupId = p.Parent_Id.Value,
                            SubGroupName = parentGroup.Ingredient_Name,
                            Description = parentGroup.Ingredient_Desc,
                            Image = parentGroup.Ingredient_Image != null ? imageUrl + "Ingredients/" + p.Ingredient_Id.ToString() + "/" + parentGroup.Ingredient_Image : null
                        };

                        newGroup.PreferenceItems.Add(new RestaurantPreferenceItemDTO { Id = string.Format("{0},{1}", "ING", p.Ingredient_Id), ItemName = p.Ingredient_Name, ItemValue = p.Include, ItemImage = p.Ingredient_Image != null ? imageUrl + "Ingredients/" + p.Ingredient_Id.ToString() + "/" + p.Ingredient_Image : null });

                        ingredientsGroup.SubGroups.Add(newGroup);
                    }

                    group.PreferenceItems.Add(new RestaurantPreferenceItemDTO { Id = string.Format("{0},{1}", "ING", p.Ingredient_Id), ItemName = p.Ingredient_Name, ItemValue = p.Include, ItemImage = p.Ingredient_Image != null ? imageUrl + "Ingredients/" + p.Ingredient_Id.ToString() + "/" + p.Ingredient_Image : null });

                }
            });
            DTO.Groups.Add(ingredientsGroup);
            DTO.Groups.Add(allergensGroup);
            return DTO;
        }

        /// <summary>
        /// Returns preference by id
        /// </summary>
        /// <remarks>
        /// Get a detailed of restaurant preference of the customer using Preferences Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Preference Id</param>
        /// <returns>Details of preferences</returns>

        [Route("customer/restaurant_preferences/{id}", Name = "GetRestaurantPreferencesById")]
        [HttpGet]

        [ResponseType(typeof(CustomerRestaurantPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Preferences Found")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantPreferencesById(int id)
        {
            var preference = _customerService.GetRestaurantPreferencesById(id);

            if (preference == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var preferenceDTO = Mapper.Map<CustomerRestaurantPreferenceDTO>(preference);

            return Content(HttpStatusCode.OK, preferenceDTO);
        }


        /// <summary>
        /// Create a new customer preference
        /// </summary>
        /// <remarks>
        /// Adds new customer restaurant preference in the backend
        /// </remarks>
        /// <param name="preferenceDTO">Details in Request body</param>
        /// <returns>Returns url of newly created, also details in request</returns>

        [Route("customer/restaurant_preferences")]
        [HttpPost]
        [ResponseType(typeof(CustomerRestaurantPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateRestaurantPreference(CreateRestaurantPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_Restaurant_Preference>(preferenceDTO);

            entity = _customerService.CreateRestaurantPreference(entity);

            return Created(Url.Link("GetRestaurantPreferencesById", new { id = 1 }), Mapper.Map<CustomerRestaurantPreferenceDTO>(entity));

        }

        /// <summary>
        /// Modifies restaurant preference
        /// </summary> 
        /// <remarks>
        /// Modifies any of the information of the restaurant preference.
        /// </remarks>
        /// <param name="preferenceDTO">New details of preference</param>
        /// <returns>Returns Modifed details</returns>
        [Route("customer/restaurant_preferences")]
        [HttpPut]

        [ResponseType(typeof(RestaurantPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult ModifyRestaurantPreference(ModifyRestaurantPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var imageUrl = Url.Content("/Images/Preferences/");

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var entity = Mapper.Map<CustomerPreferences>(preferenceDTO);
            try
            {
                _customerService.ModifyRestaurantPreference(entity, authenticatedCustomer.Id);
            }
            catch (ArgumentNullException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }

            var preference = _customerService.GetCustomerRestaurantPreference(authenticatedCustomer.Id);
            if (preference == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });
            RestaurantPreferenceDTO DTO = BuildCustomerRestaurantPreference(authenticatedCustomer, preference, imageUrl);
            return Ok(DTO);
        }

        /// <summary>
        /// Deteles restaurant preference
        /// </summary>
        /// <remarks>
        /// Delete restaurant preference details based on preference id.
        /// </remarks>
        /// <param name="preferenceDTO">Id of the preference</param>
        /// <returns></returns>


        [Route("customer/restaurant_preferences/{id}")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Restaurant Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteRestaurantPreference(int id)
        {
            _customerService.DeleteRestaurantPreference(id);
            return Ok();
        }

        #endregion

        #region Favorite Restaurants

        /// <summary>
        /// Togges Favorite
        /// </summary> 
        /// <remarks>
        /// Togges Restarant Favorite status, if exists then removes this restaurant from favorite list, else adds
        /// </remarks>
        /// <param name="RestaurantId">Restaurant Id</param>
        /// <returns>Status</returns>
        [Route("customer/restaurant_favorite")]
        [HttpPost]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Restaurant" })]

        public IHttpActionResult ToggleFavoriteRestaurant(int RestaurantId)
        {
            if (RestaurantId < 1)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_RestaurantId } });

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var resturant = _restaurantService.GetRestaurantById(RestaurantId);
            if (resturant == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurant_Not_Found } });
            }

            // send it to db to modify 
            _customerService.CustomerFavoriteRestaurantToggle(authenticatedCustomer.Id, RestaurantId);

            return Ok("success");

        }


        /// <summary>
        /// Returns authenticated customer favorite restaurants
        /// </summary>
        /// <remarks>
        /// Get authenticated customer's favorite restaurants
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of favorite restaurants</returns>
        [HttpGet]
        [Route("customer/restaurant_favorite")]

        [ResponseType(typeof(List<GetRestaurantsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No favorite restaurants found")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Restaurant" })]

        public IHttpActionResult GetFavoriteRestaurants(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var restaurants = _customerService.GetFavoriteRestaurants(authenticatedCustomer.Id, page.Value, limit.Value, out total);

            if (restaurants == null || restaurants.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Favorite_Restaurant } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantDTO>>(restaurants);
            restaurantsDTO.Select(r => { r.IsFavourite = true; return r; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = restaurantsDTO });
        }




        #endregion Favorite Restaurants

        #region Favorite Hotels

        /// <summary>
        /// Add Favorite hotel
        /// </summary> 
        /// <remarks>
        /// Togges Hotel Favorite status, if exists then removes this hotel from favorite list, else adds
        /// </remarks>
        /// <param name="HotelId">Hotel Id</param>
        /// <returns>Favorite hotel </returns>
        [Route("customer/hotel_favorite")]
        [HttpPost]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Hotel" })]

        public IHttpActionResult ToggleFavoriteHotel(int HotelId)
        {
            if (HotelId < 1)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_HotelId } });

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var hotel = _hotelService.GetHotelById(HotelId);
            if (hotel == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_Not_Found } });
            }

            // send it to db to modify 
            _customerService.CustomerFavoriteHotelToggle(authenticatedCustomer.Id, HotelId);

            return Ok("success");

        }

        /// <summary>
        /// Returns authenticated customer favorite hotels
        /// </summary>
        /// <remarks>
        /// Get authenticated customer's favorite hotels
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of favorite hotels</returns>
        [HttpGet]
        [Route("customer/hotel_favorite")]

        [ResponseType(typeof(List<HotelDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Favorite hotels not found")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Hotel" })]

        public IHttpActionResult GetFavoriteHotels(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var hotels = _customerService.GetFavoriteHotels(authenticatedCustomer.Id, page.Value, limit.Value, out total);

            if (hotels == null || hotels.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.FavoriteHotel_NotFound } });

            var hotelsDTO = Mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(hotels);
            hotelsDTO.Select(r => { r.IsFavourite = true; return r; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = hotelsDTO });
        }
        #endregion Favorite Hotels

        #region hotel preferences

        /// <summary>
        /// Returns list of authenticated customer hotel preferences
        /// </summary> 
        /// <remarks>
        /// Get a list of authenticated customer hotel preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>Details of preferences</returns>
        [Route("customers/hotel_preferences", Name = "GetHotelPreferences")]
        [HttpGet]

        [ResponseType(typeof(CustomerHotelPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "No Preferences Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetHotelPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return NotFound();
            }
            var preferences = _customerService.GetHotelPreferences(authenticatedCustomer.Id);

            if (preferences == null || preferences.Count() < 1)
                return NotFound();

            var preferenceDTO = Mapper.Map<IEnumerable<Customer_Hotel_Preferences>, List<CustomerHotelPreferenceDTO>>(preferences);

            return Content(HttpStatusCode.OK, preferenceDTO);
        }


        /// <summary>
        /// Returns preferences by Id
        /// </summary>
        /// <remarks>
        /// Get a detailed of customer's hotel preferences using Preferences Id. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Preference Id</param>
        /// <returns></returns>

        [Route("customers/hotel_preferences/{id}", Name = "GetHotelPreferencesById")]
        [HttpGet]
        [ResponseType(typeof(CustomerHotelPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, "No Preferences found")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetHotelPreferencesById(int id)
        {
            var preference = _customerService.GetHotelPreferencesById(id);

            if (preference == null)
                return NotFound();

            var preferenceDTO = Mapper.Map<CustomerHotelPreferenceDTO>(preference);

            return Content(HttpStatusCode.OK, preferenceDTO);
        }

        /// <summary>
        /// Creates new hotel preference
        /// </summary>
        /// <remarks>
        /// Adds new customer hotel preference in the backend
        /// </remarks>
        /// <param name="preferenceDTO">Preference details in request body</param>
        /// <returns>Returns url in the header, and details in request body</returns>
        [Route("customers/hotel_preferences")]
        [HttpPost]
        [ResponseType(typeof(CustomerHotelPreferenceDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Validation Errors")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateHotelPreference(CreateHotelPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_Hotel_Preferences>(preferenceDTO);

            entity = _customerService.CreateHotelPreference(entity);

            //return Created(Url.Link("GetHotelPreferencesById", new { id = entity.Customer_Hotel_Preference_ID }), Mapper.Map<CustomerHotelPreferenceDTO>(entity));
            return null;

        }

        /// <summary>
        /// Modifies Hotel Preference
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the Hotel preference.
        /// </remarks>
        /// <param name="preferenceDTO">Modification details in request body</param>
        /// <returns>Returns modified details</returns>
        [Route("customers/hotel_preferences")]
        [HttpPut]
        [ResponseType(typeof(CustomerHotelPreferenceDTO))]

        [SwaggerResponse(HttpStatusCode.BadRequest, "Validation Errors")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ModifyHotelPreference(ModifyHotelPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_Hotel_Preferences>(preferenceDTO);

            entity = _customerService.ModifyHotelPreference(entity);

            return Ok(Mapper.Map<CustomerHotelPreferenceDTO>(entity));

        }

        /// <summary>
        /// Deletes hotel preference
        /// </summary>
        /// <remarks>
        /// Delete hotel preference details based on preference id.
        /// </remarks>
        /// <param name="preferenceDTO">Id of the preferece to be deleted</param>
        /// <returns></returns>
        [Route("customers/hotel_preferences")]
        [HttpDelete]

        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotel Preferences" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteHotelPreference(CustomerHotelPreferenceDTO preferenceDTO)
        {

            _customerService.DeleteHotelPreference(preferenceDTO.Customer_Hotel_Preference_ID);

            return Ok();

        }




        #endregion

        #region orders

        /// <summary>
        /// Returns list of restaurant orders
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of customer restaurant orders. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of customer restaurant orders</returns>
        [Route("customers/restaurant/orders")]
        [HttpGet]
        [ResponseType(typeof(List<RestaurantOrdersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Orders Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]

        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantOrders()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var orders = _customerService.GetRestaurantOrders(authenticatedCustomer.Id);

            if (orders == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var ordersDTO = Mapper.Map<IEnumerable<RestaurantOrder>, List<RestaurantOrdersDTO>>(orders);

            return Content(HttpStatusCode.OK, ordersDTO);
        }

        /// <summary>
        /// Return customers orders for specified restaurant only
        /// </summary>
        /// <remarks>
        /// Get a detailed list of customer restaurant orders using restaurant ID. Authorization required to access this method.
        /// </remarks>
        /// <param name="restaurantId">Restaurant Id</param>
        /// <returns>List of customer restaurant orders</returns>
        [Route("customers/restaurant/{restaurantId}/orders")]
        [HttpGet]
        [ResponseType(typeof(List<RestaurantOrdersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Orders Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantOrders(int restaurantId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var orders = _customerService.GetRestaurantOrders(authenticatedCustomer.Id, restaurantId);

            if (orders == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var ordersDTO = Mapper.Map<IEnumerable<RestaurantOrder>, List<RestaurantOrdersDTO>>(orders);

            return Content(HttpStatusCode.OK, ordersDTO);
        }

        /// <summary>
        /// Return Restaurant order based on id
        /// </summary> 
        /// <remarks>
        /// Get a detailed list of customer's restaurant order using youORDR order ID. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Order Id</param>
        /// <returns>Order</returns>
        [Route("customers/restaurant/orders/{id}", Name = "GetRestaurantOrders")]
        [HttpGet]
        [ResponseType(typeof(RestaurantOrdersDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Restaurant Order Found")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantOrderById(int id)
        {
            var order = _customerService.GetRestaurantOrderById(id);

            if (order == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var orderDTO = Mapper.Map<RestaurantOrdersDTO>(order);

            return Content(HttpStatusCode.OK, orderDTO);
        }


        /// <summary>
        /// Creates a new restuarnat order
        /// </summary>
        /// <remarks>
        /// Adds new restuarnat order in the backend
        /// </remarks>
        /// <param name="orderDTO">New Order details using DTO</param>
        /// <returns>In the response header, url of the newly created resource, and in the body newly created order details</returns>
        [Route("customers/restaurant/orders")]
        [HttpPost]
        [ResponseType(typeof(RestaurantOrdersDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CreateRestaurantOrder(CreateRestaurantOrdersDTO orderDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<RestaurantOrder>(orderDTO);

            entity = _customerService.CreateRestaurantOrder(entity);

            return Created(Url.Link("GetRestaurantOrders", new { id = entity.Id }), Mapper.Map<RestaurantOrdersDTO>(entity));

        }

        /// <summary>
        /// Modifies restaurant order
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the restaurant order.
        /// </remarks>
        /// <param name="orderDTO">Details of order send as put body</param>
        /// <returns>Returns current status of order</returns>
        [Route("customers/restaurant/orders")]
        [HttpPut]
        [ResponseType(typeof(RestaurantOrdersDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ModifyRestaurantOrder(RestaurantOrdersDTO orderDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<RestaurantOrder>(orderDTO);

            entity = _customerService.ModifyRestaurantOrder(entity);

            return Ok(Mapper.Map<RestaurantOrdersDTO>(entity));

        }

        /// <summary>
        /// Deletes restaurant order
        /// </summary>
        /// <remarks>
        /// Delete restaurant order details.
        /// </remarks>
        /// <param name="orderDTO">Need to pass id of the order</param>
        /// <returns>Status 200 when ok</returns>

        [Route("customers/restaurant/orders")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]

        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteRestaurantOrder(RestaurantOrdersDTO orderDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<RestaurantOrder>(orderDTO);

            _customerService.DeleteRestaurantOrder(entity.Id);

            return Ok();

        }

        /// <summary>
        /// Add new Item to order
        /// </summary> 
        /// <remarks>
        /// Adds new Item to order in the backend
        /// </remarks>
        /// <param name="itemDTO">Item details</param>
        /// <returns></returns>
        [Route("customers/restaurant/orders/item")]
        [HttpPost]
        [ResponseType(typeof(CustomerRestaurantOrderItemsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult AddRestaurantOrderItem(CreateRestaurantOrderItemsDTO itemDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_RestaurantOrder_Items>(itemDTO);

            entity = _customerService.AddRestaurantOrderItem(entity);

            return Created(Url.Link("GetRestaurantOrders", new { id = 0 }), Mapper.Map<CustomerRestaurantOrderItemsDTO>(entity));

        }


        /// <summary>
        /// Modifies Order item 
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the restaurant order item.
        /// </remarks>
        /// <param name="itemDTO">Item Details</param>
        /// <returns>Modifed Item details</returns>

        [Route("customers/restaurant/orders/item")]
        [HttpPut]
        [ResponseType(typeof(CustomerRestaurantOrderItemsDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ModifyRestaurantOrderItem(CustomerRestaurantOrderItemsDTO itemDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_RestaurantOrder_Items>(itemDTO);

            entity = _customerService.ModifyRestaurantOrderItem(entity);

            return Ok(Mapper.Map<CustomerRestaurantOrderItemsDTO>(entity));

        }

        /// <summary>
        /// Removes item from order
        /// </summary>
        /// <remarks>
        /// Delete restaurant item details from order.
        /// </remarks>
        /// <param name="itemDTO">Id of the item</param>
        /// <returns>Status ok when success</returns>
        [Route("customers/restaurant/orders/item")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult DeleteRestaurantOrderItem(CustomerRestaurantOrderItemsDTO itemDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var entity = Mapper.Map<Customer_RestaurantOrder_Items>(itemDTO);

            _customerService.RemoveRestaurantOrderItem(itemDTO.Customer_RestaurantOrder_Item_ID);

            return Ok();
        }


        /// <summary>
        /// Get Restaurant Order Item Customization
        /// </summary>
        /// <remarks>
        /// Get a detailed of customer restaurant order Item Customization using youORDR item ID. Authorization required to access this method.
        /// </remarks>
        /// <param name="id">Id of the item</param>
        /// <returns>RestaurantOrder_Item_Customization</returns>
        [Route("customers/restaurant/orders/item/{id}/customisations")]
        [HttpGet]
        [ResponseType(typeof(CustomerRestaurantOrderItemCustomisationDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Not Found")]
        [SwaggerOperation(Tags = new[] { "Customer Orders" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRestaurantOrderItemCustomisation(int id)
        {
            var customisations = _customerService.GetRestaurantItemCustamisations(id);

            if (customisations == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Data_NotFound } });

            var customisationsDTO = Mapper.Map<IEnumerable<Customer_RestaurantOrder_Item_customisation>, List<CustomerRestaurantOrderItemCustomisationDTO>>(customisations);

            return Content(HttpStatusCode.OK, customisationsDTO);
        }


        #endregion

        #region payment cards

        /// <summary>
        /// Returns authenticated customer credit cards details from quickpay API
        /// </summary>
        /// <remarks>
        /// Get authenticated customer credit cards details from quickpay API. Authorization Required.
        /// </remarks>
        /// <returns>List of Customer credit Cards from quickpay API</returns>
        [HttpGet]
        [Route("customer/cards")]

        [ResponseType(typeof(CustomerCreditCardDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Credit card Found or No Authorisation")]
        [SwaggerOperation(Tags = new[] { "Customer Cards" })]

        public IHttpActionResult GetAllCustomerCards()
        {
            try
            {
                var authenticatedCustomer = GetAuthenticatedCustomer();
                if (authenticatedCustomer == null)
                {
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
                }
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var card = _customerService.GetCustomerCards(authenticatedCustomer.Id, ToOrderConfigs.QuickPayAuthToken, quickPayUrl);

                if (card == null || card.Count() == 0)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cards_Not_Found } });

                var customerCreditCardDTO = Mapper.Map<IEnumerable<CustomerPaymentCards>, List<CustomerCreditCardDTO>>(card);

                return Content(HttpStatusCode.OK, customerCreditCardDTO);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Card not found");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Returns customer card details from quickpay API by Card_Id
        /// </summary>
        /// <remarks>
        /// Get detailed card information from quickpay API using youORDR card ID. Authorization Required.
        /// </remarks>
        /// <param name="id">Card Id</param>
        /// <returns>Customer Credit Card Details from quickpay API</returns>
        [HttpGet]
        [Route("customer/cards/{id}", Name = "GetCardById")]

        [ResponseType(typeof(CustomerCreditCardDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Credit card Found")]
        [SwaggerOperation(Tags = new[] { "Customer Cards" })]

        public IHttpActionResult GetCustomerCardById(int id)
        {
            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var card = _customerService.GetCardById(id, ToOrderConfigs.QuickPayAuthToken, quickPayUrl);

                if (card == null)
                    return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Cards_Not_Found } });

                var authenticatedCustomer = GetAuthenticatedCustomer();
                if (authenticatedCustomer == null || card.CustomerId != authenticatedCustomer.Id)
                {
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
                }

                var customerCreditCardDTO = Mapper.Map<CustomerCreditCardDTO>(card);

                return Content(HttpStatusCode.OK, customerCreditCardDTO);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Card not found");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        /// <summary>
        /// Creates new Customer credit Card and save on quickpay API
        /// </summary>
        /// <remarks>
        /// Add and authorize new Customer credit Card details on quickpay API
        /// </remarks>
        /// <param name="createCustomerCreditCardsDTO">Customer credit Card Details</param>
        /// <returns>Returns newly created Customer credit Card details</returns>
        [HttpPost]
        [Route("customer/cards")]

        [ResponseType(typeof(CustomerCreditCardDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Cards" })]

        public IHttpActionResult CreateCustomerCard(CreateCustomerCreditCardDTO createCustomerCreditCardsDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            //if (!IsValidCardNumber(createCustomerCreditCardsDTO.CardNumber))
            //    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Invalid_CardNumber } });


            if (createCustomerCreditCardsDTO.SpendingLimitEnabled.HasValue && createCustomerCreditCardsDTO.SpendingLimitEnabled.Value)
            {
                if (createCustomerCreditCardsDTO.SpendingLimitAmount <= 0)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_SpendingLimitAmount } });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var quickPayToken = ToOrderConfigs.QuickPayAuthToken;
            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var entity = Mapper.Map<CustomerPaymentCards>(createCustomerCreditCardsDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                entity = _customerService.CreateCustomerCard(entity, quickPayToken, quickPayUrl);
                return Created(Url.Link("GetCardById", new { id = entity.Id }), Mapper.Map<CustomerCreditCardDTO>(entity));
            }
            catch (BadRequestException ex)
            {
                logger.Error(ex, "Invalid parameters");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (ForbiddenException ex)
            {
                logger.Error(ex, "Not authorized");
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                logger.Error(ex, "error adding credit card");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }


        /// <summary>
        /// Modifies existing card details on QuickPay API
        /// </summary>
        /// <remarks>
        /// Modifies any of the information of the customer card on quickpay API.
        /// </remarks>
        /// <param name="cardDTO">Customer Credit Card</param>
        /// <returns>Returns modifed card details from quickpay API</returns>
        [Route("customer/cards")]
        [HttpPut]

        [ResponseType(typeof(CustomerCreditCardDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Cards" })]

        public IHttpActionResult ModifyCustomerCard(ModifyCustomerCreditCardDTO cardDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            if (cardDTO.SpendingLimitEnabled.HasValue && cardDTO.SpendingLimitEnabled.Value)
            {
                if (cardDTO.SpendingLimitAmount < 0 || cardDTO.SpendingLimitAmount == null)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_SpendingLimitAmount } });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            try
            {
                var existingCard = _customerService.GetCustomerCardDetailsById(cardDTO.Id);
                if (existingCard == null)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Not_Found_Card } });

                if (existingCard.CustomerId != authenticatedCustomer.Id)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var entity = Mapper.Map<CustomerPaymentCards>(cardDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                var modifiedCard = _customerService.ModifyCustomerCard(entity, existingCard, ToOrderConfigs.QuickPayAuthToken, quickPayUrl);
                return Ok(Mapper.Map<CustomerCreditCardDTO>(modifiedCard));
            }
            catch (BadRequestException ex)
            {
                logger.Error(ex, "Invalid parameters");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
            catch (ForbiddenException ex)
            {
                logger.Error(ex, "Not authorized");
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { ex.Message } });
            }
            catch (NotFoundException ex)
            {
                logger.Error(ex, "Not found");
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { ex.Message } });
            }
            catch (Exception ex)
            {
                logger.Error(ex, "error modifying credit card");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        /// <summary>
        /// Delete Card by id 
        /// </summary>
        /// <remarks>
        /// Delete card details based on card id.
        /// </remarks>
        /// <param name="id">Customer Credit Card ID</param>
        /// <returns>Success</returns>
        [Route("customer/cards/{id}")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Cards" })]

        public IHttpActionResult DeleteCustomerCard(int id)
        {

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            try
            {
                var existingCard = _customerService.GetCustomerCardDetailsById(id);
                if (existingCard == null)
                    return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Not_Found_Card } });

                if (existingCard.CustomerId != authenticatedCustomer.Id)
                    return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

                _customerService.DeleteCustomerCard(existingCard);

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "error Deleting credit card");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        public static bool IsValidCardNumber(string creditCardNumber)
        {
            try
            {
                System.Collections.ArrayList CheckNumbers = new ArrayList();
                int CardLength = creditCardNumber.Length;

                for (int i = CardLength - 2; i >= 0; i = i - 2)
                {
                    CheckNumbers.Add(Int32.Parse(creditCardNumber[i].ToString()) * 2);
                }

                int CheckSum = 0;
                for (int iCount = 0; iCount <= CheckNumbers.Count - 1; iCount++)
                {
                    int _count = 0;
                    if ((int)CheckNumbers[iCount] > 9)
                    {
                        int _numLength = ((int)CheckNumbers[iCount]).ToString().Length;
                        for (int x = 0; x < _numLength; x++)
                        {
                            _count = _count + Int32.Parse(
                                  ((int)CheckNumbers[iCount]).ToString()[x].ToString());
                        }
                    }
                    else
                    {
                        _count = (int)CheckNumbers[iCount];
                    }
                    CheckSum = CheckSum + _count;
                }
                int OriginalSum = 0;
                for (int y = CardLength - 1; y >= 0; y = y - 2)
                {
                    OriginalSum = OriginalSum + Int32.Parse(creditCardNumber[y].ToString());
                }
                return (((OriginalSum + CheckSum) % 10) == 0);
            }
            catch
            {
                return false;
            }
        }

        #endregion cards

        #region Customer Card Spending Limit
        /// <summary>
        /// Check the spending limit of given card Id or assigned card in order
        /// </summary>
        /// <param name="payDTO">Request data</param>
        /// <param name="appService">Select App service</param>
        /// <param name="isTips">Pass true for the tip payment</param>
        /// <returns>Status</returns>
        [Route("customer/order/checkCardSpendingLimit")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Card Spending Limit" })]
        public IHttpActionResult CheckSpendingLimit(CreateOrderTipPayDTO payDTO, AppServices appService, bool isTips = false)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                var quickPayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var response = _paymentService.CheckCardSpendingLimit(payDTO.OrderId, ToOrderConfigs.QuickPayAuthToken, quickPayUrl, payDTO.CardId, authenticatedCustomer, appService, isTips, payDTO.TipAmount);
                return Content(HttpStatusCode.OK, string.Format(response));
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }
        #endregion Customer Card Spending Limit

        #region Customer Busy schedule popup

        /// <summary>
        /// Check the Customer Busy schedule based on Scheduled date
        /// </summary>
        /// <param name="scheduledDate">order scheduled Date</param>
        /// <returns>Status</returns>
        [Route("customer/order/checkBusyScheduled")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Busy Schedule" })]
        public IHttpActionResult CheckBusySchedule(DateTime scheduledDate)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                string message = "You have some task at this time";
                if (authenticatedCustomer.RestaurantOrders.Any(o => o.ScheduledDate == scheduledDate))
                    return Content(HttpStatusCode.OK, string.Format(message));

                else if (authenticatedCustomer.SpaOrder.Any(s => s.SpaOrderItems.Any(o => scheduledDate >= o.StartDateTime && scheduledDate <= o.EndDateTime)))
                    return Content(HttpStatusCode.OK, string.Format(message));

                else if (authenticatedCustomer.LaundryOrder.Any(l => l.LaundryOrderItems.Any(o => o.ScheduledDate == scheduledDate)))
                    return Content(HttpStatusCode.OK, string.Format(message));

                else if (authenticatedCustomer.HousekeepingOrder.Any(l => l.HousekeepingOrderItems.Any(o => o.ScheduleDate == scheduledDate)))
                    return Content(HttpStatusCode.OK, string.Format(message));

                else if (authenticatedCustomer.ExcursionOrder.Any(l => l.ExcursionOrderItems.Any(o => o.ScheduleDate == scheduledDate)))
                    return Content(HttpStatusCode.OK, string.Format(message));

                else
                    return Content(HttpStatusCode.OK, string.Format("Please proceed anyway"));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }

        }

        #endregion Customer Busy schedule popup

        #region paypal

        /// <summary>
        /// API to add or modify paypal details of customer
        /// </summary>
        /// <remarks>
        /// API to add or modify paypal details of customer. 
        /// </remarks>
        /// <param name="createCustomerPaypalDTO">Details such as email and password</param>
        /// <returns>Status message</returns>
        [HttpPost]
        [Route("customer/paypal")]

        [ResponseType(typeof(CreateCustomerPaypalDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Paypal" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult CreatePaypal(CreateCustomerPaypalDTO createCustomerPaypalDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();

                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }

                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            try
            {
                var entity = Mapper.Map<PaypalDetail>(createCustomerPaypalDTO);

                entity.CustomerId = authenticatedCustomer.Id;
                entity.CreationDate = DateTime.UtcNow;

                entity = _customerService.UpdatePaypalDetails(entity);

                return Content(HttpStatusCode.OK, new { messages = new string[] { Resources.PaypalUpdateSuccess } });
            }
            catch (Exception ex)
            {
                logger.Error(ex, "error adding credit card");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }




        /// <summary>
        /// Returns customer paypal email id
        /// </summary>
        /// <remarks>
        /// Returns Customer's paypal email details
        /// </remarks>
        /// <param name="id">Card Id</param>
        /// <returns>Customer Credit Card Details</returns>
        [HttpGet]
        [Route("customer/paypal", Name = "GetPaypalDetails")]

        [ResponseType(typeof(CustomerPaypalDetailsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Paypal Details not found")]
        [SwaggerOperation(Tags = new[] { "Customer Paypal" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult GetCustomerPaypalDetails()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }

            var entity = _customerService.GetPaypalDetails(authenticatedCustomer.Id);

            if (entity == null)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.PaypalDetailsNotFound } });
            }

            var dto = Mapper.Map<CustomerPaypalDetailsDTO>(entity);

            return Content(HttpStatusCode.OK, dto);
        }


        /// <summary>
        /// Delete Paypal details
        /// </summary>
        /// <remarks>
        /// Delete paypal details of customer
        /// </remarks>
        /// <returns>Success</returns>
        [Route("customer/paypal")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Paypal" })]
        [ApiExplorerSettings(IgnoreApi = true)]

        public IHttpActionResult DeletePaypalDetails()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            try
            {
                _customerService.DeletePaypalDetails(authenticatedCustomer.Id);

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "error deleting paypal details");
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }



        #endregion paypal

        #region Customer Active Orders

        /// <summary>
        /// Returns all orders place by customer
        /// </summary>
        /// <remarks>
        /// Returns all orders place by customer
        /// </remarks>
        /// <returns>Returns all orders place by customer</returns>
        [HttpGet]
        [Route("customer/restaurant_orders", Name = "GetCustomerOrders")]

        [ResponseType(typeof(List<RestaurantOrderDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Orders Not Available ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerOrders()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var orders = _restaurantService.GetCustomerOrders(customer.Id);

            if (orders == null || orders.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Order_NotFound } });

            var restaurantOrderDTO = Mapper.Map<IEnumerable<RestaurantOrder>, List<RestaurantOrderDTO>>(orders);
            foreach (var order in restaurantOrderDTO)
            {
                Status_Type ordrStatus = (Status_Type)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();

                foreach (var ord in orders)
                {
                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = (ord.ScheduledDate == null) ? ord.CreationDate : ord.ScheduledDate;
                    }
                }
            }
            return Content(HttpStatusCode.OK, restaurantOrderDTO);
        }

        /// <summary>
        /// Returns active orders and all baskets of customer
        /// </summary>
        /// <remarks>
        /// Return all active orders and all baskets of customer
        /// </remarks>
        /// <returns>Return active orders and all baskets of customer</returns>
        [HttpGet]
        [Route("customer/restaurants_active_orders", Name = "GetCustomerActiveOrdersAndBaskets")]

        [ResponseType(typeof(CustomerActiveOrdersAndBasketsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Orders Not Available ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveOrdersAndBaskets()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeOrdersAndBaskets = _restaurantService.GetCustomerActiveOrders(customer.Id);

            if (activeOrdersAndBaskets.ActiveOrders.Count() == 0 && activeOrdersAndBaskets.ActiveBaskets.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            var activeOrdersAndBasketsDTO = Mapper.Map<CustomerActiveOrdersAndBaskets, CustomerActiveOrdersAndBasketsDTO>(activeOrdersAndBaskets);
            foreach (var order in activeOrdersAndBasketsDTO.ActiveOrders)
            {
                foreach (var ord in activeOrdersAndBaskets.ActiveOrders)
                {
                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = (ord.ScheduledDate == null) ? ord.CreationDate : ord.ScheduledDate;
                    }
                }
                Status_Type ordrStatus = (Status_Type)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            return Content(HttpStatusCode.OK, activeOrdersAndBasketsDTO);
        }

        /// <summary>
        /// Returns all active room bookings of customer
        /// </summary>
        /// <remarks>
        /// Returns all active room bookings of customer
        /// </remarks>
        /// <returns>Returns all active room bookings of customer</returns>
        [HttpGet]
        [Route("customer/hotel_active_bookings", Name = "GetCustomerActiveBookings")]

        [ResponseType(typeof(List<CustomerHotelBookingsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Bookings Not Available ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveBookings()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var roomBooked = _hotelService.GetAllRoomBookingByCustomer(customer.Id);

            if (roomBooked == null || roomBooked.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.RoomBooking_NotFound } });

            var roomBookingDTO = Mapper.Map<IEnumerable<CustomerHotelBooking>, List<CustomerHotelBookingsDTO>>(roomBooked);

            return Content(HttpStatusCode.OK, roomBookingDTO);
        }

        /// <summary>
        /// Returns active spa orders and all baskets of customer
        /// </summary>
        /// <remarks>
        /// Return all active spa orders and all baskets of customer
        /// </remarks>
        /// <returns>Return active spa orders and all baskets of customer</returns>
        [HttpGet]
        [Route("customer/spa_active_orders", Name = "GetCustomerActiveSpaOrdersAndBaskets")]

        [ResponseType(typeof(CustomerActiveSpaOrdersAndBasketsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Active Orders/Basket Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveSpaOrdersAndBaskets()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeSpaOrdersAndBaskets = _hotelService.GetCustomerActiveSpaOrders(customer.Id);

            if (activeSpaOrdersAndBaskets.ActiveSpaOrders.Count() == 0 && activeSpaOrdersAndBaskets.ActiveSpaBaskets.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            var activeOrdersAndBasketsDTO = Mapper.Map<CustomerActiveSpaOrdersAndBaskets, CustomerActiveSpaOrdersAndBasketsDTO>(activeSpaOrdersAndBaskets);
            foreach (var order in activeOrdersAndBasketsDTO.ActiveSpaOrders)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in activeSpaOrdersAndBaskets.ActiveSpaOrders)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = ord.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.preferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                    }
                }
                SpaOrderStatus ordrStatus = (SpaOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            foreach (var activeBasket in activeOrdersAndBasketsDTO.ActiveSpaBaskets)
            {
                double? baseCurrencyRate = 0;
                foreach (var actBask in activeSpaOrdersAndBaskets.ActiveSpaBaskets)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = actBask.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (actBask.Id == activeBasket.Id)
                    {
                        activeBasket.preferredCurrencyTotal = System.Math.Round(Convert.ToDouble(activeBasket.Total * baseCurrencyRate), 2);
                    }
                }
            }
            return Content(HttpStatusCode.OK, activeOrdersAndBasketsDTO);
        }

        /// <summary>
        /// Returns all hotel active bookings and orders of customer
        /// </summary>
        /// <remarks>
        /// Return all hotel active bookings and orders  of customer
        /// </remarks>
        /// <returns>Return hotel active bookings and orders of customer</returns>
        [HttpGet]
        [Route("customer/hotel_active_orders", Name = "GetCustomerActiveBookingsInHotel")]

        [ResponseType(typeof(CustomerActiveBookingsInHotelDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Active Bookings in hotel not found ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveBookingsInHotel()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeBookingsInHotel = _hotelService.GetCustomerActiveBookingsInHotel(customer.Id);

            if (activeBookingsInHotel.SpaOrder.Count() == 0
                //&& activeBookingsInHotel.LaundryBooking.Count() == 0
                && activeBookingsInHotel.CustomerHotelBooking.Count() == 0
                //&& activeBookingsInHotel.TaxiBooking.Count() == 0 
                //&& activeBookingsInHotel.TripBooking.Count() == 0
                )
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.ActiveBookingInHotel_NotFound } });
            }
            var activeBookingsInHotelDTO = Mapper.Map<CustomerActiveBookingsInHotel, CustomerActiveBookingsInHotelDTO>(activeBookingsInHotel);

            return Content(HttpStatusCode.OK, activeBookingsInHotelDTO);
        }

        /// <summary>
        /// Returns all active taxi bookings of customer
        /// </summary>
        /// <remarks>
        /// Returns all active taxi bookings of customer
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Returns all active taxi bookings of customer</returns>
        [HttpGet]
        [Route("customer/active_taxi_bookings", Name = "GetCustomerActiveTaxiBookings")]

        [ResponseType(typeof(List<CustomerTaxiBookingDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Bookings Not Available ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveTaxiBookings(int? page = 1, int? limit = null)
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var taxiBooked = _hotelService.GetAllActiveTaxiBookingOfCustomer(customer.Id, page.Value, limit.Value, out total);

            if (taxiBooked == null || taxiBooked.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.TaxiBooking_NotFound } });

            var taxiBookingDTO = Mapper.Map<IEnumerable<TaxiBooking>, List<CustomerTaxiBookingDTO>>(taxiBooked);
            foreach (var eachBooking in taxiBookingDTO)
            {
                TaxiBookingStatus ordrStatus = (TaxiBookingStatus)Convert.ToInt32(eachBooking.OrderStatus);
                eachBooking.OrderStatus = ordrStatus.ToString();
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = taxiBookingDTO });
        }


        /// <summary>
        /// Returns all active laundry orders and baskets of customer
        /// </summary>
        /// <remarks>
        /// Returns all active laundry orders and baskets of customer
        /// </remarks>
        /// <returns>Returns all active laundry orders and baskets of customer</returns>
        [HttpGet]
        [Route("customer/laundry_active_orders", Name = "GetCustomerActiveLaundryOrders")]

        [ResponseType(typeof(List<CustomerActiveLaundyOrdersAndBasketsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Laundry Active Orders/Basket Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveLaundryOrdersAndBaskets()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeLaundryOrdersAndBasket = _hotelService.GetAllActiveLaundryOrdersOfCustomer(customer.Id);

            if (activeLaundryOrdersAndBasket.ActiveLaundryOrders.Count() == 0 && activeLaundryOrdersAndBasket.ActiveLaundryBaskets.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            var activeOrdersAndBasketsDTO = Mapper.Map<CustomerActiveLaundryOrdersAndBaskets, CustomerActiveLaundyOrdersAndBasketsDTO>(activeLaundryOrdersAndBasket);
            foreach (var order in activeOrdersAndBasketsDTO.ActiveLaundryOrders)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in activeLaundryOrdersAndBasket.ActiveLaundryOrders)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = ord.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        if (baseCurrencyRate != 0)
                        {
                            order.OrderTotalInPreferredCurrency = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                            order.LaundryOrderItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                            order.LaundryOrderItems.Select(i => { i.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryDetail.Price * baseCurrencyRate), 2); return i; }).ToList();
                            order.LaundryOrderItems.Select(i => { i.LaundryOrderItemsAdditional.Select(a => { a.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(a.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return a; }).ToList(); return i; }).ToList();
                        }
                    }
                }
                LaundryOrderStatus ordrStatus = (LaundryOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            foreach (var activeBasket in activeOrdersAndBasketsDTO.ActiveLaundryBaskets)
            {
                double? baseCurrencyRate = 0;
                foreach (var actBask in activeLaundryOrdersAndBasket.ActiveLaundryBaskets)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = actBask.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();
                    activeBasket.PreferredCurrencySymbol = customer.Currency.Symbol;
                    if (actBask.Id == activeBasket.Id)
                    {
                        activeBasket.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(activeBasket.Total * baseCurrencyRate), 2);
                        activeBasket.LaundryCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                        activeBasket.LaundryCartItems.Select(s => { s.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.LaundryDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                        activeBasket.LaundryCartItems.Select(s => { s.LaundryCartItemsAdditional.Select(i => { i.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return i; }).ToList(); return s; }).ToList();
                    }
                }
            }
            return Content(HttpStatusCode.OK, activeOrdersAndBasketsDTO);
        }


        /// <summary>
        /// Returns active housekeeping orders and all baskets of customer
        /// </summary>
        /// <remarks>
        /// Return all active housekeeping orders and all baskets of customer
        /// </remarks>
        /// <returns>Return active housekeeping orders and all baskets of customer</returns>
        [HttpGet]
        [Route("customer/housekeeping_active_orders", Name = "GetCustomerActiveHousekeepingOrdersAndBaskets")]

        [ResponseType(typeof(CustomerActiveHousekeepingOrdersAndBasketsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Housekeeping Active Orders/Basket Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveHousekeepingOrdersAndBaskets()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeHousekeepingOrdersAndBaskets = _hotelService.GetCustomerActiveHousekeepingOrders(customer.Id);

            if (activeHousekeepingOrdersAndBaskets.ActiveHousekeepingOrders.Count() == 0 && activeHousekeepingOrdersAndBaskets.ActiveHousekeepingBaskets.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            var activeOrdersAndBasketsDTO = Mapper.Map<CustomerActiveHousekeepingOrdersAndBaskets, CustomerActiveHousekeepingOrdersAndBasketsDTO>(activeHousekeepingOrdersAndBaskets);
            activeOrdersAndBasketsDTO.ActiveHousekeepingBaskets.Select(s => s.PreferredCurrencySymbol = customer.Currency.Symbol).ToList();

            foreach (var order in activeOrdersAndBasketsDTO.ActiveHousekeepingOrders)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in activeHousekeepingOrdersAndBaskets.ActiveHousekeepingOrders)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = ord.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                        if (order.HousekeepingOrderItems != null && order.HousekeepingOrderItems.Count() > 0)
                        {
                            order.HousekeepingOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            order.HousekeepingOrderItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
                HousekeepingOrderStatus ordrStatus = (HousekeepingOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            foreach (var activeBasket in activeOrdersAndBasketsDTO.ActiveHousekeepingBaskets)
            {
                double? baseCurrencyRate = 0;
                foreach (var actBask in activeHousekeepingOrdersAndBaskets.ActiveHousekeepingBaskets)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = actBask.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (actBask.Id == activeBasket.Id)
                    {
                        activeBasket.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(activeBasket.Total * baseCurrencyRate), 2);
                        if (activeBasket.HousekeepingCartItems != null && activeBasket.HousekeepingCartItems.Count() > 0)
                        {
                            activeBasket.HousekeepingCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            activeBasket.HousekeepingCartItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, activeOrdersAndBasketsDTO);
        }


        /// <summary>
        /// Returns active excursion orders and all baskets of customer
        /// </summary>
        /// <remarks>
        /// Return all active excursion orders and all baskets of customer
        /// </remarks>
        /// <returns>Return active excursion orders and all baskets of customer</returns>
        [HttpGet]
        [Route("customer/excursion_active_orders", Name = "GetCustomerActiveExcursionOrdersAndBaskets")]

        [ResponseType(typeof(CustomerActiveExcursionOrdersAndBasketsDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Active Orders/Basket Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveExcursionOrdersAndBaskets()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeExcursionOrdersAndBaskets = _hotelService.GetCustomerActiveExcursionOrders(customer.Id);

            if (activeExcursionOrdersAndBaskets.ActiveExcursionOrders.Count() == 0 && activeExcursionOrdersAndBaskets.ActiveExcursionBaskets.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            var activeOrdersAndBasketsDTO = Mapper.Map<CustomerActiveExcursionOrdersAndBaskets, CustomerActiveExcursionOrdersAndBasketsDTO>(activeExcursionOrdersAndBaskets);
            activeOrdersAndBasketsDTO.ActiveExcursionBaskets.Select(s => s.PreferredCurrencySymbol = customer.Currency.Symbol).ToList();

            foreach (var order in activeOrdersAndBasketsDTO.ActiveExcursionOrders)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in activeExcursionOrdersAndBaskets.ActiveExcursionOrders)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = ord.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.Total * baseCurrencyRate), 2);
                        if (order.ExcursionOrderItems != null && order.ExcursionOrderItems.Count() > 0)
                        {
                            order.ExcursionOrderItems.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                            order.ExcursionOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            order.ExcursionOrderItems.Select(s => { s.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.OfferPrice * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
                ExcursionOrderStatus ordrStatus = (ExcursionOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            foreach (var activeBasket in activeOrdersAndBasketsDTO.ActiveExcursionBaskets)
            {
                double? baseCurrencyRate = 0;
                foreach (var actBask in activeExcursionOrdersAndBaskets.ActiveExcursionBaskets)
                {
                    if (customer.CurrencyId != null && customer.CurrencyId > 0)
                        baseCurrencyRate = actBask.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (actBask.Id == activeBasket.Id)
                    {
                        activeBasket.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(activeBasket.Total * baseCurrencyRate), 2);
                        if (activeBasket.ExcursionCartItems != null && activeBasket.ExcursionCartItems.Count() > 0)
                        {
                            activeBasket.ExcursionCartItems.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                            activeBasket.ExcursionCartItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            activeBasket.ExcursionCartItems.Select(s => { s.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.OfferPrice * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, activeOrdersAndBasketsDTO);
        }


        /// <summary>
        /// Returns active room service orders of customer
        /// </summary>
        /// <remarks>
        /// Return all active room service orders of customer
        /// </remarks>
        /// <returns>Return active room service orders of customer</returns>
        [HttpGet]
        [Route("customer/roomservice_active_orders", Name = "GetCustomerActiveRoomServiceOrders")]

        [ResponseType(typeof(List<CustomerActiveOrdersAndBasketsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Orders Not Available ")]
        [SwaggerOperation(Tags = new[] { "Customer Active Orders" })]
        public IHttpActionResult GetCustomerActiveRoomServiceOrders()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeRoomserviceOrders = _restaurantService.GetCustomerActiveRoomServiceOrders(customer.Id);
            if (activeRoomserviceOrders.ActiveOrders.Count() == 0 && activeRoomserviceOrders.ActiveBaskets.Count() == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.ActiveOrdersAndBaskets_NotFound } });

            //var activeRoomserviceOrdersDTO = Mapper.Map<IEnumerable<RestaurantOrder>, List<RestaurantOrderDTO>>(activeRoomserviceOrders);
            var activeRoomserviceOrdersDTO = Mapper.Map<CustomerActiveOrdersAndBaskets, CustomerActiveOrdersAndBasketsDTO>(activeRoomserviceOrders);
            foreach (var order in activeRoomserviceOrdersDTO.ActiveOrders)
            {
                foreach (var ord in activeRoomserviceOrders.ActiveOrders)
                {
                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = (ord.ScheduledDate == null) ? ord.CreationDate : ord.ScheduledDate;
                    }
                }
                Status_Type ordrStatus = (Status_Type)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            return Content(HttpStatusCode.OK, activeRoomserviceOrdersDTO);
        }

        #endregion

        #region Customer Hotel/Restaurant Orders
        /// <summary>
        /// Returns list of all todays and later hotels/restaurants oders of customer
        /// </summary> 
        /// <remarks>
        /// Returns list of all todays and later hotels/restaurants oders of customer. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of all todays and later hotels/restaurants oders of customer</returns>
        [Route("customer/allOrders", Name = "GetAllOrdersOfCustomer")]
        [HttpGet]

        [ResponseType(typeof(List<AllRestaurantsAndHotelsOrdersDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Oders not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotels/Restaurants Orders" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetAllOrdersOfCustomer()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var custAllOrders = _customerService.GetAllHotelAndRestOrdersOfCustomer(authenticatedCustomer.Id);
            if (custAllOrders == null || custAllOrders.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.CustomerOrders_NotFound } });

            var allOrdersDTO = Mapper.Map<IEnumerable<CustomerAllHotelAndRestActiveOrders>, List<AllRestaurantsAndHotelsOrdersDTO>>(custAllOrders);

            return Content(HttpStatusCode.OK, allOrdersDTO);
        }

        /// <summary>
        /// update restaurant order table status
        /// </summary>
        /// <remarks>
        /// update restaurant order table status
        /// </remarks>
        /// <param name="orderId">Restaurant OrderId</param>
        /// <param name="tableStatus">Table Status</param>
        /// <returns>returns updated restaurant table details</returns>
        [Route("customer/order/changeTableStatus")]
        [HttpPut]

        [ResponseType(typeof(RestaurantTablesDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Order Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotels/Restaurants Orders" })]
        public IHttpActionResult ModifyRestaurantTableStatus(int orderId, RestaurantTableStatus tableStatus = RestaurantTableStatus.ClearTable)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var restOrder = _restaurantService.GetRestaurantOrderById(orderId);
            if (restOrder == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (restOrder.CustomerTableNumber == null && restOrder.RoomId != null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.RoomService_Order } });

            if (restOrder.CustomerTableNumber == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.TableId_NotFound } });

            var tableDetails = _restaurantService.GetRestaurantTableById(Convert.ToInt32(restOrder.CustomerTableNumber));
            if (tableDetails == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.RestTable_NotFound } });

            tableDetails.TableStatus = Convert.ToInt32(tableStatus);

            var results = _restaurantService.ModifyRestaurantTable(tableDetails);
            Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();
            Hubs.NotificationHub.NotifyStatusChangeToKitchenAdmin();

            var tableDTO = Mapper.Map<RestaurantTable, RestaurantTablesDTO>(results);
            RestaurantTableStatus tblStatus = (RestaurantTableStatus)results.TableStatus;
            tableDTO.TableStatus = tblStatus.ToString();
            return Content(HttpStatusCode.OK, new { status = "Success", Result = tableDTO });
        }

        /// <summary>
        /// Call next serving from the restaurant order
        /// </summary>
        /// <remarks>
        /// Call next serving from the restaurant order
        /// </remarks>
        /// <param name="id">Restaurant OrderId</param>
        /// <returns>Return next serving set response</returns>
        [Route("customer/order/{id}/next_serving")]
        [HttpPut]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Order Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotels/Restaurants Orders" })]
        public IHttpActionResult GetNextServingFromOrder(int id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var restOrder = _restaurantService.GetRestaurantOrderById(Convert.ToInt32(id));
            if (restOrder == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Order_NotFound } });

            if (restOrder.CustomerTableNumber == null && restOrder.RoomId != null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.RoomService_Order } });

            if (restOrder.CustomerTableNumber == null)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.TableId_NotFound } });

            if (restOrder.RestaurantOrderItems.Count(oi => oi.ServedStatus == false) <= 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.OrderServing_AlreadyDelivered } });
            try
            {
                restOrder.NextServingStatus = true;
                _customerService.ModifyRestaurantOrder(restOrder);
                Hubs.OrderHub.NotifyCurrentEmployeeInformationToAllClients();
                Hubs.NotificationHub.NotifyStatusChangeToKitchenAdmin();
                return Content(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = ex.Message });
            }
        }

        /// <summary>
        /// Returns list of hotels/restaurants oders history of customer
        /// </summary> 
        /// <remarks>
        /// Returns list of hotels/restaurants oders history of customer. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of hotels/restaurants oders history of customer</returns>
        [Route("customer/orders_history", Name = "GetCustomerOrdersHistory")]
        [HttpGet]

        [ResponseType(typeof(List<CustomerRestAndHotelsOrdersHistoryDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Oders History Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotels/Restaurants Orders" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetCustomerOrdersHistory()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var custOrdersHistory = _customerService.GetCustomerRestAndHotelsOrdersHistory(authenticatedCustomer.Id);
            if (custOrdersHistory == null || custOrdersHistory.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.CustOrdersHistory_NotFound } });

            var custOrdersHistoryDTO = Mapper.Map<IEnumerable<CustomerAllHotelAndRestOrderHistory>, List<CustomerRestAndHotelsOrdersHistoryDTO>>(custOrdersHistory);

            return Content(HttpStatusCode.OK, custOrdersHistoryDTO);
        }

        /// <summary>
        /// Deletes hotels/restaurants oders history and/or active orders of customer
        /// </summary> 
        /// <remarks>
        /// Deletes hotels/restaurants oders history and/or active orders of customer. Authorization required to access this method.
        /// </remarks>
        /// <param name="orderType">order type</param>
        /// <param name="OrderItemId">Order Item Id </param>
        /// <returns>Deletes hotels/restaurants oders history and/or active orders of customer</returns>
        [Route("customer/orders", Name = "DeleteCustOrdersHistoryAndActiveOrders")]
        [HttpDelete]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Oders History Not Found")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Bad Request")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Hotels/Restaurants Orders" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult DeleteCustOrdersHistoryAndActiveOrders(string orderType, int OrderItemId)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            try
            {
                _customerService.DeleteCustOrdersHistoryAndActiveOrders(orderType, OrderItemId);
                return Content(HttpStatusCode.OK, "Success");
            }
            catch (NotFoundException ex)
            {
                return Content(HttpStatusCode.NotFound, new { messages = ex.Message });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = ex.Message });
            }

        }
        #endregion Customer Hotel/Restaurant Orders

        #region currency

        /// <summary>
        /// Returns all currencies
        /// </summary>
        /// <remarks>
        /// Returns all currencies
        /// </remarks>
        /// <returns> List of currencies</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("currencies", Name = "GetAllCurrencies")]
        [ResponseType(typeof(List<GetCurrencyDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Currencies not found")]
        [SwaggerOperation(Tags = new[] { "Currency" })]

        public IHttpActionResult GetAllCurrencies()
        {
            var entity = _customerService.GetAllCurrencies();
            if (entity == null || entity.Count() < 1)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.CurrenciesNotFound } });
            }

            var currenciesDTO = Mapper.Map<IEnumerable<Currency>, List<GetCurrencyDTO>>(entity);

            return Content(HttpStatusCode.OK, currenciesDTO);
        }


        /// <summary>
        /// Returns currency by Id
        /// </summary>
        /// <remarks>
        /// Get detailed currency information using currency Id. Authorization Required.
        /// </remarks>
        /// <param name="id">Currency Id</param>
        /// <returns>Currency Details</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("currencies/{id}", Name = "GetCurrencyById")]
        [ResponseType(typeof(GetCurrencyDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No Currency Found")]
        [SwaggerOperation(Tags = new[] { "Currency" })]

        public IHttpActionResult GetCurrencyById(int id)
        {
            var currency = _customerService.GetCurrencyById(id);
            if (currency == null)
            {
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.CurrenciesNotFound } });
            }
            var currencyDTO = Mapper.Map<GetCurrencyDTO>(currency);

            return Content(HttpStatusCode.OK, currencyDTO);
        }

        #endregion currency

        #region Customer reviews

        /// <summary>
        /// Returns customer restaurants reviews
        /// </summary>
        /// <remarks>
        /// Get customer restaurants reviews. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns restaurants reviews</returns>
        [HttpGet]
        [Route("customer/restaurant_reviews")]
        [ResponseType(typeof(List<CustomerRestaurantReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No reviews Found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetCustomerRestaurantReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var restReviews = _restaurantService.GetCustomerRestaurantReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (restReviews == null || restReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var restReviewsDTO = Mapper.Map<IEnumerable<RestaurantReview>, List<CustomerRestaurantReviewDTO>>(restReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = restReviewsDTO });
        }


        /// <summary>
        /// Returns customer hotels reviews
        /// </summary>
        /// <remarks>
        /// Get customer hotels reviews. Authorization required to access this method.
        /// </remarks>
        /// <returns>Return list of hotels reviews</returns>
        [HttpGet]
        [Route("customer/hotel_reviews")]
        [ResponseType(typeof(List<CustomerHotelReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No reviews Found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetCustomerhotelReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var hotelReviews = _hotelService.GetCustomerHotelReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (hotelReviews == null || hotelReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var hotelReviewsDTO = Mapper.Map<IEnumerable<HotelReview>, List<CustomerHotelReviewDTO>>(hotelReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = hotelReviewsDTO });
        }

        /// <summary>
        /// Returns customer menu item reviews
        /// </summary>
        /// <remarks>
        /// Get customer menu itmem reviews. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns menu item reviews</returns>
        [HttpGet]
        [Route("customer/menu_item_reviews")]
        [ResponseType(typeof(List<CustomerMenuItemReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No reviews Found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetCustomerMenuItemReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var menuItemReviews = _restaurantService.GetCustomerMenuItemReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (menuItemReviews == null || menuItemReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var menuIemReviewsDTO = Mapper.Map<IEnumerable<MenuItemReviewAndRestDetails>, List<CustomerMenuItemReviewDTO>>(menuItemReviews);
            foreach (var reviewMenuItems in menuItemReviews.Select(r => r.RestaurantMenuItem).Distinct())
            {
                foreach (var menuItems in menuIemReviewsDTO.Select(r => r.RestaurantMenuItem).Distinct())
                {
                    if (reviewMenuItems.Id == menuItems.Id)
                    {
                        menuItems.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;
                        double? baseCurrencyRate = 0;
                        if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        {
                            baseCurrencyRate = reviewMenuItems.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();
                            menuItems.PreferredCurrencyPrice = System.Math.Round((menuItems.Price * Convert.ToDouble(baseCurrencyRate)), 2);
                        }
                        foreach (var itemImage in menuItems.RestaurantMenuItemImages.Distinct())
                        {
                            if (!string.IsNullOrWhiteSpace(itemImage.Image))
                                itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                        }
                    }
                }
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuIemReviewsDTO });
        }

        /// <summary>
        /// Returns customer spa service reviews
        /// </summary>
        /// <remarks>
        /// Get customer spa service reviews. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns spa service reviews</returns>
        [HttpGet]
        [Route("customer/spa_service_reviews")]
        [ResponseType(typeof(List<CustomerSpaServiceReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No reviews Found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetCustomerSpaServiceReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var spaServiceReviews = _hotelService.GetCustomerSpaServiceReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (spaServiceReviews == null || spaServiceReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var spaServiceReviewsDTO = Mapper.Map<IEnumerable<SpaServiceReview>, List<CustomerSpaServiceReviewDTO>>(spaServiceReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = spaServiceReviewsDTO });
        }


        /// <summary>
        /// Returns list of restaurants which not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of restaurants. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of restaurants</returns>
        [HttpGet]
        [Route("customer/pendingReviewRestaurant")]
        [ResponseType(typeof(List<RestaurantDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Restaurants with pending reviews not found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetPendingReviewsRestaurant(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var restaurants = _restaurantService.GetPendingReviewsRestaurants(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (restaurants == null || restaurants.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Restaurants_PendingReviews } });

            var restaurantsDTO = Mapper.Map<IEnumerable<Restaurant>, List<RestaurantDTO>>(restaurants);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = restaurantsDTO });
        }



        /// <summary>
        /// Returns list of MenuItems which not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of MenuItems. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of MenuItems</returns>
        [HttpGet]
        [Route("customer/pendingReviewMenuItem")]
        [ResponseType(typeof(List<RestMenuItemsPendingReviewDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Menu Items with pending reviews not found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetPendingReviewsMenuItem(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var menuitems = _restaurantService.GetPendingReviewsMenuItems(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (menuitems == null || menuitems.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.MenuItems_PendingReviews } });

            var menuitemsDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestMenuItemsPendingReviewDTO>>(menuitems);
            //var favMenuItems = _customerService.GetCustomerFavoriteMenuItems(authenticatedCustomer.Id);

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = menuitems.Select(m => m.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();
            menuitemsDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();
            if (baseCurrencyRate != 0)
                menuitemsDTO.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();

            //menuitemsDTO.Select(i => { i.IsFavourite = false; return i; }).ToList();
            foreach (var item in menuitemsDTO)
            {
                //if (favMenuItems.Any(r => r.Id == item.Id)) item.IsFavourite = true;
                foreach (var itemImage in item.RestaurantMenuItemImages)
                {
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                        itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                }
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuitemsDTO });
        }



        /// <summary>
        /// Returns list of Hotels which not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of Hotels. Authorization required to access this method.
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page size</param>
        /// <returns>Returns list of Hotels</returns>
        [HttpGet]
        [Route("customer/pendingReviewHotels")]
        [ResponseType(typeof(List<HotelDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Hotels with pending reviews not found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetPendingReviewsHotels(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var hotels = _hotelService.GetPendingReviewsHotels(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (hotels == null || hotels.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Hotel_PendingReviews } });

            var hotelsDTO = Mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(hotels);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = hotelsDTO });
        }



        /// <summary>
        /// Returns list of Spa service details which are order but not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of Spa service details pending reviews. Authorization required to access this method.
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page size</param>
        /// <returns>Returns list of Spa service details pending reviews</returns>
        [HttpGet]
        [Route("customer/pendingSpaServiceReviews")]
        [ResponseType(typeof(List<SpaServiceDetailWithImagesDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Service Detail pending reviews not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetPendingSpaServiceDetReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaSerDetPendingToReview = _customerService.GetSpaServiceDetailPendingReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (spaSerDetPendingToReview == null || spaSerDetPendingToReview.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.SpaSerDetPendinReview_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaSerDetPendingToReview.Select(s => s.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var spaSerDetPendingToReviewDTO = Mapper.Map<IEnumerable<SpaServiceDetail>, List<SpaServiceDetailWithImagesDTO>>(spaSerDetPendingToReview);
            var favSpaServDetails = _customerService.GetCustomerFavoriteSpaServiceDetails(authenticatedCustomer.Id);
            spaSerDetPendingToReviewDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();

            spaSerDetPendingToReviewDTO.Select(i => { i.IsFavourite = false; return i; }).ToList();
            foreach (var item in spaSerDetPendingToReviewDTO)
            {
                if (favSpaServDetails.Any(r => r.Id == item.Id))
                    item.IsFavourite = true;
                item.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(item.Price * baseCurrencyRate), 2);
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = spaSerDetPendingToReviewDTO });
        }

        /// <summary>
        /// Returns list of trip services which are not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of trip services. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of trip services which are not reviewed by customer</returns>
        [HttpGet]
        [Route("customer/pendingTripServiceReviews")]
        [ResponseType(typeof(List<HotelTripsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Trip service reviews not found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetPendingTripServiceReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var pendTripServiceDetReview = _hotelService.GetPendingTripServiceReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (pendTripServiceDetReview == null || pendTripServiceDetReview.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Pending reviews not found" } });

            var pendTripServiceDetReviewDTO = Mapper.Map<IEnumerable<Trip>, List<HotelTripsDTO>>(pendTripServiceDetReview);
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = pendTripServiceDetReviewDTO });
        }

        /// <summary>
        /// Returns list of taxi services which are not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of taxi services. Authorization required to access this method.
        /// </remarks>
        /// <returns>Returns list of taxi services which are not reviewed by customer</returns>
        [HttpGet]
        [Route("customer/pendingTaxiServiceReviews")]
        [ResponseType(typeof(List<TaxiDetailDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Taxi service reviews not found")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetPendingTaxiServiceReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
            {
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
            }
            var pendTaxiServiceDetReview = _hotelService.GetPendingTaxiServiceReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (pendTaxiServiceDetReview == null || pendTaxiServiceDetReview.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Pending reviews not found" } });

            var pendTaxiServiceDetReviewDTO = Mapper.Map<IEnumerable<TaxiDetail>, List<TaxiDetailDTO>>(pendTaxiServiceDetReview);
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = pendTaxiServiceDetReviewDTO });
        }

        ///// <summary>
        ///// Returns list of laundry services which are not reviewed by customer
        ///// </summary>
        ///// <remarks>
        ///// Get List of laundry services. Authorization required to access this method.
        ///// </remarks>
        ///// <returns>Returns list of laundry services which are not reviewed by customer</returns>
        //[HttpGet]
        //[Route("customer/pendingLaundryServiceReviews")]
        //[ResponseType(typeof(List<HotelLaundryDTO>))]
        //[SwaggerResponse(HttpStatusCode.NotFound, Description = "Laundry service reviews not found")]
        //[SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IHttpActionResult GetPendingLaundryServiceReviews(int? page = 1, int? limit = null)
        //{
        //    page = page <= 0 ? 1 : page;
        //    limit = limit ?? ToOrderConfigs.DefaultPageSize;
        //    int total;
        //    var authenticatedCustomer = GetAuthenticatedCustomer();
        //    if (authenticatedCustomer == null)
        //    {
        //        return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });
        //    }
        //    var pendLaundryServiceDetReview = _hotelService.GetPendingLaundryServiceReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
        //    if (pendLaundryServiceDetReview == null || pendLaundryServiceDetReview.Count() < 1)
        //        return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Pending reviews not found" } });

        //    var pendLaundryServiceDetReviewDTO = Mapper.Map<IEnumerable<Laundry>, List<HotelLaundryDTO>>(pendLaundryServiceDetReview);
        //    var mod = total % limit.Value;
        //    var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
        //    return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = pendLaundryServiceDetReviewDTO });
        //}


        /// <summary>
        /// Returns list of hotel all services reviews of particular customer
        /// </summary> 
        /// <remarks>
        /// Returns list of hotel all services reviews of particular customer. Authorization required to access this method.
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page Size</param>
        /// <returns>Returns list of all services reviews of customer</returns>
        [Route("customer/allServicesReviews", Name = "GetHotelAllServicesReviews")]
        [HttpGet]

        [ResponseType(typeof(List<HotelAllServicesReviewsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Service reviews not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        [ApiExplorerSettings(IgnoreApi = false)]
        public IHttpActionResult GetHotelAllServicesReviews(int? page = 1, int? limit = null)
        {
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var serviceReviews = _customerService.GetHotelAllServicesReviewsOfCust(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (serviceReviews == null || serviceReviews.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Review_NotFound } });

            var serviceReviewsDTO = Mapper.Map<IEnumerable<HotelAllServicesReviews>, List<HotelAllServicesReviewsDTO>>(serviceReviews);

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page, result = serviceReviewsDTO });
        }


        /// <summary>
        /// Returns list of Excursion details which are order but not reviewed by customer
        /// </summary>
        /// <remarks>
        /// Get List of Excursion details pending reviews. Authorization required to access this method.
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">Page size</param>
        /// <returns>Returns list of Excursion details pending reviews</returns>
        [HttpGet]
        [Route("customer/pendingExcursionDetailsReviews")]
        [ResponseType(typeof(List<ExcursionDetailsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Detail pending reviews not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Reviews" })]
        public IHttpActionResult GetPendingExcursionDetailsReviews(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionDetPendingToReview = _customerService.GetExcursionDetailPendingReviews(authenticatedCustomer.Id, page.Value, limit.Value, out total);
            if (excursionDetPendingToReview == null || excursionDetPendingToReview.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetPendinReview_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = excursionDetPendingToReview.Select(s => s.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var excursionDetPendingToReviewDTO = Mapper.Map<IEnumerable<ExcursionDetail>, List<ExcursionDetailsDTO>>(excursionDetPendingToReview);

            excursionDetPendingToReviewDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();
            var favExcursionDetails = _customerService.GetCustomerFavoriteExcursionDetails(authenticatedCustomer.Id);

            excursionDetPendingToReviewDTO.Select(i => { i.IsFavourite = false; return i; }).ToList();
            foreach (var item in excursionDetPendingToReviewDTO)
            {
                if (favExcursionDetails.Any(r => r.Id == item.Id))
                    item.IsFavourite = true;
                item.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(item.Price * baseCurrencyRate), 2);
            }

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = excursionDetPendingToReviewDTO });
        }
        #endregion  Customer reviews

        #region Favorite MenuItem

        /// <summary>
        /// Togges Favorite
        /// </summary> 
        /// <remarks>
        /// Togges MenuItem Favorite status, if exists then removes this item from favorite list, else adds
        /// </remarks>
        /// <param name="menuItemId">MenuItemId</param>
        /// <returns>Status</returns>
        [Route("customer/menuItem_favorite")]
        [HttpPost]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite MenuItem" })]

        public IHttpActionResult ToggleFavoriteMenuItem(int menuItemId)
        {
            if (menuItemId < 1)
                return BadRequest();

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            int? languageId = null;
            var menuItem = _restaurantService.GetMenuItem(menuItemId, languageId);
            if (menuItem == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_RestMenuItem } });

            // send it to db to modify 
            _customerService.CustomerFavoriteMenuItemToggle(authenticatedCustomer.Id, menuItemId);

            return Ok("success");
        }


        /// <summary>
        /// Returns authenticated customer favorite menuitems
        /// </summary>
        /// <remarks>
        /// Get authenticated customer's favorite menuitems
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of favorite menuitems</returns>
        [HttpGet]
        [Route("customer/menuItem_favorite")]

        [ResponseType(typeof(List<RestaurantFavoriteMenuItemsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No favorite menu item found")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite MenuItem" })]

        public IHttpActionResult GetFavoriteMenuItems(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var menuItems = _customerService.GetCustomerFavoriteMenuItems(authenticatedCustomer.Id, page.Value, limit.Value, out total);

            if (menuItems == null || menuItems.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Favorite_ManuItem } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = menuItems.Select(m => m.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            var menuItemsDTO = Mapper.Map<IEnumerable<RestaurantFavoriteMenuItems>, List<RestaurantFavoriteMenuItemsDTO>>(menuItems);
            menuItemsDTO.Select(m => { m.IsFavourite = true; return m; }).ToList();
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
            {
                menuItemsDTO.Select(m => { m.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(m.Price) * baseCurrencyRate), 2); return m; }).ToList();
                menuItemsDTO.Select(m => { m.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return m; }).ToList();
            }

            foreach (var menuItem in menuItemsDTO)
            {
                foreach (var itemImage in menuItem.RestaurantMenuItemImages)
                {
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                        itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                }

                var orderDetail = _customerService.GetCustomerOrderDetails(authenticatedCustomer.Id, menuItem.Id);
                menuItem.TotalOrders = orderDetail.TotalOrders;
                menuItem.LastOrder = orderDetail.LastOrder;
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemsDTO });
        }

        #endregion Favorite MenuItem

        #region Country

        /// <summary>
        /// Returns List of Countries
        /// </summary>
        /// <remarks>
        /// Get list of Countries
        /// </remarks>
        /// <returns>List of Countries</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("countries")]

        [ResponseType(typeof(List<CountryDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No country found")]
        [SwaggerOperation(Tags = new[] { "Country" })]

        public IHttpActionResult GetAllCountry()
        {
            var countries = _customerService.GetAllCountries().OrderBy(c => c.Name);

            if (countries == null || countries.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { "Countries not found" } });

            var countriesDTO = Mapper.Map<IEnumerable<Country>, List<CountryDTO>>(countries);

            return Content(HttpStatusCode.OK, new { Countries = countriesDTO });

        }

        #endregion Country

        #region Menu Item Preferences
        /// <summary>
        /// Returns list of Menu Items based on preferences
        /// </summary>
        /// <remarks>
        /// Get list of Menu Items based on preferences
        /// </remarks>
        /// <param name="id">Restaurant Id</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>Detailed list of Menu Items</returns>
        [HttpGet]
        [Route("customer/preference_menuItems")]
        [ResponseType(typeof(List<RestMenuItemsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Item Not found")]
        [SwaggerOperation(Tags = new[] { "Customer Preference Menu Items" })]

        public IHttpActionResult GetMenuItemsByPreferences(int? id = 0, int? page = 1, int? limit = null)
        {
            var user = GetAuthenticatedCustomer();
            if (user == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;

            var menuItems = _customerService.GetMenuItemsBasedOnPreferences(user.Id, id.Value, page.Value, limit.Value, out total);
            if (menuItems == null || menuItems.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_RestMenuItem } });

            double? baseCurrencyRate = 0;
            if (user.CurrencyId != null && user.CurrencyId > 0)
                baseCurrencyRate = menuItems.Select(m => m.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == user.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();

            foreach (var menuItem in menuItems)
            {
                foreach (var itemImage in menuItem.RestaurantMenuItemImages)
                {
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                        itemImage.Image = Url.Content("/Images/MenuItem/") + itemImage.RestaurantMenuItemId.ToString() + "/" + itemImage.Image;
                }
            }
            var favRestaurants = _customerService.GetCustomerFavoriteMenuItems(user.Id);
            var menuItemDTO = Mapper.Map<IEnumerable<RestaurantMenuItem>, List<RestMenuItemsDTO>>(menuItems);
            menuItemDTO.Select(m => { m.IsFavourite = false; return m; }).ToList();
            menuItemDTO.Select(m => { m.MyPreference = true; return m; }).ToList();
            if (user.CurrencyId != null && user.CurrencyId > 0)
            {
                menuItemDTO.Select(m => { m.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(m.Price) * baseCurrencyRate), 2); return m; }).ToList();
                menuItemDTO.Select(m => { m.PreferredCurrencySymbol = user.Currency.Symbol; return m; }).ToList();
            }

            if (favRestaurants != null && favRestaurants.Count() > 0)
            {
                foreach (var resItem in menuItemDTO)
                {
                    if (favRestaurants.Any(r => r.Id == resItem.Id))
                        resItem.IsFavourite = true;
                }
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = menuItemDTO });
        }
        #endregion

        #region Payment Details

        /// <summary>
        /// Returns authenticated customer payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer payments detail
        /// </remarks>
        /// <param name="listBy">Select group by</param>
        /// <param name="year">year</param>
        /// <returns>List of payments detail</returns>

        [HttpGet]
        [Route("customer/paymentDetails")]
        [ResponseType(typeof(List<CustomerYearlyPaymentsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerPaymentDetails(int? year = 0, ListBy listBy = ListBy.Year)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0 && ListBy.Month == listBy)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _restaurantService.GetCustomerOrders(authenticatedCustomer.Id, Convert.ToInt32(year));
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            if (ListBy.Month == listBy)
            {
                var monthlyOrdersGroups = customerOrders.GroupBy(r => r.CreationDate.Month).OrderBy(o => o.Key).Select(gr => new CustomerMonthlyPayments
                {
                    Month = gr.Key,
                    TotalAmount = gr.Sum(o => o.OrderTotal),
                    PreferredCurrencyTotalAmount = System.Math.Round(Convert.ToDouble(gr.Sum(o => o.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault() * o.OrderTotal)), 2),
                    PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol,
                    RestaurantOrder = gr.Take(5).ToList()
                });
                var monthlyOrdersGroupsDTO = Mapper.Map<IEnumerable<CustomerMonthlyPayments>, List<CustomerMonthlyPaymentsDTO>>(monthlyOrdersGroups);

                foreach (var eachOrders in monthlyOrdersGroups.Select(r => r.RestaurantOrder))
                {
                    foreach (var eachOrder in eachOrders)
                    {
                        foreach (var eachCustOrders in monthlyOrdersGroupsDTO.Select(r => r.RestaurantOrder))
                        {
                            foreach (var eachCustOrder in eachCustOrders)
                            {
                                if (eachOrder.Id == eachCustOrder.Id)
                                {
                                    double? baseCurrencyRate = 0;
                                    if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                                    {
                                        baseCurrencyRate = eachOrder.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();
                                        eachCustOrder.PreferredCurrencyOrderTotal = System.Math.Round(Convert.ToDouble(Convert.ToDouble(eachCustOrder.OrderTotal) * baseCurrencyRate), 2);
                                    }
                                }
                            }
                        }
                    }
                }
                return Content(HttpStatusCode.OK, new { Message = "success", Result = monthlyOrdersGroupsDTO });
            }

            var yearlyOrdersGroups = customerOrders.GroupBy(r => r.CreationDate.Year).OrderBy(o => o.Key).Select(gr => new CustomerYearlyPayments
            {
                Year = gr.Key,
                TotalAmount = gr.Sum(o => o.OrderTotal),
                PreferredCurrencyTotalAmount = System.Math.Round(Convert.ToDouble(gr.Sum(o => o.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault() * o.OrderTotal)), 2),
                PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol,
                RestaurantOrder = gr.Take(5).ToList(),
            });
            var yearlyOrdersGroupsDTO = Mapper.Map<IEnumerable<CustomerYearlyPayments>, List<CustomerYearlyPaymentsDTO>>(yearlyOrdersGroups);

            foreach (var eachOrders in yearlyOrdersGroups.Select(r => r.RestaurantOrder))
            {
                foreach (var eachOrder in eachOrders)
                {
                    foreach (var eachCustOrders in yearlyOrdersGroupsDTO.Select(r => r.RestaurantOrder))
                    {
                        foreach (var eachCustOrder in eachCustOrders)
                        {
                            if (eachOrder.Id == eachCustOrder.Id)
                            {
                                double? baseCurrencyRate = 0;
                                if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                                {
                                    baseCurrencyRate = eachOrder.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();
                                    eachCustOrder.PreferredCurrencyOrderTotal = System.Math.Round(Convert.ToDouble(Convert.ToDouble(eachCustOrder.OrderTotal) * baseCurrencyRate), 2);
                                }
                            }
                        }
                    }
                }
            }

            return Content(HttpStatusCode.OK, new { Message = "success", Result = yearlyOrdersGroupsDTO });
        }

        /// <summary>
        /// Returns authenticated customer payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer payments detail
        /// </remarks>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of payments detail</returns>

        [HttpGet]
        [Route("customer/paymentDetail")]
        [ResponseType(typeof(List<RestaurantOrderInfoDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerPaymentDetail(int? year = 0, int? month = 0, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _restaurantService.GetCustomerOrders(authenticatedCustomer.Id, Convert.ToInt32(year), Convert.ToInt32(month), page.Value, limit.Value, out total);
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            var custPaidOrdersDTO = Mapper.Map<IEnumerable<RestaurantOrder>, List<RestaurantOrderInfoDTO>>(customerOrders);
            foreach (var eachOrder in customerOrders)
            {
                foreach (var eachCustOrder in custPaidOrdersDTO)
                {
                    if (eachOrder.Id == eachCustOrder.Id)
                    {
                        double? baseCurrencyRate = 0;
                        if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        {
                            baseCurrencyRate = eachOrder.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();
                            eachCustOrder.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(Convert.ToDouble(eachCustOrder.OrderTotal) * baseCurrencyRate), 2);
                            eachCustOrder.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol;

                            eachCustOrder.RestaurantOrderItems.Select(i => { i.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(Convert.ToDouble(i.Price) * baseCurrencyRate), 2); return i; }).ToList();
                        }
                        eachCustOrder.ReceiptNumber = "#" + eachOrder.Id;
                        eachCustOrder.OrderDate = (eachOrder.ScheduledDate == null) ? eachOrder.CreationDate : eachOrder.ScheduledDate;
                        Status_Type ordrStatus = (Status_Type)eachOrder.OrderStatus;
                        eachCustOrder.OrderStatus = ordrStatus.ToString();
                    }
                }
            }

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = custPaidOrdersDTO });
        }


        /// <summary>
        /// Returns authenticated customer Spa payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer Spa payments detail
        /// </remarks>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of Spa payments detail</returns>

        [HttpGet]
        [Route("customer/spaPaymentDetail")]
        [ResponseType(typeof(List<SpaOrderInfoDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerSpaPaymentDetail(int? year = 0, int? month = 0, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _hotelService.GetCustomerSpaOrders(authenticatedCustomer.Id, Convert.ToInt32(year), Convert.ToInt32(month), page.Value, limit.Value, out total);
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            var custPaidOrdersDTO = Mapper.Map<IEnumerable<SpaOrder>, List<SpaOrderInfoDTO>>(customerOrders);
            foreach (var order in custPaidOrdersDTO)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in customerOrders)
                {
                    if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        baseCurrencyRate = ord.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.preferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                    }
                }
                SpaOrderStatus ordrStatus = (SpaOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = custPaidOrdersDTO });
        }

        /// <summary>
        /// Returns authenticated customer laundry payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer laundry payments detail
        /// </remarks>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of laundry payments detail</returns>

        [HttpGet]
        [Route("customer/laundryPaymentDetail")]
        [ResponseType(typeof(List<CustomerActiveLaundryOrders>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerLaundryPaymentDetail(int? year = 0, int? month = 0, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _hotelService.GetCustomerLaundryOrders(authenticatedCustomer.Id, Convert.ToInt32(year), Convert.ToInt32(month), page.Value, limit.Value, out total);
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            var custPaidOrdersDTO = Mapper.Map<IEnumerable<LaundryOrder>, List<CustomerActiveLaundryOrders>>(customerOrders);
            foreach (var order in custPaidOrdersDTO)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in customerOrders)
                {
                    if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        baseCurrencyRate = ord.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.OrderTotalInPreferredCurrency = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                        order.LaundryOrderItems.Select(i => { i.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(i.Total * baseCurrencyRate), 2); return i; }).ToList();
                        order.LaundryOrderItems.Select(i => { i.LaundryDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(i.LaundryDetail.Price * baseCurrencyRate), 2); return i; }).ToList();
                        order.LaundryOrderItems.Select(i => { i.LaundryOrderItemsAdditional.Select(a => { a.LaundryAdditionalElement.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(a.LaundryAdditionalElement.Price * baseCurrencyRate), 2); return a; }).ToList(); return i; }).ToList();
                    }
                }
                LaundryOrderStatus ordrStatus = (LaundryOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = custPaidOrdersDTO });
        }


        /// <summary>
        /// Returns authenticated customer laundry payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer laundry payments detail
        /// </remarks>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of laundry payments detail</returns>

        [HttpGet]
        [Route("customer/excursionPaymentDetail")]
        [ResponseType(typeof(List<ExcursionOrderInfoDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerExcursionPaymentDetail(int? year = 0, int? month = 0, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _hotelService.GetCustomerExcursionOrders(authenticatedCustomer.Id, Convert.ToInt32(year), Convert.ToInt32(month), page.Value, limit.Value, out total);
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            var custPaidOrdersDTO = Mapper.Map<IEnumerable<ExcursionOrder>, List<ExcursionOrderInfoDTO>>(customerOrders);
            foreach (var order in custPaidOrdersDTO)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in customerOrders)
                {
                    if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        baseCurrencyRate = ord.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.Total * baseCurrencyRate), 2);
                        if (order.ExcursionOrderItems != null && order.ExcursionOrderItems.Count() > 0)
                        {
                            order.ExcursionOrderItems.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();
                            order.ExcursionOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            order.ExcursionOrderItems.Select(s => { s.OfferPriceInPreferredCurrency = System.Math.Round(Convert.ToDouble(s.OfferPrice * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
                ExcursionOrderStatus ordrStatus = (ExcursionOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = custPaidOrdersDTO });
        }

        /// <summary>
        /// Returns authenticated customer housekeeping payments detail
        /// </summary>
        /// <remarks>
        /// Get authenticated customer housekeeping payments detail
        /// </remarks>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of items on each page</param>
        /// <returns>List of housekeeping payments detail</returns>

        [HttpGet]
        [Route("customer/housekeepingPaymentDetail")]
        [ResponseType(typeof(List<CustomerActiveHousekeepingOrderDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "No payment details found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerHousekeepingPaymentDetail(int? year = 0, int? month = 0, int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;

            int total;

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            if (year == 0)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Rqd_Year } });

            var customerOrders = _hotelService.GetCustomerHousekeepingOrders(authenticatedCustomer.Id, Convert.ToInt32(year), Convert.ToInt32(month), page.Value, limit.Value, out total);
            if (customerOrders == null || customerOrders.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PayDetails_NotFound } });

            var custPaidOrdersDTO = Mapper.Map<IEnumerable<HousekeepingOrder>, List<CustomerActiveHousekeepingOrderDTO>>(customerOrders);
            foreach (var order in custPaidOrdersDTO)
            {
                double? baseCurrencyRate = 0;
                foreach (var ord in customerOrders)
                {
                    if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                        baseCurrencyRate = ord.Room.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (ord.Id == order.Id)
                    {
                        order.ReceiptNumber = "#" + order.Id;
                        order.OrderDate = ord.CreationDate;
                        order.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(order.OrderTotal * baseCurrencyRate), 2);
                        if (order.HousekeepingOrderItems != null && order.HousekeepingOrderItems.Count() > 0)
                        {
                            order.HousekeepingOrderItems.Select(s => { s.PreferredCurrencyTotal = System.Math.Round(Convert.ToDouble(s.Total * baseCurrencyRate), 2); return s; }).ToList();
                            order.HousekeepingOrderItems.Select(s => { s.HouseKeepingFacilityDetail.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.HouseKeepingFacilityDetail.Price * baseCurrencyRate), 2); return s; }).ToList();
                        }
                    }
                }
                HousekeepingOrderStatus ordrStatus = (HousekeepingOrderStatus)Convert.ToInt32(order.OrderStatus);
                order.OrderStatus = ordrStatus.ToString();
            }
            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = custPaidOrdersDTO });
        }

        /// <summary>
        /// Returns authenticated customer payments history
        /// </summary>
        /// <remarks>
        /// Get authenticated customer payments history
        /// </remarks>
        /// <param name="year">year</param>
        /// <returns>List of payments history</returns>

        [HttpGet]
        [Route("customer/paymentHistory")]
        [ResponseType(typeof(List<CustomerMonthlyPaymentHistoryDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Payment history not found")]
        [SwaggerOperation(Tags = new[] { "Customer Payment Details" })]
        public IHttpActionResult CustomerPaymentHistory(int? year = 0)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var customerPaymentHistory = _customerService.GetCustomerHotelAndRestPaymentHistory(authenticatedCustomer.Id, authenticatedCustomer.CurrencyId, Convert.ToInt32(year));
            if (customerPaymentHistory == null || customerPaymentHistory.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.PaymentHistory_NotFound } });

            var monthlyPaymentHistoryDTO = Mapper.Map<IEnumerable<CustomerMonthlyPaymentHistory>, List<CustomerMonthlyPaymentHistoryDTO>>(customerPaymentHistory);
            monthlyPaymentHistoryDTO.Select(c => { c.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return c; }).ToList();
            return Content(HttpStatusCode.OK, new { Message = "success", Result = monthlyPaymentHistoryDTO });
        }
        #endregion Payment Details

        #region Spa Ingredient Preference

        /// <summary>
        /// Returns authenticated customer Spa ingredient preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer Spa ingredient preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of Spa ingredient Preferences</returns>
        [Route("customer/spa_ingredient_preferences")]
        [HttpGet]
        [ResponseType(typeof(SpaIngredientCategoriesPrefDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Ingredient Categories Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Spa Ingredient Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetSpaIngredientPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeSpaIngredientCategories = _hotelService.GetAllSpaIngredientCategories();
            if (activeSpaIngredientCategories.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.IngredientCat_NotFound } });

            var prefSpaIngredientDTO = Mapper.Map<IEnumerable<SpaIngredientCategory>, List<SpaIngredientCategoriesPrefDTO>>(activeSpaIngredientCategories);

            var custSpaIngredientPref = _customerService.GetCustomerSpaIngredientPreference(authenticatedCustomer.Id);

            if (custSpaIngredientPref.Count() > 0)
            {
                foreach (var spaIngredientCatDTO in prefSpaIngredientDTO)
                {
                    if (spaIngredientCatDTO.SpaIngredients.Count() > 0)
                    {
                        foreach (var eachSpaIngredientDTO in spaIngredientCatDTO.SpaIngredients)
                        {
                            foreach (var eachCustSpaIngredientPref in custSpaIngredientPref)
                            {
                                if (eachCustSpaIngredientPref.SpaIngredientId == eachSpaIngredientDTO.Id)
                                    eachSpaIngredientDTO.Include = eachCustSpaIngredientPref.Include;
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, prefSpaIngredientDTO);
        }

        /// <summary>
        /// Sets Spa ingredient preference
        /// </summary> 
        /// <remarks>
        /// Sets Spa ingredient preference.
        /// </remarks>
        /// <param name="preferenceDTO">Ingredinet preference</param>
        /// <returns>Returns success</returns>
        [Route("customer/spa_ingredient_preferences")]
        [HttpPut]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Spa Ingredient Not Found")]
        [SwaggerOperation(Tags = new[] { "Customer Spa Ingredient Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult SetSpaIngredientPreference(ModifySpaIngredientPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaIngredient = _hotelService.GetSpaIngredientById(preferenceDTO.SpaIngredientId);
            if (spaIngredient == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Ingredient_NotFound } });

            try
            {
                var entity = Mapper.Map<CustomerSpaServicePreference>(preferenceDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                var result = _customerService.ModifySpaIngredientPreference(entity);

                return Ok("Success");
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        #endregion Spa Ingredient Preference

        #region Favorite Spa Service

        /// <summary>
        /// Togges Favorite
        /// </summary> 
        /// <remarks>
        /// Togges SPA service Favorite status, if exists then removes this service from favorite list, else adds
        /// </remarks>
        /// <param name="spaServiceDetailId">Spa Service Detail Id</param>
        /// <returns>Status</returns>
        [Route("customer/favorite_spa_service")]
        [HttpPost]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Spa Service" })]

        public IHttpActionResult ToggleFavoriteSpaService(int spaServiceDetailId)
        {
            if (spaServiceDetailId < 1)
                return BadRequest();

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaServiceDetail = _hotelService.GetSpaServiceDetailById(spaServiceDetailId);
            if (spaServiceDetail == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_SpaService_Dtl } });

            // send it to db to modify 
            _customerService.CustomerFavoriteSpaServiceToggle(authenticatedCustomer.Id, spaServiceDetailId);

            return Ok("success");
        }


        /// <summary>
        /// Returns authenticated customer favorite Spa services
        /// </summary>
        /// <remarks>
        /// Get authenticated customer's favorite Spa service
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of Spa service on each page</param>
        /// <returns>List of favorite Spa service</returns>
        [HttpGet]
        [Route("customer/favorite_spa_Service")]

        [ResponseType(typeof(List<FavoriteSpaServiceDetailDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Favorite spa service not found")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Spa Service" })]

        public IHttpActionResult GetFavoriteSpaServices(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var spaServiceDetail = _customerService.GetCustomerFavoriteSpaServiceDetails(authenticatedCustomer.Id, page.Value, limit.Value, out total);

            if (spaServiceDetail == null || spaServiceDetail.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.No_Favorite_SpaService } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = spaServiceDetail.Select(s => s.SpaService.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();


            var spaServiceDetailDTO = Mapper.Map<IEnumerable<SpaServiceDetail>, List<FavoriteSpaServiceDetailDTO>>(spaServiceDetail);
            spaServiceDetailDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();
            foreach (var eachFavSpa in spaServiceDetailDTO)
            {
                var orderDetail = _customerService.GetCustomerSpaOrderDetails(authenticatedCustomer.Id, eachFavSpa.Id);
                eachFavSpa.TotalOrders = orderDetail.TotalOrders;
                eachFavSpa.LastOrder = orderDetail.LastOrder;
            }

            if (baseCurrencyRate != 0)
                spaServiceDetailDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = spaServiceDetailDTO });
        }

        #endregion Favorite Spa Service

        #region Favorite Excursion

        /// <summary>
        /// Togges Favorite
        /// </summary> 
        /// <remarks>
        /// Togges excursion Favorite status, if exists then removes this service from favorite list, else adds
        /// </remarks>
        /// <param name="excursionDetailId">Excursion Detail Id</param>
        /// <returns>Status</returns>
        [Route("customer/favorite_excursion")]
        [HttpPost]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Detail Not Found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Excursion" })]

        public IHttpActionResult ToggleFavoriteExcursionDetail(int excursionDetailId)
        {
            if (excursionDetailId < 1)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.Rqd_ExcursionDetailId } });

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionDetail = _hotelService.GetExcursionDetailById(excursionDetailId);
            if (excursionDetail == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.ExcursionDetail_NotFound } });

            // send it to db to modify 
            _customerService.CustomerFavoriteExcursionToggle(authenticatedCustomer.Id, excursionDetailId);

            return Ok("success");
        }


        /// <summary>
        /// Returns authenticated customer favorite excursion details
        /// </summary>
        /// <remarks>
        /// Get authenticated customer's favorite excursion details
        /// </remarks>
        /// <param name="page">Page Number</param>
        /// <param name="limit">No. of excursion details on each page</param>
        /// <returns>List of favorite excursion details</returns>
        [HttpGet]
        [Route("customer/favorite_excursion")]

        [ResponseType(typeof(List<FavoriteExcursionDetailsDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Favorite excursion details not found")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer Favorite Excursion" })]

        public IHttpActionResult GetFavoriteExcursionDetails(int? page = 1, int? limit = null)
        {
            page = page <= 0 ? 1 : page;
            limit = limit ?? ToOrderConfigs.DefaultPageSize;
            int total;
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excursionDetail = _customerService.GetCustomerFavoriteExcursionDetails(authenticatedCustomer.Id, page.Value, limit.Value, out total);

            if (excursionDetail == null || excursionDetail.Count() == 0)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.FavExcursionDet_NotFound } });

            double? baseCurrencyRate = 0;
            if (authenticatedCustomer.CurrencyId != null && authenticatedCustomer.CurrencyId > 0)
                baseCurrencyRate = excursionDetail.Select(s => s.HotelExcursion.HotelServices.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == authenticatedCustomer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault()).FirstOrDefault();


            var excursionDetailDTO = Mapper.Map<IEnumerable<ExcursionDetail>, List<FavoriteExcursionDetailsDTO>>(excursionDetail);
            excursionDetailDTO.Select(i => { i.PreferredCurrencySymbol = authenticatedCustomer.Currency.Symbol; return i; }).ToList();

            foreach (var eachFavExcursion in excursionDetailDTO)
            {
                var orderDetail = _customerService.GetCustomerExcursionOrderDetails(authenticatedCustomer.Id, eachFavExcursion.Id);
                eachFavExcursion.TotalOrders = orderDetail.TotalOrders;
                eachFavExcursion.LastOrder = orderDetail.LastOrder;
            }

            if (baseCurrencyRate != 0)
                excursionDetailDTO.Select(s => { s.PreferredCurrencyPrice = System.Math.Round(Convert.ToDouble(s.Price * baseCurrencyRate), 2); return s; }).ToList();

            var mod = total % limit.Value;
            var totalPageCount = (total / limit.Value) + (mod == 0 ? 0 : 1);
            return Content(HttpStatusCode.OK, new { total_items = total, total_pages = totalPageCount, current_page = page.Value, result = excursionDetailDTO });
        }

        #endregion Favorite Excursion

        #region Excursion Ingredient Preference

        /// <summary>
        /// Returns authenticated customer excursion ingredient preferences
        /// </summary>
        /// <remarks>
        /// Get authenticated customer excursion ingredient preferences. Authorization required to access this method.
        /// </remarks>
        /// <returns>List of excursion ingredient Preferences</returns>
        [Route("customer/excursion_preferences")]
        [HttpGet]
        [ResponseType(typeof(ExcursionIngredientCategoriesPrefDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Ingredient Categories Not Found")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerOperation(Tags = new[] { "Customer Excursion Ingredient Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult GetExcursionIngredientPreferences()
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var activeExcIngredientCategories = _hotelService.GetAllExcursionIngredientCategories();
            if (activeExcIngredientCategories.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.IngredientCat_NotFound } });

            var prefExcIngredientDTO = Mapper.Map<IEnumerable<ExcursionIngredientCategory>, List<ExcursionIngredientCategoriesPrefDTO>>(activeExcIngredientCategories);

            var custExcIngredientPref = _customerService.GetCustomerExcursionIngredientPreference(authenticatedCustomer.Id);

            if (custExcIngredientPref.Count() > 0)
            {
                foreach (var excIngredientCatDTO in prefExcIngredientDTO)
                {
                    if (excIngredientCatDTO.ExcursionIngredients.Count() > 0)
                    {
                        foreach (var eachExcIngredientDTO in excIngredientCatDTO.ExcursionIngredients)
                        {
                            foreach (var eachCustExcIngredientPref in custExcIngredientPref)
                            {
                                if (eachCustExcIngredientPref.ExcursionIngredientId == eachExcIngredientDTO.Id)
                                    eachExcIngredientDTO.Include = eachCustExcIngredientPref.Include;
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
            return Content(HttpStatusCode.OK, prefExcIngredientDTO);
        }

        /// <summary>
        /// Sets excursion ingredient preference
        /// </summary> 
        /// <remarks>
        /// Sets excursion ingredient preference.
        /// </remarks>
        /// <param name="preferenceDTO">Ingredinet preference</param>
        /// <returns>Returns success</returns>
        [Route("customer/excursion_preferences")]
        [HttpPut]

        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Excursion Ingredient Not Found")]
        [SwaggerOperation(Tags = new[] { "Customer Excursion Ingredient Preferences" })]
        [ApiExplorerSettings(IgnoreApi = false)]

        public IHttpActionResult SetExcursionIngredientPreference(ModifyExcursionIngredientPreferenceDTO preferenceDTO)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }

            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var excIngredient = _hotelService.GetExcursionIngredientById(preferenceDTO.ExcursionIngredientId);
            if (excIngredient == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.Ingredient_NotFound } });

            try
            {
                var entity = Mapper.Map<CustomerExcursionPreferences>(preferenceDTO);
                entity.CustomerId = authenticatedCustomer.Id;
                var result = _customerService.ModifyExcursionIngredientPreference(entity);

                return Ok("Success");
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
        }

        #endregion Excursion Ingredient Preference

        #region language

        /// <summary>
        /// Returns all languages
        /// </summary>
        /// <remarks>
        /// Returns all languages
        /// </remarks>
        /// <returns> List of languages</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("languages", Name = "GetAllLanguages")]
        [ResponseType(typeof(List<LanguageDTO>))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Languages not found")]
        [SwaggerOperation(Tags = new[] { "Language" })]

        public IHttpActionResult GetAllLanguages()
        {
            var languages = _customerService.GetAllLanguages();
            if (languages == null || languages.Count() < 1)
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { Resources.LanguagesNotFound } });

            var languagesDTO = Mapper.Map<IEnumerable<Language>, List<LanguageDTO>>(languages);

            return Content(HttpStatusCode.OK, languagesDTO);
        }

        #endregion language

        #region WakeUp

        /// <summary>
        /// Returns the WakeUp call of customer
        /// </summary>
        /// <remarks>
        /// Returs the WakeUp call of customer
        /// </remarks>
        /// <returns>Returns WakeUp call Details</returns>
        [HttpGet]
        [Route("customer/wakeUp", Name = "GetCustomerWakeUp")]

        [ResponseType(typeof(WakeUpDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "WakeUp call not available for customer")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Success")]
        [SwaggerOperation(Tags = new[] { "Customer WakeUp" })]
        public IHttpActionResult GetCustomerWakeUp()
        {
            var customer = GetAuthenticatedCustomer();
            if (customer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var customerWakeUpCall = _customerService.GetCustomerWakeUpCall(customer.Id);

            if (customerWakeUpCall != null && customerWakeUpCall.Count() < 1)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { "WakeUp Call not available for customer" } });

            var customerWakeUpCallDTO = Mapper.Map<IEnumerable<WakeUp>, List<WakeUpDTO>>(customerWakeUpCall);

            foreach (var wakeup in customerWakeUpCall)
            {
                foreach (var wakeupdto in customerWakeUpCallDTO)
                {
                    if (wakeup.Id == wakeupdto.Id)
                    {
                        WakeUpMethod ordrStatus = (WakeUpMethod)wakeup.Method;
                        wakeupdto.Method = ordrStatus.ToString();
                    }
                }
            }
            return Content(HttpStatusCode.OK, customerWakeUpCallDTO);
        }


        /// <summary>
        /// Creates new WakeUp call
        /// </summary>
        /// <remarks>
        /// Creates customer WakeUp call
        /// </remarks>
        /// <param name="createWakeUpDTO">WakeUp Details in Request body</param>
        /// <returns>Returns newly created WakeUp details in response</returns>

        [Route("customer/wakeUp")]
        [HttpPost]
        [ResponseType(typeof(WakeUpDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Validation Error")]
        [SwaggerResponse(HttpStatusCode.Forbidden, Description = "Not Authorized")]
        [SwaggerResponse(HttpStatusCode.Created, Description = "WakeUp Created")]
        [SwaggerOperation(Tags = new[] { "Customer WakeUp" })]
        public IHttpActionResult CreateWakeUpCall(CreateWakeUpDTO createWakeUpDTO)
        {
            string interval = createWakeUpDTO.Interval != null ? createWakeUpDTO.Interval.ToString() : "00:00:00.000";

            TimeSpan time;
            if (!TimeSpan.TryParse(interval, out time))
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { "PLEASE_PROVIDE_VALID_TIME_INTERVAL" } });

            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var error in ModelState.Values)
                {
                    var x = error.Errors.Select(e => e.ErrorMessage);
                    messages.AddRange(x);
                }
                return Content(HttpStatusCode.BadRequest, new { messages = messages });
            }
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var entity = Mapper.Map<WakeUp>(createWakeUpDTO);
            entity.CustomerId = authenticatedCustomer.Id;
            try
            {
                entity = _customerService.CreateCustomerWakeUpcall(entity);
                var wakeUpDTO = Mapper.Map<WakeUp, WakeUpDTO>(entity);

                WakeUpMethod ordrStatus = (WakeUpMethod)entity.Method;
                wakeUpDTO.Method = ordrStatus.ToString();

                return Created(Url.Link("GetCustomerWakeUp", new { id = entity.CustomerId }), new { status = true, data = wakeUpDTO });
            }
            catch (ArgumentException arEx)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { arEx.Message } });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, new { messages = new string[] { ex.Message } });
            }
        }

        /// <summary>
        /// Deletes customer wakeup call
        /// </summary>
        /// <remarks>
        /// Delete customer wakeup call based on id.
        /// </remarks>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [Route("customer/wakeUp", Name = "DeleteCustomerWakeUp")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Success")]
        [SwaggerOperation(Tags = new[] { "Customer WakeUp" })]
        public IHttpActionResult DeleteCustomerWakeUp(int Id)
        {
            var authenticatedCustomer = GetAuthenticatedCustomer();
            if (authenticatedCustomer == null)
                return Content(HttpStatusCode.Forbidden, new { messages = new string[] { Resources.Not_Authorized } });

            var wakeup = _customerService.GetCustomerWakeUpcall(Id, authenticatedCustomer.Id);
            if (wakeup == null)
                return Content(HttpStatusCode.NotFound, new { messages = new string[] { Resources.WakeUp_NotFound } });

            _customerService.DeleteCustomerWakeUpCall(Id);
            return Ok();
        }

        #endregion WakeUp
    }
}
