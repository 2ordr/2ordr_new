using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;
using ToOrdr.Api.Areas.HelpPage.ModelDescriptions;
using ToOrdr.Api.Areas.HelpPage.Models;
using ToOrdr.Api.Controllers;

namespace ToOrdr.Api.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        private const string ErrorViewName = "Error";

        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult Index()
        {
            var displayOrder = new Dictionary<Type, int>();
            displayOrder.Add(typeof(CustomersController), 1);
            displayOrder.Add(typeof(HotelController), 2);
            displayOrder.Add(typeof(RestaurantController), 3);
            displayOrder.Add(typeof(AccountController), 4);
            ViewBag.DisplayOrder = displayOrder;

            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
            return View(Configuration.Services.GetApiExplorer().ApiDescriptions);
        }

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View(ErrorViewName);
        }

        public ActionResult ResourceModel(string modelName)
        {
            if (!String.IsNullOrEmpty(modelName))
            {
                ModelDescriptionGenerator modelDescriptionGenerator = Configuration.GetModelDescriptionGenerator();
                ModelDescription modelDescription;
                if (modelDescriptionGenerator.GeneratedModels.TryGetValue(modelName, out modelDescription))
                {
                    return View(modelDescription);
                }
            }

            return View(ErrorViewName);
        }
    }
}