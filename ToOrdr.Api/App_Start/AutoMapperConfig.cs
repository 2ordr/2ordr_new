﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Api.DTO;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customer, CustomerDTO>();
            });

            IMapper mapper = config.CreateMapper();

        }
    }
}