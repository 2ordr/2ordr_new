﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using ToOrdr.Api.Providers;
using ToOrdr.Api.Models;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Net;
using System.Web.Http;
using System.Diagnostics;

namespace ToOrdr.Api
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            // our middleware for formating \token error response
            app.UseCommonErrorResponse();


            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);



            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});


        }
    }


    public static class ErrorMessageFormatter
    {

        public static IAppBuilder UseCommonErrorResponse(this IAppBuilder app)
        {
            app.Use<JsonErrorFormatter>();
            return app;
        }

        public class JsonErrorFormatter : OwinMiddleware
        {
            public JsonErrorFormatter(OwinMiddleware next)
                : base(next)
            {
            }

            public override async Task Invoke(IOwinContext context)
            {
                var owinRequest = context.Request;
                var owinResponse = context.Response;
                //buffer the response stream for later
                var owinResponseStream = owinResponse.Body;
                //buffer the response stream in order to intercept downstream writes
                using (var responseBuffer = new MemoryStream())
                {
                    //assign the buffer to the resonse body
                    owinResponse.Body = responseBuffer;

                    await Next.Invoke(context);

                    //reset body
                    owinResponse.Body = owinResponseStream;

                    if (responseBuffer.CanSeek && responseBuffer.Length > 0 && responseBuffer.Position > 0)
                    {
                        //reset buffer to read its content
                        responseBuffer.Seek(0, SeekOrigin.Begin);
                    }

                    if (!IsSuccessStatusCode(owinResponse.StatusCode) && responseBuffer.Length > 0)
                    {
                        //NOTE: perform your own content negotiation if desired but for this, using JSON
                        var body = await CreateCommonApiResponse(owinResponse, responseBuffer);

                        var content = body != null ? body.ToString() : string.Empty;

                        var mediaType = MediaTypeHeaderValue.Parse(owinResponse.ContentType);
                        using (var customResponseBody = new StringContent(content, Encoding.UTF8, mediaType.MediaType))
                        {
                            var customResponseStream = await customResponseBody.ReadAsStreamAsync();
                            await customResponseStream.CopyToAsync(owinResponseStream, (int)customResponseStream.Length, owinRequest.CallCancelled);
                            owinResponse.ContentLength = customResponseStream.Length;
                        }
                    }
                    else if (responseBuffer.Length > 0)
                    {
                        //copy buffer to response stream this will push it down to client
                        await responseBuffer.CopyToAsync(owinResponseStream, (int)responseBuffer.Length, owinRequest.CallCancelled);
                        owinResponse.ContentLength = responseBuffer.Length;
                    }
                }
            }

            async Task<object> CreateCommonApiResponse(IOwinResponse response, Stream stream)
            {

                var json = await new StreamReader(stream).ReadToEndAsync();

                var statusCode = ((HttpStatusCode)response.StatusCode).ToString();
                var responseReason = response.ReasonPhrase ?? statusCode;

                //Is this an OAuth Error
                var oAuthError = Newtonsoft.Json.Linq.JObject.Parse(json);
                if (oAuthError["error"] != null && oAuthError["error_description"] != null)
                {
                    dynamic obj = oAuthError;
                    return JsonConvert.SerializeObject(new
                    {
                        messages = new string[] { obj.error != null ? (string)obj.error : string.Empty }
                    });
                }


                return json;  //else return as it is

                ////Is this a HttpError
                //var httpError = JsonConvert.DeserializeObject<HttpError>(json);
                //if (httpError != null)
                //{
                //    return new
                //    {
                //        messages = httpError.Message ?? responseReason,
                //        error_description = (object)httpError.MessageDetail
                //        ?? (object)httpError.ModelState
                //        ?? (object)httpError.ExceptionMessage
                //    };
                //}



                ////Is this some other unknown error (Just wrap in common model)
                //var error = JsonConvert.DeserializeObject(json);
                //return new
                //{
                //    error = responseReason,
                //    error_description = error
                //};
            }

            bool IsSuccessStatusCode(int statusCode)
            {
                return statusCode >= 200 && statusCode <= 299;
            }
        }
    }




}





