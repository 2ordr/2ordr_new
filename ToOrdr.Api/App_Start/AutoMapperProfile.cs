﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Api.DTO;
using ToOrdr.Core.Entities;
using System.Data.Entity;
using System.Configuration;

namespace ToOrdr.Api.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<AboutYouOrdr, AboutYouOrdrDescriptionDTO>().ReverseMap();


            CreateMap<Customer, CustomerDTO>()
                .ForMember(dest => dest.ProfileImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.ProfileImage))
                        return api_path + "Images/Customers/" + src.Id.ToString() + "/" + src.ProfileImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<Customer, ModifyCustomerDTO>().ReverseMap();
            CreateMap<Customer, CreateCustomerDTO>().ReverseMap();
            CreateMap<Customer, CustomerDetailsDTO>().ReverseMap();
            CreateMap<Customer, CustomerInfoDTO>()
                .ForMember(dest => dest.ProfileImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.ProfileImage))
                        return api_path + "Images/Customers/" + src.Id.ToString() + "/" + src.ProfileImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<CustomerActiveBookingsInHotel, CustomerActiveBookingsInHotelDTO>().ReverseMap();

            CreateMap<CustomerHotelBooking, HotelBookingsDTO>().ReverseMap();
            CreateMap<CustomerHotelBooking, CreateHotelBookingDTO>().ReverseMap();
            CreateMap<CustomerHotelBooking, ModifyHotelBookingDTO>().ReverseMap();
            CreateMap<CustomerHotelBooking, CustomerHotelBookingsDTO>()
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src =>
                {
                    return src.Room.Hotel;
                })).ReverseMap();


            CreateMap<Customer_Restaurant_Preference, CustomerRestaurantPreferenceDTO>().ReverseMap();
            CreateMap<Customer_Restaurant_Preference, CreateRestaurantPreferenceDTO>().ReverseMap();

            CreateMap<Customer_Hotel_Preferences, CustomerHotelPreferenceDTO>().ReverseMap();
            CreateMap<Customer_Hotel_Preferences, CreateHotelPreferenceDTO>().ReverseMap();
            CreateMap<Customer_Hotel_Preferences, ModifyHotelPreferenceDTO>().ReverseMap();

            CreateMap<Customer_RestaurantOrder_Items, CreateRestaurantOrderItemsDTO>().ReverseMap();
            CreateMap<Customer_RestaurantOrder_Items, CustomerRestaurantOrderItemsDTO>().ReverseMap();

            CreateMap<RestaurantOrder, CreateRestaurantOrdersDTO>().ReverseMap();
            CreateMap<RestaurantOrder, RestaurantOrdersDTO>().ReverseMap();
            CreateMap<Customer_RestaurantOrder_Item_customisation, CustomerRestaurantOrderItemCustomisationDTO>().ReverseMap();

            CreateMap<Restaurant, RestaurantDTO>()
                .ForMember(dest => dest.Ratings_Count, opt => opt.ResolveUsing(src =>
                    {
                        var reviews = src.RestaurantReviews;
                        if (reviews.Any())
                            return reviews.Count();
                        return 0;
                    }
                    ))
                .ForMember(dest => dest.Rating_Average, opt => opt.ResolveUsing(src =>
                    {
                        var reviews = src.RestaurantReviews;

                        if (reviews.Any())
                            return reviews.Average(rv => rv.Score);
                        return 0;
                    }
                    ))
                    .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                        else
                            return null;
                    }
                    ))
                    .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    })).ReverseMap();


            CreateMap<Restaurant, CreateRestaurantDTO>().ReverseMap();
            CreateMap<Restaurant, ModifyRestaurantDTO>().ReverseMap();
            CreateMap<Restaurant, GetRestaurantsDTO>()
                .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                        else
                            return null;
                    }
                    ))
                    .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    })).ReverseMap();

            CreateMap<Restaurant, RestaurantDetailsDTO>()
                .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    }))
                .ForMember(dest => dest.CurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Currency != null)
                    {
                        return src.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<Restaurant, RestaurantInfoDTO>()
                .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    }))
                    .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                            return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                        else
                            return null;
                    }
                    ))
                    .ForMember(dest => dest.CurrencySymbol, opt => opt.ResolveUsing(src =>
                    {
                        if (src.Currency != null)
                        {
                            return src.Currency.Symbol;
                        }
                        return null;
                    })).ReverseMap();

            CreateMap<Restaurant, RestaurantInfoPdfDTO>()
               .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
               {
                   if (!string.IsNullOrWhiteSpace(src.LogoImage))
                       return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                   else
                       return null;
               }))
                   .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                   {
                       if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                           return api_path + "Images/Restaurant/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                       else
                           return null;
                   }
                   ))
                   .ForMember(dest => dest.CurrencySymbol, opt => opt.ResolveUsing(src =>
                   {
                       if (src.Currency != null)
                       {
                           return src.Currency.Symbol;
                       }
                       return null;
                   })).ReverseMap();

            CreateMap<Restaurant, RestaurantGroupDetailDTO>().ReverseMap();

            CreateMap<RestaurantGroupByRadius, RestaurantGroupByRadiusDTO>().ReverseMap();


            CreateMap<Hotel, HotelDTO>()
                .ForMember(dest => dest.Rating_Average, opt => opt.ResolveUsing(src =>
                    {
                        var reviews = src.HotelReviews;
                        if (reviews.Any())
                            return reviews.Average(rv => rv.Score);
                        return 0;
                    }
                    ))
                .ForMember(dest => dest.Ratings_Count, opt => opt.ResolveUsing(src =>
                    {
                        var reviews = src.HotelReviews;

                        if (reviews.Any())
                            return reviews.Count();
                        return 0;
                    }
                    ))
                    .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                            return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                        else
                            return null;
                    }
                    ))
                    .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    })).ReverseMap();
            CreateMap<Hotel, HotelPdfDTO>()
                 .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                 {
                     if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                         return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                     else
                         return null;
                 }
                    ))
                    .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    })).ReverseMap();
            CreateMap<Hotel, HotelDetailWithExcursionPriceDTO>()
                .ForMember(dest => dest.Rating_Average, opt => opt.ResolveUsing(src =>
                {
                    var reviews = src.HotelReviews;
                    if (reviews.Any())
                        return reviews.Average(rv => rv.Score);
                    return 0;
                }
                    ))
                .ForMember(dest => dest.Ratings_Count, opt => opt.ResolveUsing(src =>
                {
                    var reviews = src.HotelReviews;

                    if (reviews.Any())
                        return reviews.Count();
                    return 0;
                }
                  ))
                .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                            return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                        else
                            return null;
                    }
                    ))
                 .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    }))
                    .ReverseMap();

            CreateMap<Hotel, CreateHotelDTO>().ReverseMap();
            CreateMap<Hotel, ModifyHotelDTO>().ReverseMap();
            CreateMap<Hotel, GetHotelDTO>().ReverseMap();
            CreateMap<Hotel, HotelDetailsDTO>()
                .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.LogoImage))
                        return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                    else
                        return null;
                }))
                .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                        return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<Hotel, HotelDetailsWithCurrencySymbolDTO>()
            .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src => src.Currency.Symbol))
            .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
            {
                if (!string.IsNullOrWhiteSpace(src.LogoImage))
                    return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                else
                    return null;
            }))
            .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
            {
                if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                    return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                else
                    return null;
            })).ReverseMap();

            CreateMap<HotelsGroupByPriceRange, HotelGroupByPriceRangeDTO>().ReverseMap();
            CreateMap<HotelsGroupByStarRating, HotelGroupByStarRatingDTO>().ReverseMap();
            CreateMap<HotelsGroupByRadius, HotelGroupByRadiusDTO>().ReverseMap();

            CreateMap<Hotel, HotelVirtualRestaurantsDTO>().ForMember(dest => dest.HotelRestaurants, opt => opt.ResolveUsing(src =>
                {
                    if (src.HotelRestaurants.Any())
                        return src.HotelRestaurants.Where(r => r.Restaurant.IsActive);
                    else
                        return null;
                }))
                .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.LogoImage))
                        return api_path + "Images/Hotel/" + src.Id.ToString() + "/Logo/" + src.LogoImage;
                    else
                        return null;
                }))
                .ForMember(dest => dest.BackgroundImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.BackgroundImage))
                        return api_path + "Images/Hotel/" + src.Id.ToString() + "/Background/" + src.BackgroundImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<HotelRestaurant, HotelRestaurantDTO>().ReverseMap();

            CreateMap<HotelsNearByPlace, HotelNearByPlacesDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Hotel/" + src.HotelId.ToString() + "/NearByPlaces/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<RestaurantMenu, RestaurantMenusDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.Image))
                            return api_path + "Images/Restaurant/" + src.RestaurantId.ToString() + "/Menu/" + src.Id + "/" + src.Image;
                        else
                            return null;
                    }))
                    .ForMember(dest => dest.RestaurantCurrencySymbol, opt => opt.ResolveUsing(src =>
                    {
                        if (src.Restaurant.Currency != null)
                            return src.Restaurant.Currency.Symbol;
                        else
                            return null;
                    })).ReverseMap();

            CreateMap<RestaurantMenu, ConcreteRestaurantMenusDTO>().ReverseMap();

            CreateMap<RestaurantMenuGroup, RestaurantMenuGroupsDTO>().ReverseMap();
            CreateMap<RestaurantMenuGroup, RestaurantMenuGroupsDTO4MenuItem>().ReverseMap();
            CreateMap<RestaurantMenuGroupsOfMenu, RestMenuGroupsDTO>().ReverseMap();

            CreateMap<RestaurantMenuItem, RestaurantMenuItemsDTO>().ReverseMap();
            CreateMap<RestaurantMenuItem, RestaurantMenuItemDetailsDTO>().ReverseMap();
            CreateMap<RestaurantFavoriteMenuItems, RestaurantFavoriteMenuItemsDTO>().ReverseMap();
            CreateMap<RestaurantMenuItem, RestMenuItemsDTO>()
                .ForMember(dest => dest.RestaurantCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency != null)
                    {
                        return src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.Symbol;
                    }
                    return null;
                }))
                .ReverseMap();
            CreateMap<RestaurantMenuItem, RestMenuItemsPendingReviewDTO>()
                .ForMember(dest => dest.RestaurantCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency != null)
                    {
                        return src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.Symbol;
                    }
                    return null;
                }))
                .ReverseMap();

            CreateMap<RestaurantMenuItemImage, RestaurantMenuItemImagesDTO>().ReverseMap();

            CreateMap<RestaurantMenuItem, RestaurantMenuItemDetailedDTO>().ForMember(dest => dest.Ratings_Count, opt => opt.ResolveUsing(src =>
            {
                if (src.MenuItemReviews.Any())
                {
                    return src.MenuItemReviews.Count();
                }
                return 0;
            }))
            .ForMember(dest => dest.Rating_Average, opt => opt.ResolveUsing(src =>
            {
                if (src.MenuItemReviews.Any())
                {
                    return src.MenuItemReviews.Average(r => r.Score);
                }
                return 0;
            }))
            .ForMember(dest => dest.MenuItemAdditionals, opt => opt.ResolveUsing(src =>
            {
                return src.MenuItemAdditionals.Where(ia => ia.IsActive == true && ia.MenuAdditionalGroup.IsActive);

            }))
            .ForMember(dest => dest.Percentage, opt => opt.ResolveUsing(src =>
            {
                if (src.MenuItemOffers != null && src.MenuItemOffers.Count() > 0)
                {
                    var percentage = src.MenuItemOffers.Select(i => i.Percentage).FirstOrDefault();
                    return percentage;
                }
                return 0;
            }))
            .ForMember(dest => dest.OfferAmount, opt => opt.ResolveUsing(src =>
            {
                if (src.MenuItemOffers != null && src.MenuItemOffers.Count() > 0)
                {
                    var percentage = src.MenuItemOffers.Select(i => i.Percentage).FirstOrDefault();
                    var offerAmnt = (src.Price * percentage) / 100;
                    return offerAmnt;
                }
                return 0;
            }))
             .ForMember(dest => dest.RestaurantCurrencySymbol, opt => opt.ResolveUsing(src =>
             {
                 if (src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency != null)
                 {
                     return src.RestaurantMenuGroup.RestaurantMenu.Restaurant.Currency.Symbol;
                 }
                 return null;
             }))
             .ReverseMap();

            CreateMap<RestaurantMenuItem, RestaurantMenuItemInfoDTO>().ReverseMap();
            CreateMap<MenuItemOffer, MenuItemOffersDTO>()
            .ForMember(dest => dest.RestaurantName, opt => opt.ResolveUsing(src => src.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant.Name))
                //.ForMember(dest => dest.OfferAmount, opt => opt.ResolveUsing(src =>
                //{
                //    if (src.Percentage != null)
                //    {
                //        var offerAmnt = (src.RestaurantMenuItem.Price * src.Percentage) / 100;
                //        return offerAmnt;
                //    }
                //    return 0;
                //}))
            .ReverseMap();


            CreateMap<MenuItemAdditional, MenuItemAdditionalDTO>().ReverseMap();
            CreateMap<MenuAdditionalGroup, MenuAdditionalGroupDTO>()
            .ForMember(dest => dest.MenuAdditionalElements, opt => opt.ResolveUsing(src =>
            {
                return src.MenuAdditionalElements.Where(ade => ade.IsActive == true);

            })).ReverseMap();
            CreateMap<MenuAdditionalElement, MenuAdditionalElementDTO>().ReverseMap();
            CreateMap<MenuItemSize, MenuItemSizeDTO>().ReverseMap();
            CreateMap<MenuItemIngredient, MenuItemIngredientDTO>()
                .ForMember(dest => dest.UnitSymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Ingredient.IngredientNutritionValues != null)
                    {
                        return "gram(g)";
                    }
                    return null;
                }))
                .ReverseMap();
            CreateMap<MenuItemIngredient, MenuItemIngredientInfoDTO>()
               .ForMember(dest => dest.UnitSymbol, opt => opt.ResolveUsing(src =>
               {
                   if (src.Ingredient.IngredientNutritionValues != null)
                   {
                       return "gram(g)";
                   }
                   return null;
               }))
              .ReverseMap();

            CreateMap<MenuItemIngredientNutrition, MenuItemIngredientNutritionDTO>().ReverseMap();
            CreateMap<Nutrition, NutritionDTO>()
                 .ForMember(dest => dest.MeasurementUnit, opt => opt.ResolveUsing(src =>
                 {
                     if (src.MeasurementUnitId != null)
                     {
                         return src.MeasurementUnit.UnitName + "(" + src.MeasurementUnit.Symbols + ")";
                     }
                     return null;
                 }))
              .ReverseMap();

            CreateMap<MenuItemReview, MenuItemReviewDTO>().ReverseMap();
            CreateMap<MenuItemReview, CreateMenuItemReviewDTO>().ReverseMap();
            CreateMap<MenuItemReviewAndRestDetails, CustomerMenuItemReviewDTO>().ReverseMap();

            CreateMap<RestaurantReview, RestaurantReviewDTO>().ReverseMap();
            CreateMap<RestaurantReview, CreateRestaurantReviewDTO>().ReverseMap();
            CreateMap<RestaurantReview, CustomerRestaurantReviewDTO>().ReverseMap();

            CreateMap<RestaurantOrderReview, RestaurantOrderReviewsDTO>()
                .ForMember(src => src.Customer, opt => opt.ResolveUsing(src => src.RestaurantOrder.Customer))
                .ReverseMap();

            CreateMap<RestaurantOrderReview, CreateRestaurantOrderReviewDTO>().ReverseMap();

            CreateMap<HotelReview, HotelReviewsDTO>().ReverseMap();
            CreateMap<HotelReview, CreateHotelReviewsDTO>().ReverseMap();
            CreateMap<HotelReview, CustomerHotelReviewDTO>().ReverseMap();

            //CreateMap<Customer_Table_Reservations, CustomerTableReservationsDTO>().ReverseMap();
            //CreateMap<Customer_Table_Reservations, CreateCustomerTableReservationsDTO>().ReverseMap();

            CreateMap<RestaurantTable, RestaurantTablesDTO>()
                .ForMember(dest => dest.RestaurantId, opt => opt.ResolveUsing(src =>
                {
                    if (src.RestaurantTableGroup != null)
                        return src.RestaurantTableGroup.RestaurantId;
                    else
                        return 0;
                }))
                 .ForMember(dest => dest.ActiveStatus, opt => opt.ResolveUsing(src =>
                 {
                     return src.IsActive;
                 }))
                .ReverseMap();
            //CreateMap<Customer_Reserved_Table, CustomerReservedTableDTO>().ReverseMap();

            CreateMap<SpaService, SpaServiceDTO>()
                .ForMember(dest => dest.SpaServiceDetail, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceDetail.Any())
                        return src.SpaServiceDetail.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Services/SpaService/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<SpaService, SpaServicesListDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Services/SpaService/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<SpaServiceDetail, SpaServiceDetailDTO>()
                .ForMember(dest => dest.SpaServiceDetailImages, opt => opt.ResolveUsing(src =>
            {
                if (src.SpaServiceDetailImages.Any())
                    return src.SpaServiceDetailImages.Where(sg => sg.IsActive);
                else
                    return null;

            }))
                .ForMember(dest => dest.SpaDetailAdditional, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaDetailAdditional.Any())
                        return src.SpaDetailAdditional.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaEmployeeDetails, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaEmployeeDetails.Any())
                        return src.SpaEmployeeDetails.Where(sed => sed.SpaEmployee.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaService.HotelServices.Hotel.Currency != null)
                    {
                        return src.SpaService.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();
            CreateMap<SpaServiceDetail, FavoriteSpaServiceDetailDTO>()
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.SpaService.HotelServices.Hotel))
                .ForMember(dest => dest.SpaServiceDetailImages, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceDetailImages.Any())
                        return src.SpaServiceDetailImages.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaDetailAdditional, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaDetailAdditional.Any())
                        return src.SpaDetailAdditional.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaEmployeeDetails, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaEmployeeDetails.Any())
                        return src.SpaEmployeeDetails.Where(sed => sed.SpaEmployee.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaService.HotelServices.Hotel.Currency != null)
                    {
                        return src.SpaService.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<SpaServiceDetail, SpaServiceDetailWithOfferDTO>()
                .ForMember(dest => dest.SpaServiceDetailImages, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceDetailImages.Any())
                        return src.SpaServiceDetailImages.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaDetailAdditional, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaDetailAdditional.Any())
                        return src.SpaDetailAdditional.Where(sg => sg.IsActive && sg.SpaAdditionalGroup.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaEmployeeDetails, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaEmployeeDetails.Any())
                        return src.SpaEmployeeDetails.Where(sed => sed.SpaEmployee.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.SpaServiceOffers, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceOffers.Any())
                        return src.SpaServiceOffers.Where(so => so.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.ExtraTimes, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExtraTimes.Any())
                        return src.ExtraTimes.Where(set => set.IsActive);
                    else
                        return null;
                }))
                .ForMember(dest => dest.ExtraProcedures, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExtraProcedures.Any())
                        return src.ExtraProcedures.Where(sep => sep.IsActive);
                    else
                        return null;
                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaService.HotelServices.Hotel.Currency != null)
                    {
                        return src.SpaService.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<SpaSuggestion, SpaSuggestionDTO>().ReverseMap();

            CreateMap<SpaServiceDetail, SpaServiceDetailWithImagesDTO>()
                .ForMember(dest => dest.SpaServiceDetailImages, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceDetailImages.Any())
                        return src.SpaServiceDetailImages.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaService.HotelServices.Hotel.Currency != null)
                    {
                        return src.SpaService.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<SpaServiceDetail, SpaServiceDetailListDTO>()
                .ForMember(dest => dest.Ratings_Count, opt => opt.ResolveUsing(src =>
            {
                if (src.SpaServiceReview.Any())
                {
                    return src.SpaServiceReview.Count();
                }
                return 0;
            }))
            .ForMember(dest => dest.Rating_Average, opt => opt.ResolveUsing(src =>
            {
                if (src.SpaServiceReview.Any())
                {
                    return src.SpaServiceReview.Average(r => r.Score);
                }
                return 0;
            }))
             .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
             {
                 if (src.SpaService.HotelServices.Hotel.Currency != null)
                 {
                     return src.SpaService.HotelServices.Hotel.Currency.Symbol;
                 }
                 return null;
             })).ReverseMap();

            CreateMap<SpaServiceDetail, SpaServiceDetailsInfoDTO>().ReverseMap();

            CreateMap<SpaServiceDetailImages, SpaServiceDetailImagesDTO>()
               .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
               {
                   if (src.Image != null)
                       return api_path + "Images/Services/SpaService/SpaServiceDetails/" + src.SpaServiceDetailId.ToString() + "/" + src.Image;
                   else
                       return null;
               })).ReverseMap();

            CreateMap<SpaDetailAdditional, SpaDetailAdditionalDTO>()
                .ForMember(dest => dest.SpaAdditionalGroup, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaAdditionalGroup != null && src.SpaAdditionalGroup.IsActive)
                        return src.SpaAdditionalGroup;
                    else
                        return null;

                })).ReverseMap();

            CreateMap<SpaAdditionalGroup, SpaAdditionalGroupDTO>()
                .ForMember(dest => dest.SpaAdditionalElement, opt => opt.ResolveUsing(src =>
            {
                if (src.SpaAdditionalElement.Any())
                    return src.SpaAdditionalElement.Where(sg => sg.IsActive);
                else
                    return null;

            })).ReverseMap();


            CreateMap<SpaAdditionalElement, SpaAdditionalElementDTO>()
               .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
               {
                   if (!string.IsNullOrWhiteSpace(src.Image))
                       return api_path + "Images/Services/SpaService/SpaAdditionalGroups/" + src.SpaAdditionalGroupId.ToString() + "/SpaAdditionalElements/" + src.Id + "/" + src.Image;
                   else
                       return null;
               })).ReverseMap();
            CreateMap<SpaEmployeeDetails, SpaEmployeeDetailsDTO>()
                .ForMember(dest => dest.SpaEmployee, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaEmployee.IsActive != null && src.SpaEmployee.IsActive)
                        return src.SpaEmployee;
                    else
                        return null;

                })).ReverseMap();
            CreateMap<SpaRoom, SpaRoomDTO>().ReverseMap();

            //CreateMap<SpaDetailEmployeeTimeSlot, SpaDetailEmployeeTimeSlotDTO>().ReverseMap();

            CreateMap<SpaServiceOffer, SpaServiceOffersDTO>()
               .ForMember(dest => dest.HotelName, opt => opt.ResolveUsing(src => src.SpaServiceDetail.SpaService.HotelServices.Hotel.Name)).ReverseMap();

            CreateMap<SpaServiceOffer, SpaServiceDetailOfferDTO>()
                .ForMember(dest => dest.DiscountPrice, opt => opt.ResolveUsing(src =>
                {
                    if (src.Percentage != null)
                    {
                        var offerAmnt = (src.SpaServiceDetail.Price * src.Percentage) / 100;
                        offerAmnt = System.Math.Round(offerAmnt, 2);
                        return src.SpaServiceDetail.Price - offerAmnt;
                    }
                    return 0;
                })).ReverseMap();

            CreateMap<SpaServiceOffer, HotelSpaServiceOffersDTO>()
                 .ForMember(dest => dest.OfferDescription, opt => opt.ResolveUsing(src => src.Description))
                 .ForMember(dest => dest.SpaServiceDetailId, opt => opt.ResolveUsing(src =>
                 {
                     if (src.SpaServiceDetail != null)
                     {
                         return src.SpaServiceDetailId;
                     }
                     return 0;
                 }))
                  .ForMember(dest => dest.Name, opt => opt.ResolveUsing(src =>
                  {
                      if (src.SpaServiceDetail != null)
                      {
                          return src.SpaServiceDetail.Name;
                      }
                      return null;
                  }))
                   .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                   {
                       if (src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency != null)
                       {
                           return src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.Symbol;
                       }
                       return null;
                   }))
                    .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                    {
                        if (src.SpaServiceDetail != null)
                        {
                            return src.SpaServiceDetail.Price;
                        }
                        return 0;
                    }))
                     .ForMember(dest => dest.DiscountPrice, opt => opt.ResolveUsing(src =>
                     {
                         if (src.Percentage != null)
                         {
                             var offerAmnt = (src.SpaServiceDetail.Price * src.Percentage) / 100;
                             offerAmnt = System.Math.Round(offerAmnt, 2);
                             return src.SpaServiceDetail.Price - offerAmnt;
                         }
                         return 0;
                     }))
                    .ForMember(dest => dest.OfferImage, opt => opt.ResolveUsing(src =>
                   {
                       if (src.OfferImage != null)
                           return api_path + "Images/Services/SpaService/SpaServiceDetails/" + src.SpaServiceDetailId.ToString() + "/OfferImage/" + src.Id.ToString() + "/" + src.OfferImage;
                       else
                           return null;
                   }))
                .ReverseMap();

            CreateMap<SpaEmployee, SpaEmployeeDTO>().ReverseMap();
            CreateMap<Customer, SpaEmployeeInfoDTO>()
              .ForMember(dest => dest.ProfileImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.ProfileImage))
                        return api_path + "Images/Customers/" + src.Id.ToString() + "/" + src.ProfileImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<SPAServiceGroup, SPAServiceGroupDTO>().ReverseMap();
            //CreateMap<SPAServiceBooking, SPAServiceBookingDTO>()
            //.ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src =>
            //{
            //    return src.SPAService.HotelServices.Hotel;
            //})).ReverseMap();

            CreateMap<SPAServiceBooking, CreateSPAServiceBookingDTO>().ReverseMap();
            CreateMap<SPAServiceBookingDetail, SPAServiceBookingDetailDTO>().ReverseMap();
            CreateMap<SPAServiceBookingDetail, CreateSPAServiceBookingDetailDTO>().ReverseMap();
            CreateMap<SPAServiceBooking, UpdateSPAServiceBookingDTO>().ReverseMap();
            //CreateMap<SPAService, HotelSPAServiceDTO>().ReverseMap();
            //CreateMap<SPAServiceGroup, SPAServiceGroupDetDTO>().ReverseMap();
            //CreateMap<SPAServiceDetailsGroup, SPAServiceDetailsGroupDTO>().ReverseMap();

            CreateMap<SpaOrderReview, SpaOrderReviewDTO>()
                .ForMember(src => src.Customer, opt => opt.ResolveUsing(src => src.SpaOrder.Customer))
                .ReverseMap();
            CreateMap<SpaOrderReview, CreateSpaOrderReviewDTO>().ReverseMap();


            CreateMap<Trip, HotelTripsDTO>().ReverseMap();
            CreateMap<Trip, HotelTripDetailsDTO>().
                ForMember(dest => dest.TripDetails, opt => opt.ResolveUsing(src =>
            {
                if (src.TripDetails.Any())
                    return src.TripDetails.Where(t => t.IsActive);
                else
                    return null;
            }
            )).ReverseMap();
            CreateMap<TripDetail, TripDetailsDTO>().ReverseMap();
            CreateMap<TripBooking, TripBookingDTO>()
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src =>
                {
                    return src.Trip.HotelServices.Hotel;
                })).ReverseMap();
            CreateMap<TripBooking, CreateTripBookingDTO>().ReverseMap();
            CreateMap<TripBooking, UpdateTripBookingDTO>().ReverseMap();
            CreateMap<TripReview, TripReviewDTO>().ReverseMap();
            CreateMap<TripReview, CreateTripReviewDTO>().ReverseMap();

            CreateMap<Laundry, HotelLaundryDTO>().ReverseMap();
            CreateMap<Laundry, LaundryServiceDetailsDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Services/LaundryService/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                }))

                .ReverseMap();
            CreateMap<LaundryDetail, LaundryDetailsDTO>()
                .ForMember(dest => dest.LaundryDetailAdditional, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryDetailAdditional.Any())
                        return src.LaundryDetailAdditional.Where(l => l.IsActive && l.LaundryAdditionalGroup.IsActive);
                    else
                        return null;
                }))
                   .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                   {
                       if (src.Laundry.HotelServices.Hotel.Currency != null)
                       {
                           return src.Laundry.HotelServices.Hotel.Currency.Symbol;
                       }
                       return null;
                   }))
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Services/LaundryService/" + src.LaundryId.ToString() + "/LaundryDetails/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                }))
                .ReverseMap();
            CreateMap<Garments, GarmentsDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Garments/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                }))
                .ReverseMap();
            CreateMap<LaundryDetailAdditional, LaundryDetailAdditionalDTO>()
                .ForMember(dest => dest.LaundryAdditionalGroup, opt => opt.ResolveUsing(src =>
                    {
                        if (src.LaundryAdditionalGroup != null && src.LaundryAdditionalGroup.IsActive)
                            return src.LaundryAdditionalGroup;
                        else
                            return null;
                    }
            )).ReverseMap();

            CreateMap<LaundryAdditionalGroup, LaundryAdditionalGroupDTO>()
               .ForMember(dest => dest.LaundryAdditionalElement, opt => opt.ResolveUsing(src =>
               {
                   if (src.LaundryAdditionalElements.Any())
                       return src.LaundryAdditionalElements.Where(l => l.IsActive);
                   else
                       return null;
               }
            )).ReverseMap();

            CreateMap<LaundryAdditionalElement, LaundryAdditionalElementDTO>()
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryAdditionalGroup.Hotel.Currency != null)
                    {
                        return src.LaundryAdditionalGroup.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<LaundryReview, LaundryReviewsDTO>().ReverseMap();
            CreateMap<LaundryReview, CreateLaundryReviewDTO>().ReverseMap();

            CreateMap<LaundryOrderReview, LaundryOrderReviewDTO>()
                .ForMember(src => src.Customer, opt => opt.ResolveUsing(src => src.LaundryOrder.Customer)).ReverseMap();
            CreateMap<LaundryOrderReview, CreateLaundryOrderReviewDTO>().ReverseMap();

            CreateMap<TaxiDetail, TaxiDetailDTO>().ReverseMap();
            CreateMap<TaxiDestination, TaxiDestinationDTO>().ReverseMap();

            CreateMap<TaxiBooking, TaxiBookingDTO>().ReverseMap();
            CreateMap<TaxiBooking, CreateTaxiBookingDTO>().ReverseMap();
            CreateMap<TaxiBooking, UpdateTaxiBookingDTO>().ReverseMap();
            CreateMap<TaxiBooking, CustomerTaxiBookingDTO>()
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.TaxiDetails.HotelServices.Hotel))
                .ReverseMap();

            CreateMap<TaxiReview, TaxiReviewDTO>().ReverseMap();
            CreateMap<TaxiReview, CreateTaxiReviewDTO>().ReverseMap();
            CreateMap<TaxiBookingReview, CreateTaxiBookingReviewDTO>().ReverseMap();
            CreateMap<TaxiBookingReview, TaxiBookingReviewDTO>()
                .ForMember(dest => dest.Customer, opt => opt.ResolveUsing(src => src.TaxiBooking.Customer))
                .ReverseMap();

            CreateMap<Cuisine, CuisineDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.Image))
                            return api_path + "Images/Cuisines/" + src.Id.ToString() + "/" + src.Image;
                        else
                            return null;
                    }
                    )).ReverseMap();

            CreateMap<RestaurantCuisine, RestaurantCuisinesDTO>().ReverseMap();
            CreateMap<RestaurantCuisine, RestCuisinesDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "Images/Restaurant/" + src.RestaurantId.ToString() + "/Cuisines/" + src.Id + "/" + src.Image;
                    else
                        return null;
                }
                )).ReverseMap();

            CreateMap<Currency, GetCurrencyDTO>().ReverseMap();
            CreateMap<Currency, CurrenciesDTO>().ReverseMap();

            CreateMap<HotelServices, HotelServicesDTO>().ReverseMap();
            CreateMap<HotelServicesReviews, HotelServicesReviewsDTO>().ReverseMap();
            CreateMap<HotelServicesReviews, CreateHotelServicesReviewsDTO>().ReverseMap();

            CreateMap<HotelAllServicesReviews, HotelAllServicesReviewsDTO>()
                    .ForMember(dest => dest.LogoImage, opt => opt.ResolveUsing(src =>
                    {
                        if (!string.IsNullOrWhiteSpace(src.LogoImage))
                            return api_path + "Images/Hotel/" + src.HotelId.ToString() + "/Logo/" + src.LogoImage;
                        else
                            return null;
                    })).ReverseMap();

            CreateMap<Services, ServicesDTO>().ReverseMap();

            CreateMap<Room, HotelRoomsDTO>().ReverseMap();
            CreateMap<Room, HotelRoomDetailsDTO>().ReverseMap();

            CreateMap<HotelRoomImages, HotelRoomImagesDTO>().ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
            {
                if (!string.IsNullOrWhiteSpace(src.Image))
                    return api_path + "Images/Hotel/Rooms/" + src.RoomId + "/" + src.Image;
                else
                    return null;
            }
                )).ReverseMap();

            CreateMap<RoomReview, RoomReviewsDTO>().ReverseMap();
            CreateMap<RoomReview, CreateRoomReviewDTO>().ReverseMap();

            CreateMap<CustomerPreferences, ModifyRestaurantPreferenceDTO>().ReverseMap();
            CreateMap<Preferences, PreferenceDTO>().ReverseMap();

            CreateMap<Country, CountryDTO>()
                 .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                 {
                     if (!string.IsNullOrWhiteSpace(src.Image))
                         return api_path + "Images/Country/" + src.Id.ToString() + "/" + src.Image;
                     else
                         return null;
                 }
                 )).ReverseMap();

            CreateMap<CustomerMonthlyPayments, CustomerMonthlyPaymentsDTO>().ReverseMap();
            CreateMap<CustomerYearlyPayments, CustomerYearlyPaymentsDTO>().ReverseMap();

            CreateMap<CustomerMonthlyPaymentHistory, CustomerMonthlyPaymentHistoryDTO>()
                .ForMember(dest => dest.Month, opt => opt.ResolveUsing(src => System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(src.Month))).ReverseMap();

            //CreateMap<RestaurantOrder, CustRestaurantOrdersDTO>()
            //   .ForMember(dest => dest.Order_Total, opt => opt.ResolveUsing(src =>
            //   {
            //       if (src == null || !src.RestaurantOrderItems.Any())
            //           return 0;
            //       decimal result = 0.0m;
            //       var total = src.RestaurantOrderItems.Sum(i => (i.Qty * i.RestaurantMenuItem.Price));

            //       var additional = src.RestaurantOrderItems.Sum(i => i.RestaurantOrderItemAdditionals.Sum(o => o.MenuAdditionalElement.AdditionalCost));

            //       if (total.HasValue)
            //           result = (decimal)total + (decimal)additional;

            //       return result;
            //   })).ReverseMap();

            CreateMap<RestaurantOrder, CustRestaurantOrdersDTO>().ReverseMap();

            CreateMap<CustomerSPAPreferences, ModifySPAServicePreferenceDTO>().ReverseMap();
            CreateMap<SpaServicePreferences, SPAServicePreferenceDTO>().ReverseMap();
            CreateMap<SpaServiceReview, HotelSPAServiceReviewDTO>().ReverseMap();
            CreateMap<SpaServiceReview, CustomerSpaServiceReviewDTO>().ReverseMap();
            CreateMap<SpaServiceDetail, SpaServiceDetailInfoDTO>().ReverseMap();
            CreateMap<SpaServiceReview, CreateHotelSPAServiceReviewsDTO>().ReverseMap();
            CreateMap<Language, LanguageDTO>().ReverseMap();

            CreateMap<HouseKeepingFacility, HouseKeepingFacilitiesDTO>()
                 .ForMember(dest => dest.HouseKeepingFacilityDetails, opt => opt.ResolveUsing(src =>
                 {
                     if (src.HouseKeepingFacilityDetails != null && src.HouseKeepingFacilityDetails.Count() > 0)
                     {
                         return src.HouseKeepingFacilityDetails.Where(hkf => hkf.IsActive);
                     }
                     else return null;
                 }))
                  .ForMember(dest => dest.HouseKeepingfacilitiesAdditionals, opt => opt.ResolveUsing(src =>
                  {
                      if (src.HouseKeepingfacilitiesAdditionals != null && src.HouseKeepingfacilitiesAdditionals.Count() > 0)
                      {
                          return src.HouseKeepingfacilitiesAdditionals.Where(hkfa => hkfa.HousekeepingAdditionalGroup.IsActive && hkfa.IsActive);
                      }
                      else return null;
                  })).ReverseMap();

            CreateMap<HouseKeepingFacility, GetHouseKeepingFacilitiesDTO>().ReverseMap();

            CreateMap<HouseKeepingFacilityDetail, HouseKeepingFacilityDetailDTO>()
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.HouseKeepingFacility.HotelService.Hotel.Currency != null)
                     {
                         return src.HouseKeepingFacility.HotelService.Hotel.Currency.Symbol;
                     }
                     else return null;
                 })).ReverseMap();

            CreateMap<HouseKeepingfacilitiesAdditional, HouseKeepingfacilitiesAdditionalDTO>()
                 .ForMember(dest => dest.Additional_Group_Name, opt => opt.ResolveUsing(src =>
                  {
                      if (src.HousekeepingAdditionalGroup == null)
                          return null;
                      return src.HousekeepingAdditionalGroup.Name;
                  }))
                .ForMember(dest => dest.HouseKeepingAdditionalGroup_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalGroup == null)
                        return 0;
                    return src.HouseKeepingAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<HousekeepingAdditionalGroup, HousekeepingAdditionalGroupDTO>()
                .ForMember(dest => dest.HousekeepingAdditionalElements, opt => opt.ResolveUsing(src =>
            {
                if (src.HousekeepingAdditionalElements != null && src.HousekeepingAdditionalElements.Count() > 0)
                {
                    return src.HousekeepingAdditionalElements.Where(ha => ha.IsActive);
                }
                else
                    return null;
            })).ReverseMap();

            CreateMap<HousekeepingAdditionalElement, HousekeepingAdditionalElementDTO>()
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalGroup.Hotel.Currency != null)
                    {
                        return src.HousekeepingAdditionalGroup.Hotel.Currency.Symbol;
                    }
                    else return null;
                })).ReverseMap();

            //Hotel house Keeping Info
            CreateMap<HotelHouseKeepingInfo, HotelHouseKeepingInfoDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/Hotel/" + src.HotelId.ToString() + "/HouseKeepingInfoImage/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<Room, RoomDTO>().
            ForMember(dest => dest.RoomDetails, opt => opt.ResolveUsing(src =>
            {
                if (src.RoomDetails.Any())
                    return src.RoomDetails.Where(r => r.IsActive);
                else
                    return null;
            }
            )).ReverseMap();

            CreateMap<RoomDetails, RoomDetailsDTO>().
            ForMember(dest => dest.HouseKeeping, opt => opt.ResolveUsing(src =>
            {
                if (src.HouseKeeping.Any())
                    return src.HouseKeeping.Where(h => h.IsActive);
                else
                    return null;
            }))
             .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
            {
                if (src.Image != null)
                    return api_path + "Images/Hotel/Rooms/" + src.RoomId.ToString() + "/RoomDetails/" + src.Id + "/" + src.Image;
                else
                    return null;
            })).ReverseMap();

            CreateMap<HouseKeeping, RoomHousekeepingDTO>().
                ForMember(dest => dest.HouseKeepingFacility, opt => opt.ResolveUsing(src =>
            {
                if (src.HouseKeepingFacility != null && src.HouseKeepingFacility.IsActive)
                    return src.HouseKeepingFacility;
                else
                    return null;
            }
            )).ReverseMap();

            CreateMap<CustomerHotelRoom, CustomerHotelRoomDTO>().ReverseMap();
            CreateMap<Room, RoomInfoDTO>().
           ForMember(dest => dest.RoomDetails, opt => opt.ResolveUsing(src =>
           {
               if (src.RoomDetails.Any())
                   return src.RoomDetails.Where(r => r.IsActive);
               else
                   return null;
           }
           )).ReverseMap();
            CreateMap<RoomDetails, RoomInfoDetailsDTO>()
                 .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Image != null)
                         return api_path + "Images/Hotel/Rooms/" + src.RoomId.ToString() + "/RoomDetails/" + src.Id + "/" + src.Image;
                     else
                         return null;
                 })).ReverseMap();

            CreateMap<CustomerHotelRoom, CustomerHotelRoomInfoDTO>()
                 .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Room != null)
                     {
                         return src.Room.Hotel.Currency.Symbol;
                     }
                     else return null;
                 }))
                 .ForMember(dest => dest.Room, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Room != null && src.Room.IsActive)
                     {
                         return src.Room;
                     }
                     else return null;
                 })).ReverseMap();

            //Restaurant Allergen/Ingredient details
            CreateMap<Allergen, AllergenDTO>()
                .ForMember(dest => dest.AllergenImage, opt => opt.ResolveUsing(src =>
                 {
                     if (!string.IsNullOrWhiteSpace(src.AllergenImage))
                         return api_path + "/Images/Preferences/Allergens/" + src.Id.ToString() + "/" + src.AllergenImage;
                     else
                         return null;
                 })).ReverseMap();
            CreateMap<AllergenCategory, AllergenCategoriesDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/AllergenCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<Allergen, AllergenPreferenceDTO>()
                .ForMember(dest => dest.AllergenImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.AllergenImage))
                        return api_path + "/Images/Preferences/Allergens/" + src.Id.ToString() + "/" + src.AllergenImage;
                    else
                        return null;
                })).ReverseMap();
            CreateMap<AllergenCategory, AllergenCategoriesPrefDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/AllergenCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();


            CreateMap<Ingredient, IngredientDTO>()
                .ForMember(dest => dest.IngredientImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.IngredientImage))
                        return api_path + "/Images/Preferences/Ingredients/" + src.Id.ToString() + "/" + src.IngredientImage;
                    else
                        return null;
                })).ReverseMap();
            CreateMap<IngredientCategory, IngredientCategoriesDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/IngredientCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<Ingredient, IngredientPreferenceDTO>()
                .ForMember(dest => dest.IngredientImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.IngredientImage))
                        return api_path + "/Images/Preferences/Ingredients/" + src.Id.ToString() + "/" + src.IngredientImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<IngredientCategory, IngredientCategoriesPrefDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/IngredientCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<DrinkGroups, DrinkGroupsDTO>()
            .ForMember(dest => dest.DrinkType, opt => opt.ResolveUsing(src =>
            {
                switch (src.DrinkType)
                {
                    case true:
                        return "NonAlcoholic";
                    case false:
                        return "Alcoholic";
                    default:
                        return "None";
                }
            })).ReverseMap();

            CreateMap<FoodProfile, FoodProfileDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/FoodProfiles/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<CustomerAllergenPreference, ModifyAllergenPreferenceDTO>().ReverseMap();
            CreateMap<CustomerIngredientPreference, ModifyIngredientPreferenceDTO>().ReverseMap();

            //Customer Preferred Food Profile
            CreateMap<CustomerPreferredFoodProfile, CustomerPreferredFoodProfileDTO>().ReverseMap();

            //Concierge
            CreateMap<ConciergeGroup, ConciergeGroupDTO>()
               .ForMember(dest => dest.Concierges, opt => opt.ResolveUsing(src =>
               {
                   if (src.Concierges.Any())
                       return src.Concierges.Where(l => l.IsActive);
                   else
                       return null;
               }
            )).ReverseMap();
            CreateMap<Concierge, ConciergeDTO>().ReverseMap();

            CreateMap<CustomerAllHotelAndRestActiveOrders, AllRestaurantsAndHotelsOrdersDTO>()
               .ForMember(dest => dest.Icon, opt => opt.ResolveUsing(src =>
               {
                   if (!string.IsNullOrWhiteSpace(src.Icon))
                   {
                       switch (src.OrderType)
                       {
                           case "Restaurant":
                               return api_path + "Images/Restaurant/" + src.OrgId.ToString() + "/Logo/" + src.Icon;
                           default:
                               return api_path + "Images/Hotel/" + src.OrgId.ToString() + "/Logo/" + src.Icon;
                       }
                   }
                   else
                       return null;
               }
            ))
            .ForMember(dest => dest.StartDateTime, opt => opt.ResolveUsing(src =>
            {
                if (src.StartDateTime != null)
                    return Convert.ToDateTime(src.StartDateTime).AddTicks(-(Convert.ToDateTime(src.StartDateTime).Ticks % TimeSpan.TicksPerSecond));
                else
                    return src.StartDateTime;
            }
            ))
           .ForMember(dest => dest.EndDateTime, opt => opt.ResolveUsing(src =>
           {
               if (src.EndDateTime != null)
                   return Convert.ToDateTime(src.EndDateTime).AddTicks(-(Convert.ToDateTime(src.EndDateTime).Ticks % TimeSpan.TicksPerSecond));
               else
                   return src.EndDateTime;
           }
            )).ReverseMap();

            CreateMap<CustomerAllHotelAndRestOrderHistory, CustomerRestAndHotelsOrdersHistoryDTO>()
                .ForMember(dest => dest.PlaceId, opt => opt.ResolveUsing(src => src.OrgId))
                .ForMember(dest => dest.Icon, opt => opt.ResolveUsing(src =>
                   {
                       if (!string.IsNullOrWhiteSpace(src.Icon))
                       {
                           switch (src.OrderType)
                           {
                               case "Restaurant":
                                   return api_path + "Images/Restaurant/" + src.OrgId.ToString() + "/Logo/" + src.Icon;
                               default:
                                   return api_path + "Images/Hotel/" + src.OrgId.ToString() + "/Logo/" + src.Icon;
                           }
                       }
                       else
                           return null;
                   }
                )).ReverseMap();

            CreateMap<ExtraTime, ExtraTimeDTO>()
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                    {
                        if (src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency != null)
                        {
                            return src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.Symbol;
                        }
                        else return null;
                    }))
               .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
               {
                   return api_path + "Images/Services/SpaService/defExtraTimeImg.png";
               })).ReverseMap();


            CreateMap<ExtraProcedure, ExtraProcedureDTO>()
              .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
              {
                  if (src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency != null)
                  {
                      return src.SpaServiceDetail.SpaService.HotelServices.Hotel.Currency.Symbol;
                  }
                  else return null;
              }))
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/Services/SpaService/SpaServiceDetails/" + src.SpaServiceDetailId.ToString() + "/ExtraProcedures/" + src.Id + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<SpaIngredient, SpaIngredientPreferenceDTO>()
                .ForMember(dest => dest.IngredientImage, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.IngredientImage))
                        return api_path + "/Images/SpaIngredients/" + src.Id.ToString() + "/" + src.IngredientImage;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<SpaIngredientCategory, SpaIngredientCategoriesPrefDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/SpaIngredientCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<CustomerSpaServicePreference, ModifySpaIngredientPreferenceDTO>().ReverseMap();

            CreateMap<SpaEmployeeAvailableTimeSlot, SpaEmployeeAvailableTimeSlotDTO>().ReverseMap();

            CreateMap<WakeUp, WakeUpDTO>().ReverseMap();
            CreateMap<WakeUp, CreateWakeUpDTO>().ReverseMap();
            CreateMap<Room, WakeUpRoomDTO>().ReverseMap();

            CreateMap<AllHotelServicesOffers, AllHotelServicesOffersDTO>()
                .ForMember(dest => dest.OfferImage, opt => opt.ResolveUsing(src =>
                    {
                        if (src.OfferImage != null)
                            switch (src.OfferType)
                            {
                                case "Spa":
                                    return api_path + "Images/Services/SpaService/SpaServiceDetails/" + src.OfferDetailsId.ToString() + "/OfferImage/" + src.OfferId.ToString() + "/" + src.OfferImage;

                                case "Excursion":
                                    return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.OfferDetailsId.ToString() + "/OfferImage/" + src.OfferId.ToString() + "/" + src.OfferImage;

                                default:
                                    return null;
                            }
                        else
                            return null;
                    })).ReverseMap();
        }
    }
}