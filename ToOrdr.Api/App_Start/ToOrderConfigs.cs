﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.App_Start
{
    public static class ToOrderConfigs
    {
        public static int DefaultPageSize
        {
            get
            {
                int pageSize = 50;
                int.TryParse(ConfigurationManager.AppSettings.Get("PageSize"), out pageSize);
                return pageSize;
            }
        }
        public static string QuickPayAuthToken
        {
            get
            {
                string quickPayToken = string.Empty;
                quickPayToken = ConfigurationManager.AppSettings.Get("QuickPayAuthToken");
                return quickPayToken;
            }
        }

        public static string BackOfficeUrl
        {
            get
            {
                string url = string.Empty;
                url = ConfigurationManager.AppSettings.Get("BackofficeUrl");
                return url;
            }
        }

    }
}