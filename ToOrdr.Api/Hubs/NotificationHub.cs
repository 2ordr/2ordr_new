﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.Hubs
{
    public class NotificationHub:Hub
    {
        //[HubMethodName("NotifyClients")]
        public static void NotifyStatusChangeToKitchenAdmin()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

            // the update client method will update the connected client about any recent changes in the server data
            context.Clients.All.updatedClients();
        }
    }
}