﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ToOrdr.Localization;

namespace ToOrdr.Api.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }


    public class ForgotPasswordModel
    {
        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }

    }



    public class ChangePasswordBindingModel
    {
        [Required(ErrorMessageResourceName = "Rqd_Password", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Password", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$", ErrorMessageResourceName = "Valid_Password", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessageResourceName = "Valid_ConfirmPassword", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }
    }

    public class ChangeEmailBindingModel
    {
        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        public string OldEmail { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        public string NewEmail { get; set; }
    }

    public class ExternalTokenModel
    {
        [Required(ErrorMessageResourceName = "Identity_Provider_Required", ErrorMessageResourceType = typeof(Resources))]
        public string Provider { get; set; }

        public string ProviderKey { get; set; }

        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }


    public class RegisterBindingModel
    {
        [Required(ErrorMessageResourceName = "Rqd_Customer_ForeName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(50, ErrorMessageResourceName = "Max_Customer_ForeName", ErrorMessageResourceType = typeof(Resources))]
        public string FirstName { get; set; }


        [Required(ErrorMessageResourceName = "Rqd_LastName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(50, ErrorMessageResourceName = "Max_LastName", ErrorMessageResourceType = typeof(Resources))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(100, ErrorMessageResourceName = "Max_Email", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Telephone", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(15, ErrorMessageResourceName = "Max_Telephone", ErrorMessageResourceType = typeof(Resources))]
        public string Telephone { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CountryId", ErrorMessageResourceType = typeof(Resources))]
        public int CountryId { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CurrencyId", ErrorMessageResourceType = typeof(Resources))]
        public int CurrencyId { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_PhoneCode", ErrorMessageResourceType = typeof(Resources))]
        public string PhoneCode { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Password", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$", ErrorMessageResourceName = "Valid_Password", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceName = "Valid_ConfirmPassword", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }

    }
    
    public class OTPVerificationBindingModel
    {
        [Required(ErrorMessageResourceName = "Rqd_PhoneCode", ErrorMessageResourceType = typeof(Resources))]
        public string PhoneCode { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Telephone", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(15, ErrorMessageResourceName = "Max_Telephone", ErrorMessageResourceType = typeof(Resources))]
        public string Telephone { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_OTPCode", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(15, ErrorMessageResourceName = "Max_OTPCode", ErrorMessageResourceType = typeof(Resources))]
        public string OTP { get; set; }
    }
    public class ResendOTPBindingModel
    {
        [Required(ErrorMessageResourceName = "Rqd_PhoneCode", ErrorMessageResourceType = typeof(Resources))]
        public string PhoneCode { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Telephone", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(15, ErrorMessageResourceName = "Max_Telephone", ErrorMessageResourceType = typeof(Resources))]
        public string Telephone { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email address")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
