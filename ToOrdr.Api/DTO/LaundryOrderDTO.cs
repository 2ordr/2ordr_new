﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.DTO
{
    public class LaundryOrderProfile : AutoMapper.Profile
    {
        public LaundryOrderProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<LaundryOrder, LaundryOrderDTO>()
                .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.LaundryOrderItems.Any())
                        return 0; ;

                    return src.LaundryOrderItems.Count();
                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Room.Hotel.Currency != null)
                    {
                        return src.Room.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ReverseMap();

            CreateMap<LaundryOrder, LaundryOrderInfoDTO>()
                 .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Room.Hotel != null)
                     {
                         return src.Room.Hotel;
                     }
                     return null;
                 }))
               .ReverseMap();

            CreateMap<LaundryOrderItems, LaundryOrderItemsDTO>().ReverseMap();

            CreateMap<LaundryOrderItemsAdditional, LaundryOrderItemsAdditionalDTO>()
                .ForMember(dest => dest.Laundry_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryAdditionalElement == null)
                        return null;
                    return src.LaundryAdditionalElement.LaundryAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Laundry_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryAdditionalElement == null)
                        return 0;
                    return src.LaundryAdditionalElement.LaundryAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<LaundryOrderItemImages, LaundryOrderItemImagesDTO>()
                 .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                 {
                     if (!string.IsNullOrWhiteSpace(src.Image))
                         return api_path + "Images/Customers/" + src.LaundryOrderItems.LaundryOrder.CustomerId.ToString() + "/Room/" + src.LaundryOrderItems.LaundryOrder.RoomId.ToString() + "/LaundryOrder/" + src.LaundryOrderItems.LaundryDetailId.ToString() + "/" + src.Image;
                     else
                         return null;
                 }))
               .ReverseMap();

            CreateMap<LaundryOrder, CustomerActiveLaundryOrders>()
               .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
               {
                   if (src == null || !src.LaundryOrderItems.Any())
                       return 0;

                   return src.LaundryOrderItems.Count();
               }))
               .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src => src.Customer.Currency.Symbol))
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src =>
                {
                    if (src.RoomId != 0)
                        return src.Room.Hotel;
                    return null;
                }))
               .ReverseMap();
            CreateMap<CustomerActiveLaundryOrdersAndBaskets, CustomerActiveLaundyOrdersAndBasketsDTO>().ReverseMap();
        }
    }

    public class LaundryOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual ICollection<LaundryOrderItemsDTO> LaundryOrderItems { get; set; }

    }

    public class LaundryOrderItemsDTO
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public Nullable<System.DateTime> ScheduledDate { get; set; }
        public bool LaundryDetailOrderStatus { get; set; }
        public virtual LaundryDetailsInCartDTO LaundryDetail { get; set; }
        public virtual ICollection<LaundryOrderItemsAdditionalDTO> LaundryOrderItemsAdditional { get; set; }
        public virtual ICollection<LaundryOrderItemImagesDTO> LaundryOrderItemImages { get; set; }
    }

    public class LaundryOrderItemsAdditionalDTO
    {
        public int Laundry_Additional_Group_ID { get; set; }
        public string Laundry_Additional_Group_Name { get; set; }
        public int Id { get; set; }
        public int LaundryOrderItemsId { get; set; }
        public int LaundryAdditionalElementId { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }

        public virtual LaundryAdditionalElementDTO LaundryAdditionalElement { get; set; }
    }

    public class LaundryOrderItemImagesDTO
    {
        public int Id { get; set; }
        public int LaundryOrderItemId { get; set; }
        public string Image { get; set; }
    }

    public class CustomerActiveLaundryOrders
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public double OrderTotalInPreferredCurrency { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual ICollection<LaundryOrderItemsDTO> LaundryOrderItems { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
    }

    public class LaundryOrderInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual HotelPdfDTO Hotel { get; set; }
        public virtual ICollection<LaundryOrderItemsDTO> LaundryOrderItems { get; set; }

    }

}