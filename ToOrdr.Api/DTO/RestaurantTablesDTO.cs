﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class RestaurantTablesBaseDTO
    {
        public int TableNumber { get; set; }

        public int RestaurantId { get; set; }

        public int TotalSeats { get; set; }

        public string Description { get; set; }

        public bool ActiveStatus { get; set; }

        public string TableStatus { get; set; }
    }
    public class RestaurantTablesDTO : RestaurantTablesBaseDTO
    {
        public int Id { get; set; }

        public System.DateTime CreationDate { get; set; }
    }
}