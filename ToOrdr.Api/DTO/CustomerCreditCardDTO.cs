﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{

    public class CustomerCreditCardProfile : AutoMapper.Profile
    {
        public CustomerCreditCardProfile()
        {



            CreateMap<CustomerPaymentCards, CustomerCreditCardDTO>()
                 .ForMember(dest => dest.CardExpiryDate, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.CardExpiryDate == null)
                        return string.Empty;

                    return src.CardExpiryDate.ToString("MM/yyyy");
                }))
                .ForMember(dest => dest.CVVNumber, opt => opt.UseValue<string>("****"));


            CreateMap<CreateCustomerCreditCardDTO, CustomerPaymentCards>()
                 .ForMember(dest => dest.CardExpiryDate, opt => opt.ResolveUsing(src =>
                 {
                     if (src == null || string.IsNullOrWhiteSpace(src.CardExpiryDate))
                         return DateTime.MinValue;

                     DateTime expDt = DateTime.MinValue;
                     DateTime.TryParseExact("28/" + src.CardExpiryDate, "dd/M/yyyy", null, System.Globalization.DateTimeStyles.None, out expDt);

                     return expDt;
                 }));

            CreateMap<ModifyCustomerCreditCardDTO, CustomerPaymentCards>();


            //CreateMap<CreateCustomerPaypalDTO, PaypalDetail>();
            //CreateMap<CustomerPaypalDetailsDTO, PaypalDetail>().ReverseMap();
        }
    }



    public abstract class CustomerCreditCardBaseDTO
    {
        public int CustomerId { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public System.DateTime CardExpiryDate { get; set; }
        public string CVVNumber { get; set; }
        public bool IsPrimary { get; set; }
        public string CardLabel { get; set; }
        public string CardLabelColor { get; set; }
        public Nullable<bool> SpendingLimitEnabled { get; set; }
        public string Currency { get; set; }
        public Nullable<int> Duration { get; set; }
        //public string Duration_Type { get; set; }

        public int SpendingLimitAmount { get; set; }

    }



    public class CustomerCreditCardDTO
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public string CardExpiryDate { get; set; }
        public string CVVNumber { get; set; }
        public bool IsPrimary { get; set; }
        public string CardLabel { get; set; }
        public string CardLabelColor { get; set; }
        public Nullable<bool> SpendingLimitEnabled { get; set; }
        public int SpendingLimitAmount { get; set; }
        public string Currency { get; set; }
        public Nullable<int> Duration { get; set; }
        public string CardType { get; set; }

    }


    public class CreateCustomerCreditCardDTO
    {
        [Required(ErrorMessageResourceName = "Rqd_CardHolderName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(ErrorMessageResourceName = "Max_CardHolderName", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression("^[a-zA-Z][a-zA-Z\\s]+$", ErrorMessageResourceName = "Invalid_CardHolderName", ErrorMessageResourceType = typeof(Resources))]
        public string CardHolderName { get; set; }


        [Required(ErrorMessageResourceName = "Rqd_CardNumber", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(ErrorMessageResourceName = "Max_CardNumber", ErrorMessageResourceType = typeof(Resources))]
        public string CardNumber { get; set; }


        [Required(ErrorMessageResourceName = "Rqd_CardExpiryDate", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(ErrorMessageResourceName = "Max_CardExpiryDate", ErrorMessageResourceType = typeof(Resources))]
        public string CardExpiryDate { get; set; }


        [Required(ErrorMessageResourceName = "Rqd_CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(4, ErrorMessageResourceName = "Max_CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        public string CVVNumber { get; set; }

        public bool IsPrimary { get; set; }
        public string CardLabel { get; set; }
        public string CardLabelColor { get; set; }
        public Nullable<bool> SpendingLimitEnabled { get; set; }
        public int SpendingLimitAmount { get; set; }
        public string Currency { get; set; }
        public Nullable<int> Duration { get; set; }

    }



    public class ModifyCustomerCreditCardDTO
    {

        public int Id { get; set; }
        public bool IsPrimary { get; set; }
        public string CardLabel { get; set; }
        public string CardLabelColor { get; set; }
        public Nullable<bool> SpendingLimitEnabled { get; set; }
        public Nullable<int> SpendingLimitAmount { get; set; }
        public string Currency { get; set; }
        public Nullable<int> Duration { get; set; }


    }


    public class CreateCustomerPaypalDTO
    {


        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        public string PaypalEmail { get; set; }


        [Required(ErrorMessageResourceName = "Rqd_Password", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }


    public class CustomerPaypalDetailsDTO
    {
        public string PaypalEmail { get; set; }
    }

    public class CreateRestaurantPaypalPayDTO
    {
        [Required]
        public string PaypalEmailId { get; set; }
        public int OrderId { get; set; }
        [Required]
        public int PaypalCardId { get; set; }

    }
}