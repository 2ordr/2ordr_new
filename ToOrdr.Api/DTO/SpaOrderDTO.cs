﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.DTO
{
    public class SpaOrderProfile : AutoMapper.Profile
    {
        public SpaOrderProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<SpaOrder, SpaOrderDTO>()
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.SpaOrderItems.Any())
                        return 0;

                    return src.SpaOrderItems.Count();

                }))
                 .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
                 {
                     return src.CreationDate;
                 }))
                 .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                 {
                     double? baseCurrencyRate = 0;
                     if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                         baseCurrencyRate = src.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                     if (baseCurrencyRate > 0)
                     {
                         return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.OrderTotal) * baseCurrencyRate), 2);
                     }
                     return 0;
                 }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                }))
                 .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Hotel.Currency != null)
                     {
                         return src.Hotel.Currency.Symbol;
                     }
                     return null;
                 })).ReverseMap();
            CreateMap<SpaOrder, SpaOrderInfoDTO>()
               .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>src.Customer.Currency.Symbol))
               .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
               {
                   if (src == null || !src.SpaOrderItems.Any())
                       return 0; ;

                   return src.SpaOrderItems.Count();

               }))
               .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
               {
                   return src.CreationDate;
               })).ReverseMap();

            CreateMap<SpaOrder, SpaOrderInfoPdfDTO>()
            .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
            {
                return src.CreationDate;
            })).ReverseMap();

            CreateMap<SpaOrderItem, SpaOrderItemsDTO>()
                .ForMember(dest => dest.SpaServiceDetailName, opt => opt.ResolveUsing(src =>
            {
                if (src == null || src.SpaServiceDetail == null)
                    return string.Empty;
                return src.SpaServiceDetail.Name;
            }))
                .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.SpaServiceDetail == null)
                        return 0;
                    return src.SpaServiceDetail.Price;
                }))
                .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.SpaOrder.Customer.CurrencyId != null && src.SpaOrder.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.SpaOrder.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaOrder.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.SpaServiceDetail.Price) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                 .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                 {
                     if (src.SpaServiceDetail.SpaServiceDetailImages.Count() == 0)
                         return null;
                     return api_path + "Images/Services/SpaService/SpaServiceDetails/" + src.SpaServiceDetailId.ToString() + "/" + src.SpaServiceDetail.SpaServiceDetailImages.FirstOrDefault(i => i.IsPrimary == true).Image;
                 }))
                .ReverseMap();


            CreateMap<SpaOrderItemsAdditional, SpaOrderItemsAdditionalDTO>()
                .ForMember(dest => dest.Spa_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaAdditionalElement == null)
                        return null;
                    return src.SpaAdditionalElement.SpaAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Spa_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaAdditionalElement == null)
                        return 0;
                    return src.SpaAdditionalElement.SpaAdditionalGroupId;
                }))
                .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.SpaOrderItem.SpaOrder.Customer.CurrencyId != null && src.SpaOrderItem.SpaOrder.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.SpaOrderItem.SpaOrder.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaOrderItem.SpaOrder.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Total) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ReverseMap();

            CreateMap<SpaOrderExtraProcedure, SpaOrderExtraProcedureDTO>().ReverseMap();
        }
    }
    public class SpaOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual ICollection<SpaOrderItemsDTO> SpaOrderItems { get; set; }
    }
    public class SpaOrderInfoDTO
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public Nullable<double> preferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<SpaOrderItemsDTO> SpaOrderItems { get; set; }
    }

    public class SpaOrderInfoPdfDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual HotelPdfDTO Hotel { get; set; }
        public virtual ICollection<SpaOrderItemsDTO> SpaOrderItems { get; set; }
    }

    public class SpaOrderItemsDTO
    {
        public int Id { get; set; }
        public int SpaOrderId { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string SpaServiceDetailName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public int Qty { get; set; }
        public int ServeLevel { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public string Image { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public System.DateTime EndDateTime { get; set; }
        public int SpaEmployeeDetailsId { get; set; }
        public virtual SpaEmployeeDetailsDTO SpaEmployeeDetail { get; set; }
        public Nullable<int> ExtraTimeId { get; set; }
        public virtual ExtraTimeDTO ExtraTime { get; set; }
        public virtual ICollection<SpaOrderItemsAdditionalDTO> SpaOrderItemsAdditionals { get; set; }
        public virtual ICollection<SpaOrderExtraProcedureDTO> SpaOrderExtraProcedures { get; set; }
    }

    public class SpaOrderItemsAdditionalDTO
    {
        public int Spa_Additional_Group_ID { get; set; }
        public string Spa_Additional_Group_Name { get; set; }
        public int SpaOrderItemsId { get; set; }
        public int SpaAdditionalElementId { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public virtual SpaAdditionalElementInfoDTO SpaAdditionalElement { get; set; }

    }

    public class SpaOrderExtraProcedureDTO
    {
        public int Id { get; set; }
        public int ExtraProcedureId { get; set; }
        public virtual ExtraProcedureDTO ExtraProcedure { get; set; }
    }
}