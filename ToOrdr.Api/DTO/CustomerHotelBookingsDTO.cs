﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{

    public class CreateHotelBookingDTO
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_RoomId", ErrorMessageResourceType = typeof(Resources))]
        public int RoomId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CreditCardId", ErrorMessageResourceType = typeof(Resources))]
        public int CreditCardId { get; set; }
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
    }

    public class ModifyHotelBookingDTO
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_BookingId", ErrorMessageResourceType = typeof(Resources))]
        public int Id { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_RoomId", ErrorMessageResourceType = typeof(Resources))]
        public int RoomId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CreditCardId", ErrorMessageResourceType = typeof(Resources))]
        public int CreditCardId { get; set; }
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
    }

    public class HotelBookingsDTO
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int CreditCardId { get; set; }
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
        public int StatusId { get; set; }
        public double Total { get; set; }
        public virtual CustomerDTO Customer { get; set; }

    }

    public class CustomerHotelBookingsDTO
    {
        public int Id { get; set; }
        public int CreditCardId { get; set; }
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
        public int StatusId { get; set; }
        public virtual HotelRoomsDTO Room { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }

    }
}