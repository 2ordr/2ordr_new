﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class LaundryOrderReviewDTO
    {
        public int Id { get; set; }
        public int LaundryOrderId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateLaundryOrderReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_LaundryOrderId", ErrorMessageResourceType = typeof(Resources))]
        public int LaundryOrderId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }
}