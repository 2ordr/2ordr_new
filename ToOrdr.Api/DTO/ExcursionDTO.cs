﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class HotelExcursionProfile : AutoMapper.Profile
    {
        public HotelExcursionProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<Excursion, ExcursionDTO>().ReverseMap();
            CreateMap<HotelExcursion, HotelExcursionDTO>().ReverseMap();
            CreateMap<ExcursionDetail, ExcursionDetailsDTO>()
                .ForMember(dest => dest.ExcursionDetailImages, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExcursionDetailImages.Any())
                        return src.ExcursionDetailImages.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.HotelExcursion.HotelServices.Hotel.Currency != null)
                    {
                        return src.HotelExcursion.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ForMember(dest => dest.WeekDay, opt => opt.ResolveUsing(src =>
                {
                    if (src.WeekDay > 0 && src.WeekDay < 8)
                        return ((WeekDays)src.WeekDay).ToString();
                    return null;
                })).ReverseMap();
            CreateMap<ExcursionDetail, FavoriteExcursionDetailsDTO>()
            .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.HotelExcursion.HotelServices.Hotel))
            .ForMember(dest => dest.ExcursionDetailImages, opt => opt.ResolveUsing(src =>
            {
                if (src.ExcursionDetailImages.Any())
                    return src.ExcursionDetailImages.Where(sg => sg.IsActive);
                else
                    return null;

            }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.HotelExcursion.HotelServices.Hotel.Currency != null)
                    {
                        return src.HotelExcursion.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
            .ForMember(dest => dest.WeekDay, opt => opt.ResolveUsing(src =>
            {
                if (src.WeekDay > 0 && src.WeekDay < 8)
                    return ((WeekDays)src.WeekDay).ToString();
                return null;
            })).ReverseMap();

            CreateMap<ExcursionDetail, ExcursionDetailsWithFullInfoDTO>()
                .ForMember(dest => dest.ExcursionDetailImages, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExcursionDetailImages.Any())
                        return src.ExcursionDetailImages.Where(sg => sg.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.ExcursionReviews, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExcursionReviews.Any())
                        return src.ExcursionReviews;
                    else
                        return null;

                }))
                .ForMember(dest => dest.ExcursionOffers, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExcursionOffers.Any())
                        return src.ExcursionOffers.Where(eo => eo.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.ExcursionSuggestions, opt => opt.ResolveUsing(src =>
                {
                    if (src.ExcursionSuggestions.Any())
                        return src.ExcursionSuggestions.Where(es => es.ExcursionDetail1.IsActive);
                    else
                        return null;

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.HotelExcursion.HotelServices.Hotel.Currency != null)
                    {
                        return src.HotelExcursion.HotelServices.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ForMember(dest => dest.WeekDay, opt => opt.ResolveUsing(src =>
                {
                    if (src.WeekDay > 0 && src.WeekDay < 8)
                        return ((WeekDays)src.WeekDay).ToString();
                    return null;
                }))
                 .ForMember(dest => dest.EndTime, opt => opt.ResolveUsing(src =>
                 {
                     if (src.DurationInDays == 0 && src.DurationHours != null)
                         return src.StartTime + src.DurationHours;
                     else if (src.DurationInDays != 0 && src.DurationHours == null)
                         return new TimeSpan(23, 00, 00);
                     else
                         return src.DurationHours;
                 }))
                .ReverseMap();
            CreateMap<ExcursionSuggestion, ExcursionSuggestionDTO>().ReverseMap();
            CreateMap<ExcursionDetailImages, ExcursionDetailImagesDTO>()
               .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
               {
                   if (src.Image != null)
                       return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailId.ToString() + "/" + src.Image;
                   else
                       return null;
               })).ReverseMap();

            CreateMap<ExcursionDetailGallery, ExcursionDetailGalleryDTO>()
               .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
               {
                   if (src.Image != null)
                       return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailId.ToString() + "/Gallery/" + src.Id + "/" + src.Image;
                   else
                       return null;
               }))
                .ForMember(dest => dest.Video, opt => opt.ResolveUsing(src =>
                {
                    if (src.Video != null)
                        return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailId.ToString() + "/Gallery/" + src.Id + "/" + src.Video;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<ExcursionIngredient, ExcursionIngredientPreferenceDTO>()
               .ForMember(dest => dest.IngredientImage, opt => opt.ResolveUsing(src =>
               {
                   if (!string.IsNullOrWhiteSpace(src.IngredientImage))
                       return api_path + "/Images/ExcursionIngredients/" + src.Id.ToString() + "/" + src.IngredientImage;
                   else
                       return null;
               })).ReverseMap();

            CreateMap<ExcursionIngredientCategory, ExcursionIngredientCategoriesPrefDTO>()
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (!string.IsNullOrWhiteSpace(src.Image))
                        return api_path + "/Images/ExcursionIngredientCategories/" + src.Id.ToString() + "/" + src.Image;
                    else
                        return null;
                })).ReverseMap();

            CreateMap<CustomerExcursionPreferences, ModifyExcursionIngredientPreferenceDTO>().ReverseMap();

            CreateMap<ExcursionReview, CreateExcursionReviewDTO>().ReverseMap();
            CreateMap<ExcursionReview, ExcursionReviewsDTO>().ReverseMap();
            CreateMap<ExcursionOffer, ExcursionOffersDTO>()
                 .ForMember(dest => dest.OfferDescription, opt => opt.ResolveUsing(src => src.Description))
                  .ForMember(dest => dest.Name, opt => opt.ResolveUsing(src =>
                  {
                      if (src.ExcursionDetail != null)
                      {
                          return src.ExcursionDetail.Name;
                      }
                      return null;
                  }))
                   .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                   {
                       if (src.ExcursionDetail.HotelExcursion.HotelServices.Hotel.Currency != null)
                       {
                           return src.ExcursionDetail.HotelExcursion.HotelServices.Hotel.Currency.Symbol;
                       }
                       return null;
                   }))
                    .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                    {
                        if (src.ExcursionDetail != null)
                        {
                            return src.ExcursionDetail.Price;
                        }
                        return 0;
                    }))
                     .ForMember(dest => dest.DiscountPrice, opt => opt.ResolveUsing(src =>
                     {
                         if (src.Percentage != null)
                         {
                             var offerAmnt = (src.ExcursionDetail.Price * src.Percentage) / 100;
                             offerAmnt = System.Math.Round(offerAmnt, 2);
                             return src.ExcursionDetail.Price - offerAmnt;
                         }
                         return 0;
                     }))
                    .ForMember(dest => dest.OfferImage, opt => opt.ResolveUsing(src =>
                    {
                        if (src.OfferImage != null)
                            return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailId.ToString() + "/OfferImage/" + src.Id.ToString() + "/" + src.OfferImage;
                        else
                            return null;
                    }))
                .ReverseMap();

            CreateMap<ExcursionOffer, ExcursionDetailOfferDTO>()
            .ForMember(dest => dest.DiscountPrice, opt => opt.ResolveUsing(src =>
            {
                if (src.Percentage != null)
                {
                    var offerAmnt = (src.ExcursionDetail.Price * src.Percentage) / 100;
                    offerAmnt = System.Math.Round(offerAmnt, 2);
                    return src.ExcursionDetail.Price - offerAmnt;
                }
                return 0;
            })).ReverseMap();


            CreateMap<ExcursionCart, ExcursionCartDTO>()
           .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
           {
               if (src == null || !src.ExcursionCartItems.Any())
                   return 0; ;

               return src.ExcursionCartItems.Count();

           }))
           .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
           {
               if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                   return src.Customer.Currency.Symbol;
               return null;
           }))
            .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
           {
               if (src.Hotel.Currency != null)
               {
                   return src.Hotel.Currency.Symbol;
               }
               return null;
           })).ReverseMap();

            CreateMap<ExcursionCart, ExcursionCartInfoDTO>()
          .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
          {
              if (src == null || !src.ExcursionCartItems.Any())
                  return 0; ;

              return src.ExcursionCartItems.Count();

          }))
          .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
          {
              if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                  return src.Customer.Currency.Symbol;
              return null;
          })).ReverseMap();

            CreateMap<ExcursionCartItems, ExcursionCartItemsDTO>()
              .ForMember(dest => dest.ExcursionDetailName, opt => opt.ResolveUsing(src =>
              {
                  if (src == null || src.ExcursionDetails == null)
                      return string.Empty;
                  return src.ExcursionDetails.Name;
              }))
              .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
              {
                  if (src == null || src.ExcursionDetails == null)
                      return 0;
                  return src.ExcursionDetails.Price;
              }))
             .ForMember(dest => dest.ImagePath, opt => opt.ResolveUsing(src =>
             {
                 if (src.ExcursionDetails.ExcursionDetailImages.Count() == 0)
                     return null;
                 return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailsId.ToString() + "/" + src.ExcursionDetails.ExcursionDetailImages.FirstOrDefault(i => i.IsPrimary == true).Image;
             }))
            .ReverseMap();

            CreateMap<CreateExcursionCartDTO, ExcursionCart>().ReverseMap();
            CreateMap<CreateExcursionCartItemDTO, ExcursionCartItems>().ReverseMap();

            CreateMap<UpdateExcursionCartDTO, ExcursionCart>().ReverseMap();
            CreateMap<UpdateExcursionCartItemDTO, ExcursionCartItems>().ReverseMap();

            CreateMap<ExcursionOrder, ExcursionOrderDTO>()
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.ExcursionOrderItems.Any())
                        return 0;

                    return src.ExcursionOrderItems.Count();

                }))
                 .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
                 {
                     return src.CreationDate;
                 }))
                .ForMember(dest => dest.Total, opt => opt.ResolveUsing(src => src.OrderTotal))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                }))
                 .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Hotel.Currency != null)
                     {
                         return src.Hotel.Currency.Symbol;
                     }
                     return null;
                 })).ReverseMap();

            CreateMap<ExcursionOrder, ExcursionOrderInfoDTO>()
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.ExcursionOrderItems.Any())
                        return 0;

                    return src.ExcursionOrderItems.Count();

                }))
                 .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
                 {
                     return src.CreationDate;
                 }))
                .ForMember(dest => dest.Total, opt => opt.ResolveUsing(src => src.OrderTotal))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                })).ReverseMap();

            CreateMap<ExcursionOrder, ExcursionOrderInfoPdfDTO>()
                 .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src =>
                 {
                     return src.CreationDate;
                 }))
                .ForMember(dest => dest.Total, opt => opt.ResolveUsing(src => src.OrderTotal))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                })).ReverseMap();

            CreateMap<ExcursionOrderItems, ExcursionOrderItemsDTO>()
                .ForMember(dest => dest.ExcursionDetailName, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.ExcursionDetails == null)
                        return string.Empty;
                    return src.ExcursionDetails.Name;
                }))
                .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.ExcursionDetails == null)
                        return 0;
                    return src.ExcursionDetails.Price;
                }))
                 .ForMember(dest => dest.ImagePath, opt => opt.ResolveUsing(src =>
                 {
                     if (src.ExcursionDetails.ExcursionDetailImages.Count() == 0)
                         return null;
                     return api_path + "Images/Services/Excursion/ExcursionDetails/" + src.ExcursionDetailsId.ToString() + "/" + src.ExcursionDetails.ExcursionDetailImages.FirstOrDefault(i => i.IsPrimary == true).Image;
                 }))
                .ReverseMap();

            CreateMap<ExcursionOrderReview, ExcursionOrderReviewDTO>()
                .ForMember(src => src.Customer, opt => opt.ResolveUsing(src => { return src.ExcursionOrder.Customer; }))
                .ForMember(src => src.CustomerId, opt => opt.ResolveUsing(src => { return src.ExcursionOrder.CustomerId; })).ReverseMap();
            CreateMap<ExcursionOrderReview, CreateExcursionOrderReviewDTO>().ReverseMap();

            CreateMap<CustomerActiveExcursionOrdersAndBaskets, CustomerActiveExcursionOrdersAndBasketsDTO>().ReverseMap();
        }
    }
    public class ExcursionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
    public class ExcursionReviewsDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ExcursionDetailsId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateExcursionReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_ExcursionDetailId", ErrorMessageResourceType = typeof(Resources))]
        public int ExcursionDetailsId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }

    public class ExcursionOffersDTO
    {
        public int Id { get; set; }
        public int ExcursionDetailId { get; set; }
        public string Name { get; set; }
        public string OfferDescription { get; set; }
        public double Percentage { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public double DiscountPrice { get; set; }
        public double DiscountPriceInPreferredCurrency { get; set; }
        public string OfferImage { get; set; }
        public bool IsFavourite { get; set; }
    }

    public class ExcursionDetailOfferDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public double DiscountPrice { get; set; }
        public double DiscountPriceInPreferredCurrency { get; set; }
    }

    public class HotelExcursionDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public bool IsActive { get; set; }
        public virtual ExcursionDTO Excursion { get; set; }
    }

    public class ExcursionDetailsDTO
    {
        public int Id { get; set; }
        public int HotelExcursionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int DurationInDays { get; set; }
        public Nullable<System.TimeSpan> DurationHours { get; set; }
        public string WeekDay { get; set; }
        public System.TimeSpan StartTime { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsFavourite { get; set; }
        public virtual ICollection<ExcursionDetailImagesDTO> ExcursionDetailImages { get; set; }
    }

    public class FavoriteExcursionDetailsDTO
    {
        public int Id { get; set; }
        public int HotelExcursionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int DurationInDays { get; set; }
        public Nullable<System.TimeSpan> DurationHours { get; set; }
        public string WeekDay { get; set; }
        public System.TimeSpan StartTime { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int TotalOrders { get; set; }
        public Nullable<DateTime> LastOrder { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<ExcursionDetailImagesDTO> ExcursionDetailImages { get; set; }
    }

    public class ExcursionDetailsWithFullInfoDTO
    {
        public int Id { get; set; }
        public int HotelExcursionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int DurationInDays { get; set; }
        public Nullable<System.TimeSpan> DurationHours { get; set; }
        public string WeekDay { get; set; }
        public System.TimeSpan StartTime { get; set; }
        public System.TimeSpan EndTime { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsFavourite { get; set; }
        public double CustomerRating { get; set; }
        public virtual ICollection<ExcursionDetailImagesDTO> ExcursionDetailImages { get; set; }
        public virtual ICollection<ExcursionDetailGalleryDTO> ExcursionDetailGallery { get; set; }
        public virtual ICollection<ExcursionDetailOfferDTO> ExcursionOffers { get; set; }
        public virtual ICollection<ExcursionSuggestionDTO> ExcursionSuggestions { get; set; }
        public virtual ICollection<ExcursionReviewsDTO> ExcursionReviews { get; set; }

    }
    public class ExcursionSuggestionDTO
    {
        public virtual ExcursionDetailsDTO ExcursionDetail1 { get; set; }
    }

    public class ExcursionDetailImagesDTO
    {
        public int Id { get; set; }
        public int ExcursionDetailId { get; set; }
        public string Image { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }

    public partial class ExcursionDetailGalleryDTO
    {
        public int Id { get; set; }
        public int ExcursionDetailId { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
        public bool IsActive { get; set; }
    }

    public class ExcursionIngredientPreferenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IngredientImage { get; set; }
        public Nullable<bool> Include { get; set; }
    }
    public class ExcursionIngredientCategoriesPrefDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<ExcursionIngredientPreferenceDTO> ExcursionIngredients { get; set; }
    }
    public class ModifyExcursionIngredientPreferenceDTO
    {
        [Required(ErrorMessageResourceName = "Rqd_IngredientId", ErrorMessageResourceType = typeof(Resources))]
        public int ExcursionIngredientId { get; set; }
        public Nullable<bool> Include { get; set; }
    }

    public class ExcursionCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<ExcursionCartItemsDTO> ExcursionCartItems { get; set; }
    }

    public class ExcursionCartInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<ExcursionCartItemsDTO> ExcursionCartItems { get; set; }
    }
    public class ExcursionCartItemsDTO
    {
        public int Id { get; set; }
        public int ExcursionDetailsId { get; set; }
        public string ExcursionDetailName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public double OfferPriceInPreferredCurrency { get; set; }
        public System.DateTime ScheduleDate { get; set; }
        public string Comment { get; set; }
        public int NoOfPerson { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string ImagePath { get; set; }
    }

    public class CreateExcursionCartDTO
    {
        public ICollection<CreateExcursionCartItemDTO> ExcursionCartItems { get; set; }
    }

    public class CreateExcursionCartItemDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Excursion detail Id shouldn't be zero or negative")]
        public int ExcursionDetailsId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "NoOfPerson shouldn't be zero or negative")]
        public Nullable<int> NoOfPerson { get; set; }

        [ValidScheduledDateTime()]
        [Required(ErrorMessage = "SCHEDULEDATE_MUST_BE_REQUIRED")]
        public System.DateTime ScheduleDate { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

    }

    public class UpdateExcursionCartDTO
    {
        public ICollection<UpdateExcursionCartItemDTO> ExcursionCartItems { get; set; }
    }

    public class UpdateExcursionCartItemDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id shouldn't be Zero or Negative")]
        public Nullable<int> Id { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Excursion detail Id shouldn't be zero or negative")]
        public int ExcursionDetailsId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "NoOfPerson shouldn't be zero or negative")]
        public Nullable<int> NoOfPerson { get; set; }

        [ValidScheduledDateTime()]
        [Required(ErrorMessage = "SCHEDULEDATE_MUST_BE_REQUIRED")]
        public System.DateTime ScheduleDate { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

    }

    public class ExcursionOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> Total { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Order_Item_Count { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual ICollection<ExcursionOrderItemsDTO> ExcursionOrderItems { get; set; }
    }

    public class ExcursionOrderInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Order_Item_Count { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<ExcursionOrderItemsDTO> ExcursionOrderItems { get; set; }
    }

    public class ExcursionOrderInfoPdfDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public virtual HotelPdfDTO Hotel { get; set; }
        public virtual ICollection<ExcursionOrderItemsDTO> ExcursionOrderItems { get; set; }
    }
    public class ExcursionOrderItemsDTO
    {
        public int Id { get; set; }
        public int ExcursionDetailsId { get; set; }
        public string ExcursionDetailName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public double OfferPriceInPreferredCurrency { get; set; }
        public System.DateTime ScheduleDate { get; set; }
        public string Comment { get; set; }
        public int NoOfPerson { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string ImagePath { get; set; }
    }

    public class ExcursionOrderReviewDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ExcursionOrderId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateExcursionOrderReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_ExcursionOrderId", ErrorMessageResourceType = typeof(Resources))]
        public int ExcursionOrderId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}