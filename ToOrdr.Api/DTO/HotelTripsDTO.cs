﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class HotelTripsDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public double Days { get; set; }
        public double Rate { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public System.DateTime TripDate { get; set; }
    }
    public class HotelTripDetailsDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public double Days { get; set; }
        public double Rate { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public System.DateTime TripDate { get; set; }
        public virtual ICollection<TripDetailsDTO> TripDetails { get; set; }
    }
    public class TripDetailsDTO
    {
        public int Id { get; set; }
        public int TripID { get; set; }
        public string Place { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class TripBookingDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TripId { get; set; }
        public int Seats { get; set; }
        public int StatusId { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<double> Amount { get; set; }
        public virtual HotelTripDetailsDTO Trip { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }
    }

    public class CreateTripBookingDTO
    {
        public int TripId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Invalid_SeatNumber", ErrorMessageResourceType = typeof(Resources))]
        public int Seats { get; set; }
    }
    public class UpdateTripBookingDTO
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Invalid_SeatNumber", ErrorMessageResourceType = typeof(Resources))]
        public int Seats { get; set; }
    }

}