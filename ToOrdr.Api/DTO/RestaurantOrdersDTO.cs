﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace ToOrdr.Api.DTO
{
    public class RestaurantOrdersBaseDTO
    {

        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public int CustomerTableNumber { get; set; }

    }

    public class CreateRestaurantOrdersDTO : RestaurantOrdersBaseDTO
    {
        public virtual ICollection<CreateRestaurantOrderItemsDTO> Customer_RestaurantOrder_Items { get; set; }
    }

    public class RestaurantOrdersDTO : RestaurantOrdersBaseDTO
    {
        public int Id { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual ICollection<CustomerRestaurantOrderItemsDTO> Customer_RestaurantOrder_Items { get; set; }
    }

    public class CustomerMonthlyPaymentsDTO
    {
        public int Month { get; set; }
        public double TotalAmount { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<CustRestaurantOrdersDTO> RestaurantOrder { get; set; }
    }
    public class CustomerMonthlyPaymentHistoryDTO
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
    }

    public class CustRestaurantOrdersDTO
    {
        public int Id { get; set; }
        public System.DateTime CreationDate { get; set; }
        public double OrderTotal { get; set; }
        public double PreferredCurrencyOrderTotal { get; set; }        
        public string Comment { get; set; }
        public RestaurantDetailsDTO Restaurant { get; set; }
    }

    public class CustomerYearlyPaymentsDTO
    {
        public int Year { get; set; }
        public double TotalAmount { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<CustRestaurantOrdersDTO> RestaurantOrder { get; set; }
    }

}