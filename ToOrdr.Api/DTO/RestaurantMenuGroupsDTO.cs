﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class RestaurantMenuGroupsBaseDTO
    {
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
    }


    public class RestaurantMenuGroupsDTO : RestaurantMenuGroupsBaseDTO
    {
        public int Id { get; set; }
        public virtual ICollection<RestaurantMenuItemsDTO> RestaurantMenuItems { get; set; }

    }
    public class RestMenuGroupsDTO
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public bool MyPreference { get; set; }
        public virtual ICollection<RestMenuItemsDTO> RestaurantMenuItems { get; set; }
    }

    public class RestaurantMenuGroupsDTO4MenuItem : RestaurantMenuGroupsBaseDTO
    {
        public int Id { get; set; }

    }


    public class Others
    {

        public int RestaurantMenuId { get; set; }


        public Nullable<bool> IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }

        //public virtual ICollection<Menu_Group_Options> Menu_Group_Options { get; set; }

        //public virtual RestaurantMenu RestaurantMenus { get; set; }
    }

}