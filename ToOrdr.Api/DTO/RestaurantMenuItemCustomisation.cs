﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class RestaurantMenuItemCustomisation
    {
        public int RestaurantMenuItems_Customisation_ID { get; set; }
        public string RestaurantMenuItems_Customisation_Name { get; set; }
        public bool RestaurantMenuItems_Customisation_Valid { get; set; }
        public System.DateTime Creation_Date { get; set; }
    }
}