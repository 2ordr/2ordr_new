﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class CustomerSpaIngredientPreferenceDTO
    {

    }
    public class SpaIngredientPreferenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IngredientImage { get; set; }
        public Nullable<bool> Include { get; set; }
    }
    public class SpaIngredientCategoriesPrefDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaIngredientPreferenceDTO> SpaIngredients { get; set; }
    }
    public class ModifySpaIngredientPreferenceDTO
    {
        [Required(ErrorMessageResourceName = "Rqd_IngredientId", ErrorMessageResourceType = typeof(Resources))]
        public int SpaIngredientId { get; set; }
        public Nullable<bool> Include { get; set; }
    }
}