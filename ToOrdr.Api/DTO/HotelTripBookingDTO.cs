﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.DTO
{
    public abstract class HotelTripBookingBaseDTO
    {

        public int Hotel_Trip_ID { get; set; }
        public int Id { get; set; }
        public System.DateTime Booking_Date_Time { get; set; }
        public int Number_Of_Seats { get; set; }
        public bool Booking_Status { get; set; }
    }

    public class CreateHotelTripBookingDTO : HotelTripBookingBaseDTO
    {

    }
    public class HotelTripBookingDTO : HotelTripBookingBaseDTO
    {
        public int Hotel_Trip_Booking_ID { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerDTO Customer { get; set; }
        public virtual HotelTripsDTO Hotel_Trip { get; set; }
    }
}