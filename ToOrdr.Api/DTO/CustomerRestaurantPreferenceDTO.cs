﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{


    public class RestaurantPreferenceDTO
    {
        public RestaurantPreferenceDTO()
        {
            Groups = new List<RestaurantPreferenceGroupDTO>();
        }
        public int Id { get; set; }
        public List<RestaurantPreferenceGroupDTO> Groups { get; set; }

    }


    public class RestaurantPreferenceGroupDTO
    {
        public RestaurantPreferenceGroupDTO()
        {
            SubGroups = new List<RestaurantPreferenceSubGroupDTO>();
        }

        public string GroupName { get; set; }
        public string Description { get; set; }

        public List<RestaurantPreferenceSubGroupDTO> SubGroups { get; set; }
    }

    public class RestaurantPreferenceSubGroupDTO
    {
        public int SubGroupId { get; set; }

        public RestaurantPreferenceSubGroupDTO()
        {
            PreferenceItems = new List<RestaurantPreferenceItemDTO>();
        }

        public string SubGroupName { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public List<RestaurantPreferenceItemDTO> PreferenceItems { get; set; }
    }



    public class RestaurantPreferenceItemDTO
    {
        public string Id { get; set; }
        public string ItemName { get; set; }
        public object ItemValue { get; set; }
        public string ItemImage { get; set; }
    }


    public class CustomerIngredientPreferenceDTO
    {
        public int Ingredient_Preference_Id { get; set; }
        public int Id { get; set; }
        public int Ingredient_Id { get; set; }
        public Nullable<bool> Include { get; set; }
    }


    public abstract class CustomerRestaurantPreferenceBaseDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuItems_Customisation_ID { get; set; }
    }


    public class CreateRestaurantPreferenceDTO : CustomerRestaurantPreferenceBaseDTO { }

    public class ModifyRestaurantPreferenceDTO
    {
        public ICollection<PreferenceDTO> CustomerPreference { get; set; }
    }

    public class CustomerRestaurantPreferenceDTO : CustomerRestaurantPreferenceBaseDTO
    {
        public int Customer_Restaurant_Preference_ID { get; set; }
        public bool Is_Valid { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerDTO Customer { get; set; }
        public virtual RestaurantMenuItemCustomisation RestaurantMenuItems_Customisation { get; set; }
    }

    public class PreferenceDTO
    {
        [Required(ErrorMessageResourceName = "Id_Reqd", ErrorMessageResourceType = typeof(Resources))]
        public string Id { get; set; }
        public bool? ItemValue { get; set; }
    }
}