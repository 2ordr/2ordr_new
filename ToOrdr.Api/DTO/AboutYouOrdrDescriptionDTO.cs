﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class AboutYouOrdrDescriptionDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public virtual LanguageDTO Language { get; set; }
    }
}