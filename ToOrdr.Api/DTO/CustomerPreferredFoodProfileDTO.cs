﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class CustomerPreferredFoodProfileDTO
    {
        public int Id { get; set; }
        public virtual FoodProfileDTO FoodProfile { get; set; }

    }
}