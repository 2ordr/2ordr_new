﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class CustomerRestaurantOrderItemsBaseDTO
    {

        public int Customer_Id { get; set; }
        public int RestaurantMenuItemId { get; set; }

    }


    public class CreateRestaurantOrderItemsDTO : CustomerRestaurantOrderItemsBaseDTO { }


    public class CustomerRestaurantOrderItemsDTO : CustomerRestaurantOrderItemsBaseDTO
    {
        public int Customer_RestaurantOrder_Item_ID { get; set; }
        public System.DateTime CreationDate { get; set; }
    }


}