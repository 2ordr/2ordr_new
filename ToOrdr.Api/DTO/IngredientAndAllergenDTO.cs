﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class IngredientAndAllergenDTO
    {
    }

    public class AllergenDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AllergenImage { get; set; }
    }
    public class AllergenCategoriesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual ICollection<AllergenDTO> Allergens { get; set; }
    }
    public class IngredientDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IngredientImage { get; set; }
    }
    public class IngredientCategoriesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual ICollection<IngredientDTO> Ingredients { get; set; }
    }

    public class FoodProfileDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
    }

    public class IngredientPreferenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IngredientImage { get; set; }
        public Nullable<bool> Include { get; set; }
    }
    public class IngredientCategoriesPrefDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual ICollection<IngredientPreferenceDTO> Ingredients { get; set; }
    }
    public class ModifyIngredientPreferenceDTO
    {
        [Required(ErrorMessageResourceName = "Rqd_IngredientId", ErrorMessageResourceType = typeof(Resources))]
        public int IngredientId { get; set; }
        public Nullable<bool> Include { get; set; }
    }

    public class AllergenPreferenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AllergenImage { get; set; }
        public bool Include { get; set; }
    }
    public class AllergenCategoriesPrefDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual ICollection<AllergenPreferenceDTO> Allergens { get; set; }
    }
    public class ModifyAllergenPreferenceDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_AllergenId", ErrorMessageResourceType = typeof(Resources))]
        public int AllergenId { get; set; }
        public bool Include { get; set; }
    }

    public class DrinkGroupsDTO
    {
        public string DrinkType { get; set; }

        public virtual ICollection<IngredientCategoriesPrefDTO> IngredientCategory { get; set; }
    }
}