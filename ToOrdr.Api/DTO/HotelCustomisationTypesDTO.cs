﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class HotelCustomisationTypesDTO
    {
        public int Hotel_Customisation_Type_ID { get; set; }
        public string Hotel_Customisation_Type_Description { get; set; }
        public bool Hotel_Customisation_Type_Valid { get; set; }
        public System.DateTime CreationDate { get; set; }
    }
}