﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class HousekeepingCartProfile : AutoMapper.Profile
    {
        public HousekeepingCartProfile()
        {
            CreateMap<HousekeepingCart, HousekeepingCartDTO>()
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.HousekeepingCartItems.Any())
                        return 0;

                    return src.HousekeepingCartItems.Count();

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Room.Hotel.Currency != null)
                    {
                        return src.Room.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ReverseMap();

            CreateMap<HousekeepingCart, CustomerActiveHousekeepingCartDTO>()
            .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.Room.Hotel))
            .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
            {
                if (src == null || !src.HousekeepingCartItems.Any())
                    return 0;

                return src.HousekeepingCartItems.Count();

            }))
            .ReverseMap();

            CreateMap<HousekeepingCartItem, HousekeepingCartItemDTO>().ReverseMap();
            CreateMap<HouseKeepingFacilityDetail, HousekeepingFacilityDetailsInCartDTO>().ReverseMap();

            CreateMap<HousekeepingCartItemAdditional, HousekeepingCartItemAdditionalDTO>()
                .ForMember(dest => dest.Housekeeping_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalElement == null)
                        return null;
                    return src.HousekeepingAdditionalElement.HousekeepingAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Housekeeping_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalElement == null)
                        return 0;
                    return src.HousekeepingAdditionalElement.HousekeepingAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<CreateHousekeepingCartDTO, HousekeepingCart>().ReverseMap();
            CreateMap<CreateHousekeepingCartItemsDTO, HousekeepingCartItem>().ReverseMap();
            CreateMap<CreateHousekeepingCartItemAdditionalDTO, HousekeepingCartItemAdditional>().ReverseMap();

            CreateMap<UpdateHousekeepingCartDTO, HousekeepingCart>().ReverseMap();
            CreateMap<UpdateHousekeepingCartItemsDTO, HousekeepingCartItem>().ReverseMap();
            CreateMap<CustomerActiveHousekeepingOrdersAndBaskets, CustomerActiveHousekeepingOrdersAndBasketsDTO>().ReverseMap();
        }
    }
    public class HousekeepingCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public Nullable<double> Total { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<HousekeepingCartItemDTO> HousekeepingCartItems { get; set; }
    }

    public class CustomerActiveHousekeepingCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<HousekeepingCartItemDTO> HousekeepingCartItems { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
    }

    public class HousekeepingCartItemDTO
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<double> PreferredCurrencyTotal { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public virtual HousekeepingFacilityDetailsInCartDTO HouseKeepingFacilityDetail { get; set; }
        public virtual ICollection<HousekeepingCartItemAdditionalDTO> HousekeepingCartItemAdditionals { get; set; }
    }

    public class HousekeepingFacilityDetailsInCartDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string Image { get; set; }
    }

    public class HousekeepingCartItemAdditionalDTO
    {
        public int Housekeeping_Additional_Group_ID { get; set; }
        public string Housekeeping_Additional_Group_Name { get; set; }
        public virtual HousekeepingAdditionalElementDTO HousekeepingAdditionalElement { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
    }

    //create housekeeping cart
    public class CreateHousekeepingCartDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Room id shouldn't be zero or negative")]
        public int RoomId { get; set; }
        public ICollection<CreateHousekeepingCartItemsDTO> HousekeepingCartItems { get; set; }
    }

    public class CreateHousekeepingCartItemsDTO
    {
        public int HouseKeepingFacilityDetailsId { get; set; }

        public int Quantity { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public ICollection<CreateHousekeepingCartItemAdditionalDTO> HousekeepingCartItemAdditionals { get; set; }
    }

    public class CreateHousekeepingCartItemAdditionalDTO
    {
        public int HousekeepingAdditionalElementId { get; set; }
        public Nullable<int> Qty { get; set; }
    }


    //update housekeeping cart
    public class UpdateHousekeepingCartDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Room id shouldn't be zero or negative")]
        public int RoomId { get; set; }

        public ICollection<UpdateHousekeepingCartItemsDTO> HousekeepingCartItems { get; set; }
    }

    public class UpdateHousekeepingCartItemsDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id shouldn't be Zero or Negative")]
        public Nullable<int> Id { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "HouseKeepingFacility Detail Id shouldn't be zero or negative")]
        public int HouseKeepingFacilityDetailsId { get; set; }

        public int Quantity { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        public Nullable<System.DateTime> ScheduleDate { get; set; }

        public ICollection<CreateHousekeepingCartItemAdditionalDTO> HousekeepingCartItemAdditionals { get; set; }
    }
}