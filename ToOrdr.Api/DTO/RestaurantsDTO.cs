﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class RestaurantsBaseDTO
    {
        public int RestaurantId { get; set; }
        public int Restaurant_Type_ID { get; set; }
        public bool Restaurant_Valid { get; set; }
    }

    public class RestaurantsDTO : RestaurantsBaseDTO
    {
        public int Restaurants_ID { get; set; }

        public System.DateTime CreationDate { get; set; }
    }
    public class RestaurantDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoImage { get; set; }
        public string CurrencySymbol { get; set; }
    }

    public class RestaurantInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoImage { get; set; }
        public string BackgroundImage { get; set; }
        public string CurrencySymbol { get; set; }
    }

    public class RestaurantInfoPdfDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public string CurrencySymbol { get; set; }
    }
}