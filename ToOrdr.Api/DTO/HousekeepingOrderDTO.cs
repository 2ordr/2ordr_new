﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.DTO
{
    public class HousekeepingOrderProfile : AutoMapper.Profile
    {
        public HousekeepingOrderProfile()
        {
            CreateMap<HousekeepingOrder, HousekeepingOrderDTO>()
                .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.HousekeepingOrderItems.Any())
                        return 0; ;

                    return src.HousekeepingOrderItems.Count();
                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Room.Hotel.Currency != null)
                    {
                        return src.Room.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src => src.Customer.Currency.Symbol))
                .ReverseMap();

                CreateMap<HousekeepingOrder, CustomerActiveHousekeepingOrderDTO>()
                .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.Room.Hotel))
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.HousekeepingOrderItems.Any())
                        return 0; ;

                    return src.HousekeepingOrderItems.Count();
                }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src => src.Customer.Currency.Symbol))
                .ReverseMap();

                CreateMap<HousekeepingOrder,  HousekeepingOrderPdfDTO>()
                   .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                   .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.Room.Hotel))
                   .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src => src.Customer.Currency.Symbol))
                   .ReverseMap();

            CreateMap<HousekeepingOrderItem, HousekeepingOrderItemDTO>().ReverseMap();

            CreateMap<HousekeepingOrderItemAdditional, HousekeepingOrderItemAdditionalDTO>()
                .ForMember(dest => dest.Housekeeping_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalElement == null)
                        return null;
                    return src.HousekeepingAdditionalElement.HousekeepingAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Housekeeping_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.HousekeepingAdditionalElement == null)
                        return 0;
                    return src.HousekeepingAdditionalElement.HousekeepingAdditionalGroupId;
                }))
                .ReverseMap();
        }
    }

    public class HousekeepingOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual ICollection<HousekeepingOrderItemDTO> HousekeepingOrderItems { get; set; }
    }

    public class CustomerActiveHousekeepingOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public int Order_Item_Count { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual ICollection<HousekeepingOrderItemDTO> HousekeepingOrderItems { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
    }

    public class  HousekeepingOrderPdfDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public int CardId { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public System.DateTime OrderDate { get; set; }
        public virtual ICollection<HousekeepingOrderItemDTO> HousekeepingOrderItems { get; set; }
        public virtual HotelPdfDTO Hotel { get; set; }
    }

    public class HousekeepingOrderItemDTO
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public bool HousekeepingOrderStatus { get; set; }
        public virtual HousekeepingFacilityDetailsInCartDTO HouseKeepingFacilityDetail { get; set; }
        public virtual ICollection<HousekeepingOrderItemAdditionalDTO> HousekeepingOrderItemAdditionals { get; set; }
    }

    public class HousekeepingOrderItemAdditionalDTO
    {
        public int Housekeeping_Additional_Group_ID { get; set; }
        public string Housekeeping_Additional_Group_Name { get; set; }
        public int Id { get; set; }
        public int HousekeepingOrdertemId { get; set; }
        public int HousekeepingAdditionalElementId { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
        public virtual HousekeepingAdditionalElementDTO HousekeepingAdditionalElement { get; set; }
    }
}