﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class RestaurantMenuItemsBaseDTO
    {
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<float> Price { get; set; }
    }
    public class RestaurantMenuItemDetailsDTO
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }
    }

    /// <summary>
    /// Menu Items Model
    /// </summary>
    public class RestaurantMenuItemsDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuGroupId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public bool IsFavourite { get; set; }
        public Nullable<System.TimeSpan> PreparationTime { get; set; }
        public bool SoldOut { get; set; }
        public Nullable<double> Percentage { get; set; }
        public double OfferAmount { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }

    }

    public class RestMenuItemsDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuGroupId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }
        public string RestaurantCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsFavourite { get; set; }
        public bool MyPreference { get; set; }
        public Nullable<System.TimeSpan> PreparationTime { get; set; }
        public bool SoldOut { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }

    }

    public class RestMenuItemsPendingReviewDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuGroupId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }
        public string RestaurantCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.TimeSpan> PreparationTime { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }

    }

    /// <summary>
    /// Favorite Menu Items Model
    /// </summary>
    public class RestaurantFavoriteMenuItemsDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuGroupId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsActive { get; set; }
        public bool IsFavourite { get; set; }
        public int TotalOrders { get; set; }
        public Nullable<DateTime> LastOrder { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }
        public virtual RestaurantDetailsDTO Restaurant { get; set; }

    }
    public class RestaurantMenuItemImagesDTO
    {
        public int RestaurantMenuItemId { get; set; }
        public string Image { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }

    public class RestaurantMenuItemDetailedDTO : RestaurantMenuItemsDTO
    {

        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public string RestaurantCurrencySymbol { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<MenuItemAdditionalDTO> MenuItemAdditionals { get; set; }

        public virtual RestaurantMenuGroupsDTO4MenuItem RestaurantMenuGroup { get; set; }

        public virtual ICollection<MenuItemSizeDTO> MenuItemSizes { get; set; }

        public virtual ICollection<MenuItemIngredientDTO> MenuItemIngredient { get; set; }
    }

    public class MenuItemSuggestionsDTO
    {
        public virtual ICollection<RestaurantMenuItemsDTO> RestaurantMenuItems { get; set; }
    }

    public class MenuItemReviewDTO
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CustomerInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public virtual CountryDTO Country { get; set; }
        public string ProfileImage { get; set; }
    }

    public class CustomerDetailsDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public virtual CountryDTO Country { get; set; }
    }

    public class MenuItemIngredientDTO
    {
        public int IngredientId { get; set; }
        public string Ingredient_Name { get; set; }
        public double Weight { get; set; }
        public string UnitSymbol { get; set; }
    }

    public class MenuItemIngredientInfoDTO
    {
        public int IngredientId { get; set; }
        public string Ingredient_Name { get; set; }
        public double Weight { get; set; }
        public string UnitSymbol { get; set; }
        public virtual ICollection<MenuItemIngredientNutritionDTO> MenuItemIngredientNutrition { get; set; }
    }

    public class MenuItemIngredientNutritionDTO
    {
        public int Id { get; set; }
        public virtual NutritionDTO Nutrition { get; set; }
        public string NutritionValue { get; set; }
    }

    public class NutritionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MeasurementUnit { get; set; }
    }

    public class MenuItemSizeDTO
    {
        public int Id { get; set; }
        public string SizeName { get; set; }
        public Nullable<decimal> SizePrice { get; set; }
        public Nullable<int> SizeCalories { get; set; }
        public Nullable<bool> IsDefault { get; set; }

    }

    public class MenuItemAdditionalDTO
    {

        public int MenuAdditionalGroupId { get; set; }

        public virtual MenuAdditionalGroupDTO MenuAdditionalGroup { get; set; }
    }

    public class MenuAdditionalGroupDTO
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public Nullable<int> MinimumSelected { get; set; }
        public Nullable<int> MaximumSelected { get; set; }

        public virtual ICollection<MenuAdditionalElementDTO> MenuAdditionalElements { get; set; }
    }

    public class MenuAdditionalElementDTO
    {
        public int Id { get; set; }
        public string AdditionalElementName { get; set; }
        public string AdditionalElementDescription { get; set; }
        public double AdditionalCost { get; set; }
        public double PreferredCurrencyAdditionalCost { get; set; }
        public string Image { get; set; }


    }

    public class CreateMenuItemReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_MenuItemId", ErrorMessageResourceType = typeof(Resources))]
        public int RestaurantMenuItemId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }

    // Menu Item Offers
    public class MenuItemOffersDTO
    {
        public int Id { get; set; }
        public string RestaurantName { get; set; }
        public string Description { get; set; }
        public Nullable<double> Percentage { get; set; }
        // public double OfferAmount { get; set; }
        public virtual RestaurantMenuItemInfoDTO RestaurantMenuItem { get; set; }
    }

    public class RestaurantMenuItemInfoDTO
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public bool SoldOut { get; set; }
        public virtual ICollection<RestaurantMenuItemImagesDTO> RestaurantMenuItemImages { get; set; }

    }

}