﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class SpaOrderReviewDTO
    {
        public int Id { get; set; }
        public int SpaOrderId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }
    public class CreateSpaOrderReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_SpaOrderId", ErrorMessageResourceType = typeof(Resources))]
        public int SpaOrderId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }
}