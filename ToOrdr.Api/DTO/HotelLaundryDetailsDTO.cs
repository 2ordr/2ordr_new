﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToOrdr.Api.DTO
{

    public abstract class HotelLaundryDetailsBaseDTO
    {
        public int Hotel_Laundry_Details_ID { get; set; }
        public int Hotel_Laundry_ID { get; set; }
        public string Laundry_Item { get; set; }
        public string Laundry_Item_Description { get; set; }
        public double Laundry_Item_Rate { get; set; }
        public bool IsActive { get; set; }

    }
    public class HotelLaundryDetailsDTO : HotelLaundryDetailsBaseDTO
    {
        public int Hotel_Laundry_Details_ID { get; set; }
        public System.DateTime CreationDate { get; set; }
    }
}