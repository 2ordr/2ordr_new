﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToOrdr.Api.DTO
{
    public class HotelLaundryBookingDetailsBaseDTO
    {
        public int Quantity { get; set; }
        public string Description { get; set; }
    }

    public class CreateHotelLaundryBookingDetailsDTO : HotelLaundryBookingDetailsDTO
    {

    }
    public class HotelLaundryBookingDetailsDTO : HotelLaundryBookingDetailsBaseDTO
    {
        public int Hotel_Laundry_Booking_Details_ID { get; set; }
        public int Hotel_Laundry_Booking_ID { get; set; }
        public int Hotel_Laundry_Details_ID { get; set; }
        //public virtual HotelLaundryBookingDTO Hotel_Laundry_Booking { get; set; }
        //public virtual HotelLaundryDetailsDTO Hotel_Laundry_Details { get; set; }
    }
}