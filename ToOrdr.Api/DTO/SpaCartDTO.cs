﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class SpaCartProfile : AutoMapper.Profile
    {
        public SpaCartProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<SpaCart, SpaCartDTO>()
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.SpaCartItems.Any())
                        return 0; ;

                    return src.SpaCartItems.Count();

                }))
                .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Total) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                }))
                 .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Hotel.Currency != null)
                    {
                        return src.Hotel.Currency.Symbol;
                    }
                    return null;
                })).ReverseMap();

            CreateMap<SpaCart, SpaCartInfoDTO>()
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>src.Customer.Currency.Symbol))
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.SpaCartItems.Any())
                        return 0; ;

                    return src.SpaCartItems.Count();

                }))
                .ReverseMap();

            CreateMap<SpaCartItem, SpaCartItemsDTO>()
                 .ForMember(dest => dest.SpaServiceDetailName, opt => opt.ResolveUsing(src =>
                 {
                     if (src == null || src.SpaServiceDetail == null)
                         return string.Empty;
                     return src.SpaServiceDetail.Name;
                 }))
                 .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                 {
                     if (src == null || src.SpaServiceDetail == null)
                         return 0;
                     return src.SpaServiceDetail.Price;
                 }))
                 .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
                 {
                     double? baseCurrencyRate = 0;
                     if (src.SpaCart.Customer.CurrencyId != null && src.SpaCart.Customer.CurrencyId > 0)
                         baseCurrencyRate = src.SpaCart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                     if (baseCurrencyRate > 0)
                     {
                         return System.Math.Round(Convert.ToDouble(src.SpaServiceDetail.Price * baseCurrencyRate), 2);
                     }
                     return 0;
                 }))
                 .ForMember(dest => dest.PreferredCurrencyOfferPrice, opt => opt.ResolveUsing(src =>
                 {
                     double? baseCurrencyRate = 0;
                     if (src.SpaCart.Customer.CurrencyId != null && src.SpaCart.Customer.CurrencyId > 0)
                         baseCurrencyRate = src.SpaCart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                     if (baseCurrencyRate > 0)
                     {
                         return System.Math.Round(Convert.ToDouble(src.OfferPrice * baseCurrencyRate), 2);
                     }
                     return 0;
                 }))
                  .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                  {
                      double? baseCurrencyRate = 0;
                      if (src.SpaCart.Customer.CurrencyId != null && src.SpaCart.Customer.CurrencyId > 0)
                          baseCurrencyRate = src.SpaCart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                      if (baseCurrencyRate > 0)
                      {
                          return System.Math.Round(Convert.ToDouble(src.Total * baseCurrencyRate), 2);
                      }
                      return 0;
                  }))
                .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaServiceDetail.SpaServiceDetailImages.Count() == 0)
                        return null;
                    return api_path + "Images/Services/SpaService/SpaServiceDetails/" + src.SpaServiceDetailId.ToString() + "/" + src.SpaServiceDetail.SpaServiceDetailImages.FirstOrDefault(i => i.IsPrimary == true).Image;
                }))
         .ReverseMap();

            CreateMap<SpaCartItemsAdditional, SpaCartItemAdditionalDTO>()
                .ForMember(dest => dest.Spa_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaAdditionalElement == null)
                        return null;
                    return src.SpaAdditionalElement.SpaAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Spa_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.SpaAdditionalElement == null)
                        return 0;
                    return src.SpaAdditionalElement.SpaAdditionalGroupId;
                }))
                .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.SpaCartItem.SpaCart.Customer.CurrencyId != null && src.SpaCartItem.SpaCart.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.SpaCartItem.SpaCart.Hotel.Currency.ExchangeRates.Where(i => i.ToCurrency == src.SpaCartItem.SpaCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Total) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ReverseMap();

            CreateMap<SpaAdditionalElement, SpaAdditionalElementInfoDTO>().ReverseMap();

            CreateMap<CreateSpaCartDTO, SpaCart>().ReverseMap();
            CreateMap<CreateSpaCartItemDTO, SpaCartItem>().ReverseMap();
            CreateMap<CreateSpaCartItemAdditionalDTO, SpaCartItemsAdditional>().ReverseMap();
            CreateMap<SpaCartExtraProcedureDTO, SpaCartExtraProcedure>().ReverseMap();
            CreateMap<CreateSpaCartExtraProcedureDTO, SpaCartExtraProcedure>().ReverseMap();

            CreateMap<UpdateSpaCartDTO, SpaCart>().ReverseMap();
            CreateMap<UpdateSpaCartItemDTO, SpaCartItem>().ReverseMap();
            CreateMap<CustomerActiveSpaOrdersAndBaskets, CustomerActiveSpaOrdersAndBasketsDTO>().ReverseMap();
        }
    }
    public class SpaCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<SpaCartItemsDTO> SpaCartItems { get; set; }
    }
    public class SpaCartInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<double> preferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<SpaCartItemsDTO> SpaCartItems { get; set; }
    }

    public class SpaCartItemsDTO
    {
        public int Id { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string SpaServiceDetailName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public double PreferredCurrencyOfferPrice { get; set; }
        public int Qty { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public System.DateTime EndDateTime { get; set; }
        public int ServeLevel { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string Image { get; set; }
        public int SpaEmployeeDetailsId { get; set; }
        public virtual SpaEmployeeDetailsDTO SpaEmployeeDetail { get; set; }
        public Nullable<int> ExtraTimeId { get; set; }
        public virtual ExtraTimeDTO ExtraTime { get; set; }
        public virtual ICollection<SpaCartItemAdditionalDTO> SpaCartItemsAdditionals { get; set; }
        public virtual ICollection<SpaCartExtraProcedureDTO> SpaCartExtraProcedures { get; set; }
    }

    public class SpaCartItemAdditionalDTO
    {
        public int Spa_Additional_Group_ID { get; set; }
        public string Spa_Additional_Group_Name { get; set; }
        public virtual SpaAdditionalElementInfoDTO SpaAdditionalElement { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyPrice { get; set; }
    }

    public class SpaAdditionalElementInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }

    public class SpaCartExtraProcedureDTO
    {
        public int Id { get; set; }
        public int ExtraprocedureId { get; set; }
        public virtual ExtraProcedureDTO ExtraProcedure { get; set; }
    }

    //create cart
    public class CreateSpaCartDTO
    {
        public ICollection<CreateSpaCartItemDTO> SpaCartItems { get; set; }
    }

    public class CreateSpaCartItemDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "SpaServiceDetail Id shouldn't be zero or negative")]
        public int SpaServiceDetailId { get; set; }

        public int Qty { get; set; }

        [ValidStartDateTime()]
        [Required(ErrorMessage = "START_DATE_MUST_BE_REQUIRED")]
        public System.DateTime StartDateTime { get; set; }

        [ValidEndDateTime()]
        [Required(ErrorMessage = "END_DATE_MUST_BE_REQUIRED")]
        public System.DateTime EndDateTime { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter proper ServeLevel")]
        public int ServeLevel { get; set; }
        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "SpaEmployeeDetail Id shouldn't be zero or negative")]
        public int SpaEmployeeDetailsId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "ExtraTimeId shouldn't be zero or negative")]
        public Nullable<int> ExtraTimeId { get; set; }

        public ICollection<CreateSpaCartItemAdditionalDTO> SpaCartItemsAdditionals { get; set; }

        public ICollection<CreateSpaCartExtraProcedureDTO> SpaCartExtraProcedures { get; set; }
    }

    public class CreateSpaCartItemAdditionalDTO
    {
        public int SpaAdditionalElementId { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Additional_Qty", ErrorMessageResourceType = typeof(Resources))]
        public Nullable<int> Qty { get; set; }
    }

    public class CreateSpaCartExtraProcedureDTO
    {
        public int ExtraprocedureId { get; set; }
    }


    //update Cart

    public class UpdateSpaCartDTO
    {
        public ICollection<UpdateSpaCartItemDTO> SpaCartItems { get; set; }
    }

    public class UpdateSpaCartItemDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id shouldn't be Zero or Negative")]
        public Nullable<int> Id { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "SpaServiceDetail Id shouldn't be zero or negative")]
        public int SpaServiceDetailId { get; set; }

        public int Qty { get; set; }


        [Required(ErrorMessage = "START_DATE_MUST_BE_REQUIRED")]
        public System.DateTime StartDateTime { get; set; }


        [Required(ErrorMessage = "END_DATE_MUST_BE_REQUIRED")]
        public System.DateTime EndDateTime { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter proper ServeLevel")]
        public int ServeLevel { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "SpaEmployeeDetail Id shouldn't be zero or negative")]
        public int SpaEmployeeDetailsId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "ExtraTimeId shouldn't be zero or negative")]
        public Nullable<int> ExtraTimeId { get; set; }
        public ICollection<CreateSpaCartItemAdditionalDTO> SpaCartItemsAdditionals { get; set; }
        public ICollection<CreateSpaCartExtraProcedureDTO> SpaCartExtraProcedures { get; set; }
    }
}