﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class CurrencyBaseDTO
    {
        public string CurrencyName { get; set; }        
        public string Code { get; set; }
        public string Symbol { get; set; }
    }

    public class GetCurrencyDTO : CurrencyBaseDTO
    {
        public int Id { get; set; }
    }
    public class CurrenciesDTO
    {
        public int Id { get; set; }
        public string CurrencyName { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
    }
}