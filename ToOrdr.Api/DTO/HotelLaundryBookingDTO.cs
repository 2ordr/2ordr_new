﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToOrdr.Api.DTO
{
    public abstract class HotelLaundryBookingBaseDTO
    {
        public int Id { get; set; }
        public System.DateTime Laundry_Booking_DateTime { get; set; }
        public bool Laundry_Status { get; set; }
        public bool IsActive { get; set; }
    }

    public class CreateHotelLaundryBookingDTO : HotelLaundryBookingDTO
    {
        public List<HotelLaundryBookingDetailsDTO> Hotel_Laundry_Booking_Details { get; set; }
    }
    public class HotelLaundryBookingDTO : HotelLaundryBookingBaseDTO
    {
        public int Hotel_Laundry_Booking_ID { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerDTO Customer { get; set; }
        public virtual HotelLaundryBookingDetailsDTO Hotel_Laundry_Booking_Details { get; set; }
    }
}