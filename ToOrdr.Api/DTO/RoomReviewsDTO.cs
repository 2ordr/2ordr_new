﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class RoomReviewsDTO
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerDetailsDTO Customer { get; set; }
    }

    public class CreateRoomReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_RoomId", ErrorMessageResourceType = typeof(Resources))]
        public int RoomId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }
}