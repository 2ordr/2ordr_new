﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class TripReviewDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TripId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateTripReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_TripId", ErrorMessageResourceType = typeof(Resources))]
        public int TripId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}