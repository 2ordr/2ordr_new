﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class RestaurantOrderReviewsBaseDTO
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
    }
    public class RestaurantOrderReviewsDTO : RestaurantOrderReviewsBaseDTO
    {
        public int Id { get; set; }

        public System.DateTime CreationDate { get; set; }


        public virtual CustomerInfoDTO Customer { get; set; }


    }

    public class CreateRestaurantOrderReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_RestaurantOrderId", ErrorMessageResourceType = typeof(Resources))]
        public int RestaurantOrderId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }


    public class RestaurantReviewDTO
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public int CustomerId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }

        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CustomerRestaurantReviewDTO
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual RestaurantDetailsDTO Restaurant { get; set; }
    }


    public class CustomerMenuItemReviewDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuItemId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual RestaurantMenuItemDetailsDTO RestaurantMenuItem { get; set; }
        public virtual RestaurantDetailsDTO Restaurant { get; set; }
    }

    public class CreateRestaurantReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_RestaurantId", ErrorMessageResourceType = typeof(Resources))]
        public int RestaurantId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }

    public class CreateRestaurantPayDTO
    {
        public int OrderId { get; set; }
        public Nullable<int> CardId { get; set; }
    }
    public class CreateOrderTipPayDTO
    {
        public int OrderId { get; set; }
        public Nullable<int> CardId { get; set; }
        public Nullable<Double> TipAmount { get; set; }
    }

    public class CustomerHotelReviewDTO
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }
    }

}