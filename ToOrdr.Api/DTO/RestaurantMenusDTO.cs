﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Api.DTO
{

    public abstract class RestaurantMenusBaseDTO
    {
        [Required]
        public int RestaurantId { get; set; }
        public string MenuName { get; set; }
        public string MenuDescription { get; set; }
        public string Image { get; set; }
        public string RestaurantCurrencySymbol { get; set; }
        public string PreferredCurrencySymbol { get; set; }
    }

    public class RestaurantMenusDTO : RestaurantMenusBaseDTO
    {
        public int Id { get; set; }
        public virtual ICollection<RestaurantMenuGroupsDTO> RestaurantMenuGroups { get; set; }
    }

    public class ConcreteRestaurantMenusDTO
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public string MenuName { get; set; }
        public string MenuDescription { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public bool MyPreference { get; set; }
    }
}