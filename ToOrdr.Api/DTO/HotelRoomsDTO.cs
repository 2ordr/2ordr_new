﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class HotelRoomsDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int RoomTypeId { get; set; }
        public string Number { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        public double Rate { get; set; }
        public int Adults { get; set; }
        public int Childrens { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<HotelRoomImagesDTO> HotelRoomImages { get; set; }
    }
    public class HotelRoomDetailsDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int RoomTypeId { get; set; }
        public string Number { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        public double Rate { get; set; }
        public int Adults { get; set; }
        public int Childrens { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<HotelRoomImagesDTO> HotelRoomImages { get; set; }
        public virtual ICollection<RoomReviewsDTO> RoomReviews { get; set; }
    }

    public class HotelRoomImagesDTO
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }

    public class RoomDTO
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<RoomDetailsDTO> RoomDetails { get; set; }
    }
    public class RoomDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual ICollection<RoomHousekeepingDTO> HouseKeeping { get; set; }
    }
    public class RoomHousekeepingDTO
    {
        public virtual HouseKeepingFacilitiesDTO HouseKeepingFacility { get; set; }
    }

    public class CustomerHotelRoomInfoDTO
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int CustomerId { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public virtual RoomDTO Room { get; set; }
    }

    public class CustomerHotelRoomDTO
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int CustomerId { get; set; }
        public virtual RoomInfoDTO Room { get; set; }
    }

    public class RoomInfoDTO
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<RoomInfoDetailsDTO> RoomDetails { get; set; }
    }
    public class RoomInfoDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
    }

    public class WakeUpRoomDTO
    {
        public int Id { get; set; }
        public string Number { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}