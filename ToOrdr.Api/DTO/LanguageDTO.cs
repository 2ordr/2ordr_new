﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class LanguageBaseDTO
    {
        public string Name { get; set; }
        public string LanguageCulture { get; set; }
        public System.DateTime CreationDate { get; set; }
    }

    public class LanguageDTO : LanguageBaseDTO
    {
        public int Id { get; set; }
    }
}