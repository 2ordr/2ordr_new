﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class CountryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneCode { get; set; }
        public string Image { get; set; }             
    }
}