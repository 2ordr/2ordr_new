﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class TaxiDetailDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public double RatePerKm { get; set; }
        public int Seats { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public System.DateTime CreationDate { get; set; }
    }

    public class TaxiBookingDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TaxiDetailsId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Comment { get; set; }
        public string OrderStatus { get; set; }
        public System.DateTime CreationDate { get; set; }

        public virtual CustomerInfoDTO Customer { get; set; }
        public virtual TaxiDetailDTO TaxiDetails { get; set; }
    }

    public class CreateTaxiBookingDTO
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_TaxiDetailId", ErrorMessageResourceType = typeof(Resources))]
        public int TaxiDetailsId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Comment { get; set; }
    }

    public class UpdateTaxiBookingDTO
    {
        public int Id { get; set; }
        public int TaxiDetailsId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Comment { get; set; }
    }

    public class TaxiDestinationDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public string FromPlace { get; set; }
        public string ToPlace { get; set; }
        public bool IsActive { get; set; }

    }

    public class TaxiBookingReviewDTO
    {
        public int Id { get; set; }
        public int TaxiBookingId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }
    public class CreateTaxiBookingReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Req_TaxiBookingId", ErrorMessageResourceType = typeof(Resources))]
        public int TaxiBookingId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }
    public class CustomerTaxiBookingDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TaxiDetailsId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Comment { get; set; }
        public string OrderStatus { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual TaxiDetailDTO TaxiDetails { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }
    }

}