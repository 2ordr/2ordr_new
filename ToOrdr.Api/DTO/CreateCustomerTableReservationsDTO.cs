﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class CustomerTableReservationsBaseDTO
    {

        public int Id { get; set; }

        public int TotalSeats { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public System.DateTime Reservation_DateTime { get; set; }

    }

    public class CreateCustomerTableReservationsDTO : CustomerTableReservationsBaseDTO
    {

    }
    public class CustomerTableReservationsDTO : CustomerTableReservationsBaseDTO
    {
        public int Customer_Table_Reservation_ID { get; set; }

        public System.DateTime CreationDate { get; set; }

        public virtual CustomerDTO Customer { get; set; }

        public virtual ICollection<CustomerReservedTableDTO> Customer_Reserved_Table { get; set; }
    }
}