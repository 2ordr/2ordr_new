﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class CustomerRestaurantOrderItemCustomisationDTO
    {
        public int Customer_RestaurantOrder_Item_customisation_ID { get; set; }
        public int Customer_RestaurantOrder_Item_ID { get; set; }
        public int RestaurantMenuItems_Customisations_ID { get; set; }
        public System.DateTime CreationDate { get; set; }

        public virtual CustomerRestaurantOrderItemsDTO Customer_RestaurantOrder_Items { get; set; }
        public virtual RestaurantMenuItemsCustomisationsDTO RestaurantMenuItems_Customisations { get; set; }
    }
}