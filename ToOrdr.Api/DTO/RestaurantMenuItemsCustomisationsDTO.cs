﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class RestaurantMenuItemsCustomisationsDTO
    {
        public int Restaurant_Menu_Items_Customisations_ID { get; set; }
        public int Restaurant_Menu_Item_Id { get; set; }
        public int Restaurant_Menu_Items_Customisation_ID { get; set; }
        public string Restaurant_Menu_Items_Customisations_Icon { get; set; }
        public System.DateTime Creation_Date { get; set; }
    }
}