﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class CuisineBaseDTO
    {

        public string Name { get; set; }


    }
    public class CuisineDTO 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class RestCuisinesDTO
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public string Image { get; set; }
        public virtual CuisineDTO Cuisine { get; set; }
    }

    public abstract class RestaurantCuisineBaseDTO
    {

        public int RestaurantId { get; set; }
        public int Id { get; set; }

    }
    public class RestaurantCuisinesDTO : RestaurantCuisineBaseDTO
    {
        public int Id { get; set; }

        public virtual CuisineDTO Cuisine { get; set; }
    }



}