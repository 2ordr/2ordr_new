﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class CustomerBaseDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public System.DateTime BirthDate { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Pincode { get; set; }
        public virtual CountryDTO Country { get; set; }
        public string PhoneCode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public virtual GetCurrencyDTO Currency { get; set; }

    }

    public class CreateCustomerDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public int CountryId { get; set; }
        public int CurrencyId { get; set; }
        public string PhoneCode { get; set; }
    }

    public class CustomerDTO : CustomerBaseDTO
    {
        public int Id { get; set; }
        public bool ActiveStatus { get; set; }
    }



    public class ModifyCustomerDTO
    {

        [MaxLength(50, ErrorMessageResourceName = "Max_FirstName", ErrorMessageResourceType = typeof(Resources))]
        public string FirstName { get; set; }


        [MaxLength(50, ErrorMessageResourceName = "Max_LastName", ErrorMessageResourceType = typeof(Resources))]
        public string LastName { get; set; }

        public string Gender { get; set; }

        public System.DateTime BirthDate { get; set; }


        [MaxLength(100, ErrorMessageResourceName = "Max_Street", ErrorMessageResourceType = typeof(Resources))]
        public string Street { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "Max_Street2", ErrorMessageResourceType = typeof(Resources))]
        public string Street2 { get; set; }


        [MaxLength(100, ErrorMessageResourceName = "Max_City", ErrorMessageResourceType = typeof(Resources))]
        public string City { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "Max_Region", ErrorMessageResourceType = typeof(Resources))]
        public string Region { get; set; }


        public int CountryId { get; set; }

        public string PhoneCode { get; set; }

        [MaxLength(12, ErrorMessageResourceName = "Max_Pincode", ErrorMessageResourceType = typeof(Resources))]
        public string Pincode { get; set; }

        [MaxLength(15, ErrorMessageResourceName = "Max_Telephone", ErrorMessageResourceType = typeof(Resources))]
        public string Telephone { get; set; }

        public int CurrencyId { get; set; }

    }

    public class CustomerActiveOrdersAndBasketsDTO
    {
        public ICollection<RestaurantOrderInfoDTO> ActiveOrders { get; set; }
        public ICollection<RestaurantCartInfoDTO> ActiveBaskets { get; set; }
    }
    public class CustomerActiveSpaOrdersAndBasketsDTO
    {
        public ICollection<SpaOrderInfoDTO> ActiveSpaOrders { get; set; }
        public ICollection<SpaCartInfoDTO> ActiveSpaBaskets { get; set; }
    }
    public class CustomerActiveBookingsInHotelDTO
    {
        public ICollection<CustomerHotelBookingsDTO> CustomerHotelBooking { get; set; }
        public ICollection<SpaOrderInfoDTO> SpaOrder { get; set; }
        //public ICollection<LaundryBookingDTO> LaundryBooking { get; set; }
        //public ICollection<TaxiBookingDTO> TaxiBooking { get; set; }
        //public ICollection<TripBookingDTO> TripBooking { get; set; }
    }

    public class AllRestaurantsAndHotelsOrdersDTO
    {
        public string OrderType { get; set; }
        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Activity { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public Nullable<int> DisheshCount { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
    }

    public class CustomerRestAndHotelsOrdersHistoryDTO
    {
        public string OrderType { get; set; }
        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public int CustomerId { get; set; }
        public int PlaceId { get; set; }
        public Nullable<int> ServiceDetailId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Activity { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public Nullable<int> DisheshCount { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public string CurrencySymbol { get; set; }
        public Nullable<bool> TipsPaid { get; set; }
    }

    public class CustomerActiveLaundyOrdersAndBasketsDTO
    {
        public ICollection<CustomerActiveLaundryOrders> ActiveLaundryOrders { get; set; }
        public ICollection<ActiveLaundryCartDTO> ActiveLaundryBaskets { get; set; }
    }
    public class CustomerActiveHousekeepingOrdersAndBasketsDTO
    {
        public ICollection<CustomerActiveHousekeepingOrderDTO> ActiveHousekeepingOrders { get; set; }
        public ICollection<CustomerActiveHousekeepingCartDTO> ActiveHousekeepingBaskets { get; set; }
    }

    public class CustomerActiveExcursionOrdersAndBasketsDTO
    {
        public ICollection<ExcursionOrderInfoDTO> ActiveExcursionOrders { get; set; }
        public ICollection<ExcursionCartInfoDTO> ActiveExcursionBaskets { get; set; }
    }
}