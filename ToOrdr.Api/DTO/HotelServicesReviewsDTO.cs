﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class HotelServicesReviewsDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelServiceId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual HotelServicesDTO HotelServices { get; set; }
    }
    public class CreateHotelServicesReviewsDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_HotelServiceId", ErrorMessageResourceType = typeof(Resources))]
        public int HotelServiceId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
    }
    public class HotelAllServicesReviewsDTO
    {
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public string LogoImage { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int HotelServiceId { get; set; }
        public int ServiceDetailId { get; set; }
        public string ServiceDetailName { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
    }
}