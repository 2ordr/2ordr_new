﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class ConciergeGroupDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public ICollection<ConciergeDTO> Concierges { get; set; }
    }

    public class ConciergeDTO
    {
        public int Id { get; set; }
        public int ConciergeGroupId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool IsActive { get; set; }
    }
}