﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class SpaServiceDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaServiceDetailWithImagesDTO> SpaServiceDetail { get; set; }
    }
    public class GetSpaServiceDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaServiceDetailDTO> SpaServiceDetail { get; set; }
    }
    public class SpaServicesListDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }


    public class SpaServiceDetailDTO
    {
        public int Id { get; set; }
        public int SPAServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaDetailAdditionalDTO> SpaDetailAdditional { get; set; }
        public virtual ICollection<SpaEmployeeDetailsDTO> SpaEmployeeDetails { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
    }

    public class FavoriteSpaServiceDetailDTO
    {
        public int Id { get; set; }
        public int SPAServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public bool IsActive { get; set; }
        public int TotalOrders { get; set; }
        public Nullable<DateTime> LastOrder { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
        public virtual ICollection<SpaDetailAdditionalDTO> SpaDetailAdditional { get; set; }
        public virtual ICollection<SpaEmployeeDetailsDTO> SpaEmployeeDetails { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
    }

    public class SpaServiceDetailWithOfferDTO
    {
        public int Id { get; set; }
        public int SPAServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsFavourite { get; set; }
        public double CustomerRating { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaServiceDetailOfferDTO> SpaServiceOffers { get; set; }
        public virtual ICollection<SpaDetailAdditionalDTO> SpaDetailAdditional { get; set; }
        public virtual ICollection<SpaEmployeeDetailsDTO> SpaEmployeeDetails { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
        public virtual ICollection<ExtraTimeDTO> ExtraTimes { get; set; }
        public virtual ICollection<ExtraProcedureDTO> ExtraProcedures { get; set; }
        public virtual ICollection<SpaSuggestionDTO> SpaSuggestions { get; set; }

    }

    public class SpaSuggestionDTO
    {
        public virtual SpaServiceDetailWithImagesDTO SpaServiceDetail1 { get; set; }
    }

    public class SpaServiceDetailWithImagesDTO
    {
        public int Id { get; set; }
        public int SPAServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public bool IsActive { get; set; }
        public bool IsFavourite { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
    }

    public class SpaServiceDetailImagesDTO
    {
        public int Id { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string Image { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }

    public class SpaDetailAdditionalDTO
    {
        public int Id { get; set; }
        public int SpaAdditionalGroupId { get; set; }
        public bool IsActive { get; set; }
        public virtual SpaAdditionalGroupDTO SpaAdditionalGroup { get; set; }
    }

    public class SpaAdditionalGroupDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int MinSelected { get; set; }
        public int MaxSelected { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SpaAdditionalElementDTO> SpaAdditionalElement { get; set; }
    }

    public class SpaAdditionalElementDTO
    {
        public int Id { get; set; }
        public int SpaAdditionalGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
    }

    public class SpaEmployeeDetailsDTO
    {
        public int Id { get; set; }
        public int SpaEmployeeId { get; set; }
        public virtual SpaEmployeeDTO SpaEmployee { get; set; }
    }

    public class SpaEmployeeDTO
    {
        public int CustomerId { get; set; }
        public virtual SpaEmployeeInfoDTO Customer { get; set; }
        public int Age { get; set; }
        public string Education { get; set; }
        public double Experience { get; set; }
        public string Skill { get; set; }
        public bool IsActive { get; set; }
    }

    public class SpaEmployeeInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string ProfileImage { get; set; }
    }


    public class ModifySPAServicePreferenceDTO
    {
        public ICollection<SPAServicePreferenceDTO> CustomerSpaServicePreference { get; set; }
    }

    public class SPAServicePreferenceDTO
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Id_Reqd", ErrorMessageResourceType = typeof(Resources))]
        public int SPAServiceDetailId { get; set; }
        public bool? IsPreference { get; set; }
    }

    public class HotelSPAServiceReviewDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int SPAServiceDetailId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateHotelSPAServiceReviewsDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Req_SPAServiceDetailId", ErrorMessageResourceType = typeof(Resources))]
        public int SPAServiceDetailId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }

    public class CustomerSpaServiceReviewDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int SPAServiceDetailId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public virtual SpaServiceDetailInfoDTO SPAServiceDetail { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class SpaServiceDetailInfoDTO
    {
        public int SPAServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public TimeSpan Duration { get; set; }
        public bool IsActive { get; set; }
    }

    public class SpaServiceDetailListDTO
    {
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public TimeSpan Duration { get; set; }
        public bool IsActive { get; set; }
        public virtual SpaServicesListDTO SpaService { get; set; }
        public virtual ICollection<SpaDetailAdditionalDTO> SpaDetailAdditional { get; set; }
        public virtual ICollection<SpaEmployeeDetailsDTO> SpaEmployeeDetails { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
    }


    public class SpaEmployeeTimeSlotDTO
    {
        public virtual SpaEmployeeDetailsDTO SpaEmployeeDetails { get; set; }
        public int Id { get; set; }
        public int SpaEmployeeId { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public System.DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    }

    public class SpaRoomDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class SpaServiceDetailsInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<SpaServiceDetailImagesDTO> SpaServiceDetailImages { get; set; }
    }

    public class SpaServiceOffersDTO
    {
        public int Id { get; set; }
        public string HotelName { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public virtual SpaServiceDetailsInfoDTO SpaServiceDetail { get; set; }
    }

    public class HotelSpaServiceOffersDTO
    {
        public int Id { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string Name { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public string OfferDescription { get; set; }
        public double Percentage { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double DiscountPrice { get; set; }
        public double DiscountPriceInPreferredCurrency { get; set; }
        public string OfferImage { get; set; }
        public bool IsFavourite { get; set; }
    }

    public class SpaServiceDetailOfferDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public double DiscountPrice { get; set; }
        public double DiscountPriceInPreferredCurrency { get; set; }
    }

    public class ExtraTimeDTO
    {
        public int Id { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string Name { get; set; }
        public System.TimeSpan Duration { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public bool IsActive { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public string Image { get; set; }
    }

    public class ExtraProcedureDTO
    {
        public int Id { get; set; }
        public int SpaServiceDetailId { get; set; }
        public string Name { get; set; }
        public System.TimeSpan Duration { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public bool IsActive { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public string Image { get; set; }
    }

    public class SpaEmployeeAvailableTimeSlotDTO
    {
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
    }
}