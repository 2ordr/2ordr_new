﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;


namespace ToOrdr.Api.DTO
{

    public class RestaurantCartProfile : AutoMapper.Profile
    {
        public RestaurantCartProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<RestaurantCart, RestaurantCartDTO>()
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.RestaurantCartItems.Any())
                        return 0; ;

                    return src.RestaurantCartItems.Count();

                }))
                .ForMember(dest => dest.PreferredCurrencyAmount, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Amount) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                }))
                  .ForMember(dest => dest.RestaurantCurrencySymbol, opt => opt.ResolveUsing(src =>
                  {
                      if (src.Restaurant.CurrencyId != null && src.Restaurant.CurrencyId > 0)
                          return src.Restaurant.Currency.Symbol;
                      return null;
                  }))
                .ReverseMap();
            CreateMap<RestaurantCart, RestaurantCartInfoDTO>()
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.RestaurantCartItems.Any())
                        return 0; ;
                    return src.RestaurantCartItems.Count();
                }))
                 .ForMember(dest => dest.PreferredCurrencyAmount, opt => opt.ResolveUsing(src =>
                 {
                     double? baseCurrencyRate = 0;
                     if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                         baseCurrencyRate = src.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                     if (baseCurrencyRate > 0)
                     {
                         return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Amount) * baseCurrencyRate), 2);
                     }
                     return 0;
                 }))
                 .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                         return src.Customer.Currency.Symbol;
                     return null;
                 }))
                .ReverseMap();

            CreateMap<RestaurantCartItem, RestaurantCartItemDTO>()
                .ForMember(dest => dest.ItemName, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.RestaurantMenuItem == null)
                        return string.Empty;
                    return src.RestaurantMenuItem.ItemName;
                }))
                .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.RestaurantCart.Customer.CurrencyId != null && src.RestaurantCart.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.RestaurantCart.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.RestaurantCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.RestaurantMenuItem.Price) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.RestaurantCart.Customer.CurrencyId != null && src.RestaurantCart.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.RestaurantCart.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.RestaurantCart.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.Total) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                {
                    if (src != null || src.RestaurantMenuItem != null)
                    {
                        return src.RestaurantMenuItem.Price;
                    }
                    return 0;
                }))
                 .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                 {
                     if (src.RestaurantMenuItem.RestaurantMenuItemImages.Count() == 0)
                         return null;
                     return api_path + "Images/MenuItem/" + src.RestaurantMenuItemId.ToString() + "/" + src.RestaurantMenuItem.RestaurantMenuItemImages.FirstOrDefault(i => i.IsPrimary == true).Image;
                 }))
                 .ForMember(dest => dest.PreparationTime, opt => opt.ResolveUsing(src =>
                 {
                     if (src == null || src.RestaurantMenuItem == null)
                         return null;
                     return src.RestaurantMenuItem.PreparationTime;
                 }))
                .ReverseMap();

            CreateMap<RestaurantCartItemAdditional, RestaurantCartItemAdditionalDTO>()
                .ForMember(dest => dest.Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.MenuAdditionalElement == null)
                        return null;
                    return src.MenuAdditionalElement.MenuAdditionalGroup.GroupName;
                }))
                .ForMember(dest => dest.Menu_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.MenuAdditionalElement == null)
                        return 0;
                    return src.MenuAdditionalElement.MenuAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<MenuAdditionalElement, RestaurantCartItemAdditionalElementDTO>().ReverseMap();
            CreateMap<RestaurantMenuItemImage, RestaurantMenuItemImagesDTO>().ReverseMap();

            CreateMap<CreateCartDTO, RestaurantCart>().ReverseMap();
            CreateMap<CreateCartItemDTO, RestaurantCartItem>().ReverseMap();
            CreateMap<UpdateCartDTO, RestaurantCart>().ReverseMap();
            CreateMap<UpdateCartItemDTO, RestaurantCartItem>().ReverseMap();
            CreateMap<UpdateCartItemAdditionalDTO, RestaurantCartItemAdditional>().ReverseMap();

            CreateMap<CustomerActiveOrdersAndBaskets, CustomerActiveOrdersAndBasketsDTO>().ReverseMap();

            // orders related

            CreateMap<RestaurantOrder, RestaurantOrderDTO>()
                .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.RestaurantOrderItems.Any())
                        return 0; ;
                    return src.RestaurantOrderItems.Count();

                }))
                .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.OrderTotal) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                 .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                         return src.Customer.Currency.Symbol;
                     return null;
                 }))
                .ReverseMap();

            CreateMap<RestaurantOrder, RestaurantOrderInfoDTO>()
                .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
                .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.RestaurantOrderItems.Any())
                        return 0; ;

                    return src.RestaurantOrderItems.Count();
                }))
                .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
                {
                    double? baseCurrencyRate = 0;
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        baseCurrencyRate = src.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                    if (baseCurrencyRate > 0)
                    {
                        return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.OrderTotal) * baseCurrencyRate), 2);
                    }
                    return 0;
                }))
                 .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                 {
                     if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                         return src.Customer.Currency.Symbol;
                     return null;
                 }))
                .ReverseMap();

            CreateMap<RestaurantOrder, RestaurantOrderPdfDTO>()
               .ForMember(dest => dest.OrderDate, opt => opt.ResolveUsing(src => src.CreationDate))
               .ForMember(dest => dest.Order_Item_Count, opt => opt.ResolveUsing(src =>
               {
                   if (src == null || !src.RestaurantOrderItems.Any())
                       return 0; ;

                   return src.RestaurantOrderItems.Count();
               }))
               .ForMember(dest => dest.PreferredCurrencyTotal, opt => opt.ResolveUsing(src =>
               {
                   double? baseCurrencyRate = 0;
                   if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                       baseCurrencyRate = src.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                   if (baseCurrencyRate > 0)
                   {
                       return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.OrderTotal) * baseCurrencyRate), 2);
                   }
                   return 0;
               }))
                .ForMember(dest => dest.PreferredCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Customer.CurrencyId != null && src.Customer.CurrencyId > 0)
                        return src.Customer.Currency.Symbol;
                    return null;
                }))
               .ReverseMap();

            CreateMap<RestaurantOrderItem, RestaurantOrderItemDTO>()
                .ForMember(dest => dest.ItemName, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.RestaurantMenuItem == null)
                        return string.Empty;
                    return src.RestaurantMenuItem.ItemName;
                }))
               .ForMember(dest => dest.PreferredCurrencyPrice, opt => opt.ResolveUsing(src =>
               {
                   double? baseCurrencyRate = 0;
                   if (src.RestaurantOrder.Customer.CurrencyId != null && src.RestaurantOrder.Customer.CurrencyId > 0)
                       baseCurrencyRate = src.RestaurantOrder.Restaurant.Currency.ExchangeRates.Where(i => i.ToCurrency == src.RestaurantOrder.Customer.CurrencyId).Select(i => i.BaseRate).FirstOrDefault();

                   if (src != null || baseCurrencyRate != 0)
                   {
                       return System.Math.Round(Convert.ToDouble(Convert.ToDouble(src.RestaurantMenuItem.Price) * baseCurrencyRate), 2);
                   }
                   return 0;
               }))
                .ForMember(dest => dest.Price, opt => opt.ResolveUsing(src =>
                {
                    if (src != null || src.RestaurantMenuItem != null)
                    {
                        return src.RestaurantMenuItem.Price;
                    }
                    return null;
                }))
                .ForMember(dest => dest.PreparationTime, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || src.RestaurantMenuItem == null)
                        return null;
                    return src.RestaurantMenuItem.PreparationTime;
                }))
                .ReverseMap();

            CreateMap<RestaurantOrderItemAdditional, RestaurantOrderItemAdditionalDTO>()
                .ForMember(dest => dest.Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.MenuAdditionalElement == null)
                        return null;
                    return src.MenuAdditionalElement.MenuAdditionalGroup.GroupName;
                }))
                .ForMember(dest => dest.Menu_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.MenuAdditionalElement == null)
                        return 0;
                    return src.MenuAdditionalElement.MenuAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<MenuAdditionalElement, RestaurantOrderItemAdditionalElementDTO>().ReverseMap();

        }
    }

    public class RestaurantCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> RoomId { get; set; }      
        public int RestaurantId { get; set; }
        public Nullable<int> TableId { get; set; }
        public double Amount { get; set; }
        public double PreferredCurrencyAmount { get; set; }
        public string RestaurantCurrencySymbol { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }

        public virtual ICollection<RestaurantCartItemDTO> RestaurantCartItems { get; set; }
    }
    public class RestaurantCartInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RestaurantId { get; set; }
        public float Amount { get; set; }
        public double PreferredCurrencyAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual RestaurantInfoDTO Restaurant { get; set; }
        public virtual ICollection<RestaurantCartItemDTO> RestaurantCartItems { get; set; }
    }


    public class RestaurantCartItemDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuItemId { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public int Qty { get; set; }
        public float Total { get; set; }
        public float PreferredCurrencyTotal { get; set; }
        public int ServeLevel { get; set; }
        public string Comment { get; set; }
        public Nullable<System.TimeSpan> PreparationTime { get; set; }
        public string Image { get; set; }
        public virtual ICollection<RestaurantCartItemAdditionalDTO> RestaurantCartItemAdditionals { get; set; }

    }

    public class RestaurantCartItemAdditionalDTO
    {
        public int Menu_Additional_Group_ID { get; set; }
        public string Additional_Group_Name { get; set; }

        public virtual RestaurantCartItemAdditionalElementDTO MenuAdditionalElement { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
    }


    public class RestaurantCartItemAdditionalElementDTO
    {
        public int Id { get; set; }
        public string AdditionalElementName { get; set; }
        public int AdditionalCost { get; set; }

    }
    //create cart
    public class CreateCartDTO
    {
        public ICollection<CreateCartItemDTO> RestaurantCartItems { get; set; }
    }

    public class CreateCartItemDTO
    {
        public int RestaurantMenuItemId { get; set; }
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Menu_Item_Qty", ErrorMessageResourceType = typeof(Resources))]
        public Nullable<int> Qty { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter proper ServeLevel")]
        public Nullable<int> ServeLevel { get; set; }
        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
        public ICollection<UpdateCartItemAdditionalDTO> RestaurantCartItemAdditionals { get; set; }

    }

    //update cart

    public class UpdateCartDTO
    {
        public ICollection<UpdateCartItemDTO> RestaurantCartItems { get; set; }
    }

    public class UpdateCartItemDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id shouldn't be Zero or Negative")]
        public Nullable<int> Id { get; set; }
        public int RestaurantMenuItemId { get; set; }
        public int Qty { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please enter proper ServeLevel")]
        public Nullable<int> ServeLevel { get; set; }
        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }
        public ICollection<UpdateCartItemAdditionalDTO> RestaurantCartItemAdditionals { get; set; }
    }


    public class UpdateCartItemAdditionalDTO
    {
        public int MenuAdditionalElementId { get; set; }

        //[Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Additional_Qty", ErrorMessageResourceType = typeof(Resources))]
        public Nullable<int> Qty { get; set; }
    }



    // Restaurant Orders

    public class RestaurantOrderDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public double OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Order_Item_Count { get; set; }
        public System.DateTime ScheduledDate { get; set; }
        public string ReceiptNumber { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public string Comment { get; set; }
        public Nullable<int> RoomId { get; set; }
        public Nullable<int> CustomerTableNumber { get; set; }
        public virtual RestaurantDetailsDTO Restaurant { get; set; }
        public virtual ICollection<RestaurantOrderItemDTO> RestaurantOrderItems { get; set; }
    }

    public class RestaurantOrderInfoDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public double OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Order_Item_Count { get; set; }
        public System.DateTime ScheduledDate { get; set; }
        public string ReceiptNumber { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public string Comment { get; set; }
        public Nullable<int> RoomId { get; set; }
        public virtual RestaurantInfoDTO Restaurant { get; set; }
        public virtual ICollection<RestaurantOrderItemDTO> RestaurantOrderItems { get; set; }
    }

    public class RestaurantOrderPdfDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public double OrderTotal { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Order_Item_Count { get; set; }
        public System.DateTime ScheduledDate { get; set; }
        public string ReceiptNumber { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public string Comment { get; set; }
        public virtual RestaurantInfoPdfDTO Restaurant { get; set; }
        public virtual ICollection<RestaurantOrderItemDTO> RestaurantOrderItems { get; set; }
    }

    public class RestaurantOrderItemDTO
    {
        public int Id { get; set; }
        public int RestaurantMenuItemId { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public double OfferPrice { get; set; }
        public int Qty { get; set; }
        public Nullable<double> OrderItemTotal { get; set; }
        public int ServeLevel { get; set; }
        public string Comment { get; set; }
        public Nullable<System.TimeSpan> PreparationTime { get; set; }
        public virtual ICollection<RestaurantOrderItemAdditionalDTO> RestaurantOrderItemAdditionals { get; set; }

    }


    public class RestaurantOrderItemAdditionalDTO
    {
        public int Menu_Additional_Group_ID { get; set; }
        public string Additional_Group_Name { get; set; }

        public virtual RestaurantOrderItemAdditionalElementDTO MenuAdditionalElement { get; set; }
        public int Qty { get; set; }
        public Nullable<double> Total { get; set; }
    }

    public class RestaurantOrderItemAdditionalElementDTO
    {
        public int Id { get; set; }
        public string Additional_Element_Name { get; set; }
        public int Additional_Cost { get; set; }

    }

    public class CreateRestaurantTipPayDTO
    {
        public int OrderId { get; set; }
        public Nullable<int> CardId { get; set; }

        [Range(0.1, Double.MaxValue, ErrorMessageResourceName = "Invalid_TipsAmount", ErrorMessageResourceType = typeof(Resources))]
        public double TipAmount { get; set; }
        public string Comment { get; set; }

    }

}