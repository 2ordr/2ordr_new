﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{

    public abstract class CustomerHotelPreferenceBaseDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Hotel_Customisation_Type_ID { get; set; }
    }

    public class CreateHotelPreferenceDTO : CustomerHotelPreferenceBaseDTO
    { }

    public class ModifyHotelPreferenceDTO : CustomerHotelPreferenceBaseDTO
    {
        public int Customer_Hotel_Preference_ID { get; set; }
    }

    public class CustomerHotelPreferenceDTO : CustomerHotelPreferenceBaseDTO
    {
        public int Customer_Hotel_Preference_ID { get; set; }

        public bool Is_Valid { get; set; }
        public System.DateTime CreationDate { get; set; }

        public virtual CustomerDTO Customer { get; set; }
        public virtual HotelCustomisationTypesDTO Hotel_Customisation_Types { get; set; }
    }
}