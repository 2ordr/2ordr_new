﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class CustomerReservedTableBaseDTO
    {
        public int Customer_Table_Reservation_ID { get; set; }

        public int Table_ID { get; set; }

        public int Seat_Reserved { get; set; }
    }

    public class CustomerReservedTableDTO : CustomerReservedTableBaseDTO
    {
        public int Customer_Reserved_Table_ID { get; set; }

        public virtual RestaurantTablesDTO RestaurantTables { get; set; }

    }
}