﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    /// <summary>
    /// Contains all details for Restaurant
    /// </summary>
    public abstract class RestaurantBaseDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public virtual CountryDTO Country { get; set; }
        public virtual CurrenciesDTO Currency { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int CostForTwo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public bool IsFavourite { get; set; }
        public Nullable<TimeSpan> OpeningHours { get; set; }
        public Nullable<TimeSpan> ClosingHours { get; set; }
        public int StarRating { get; set; }
        public int PriceRange { get; set; }

    }

    public class CreateRestaurantDTO : RestaurantBaseDTO
    {
        public DbGeography Location { get; set; }
    }

    public class ModifyRestaurantDTO : RestaurantBaseDTO
    {

        public int Id { get; set; }
        public DbGeography Location { get; set; }
    }

    public class GetRestaurantsDTO : RestaurantBaseDTO
    {
        public int Id { get; set; }
        public System.DateTime CreationDate { get; set; }
    }


    public class RestaurantDTO : RestaurantBaseDTO
    {
        public int Id { get; set; }

        public virtual ICollection<RestCuisinesDTO> RestaurantCuisines { get; set; }
    }
    public class RestaurantGroupDetailDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int CostForTwo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public string IsFavourite { get; set; }
        public Nullable<TimeSpan> OpeningHours { get; set; }
        public Nullable<TimeSpan> ClosingHours { get; set; }
        public int StarRating { get; set; }
        public int PriceRange { get; set; }

    }
    public class RestaurantGroupByUserRatingDTO
    {
        public int UserRatingAvg { get; set; }
        public ICollection<RestaurantDTO> Restaurant { get; set; }
    }

    public class RestaurantGroupByPriceRangeDTO
    {
        public int PriceRange { get; set; }
        public ICollection<RestaurantDTO> Restaurant { get; set; }
    }
    public class RestaurantGroupByRadiusDTO
    {
        public int Distance { get; set; }
        public ICollection<RestaurantDTO> Restaurant { get; set; }
    }
}