﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class LaundryReviewsDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int LaundryId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }

    public class CreateLaundryReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_LaundryId", ErrorMessageResourceType = typeof(Resources))]
        public int LaundryId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}