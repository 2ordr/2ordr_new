﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class SPAServiceBookingBaseDTO
    {

    }

    public class SPAServiceBookingDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int SPAServiceId { get; set; }
        public int StatusId { get; set; }
        public float Amount { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual ICollection<SPAServiceBookingDetailDTO> SPAServiceBookingDetails { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }
    }

    public class CreateSPAServiceBookingDTO
    {
        public DateTime BookingDate { get; set; }
        public virtual ICollection<CreateSPAServiceBookingDetailDTO> SPAServiceBookingDetails { get; set; }
    }

    public class UpdateSPAServiceBookingDTO
    {
        public int Id { get; set; }
        public DateTime BookingDate { get; set; }
        public virtual ICollection<CreateSPAServiceBookingDetailDTO> SPAServiceBookingDetails { get; set; }
    }

    public class CreateSPAServiceBookingDetailDTO
    {
        public int SPAServiceDetailId { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Qty", ErrorMessageResourceType = typeof(Resources))]
        public int Quantity { get; set; }
    }

    public class SPAServiceBookingDetailDTO
    {
        public int SPAServiceBookingId { get; set; }
        public int SPAServiceDetailId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Amount { get; set; }
        //public virtual SPAServiceDetailDTO SPAServiceDetail { get; set; }
    }
}