﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class HotelLaundryBaseDTO
    {

        public int Hotel_ID { get; set; }
        public string Laundry_Name { get; set; }
        public string Laundry_Description { get; set; }
        public double Laundry_Hours { get; set; }
        public TimeSpan Laundry_Available_From { get; set; }
        public TimeSpan Laundry_Available_To { get; set; }
        public string Laundry_Photo { get; set; }
        public bool IsActive { get; set; }

    }

    public class HotelLaundryDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
    public class LaundryServiceDetailsDTO
    {
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
    public class LaundryDetailsDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public virtual GarmentsDTO Garments { get; set; }
        public virtual ICollection<LaundryDetailAdditionalDTO> LaundryDetailAdditional { get; set; }
    }

    public class LaundryDetailAdditionalDTO
    {
        public virtual LaundryAdditionalGroupDTO LaundryAdditionalGroup { get; set; }
    }

    public class LaundryAdditionalGroupDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> MinSelected { get; set; }
        public Nullable<int> MaxSelected { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<LaundryAdditionalElementDTO> LaundryAdditionalElement { get; set; }
    }

    public class LaundryAdditionalElementDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public bool IsActive { get; set; }              
    }

    public class LaundryBookingDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int LaundryId { get; set; }
        public int StatusId { get; set; }
        public float Amount { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual ICollection<LaundryBookingDetailDTO> LaundryBookingDetails { get; set; }
        public virtual HotelDetailsDTO Hotel { get; set; }
    }
    public class LaundryBookingDetailDTO
    {
        public int LaundryBookingId { get; set; }
        public int LaundryDetailsId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Amount { get; set; }
    }

    public class CreateLaundryBookingDTO
    {
        public DateTime BookingDate { get; set; }
        public virtual ICollection<CreateLaundryBookingDetailsDTO> LaundryBookingDetails { get; set; }
    }

    public class CreateLaundryBookingDetailsDTO
    {
        public int LaundryDetailsId { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Qty", ErrorMessageResourceType = typeof(Resources))]
        public int Quantity { get; set; }
    }
    public class UpdateLaundryBookingDTO
    {
        public int Id { get; set; }
        public DateTime BookingDate { get; set; }
        public virtual ICollection<CreateLaundryBookingDetailsDTO> LaundryBookingDetails { get; set; }
    }

    public class GarmentsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }


}