﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public abstract class HotelReviewsBaseDTO
    {
        public int Customer_Hotel_Booking_ID { get; set; }
        public int Score { get; set; }
    }
    public class CreateHotelReviewsDTO 
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_HotelId", ErrorMessageResourceType = typeof(Resources))]
        public int HotelId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        [MaxLength(1000, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

    }
    public class HotelReviewsDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }
}