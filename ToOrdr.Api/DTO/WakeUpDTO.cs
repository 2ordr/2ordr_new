﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Helpers;

namespace ToOrdr.Api.DTO
{
    public class WakeUpDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public string Method { get; set; }
        public System.DateTime ScheduledDate { get; set; }
        public bool IsRepeated { get; set; }
        public Nullable<System.TimeSpan> Interval { get; set; }
        public string Comment { get; set; }
        public bool IsCompleted { get; set; }
        public virtual WakeUpRoomDTO Room { get; set; }
    }

    public class CreateWakeUpDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Room id shouldn't be zero or negative")]
        public int RoomId { get; set; }

        [Required(ErrorMessage = "Please provide valid method for wakeup call")]
        public int Method { get; set; }

        [Required(ErrorMessage = "Please provide valid Scheduled Datetime for wakeup call")]
        [ValidScheduledDateTime()]
        public System.DateTime ScheduledDate { get; set; }
        public Nullable<bool> IsRepeated { get; set; }
        public string Interval { get; set; }
        public string Comment { get; set; }
    }
}