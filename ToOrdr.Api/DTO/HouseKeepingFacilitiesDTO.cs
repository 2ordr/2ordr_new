﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public class HouseKeepingFacilitiesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<HouseKeepingFacilityDetailDTO> HouseKeepingFacilityDetails { get; set; }
        public virtual ICollection<HouseKeepingfacilitiesAdditionalDTO> HouseKeepingfacilitiesAdditionals { get; set; }
    }

    public class HouseKeepingFacilityDetailDTO
    {
        public int Id { get; set; }
        public int HouseKeepingFacilityId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<double> Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public bool IsActive { get; set; }
    }

    public class HouseKeepingfacilitiesAdditionalDTO
    {
        public int HouseKeepingAdditionalGroup_ID { get; set; }
        public string Additional_Group_Name { get; set; }
        public virtual HousekeepingAdditionalGroupDTO HousekeepingAdditionalGroup { get; set; }
        
    }

    public class HousekeepingAdditionalGroupDTO
    {
        public Nullable<int> MinSelected { get; set; }
        public Nullable<int> MaxSelected { get; set; }
        public virtual ICollection<HousekeepingAdditionalElementDTO> HousekeepingAdditionalElements { get; set; }
    }

    public class HousekeepingAdditionalElementDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<double> Price { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public bool IsActive { get; set; }
    }

    public class GetHouseKeepingFacilitiesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<double> FullCleanPrice { get; set; }
        public Nullable<double> SomeCleanPrice { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    }

}