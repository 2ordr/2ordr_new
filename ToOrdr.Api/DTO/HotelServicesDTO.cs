﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrdr.Api.DTO
{
    public abstract class HotelServiceBaseDTO
    {
       
    }
    public class HotelServicesDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int ServiceId { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.TimeSpan> OpeningHours { get; set; }
        public Nullable<System.TimeSpan> ClosingHours { get; set; }
        public virtual ServicesDTO Services { get; set; }
    }

    public class ServicesDTO
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
    }
}