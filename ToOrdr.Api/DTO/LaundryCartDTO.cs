﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{

    public class LaundryCartProfile : AutoMapper.Profile
    {
        public LaundryCartProfile()
        {
            var api_path = ConfigurationManager.AppSettings["APIURL"];
            CreateMap<LaundryCart, LaundryCartDTO>()
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.LaundryCartItems.Any())
                        return 0; ;

                    return src.LaundryCartItems.Count();

                }))
                .ForMember(dest => dest.HotelCurrencySymbol, opt => opt.ResolveUsing(src =>
                {
                    if (src.Room.Hotel.Currency != null)
                    {
                        return src.Room.Hotel.Currency.Symbol;
                    }
                    return null;
                }))
                .ReverseMap();

            CreateMap<LaundryCart, ActiveLaundryCartDTO>()
                .ForMember(dest => dest.Hotel, opt => opt.ResolveUsing(src => src.Room.Hotel))
                .ForMember(dest => dest.Cart_Item_Count, opt => opt.ResolveUsing(src =>
                {
                    if (src == null || !src.LaundryCartItems.Any())
                        return 0; ;

                    return src.LaundryCartItems.Count();

                }))
                .ReverseMap();
            CreateMap<LaundryCartItems, LaundryCartItemsDTO>().ReverseMap();

            CreateMap<LaundryDetail, LaundryDetailsInCartDTO>()
            .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
            {
                if (!string.IsNullOrWhiteSpace(src.Image))
                    return api_path + "Images/Services/LaundryService/" + src.LaundryId.ToString() + "/LaundryDetails/" + src.Id.ToString() + "/" + src.Image;
                else
                    return null;
            }))
            .ReverseMap();

            CreateMap<LaundryCartItemsAdditional, LaundryCartItemsAdditionalDTO>()
                .ForMember(dest => dest.Laundry_Additional_Group_Name, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryAdditionalElement == null)
                        return null;
                    return src.LaundryAdditionalElement.LaundryAdditionalGroup.Name;
                }))
                .ForMember(dest => dest.Laundry_Additional_Group_ID, opt => opt.ResolveUsing(src =>
                {
                    if (src.LaundryAdditionalElement == null)
                        return 0;
                    return src.LaundryAdditionalElement.LaundryAdditionalGroupId;
                }))
                .ReverseMap();

            CreateMap<LaundryCartItemImages, LaundryCartItemImagesDTO>()
                  .ForMember(dest => dest.Image, opt => opt.ResolveUsing(src =>
                  {
                      if (!string.IsNullOrWhiteSpace(src.Image))
                          return api_path + "Images/Customers/" + src.LaundryCartItems.LaundryCart.CustomerId.ToString() + "/Room/" + src.LaundryCartItems.LaundryCart.RoomId.ToString() + "/Laundry/" + src.LaundryCartItems.LaundryDetailId.ToString() + "/" + src.Image;
                      else
                          return null;
                  }))
                .ReverseMap();

            CreateMap<CreateLaundryCartDTO, LaundryCart>().ReverseMap();
            CreateMap<CreateLaundryCartItemsDTO, LaundryCartItems>().ReverseMap();
            CreateMap<CreateLaundryCartItemsAdditionalDTO, LaundryCartItemsAdditional>().ReverseMap();

            CreateMap<UpdateLaundryCartDTO, LaundryCart>().ReverseMap();
            CreateMap<UpdateLaundryCartItemsDTO, LaundryCartItems>().ReverseMap();
        }
    }

    public class LaundryCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public Nullable<double> Total { get; set; }
        public string HotelCurrencySymbol { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<LaundryCartItemsDTO> LaundryCartItems { get; set; }
    }
    public class ActiveLaundryCartDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public Nullable<double> Total { get; set; }
        public double PreferredCurrencyTotal { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public int Cart_Item_Count { get; set; }
        public virtual ICollection<LaundryCartItemsDTO> LaundryCartItems { get; set; }
        public virtual HotelDetailsWithCurrencySymbolDTO Hotel { get; set; }
    }

    public class LaundryCartItemsDTO
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Comment { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<double> PreferredCurrencyTotal { get; set; }
        public Nullable<System.DateTime> ScheduledDate { get; set; }
        public virtual LaundryDetailsInCartDTO LaundryDetail { get; set; }
        public virtual ICollection<LaundryCartItemsAdditionalDTO> LaundryCartItemsAdditional { get; set; }
        public virtual ICollection<LaundryCartItemImagesDTO> LaundryCartItemImages { get; set; }
    }
    public class LaundryDetailsInCartDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double PreferredCurrencyPrice { get; set; }
        public string Image { get; set; }
        public virtual GarmentsDTO Garments { get; set; }
    }
    public class LaundryCartItemsAdditionalDTO
    {
        public int Laundry_Additional_Group_ID { get; set; }
        public string Laundry_Additional_Group_Name { get; set; }
        public virtual LaundryAdditionalElementDTO LaundryAdditionalElement { get; set; }
        public int Quantity { get; set; }
        public Nullable<double> Total { get; set; }
    }
    public class LaundryCartItemImagesDTO
    {
        public int Id { get; set; }
        public int LaundryCartItemId { get; set; }
        public string Image { get; set; }
    }


    //create cart
    public class CreateLaundryCartDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Room id shouldn't be zero or negative")]
        public int RoomId { get; set; }
        public ICollection<CreateLaundryCartItemsDTO> LaundryCartItems { get; set; }
    }

    public class CreateLaundryCartItemsDTO
    {
        public int LaundryDetailId { get; set; }
        //[Range(1, int.MaxValue, ErrorMessage = "Laundry detail Qty shouldn't be zero or negative")]
        public int Quantity { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        public Nullable<System.DateTime> ScheduledDate { get; set; }
        public ICollection<CreateLaundryCartItemsAdditionalDTO> LaundryCartItemsAdditional { get; set; }
    }

    public class CreateLaundryCartItemsAdditionalDTO
    {
        public int LaundryAdditionalElementId { get; set; }

        //[Range(1, int.MaxValue, ErrorMessageResourceName = "Zero_Additional_Qty", ErrorMessageResourceType = typeof(Resources))]
        public Nullable<int> Quantity { get; set; }
    }

    //create cart
    public class UpdateLaundryCartDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Room id shouldn't be zero or negative")]
        public int RoomId { get; set; }

        public ICollection<UpdateLaundryCartItemsDTO> LaundryCartItems { get; set; }
    }

    public class UpdateLaundryCartItemsDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id shouldn't be Zero or Negative")]
        public Nullable<int> Id { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Laundry Detail Id shouldn't be zero or negative")]
        public int LaundryDetailId { get; set; }

        public int Quantity { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "Max_CommentLength", ErrorMessageResourceType = typeof(Resources))]
        public string Comment { get; set; }

        public Nullable<System.DateTime> ScheduledDate { get; set; }

        public ICollection<CreateLaundryCartItemsAdditionalDTO> LaundryCartItemsAdditional { get; set; }
    }

}