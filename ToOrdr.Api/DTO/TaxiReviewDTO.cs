﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{
    public class TaxiReviewDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TaxiId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public virtual CustomerInfoDTO Customer { get; set; }
    }
    public class CreateTaxiReviewDTO
    {
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_TaxiId", ErrorMessageResourceType = typeof(Resources))]
        public int TaxiId { get; set; }
        [Range(1, 10, ErrorMessageResourceName = "Valid_ReviewScore", ErrorMessageResourceType = typeof(Resources))]
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}