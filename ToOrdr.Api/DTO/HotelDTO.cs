﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Api.DTO
{



    /// <summary>
    /// Contains all details for Hotel
    /// </summary>
    public abstract class HotelBaseDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public virtual CountryDTO Country { get; set; }
        public virtual CurrenciesDTO Currency { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string HouseKeepingTelephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int CostForTwo { get; set; }
        public bool Status { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public Nullable<TimeSpan> CheckInTime { get; set; }
        public Nullable<TimeSpan> CheckOutTime { get; set; }
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public int StarRating { get; set; }
        public int PriceRange { get; set; }
    }

    public class CreateHotelDTO : HotelBaseDTO
    {
        public DbGeography Location { get; set; }
    }

    public class ModifyHotelDTO : HotelBaseDTO
    {

        public int Id { get; set; }
        public DbGeography Location { get; set; }
    }

    public class GetHotelDTO : HotelBaseDTO
    {
        public int Id { get; set; }
        public System.DateTime CreationDate { get; set; }
    }

    public class HotelDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public virtual CountryDTO Country { get; set; }
        public virtual CurrenciesDTO Currency { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string HouseKeepingTelephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int CostForTwo { get; set; }
        public bool Status { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public Nullable<TimeSpan> CheckInTime { get; set; }
        public Nullable<TimeSpan> CheckOutTime { get; set; }
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public int StarRating { get; set; }
        public int PriceRange { get; set; }
        public bool IsFavourite { get; set; }
        public ICollection<HotelServicesDTO> HotelServices { get; set; }
    }
    public class HotelDetailWithExcursionPriceDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public virtual CountryDTO Country { get; set; }
        public virtual CurrenciesDTO Currency { get; set; }
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string HouseKeepingTelephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int CostForTwo { get; set; }
        public bool Status { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public Nullable<TimeSpan> CheckInTime { get; set; }
        public Nullable<TimeSpan> CheckOutTime { get; set; }
        public double Rating_Average { get; set; }
        public int Ratings_Count { get; set; }
        public int StarRating { get; set; }
        public int PriceRange { get; set; }
        public bool IsFavourite { get; set; }
        public double Excursion_Price_Min { get; set; }
        public double Excursion_Price_Max { get; set; }
        public ICollection<HotelServicesDTO> HotelServices { get; set; }
    }
    public class HotelGroupByStarRatingDTO
    {
        public int StarRating { get; set; }
        public ICollection<HotelDTO> Hotel { get; set; }
    }

    public class HotelGroupByPriceRangeDTO
    {
        public int PriceRange { get; set; }
        public ICollection<HotelDTO> Hotel { get; set; }
    }

    public class HotelDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoImage { get; set; }
        public string BackgroundImage { get; set; }
    }
    public class HotelDetailsWithCurrencySymbolDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoImage { get; set; }
        public string BackgroundImage { get; set; }
        public string HotelCurrencySymbol { get; set; }
    }

    public class HotelGroupByRadiusDTO
    {
        public int Distance { get; set; }
        public ICollection<HotelDTO> Hotel { get; set; }
    }
    public class CreateHotelPayDTO
    {
        public int OrderId { get; set; }
        public string Currency { get; set; }
        public Nullable<int> CardId { get; set; }
    }

    public class CreateHotelTipPayDTO
    {
        public int OrderId { get; set; }
        public Nullable<int> CardId { get; set; }
        [Range(0.1, Double.MaxValue, ErrorMessageResourceName = "Invalid_TipsAmount", ErrorMessageResourceType = typeof(Resources))]
        public double TipAmount { get; set; }
        public string Comment { get; set; }

    }

    public class HotelNearByPlacesDTO
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public string Place { get; set; }
        public string Distance { get; set; }
        public string Image { get; set; }
    }
    public class HotelRestaurantDTO
    {
        public virtual RestaurantDTO Restaurant { get; set; }
    }

    public class HotelVirtualRestaurantsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoImage { get; set; }
        public string BackgroundImage { get; set; }
        public virtual ICollection<HotelRestaurantDTO> HotelRestaurants { get; set; }
    }

    public class HotelHouseKeepingInfoDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string HouseKeepingContact { get; set; }
        public string Image { get; set; }
    }

    public class AllHotelServicesOffersDTO
    {
        public string OfferType { get; set; }
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public int OfferId { get; set; }
        public int OfferDetailsId { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public string OfferImage { get; set; }
        public bool IsActive { get; set; }
    }

    public class HotelPdfDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
    }
}