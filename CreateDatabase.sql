USE [2ORDR]
GO
/****** Object:  Table [dbo].[Restaurant Details]    Script Date: 07/14/2017 15:46:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* Tabels Restaurants*/

CREATE TABLE [dbo].[Restaurant_Details](
	[Restaurant_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Name] [nvarchar](500) NOT NULL,
	[Restaurant_Description] [nvarchar](3000) NOT NULL,
	[Address_Street] [nvarchar](100) NOT NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NOT NULL,
	[Address_Region] [nvarchar](100) NOT NULL,
	[Address_Country] [nvarchar](50) NOT NULL,
	[Address_Code] [nvarchar](100) NOT NULL,
	[Contact_Telephone] [nvarchar](15) NOT NULL,
	[Contact_Fax] [nvarchar](15) NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Contact_Website] [nvarchar](100) NULL,
	[Star_Rating] [int] NOT NULL,
	[Status_Partnered] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Restaurant_Details] PRIMARY KEY CLUSTERED 
(
	[Restaurant_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Restaurant_Types](
	[Restaurant_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Type] [nvarchar](100) NOT NULL,
	[Valid_Type] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Restaurant_Types] PRIMARY KEY CLUSTERED 
(
	[Restaurant_Type_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurants](
	[Restaurants_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Restaurant_ID] [int] NOT NULL,
	[Restaurant_Type_ID] [int] NOT NULL,	
	[Restaurant_Valid] [bit] NOT NULL,	
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [FK_dbo.Restaurants_dbo.Restaurant_Details_Restaurant_ID] FOREIGN KEY  
(
	[Restaurant_ID] 
)REFERENCES [dbo].[Restaurant_Details]([Restaurant_ID]),
CONSTRAINT [FK_dbo.Restaurants_dbo.Restaurant_Types_Restaurant_Type_ID] FOREIGN KEY  
(
	[Restaurant_Type_ID] 
)REFERENCES [dbo].[Restaurant_Types]([Restaurant_Type_ID]) 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurant_Menu_Types](
	[Restaurant_Menu_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Type_Description]  [nvarchar](100) NOT NULL,	
	[Restaurant_Menu_Type_Valid] [bit] NOT NULL,	
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Restaurant_Menu_Types] PRIMARY KEY CLUSTERED 
(
	[Restaurant_Menu_Type_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurant_Menu_Items](
	[Restaurant_Menu_Item_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_Name] [nvarchar](100) NOT NULL,	
	[Restaurant_Menu_Item_Description] [nvarchar](100) NULL,	
	[Restaurant_Menu_Item_Image] [varchar](500) NOT NULL,		
	[Restaurant_Menu_Item_Valid] [bit] NOT NULL,	
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Restaurant_Menu_Items] PRIMARY KEY CLUSTERED 
(
	[Restaurant_Menu_Item_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurant_Menu_Items_Customisation](
	[Restaurant_Menu_Items_Customisation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Items_Customisation_Name] [nvarchar](100) NOT NULL,
	[Restaurant_Menu_Items_Customisation_Valid] [bit] NOT NULL,	
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Restaurant_Menu_Items_Customisation] PRIMARY KEY CLUSTERED 
(
	[Restaurant_Menu_Items_Customisation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurant_Menu_Items_Customisations](
	[Restaurant_Menu_Items_Customisations_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisation_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisations_Icon] [nvarchar](500) NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
CONSTRAINT [FK_dbo.Restaurant_Menu_Items_Customisations_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Item_ID] 
)REFERENCES [dbo].[Restaurant_Menu_Items]([Restaurant_Menu_Item_ID]) 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Restaurant_Menus](
	[Restaurant_Menus_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Restaurants_ID] [int] NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Restaurant_Menu_Type_ID] [int] NOT NULL,
	[Restaurant_Menus_Valid] [bit] NOT NULL, 
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
	CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurants_Restaurants_ID] FOREIGN KEY  
(
	[Restaurants_ID] 
)REFERENCES [dbo].[Restaurants]([Restaurants_ID]),

 CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Item_ID] 
)REFERENCES [dbo].[Restaurant_Menu_Items]([Restaurant_Menu_Item_ID]),

CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Types_Restaurant_Menu_Type_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Type_ID] 
)REFERENCES [dbo].[Restaurant_Menu_Types]([Restaurant_Menu_Type_ID]) 
) ON [PRIMARY]
GO

/*Tabels Hotels*/

CREATE TABLE [dbo].[Hotel_Details](
	[Hotel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Name] [nvarchar](500) NOT NULL,
	[Hotel_Description] [nvarchar](3000) NOT NULL,
	[Address_Street] [nvarchar](100) NOT NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NOT NULL,
	[Address_Region] [nvarchar](100) NOT NULL,
	[Address_Country] [nvarchar](50) NOT NULL,
	[Address_Code] [nvarchar](100) NOT NULL,
	[Contact_Telephone] [nvarchar](15) NOT NULL,
	[Contact_Fax] [nvarchar](15) NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Contact_Website] [nvarchar](100) NULL,
	[Star_Rating] [int] NOT NULL,
	[Status_Partnered] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Types](
	[Hotel_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Type] [nvarchar](100) NOT NULL,
	[Valid_Type] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Type_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotels](
	[Hotels_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Hotel_ID] [int] NOT NULL,
	[Hotel_Type_ID] [int] NOT NULL,	
	[Hotel_Valid] [bit] NOT NULL,	
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY  
(
	[Hotel_ID] 
)REFERENCES [dbo].[Hotel_Details]([Hotel_ID]),

CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Types_Hotel_Type_ID] FOREIGN KEY  
(
	[Hotel_Type_ID] 
)REFERENCES [dbo].[Hotel_Types]([Hotel_Type_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Rooms](
	[Room_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Room_Description] [int] NOT NULL,
	[Hotels_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
CONSTRAINT [FK_dbo.Hotel_Rooms_dbo.Hotels_Hotels_ID] FOREIGN KEY  
(
	[Hotels_ID] 
)REFERENCES [dbo].[Hotels]([Hotels_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Customisation_Types](
	[Hotel_Customisation_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Customisation_Type_Description] [nvarchar](500) NOT NULL,
	[Hotel_Customisation_Type_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Customisation_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Customisation_Type_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Customisations](
	[Hotel_Customisation_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Hotel_Customisation_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
  CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY  
(
	[Hotel_Customisation_Type_ID] 
)REFERENCES [dbo].[Hotel_Customisation_Types]([Hotel_Customisation_Type_ID]),

CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY  
(
	[Room_ID] 
)REFERENCES [dbo].[Hotel_Rooms]([Room_ID])
) ON [PRIMARY]
GO

/*Tabels Customers*/

CREATE TABLE [dbo].[Customer_Details](
	[Customer_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ForeName] [nvarchar](50) NOT NULL,
	[Customer_SurName] [nvarchar](50) NOT NULL,
	[Customer_DOB] [date] NOT NULL,
	[Address_Street] [nvarchar](100) NOT NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NOT NULL,
	[Address_Region] [nvarchar](100) NOT NULL,
	[Address_Country] [nvarchar](50) NOT NULL,
	[Address_Code] [nvarchar](100) NOT NULL,
	[Contact_Telephone] [nvarchar](15) NOT NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Status_Active] [bit] NOT NULL,
	[ResetPasswordCode] [nvarchar](max) NULL,
	[ActivationCode] [nvarchar](max) NULL,
	[LastUpdated] [datetime] NULL default GETDATE(),
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Customer_Details] PRIMARY KEY CLUSTERED 
(
	[Customer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Restaurant_Orders](
	[Customer_Restaurant_Order_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_ID] [int] NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[Customer_Table_Number] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID]),

 CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Restaurant_Details_Restaurant_ID] FOREIGN KEY  
(
	[Restaurant_ID] 
)REFERENCES [dbo].[Restaurant_Details]([Restaurant_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Restaurant_Order_Items](
	[Customer_restaurant_Order_Item_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_Restaurant_Order_ID] [int] NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Items_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID] FOREIGN KEY  
(
	[Customer_Restaurant_Order_ID] 
)REFERENCES [dbo].[Customer_Restaurant_Orders]([Customer_Restaurant_Order_ID]),

 CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Items_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Item_ID] 
)REFERENCES [dbo].[Restaurant_Menu_Items]([Restaurant_Menu_Item_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Restaurant_Order_Item_customisation](
	[Customer_Restaurant_Order_Item_customisation_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_restaurant_Order_Item_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisations_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
	
CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Item_customisation_dbo.Customer_Restaurant_Order_Items_Customer_restaurant_Order_Item_ID] FOREIGN KEY  
(
	[Customer_restaurant_Order_Item_ID] 
)REFERENCES [dbo].[Customer_Restaurant_Order_Items]([Customer_restaurant_Order_Item_ID]),

CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Item_customisation_dbo.Restaurant_Menu_Items_Customisations_Customisations_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Items_Customisations_ID] 
)REFERENCES [dbo].[Restaurant_Menu_Items_Customisations]([Restaurant_Menu_Items_Customisations_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Hotel_Bookings](
	[Customer_Hotel_Booking_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_ID] [int] NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID]),

 CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY  
(
	[Hotel_ID] 
)REFERENCES [dbo].[Hotel_Details]([Hotel_ID]),

 CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY  
(
	[Room_ID] 
)REFERENCES [dbo].[Hotel_Rooms]([Room_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Hotel_Booking_Customisations](
	[Customer_Hotel_Booking_Customisations_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Hotel_Customisation_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 
 CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY  
(
	[Customer_Hotel_Booking_ID] 
)REFERENCES [dbo].[Customer_Hotel_Bookings]([Customer_Hotel_Booking_ID]),

CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Hotel_Customisations_Hotel_Customisation_ID] FOREIGN KEY  
(
	[Hotel_Customisation_ID] 
)REFERENCES [dbo].[Hotel_Customisations]([Hotel_Customisation_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Hotel_Preferences](
	[Customer_Hotel_Preference_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_ID] [int] NOT NULL,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Is_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
	
 CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID]),

 CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY  
(
	[Hotel_Customisation_Type_ID] 
)REFERENCES [dbo].[Hotel_Customisation_Types]([Hotel_Customisation_Type_ID])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Restaurant_Preferences](
	[Customer_Restaurant_Preference_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisation_ID] [int] NOT NULL,
	[Is_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [FK_dbo.Customer_Restaurant_Preferences_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID]),

 CONSTRAINT [FK_dbo.Customer_Restaurant_Preferences_dbo.Restaurant_Menu_Items_Customisation_Restaurant_Menu_Items_Customisation_ID] FOREIGN KEY  
(
	[Restaurant_Menu_Items_Customisation_ID] 
)
REFERENCES [dbo].[Restaurant_Menu_Items_Customisation]([Restaurant_Menu_Items_Customisation_ID])
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Role](
	[Role_Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[Role_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Customer_Role](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_Id] [int] NOT NULL,
	[Role_Id] [int] NOT NULL,
CONSTRAINT [FK_dbo.Customer_Role_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID]),

 CONSTRAINT [FK_dbo.Customer_Role_dbo.Role_Role_Id] FOREIGN KEY  
(
	[Role_Id] 
)REFERENCES [dbo].[Role]([Role_Id])
) ON [PRIMARY]

GO

/*Hotel_Customer_Reviews*/

CREATE TABLE [dbo].[Hotel_Customer_Reviews](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
CONSTRAINT [FK_dbo.Hotel_Customer_Reviews_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY  
(
	[Customer_Hotel_Booking_ID] 
)REFERENCES [dbo].[Customer_Hotel_Bookings]([Customer_Hotel_Booking_ID])
) ON [PRIMARY]

GO

/*Restaurant_Customer_Reviews*/

CREATE TABLE [dbo].[Restaurant_Customer_Reviews](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Customer_Restaurant_Order_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
  CONSTRAINT [FK_dbo.Restaurant_Customer_Reviews_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID] FOREIGN KEY  
(
	[Customer_Restaurant_Order_ID] 
)REFERENCES [dbo].[Customer_Restaurant_Orders]([Customer_Restaurant_Order_ID]) 
) ON [PRIMARY]
GO



/* Restaurants View */
Create view Restaurants_VW As
select 
	RD.Restaurant_ID,
	RD.Restaurant_Name,
	RD.Restaurant_Description,
	RD.Contact_Telephone,
	RD.Star_Rating,
	Review_Score,
	Restaurant_Type

from dbo.Restaurant_Details RD
	inner join dbo.Customer_Restaurant_Orders CRO on RD.Restaurant_ID=CRO.Restaurant_ID
	inner join dbo.Restaurant_Customer_Reviews RCR on CRO.Customer_Restaurant_Order_ID=RCR.Customer_Restaurant_Order_ID
	inner join dbo.Restaurants R on RD.Restaurant_ID=R.Restaurant_ID
	inner join dbo.Restaurant_Types RT on R.Restaurant_Type_ID=RT.Restaurant_Type_ID
GO	


/*Restaurant Menus View*/
Create view Restaurant_Menus_VW as
select 
	RD.Restaurant_ID,
	RMT.Restaurant_Menu_Type_ID,
	RMT.Restaurant_Menu_Type_Description,
	RMI.Restaurant_Menu_Item_ID,
	RMI.Restaurant_Menu_Item_Name,
	RMI.Restaurant_Menu_Item_Description,
	RMI.Restaurant_Menu_Item_Image

from dbo.Restaurant_Details RD
	inner join dbo.Restaurant_Menus RM on RD.Restaurant_ID=RM.Restaurants_ID
	inner join dbo.Restaurant_Menu_Types RMT on RM.Restaurant_Menu_Type_ID=RMT.Restaurant_Menu_Type_ID
	inner join dbo.Restaurant_Menu_Items RMI on RM.Restaurant_Menu_Item_ID=RMI.Restaurant_Menu_Item_ID
	GO
	
/*Restaurants Orders Views*/
create view Restaurant_Orders_VW as
select 
	RD.Restaurant_ID,
	RMT.Restaurant_Menu_Type_ID,
	RMT.Restaurant_Menu_Type_Description,
	RMI.Restaurant_Menu_Item_ID,
	RMI.Restaurant_Menu_Item_Name,
	RMI.Restaurant_Menu_Item_Description,
	CRO.Customer_Restaurant_Order_ID,
	CROI.Restaurant_Menu_Item_ID as CROI_Restaurant_Menu_Item_ID ,/*We are already fetching this from Restaurant_Menu_Items */
	RMIC.Restaurant_Menu_Items_Customisation_Name
from dbo.Restaurant_Details RD
	inner join dbo.Restaurant_Menus RM on RD.Restaurant_ID=RM.Restaurants_ID
	inner join dbo.Restaurant_Menu_Types RMT on RM.Restaurant_Menu_Type_ID=RMT.Restaurant_Menu_Type_ID
	inner join dbo.Restaurant_Menu_Items RMI on RM.Restaurant_Menu_Item_ID=RMI.Restaurant_Menu_Item_ID
	inner join dbo.Customer_Restaurant_Orders CRO on RM.Restaurants_ID=CRO.Restaurant_ID
	inner join dbo.Customer_Restaurant_Order_Items CROI on CRO.Customer_Restaurant_Order_ID=CROI.Customer_Restaurant_Order_ID
	inner join dbo.Customer_Restaurant_Order_Item_customisation CROIC on CROI.Customer_restaurant_Order_Item_ID=CROIC.Customer_restaurant_Order_Item_ID 
	inner join dbo.Restaurant_Menu_Items_Customisation RMIC on CROIC.Restaurant_Menu_Items_Customisations_ID=RMIC.Restaurant_Menu_Items_Customisation_ID
	GO
	
	/*Hotels View*/
Create view Hotels_VW AS
select 
	H.Hotels_ID,
	HD.Hotel_Name,
	HD.Hotel_Description,
	HD.Contact_Telephone,
	HD.Star_Rating,
	HCR.Review_Score,
	HT.Hotel_Type
	
From Hotel_Details HD
	inner join dbo.Hotels H on HD.Hotel_ID=H.Hotel_ID
	inner join dbo.Hotel_Types HT on H.Hotel_Type_ID=HT.Hotel_Type_ID
	inner join dbo.Customer_Hotel_Bookings CHB on H.Hotel_ID=CHB.Hotel_ID
	inner join dbo.Hotel_Customer_Reviews HCR on CHB.Customer_Hotel_Booking_ID=HCR.Customer_Hotel_Booking_ID
	GO
	
	/*Hotels Room Views*/
Create view Hotels_Room_VW AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID
GO

/*Hotel Bookings View*/
Create view Hotel_Bookings_VW AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description,
	CHB.Customer_Hotel_Booking_ID,
	HCT.Hotel_Customisation_Type_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID
inner join dbo.Customer_Hotel_Bookings CHB on H.Hotel_ID=CHB.Hotel_ID
inner join dbo.Hotel_Customisations HC on HR.Room_ID=HC.Room_ID
inner join dbo.Hotel_Customisation_Types HCT on HC.Hotel_Customisation_Type_ID=HCT.Hotel_Customisation_Type_ID
GO

/*Restaurant Customisations Views*/
Create view Restaurant_Customisations_VW As
select 
	RM.Restaurant_Menus_ID,
	RMI.Restaurant_Menu_Item_Name,
	RMI.Restaurant_Menu_Item_Description,
	RMIC.Restaurant_Menu_Items_Customisation_Name
	
From Restaurant_Menus RM
	inner join dbo.Restaurant_Menu_Items RMI on RM.Restaurant_Menu_Item_ID=RMI.Restaurant_Menu_Item_ID
	inner join dbo.Restaurant_Menu_Items_Customisations RMICs on RMI.Restaurant_Menu_Item_ID=RMICs.Restaurant_Menu_Item_ID
	inner join dbo.Restaurant_Menu_Items_Customisation RMIC on RMICs.Restaurant_Menu_Items_Customisation_ID=RMIC.Restaurant_Menu_Items_Customisation_ID
GO

/*Hotel Customisations view */	
Create view Hotel_Customisations_VW  As
select 
	HC.Hotel_Customisation_ID,
	HR.Room_Description,
	HCT.Hotel_Customisation_Type_Description,
	HC.Hotel_Customisation_Type_ID
from dbo.Hotel_Customisations HC
	inner join dbo.Hotel_Rooms HR on HC.Room_ID=HR.Room_ID
	inner join dbo.Hotel_Customisation_Types HCT on HC.Hotel_Customisation_Type_ID=HCT.Hotel_Customisation_Type_ID
GO
