﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="SelectPremises.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Web.Models
{
    /// <summary>
    /// Class SelectPremises.
    /// </summary>
    public class SelectPremises
    {
        /// <summary>
        /// Gets or sets the restaurants.
        /// </summary>
        /// <value>The restaurants.</value>
        public IEnumerable<Restaurant> Restaurants { get; set; }
        /// <summary>
        /// Gets or sets the hotels.
        /// </summary>
        /// <value>The hotels.</value>
        public IEnumerable<Hotel> Hotels { get; set; }
    }
}