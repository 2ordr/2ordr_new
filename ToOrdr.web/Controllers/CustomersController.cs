﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-17-2019
// ***********************************************************************
// <copyright file="CustomersController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Core.Util;
using ToOrdr.Localization;
using ToOrdr.Web.Models;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class CustomersController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class CustomersController : BaseController
    {
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;


        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        /// <param name="customerService">The customer service.</param>
        /// <param name="restaurantService">The restaurant service.</param>
        /// <param name="hotelService">The hotel service.</param>
        public CustomersController(ICustomerService customerService, IRestaurantService restaurantService, IHotelService hotelService)
        {
            _customerService = customerService;
            _restaurantService = restaurantService;
            _hotelService = hotelService;

        }


        /// <summary>
        /// Tests this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Test()
        {
            return View();
        }


        // GET: Customers
        /// <summary>
        /// Get All Customers.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Index()
        {
            var customers = _customerService.GetAllCustomers();
            return View(customers);
        }

        /// <summary>
        /// Creates Customer Sign In.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Create()
        {
            CustomerSignUpVM Customer = new CustomerSignUpVM();
            return View(Customer);
        }

        /// <summary>
        /// Creates the specified customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Create(CustomerSignUpVM customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Customer newCustomer = new Customer()
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        BirthDate = customer.DateOfBirth,
                        Email = customer.Email,
                        Password = customer.Password,
                        Telephone = customer.PhoneNumber,
                        City = customer.City,
                        Street = customer.Street,
                        Region = customer.Region,
                        CountryId = customer.CountryId,
                        Pincode = customer.Pincode
                    };

                    newCustomer.CreationDate = System.DateTime.Now;
                    newCustomer.ActiveStatus = false;

                    string activationCode = Guid.NewGuid().ToString();
                    newCustomer.ActivationCode = activationCode;

                    _customerService.CreateCustomer(newCustomer);
                    SendActivationEmail(newCustomer);

                    return RedirectToAction("AccountCreated", new { id = newCustomer.Id });
                }
                return View(customer);


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(customer);
            }
        }

        /// <summary>
        /// Accounts the created.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AccountCreated(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            return View(customer);
        }

        /// <summary>
        /// Sends the activation email.
        /// </summary>
        /// <param name="customer">The customer.</param>
        private void SendActivationEmail(Customer customer)
        {
            try
            {
                //todo: make it asynchronnious call and template based

                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("sender@gmail.com", customer.Email.ToString()))
                {

                    mm.Subject = "Account Activation";
                    string body = "Hello " + customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to activate your account";
                    body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("Create", "Activated?ActivationCode=" + customer.ActivationCode + "&Id=" + customer.Id.ToString()) + "'>Click here to activate your account.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("donotreplycertigoa@gmail.com", "Certigoa2017#");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }


        }

        /// <summary>
        /// Activateds the specified activation code.
        /// </summary>
        /// <param name="ActivationCode">The activation code.</param>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Activated(string ActivationCode, string Id)
        {
            var customer = _customerService.GetCustomerById(Convert.ToInt32(Id));
            if (customer.ActivationCode == ActivationCode)
            {
                customer.ActiveStatus = true;
                _customerService.ModifyCustomer(customer);
                ViewBag.Message = "Account Activated Successfully";
                return View();
            }
            else
            {
                ViewBag.Message = "Invalid  Activation Code";
                return View();
            }
        }

        /// <summary>
        /// Mies the account.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [Authorize]
        public ActionResult MyAccount()
        {
            var customer = _customerService.GetAllCustomers(c => string.Equals(c.Email, User.Identity.Name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return View(customer);
        }

        // GET: Customer/Edit/5
        /// <summary>
        /// Edits Customer.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Edit()
        {
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

            ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = e.ToString(), Name = e.ToString() };
            var customer = GetAuthenticatedCustomer();
            return View(customer);
        }

        // POST: Customer/Edit/5

        /// <summary>
        /// Edits the specified customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            try
            {
                ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

                ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = e.ToString(), Name = e.ToString() };
                if (ModelState["Password"] != null) ModelState["Password"].Errors.Clear();

                if (ModelState.IsValid)
                {
                    var authenticatedCustomer = GetAuthenticatedCustomer();

                    authenticatedCustomer.FirstName = customer.FirstName;
                    authenticatedCustomer.LastName = customer.LastName;
                    authenticatedCustomer.BirthDate = customer.BirthDate;
                    authenticatedCustomer.Street = customer.Street;
                    authenticatedCustomer.Street2 = customer.Street2;
                    authenticatedCustomer.City = customer.City;
                    authenticatedCustomer.Region = customer.Region;
                    authenticatedCustomer.CountryId = customer.CountryId;
                    authenticatedCustomer.Pincode = customer.Pincode;
                    authenticatedCustomer.Telephone = customer.Telephone;
                    authenticatedCustomer.PhoneCode = customer.PhoneCode;
                    authenticatedCustomer.CurrencyId = customer.CurrencyId;
                    authenticatedCustomer.Gender = customer.Gender;

                    var modifiedCustomer = _customerService.ModifyCustomer(authenticatedCustomer);
                    return RedirectToAction("Edit");
                }
                return View("Edit", customer);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Edit", customer);
            }
        }


        // GET: Customer/Delete/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="Id">Customer Id.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Delete(int Id)
        {
            var customer = _customerService.GetCustomerById(Id);
            return View(customer);
        }

        // POST: Customer/Delete/5
        /// <summary>
        /// Deletes the specified customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Delete(Customer customer)
        {
            try
            {
                _customerService.DeleteCustomer(customer.Id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(customer);
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [Authorize]
        public ActionResult ChangePassword()
        {
            ChangePassword changepassword = new ChangePassword();
            return PartialView(changepassword);
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="changepassword">The changepassword.</param>
        /// <returns>ActionResult.</returns>
        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword changepassword)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var customer = GetAuthenticatedCustomer();
                    if (Hashing.ValidatePassword(changepassword.CurrentPassword, customer.Password))
                    {
                        customer.Password = Hashing.HashPassword(changepassword.Password);
                        _customerService.ModifyCustomer(customer);
                        return PartialView("PasswordChanged");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Incorrect Current Password");
                        return PartialView(changepassword);
                    }
                }
                return PartialView(changepassword);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PartialView(changepassword);
            }
        }

        /// <summary>
        /// Selects the premises.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [Authorize]
        public ActionResult SelectPremises()
        {
            var customer = GetAuthenticatedCustomer();

            if (customer == null)
                return Content("Not Authorized to access backend");
            if (customer.CustomerRoles.Any(c => c.RoleId == 1))
            {
                var restaurants = _restaurantService.GetAllRestaurants().Where(r => r.ActiveStatus);
                var hotels = _hotelService.GetAllHotels().Where(h => h.Status);
                var totalPremises = restaurants.Count() + hotels.Count();


                if (totalPremises == 0)
                    return Content("Not Authorized to access backend");
                else if (totalPremises > 1)
                {
                    //Session["PremisesCount"] = 2;
                    var selectPremises = new SelectPremises
                    {
                        Hotels = hotels,
                        Restaurants = restaurants
                    };
                    return View(selectPremises);
                }
                else
                {
                    if (restaurants.Count() > 0)
                    {
                        // Session["PremisesCount"] = 1;
                        Session["RestaurantName"] = restaurants.Select(r => r.Name).FirstOrDefault();
                        setActivePremisesSessionVariables(restaurants.First().Id, 1);
                        return RedirectToAction("Dashboard", "Restaurant", new { area = "admin" });
                    }
                    else
                    {
                        //Session["PremisesCount"] = 1;
                        Session["HotelName"] = restaurants.Select(r => r.Name).FirstOrDefault();
                        setActivePremisesSessionVariables(hotels.First().Id, 2);
                        return RedirectToAction("Dashboard", "Hotel", new { area = "admin" });
                    }
                }
            }
            else
            {
                var restaurants = _customerService.CustomerAuthorizedRestaurants(customer.Id);
                var hotels = _customerService.CustomerAuthorizedHotels(customer.Id);
                var totalPremises = restaurants.Count() + hotels.Count();


                if (totalPremises == 0)
                    return Content("Not Authorized to access backend");
                else if (totalPremises > 1)
                {
                    var selectPremises = new SelectPremises
                    {
                        Hotels = hotels,
                        Restaurants = restaurants
                    };
                    return View(selectPremises);
                }
                else
                {
                    if (restaurants.Count() > 0)
                    {
                        Session["RestaurantName"] = restaurants.Select(r => r.Name).FirstOrDefault();
                        setActivePremisesSessionVariables(restaurants.First().Id, 1);
                        return RedirectToAction("Dashboard", "Restaurant", new { area = "admin" });
                    }
                    else
                    {
                        Session["HotelName"] = restaurants.Select(r => r.Name).FirstOrDefault();
                        setActivePremisesSessionVariables(hotels.First().Id, 2);
                        return RedirectToAction("Dashboard", "Hotel", new { area = "admin" });
                    }
                }
            }

        }

        /// <summary>
        /// To set active premises for logged in user
        /// </summary>
        /// <param name="id">restaurant Id.</param>
        /// <param name="type">1 restaurant, 2 hotel</param>
        /// <returns>ActionResult.</returns>
        [Authorize]
        public ActionResult SetPremises(int id, int type)
        {
            //Type
            if (type == 1)
            {
                if (GetAuthenticatedCustomer().CustomerRoles.Any(c => c.RoleId == 1 || c.RoleId == 8))
                {
                    Session["RestaurantName"] = _restaurantService.GetRestaurantById(id).Name;
                    setActivePremisesSessionVariables(id, type);
                    return RedirectToAction("Dashboard", "Restaurant", new { area = "admin" });
                }
                var authorized = _customerService.CustomerAuthorizedRestaurants(GetAuthenticatedCustomer().Id);

                if (authorized.FirstOrDefault(r => r.Id == id) == null)
                {
                    // not authorized, so redirect to selection page
                    return RedirectToAction("SelectPremises", "Customers");
                }
                else
                {
                    Session["RestaurantName"] = authorized.Where(r => r.Id == id).Select(r => r.Name).FirstOrDefault();
                    setActivePremisesSessionVariables(id, type);
                    return RedirectToAction("Dashboard", "Restaurant", new { area = "admin" });
                }

            }
            else if (type == 2) //hotel
            {
                if (GetAuthenticatedCustomer().CustomerRoles.Any(c => c.RoleId == 1 || c.RoleId == 8))
                {
                    Session["HotelName"] = _hotelService.GetHotelById(id).Name;
                    setActivePremisesSessionVariables(id, type);
                    return RedirectToAction("Dashboard", "Hotel", new { area = "admin" });
                }

                var authorized = _customerService.CustomerAuthorizedHotels(GetAuthenticatedCustomer().Id);

                if (authorized.FirstOrDefault(r => r.Id == id) == null)
                {
                    // not authorized, so redirect to selection page
                    return RedirectToAction("SelectPremises", "Customers");
                }
                else
                {
                    Session["HotelName"] = authorized.Where(r => r.Id == id).Select(r => r.Name).FirstOrDefault();
                    setActivePremisesSessionVariables(id, type);
                    return RedirectToAction("Dashboard", "Hotel", new { area = "admin" });
                }
            }

            return RedirectToAction("SelectPremises", "Customers");

        }


        /// <summary>
        /// Sets the active premises session variables.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        private void setActivePremisesSessionVariables(int id, int type)
        {
            Session["ActivePremise"] = id;
            Session["ActivePremiseType"] = type;
        }


        #region my favorites

        /// <summary>
        /// Favorites the restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult FavoriteRestaurants()
        {
            var customer = GetAuthenticatedCustomer();
            var favorites = _customerService.GetFavoriteRestaurants(customer.Id);
            return View(favorites);
        }
        /// <summary>
        /// Deletes the favorite restaurant.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteFavoriteRestaurant(int Id)
        {
            var customer = GetAuthenticatedCustomer();
            _customerService.DeleteFavoriteRestaurant(Id, customer.Id);
            return RedirectToAction("FavoriteRestaurants");
        }

        /// <summary>
        /// Favorites the hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult FavoriteHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var favorites = _customerService.GetFavoriteHotels(customer.Id).ToList();
            return View(favorites);
        }
        /// <summary>
        /// Deletes the favorite hotel.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteFavoriteHotel(int Id)
        {
            var customer = GetAuthenticatedCustomer();
            _customerService.DeleteFavoriteHotel(Id, customer.Id);
            return RedirectToAction("FavoriteHotels");
        }
        #endregion my favorites

        #region order

        /// <summary>
        /// Recents the orders.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RecentOrders()
        {
            return View();
        }
        /// <summary>
        /// Loads the customer recent orders.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadCustomerRecentOrders()
        {
            var customer = GetAuthenticatedCustomer();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var custRecentOrders = _customerService.GetCustomerRecentOrders(customer.Id, skip, pageSize, out Total);
            var data = custRecentOrders.Select(b => new
            {
                RestOrderId = b.Id,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus ? "Paid" : "Unpaid"
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Orderses the queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult OrdersQueue()
        {
            return View();
        }
        /// <summary>
        /// Loads the customer order queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadCustomerOrderQueue()
        {
            var customer = GetAuthenticatedCustomer();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var custOrderQueue = _customerService.GetCustomerOrdersQueue(customer.Id, skip, pageSize, out Total);
            var data = custOrderQueue.Select(b => new
            {
                RestOrderId = b.Id,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus ? "Paid" : "Unpaid"
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #endregion order

        #region payment cards
        /// <summary>
        /// Payments the cards.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult PaymentCards()
        {
            var customer = GetAuthenticatedCustomer();
            var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
            var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
            var paymentCards = _customerService.GetCustomerCards(customer.Id, quickpayToken, quickpayUrl);
            return View(paymentCards);
        }
        //public ActionResult CreatePaymentCard()
        //{
        //    CustomerCreditCard cards = new CustomerCreditCard();
        //    cards.CardExpiryDate = DateTime.Now;
        //    return View("CreateEditPaymentCard", cards);
        //}
        //public ActionResult SavePaymentCard(CustomerCreditCard creditCards)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (!IsValidCardNumber(creditCards.CardNumber))
        //                throw new ArgumentException(Resources.Invalid_CardNumber);

        //            if (creditCards.SpendingLimitEnabled.HasValue && creditCards.SpendingLimitEnabled.Value)
        //            {
        //                if (creditCards.SpendingLimitAmount == null || creditCards.SpendingLimitAmount <= 0)
        //                    throw new ArgumentException("Spending Limit Amount Shouldn't be empty or zero");
        //            }

        //            if (creditCards.Id == 0)
        //            {
        //                var customer = GetAuthenticatedCustomer();
        //                creditCards.CustomerId = customer.Id;
        //                _customerService.CreateCustomerCard(creditCards);
        //                return RedirectToAction("PaymentCards");
        //            }
        //            else
        //            {
        //                _customerService.ModifyCustomerCard(creditCards);
        //                return RedirectToAction("PaymentCards");
        //            }
        //        }
        //        else
        //        {
        //            return View("CreateEditPaymentCard", creditCards);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(string.Empty, ex.Message);
        //        return View("CreateEditPaymentCard", creditCards);
        //    }
        //}
        //public ActionResult EditPaymentCard(int card_id)
        //{
        //    var card = _customerService.GetCardById(card_id);
        //    if (card == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("CreateEditPaymentCard", card);
        //}
        /// <summary>
        /// Deletes the payment card.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeletePaymentCard(int id)
        {
            try
            {
                var existingCard = _customerService.GetCustomerCardDetailsById(id);
                var isPrimaryCard = existingCard.IsPrimary;
                _customerService.DeleteCustomerCard(existingCard);
                if (isPrimaryCard)
                    return Json(new { IsPrimary = true });
                else
                    return Json(new { IsPrimary = false });
            }
            catch
            {
                return RedirectToAction("PaymentCards");
            }
        }
        #endregion

        /// <summary>
        /// Restaurantses the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RestaurantsReviews()
        {
            return View();
        }

        /// <summary>
        /// Loads the restaurant reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadRestaurantReviews()
        {
            //var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var restId = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            int? selectedRestId;
            if (restId == "")
                selectedRestId = null;
            else
                selectedRestId = Convert.ToInt32(restId);
            var customer = GetAuthenticatedCustomer();
            var reviews = _restaurantService.GetRestaurantAllReviews(fromDate, toDate, selectedRestId, customer.Id, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                RestaurantName = b.Restaurant.Name,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Restaurants the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult.</returns>
        [HttpPost]
        public JsonResult RestaurantAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetAllRestaurantsByName(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });

            return Json(restaurants, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Hotelses the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult HotelsReviews()
        {
            return View();
        }

        /// <summary>
        /// Loads the hotels reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadHotelsReviews()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var hotelId = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            int? selectedHotelId;
            if (hotelId == "")
                selectedHotelId = null;
            else
                selectedHotelId = Convert.ToInt32(hotelId);
            var customer = GetAuthenticatedCustomer();
            var reviews = _hotelService.GetHotelAllReviews(selectedHotelId, customer.Id, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                HotelName = b.Hotel.Name,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Hotels the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult.</returns>
        [HttpPost]
        public JsonResult HotelAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var hotels = _hotelService.GetAllHotelsByName(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });

            return Json(hotels, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Reviewses the by hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ReviewsByHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantReviews = _hotelService.GetCustomerAllHotelsReviews(customer.Id);
            return PartialView("_ReviewsByHotels", restaurantReviews);
        }
        /// <summary>
        /// Recents the restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RecentRestaurants()
        {
            var customer = GetAuthenticatedCustomer();
            var recentRestaurants = _customerService.GetRestaurantOrders(customer.Id).OrderByDescending(c => c.CreationDate);
            return View(recentRestaurants);
        }
        /// <summary>
        /// Recents the hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RecentHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var recentHotels = _hotelService.GetAllRoomBookingByCustomer(customer.Id).Select(h => h.Room.Hotel).Distinct().OrderByDescending(c => c.CreationDate);
            return View(recentHotels);
        }

        /// <summary>
        /// Restaurants the preferences.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RestaurantPreferences()
        {
            var customer = GetAuthenticatedCustomer();
            var allergenPreference = _customerService.GetCustomerRestaurantPreferenceAllergen(customer.Id);

            RestaurantPreferenceViewModel model = BuildCustomerRestaurantPreference(customer, allergenPreference.ToList());
            return View(model);

        }

        /// <summary>
        /// Builds the customer restaurant preference.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="allergenPreference">The allergen preference.</param>
        /// <returns>RestaurantPreferenceViewModel.</returns>
        private static RestaurantPreferenceViewModel BuildCustomerRestaurantPreference(Customer customer, List<Customer_AllergenPreference> allergenPreference)
        {
            RestaurantPreferenceViewModel model = new RestaurantPreferenceViewModel()
            {
                Groups = new List<RestaurantPreferenceGroupViewModel>()
            };

            model.Id = customer.Id;
            RestaurantPreferenceGroupViewModel allergensGroup = new RestaurantPreferenceGroupViewModel()
            {
                GroupName = "Allergens",
                Description = "I'm allergic to following, so please use this inform while ordering food."
            };
            allergensGroup.SubGroups.AddRange(allergenPreference.Select(ap =>
            {
                var newGroup = new RestaurantPreferenceSubGroupViewModel
                {
                    SubGroupId = ap.Allergen_ID,
                    Description = ap.Allergen_Desc,
                    SubGroupName = ap.Allergen_Name,
                    ItemValue = (ap.Id != null) ? true : false
                };
                return newGroup;
            }));
            model.Groups.Add(allergensGroup);
            return model;
        }

        /// <summary>
        /// Updates the restaurant preference.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateRestaurantPreference(int id)
        {
            var customer = GetAuthenticatedCustomer();

            var allergenPreference = _customerService.GetAllergenPreferenceById(id, customer.Id);
            return RedirectToAction("RestaurantPreferences", "Customers");
        }

        /// <summary>
        /// Customers the recent restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult CustomerRecentRestaurants()
        {
            var customer = GetAuthenticatedCustomer();
            var recentRestaurants = _customerService.GetRestaurantOrders(customer.Id).OrderByDescending(c => c.CreationDate).Select(i => i.Restaurant).Distinct().Take(4);

            return PartialView("_RecentRestaurants", recentRestaurants);
        }

        /// <summary>
        /// Determines whether [is valid card number] [the specified credit card number].
        /// </summary>
        /// <param name="creditCardNumber">The credit card number.</param>
        /// <returns><c>true</c> if [is valid card number] [the specified credit card number]; otherwise, <c>false</c>.</returns>
        public static bool IsValidCardNumber(string creditCardNumber)
        {
            try
            {
                System.Collections.ArrayList CheckNumbers = new ArrayList();
                int CardLength = creditCardNumber.Length;

                for (int i = CardLength - 2; i >= 0; i = i - 2)
                {
                    CheckNumbers.Add(Int32.Parse(creditCardNumber[i].ToString()) * 2);
                }

                int CheckSum = 0;
                for (int iCount = 0; iCount <= CheckNumbers.Count - 1; iCount++)
                {
                    int _count = 0;
                    if ((int)CheckNumbers[iCount] > 9)
                    {
                        int _numLength = ((int)CheckNumbers[iCount]).ToString().Length;
                        for (int x = 0; x < _numLength; x++)
                        {
                            _count = _count + Int32.Parse(
                                  ((int)CheckNumbers[iCount]).ToString()[x].ToString());
                        }
                    }
                    else
                    {
                        _count = (int)CheckNumbers[iCount];
                    }
                    CheckSum = CheckSum + _count;
                }
                int OriginalSum = 0;
                for (int y = CardLength - 1; y >= 0; y = y - 2)
                {
                    OriginalSum = OriginalSum + Int32.Parse(creditCardNumber[y].ToString());
                }
                return (((OriginalSum + CheckSum) % 10) == 0);
            }
            catch
            {
                return false;
            }
        }

    }
}