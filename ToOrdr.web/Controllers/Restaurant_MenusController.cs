﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="Restaurant_MenusController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class RestaurantMenusController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class RestaurantMenusController : BaseController
    {
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantMenusController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        public RestaurantMenusController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        // GET: RestaurantMenusItem
        /// <summary>
        /// Indexes the specified identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Index(int Id)
        {
            var restaurant = _restaurantService.GetRestaurantById(Id).Name;

            ViewBag.RestaurantName = restaurant;
            ViewBag.RestaurantId = Id;

            return View();
        }

        /// <summary>
        /// Searches the menu item.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SearchMenuItem(string keywords, int Id)
        {
            int page = 1;
            int limit = int.MaxValue;
            int total;
            var items = _restaurantService.SearchMenuItems(out total,keywords, Id, page, limit).Where(i => i.RestaurantMenuGroup.RestaurantMenu.IsActive == true);

            return PartialView("_MenuItemList", items.ToList());
        }
    }
}