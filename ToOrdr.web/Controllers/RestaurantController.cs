﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="RestaurantController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class RestaurantController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class RestaurantController : BaseController
    {
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }
        // GET: Restaurant
        /// <summary>
        /// Indexes the specified keywords.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="cuisineIds">The cuisine ids.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Index(string keywords, string cuisineIds)
        {
            RestaurantSearch search = new RestaurantSearch();
            search.Cuisines = _restaurantService.GetAllCuisines().ToList();

            if (!string.IsNullOrWhiteSpace(cuisineIds))
                search.SelectedCuisinesId = cuisineIds.Split(',');

            search.Keywords = keywords;
            return View(search);
        }

        /// <summary>
        /// Tests this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Test()
        {
            return View();
        }


        /// <summary>
        /// Searches the restaurant.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="place">The place.</param>
        /// <param name="cuisineIds">The cuisine ids.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SearchRestaurant(string keywords, string place, string cuisineIds)
        {
            List<int> cuisineList = null;

            if (!string.IsNullOrWhiteSpace(cuisineIds))
                cuisineList = cuisineIds.Split(',').Select(int.Parse).ToList();

            var restaurants = _restaurantService.SearchRestaurants(keywords, place, cuisineList);
            return PartialView("RestaurantList", restaurants);
        }

        /// <summary>
        /// Sponsoreds the restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult SponsoredRestaurants()
        {
            var restaurants = _restaurantService.GetAllRestaurants().Take(8);
            return PartialView("SponsoredRestaurants", restaurants);
        }

        /// <summary>
        /// Googles the map restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult GoogleMapRestaurants()
        {
            var restaurants = _restaurantService.GetAllRestaurants();
            return PartialView("GoogleMapRestaurants", restaurants);
        }

        /// <summary>
        /// Detailses the specified restaurant identifier.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Details(int RestaurantId)
        {
            var restaurants = _restaurantService.GetRestaurantById(RestaurantId);
            return PartialView("RestaurantDetails", restaurants);
        }
    }
}