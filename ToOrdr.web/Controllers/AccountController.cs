﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-15-2019
// ***********************************************************************
// <copyright file="AccountController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Core.Util;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Controllers
{

    /// <summary>
    /// Class AccountController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class AccountController : BaseController
    {
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;
        /// <summary>
        /// The role service
        /// </summary>
        IRoleService _roleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="customerService">The customer service.</param>
        /// <param name="roleService">The role service.</param>
        public AccountController(ICustomerService customerService, IRoleService roleService)
        {
            _customerService = customerService;
            _roleService = roleService;
        }


        /// <summary>
        /// Identities the sign in.
        /// </summary>
        /// <param name="Email">The email.</param>
        /// <param name="FirstName">The first name.</param>
        /// <param name="LastName">The last name.</param>
        /// <param name="providerKey">The provider key.</param>
        /// <param name="Provider">The provider.</param>
        public void IdentitySignin(string Email, string FirstName, string LastName, string providerKey = null, string Provider = null)
        {
            var claims = new List<Claim>();

            // add required claims

            if (!string.IsNullOrWhiteSpace(Email)) claims.Add(new Claim(ClaimTypes.Email, Email));
            if (!string.IsNullOrWhiteSpace(providerKey)) claims.Add(new Claim("ProviderKey", providerKey));
            if (!string.IsNullOrWhiteSpace(Provider)) claims.Add(new Claim("Provider", Provider));
            if (!string.IsNullOrWhiteSpace(FirstName)) claims.Add(new Claim(ClaimTypes.GivenName, FirstName + " " + LastName));


            claims.Add(new Claim(ClaimTypes.NameIdentifier, providerKey == null ? "Email" : "Provider_Key"));
            claims.Add(new Claim(ClaimTypes.Name, providerKey == null ? Email : providerKey));

            // custom – my serialized AppUserState object

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = false,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, identity);
        }



        // GET: Account
        /// <summary>
        /// Signs in.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult SignIn()
        {
            CustomerSignInVM Customer = new CustomerSignInVM();

            return View(Customer);
        }

        /// <summary>
        /// Signs in.
        /// </summary>
        /// <param name="Customer">The customer.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult SignIn(CustomerSignInVM Customer, string returnUrl)
        {
            var requiredValidation = new string[] { "Email", "Password" };

            foreach (var item in ModelState.Where(k => !requiredValidation.Contains(k.Key)))
                ModelState[item.Key].Errors.Clear();


            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(Customer.Email, Customer.Password))
                {
                    var customer = _customerService.GetAllCustomers(c => String.Equals(c.Email, Customer.Email, StringComparison.OrdinalIgnoreCase) && c.ActiveStatus == true).FirstOrDefault(); ;

                    IdentitySignin(customer.Email, customer.FirstName, customer.LastName, null);

                    if (customer != null)
                    {

                        var authoriedHotels = _customerService.CustomerAuthorizedHotels(customer.Id);
                        var authoriedRestaurants = _customerService.CustomerAuthorizedRestaurants(customer.Id);
                        if (customer.CustomerRoles.Where(r => r.RoleId == 1).Any())
                        {
                            Session["AllowBackend"] = 1;
                            Session["IsAdmin"] = "true";
                            return RedirectToAction("Dashboard", "Admin", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 2).Any())
                        {
                            var restaurant = _roleService.GetRestaurantByCustomerId(customer.Id);
                            Session["RestaurantName"] = restaurant.Name;
                            Session["ActivePremise"] = restaurant.Id;
                            Session["ActivePremiseType"] = 1;
                            Session["RestaurantAdmin"] = "true";
                            return RedirectToAction("Dashboard", "Restaurant", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 4).Any())
                        {
                            var restaurant = _roleService.GetRestaurantByCustomerId(customer.Id);
                            Session["RestaurantName"] = restaurant.Name;
                            Session["ActivePremise"] = restaurant.Id;
                            Session["ActivePremiseType"] = 1;
                            Session["IsReceptionist"] = "true";
                            return RedirectToAction("ReceptionDashboard", "Restaurant", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 5).Any())
                        {
                            var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                            Session["HotelName"] = hotel.Name;
                            Session["ActivePremise"] = hotel.Id;
                            Session["ActivePremiseType"] = 2;
                            Session["HotelAdmin"] = "true";
                            return RedirectToAction("Dashboard", "Hotel", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 7).Any())
                        {
                            var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                            Session["HotelName"] = hotel.Name;
                            Session["ActivePremise"] = hotel.Id;
                            Session["ActivePremiseType"] = 2;
                            Session["IsHotelReceptionist"] = "true";
                            return RedirectToAction("HotelReceptionDashboard", "Hotel", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Any(r => r.RoleId == 8))
                        {
                            Session["AllowBackend"] = 1;
                            Session["IsChainAdmin"] = "true";
                            return RedirectToAction("ChainAdminDashboard", "Admin", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 9).Any())
                        {
                            var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                            Session["HotelName"] = hotel.Name;
                            Session["ActivePremise"] = hotel.Id;
                            Session["ActivePremiseType"] = 2;
                            Session["IsHotelSpaManager"] = "true";
                            return RedirectToAction("HotelSpaManagerDashboard", "Hotel", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 10).Any())
                        {
                            var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                            Session["HotelName"] = hotel.Name;
                            Session["ActivePremise"] = hotel.Id;
                            Session["ActivePremiseType"] = 2;
                            Session["IsHotelSpaEmployee"] = "true";
                            return RedirectToAction("HotelSpaEmployeeDashboard", "HotelSpaService", new { area = "admin" });
                        }
                        else if (customer.CustomerRoles.Where(r => r.RoleId == 11).Any())
                        {
                            var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                            Session["HotelName"] = hotel.Name;
                            Session["ActivePremise"] = hotel.Id;
                            Session["ActivePremiseType"] = 2;
                            Session["IsHotelLaundryAdmin"] = "true";
                            return RedirectToAction("HotelLaundryAdminDashboard", "HotelLaundry", new { area = "admin" });
                        }
                        else if ((authoriedHotels.Count() + authoriedRestaurants.Count()) > 0)
                        {
                            Session["AllowBackend"] = 1;
                            Session["AllowedHotels"] = authoriedHotels;
                            Session["AllowedRestaurants"] = authoriedHotels;
                            return RedirectToAction("SelectPremises", "Customers");
                        }
                        else
                        {
                            Session["AllowBackend"] = 0;
                            return RedirectToAction("Edit", "Customers");
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(returnUrl))
                        return Redirect(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "The email id or password provided is incorrect");
                }
            }
            return View(Customer);
        }


        //
        // POST: /Account/ExternalLogin
        /// <summary>
        /// Externals the login.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }


        /// <summary>
        /// Externals the login callback.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>Externals login.</returns>
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await HttpContext.GetOwinContext().Authentication.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }


            if (loginInfo == null)
            {
                IdentitySignout(); // to be safe we log out
                return RedirectToAction("SignIn", new { message = "Unable to authenticate with external login." });
            }

            // Authenticated!
            string providerKey = loginInfo.Login.ProviderKey;
            string providerName = loginInfo.Login.LoginProvider;

            if (!_customerService.ValidateExternalLogin(providerKey, providerName))  // customer not found, add customer
            {
                var newCustomer = new Customer
                {
                    FirstName = loginInfo.DefaultUserName,
                    LastName = string.Empty,
                    Password = string.Empty,
                    ProviderKey = providerKey,
                    ProviderName = providerName,
                    Email = string.IsNullOrWhiteSpace(loginInfo.Email) ? "not_available@asfasf.com" : loginInfo.Email,
                    CurrencyId = 1,
                };

                _customerService.CreateExternalCustomer(newCustomer);

            }


            IdentitySignin(loginInfo.Email, providerKey, providerName);
            return Redirect(returnUrl);

            //// Sign in the user with this external login provider if the user already has a login
            //var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            //return RedirectToLocal(returnUrl);

            //switch (result)
            //{
            //    case SignInStatus.Success:
            //        return RedirectToLocal(returnUrl);
            //    case SignInStatus.LockedOut:
            //        return View("Lockout");
            //    case SignInStatus.RequiresVerification:
            //        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
            //    case SignInStatus.Failure:
            //    default:
            //        // If the user does not have an account, then prompt the user to create an account
            //        ViewBag.ReturnUrl = returnUrl;
            //        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
            //        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            //}
        }



        /// <summary>
        /// Class ChallengeResult.
        /// Implements the <see cref="System.Web.Mvc.HttpUnauthorizedResult" />
        /// </summary>
        /// <seealso cref="System.Web.Mvc.HttpUnauthorizedResult" />
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            /// <summary>
            /// The XSRF key
            /// </summary>
            string XsrfKey = "2ordr_123";

            /// <summary>
            /// Initializes a new instance of the <see cref="ChallengeResult"/> class.
            /// </summary>
            /// <param name="provider">The provider.</param>
            /// <param name="redirectUri">The redirect URI.</param>
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ChallengeResult"/> class.
            /// </summary>
            /// <param name="provider">The provider.</param>
            /// <param name="redirectUri">The redirect URI.</param>
            /// <param name="userId">The user identifier.</param>
            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            /// <summary>
            /// Gets or sets the login provider.
            /// </summary>
            /// <value>The login provider.</value>
            public string LoginProvider { get; set; }
            /// <summary>
            /// Gets or sets the redirect URI.
            /// </summary>
            /// <value>The redirect URI.</value>
            public string RedirectUri { get; set; }
            /// <summary>
            /// Gets or sets the user identifier.
            /// </summary>
            /// <value>The user identifier.</value>
            public string UserId { get; set; }

            /// <summary>
            /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
            /// </summary>
            /// <param name="context">The context in which the result is executed. The context information includes the controller, HTTP content, request context, and route data.</param>
            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        /// <summary>
        /// Represents an event that is raised when the sign-out operation is complete.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult SignOut()
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            IdentitySignout();
            return RedirectToAction("SignIn");
        }

        /// <summary>
        /// Identities the signout.
        /// </summary>
        public void IdentitySignout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                                            DefaultAuthenticationTypes.ExternalCookie);
        }


        /// <summary>
        /// Forgots the password.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ForgotPassword()
        {
            ForgotPassword forgotpassword = new ForgotPassword();
            return View(forgotpassword);
        }

        /// <summary>
        /// Forgots the password.
        /// </summary>
        /// <param name="forgotpassword">The forgotpassword.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPassword forgotpassword)
        {
            if (forgotpassword.Email != null)
            {
                var customer = _customerService.GetAllCustomers(k => k.Email == forgotpassword.Email).FirstOrDefault();
                if (customer != null)
                {

                    string resetCode = Guid.NewGuid().ToString();
                    customer.ResetPasswordCode = resetCode;
                    _customerService.ModifyCustomer(customer);
                    SendResetEmail(customer);
                    return View("PasswordRecovery", customer);
                }
                else
                {
                    this.Response.StatusCode = 400;
                    ModelState.AddModelError("", "We couldn't find any account associated with the address :" + forgotpassword.Email.ToString());
                    return View();
                }
            }
            else
            {
                this.Response.StatusCode = 400;
                return View();
            }


        }


        /// <summary>
        /// Sends the reset email.
        /// </summary>
        /// <param name="Customer">The customer.</param>
        private void SendResetEmail(Customer Customer)
        {
            try
            {
                //todo: make it asynchronnious call and template based
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("sender@gmail.com", Customer.Email.ToString()))
                {

                    mm.Subject = "Reset Password";
                    string body = "Hello " + Customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to reset your password";
                    body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("ForgotPassword", "ResetPassword?ResetPasswordCode=" + Customer.ResetPasswordCode + "&Id=" + Customer.Id.ToString()) + "'>Click here to reset your password.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("donotreplycertigoa@gmail.com", "Certigoa2017#");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }


        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="ResetPasswordCode">The reset password code.</param>
        /// <param name="Id">Customer Id.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ResetPassword(string ResetPasswordCode, string Id)
        {

            ResetPassword reset = new ResetPassword();
            var customer = _customerService.GetCustomerById(Convert.ToInt32(Id));
            if (customer.ResetPasswordCode == ResetPasswordCode)
            {
                reset.Email = customer.Email;
                reset.Id = Id;
            }
            return View(reset);
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="reset">The reset.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult ResetPassword(ResetPassword reset)
        {
            if (ModelState.IsValid)
            {
                var customer = _customerService.GetCustomerById(Convert.ToInt32(reset.Id));
                customer.Password = Hashing.HashPassword(reset.Password);
                _customerService.ModifyCustomer(customer);
                return View("VerifiedPassword");
            }
            else
            {
                ModelState.AddModelError("", "Incorrect Password");
                return View("ResetPassword", reset);
            }
        }

    }
}