﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 02-20-2019
// ***********************************************************************
// <copyright file="BaseController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Web.Helpers;
using ZXing;
using ZXing.QrCode;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class BaseController.
    /// Implements the <see cref="System.Web.Mvc.Controller" />
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class BaseController : Controller
    {
        /// <summary>
        /// The customer service
        /// </summary>
        private readonly ICustomerService _customerService = DependencyResolver.Current.GetService<ICustomerService>();
        /// <summary>
        /// The role service
        /// </summary>
        private readonly IRoleService _roleService = DependencyResolver.Current.GetService<IRoleService>();
        /// <summary>
        /// The culture name
        /// </summary>
        public string cultureName = null;

        /// <summary>
        /// Begins to invoke the action in the current controller context.
        /// </summary>
        /// <param name="callback">The callback.</param>
        /// <param name="state">The state.</param>
        /// <returns>Returns an IAsyncController instance.</returns>
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null;     // obtain it from HTTP header AcceptLanguages

            //Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName);

            //modify current thread's cultures
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;


            VerifySessionVariables();

            return base.BeginExecuteCore(callback, state);
        }


        /// <summary>
        /// Verifies the session variables.
        /// </summary>
        private void VerifySessionVariables()
        {
            if (this.Request.IsAuthenticated && Session["AllowBackend"] == null)
            {
                var customer = GetAuthenticatedCustomer();
                if (customer == null) return;

                var authoriedHotels = _customerService.CustomerAuthorizedHotels(customer.Id);
                var authoriedRestaurants = _customerService.CustomerAuthorizedRestaurants(customer.Id);
                if (customer.CustomerRoles.Where(r => r.RoleId == 1).Any())
                {
                    Session["AllowBackend"] = 1;
                    Session["IsAdmin"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 2).Any())
                {
                    var restaurant = _roleService.GetRestaurantByCustomerId(customer.Id);
                    Session["RestaurantName"] = restaurant.Name;
                    Session["ActivePremise"] = restaurant.Id;
                    Session["ActivePremiseType"] = 1;
                    Session["RestaurantAdmin"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 4).Any())
                {
                    var restaurant = _roleService.GetRestaurantByCustomerId(customer.Id);
                    Session["RestaurantName"] = restaurant.Name;
                    Session["ActivePremise"] = restaurant.Id;
                    Session["ActivePremiseType"] = 1;
                    Session["IsReceptionist"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 5).Any())
                {
                    var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                    Session["HotelName"] = hotel.Name;
                    Session["ActivePremise"] = hotel.Id;
                    Session["ActivePremiseType"] = 2;
                    Session["HotelAdmin"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 7).Any())
                {
                    var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                    Session["HotelName"] = hotel.Name;
                    Session["ActivePremise"] = hotel.Id;
                    Session["ActivePremiseType"] = 2;
                    Session["IsHotelReceptionist"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 8).Any())
                {
                    Session["AllowBackend"] = 1;
                    Session["IsChainAdmin"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 9).Any())
                {
                    var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                    Session["HotelName"] = hotel.Name;
                    Session["ActivePremise"] = hotel.Id;
                    Session["ActivePremiseType"] = 2;
                    Session["IsHotelSpaManager"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 10).Any())
                {
                    var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                    Session["HotelName"] = hotel.Name;
                    Session["ActivePremise"] = hotel.Id;
                    Session["ActivePremiseType"] = 2;
                    Session["IsHotelSpaEmployee"] = "true";
                }
                else if (customer.CustomerRoles.Where(r => r.RoleId == 11).Any())
                {
                    var hotel = _roleService.GetHotelByCustomerId(customer.Id);
                    Session["HotelName"] = hotel.Name;
                    Session["ActivePremise"] = hotel.Id;
                    Session["ActivePremiseType"] = 2;
                    Session["IsHotelLaundryAdmin"] = "true";
                }
                else if ((authoriedHotels.Count() + authoriedRestaurants.Count()) > 0)
                {
                    Session["AllowBackend"] = 1;
                    Session["AllowedHotels"] = authoriedHotels;
                    Session["AllowedRestaurants"] = authoriedHotels;
                }
                else
                {
                    Session["AllowBackend"] = 0;
                }

            }
        }


        /// <summary>
        /// Gets the authenticated customer.
        /// </summary>
        /// <returns>Customer.</returns>
        public Customer GetAuthenticatedCustomer()
        {
            if (this.Request.IsAuthenticated)
            {
                if (User.Identity is ClaimsIdentity)
                {
                    var claims = User.Identity as ClaimsIdentity;

                    var claim = claims.Claims.FirstOrDefault(c => c.Type == "Provider");
                    if (claim != null)
                    {
                        var provideName = claim.Value;
                        return _customerService.GetAllCustomers(c => String.Equals(c.ProviderKey, User.Identity.Name, StringComparison.OrdinalIgnoreCase) && c.ActiveStatus == true).FirstOrDefault();
                    }

                    return _customerService.GetAllCustomers(c => String.Equals(c.Email, User.Identity.Name, StringComparison.OrdinalIgnoreCase) && c.ActiveStatus == true).FirstOrDefault();
                }
            }
            return null;
        }


        /// <summary>
        /// Gets the active restaurant identifier.
        /// </summary>
        /// <returns>System.Int32.</returns>
        public int GetActiveRestaurantId()
        {
            if (Session["ActivePremiseType"] == null || Session["ActivePremise"] == null) return 0;

            int type;
            int restaurantId;

            int.TryParse(Session["ActivePremiseType"].ToString(), out type);

            int.TryParse(Session["ActivePremise"].ToString(), out restaurantId);

            if (type == 1) return restaurantId;
            else return 0;
        }


        /// <summary>
        /// Gets the active hotel identifier.
        /// </summary>
        /// <returns>System.Int32.</returns>
        public int GetActiveHotelId()
        {
            if (Session["ActivePremiseType"] == null || Session["ActivePremise"] == null) return 0;

            int type;
            int hotelId;

            int.TryParse(Session["ActivePremiseType"].ToString(), out type);

            int.TryParse(Session["ActivePremise"].ToString(), out hotelId);

            if (type == 2) return hotelId;
            else return 0;
        }

        /// <summary>
        /// Generates the qr code.
        /// </summary>
        /// <param name="qrCodeId">The qr code identifier.</param>
        /// <param name="folderPath">The folder path.</param>
        /// <param name="imagePath">The image path.</param>
        /// <returns>System.String.</returns>
        public string GenerateQRCode(string qrCodeId, string folderPath, string imagePath)
        {
            folderPath = Server.MapPath(folderPath);
            // If the directory doesn't exist then create it.
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var barcodeWriter = new BarcodeWriter();
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            barcodeWriter.Options = new QrCodeEncodingOptions
            {
                Width = 250,
                Height = 250,
                Margin = 0
            };
            var result = barcodeWriter.Write(qrCodeId);

            string barcodePath = Server.MapPath(imagePath);
            var barcodeBitmap = new Bitmap(result);
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(barcodePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            return imagePath;
        }

    }
}