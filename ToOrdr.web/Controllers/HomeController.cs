﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 12-04-2018
// ***********************************************************************
// <copyright file="HomeController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class HomeController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HomeController : BaseController
    {
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        /// <param name="customerService">The customer service.</param>
        public HomeController(IRestaurantService restaurantService, ICustomerService customerService)
        {
            _restaurantService = restaurantService;
            _customerService = customerService;
        }

        // GET: Home
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Abouts this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Contacts this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Homers this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Homer()
        {
            return View();
        }

        /// <summary>
        /// Helps this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Help()
        {
            return View();
        }

        /// <summary>
        /// Termses the condition.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TermsCondition()
        {
            return View();
        }

        /// <summary>
        /// Partners the with us.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult PartnerWithUs()
        {
            return View();
        }
    }
}