﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="LanguageController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Web.Helpers;

namespace ToOrdr.Web.Controllers
{
    /// <summary>
    /// Class LanguageController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class LanguageController : BaseController
    {
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        public LanguageController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        // [ChildActionOnly]
        /// <summary>
        /// Languages the selector.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LanguageSelector()
        {
            Language language = new Language();
            language.LanguageCulture = cultureName;

            ViewBag.languages = _restaurantService.GetAllLanguages(); ;
            return PartialView("LanguageSelector", language);
        }

        /// <summary>
        /// Sets the culture.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SetCulture(Language language)
        {
            // Validate input
            string culture;
            culture = CultureHelper.GetImplementedCulture(language.LanguageCulture);  //todo: pass supported cultures from Language

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }

            Response.Cookies.Add(cookie);
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}