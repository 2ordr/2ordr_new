﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="HotelTripsController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelTripsController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelTripsController : BaseController
    {
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HotelTripsController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
          public HotelTripsController(IHotelService hotelService)
        {
            _hotelService = hotelService;

        }
        // GET: Admin/HotelSpaService
          /// <summary>
          /// Manages the trips.
          /// </summary>
          /// <param name="id">The identifier.</param>
          /// <returns>ActionResult.</returns>
        public ActionResult ManageTrips(int id)
        {
            ViewBag.HotelServiceId = id;
            var hotelId = GetActiveHotelId();
            var hotelTrips = _hotelService.GetAllHotelTrips(hotelId,id);
            return View("ManageTrips", hotelTrips);
        }

        /// <summary>
        /// Adds the trip.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddTrip(int hotelServiceId)
        {
            Trip Trips = new Trip();
            Trips.HotelServiceId = hotelServiceId;
            return View("AddEditTrip",Trips);

        }

        /// <summary>
        /// Saves the specified trips.
        /// </summary>
        /// <param name="Trips">The trips.</param>
        /// <param name="uploadTripImage">The upload trip image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Save(Trip Trips, HttpPostedFileBase uploadTripImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];

                    var tripImageFilename = Trips.Image;
                    if (uploadTripImage != null)
                    {
                        if (uploadTripImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadTripImage.FileName);
                            var ext = Path.GetExtension(uploadTripImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                tripImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("",Resources.Invalid_ImageExtension);
                                return View("AddEditTrip", Trips);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditTrip", Trips);
                        }
                    }

                    if (Trips.Id == 0)
                    {
                        Trips.Image = tripImageFilename;
                        var trip = _hotelService.CreateHotelTrip(Trips);
                        if (uploadTripImage != null)
                        {
                            ImageHelper.UploadImage(trip.Id, ImagePathFor.TripService, uploadTripImage, api_path);
                        }

                        return RedirectToAction("ManageTrips", new { id=trip.HotelServiceId});
                    }
                    else
                    {
                        if (uploadTripImage != null)
                        {
                            ImageHelper.UploadImage(Trips.Id, ImagePathFor.TripService, uploadTripImage, api_path);
                        }
                        Trips.Image = tripImageFilename;
                        var res = _hotelService.ModifyHotelTrip(Trips);
                        return RedirectToAction("ManageTrips", new { id=Trips.HotelServiceId});
                    }
                }
                else
                {

                    return View("AddEditTrip", Trips);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditTrip", Trips);
            }
        }


        /// <summary>
        /// Edits the trip.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditTrip(int id)
        {
            var trips = _hotelService.GetHotelTripByID(id);
            return View("AddEditTrip", trips);
        }
        /// <summary>
        /// Deletes the trip.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteTrip(int id)
        {
            try
            {
                _hotelService.DeleteTrip(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false,error="This trip cannot be deleted, bacause it is already in use" });
            }
        }


        /// <summary>
        /// Manages the trip details.
        /// </summary>
        /// <param name="TripId">The trip identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageTripDetails(int TripId)
        {
            ViewBag.TripID = TripId;
            ViewBag.HotelServiceId = _hotelService.GetHotelTripByID(TripId).HotelServiceId;
            var hotelTripDetails = _hotelService.GetHotelTripDetails(TripId);
            return View("ManageTripDetails", hotelTripDetails);
        }

        /// <summary>
        /// Creates the modify trip details.
        /// </summary>
        /// <param name="TripID">The trip identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult CreateModifyTripDetails(int TripID)
        {
            TripDetail Trips = new TripDetail();
            Trips.TripID = TripID;
            return View(Trips);
        }

        /// <summary>
        /// Saves the trip details.
        /// </summary>
        /// <param name="TripDetails">The trip details.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveTripDetails(TripDetail TripDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (TripDetails.Id == 0)
                    {
                        _hotelService.CreateHotelTripDetails(TripDetails);
                        return RedirectToAction("ManageTripDetails", new { TripID = TripDetails.TripID });
                    }
                    else
                    {
                        _hotelService.ModifyHotelTripDetails(TripDetails);
                        return RedirectToAction("ManageTripDetails", new { TripID = TripDetails.TripID });
                    }
                }
                else
                {
                    return View("CreateModifyTripDetails", TripDetails);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("CreateModifyTripDetails", TripDetails);
            }
        }


        /// <summary>
        /// Edits the trip details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditTripDetails(int id)
        {
            var trips_details = _hotelService.GetHotelTripDetailsByID(id);
            return View("CreateModifyTripDetails", trips_details);
        }
        /// <summary>
        /// Deletes the trip details.
        /// </summary>
        /// <param name="TripDetailsID">The trip details identifier.</param>
        /// <param name="TripID">The trip identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteTripDetails(int TripDetailsID,int TripID)
        {
            try
            {
                _hotelService.DeleteTripDetails(TripDetailsID);
                return RedirectToAction("ManageTripDetails", new { TripID = TripID, Area = "Admin"});
            }
            catch
            {
                return RedirectToAction("ManageTripDetails", new { TripID = TripID, Area = "Admin" });
            }
        }

        /// <summary>
        /// Tripses the queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TripsQueue()
        {
            return View();
        }

        /// <summary>
        /// Loads the trip orders.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadTripOrders()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetTripBookings(hotelId, skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                TripBookingId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                TripName = b.Trip.Name,
                Seats = b.Seats,
                Amount = b.Amount,
                Status = b.Status.Name
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Tripses the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TripsReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the trips reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadTripsReviews()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var reviews = _hotelService.GetTripsReviewsByHotel(hotelId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                TripsReviewId = b.Id,
                TripName = b.Trip.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult BookingStatus(bool status, int booking_id)
        //{
        //    var TripBooking = _hotelService.GetHotelTripBookingByID(booking_id);
        //    TripBooking.Booking_Status = status;
        //    var BookingModified = _hotelService.ModifyHotelTripBooking(TripBooking);
        //    return RedirectToAction("TripsQueue", "HotelTrips", new { Area = "Admin" });
        //}

        /// <summary>
        /// Deletes the trips booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteTripsBooking(int id)
        {
            try
            {
                _hotelService.DeleteTripBooking(id);
                return RedirectToAction("TripsQueue", "HotelTrips", new { Area = "Admin" });
            }
            catch
            {
                return View("TripsQueue");
            }
        }

    }
}