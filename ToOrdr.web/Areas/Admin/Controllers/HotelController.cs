﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-11-2019
// ***********************************************************************
// <copyright file="HotelController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Web.Controllers;
using ToOrdr.Localization;
using System.IO;
using ToOrdr.Core.Helpers;
using System.Net.Mail;
using System.Net;
using ToOrdr.Core.Util;
using ToOrdr.Web.Helpers;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelController : BaseController
    {
        /// <summary>
        /// The API path
        /// </summary>
        string api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;


        /// <summary>
        /// Initializes a new instance of the <see cref="HotelController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="customerService">The customer service.</param>
        public HotelController(
            IHotelService hotelService,
            ICustomerService customerService
            )
        {
            _hotelService = hotelService;
            _customerService = customerService;
        }
        // GET: Admin/Hotel
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Dashboards this instance.
        /// </summary>
        /// <returns>Return Hotel dashboard.</returns>
        public ActionResult Dashboard()
        {
            return View();
        }

        /// <summary>
        /// Loads the hotel reviews.
        /// </summary>
        /// <returns>Load all hotel reviews.</returns>
        [HttpPost]
        public ActionResult LoadHotelReviews()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var reviews = _hotelService.GetHotelAllReviews(hotelId, null, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Loads the todays room bookings.
        /// </summary>
        /// <returns>Load Todays RoomBookings.</returns>
        [HttpPost]
        public ActionResult LoadTodaysRoomBookings()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var reviews = _hotelService.GetTodaysRoomBookings(hotelId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                BookingId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RoomNumber = b.Room.Number,
                FromDate = b.From.ToString(),
                ToDate = b.To.ToString(),
                Amount = b.Total,
                Status = b.Status.Name
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Gets the hotel reviews.
        /// </summary>
        /// <returns>list Hotel Reviews.</returns>
        public ActionResult GetHotelReviews()
        {
            var hotelId = GetActiveHotelId();
            int total;
            var hotelReviews = _hotelService.GetHotelReviews(hotelId, 1, int.MaxValue, out total).OrderByDescending(o => o.CreationDate);
            return View(hotelReviews);
        }

        //public ActionResult GetHotelServicesReviews()
        //{
        //    var hotelId = GetActiveHotelId();
        //    var hotelServiceReviews = _hotelService.GetHotelServicesReviews(hotelId);
        //    return View("HotelServicesReviews", hotelServiceReviews);
        //}

        //public ActionResult ListHotelServiceReviews(int id)
        //{
        //    var hotelId = GetActiveHotelId();
        //    var hotelServiceReviews = _hotelService.GetHotelServicesReviewsByServiceId(hotelId, id);
        //    return PartialView("_ListServiceReviews", hotelServiceReviews);
        //}

        /// <summary>
        /// Hotels the service automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>Json Result of hotel services.</returns>
        public JsonResult HotelServiceAutoComplete(string prefix)
        {
            var hotelServices = _hotelService.GetAllServicesByName(prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.ServiceName
                });

            return Json(hotelServices, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets all hotel room bookings.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>List hotel room booking.</returns>
        public ActionResult GetAllHotelRoomBookings(int? page)
        {
            var hotelId = GetActiveHotelId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int total;

            var booking = _hotelService.GetAllHotelRoomBookings(hotelId, pageNumber, pageSize, out total);
            return View("HotelRoomBookings", new StaticPagedList<CustomerHotelBooking>(booking, pageNumber, pageSize, total));
        }

        /// <summary>
        /// Gets the hotel services.
        /// </summary>
        /// <returns>hotel services.</returns>
        public ActionResult GetHotelServices()
        {
            var hotelId = GetActiveHotelId();
            var hotelServices = _hotelService.GetAllHotelServices(hotelId);
            return View(hotelServices);
        }

        /// <summary>
        /// Gets the hotel active services.
        /// </summary>
        /// <returns>List Hotel ActiveServices.</returns>
        public ActionResult GetHotelActiveServices()
        {
            var hotelId = GetActiveHotelId();
            var hotelServices = _hotelService.GetHotelServices(hotelId);
            return PartialView("_HotelActiveServices", hotelServices);
        }
        /// <summary>
        /// Gets the hotel active service by service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>List Hotel ActiveServices.</returns>
        public ActionResult GetHotelActiveServiceByServiceId(int serviceId)
        {
            var hotelId = GetActiveHotelId();
            var hotelSpaService = _hotelService.GetHotelActiveServiceByHotelId(hotelId, serviceId);
            ViewBag.ServiceId = serviceId;
            return PartialView("_HotelActiveServiceByServiceId", hotelSpaService);
        }

        /// <summary>
        /// Adds the hotel services.
        /// </summary>
        /// <returns>List Hotel Services.</returns>
        public ActionResult AddHotelServices()
        {
            ViewBag.Services = _hotelService.GetAllUnassignedServices(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.ServiceName });
            return View();
        }

        /// <summary>
        /// Adds the hotel services.
        /// </summary>
        /// <param name="hotelServices">The hotel services.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult AddHotelServices(HotelServices hotelServices)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var hotelId = GetActiveHotelId();

                    hotelServices.HotelId = hotelId;
                    _hotelService.CreateHotelService(hotelServices);

                    return RedirectToAction("GetHotelServices", "Hotel", new { area = "Admin" });
                }
                else
                {
                    ViewBag.Services = _hotelService.GetAllServices().Select(s => new { Id = s.Id, Name = s.ServiceName });
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddHotelServices", hotelServices);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Services = _hotelService.GetAllServices().Select(s => new { Id = s.Id, Name = s.ServiceName });
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddHotelServices", hotelServices);
            }

        }

        /// <summary>
        /// Updates the hotel service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Updated hotel services.</returns>
        [HttpPost]
        public ActionResult UpdateHotelService(int id)
        {
            try
            {
                var service = _hotelService.GetHotelServicesByServiceId(id);
                if (service.IsActive == true)
                    service.IsActive = false;
                else
                    service.IsActive = true;

                _hotelService.ModifyHotelService(service);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This hotel service is already in use." });
            }
        }

        /// <summary>
        /// Deletes the hotel service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteHotelService(int id)
        {
            try
            {
                _hotelService.DeleteHotelService(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This hotel service is already in use." });
            }
        }

        #region Hotel House Keeping Info
        /// <summary>
        /// Adds the update house keeping information.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddUpdateHouseKeepingInfo()
        {
            var hotelId = GetActiveHotelId();
            var hotelHouseKeepingInfo = _hotelService.GetHotelHouseKeepingInfoByHotelId(hotelId);
            if (hotelHouseKeepingInfo != null)
                return View(hotelHouseKeepingInfo);
            else
            {
                HotelHouseKeepingInfo htlHouseKeepingInfo = new HotelHouseKeepingInfo();
                htlHouseKeepingInfo.HotelId = hotelId;
                return View(htlHouseKeepingInfo);
            }
        }

        /// <summary>
        /// Saves the hotel house keeping information.
        /// </summary>
        /// <param name="hotelHouseKeepingInfo">The hotel house keeping information.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult SaveHotelHouseKeepingInfo(HotelHouseKeepingInfo hotelHouseKeepingInfo, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                hotelHouseKeepingInfo.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddUpdateHouseKeepingInfo", hotelHouseKeepingInfo);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddUpdateHouseKeepingInfo", hotelHouseKeepingInfo);
                        }
                    }
                    var addedHotelHouseKeepingInfo = _hotelService.AddUpdateHouseKeepingInfo(hotelHouseKeepingInfo);
                    if (uploadImage != null)
                    {
                        ImageHelper.UploadImage(addedHotelHouseKeepingInfo.HotelId, ImagePathFor.HotelHouseKeepingInfoImage, uploadImage, api_path, addedHotelHouseKeepingInfo.Id);
                    }
                    return RedirectToAction("AddUpdateHouseKeepingInfo");

                }
                else
                {
                    ModelState.AddModelError("", "ModelState validation error");
                    return View("AddUpdateHouseKeepingInfo", hotelHouseKeepingInfo);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddUpdateHouseKeepingInfo", hotelHouseKeepingInfo);
            }
        }
        #endregion Hotel House Keeping Info

        #region Manage Hotel Near By Places

        /// <summary>
        /// Manages the hotel near by places.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageHotelNearByPlaces()
        {
            return View();
        }

        /// <summary>
        /// Loads the hotel near by places.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadHotelNearByPlaces()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var hotelNearByPlaces = _hotelService.GetAllHotelNearByPlaces(GetActiveHotelId(), skip, pageSize, out Total);
            var data = hotelNearByPlaces.Select(b => new
            {
                HotelNearByPlacesId = b.Id,
                Place = b.Place,
                Distance = b.Distance,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.HotelNearByPlaces, b.Image, b.HotelId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the hotel near by place.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddHotelNearByPlace()
        {
            HotelsNearByPlace hotelNearByPlace = new HotelsNearByPlace();
            hotelNearByPlace.HotelId = GetActiveHotelId();
            return View("AddEditHotelNearByPlace", hotelNearByPlace);
        }

        /// <summary>
        /// Saves the hotel near by place.
        /// </summary>
        /// <param name="hotelNearByPlace">The hotel near by place.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveHotelNearByPlace(HotelsNearByPlace hotelNearByPlace, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                hotelNearByPlace.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditHotelNearByPlace", hotelNearByPlace);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditHotelNearByPlace", hotelNearByPlace);
                        }
                    }
                    if (hotelNearByPlace.Id == 0)
                    {
                        var hotelNrByPlace = _hotelService.CreateHotelNearByPlace(hotelNearByPlace);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(hotelNrByPlace.HotelId, ImagePathFor.HotelNearByPlaces, uploadImage, api_path, hotelNrByPlace.Id);
                        }
                        return RedirectToAction("ManageHotelNearByPlaces");
                    }
                    else
                    {
                        var hotelNrByPlace = _hotelService.ModifyHotelNearByPlace(hotelNearByPlace);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(hotelNrByPlace.HotelId, ImagePathFor.HotelNearByPlaces, uploadImage, api_path, hotelNrByPlace.Id);
                        }
                        return RedirectToAction("ManageHotelNearByPlaces");
                    }
                }
                else
                {
                    return View("AddEditHotelNearByPlace", hotelNearByPlace);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHotelNearByPlace", hotelNearByPlace);
            }
        }

        /// <summary>
        /// Edits the hotel near by place.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditHotelNearByPlace(int id)
        {
            var hotelNearByPlace = _hotelService.GetHotelNearByPlaceById(id);
            return View("AddEditHotelNearByPlace", hotelNearByPlace);
        }
        /// <summary>
        /// Deletes the hotel near by place.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteHotelNearByPlace(int id)
        {
            try
            {
                var hotelNearByPlace = _hotelService.GetHotelNearByPlaceById(id);
                if (!string.IsNullOrWhiteSpace(hotelNearByPlace.Image))
                {
                    string imgPath = @"\Images\Hotel\" + hotelNearByPlace.HotelId.ToString() + @"\NearByPlaces\" + hotelNearByPlace.Id.ToString() + @"\" + hotelNearByPlace.Image;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                _hotelService.DeleteHotelNearByPlace(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This hotel near by place cannot be deleted, since it is already in use." });
            }
        }

        #endregion Manage Hotel Near By Places

        #region Hotel Customer Role
        /// <summary>
        /// Authorizeds the users.
        /// </summary>
        /// <returns>ActionResult Authorized Users.</returns>
        public ActionResult AuthorizedUsers()
        {
            var hotelId = GetActiveHotelId();

            var authorizedUsers = _hotelService.HotelAuthorizedCustomers(hotelId);
            return View(authorizedUsers);
        }
        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <returns>ActionResult Create CustomerRole.</returns>
        public ActionResult CreateCustomerRole()
        {
            CustomerRoleViewModel customerRole = new CustomerRoleViewModel();
            ViewBag.Roles = _hotelService.GetAllHotelAdminAccessRole();
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

            ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
            return View("CreateEditCustomer", customerRole);
        }

        /// <summary>
        /// Customerses the specified term.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult customers(string term)
        {
            var result = _hotelService.GetCustomers(term).Select(r => new { id = r.Id, Name = r.FirstName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult CreateCustomerRole(CustomerRoleViewModel customerRole)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string passwordCode = Guid.NewGuid().ToString();

                    GenderType genderType = (GenderType)Convert.ToInt32(customerRole.Customer.Gender);
                    Customer newCustomer = new Customer()
                    {
                        FirstName = customerRole.Customer.FirstName,
                        LastName = customerRole.Customer.LastName,
                        BirthDate = customerRole.Customer.BirthDate,
                        Email = customerRole.Customer.Email,
                        Password = Hashing.HashPassword(passwordCode),
                        PhoneCode = customerRole.Customer.PhoneCode,
                        Telephone = customerRole.Customer.Telephone,
                        City = customerRole.Customer.City,
                        Street = customerRole.Customer.Street,
                        Street2 = customerRole.Customer.Street2,
                        Region = customerRole.Customer.Region,
                        CountryId = customerRole.Customer.CountryId,
                        Pincode = customerRole.Customer.Pincode,
                        CurrencyId = customerRole.Customer.CurrencyId,
                        Gender = genderType.ToString() == "0" ? null : genderType.ToString()
                    };

                    newCustomer.CreationDate = System.DateTime.Now;
                    newCustomer.ActiveStatus = true;

                    string activationCode = Guid.NewGuid().ToString();
                    newCustomer.ActivationCode = activationCode;

                    var customer = _customerService.CreateCustomer(newCustomer);
                    CustomerRole role = new CustomerRole()
                    {
                        CustomerId = customer.Id,
                        RoleId = customerRole.RoleId,
                        HotelId = GetActiveHotelId(),
                        RestaurantId = null,
                        ChainId = null
                    };
                    _hotelService.CreateCustomerRole(role);

                    EmailHelper mailHelper = new EmailHelper();
                    mailHelper.SendPasswordOnEmail(customer.Email, customer.FirstName, passwordCode);
                    return RedirectToAction("AuthorizedUsers", "Hotel");
                }
                else
                {
                    ViewBag.Roles = _hotelService.GetAllRole();
                    ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                    ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                    ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _hotelService.GetAllRole();
                ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateCustomerRole(int id)
        {
            var customerRole = _hotelService.GetCustomerByRoleId(id);

            RegisterCustomerVM vModel = new RegisterCustomerVM()
            {
                FirstName = customerRole.Customer.FirstName,
                LastName = customerRole.Customer.LastName
            };

            CustomerRoleViewModel model = new CustomerRoleViewModel()
            {
                Id = customerRole.Id,
                RoleId = customerRole.RoleId,
                CustomerId = customerRole.CustomerId,
                HotelId = customerRole.HotelId,
                Customer = vModel
            };

            ViewBag.Roles = _hotelService.GetAllHotelAdminAccessRole();
            return View("CreateEditCustomer", model);
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateCustomerRole(CustomerRoleViewModel customerRole)
        {
            try
            {
                ModelState["Customer.CurrencyId"].Errors.Clear();
                ModelState["Customer.PhoneCode"].Errors.Clear();
                ModelState["Customer.Telephone"].Errors.Clear();
                ModelState["Customer.Email"].Errors.Clear();
                ModelState["Customer.LastName"].Errors.Clear();
                ModelState["Customer.CountryId"].Errors.Clear();
                if (ModelState.IsValid)
                {
                    CustomerRole custsRole = new CustomerRole()
                    {
                        Id = customerRole.Id,
                        CustomerId = customerRole.CustomerId,
                        RoleId = customerRole.RoleId,
                        HotelId = customerRole.HotelId,
                        RestaurantId = null,
                        ChainId = null
                    };

                    _hotelService.UpdateCustomerRole(custsRole);
                    return RedirectToAction("AuthorizedUsers");
                }
                else
                {
                    ViewBag.Roles = _hotelService.GetAllRole();
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _hotelService.GetAllRole();
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteCustomerRole(int id)
        {
            try
            {
                _hotelService.DeleteCustomerRole(id);
                return RedirectToAction("AuthorizedUsers", "Restaurant");
            }
            catch
            {
                return View("AuthorizedUsers");
            }
        }

        #endregion Hotel Customer Role

        #region Hotel Rooms

        /// <summary>
        /// Manages the rooms.
        /// </summary>
        /// <returns>ActionResult Manage Rooms.</returns>
        public ActionResult ManageRooms()
        {
            var hotelId = GetActiveHotelId();
            var hotelRooms = _hotelService.GetAllHotelRooomsByHotelId(hotelId);
            foreach (var room in hotelRooms)
            {
                room.QRCodeImage = Url.Content("/Images/Hotels/") + room.HotelId.ToString() + "/RoomQRCodes/" + room.Id + "/" + room.QRCodeImage;
            }
            return View(hotelRooms);
        }

        /// <summary>
        /// Adds the hotel room.
        /// </summary>
        /// <returns>ActionResult Add HotelRoom.</returns>
        public ActionResult AddHotelRoom()
        {
            Room room = new Room();
            return View("AddEditRoom", room);
        }
        /// <summary>
        /// Adds the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>ActionResult Add new HotelRoom.</returns>
        [HttpPost]
        public ActionResult AddHotelRoom(Room room)
        {
            try
            {
                var hotelId = GetActiveHotelId();
                var isRoom = _hotelService.GetHotelRoom(hotelId, room.Number);
                if (ModelState.IsValid)
                {
                    if (room.Id == 0 && isRoom.Count() == 0)
                    {
                        room.HotelId = hotelId;
                        room.QRCodeImage = "QrCode.jpg";
                        var createdRoom = _hotelService.CreateHotelRoom(room);

                        string folderPath = "~/Images/Hotels/" + createdRoom.HotelId.ToString() + "/RoomQRCodes/" + createdRoom.Id.ToString() + "/";
                        string imagePath = "~/Images/Hotels/" + createdRoom.HotelId.ToString() + "/RoomQRCodes/" + createdRoom.Id.ToString() + "/QrCode.jpg";
                        var QRCodeImage = GenerateQRCode(createdRoom.Id.ToString(), folderPath, imagePath);
                        return RedirectToAction("ManageRooms");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, Resources.Room_Num_Exist);
                        return View("AddEditRoom", room);
                    }
                }
                else
                {
                    return View("AddEditRoom", room);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoom", room);
            }
        }

        /// <summary>
        /// Edits the hotel room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HotelRoom.</returns>
        public ActionResult EditHotelRoom(int id)
        {
            var room = _hotelService.GetRooomDetailsByRoomId(id);
            return View("AddEditRoom", room);
        }

        /// <summary>
        /// Edits the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>ActionResult Edit HotelRoom.</returns>
        [HttpPost]
        public ActionResult EditHotelRoom(Room room)
        {
            try
            {
                var hotelId = GetActiveHotelId();
                if (ModelState.IsValid)
                {
                    room.HotelId = hotelId;
                    _hotelService.UpdateHotelRoom(room);
                    return RedirectToAction("ManageRooms");
                }
                else
                {
                    return View("AddEditRoom", room);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoom", room);
            }
        }

        /// <summary>
        /// Deletes the hotel room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HotelRoom.</returns>
        [HttpPost]
        public ActionResult DeleteHotelRoom(int id)
        {
            try
            {
                List<string> allRoomImagePath = new List<string>();
                var roomImages = _hotelService.GetAllRoomImages(id);
                foreach (var roomImage in roomImages)
                {
                    string imgPath = "";
                    if (!string.IsNullOrWhiteSpace(roomImage.Image))
                    {
                        imgPath = @"\Images\Hotel\Rooms\" + roomImage.RoomId.ToString() + @"\" + roomImage.Image;
                    }
                    if (imgPath != "")
                        allRoomImagePath.Add(imgPath);
                }

                _hotelService.DeleteHotelRoom(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                if (allRoomImagePath.Any())
                {
                    foreach (var roomImgPath in allRoomImagePath)
                    {
                        ImageHelper.DeleteImage(api_path, roomImgPath);
                    }
                }
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This hotel room is already in use." });
            }
        }

        /// <summary>
        /// Downloads the room qr code.
        /// </summary>
        /// <param name="imgPath">The img path.</param>
        /// <param name="roomNo">The room no.</param>
        /// <returns>FilePathResult.</returns>
        public FilePathResult DownloadRoomQRCode(string imgPath, string roomNo)
        {
            imgPath = Server.MapPath(imgPath);
            if (System.IO.File.Exists(imgPath))
            {
                var imgName = "RoomNo_" + roomNo + "_QrCode.jpg";
                return File(imgPath, "multipart/form-data", imgName);
            }
            return File("/Images/no_logo.png", "multipart/form-data", "file_not_found.png");
        }

        /// <summary>
        /// Manages the room images.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageRoomImages(int id)
        {
            ViewBag.RoomId = id;
            var roomImages = _hotelService.GetAllRoomImages(id);
            return View(roomImages);
        }
        /// <summary>
        /// Adds the room images.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddRoomImages(int roomId)
        {
            HotelRoomImages roomImages = new HotelRoomImages();
            roomImages.RoomId = roomId;
            return View(roomImages);
        }

        /// <summary>
        /// Saves the room images.
        /// </summary>
        /// <param name="roomImages">The room images.</param>
        /// <param name="uploadRoomImages">The upload room images.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult SaveRoomImages(HotelRoomImages roomImages, HttpPostedFileBase[] uploadRoomImages)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<HotelRoomImages> lstRoomImages = new List<HotelRoomImages>();

                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    //iterating through multiple file collection   
                    var count = 1;
                    foreach (HttpPostedFileBase eachRoomImage in uploadRoomImages)
                    {
                        if (eachRoomImage != null)
                        {
                            if (eachRoomImage.ContentLength <= 2000000)
                            {
                                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                                var filename = Path.GetFileName(eachRoomImage.FileName);
                                filename = filename.Replace(" ", "_");
                                var ext = Path.GetExtension(eachRoomImage.FileName);
                                if (allowedExtensions.Contains(ext))
                                {
                                    if (count == 1)
                                    {
                                        var hotelRoomImages = _hotelService.GetAllRoomImages(roomImages.RoomId).Where(i => i.IsPrimary == true).FirstOrDefault();
                                        if (hotelRoomImages != null)
                                        {
                                            if (hotelRoomImages.IsPrimary == true)
                                            {
                                                if (roomImages.IsPrimary)
                                                {
                                                    hotelRoomImages.IsPrimary = false;
                                                    _hotelService.UpdateRoomImage(hotelRoomImages);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            roomImages.IsPrimary = true;
                                        }
                                    }
                                    else
                                    {
                                        roomImages.IsPrimary = false;
                                    }
                                    roomImages.Image = filename;
                                    ImageHelper.UploadImage(roomImages.RoomId, ImagePathFor.HotelRooms, eachRoomImage, api_path);
                                    _hotelService.AddRoomImage(roomImages);
                                }
                                else
                                {
                                    ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                    return View("AddRoomImages", roomImages);
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                                return View("AddRoomImages", roomImages);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", " Please select proper Image");
                            return View("AddRoomImages", roomImages);
                        }
                        count = count + 1;
                    }
                    return RedirectToAction("ManageRoomImages", new { id = roomImages.RoomId });
                }
                else
                {
                    return View("AddRoomImages", roomImages);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddRoomImages", roomImages);
            }
        }

        /// <summary>
        /// Updates the room image primary.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateRoomImagePrimary(int id)
        {
            try
            {
                var roomImage = _hotelService.GetRoomImageByImageId(id);

                var roomImages = _hotelService.GetAllRoomImages(roomImage.RoomId).Where(i => i.IsPrimary == true).FirstOrDefault();
                if (roomImages != null && roomImages.Id != roomImage.Id)
                {
                    roomImages.IsPrimary = false;
                    _hotelService.UpdateRoomImage(roomImages);
                    roomImage.IsPrimary = true;

                    _hotelService.UpdateRoomImage(roomImage);
                    return RedirectToAction("ManageRoomImages", "Hotel", new { id = roomImage.RoomId });
                }
                else
                {
                    return Json(new { failure = true, error = "At least one image must be primary." });
                }


            }
            catch
            {
                return View("ManageRoomImages");
            }
        }

        /// <summary>
        /// Deletes the room image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteRoomImage(int id)
        {
            var roomImage = _hotelService.GetRoomImageByImageId(id);
            var api_path = ConfigurationManager.AppSettings["APIURL"];

            if (!string.IsNullOrWhiteSpace(roomImage.Image))
            {
                string imgPath = @"\Images\Hotel\Rooms\" + roomImage.RoomId + @"\" + roomImage.Image;
                ImageHelper.DeleteImage(api_path, imgPath);
            }
            if (roomImage.IsPrimary == true)
            {
                var roomImg = _hotelService.GetAllRoomImages(roomImage.RoomId).Where(r => r.Id != id).FirstOrDefault();
                if (roomImg != null)
                {
                    roomImg.IsPrimary = true;
                    _hotelService.UpdateRoomImage(roomImg);
                }
            }

            _hotelService.DeleteRoomImage(id);
            return RedirectToAction("ManageRoomImages", new { id = roomImage.RoomId });
        }

        /// <summary>
        /// Updates the room image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateRoomImageActive(int id)
        {
            try
            {
                var roomImage = _hotelService.GetRoomImageByImageId(id);
                if (roomImage.IsActive)
                    roomImage.IsActive = false;
                else
                    roomImage.IsActive = true;

                var updatedRoomImg = _hotelService.UpdateRoomImage(roomImage);
                return RedirectToAction("ManageRoomImages", "Hotel", new { id = updatedRoomImg.RoomId });
            }
            catch
            {
                return View("ManageRoomImages");
            }
        }

        #endregion Hotel Rooms

        #region Manage Room Details

        /// <summary>
        /// Manages the room details.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>ActionResult Manage RoomDetails.</returns>
        public ActionResult ManageRoomDetails(int roomId)
        {
            ViewBag.RoomId = roomId;
            return View();
        }

        /// <summary>
        /// Loads the room details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load RoomDetails.</returns>
        [HttpPost]
        public ActionResult LoadRoomDetails(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var roomDetails = _hotelService.GetRoomDetails(id, skip, pageSize, out Total);
            var data = roomDetails.Select(b => new
            {
                RoomDetId = b.Id,
                Name = b.Name,
                Description = b.Description,
                Active = b.IsActive,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.RoomDetailsImages, b.Image, b.RoomId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the room details.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>ActionResult Add RoomDetails.</returns>
        public ActionResult AddRoomDetails(int roomId)
        {
            RoomDetails roomDetails = new RoomDetails()
            {
                RoomId = roomId
            };
            return View("AddEditRoomDetails", roomDetails);
        }

        /// <summary>
        /// Saves the room details.
        /// </summary>
        /// <param name="roomDetails">The room details.</param>
        /// <param name="uploadRoomDetImages">The upload room det images.</param>
        /// <returns>ActionResult Save RoomDetails.</returns>
        public ActionResult SaveRoomDetails(RoomDetails roomDetails, HttpPostedFileBase uploadRoomDetImages)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadRoomDetImages != null)
                    {
                        if (uploadRoomDetImages.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadRoomDetImages.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadRoomDetImages.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                roomDetails.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditRoomDetails", roomDetails);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditRoomDetails", roomDetails);
                        }
                    }

                    if (roomDetails.Id == 0)
                    {
                        var result = _hotelService.CreateRoomDetail(roomDetails);
                        if (uploadRoomDetImages != null)
                        {
                            ImageHelper.UploadImage(result.RoomId, ImagePathFor.RoomDetailsImages, uploadRoomDetImages, api_path, result.Id);
                        }
                        return RedirectToAction("ManageRoomDetails", new { roomId = roomDetails.RoomId });
                    }
                    else
                    {
                        var res = _hotelService.ModifyRoomDetail(roomDetails);
                        if (uploadRoomDetImages != null)
                        {
                            ImageHelper.UploadImage(res.RoomId, ImagePathFor.RoomDetailsImages, uploadRoomDetImages, api_path, res.Id);
                        }
                        return RedirectToAction("ManageRoomDetails", new { roomId = roomDetails.RoomId });
                    }
                }
                else
                {
                    return View("AddEditRoomDetails", roomDetails);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoomDetails", roomDetails);
            }
        }

        /// <summary>
        /// Edits the room details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit RoomDetails.</returns>
        public ActionResult EditRoomDetails(int id)
        {
            var roomDetail = _hotelService.GetRoomDetailById(id);

            return View("AddEditRoomDetails", roomDetail);
        }
        /// <summary>
        /// Deletes the room details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RoomDetails.</returns>
        [HttpPost]
        public ActionResult DeleteRoomDetails(int id)
        {
            try
            {
                var roomDetails = _hotelService.GetRoomDetailById(id);
                if (!string.IsNullOrWhiteSpace(roomDetails.Image))
                {
                    string imgPath = @"\Images\Hotel\Rooms\" + roomDetails.RoomId.ToString() + @"\RoomDetails\" + roomDetails.Id.ToString() + @"\" + roomDetails.Image;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                _hotelService.DeleteRoomDetail(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This room detail already in use." });
            }
        }

        #endregion Manage Room Details

        #region Manage Room Categories
        /// <summary>
        /// Manages the room categories.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageRoomCategories()
        {
            return View();
        }

        /// <summary>
        /// Loads the room categories.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadRoomCategories()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var roomCategories = _hotelService.GetAllRoomCategories(skip, pageSize, out Total);
            var data = roomCategories.Select(b => new
            {
                RoomCategoryId = b.Id,
                Name = b.Name,
                Description = b.Description,
                Beds = b.Beds,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the room category.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddRoomCategory()
        {
            RoomCategory roomCategory = new RoomCategory();
            return View("AddEditRoomCategory", roomCategory);
        }


        /// <summary>
        /// Saves the room category.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveRoomCategory(RoomCategory roomCategory)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (roomCategory.Id == 0)
                    {
                        _hotelService.CreateRoomCategory(roomCategory);
                        return RedirectToAction("ManageRoomCategories");
                    }
                    else
                    {
                        _hotelService.ModifyRoomCategory(roomCategory);
                        return RedirectToAction("ManageRoomCategories");
                    }
                }
                else
                {
                    return View("AddEditRoomCategory", roomCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoomCategory", roomCategory);
            }
        }

        /// <summary>
        /// Edits the room category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditRoomCategory(int id)
        {
            var roomCategory = _hotelService.GetRoomCategoryById(id);
            return View("AddEditRoomCategory", roomCategory);
        }
        /// <summary>
        /// Deletes the room category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteRoomCategory(int id)
        {
            try
            {
                _hotelService.DeleteRoomCategory(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Room category already in use." });
            }
        }
        #endregion Manage Room Categories

        #region Manage Room Capacity
        /// <summary>
        /// Manages the room capacities.
        /// </summary>
        /// <returns>ActionResult Manage RoomCapacities.</returns>
        public ActionResult ManageRoomCapacities()
        {
            return View();
        }

        /// <summary>
        /// Loads the room capacities.
        /// </summary>
        /// <returns>ActionResult Load RoomCapacities.</returns>
        [HttpPost]
        public ActionResult LoadRoomCapacities()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var roomCapacity = _hotelService.GetAllRoomCapacities(skip, pageSize, out Total);
            var data = roomCapacity.Select(b => new
            {
                RoomCapacityId = b.Id,
                Adults = b.Adults,
                Childrens = b.Childrens,
                Description = b.Description,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the room capacity.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddRoomCapacity()
        {
            RoomCapacity roomCapacity = new RoomCapacity();
            return View("AddEditRoomCapacity", roomCapacity);
        }


        /// <summary>
        /// Saves the room capacity.
        /// </summary>
        /// <param name="roomCapacity">The room capacity.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveRoomCapacity(RoomCapacity roomCapacity)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (roomCapacity.Id == 0)
                    {
                        _hotelService.CreateRoomCapacity(roomCapacity);
                        return RedirectToAction("ManageRoomCapacities");
                    }
                    else
                    {
                        _hotelService.ModifyRoomCapacity(roomCapacity);
                        return RedirectToAction("ManageRoomCapacities");
                    }
                }
                else
                {
                    return View("AddEditRoomCapacity", roomCapacity);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoomCapacity", roomCapacity);
            }
        }

        /// <summary>
        /// Edits the room capacity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditRoomCapacity(int id)
        {
            var roomCapacity = _hotelService.GetRoomCapacityById(id);
            return View("AddEditRoomCapacity", roomCapacity);
        }
        /// <summary>
        /// Deletes the room capacity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteRoomCapacity(int id)
        {
            try
            {
                _hotelService.DeleteRoomCapacity(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Room capacity already in use." });
            }
        }
        #endregion Manag Room Capacity

        #region Manage Amenities

        /// <summary>
        /// Manages the amenities.
        /// </summary>
        /// <returns>ActionResult Manage Amenities.</returns>
        public ActionResult ManageAmenities()
        {
            return View();
        }

        /// <summary>
        /// Loads the amenities.
        /// </summary>
        /// <returns>ActionResult Load Amenities.</returns>
        [HttpPost]
        public ActionResult LoadAmenities()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var amenities = _hotelService.GetAllAmenities(skip, pageSize, out Total);
            var data = amenities.Select(b => new
            {
                AmenityId = b.Id,
                Name = b.Name,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the amenities.
        /// </summary>
        /// <returns>ActionResult Add Amenities.</returns>
        public ActionResult AddAmenities()
        {
            Amenities amenities = new Amenities();
            return View("AddEditAmenities", amenities);
        }

        /// <summary>
        /// Saves the amenities.
        /// </summary>
        /// <param name="amenity">The amenity.</param>
        /// <returns>ActionResult Save Amenities.</returns>
        public ActionResult SaveAmenities(Amenities amenity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (amenity.Id == 0)
                    {
                        _hotelService.CreateAmenities(amenity);
                        return RedirectToAction("ManageAmenities");
                    }
                    else
                    {
                        _hotelService.ModifyAmenities(amenity);
                        return RedirectToAction("ManageAmenities");
                    }
                }
                else
                {
                    return View("AddEditAmenities", amenity);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditAmenities", amenity);
            }
        }

        /// <summary>
        /// Edits the amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit Amenities.</returns>
        public ActionResult EditAmenities(int id)
        {
            var amenity = _hotelService.GetAmenityById(id);
            return View("AddEditAmenities", amenity);
        }
        /// <summary>
        /// Deletes the amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete Amenities.</returns>
        [HttpPost]
        public ActionResult DeleteAmenities(int id)
        {
            try
            {
                _hotelService.DeleteAmenities(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This room detail already in use." });
            }
        }

        #endregion Manage Amenities

        #region Manage Room Amenities
        /// <summary>
        /// Manages the room amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage RoomAmenities.</returns>
        public ActionResult ManageRoomAmenities(int id)
        {
            ViewBag.RoomId = id;
            return View();
        }

        /// <summary>
        /// Loads the room amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load RoomAmenities.</returns>
        [HttpPost]
        public ActionResult LoadRoomAmenities(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var roomAmenities = _hotelService.GetAllRoomAmenities(id, skip, pageSize, out Total);
            var data = roomAmenities.Select(b => new
            {
                RoomAmenitiesId = b.Id,
                RoomNumber = b.Room.Number,
                AmenitiesName = b.Amenities.Name
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the room amenities.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>ActionResult Add RoomAmenities.</returns>
        public ActionResult AddRoomAmenities(int roomId)
        {
            RoomAmenities roomAmenities = new RoomAmenities();
            roomAmenities.RoomId = roomId;
            var unassignRoomAmenities = _hotelService.GetUnassignAmenities(roomId).Select(
                 r => new
                 {
                     AmenitiesId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.Amenities = new MultiSelectList(unassignRoomAmenities, "AmenitiesId", "Name");
            return View(roomAmenities);
        }


        /// <summary>
        /// Saves the room amenities.
        /// </summary>
        /// <param name="roomAmenities">The room amenities.</param>
        /// <returns>ActionResult Save RoomAmenities.</returns>
        [HttpPost]
        public ActionResult SaveRoomAmenities(RoomAmenities roomAmenities)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<RoomAmenities> allRoomAmenities = new List<RoomAmenities>();

                    var AmenitiesIds = Request.Form["AmenitiesId"];

                    if (AmenitiesIds != null)
                    {
                        string[] allAmenIds = AmenitiesIds.Split(',');
                        foreach (var eachAmenId in allAmenIds)
                        {
                            RoomAmenities eachRoomAmen = new RoomAmenities()
                            {
                                RoomId = roomAmenities.RoomId,
                                AmenitiesId = Convert.ToInt32(eachAmenId)
                            };
                            allRoomAmenities.Add(eachRoomAmen);
                        }
                    }

                    if (roomAmenities.Id == 0)
                        _hotelService.CreateRoomAmenities(allRoomAmenities);

                    return RedirectToAction("ManageRoomAmenities", "Hotel", new { area = "admin", id = roomAmenities.RoomId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignRoomAmenities = _hotelService.GetUnassignAmenities(roomAmenities.RoomId).Select(
                     r => new
                     {
                         AmenitiesId = r.Id,
                         Name = r.Name
                     }).ToList();
                    ViewBag.Amenities = new MultiSelectList(unassignRoomAmenities, "AmenitiesId", "Name");
                    return View(roomAmenities);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignRoomAmenities = _hotelService.GetUnassignAmenities(roomAmenities.RoomId).Select(
                    r => new
                    {
                        AmenitiesId = r.Id,
                        Name = r.Name
                    }).ToList();
                ViewBag.Amenities = new MultiSelectList(unassignRoomAmenities, "AmenitiesId", "Name");
                return View(roomAmenities);
            }
        }

        /// <summary>
        /// Deletes the room amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RoomAmenities.</returns>
        [HttpPost]
        public ActionResult DeleteRoomAmenities(int id)
        {
            try
            {
                _hotelService.DeleteRoomAmenities(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This amenities already in use." });
            }
        }

        #endregion Manage Room Amenities

        #region Hotel Receptionist
        /// <summary>
        /// Hotels the reception dashboard.
        /// </summary>
        /// <returns>ActionResult Hotel ReceptionDashboard.</returns>
        public ActionResult HotelReceptionDashboard()
        {
            return View();
        }
        /// <summary>
        /// Gets the hotel current room bookings.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>ActionResult Get Hotel Current RoomBookings.</returns>
        public ActionResult GetHotelCurrentRoomBookings(int? page)
        {
            var hotelId = GetActiveHotelId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int total;

            var booking = _hotelService.GetAllCurrentRoomBookings(hotelId, pageNumber, pageSize, out total);
            return View("HotelCurrentRoomBookings", new StaticPagedList<CustomerHotelBooking>(booking, pageNumber, pageSize, total));
        }
        #endregion Hotel Receptionist

        #region Spa Manager
        /// <summary>
        /// Hotels the spa manager dashboard.
        /// </summary>
        /// <returns>ActionResult Hotel SpaManager Dashboard.</returns>
        public ActionResult HotelSpaManagerDashboard()
        {
            return View();
        }
        #endregion Spa Manager

        #region ManageRoomType

        /// <summary>
        /// Manages the room types.
        /// </summary>
        /// <returns>ActionResult Manage RoomTypes.</returns>
        public ActionResult ManageRoomTypes()
        {
            var roomTypes = _hotelService.GetAllRoomTypes();
            return View(roomTypes);
        }

        /// <summary>
        /// Adds the type of the room.
        /// </summary>
        /// <returns>ActionResult Add RoomType.</returns>
        public ActionResult AddRoomType()
        {
            RoomType roomType = new RoomType();
            return View("AddEditRoomType", roomType);
        }

        /// <summary>
        /// Saves the room types.
        /// </summary>
        /// <param name="roomType">Type of the room.</param>
        /// <returns>ActionResult Save RoomTypes.</returns>
        public ActionResult SaveRoomTypes(RoomType roomType)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (roomType.Id == 0)
                    {
                        _hotelService.CreateRoomType(roomType);
                        return RedirectToAction("ManageRoomTypes");
                    }
                    else
                    {
                        _hotelService.ModifyRoomType(roomType);
                        return RedirectToAction("ManageRoomTypes");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddEditRoomType", roomType);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRoomType", roomType);
            }
        }

        /// <summary>
        /// Edits the type of the room.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult Edit RoomType.</returns>
        public ActionResult EditRoomType(int Id)
        {
            var roomTypes = _hotelService.GetRoomTypeById(Id);
            return View("AddEditRoomType", roomTypes);
        }

        /// <summary>
        /// Deletes the type of the room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RoomType.</returns>
        [HttpPost]
        public ActionResult DeleteRoomType(int id)
        {
            try
            {
                _hotelService.DeleteRoomType(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This room type cannot be deleted, bacause it is already in use" });
            }
        }

        #endregion ManageRoomType

        #region Manage Garments

        /// <summary>
        /// Manages the garments.
        /// </summary>
        /// <returns>ActionResult Manage Garments.</returns>
        public ActionResult ManageGarments()
        {
            return View();
        }

        /// <summary>
        /// Loads the garments.
        /// </summary>
        /// <returns>ActionResult Load Garments.</returns>
        [HttpPost]
        public ActionResult LoadGarments()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var garments = _hotelService.GetAllGarments(skip, pageSize, out Total);
            var data = garments.Select(b => new
            {
                GarmentId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.Garments, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the garments.
        /// </summary>
        /// <returns>ActionResult Add Garments.</returns>
        public ActionResult AddGarments()
        {
            Garments garment = new Garments();
            return View("AddEditGarments", garment);
        }

        /// <summary>
        /// Saves the garments.
        /// </summary>
        /// <param name="garment">The garment.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save new Garments.</returns>
        public ActionResult SaveGarments(Garments garment, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                garment.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditGarments", garment);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditGarments", garment);
                        }
                    }
                    if (garment.Id == 0)
                    {
                        var garments = _hotelService.CreateGarments(garment);
                        if (uploadImage != null)
                            ImageHelper.UploadImage(garments.Id, ImagePathFor.Garments, uploadImage, api_path);
                        return RedirectToAction("ManageGarments");
                    }
                    else
                    {
                        var garments = _hotelService.ModifyGarments(garment);
                        if (uploadImage != null)
                            ImageHelper.UploadImage(garments.Id, ImagePathFor.Garments, uploadImage, api_path);
                        return RedirectToAction("ManageGarments");
                    }
                }
                else
                {
                    return View("AddEditGarments", garment);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditGarments", garment);
            }
        }

        /// <summary>
        /// Edits the garments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit Garments.</returns>
        public ActionResult EditGarments(int id)
        {
            var garment = _hotelService.GetGarmentsById(id);
            return View("AddEditGarments", garment);
        }
        /// <summary>
        /// Deletes the garments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete Garments.</returns>
        [HttpPost]
        public ActionResult DeleteGarments(int id)
        {
            try
            {
                _hotelService.DeleteGarments(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This garment detail already in use." });
            }
        }

        #endregion Manage Garments

        #region Manage ConciergeGroup

        /// <summary>
        /// Manages the concierge group.
        /// </summary>
        /// <returns>ActionResult Manage ConciergeGroup.</returns>
        public ActionResult ManageConciergeGroup()
        {
            return View();
        }

        /// <summary>
        /// Loads the concierge group.
        /// </summary>
        /// <returns>ActionResult Load ConciergeGroup.</returns>
        [HttpPost]
        public ActionResult LoadConciergeGroup()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var hotelId = GetActiveHotelId();

            var conciergeGroup = _hotelService.GetAllConciergeGroup(hotelId, skip, pageSize, out Total);
            var data = conciergeGroup.Select(b => new
            {
                ConciergeGroupId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the concierge group.
        /// </summary>
        /// <returns>ActionResult Add ConciergeGroup.</returns>
        public ActionResult AddConciergeGroup()
        {
            ConciergeGroup conciergeGroup = new ConciergeGroup();
            return View("AddEditConciergeGroup", conciergeGroup);
        }

        /// <summary>
        /// Saves the concierge group.
        /// </summary>
        /// <param name="conciergeGroup">The concierge group.</param>
        /// <returns>ActionResult Save new created ConciergeGroup.</returns>
        public ActionResult SaveConciergeGroup(ConciergeGroup conciergeGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (conciergeGroup.Id == 0)
                    {
                        conciergeGroup.HotelId = GetActiveHotelId();
                        _hotelService.CreateConciergeGroup(conciergeGroup);
                        return RedirectToAction("ManageConciergeGroup");
                    }
                    else
                    {
                        _hotelService.ModifyConciergeGroup(conciergeGroup);
                        return RedirectToAction("ManageConciergeGroup");
                    }
                }
                else
                {
                    return View("AddEditConciergeGroup", conciergeGroup);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditConciergeGroup", conciergeGroup);
            }
        }

        /// <summary>
        /// Edits the concierge group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditConciergeGroup(int id)
        {
            var conciergeGroup = _hotelService.GetConciergeGroupById(id);
            return View("AddEditConciergeGroup", conciergeGroup);
        }

        /// <summary>
        /// Deletes the concierge group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteConciergeGroup(int id)
        {
            try
            {
                _hotelService.DeleteConciergeGroup(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This concierge group detail already in use." });
            }
        }

        #endregion Manage ConciergeGroup

        #region Manage Concierge

        /// <summary>
        /// Manages the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageConcierge(int id)
        {
            ViewBag.ConciergeGroupId = id;
            return View();
        }

        /// <summary>
        /// Loads the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load oncierge.</returns>
        [HttpPost]
        public ActionResult LoadConcierge(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var concierge = _hotelService.GetAllConciergeByGroupId(id, skip, pageSize, out Total);
            var data = concierge.Select(b => new
            {
                ConciergeId = b.Id,
                Question = b.Question,
                Answer = b.Answer,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the concierge.
        /// </summary>
        /// <param name="conciergeGroupId">The concierge group identifier.</param>
        /// <returns>ActionResult Add Concierge.</returns>
        public ActionResult AddConcierge(int conciergeGroupId)
        {
            Concierge concierge = new Concierge()
            {
                ConciergeGroupId = conciergeGroupId
            };
            return View("AddEditConcierge", concierge);
        }


        /// <summary>
        /// Saves the concierge.
        /// </summary>
        /// <param name="concierge">The concierge.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveConcierge(Concierge concierge)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (concierge.Id == 0)
                    {
                        _hotelService.CreateConcierge(concierge);
                        return RedirectToAction("ManageConcierge", new { id = concierge.ConciergeGroupId });
                    }
                    else
                    {
                        _hotelService.ModifyConcierge(concierge);
                        return RedirectToAction("ManageConcierge", new { id = concierge.ConciergeGroupId });
                    }
                }
                else
                {
                    return View("AddEditConcierge", concierge);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditConcierge", concierge);
            }
        }

        /// <summary>
        /// Edits the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditConcierge(int id)
        {
            var concierge = _hotelService.GetConciergeById(id);
            return View("AddEditConcierge", concierge);
        }

        /// <summary>
        /// Deletes the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteConcierge(int id)
        {
            try
            {
                _hotelService.DeleteConcierge(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This concierge detail already in use." });
            }
        }

        #endregion Manage Concierge

        #region Manage HotelRoom Customers

        /// <summary>
        /// Manages the hotel room customers.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageHotelRoomCustomers()
        {
            return View();
        }

        /// <summary>
        /// Loads the hotel room customers.
        /// </summary>
        /// <returns>Load Hotel RoomCustomers.</returns>
        [HttpPost]
        public ActionResult LoadHotelRoomCustomers()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            int hotelId = GetActiveHotelId();

            var hotelRoomCustomers = _hotelService.GetRoomsActiveCustomers(hotelId, skip, pageSize, out Total);
            var data = hotelRoomCustomers.Select(b => new
            {
                CustomerHotelRoomId = b.Id,
                RoomNumber = b.Room.Number,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates the hotel room customers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Update Hotel RoomCustomers.</returns>
        [HttpPost]
        public ActionResult UpdateHotelRoomCustomers(int id)
        {
            try
            {
                _hotelService.ModifyCustomerHotelRooom(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "Customer already in use." });
            }
        }

        #endregion Manage HotelRoom Customers

        #region Manage WakeUp

        /// <summary>
        /// Manages the wake up call.
        /// </summary>
        /// <returns>view wakeup call for manage.</returns>
        public ActionResult ManageWakeUpCall()
        {
            return View();
        }

        /// <summary>
        /// Loads the wake upcall.
        /// </summary>
        /// <returns>Load all wakeup call.</returns>
        [HttpPost]
        public ActionResult LoadWakeUpcall()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            int hotelId = GetActiveHotelId();

            var wakeUpcall = _hotelService.GetHotelRoomWakeUpCall(hotelId, skip, pageSize, out Total);
            var data = wakeUpcall.Select(b => new
            {
                WakeUpId = b.Id,
                RoomNumber = b.Room.Number,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                ScheduledDate = b.ScheduledDate,
                Method = ((WakeUpMethod)b.Method).ToString(),
                IsReapeated = b.IsRepeated,
                Interval = b.Interval,
                Comment = b.Comment,
                IsCompleted = b.IsCompleted
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates the wake up call.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Update wakeup call.</returns>
        [HttpPost]
        public ActionResult UpdateWakeUpCall(int id)
        {
            try
            {
                _hotelService.ModifyWakeUpCall(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "WakeUp call already in use." });
            }
        }
        #endregion Manage WakeUp
    }
}