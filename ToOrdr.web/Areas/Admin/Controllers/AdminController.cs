﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-17-2019
// ***********************************************************************
// <copyright file="AdminController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Core.Util;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class AdminController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    [Authorize(Roles = "Admin , ChainAdmin, RestaurantAdmin")]

    public class AdminController : BaseController
    {
        /// <summary>
        /// The API path
        /// </summary>
        string api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];

        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The about youordr service
        /// </summary>
        IAboutYouOrdrService _aboutYouOrdrService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        /// <param name="customerService">The customer service.</param>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="aboutYouOrdrService">The about youordr service.</param>
        public AdminController(IRestaurantService restaurantService, ICustomerService customerService, IHotelService hotelService, IAboutYouOrdrService aboutYouOrdrService)
        {
            _restaurantService = restaurantService;
            _customerService = customerService;
            _hotelService = hotelService;
            _aboutYouOrdrService = aboutYouOrdrService;
        }
        // GET: Admin/Admin
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Admin Dashboards.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Dashboard()
        {
            return View();
        }

        /// <summary>
        /// Orders the history.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult OrdersHistory()
        {
            return View();
        }

        /// <summary>
        /// Loads the orders history.
        /// </summary>
        /// <returns>Returns All Order History.</returns>
        [HttpPost]
        public ActionResult LoadOrdersHistory()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var restaurantId = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);
            var restId = restaurantId == "" ? 0 : Convert.ToInt32(restaurantId);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restOrderHistory = _restaurantService.GetRestaurantOrderHistory(fromDate, toDate, restId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = restOrderHistory.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Restaurants recent orders.
        /// </summary>
        /// <returns>View all recent orders.</returns>
        public ActionResult RestaurantsRecentOrders()
        {
            return View();
        }

        /// <summary>
        /// Loads the restaurants recent orders.
        /// </summary>
        /// <returns>Load Restaurants Recent Orders.</returns>
        [HttpPost]
        public ActionResult LoadRestaurantsRecentOrders()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var restId = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            int? selectedRestId;
            if (restId == "")
                selectedRestId = null;
            else
                selectedRestId = Convert.ToInt32(restId);
            var restRecentOrders = _restaurantService.GetRestRecentOrders(selectedRestId, skip, pageSize, out Total);
            var data = restRecentOrders.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RestaurantName = b.Restaurant.Name,
                BookingDate = b.ScheduledDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Restaurants the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix should restaurant name keywords.</param>
        /// <returns>Restaurant AutoComplete.</returns>
        [HttpPost]
        public JsonResult RestaurantAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetAllRestaurantsByName(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });

            return Json(restaurants, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Hotels the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult of Hotel AutoComplete.</returns>
        [HttpPost]
        public JsonResult HotelAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var hotels = _hotelService.GetAllHotelsByName(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });

            return Json(hotels, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Chains the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult of Chain AutoComplete.</returns>
        public JsonResult ChainAutoComplete(string prefix)
        {
            var chains = _restaurantService.GetAllChainsByName(prefix).Select(c => new { Id = c.Id, Name = c.ChainName });
            return Json(chains, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Authorizeds users.
        /// </summary>
        /// <returns>List Authorized Users.</returns>
        public ActionResult AuthorizedUsers()
        {
            var customer = GetAuthenticatedCustomer();
            var authorizedUsers = _restaurantService.AuthorizedCustomers(customer.Id);
            return View(authorizedUsers);
        }

        /// <summary>
        /// Authorize the restaurant users.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>List Authorized Restaurant Users.</returns>
        public ActionResult AuthorizedRestaurantUsers(int id)
        {
            var customer = GetAuthenticatedCustomer();
            var authorizedRestUsers = _restaurantService.AuthorizedRestaurantCustomers(customer.Id, id);
            return PartialView("_AuthorizedUsersResult", authorizedRestUsers);
        }

        /// <summary>
        /// Authorizeds the hotel users.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>List Authorized Hotel Users.</returns>
        public ActionResult AuthorizedHotelUsers(int id)
        {
            var customer = GetAuthenticatedCustomer();
            var authorizedHotelUsers = _hotelService.AuthorizedHotelCustomers(customer.Id, id);
            return PartialView("_AuthorizedUsersResult", authorizedHotelUsers);
        }

        /// <summary>
        /// Manages the deleted customer.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageDeletedCustomer()
        {
            return View();
        }
        /// <summary>
        /// Loads all deleted customers.
        /// </summary>
        /// <returns>Load all deleted customer.</returns>
        [HttpPost]
        public ActionResult LoadAllDeletedCustomers()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var deletedCustomers = _customerService.GetAllDeletedCustomers(skip, pageSize, out Total);
            var data = deletedCustomers.Select(b => new
            {
                CustomerId = b.Id,
                CustomerName = b.FirstName + " " + b.LastName,
                BirthDate = b.BirthDate.ToString(),
                Gender = b.Gender,
                City = b.City,
                Region = b.Region,
                Pinncode = b.Pincode,
                Telephone = b.PhoneCode + " " + b.Telephone,
                Email = b.Email
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <returns>View page for creating customer role.</returns>
        public ActionResult CreateCustomerRole()
        {
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

            ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
            CustomerRoleViewModel customerRole = new CustomerRoleViewModel();
            return View("CreateEditCustomer", customerRole);
        }

        /// <summary>
        /// Customers the specified term.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult customers(string term)
        {
            var result = _restaurantService.GetCustomers(term).Select(r => new { id = r.Id, Name = r.FirstName + " " + r.LastName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>Create new Customer role.</returns>
        [HttpPost]
        public ActionResult CreateCustomerRole(CustomerRoleViewModel customerRole)
        {
            //Get role for Login Customer/User
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    if (customerRole.RoleId == 1 || customerRole.RoleId == 8 || customerRole.RestaurantId != null || customerRole.HotelId != null)
                    {
                        string passwordCode = Guid.NewGuid().ToString();

                        GenderType genderType = (GenderType)Convert.ToInt32(customerRole.Customer.Gender);
                        Customer newCustomer = new Customer()
                        {
                            FirstName = customerRole.Customer.FirstName,
                            LastName = customerRole.Customer.LastName,
                            BirthDate = customerRole.Customer.BirthDate,
                            Email = customerRole.Customer.Email,
                            Password = Hashing.HashPassword(passwordCode),
                            PhoneCode = customerRole.Customer.PhoneCode,
                            Telephone = customerRole.Customer.Telephone,
                            City = customerRole.Customer.City,
                            Street = customerRole.Customer.Street,
                            Street2 = customerRole.Customer.Street2,
                            Region = customerRole.Customer.Region,
                            CountryId = customerRole.Customer.CountryId,
                            Pincode = customerRole.Customer.Pincode,
                            CurrencyId = customerRole.Customer.CurrencyId,
                            Gender = genderType.ToString() == "0" ? null : genderType.ToString()
                        };

                        newCustomer.CreationDate = System.DateTime.Now;
                        newCustomer.ActiveStatus = true;

                        string activationCode = Guid.NewGuid().ToString();
                        newCustomer.ActivationCode = activationCode;

                        var customer = _customerService.CreateCustomer(newCustomer);

                        CustomerRole role = new CustomerRole()
                        {
                            CustomerId = customer.Id,
                            RoleId = customerRole.RoleId,
                            HotelId = customerRole.HotelId,
                            RestaurantId = customerRole.RestaurantId,
                            ChainId = customerRole.ChainId
                        };
                        _restaurantService.CreateCustomerRole(role);

                        SendPasswordOnEmail(customer, passwordCode);

                        return RedirectToAction("AuthorizedUsers");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, Resources.Rqd_Restaurant);
                        ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                        ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                        ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

                        ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
                        return View("CreateEditCustomer", customerRole);
                    }
                }
                else
                {
                    ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                    ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                    ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

                    ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });

                ViewBag.GenderType = from GenderType e in Enum.GetValues(typeof(GenderType)) select new { Id = (int)e, Name = e.ToString() };
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Update customer role.</returns>
        public ActionResult UpdateCustomerRole(int id)
        {
            var customerRole = _restaurantService.GetCustomerByRoleId(id);
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
            RegisterCustomerVM vModel = new RegisterCustomerVM()
            {
                FirstName = customerRole.Customer.FirstName,
                LastName = customerRole.Customer.LastName
            };

            CustomerRoleViewModel model = new CustomerRoleViewModel()
            {
                Id = customerRole.Id,
                RoleId = customerRole.RoleId,
                CustomerId = customerRole.CustomerId,
                HotelId = customerRole.HotelId,
                RestaurantId = customerRole.RestaurantId,
                ChainId = customerRole.ChainId,

                RestaurantName = customerRole.Restaurant != null ? customerRole.Restaurant.Name : null,
                HotelName = customerRole.Hotel != null ? customerRole.Hotel.Name : null,
                ChainTableName = customerRole.ChainTable != null ? customerRole.ChainTable.ChainName : null,
                Customer = vModel
            };
            return View("CreateEditCustomer", model);
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateCustomerRole(CustomerRoleViewModel customerRole)
        {
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            try
            {
                ModelState["Customer.CurrencyId"].Errors.Clear();
                ModelState["Customer.PhoneCode"].Errors.Clear();
                ModelState["Customer.Telephone"].Errors.Clear();
                ModelState["Customer.Email"].Errors.Clear();
                ModelState["Customer.LastName"].Errors.Clear();
                ModelState["Customer.CountryId"].Errors.Clear();

                if (ModelState.IsValid)
                {
                    int[] restarantRoleIds = { 2, 3, 4 }; // these are static values from DB
                    int[] hotelRoleIds = { 5, 6, 7, 9, 10, 11 }; // these are static values from DB

                    if (restarantRoleIds.Contains(customerRole.RoleId) && customerRole.RestaurantId == null)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Rqd_Restaurant);
                        ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                        return View("CreateEditCustomer", customerRole);
                    }
                    else if (hotelRoleIds.Contains(customerRole.RoleId) && customerRole.HotelId == null)
                    {
                        ModelState.AddModelError(string.Empty, "Hotel must required");
                        ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                        return View("CreateEditCustomer", customerRole);
                    }

                    var custRole = _restaurantService.GetCustomerRole(customerRole.CustomerId);
                    CustomerRole custsRole = new CustomerRole()
                    {
                        Id = customerRole.Id,
                        CustomerId = customerRole.CustomerId,
                        RoleId = customerRole.RoleId,
                        HotelId = custRole.FirstOrDefault().HotelId != null && customerRole.RestaurantId != null ? null : customerRole.HotelId,
                        RestaurantId = custRole.FirstOrDefault().RestaurantId != null && customerRole.HotelId != null ? null : customerRole.RestaurantId,
                        ChainId = customerRole.RoleId != 8 ? null : customerRole.ChainId,
                        ChainTable = customerRole.RoleId != 8 ? null : custRole.FirstOrDefault().ChainTable
                    };

                    if (customerRole.RoleId == 1)
                    {
                        custsRole.HotelId = null;
                        custsRole.RestaurantId = null;
                    }
                    _restaurantService.UpdateCustomerRole(custsRole);
                    return RedirectToAction("AuthorizedUsers");
                }
                else
                {
                    ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _restaurantService.GetRoleForChainAdmin(customerRoleId).Select(rl => new { Id = rl.Id, Name = rl.Name });
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Delete  cusstomer role.</returns>
        [HttpPost]
        public ActionResult DeleteCustomerRole(int id)
        {
            try
            {
                _restaurantService.DeleteCustomerRole(id);
                return RedirectToAction("AuthorizedUsers");
            }
            catch
            {
                return View("AuthorizedUsers");
            }
        }

        /// <summary>
        /// Sends the password on email.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="passwordCode">The password code.</param>
        private void SendPasswordOnEmail(Customer customer, string passwordCode)
        {
            try
            {
                //todo: make it asynchronnious call and template based

                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", customer.Email.ToString()))
                {
                    mm.Subject = "Account Password";
                    string body = "Hello " + customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please use below password to login your account";
                    body += "Password " + passwordCode;
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }


        /// <summary>
        /// Restaurantses the register last week.
        /// </summary>
        /// <returns>restaurant registors at last week.</returns>
        public ActionResult RestaurantsRegisterLastWeek()
        {
            DateTime lastWeek = DateTime.Now;
            lastWeek = lastWeek.AddDays(-7.0);
            var restaurants = _restaurantService.GetAllLastWeekRestaurants(lastWeek, DateTime.Now);
            var message = "";
            if (restaurants.Count() <= 0)
            {
                restaurants = _restaurantService.GetAllRestaurants();
                message = "No Registration Found, Below are previously registerd restaurants";
            }
            ViewBag.Message = message;
            return PartialView("_RestaurantsRegisterLastWeek", restaurants);
        }
        /// <summary>
        /// Todayses the top performing restaurant.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TodaysTopPerformingRestaurant()
        {
            var restaurantOrders = _restaurantService.GetTodaysOrders();
            return PartialView("_TodaysTopPerformingRestaurant", restaurantOrders);
        }
        /// <summary>
        /// Todayses the business from restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TodaysBusinessFromRestaurants()
        {
            var restaurantOrders = _restaurantService.GetTodaysOrders();
            return PartialView("_TodaysBusinessFromRestaurants", restaurantOrders);
        }
        /// <summary>
        /// Restaurantses the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RestaurantsReviews()
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantReviews = _restaurantService.GetAllRestaurantsReviews(customer.Id);
            return PartialView("_RestaurantsReviews", restaurantReviews);
        }

        /// <summary>
        /// Manages the restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageRestaurants()
        {
            var restaurants = _restaurantService.GetAllNonVirtualRestaurants();
            return View(restaurants);
        }
        /// <summary>
        /// Adds the restaurant.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddRestaurant()
        {
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
            Restaurant restaurants = new Restaurant();
            return View("AddEditRestaurant", restaurants);
        }
        /// <summary>
        /// Saves the restaurant details.
        /// </summary>
        /// <param name="restaurants">The restaurants.</param>
        /// <param name="uploadBackground">The upload background.</param>
        /// <param name="uploadLogo">The upload logo.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveRestaurantDetails(Restaurant restaurants, HttpPostedFileBase uploadBackground, HttpPostedFileBase uploadLogo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                    ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
                    if (!string.IsNullOrWhiteSpace(restaurants.Email))
                    {
                        bool isEmail = Regex.IsMatch(restaurants.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                        if (!isEmail)
                        {
                            ModelState.AddModelError("Email", Resources.Valid_Email);
                            return View("AddEditRestaurant", restaurants);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(restaurants.Telephone))
                    {
                        bool isPhone = Regex.IsMatch(restaurants.Telephone, @"^\d{1,15}$");
                        if (!isPhone)
                        {
                            ModelState.AddModelError("Telephone", Resources.Valid_TelephoneNumber);
                            return View("AddEditRestaurant", restaurants);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(restaurants.Fax))
                    {
                        bool isFax = Regex.IsMatch(restaurants.Fax, @"\+[0-9]{1,2}\([0-9]{3}\)[0-9]{7}");
                        if (!isFax)
                        {
                            ModelState.AddModelError("Fax", Resources.Valid_FaxNumber + ", ex: +23(333)3652113 / +2(333)3652113");
                            return View("AddEditRestaurant", restaurants);
                        }
                    }
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var backgroundImageFilename = restaurants.BackgroundImage;
                    if (uploadBackground != null)
                    {
                        if (uploadBackground.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadBackground.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadBackground.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                backgroundImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", "Background " + Resources.Invalid_ImageExtension);
                                return View("AddEditRestaurant", restaurants);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Background " + Resources.ImageUploadSizeExceeded);
                            return View("AddEditRestaurant", restaurants);
                        }
                    }

                    var logoImageFilename = restaurants.LogoImage;
                    if (uploadLogo != null)
                    {
                        if (uploadLogo.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadLogo.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadLogo.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                logoImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", "Logo " + Resources.Invalid_ImageExtension);
                                return View("AddEditRestaurant", restaurants);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Logo " + Resources.ImageUploadSizeExceeded);
                            return View("AddEditRestaurant", restaurants);
                        }
                    }

                    if (restaurants.Id == 0)
                    {
                        restaurants.IsActive = true;
                        restaurants.BackgroundImage = backgroundImageFilename;
                        restaurants.LogoImage = logoImageFilename;
                        var restaurant = _restaurantService.CreateRestaurant(restaurants);
                        if (uploadBackground != null)
                        {
                            ImageHelper.UploadImage(restaurant.Id, ImagePathFor.RestaurantBackground, uploadBackground, api_path);
                        }
                        if (uploadLogo != null)
                        {
                            ImageHelper.UploadImage(restaurant.Id, ImagePathFor.RestaurantLogo, uploadLogo, api_path);
                        }

                        return RedirectToAction("ManageRestaurants");
                    }
                    else
                    {
                        if (uploadBackground != null)
                        {
                            ImageHelper.UploadImage(restaurants.Id, ImagePathFor.RestaurantBackground, uploadBackground, api_path);
                        }
                        if (uploadLogo != null)
                        {
                            ImageHelper.UploadImage(restaurants.Id, ImagePathFor.RestaurantLogo, uploadLogo, api_path);
                        }

                        restaurants.BackgroundImage = backgroundImageFilename;
                        restaurants.LogoImage = logoImageFilename;
                        var rest = _restaurantService.ModifyRestaurant(restaurants);

                        if (Session["RestaurantAdmin"] != null && Session["RestaurantAdmin"].ToString() == "true")
                            return RedirectToAction("EditRestaurant", new { restaurant_id = restaurants.Id });
                        else
                        {
                            var restDet = _restaurantService.GetRestaurantById(rest.Id);
                            if (restDet.HotelRestaurants.Count() < 1)
                                return RedirectToAction("ManageRestaurants");
                            else
                                return RedirectToAction("ManageVirtualRestaurants");
                        }

                    }
                }
                else
                {
                    ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                    ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
                    return View("AddEditRestaurant", restaurants);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
                return View("AddEditRestaurant", restaurants);
            }
        }
        /// <summary>
        /// Edits the restaurant.
        /// </summary>
        /// <param name="restaurant_id">The restaurant identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditRestaurant(int? restaurant_id)
        {
            if (restaurant_id == null)
                restaurant_id = GetActiveRestaurantId();

            var restaurant = _restaurantService.GetRestaurantById(Convert.ToInt32(restaurant_id));
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
            return View("AddEditRestaurant", restaurant);
        }
        /// <summary>
        /// Deletes the restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteRestaurant(int id)
        {
            try
            {
                var restaurant = _restaurantService.GetRestaurantById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];

                _restaurantService.DeleteRestaurant(restaurant.Id);

                if (!string.IsNullOrWhiteSpace(restaurant.BackgroundImage))
                {
                    var imgPath = @"\Images\Restaurant\" + restaurant.Id.ToString() + @"\Background\" + restaurant.BackgroundImage;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                if (!string.IsNullOrWhiteSpace(restaurant.LogoImage))
                {
                    var imgPath = @"\Images\Restaurant\" + restaurant.Id.ToString() + @"\Background\" + restaurant.LogoImage;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }

                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This restaurant is already in use." });
            }
        }
        /// <summary>
        /// Deletes the hotel restaurant by rest identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteHotelRestaurantByRestId(int id)
        {
            try
            {
                var hotelRest = _hotelService.GetHotelRestaurantByRestId(id);
                _hotelService.DeleteHotelRestaurant(hotelRest.Id);

                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This hotel restaurant is already in use." });
            }
        }
        #region Manage Virtual Restaurants
        /// <summary>
        /// Manages the virtual restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageVirtualRestaurants()
        {
            return View();
        }
        /// <summary>
        /// Loads the virtual restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadVirtualRestaurants()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var virtualRestaurants = _restaurantService.GetAllVirtualRestaurants(skip, pageSize, out Total);
            var data = virtualRestaurants.Select(b => new
            {
                VirtualRestId = b.Id,
                HotelName = b.HotelRestaurants.FirstOrDefault().Hotel.Name,
                Name = b.Name,
                Description = b.Description,
                City = b.City,
                Region = b.Region,
                Country = b.CountryId == null ? "" : b.Country.Name,
                Currency = b.Currency.Code,
                Telephone = b.Telephone,
                CostForTwo = b.CostForTwo,
                BackGroundImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.RestaurantBackground, b.BackgroundImage, b.Id, api_path),
                LogoImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.RestaurantLogo, b.LogoImage, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        #endregion Manage Virtual Restaurants


        /// <summary>
        /// Updates the restaurant is active status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateRestaurantIsActiveStatus(int id)
        {
            var restaurant = _restaurantService.GetRestaurantById(id);
            if (restaurant.IsActive)
                restaurant.IsActive = false;
            else
                restaurant.IsActive = true;
            _restaurantService.ModifyRestaurant(restaurant);

            return Json(new { status = true });
        }
        /// <summary>
        /// Manages the hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var hotels = _hotelService.GetAllUserHotels(customer.Id);
            foreach (var hotel in hotels)
            {
                hotel.QRCodeImage = Url.Content("/Images/Hotels/") + hotel.Id.ToString() + "/HotelQRCodes/" + hotel.QRCodeImage;
            }
            return View(hotels);
        }
        /// <summary>
        /// Adds the hotel.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddHotel()
        {
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
            Hotel hotel = new Hotel();
            return View("AddEditHotel", hotel);
        }
        /// <summary>
        /// Saves the hotel details.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="uploadBackground">The upload background.</param>
        /// <param name="uploadLogo">The upload logo.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveHotelDetails(Hotel hotel, HttpPostedFileBase uploadBackground, HttpPostedFileBase uploadLogo)
        {
            var countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            var currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
            ViewBag.Countries = countries;
            ViewBag.Currencies = currencies;
            try
            {
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrWhiteSpace(hotel.Email))
                    {
                        bool isEmail = Regex.IsMatch(hotel.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                        if (!isEmail)
                        {
                            ModelState.AddModelError("Email", Resources.Valid_Email);
                            return View("AddEditHotel", hotel);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(hotel.Telephone))
                    {
                        bool isPhone = Regex.IsMatch(hotel.Telephone, @"^\d{1,15}$");
                        if (!isPhone)
                        {
                            ModelState.AddModelError("Telephone", Resources.Valid_TelephoneNumber);
                            return View("AddEditHotel", hotel);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(hotel.Fax))
                    {
                        bool isFax = Regex.IsMatch(hotel.Fax, @"\+[0-9]{1,2}\([0-9]{3}\)[0-9]{7}");
                        if (!isFax)
                        {
                            ModelState.AddModelError("Fax", Resources.Valid_FaxNumber + ", ex: +23(333)3652113 / +2(333)3652113");
                            return View("AddEditHotel", hotel);
                        }
                    }
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var backgroundImageFilename = hotel.BackgroundImage;
                    if (uploadBackground != null)
                    {
                        if (uploadBackground.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadBackground.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadBackground.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                backgroundImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", "Background " + Resources.Invalid_ImageExtension);
                                return View("AddEditHotel", hotel);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Background " + Resources.ImageUploadSizeExceeded);
                            return View("AddEditHotel", hotel);
                        }
                    }

                    var logoImageFilename = hotel.LogoImage;
                    if (uploadLogo != null)
                    {
                        if (uploadLogo.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadLogo.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadLogo.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                logoImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", "Logo " + Resources.Invalid_ImageExtension);
                                return View("AddEditHotel", hotel);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Logo " + Resources.ImageUploadSizeExceeded);
                            return View("AddEditHotel", hotel);
                        }
                    }

                    if (hotel.Id == 0)
                    {
                        hotel.IsActive = true;
                        hotel.BackgroundImage = backgroundImageFilename;
                        hotel.LogoImage = logoImageFilename;

                        hotel.QRCodeImage = "QrCode.jpg";
                        var hotl = _hotelService.CreateHotel(hotel);

                        string folderPath = "~/Images/Hotels/" + hotl.Id.ToString() + "/HotelQRCodes/";
                        string imagePath = "~/Images/Hotels/" + hotl.Id.ToString() + "/HotelQRCodes/QrCode.jpg";
                        var QRCodeImage = GenerateQRCode(hotl.Id.ToString(), folderPath, imagePath);

                        if (uploadBackground != null)
                        {
                            ImageHelper.UploadImage(hotl.Id, ImagePathFor.HotelBackground, uploadBackground, api_path);
                        }
                        if (uploadLogo != null)
                        {
                            ImageHelper.UploadImage(hotl.Id, ImagePathFor.HotelLogo, uploadLogo, api_path);
                        }

                        return RedirectToAction("ManageHotels");
                    }
                    else
                    {
                        if (uploadBackground != null)
                        {
                            ImageHelper.UploadImage(hotel.Id, ImagePathFor.HotelBackground, uploadBackground, api_path);
                        }
                        if (uploadLogo != null)
                        {
                            ImageHelper.UploadImage(hotel.Id, ImagePathFor.HotelLogo, uploadLogo, api_path);
                        }

                        hotel.BackgroundImage = backgroundImageFilename;
                        hotel.LogoImage = logoImageFilename;
                        _hotelService.ModifyHotel(hotel);
                        return RedirectToAction("ManageHotels");
                    }
                }
                else
                {
                    return View("AddEditHotel", hotel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHotel", hotel);
            }
        }
        /// <summary>
        /// Edits the hotel.
        /// </summary>
        /// <param name="hotel_id">The hotel identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditHotel(int hotel_id)
        {
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Name = c.Code });
            var hotel = _hotelService.GetHotelById(hotel_id);
            return View("AddEditHotel", hotel);
        }

        /// <summary>
        /// Updates the hotel is active status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateHotelIsActiveStatus(int id)
        {
            var hotel = _hotelService.GetHotelById(id);
            if (hotel.IsActive)
                hotel.IsActive = false;
            else
                hotel.IsActive = true;
            _hotelService.ModifyHotel(hotel);

            return Json(new { status = true });
        }

        /// <summary>
        /// Restaurants the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RestaurantReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the restaurant reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadRestaurantReviews()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var restId = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            int? selectedRestId;
            if (restId == "")
                selectedRestId = null;
            else
                selectedRestId = Convert.ToInt32(restId);

            var reviews = _restaurantService.GetRestaurantAllReviews(fromDate, toDate, selectedRestId, null, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                RestaurantName = b.Restaurant.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Hotelses the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult HotelsReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the hotel reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadHotelReviews()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var hotelId = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            int? selectedHotelId;
            if (hotelId == "")
                selectedHotelId = null;
            else
                selectedHotelId = Convert.ToInt32(hotelId);

            var reviews = _hotelService.GetAllHotelsReviews(fromDate, toDate, selectedHotelId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                HotelName = b.Hotel.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Pendings the registration.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult PendingRegistration()
        {
            var restaurants = _restaurantService.GetAllRestaurants().Where(r => r.ActiveStatus == false);
            var hotels = _hotelService.GetAllHotels().Where(h => h.Status == false);
            PendingRegistrationsVM pendingReg = new PendingRegistrationsVM()
            {
                Restaurant = restaurants,
                Hotel = hotels

            };
            return View(pendingReg);

        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateStatus(int Id, bool status)
        {
            var restDetails = _restaurantService.GetRestaurantById(Id);
            restDetails.ActiveStatus = status;
            var restaurant = _restaurantService.ModifyRestaurant(restDetails);
            return RedirectToAction("PendingRegistration");
        }
        /// <summary>
        /// Updates the hotel status.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns>ActionResult.</returns>
        public ActionResult UpdateHotelStatus(int Id, bool status)
        {
            var hotelDetails = _hotelService.GetHotelById(Id);
            hotelDetails.Status = status;
            var hotel = _hotelService.ModifyHotel(hotelDetails);
            return RedirectToAction("PendingRegistration");
        }

        /// <summary>
        /// Financials this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Financial()
        {
            return View();
        }

        /// <summary>
        /// Restaurants the wise monthly earnings.
        /// </summary>
        /// <returns>JsonResult.</returns>
        public JsonResult RestaurantWiseMonthlyEarnings()
        {
            var customer = GetAuthenticatedCustomer();
            var monthlyOrders = _restaurantService.GetRestaurantWiseMonthlyEarnings(customer.Id);
            var ret = new object[]
        {  
            new { label="Restaurant", data = monthlyOrders.Select(x=>new string[]{ x.RestaurantName})},
            new { label="Amount", data = monthlyOrders.Select(x=>new int[]{ Convert.ToInt32(x.TotalAmount)})},
        };
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Monthlies the order details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult MonthlyOrderDetails(int id)
        {
            var monthlyOrders = _restaurantService.GetLastTwelveMonthsOrders(id);
            var ret = new[]
        {  
            new { label="Month", data = monthlyOrders.Select(x=>new int[]{ x.Month})},
            new { label="Amount", data = monthlyOrders.Select(x=>new int[]{ Convert.ToInt32(x.TotalAmount)})},
        };
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the customers.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageCustomers()
        {
            return View();
        }
        /// <summary>
        /// Loads all customers.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadAllCustomers()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var name = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var email = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            var reviews = _customerService.GetAllCustomers(name, email, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                CustomerId = b.Id,
                CustomerName = b.FirstName + " " + b.LastName,
                Gender = b.Gender,
                DateOfBirth = Convert.ToDateTime(b.BirthDate).ToString("dd/MM/yyyy"),
                City = b.City,
                Region = b.Region,
                CountryName = b.CountryId != null ? b.Country.Name : "",
                Telephone = b.Telephone,
                Email = b.Email,
                CreationDate = Convert.ToDateTime(b.CreationDate).ToString("dd/MM/yyyy"),
                ProfileImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.ProfileImage, b.ProfileImage, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #region Cuisines
        /// <summary>
        /// Manages the cuisines.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageCuisines()
        {
            return View();
        }
        /// <summary>
        /// Loads the cuisines.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadCuisines()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var cuisineList = _restaurantService.GetAllCuisines(skip, pageSize, out Total);
            var data = cuisineList.Select(b => new
            {
                CuisineId = b.Id,
                Name = b.Name,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.Cuisines, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the cuisine.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddCuisine()
        {
            Cuisine cuisine = new Cuisine();
            return View("AddEditCuisine", cuisine);
        }

        /// <summary>
        /// Saves the cuisine.
        /// </summary>
        /// <param name="cuisine">The cuisine.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveCuisine(Cuisine cuisine, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                cuisine.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditCuisine", cuisine);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditCuisine", cuisine);
                        }
                    }
                    if (cuisine.Id == 0)
                    {
                        var result = _restaurantService.CreateCuisine(cuisine);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.Cuisines, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageCuisines");
                    }
                    else
                    {
                        var res = _restaurantService.ModifyCuisine(cuisine);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.Cuisines, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageCuisines");
                    }
                }
                else
                {
                    return View("AddEditCuisine", cuisine);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditCuisine", cuisine);
            }
        }

        /// <summary>
        /// Edits the cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditCuisine(int id)
        {
            var cuisine = _restaurantService.GetCuisineById(id);
            return View("AddEditCuisine", cuisine);
        }
        /// <summary>
        /// Deletes the cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteCuisine(int id)
        {
            try
            {
                _restaurantService.DeleteCuisine(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This cuisine already in use." });
            }
        }
        #endregion Cuisines

        #region Ingredient Categories
        /// <summary>
        /// Manages the ingredient categories.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageIngredientCategories()
        {
            return View();
        }
        /// <summary>
        /// Loads the ingredient categories.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadIngredientCategories()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var IngredientCategories = _restaurantService.GetAllIngredientCategories(skip, pageSize, out Total);
            var data = IngredientCategories.Select(b => new
            {
                IngredientCatId = b.Id,
                Name = b.Name,
                Description = b.Description,
                Type = (b.Type == false) ? "Food" : "Drink",
                DrinkType = (b.DrinkType == null) ? "None" : ((DrinksType)Convert.ToInt32(b.DrinkType)).ToString(),
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.IngredientCategories, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the ingredient category.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddIngredientCategory()
        {
            IngredientCategory ingredientCat = new IngredientCategory();
            var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                select new
                                {
                                    ID = Convert.ToBoolean((int)e),
                                    Name = e.ToString()
                                };
            ViewBag.DrinksEnumList = drinkEnumData;
            return View("AddEditIngredientCategory", ingredientCat);
        }

        /// <summary>
        /// Saves the ingredient category.
        /// </summary>
        /// <param name="IngredientCategory">The ingredient category.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveIngredientCategory(IngredientCategory IngredientCategory, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (IngredientCategory.Type == false)
                        IngredientCategory.DrinkType = null;

                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                IngredientCategory.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                                    select new
                                                    {
                                                        ID = Convert.ToBoolean((int)e),
                                                        Name = e.ToString()
                                                    };
                                ViewBag.DrinksEnumList = drinkEnumData;
                                return View("AddEditIngredientCategory", IngredientCategory);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                                select new
                                                {
                                                    ID = Convert.ToBoolean((int)e),
                                                    Name = e.ToString()
                                                };
                            ViewBag.DrinksEnumList = drinkEnumData;
                            return View("AddEditIngredientCategory", IngredientCategory);
                        }
                    }
                    if (IngredientCategory.Id == 0)
                    {
                        var result = _restaurantService.CreateIngredientCategory(IngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.IngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageIngredientCategories");
                    }
                    else
                    {
                        var res = _restaurantService.ModifyIngredientCategory(IngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.IngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageIngredientCategories");
                    }
                }
                else
                {
                    var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                        select new
                                        {
                                            ID = Convert.ToBoolean((int)e),
                                            Name = e.ToString()
                                        };
                    ViewBag.DrinksEnumList = drinkEnumData;
                    return View("AddEditIngredientCategory", IngredientCategory);
                }
            }
            catch (Exception ex)
            {
                var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                    select new
                                    {
                                        ID = Convert.ToBoolean((int)e),
                                        Name = e.ToString()
                                    };
                ViewBag.DrinksEnumList = drinkEnumData;
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditIngredientCategory", IngredientCategory);
            }
        }

        /// <summary>
        /// Edits the ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditIngredientCategory(int id)
        {
            var ingredientCategory = _restaurantService.GetIngredientCategoryById(id);
            var drinkEnumData = from DrinksType e in Enum.GetValues(typeof(DrinksType))
                                select new
                                {
                                    ID = Convert.ToBoolean((int)e),
                                    Name = e.ToString()
                                };
            ViewBag.DrinksEnumList = drinkEnumData;
            return View("AddEditIngredientCategory", ingredientCategory);
        }
        /// <summary>
        /// Deletes the ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteIngredientCategory(int id)
        {
            try
            {
                _restaurantService.DeleteIngredientCategory(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This ingredient category already in use." });
            }
        }
        #endregion Ingredient Categories

        #region Food Profile
        /// <summary>
        /// Manages the food profiles.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageFoodProfiles()
        {
            return View();
        }
        /// <summary>
        /// Loads the food profiles.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadFoodProfiles()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var foodProfiles = _restaurantService.GetAllFoodProfiles(skip, pageSize, out Total);
            var data = foodProfiles.Select(b => new
            {
                FoodProfileId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.FoodProfiles, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the food profile.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddFoodProfile()
        {
            FoodProfile foodProfile = new FoodProfile();
            return View("AddEditFoodProfile", foodProfile);
        }

        /// <summary>
        /// Saves the food profile.
        /// </summary>
        /// <param name="foodProfile">The food profile.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveFoodProfile(FoodProfile foodProfile, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                foodProfile.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditFoodProfile", foodProfile);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditFoodProfile", foodProfile);
                        }
                    }
                    if (foodProfile.Id == 0)
                    {
                        var result = _restaurantService.CreateFoodProfile(foodProfile);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.FoodProfiles, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageFoodProfiles");
                    }
                    else
                    {
                        var res = _restaurantService.ModifyFoodProfile(foodProfile);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.FoodProfiles, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageFoodProfiles");
                    }
                }
                else
                {
                    return View("AddEditFoodProfile", foodProfile);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditFoodProfile", foodProfile);
            }
        }

        /// <summary>
        /// Edits the food profile.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditFoodProfile(int id)
        {
            var foodProfile = _restaurantService.GetFoodProfileById(id);
            return View("AddEditFoodProfile", foodProfile);
        }
        /// <summary>
        /// Deletes the food profile.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteFoodProfile(int id)
        {
            try
            {
                _restaurantService.DeleteFoodProfile(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This food profile already in use." });
            }
        }
        #endregion Food Profile

        #region Allergen
        /// <summary>
        /// Manages the allergen groups.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageAllergenGroups()
        {
            return View();
        }
        /// <summary>
        /// Loads the allergen groups.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadAllergenGroups()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var allergenGroups = _restaurantService.GetAllAllergenCategories(skip, pageSize, out Total);
            var data = allergenGroups.Select(b => new
            {
                AllergenCatId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.AllergenCategories, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the allergen group.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddAllergenGroup()
        {
            AllergenCategory AllergenCat = new AllergenCategory();
            return View("AddEditAllergenGroup", AllergenCat);
        }

        /// <summary>
        /// Saves the allergen group.
        /// </summary>
        /// <param name="allergenCategory">The allergen category.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveAllergenGroup(AllergenCategory allergenCategory, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                allergenCategory.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditAllergenGroup", allergenCategory);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditAllergenGroup", allergenCategory);
                        }
                    }
                    if (allergenCategory.Id == 0)
                    {
                        var result = _restaurantService.CreateAllergenCategory(allergenCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.AllergenCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageAllergenGroups");
                    }
                    else
                    {
                        var res = _restaurantService.ModifyAllergenCategory(allergenCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.AllergenCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageAllergenGroups");
                    }
                }
                else
                {
                    return View("AddEditAllergenGroup", allergenCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditAllergenGroup", allergenCategory);
            }
        }

        /// <summary>
        /// Edits the allergen group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditAllergenGroup(int id)
        {
            var allergenCat = _restaurantService.GetAllergenCategoryById(id);
            return View("AddEditAllergenGroup", allergenCat);
        }
        /// <summary>
        /// Deletes the allergen group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteAllergenGroup(int id)
        {
            try
            {
                _restaurantService.DeleteAllergenCategory(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This allergen group already in use." });
            }
        }

        /// <summary>
        /// Manages the allergen.
        /// </summary>
        /// <param name="allergenGroupId">The allergen group identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageAllergen(int allergenGroupId)
        {
            ViewBag.AllergenGroupId = allergenGroupId;
            var allergens = _restaurantService.GetAllAllergen(allergenGroupId);
            return View(allergens);
        }

        /// <summary>
        /// Adds the allergen.
        /// </summary>
        /// <param name="allergenGroupId">The allergen group identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddAllergen(int allergenGroupId)
        {
            Allergen allergen = new Allergen();
            allergen.AllergenCategoryId = allergenGroupId;
            return View("AddEditAllergen", allergen);
        }

        /// <summary>
        /// Saves the allergen details.
        /// </summary>
        /// <param name="allergen">The allergen.</param>
        /// <param name="uploadAllergenImage">The upload allergen image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveAllergenDetails(Allergen allergen, HttpPostedFileBase uploadAllergenImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var allergenImageFilename = allergen.AllergenImage;
                    if (uploadAllergenImage != null)
                    {
                        if (uploadAllergenImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadAllergenImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadAllergenImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                allergenImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditAllergen", allergen);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditAllergen", allergen);
                        }
                    }

                    if (allergen.Id == 0)
                    {
                        allergen.AllergenImage = allergenImageFilename;
                        var allergens = _restaurantService.CreateAllergen(allergen);
                        if (uploadAllergenImage != null)
                        {
                            ImageHelper.UploadImage(allergens.Id, ImagePathFor.AllergenImage, uploadAllergenImage, api_path);
                        }
                        return RedirectToAction("ManageAllergen", new { allergenGroupId = allergen.AllergenCategoryId });
                    }
                    else
                    {
                        if (uploadAllergenImage != null)
                        {
                            ImageHelper.UploadImage(allergen.Id, ImagePathFor.AllergenImage, uploadAllergenImage, api_path);
                        }
                        allergen.AllergenImage = allergenImageFilename;
                        _restaurantService.ModifyAllergen(allergen);
                        return RedirectToAction("ManageAllergen", new { allergenGroupId = allergen.AllergenCategoryId });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddEditAllergen", allergen);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditAllergen", allergen);
            }
        }

        /// <summary>
        /// Edits the allergen.
        /// </summary>
        /// <param name="allergen_Id">The allergen identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditAllergen(int allergen_Id)
        {
            var allergen = _restaurantService.GetAllergenById(allergen_Id);
            return View("AddEditAllergen", allergen);
        }

        /// <summary>
        /// Deletes the allergen.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteAllergen(int id)
        {
            try
            {
                var allergen = _restaurantService.GetAllergenById(id);
                string imgPath = "";
                if (!string.IsNullOrWhiteSpace(allergen.AllergenImage))
                {
                    imgPath = @"\Images\Preferences\Allergens\" + allergen.Id.ToString() + @"\" + allergen.AllergenImage;
                }

                _restaurantService.DeleteAllergen(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                if (imgPath != "")
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This allergen is already in use." });
            }
        }
        #endregion Allergen

        #region Ingredient
        /// <summary>
        /// Manages the ingredient.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageIngredient(int ingredientCatId)
        {
            ViewBag.IngredientCategoryId = ingredientCatId;
            return View();
        }

        /// <summary>
        /// Loads the ingredients.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadIngredients(int ingredientCatId)
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var ingredient = _restaurantService.GetAllIngredient(ingredientCatId, skip, pageSize, out Total);

            var data = ingredient.Select(b => new
            {
                Name = b.Name,
                IngredientCategory = b.IngredientCategory.Name,
                Description = b.Description,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.IngredientImage, b.IngredientImage, b.Id, api_path),
                IngredientId = b.Id
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the ingredient.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddIngredient(int ingredientCatId)
        {
            Ingredient ingredient = new Ingredient();
            ingredient.IngredientCategoryId = ingredientCatId;
            var ingrCat = _restaurantService.GetIngredientCategoryById(ingredientCatId);
            if (ingrCat.Type == false)
            {
                ViewBag.ShowFoodProfiles = true;
                var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(null).Select(
                  r => new
                  {
                      foodProfileId = r.Id,
                      Name = r.Name
                  }).ToList();
                ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
            }
            else
            {
                ViewBag.ShowFoodProfiles = false;
            }

            return View("AddEditIngredient", ingredient);
        }

        /// <summary>
        /// Saves the ingredient details.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <param name="uploadIngredientImage">The upload ingredient image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveIngredientDetails(Ingredient ingredient, HttpPostedFileBase uploadIngredientImage)
        {
            int? ingredientId = null;
            if (ingredient.Id > 0)
                ingredientId = ingredient.Id;
            try
            {
                var ingrCat = _restaurantService.GetIngredientCategoryById(ingredient.IngredientCategoryId);
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var ingredientImageFilename = ingredient.IngredientImage;
                    if (uploadIngredientImage != null)
                    {
                        if (uploadIngredientImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadIngredientImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadIngredientImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                ingredientImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                if (ingrCat.Type == false)
                                {
                                    ViewBag.ShowFoodProfiles = true;
                                    var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(ingredientId).Select(
                                      r => new
                                      {
                                          foodProfileId = r.Id,
                                          Name = r.Name
                                      }).ToList();
                                    ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
                                    //ViewBag.allIngredient = _restaurantService.GetAllIngredient();
                                }
                                else
                                {
                                    ViewBag.ShowFoodProfiles = false;
                                }

                                return View("AddEditIngredient", ingredient);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            if (ingrCat.Type == false)
                            {
                                ViewBag.ShowFoodProfiles = true;
                                var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(ingredientId).Select(
                                  r => new
                                  {
                                      foodProfileId = r.Id,
                                      Name = r.Name
                                  }).ToList();
                                ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
                                //ViewBag.allIngredient = _restaurantService.GetAllIngredient();
                            }
                            else
                            {
                                ViewBag.ShowFoodProfiles = false;
                            }
                            return View("AddEditIngredient", ingredient);
                        }
                    }

                    //get all selected food profiles
                    List<IngredientFoodProfile> IngredientFoodProfileList = new List<IngredientFoodProfile>();
                    var selectedFoodProfileIds = Request.Form["foodProfileId"];

                    if (ingredient.Id == 0)
                    {
                        ingredient.IngredientImage = ingredientImageFilename;
                        var addedIngredinet = _restaurantService.CreateIngredient(ingredient);
                        if (uploadIngredientImage != null)
                        {
                            ImageHelper.UploadImage(addedIngredinet.Id, ImagePathFor.IngredientImage, uploadIngredientImage, api_path);
                        }
                        //Adding Ingredient food Profiles
                        if (selectedFoodProfileIds != null)
                        {
                            string[] foodProfileIds = selectedFoodProfileIds.Split(',');
                            foreach (var eachFoodProfileId in foodProfileIds)
                            {
                                IngredientFoodProfile foodProfileToAdd = new IngredientFoodProfile()
                                {
                                    IngredientId = addedIngredinet.Id,
                                    FoodProfileId = Convert.ToInt32(eachFoodProfileId)
                                };
                                IngredientFoodProfileList.Add(foodProfileToAdd);
                            }
                        }
                        if (IngredientFoodProfileList.Count > 0)
                            _restaurantService.CreateIngredientFoodProfiles(IngredientFoodProfileList);

                        return RedirectToAction("ManageIngredient", new { ingredientCatId = ingredient.IngredientCategoryId });
                    }
                    else
                    {
                        if (uploadIngredientImage != null)
                        {
                            ImageHelper.UploadImage(ingredient.Id, ImagePathFor.IngredientImage, uploadIngredientImage, api_path);
                        }
                        ingredient.IngredientImage = ingredientImageFilename;
                        var modifiedIngredient = _restaurantService.ModifyIngredient(ingredient);

                        //Adding Ingredient food Profiles
                        if (selectedFoodProfileIds != null)
                        {
                            string[] foodProfileIds = selectedFoodProfileIds.Split(',');
                            foreach (var eachFoodProfileId in foodProfileIds)
                            {
                                IngredientFoodProfile foodProfileToAdd = new IngredientFoodProfile()
                                {
                                    IngredientId = modifiedIngredient.Id,
                                    FoodProfileId = Convert.ToInt32(eachFoodProfileId)
                                };
                                IngredientFoodProfileList.Add(foodProfileToAdd);
                            }
                        }
                        if (IngredientFoodProfileList.Count > 0)
                            _restaurantService.CreateIngredientFoodProfiles(IngredientFoodProfileList);

                        return RedirectToAction("ManageIngredient", new { ingredientCatId = ingredient.IngredientCategoryId });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    if (ingrCat.Type == false)
                    {
                        ViewBag.ShowFoodProfiles = true;
                        var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(ingredientId).Select(
                          r => new
                          {
                              foodProfileId = r.Id,
                              Name = r.Name
                          }).ToList();
                        ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
                       // ViewBag.allIngredient = _restaurantService.GetAllIngredient();
                    }
                    else
                    {
                        ViewBag.ShowFoodProfiles = false;
                    }
                    return View("AddEditIngredient", ingredient);
                }
            }
            catch (Exception ex)
            {
                var ingrCat = _restaurantService.GetIngredientCategoryById(ingredient.IngredientCategoryId);
                ModelState.AddModelError(string.Empty, ex.Message);
                if (ingrCat.Type == false)
                {
                    ViewBag.ShowFoodProfiles = true;
                    var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(ingredientId).Select(
                      r => new
                      {
                          foodProfileId = r.Id,
                          Name = r.Name
                      }).ToList();
                    ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
                    //ViewBag.allIngredient = _restaurantService.GetAllIngredient();
                }
                else
                {
                    ViewBag.ShowFoodProfiles = false;
                }
                return View("AddEditIngredient", ingredient);
            }
        }

        /// <summary>
        /// Edits the ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditIngredient(int id)
        {
            var ingredient = _restaurantService.GetIngredient(id);
            if(ingredient.IngredientCategory.Type==false)
            {
                ViewBag.ShowFoodProfiles = true;
                var unassignFoodProfiles = _restaurantService.GetAllUnassignedFoodProfiles(id).Select(
                  r => new
                  {
                      foodProfileId = r.Id,
                      Name = r.Name
                  }).ToList();
                ViewBag.UnassignedFoodProfilesOfIngredient = new MultiSelectList(unassignFoodProfiles, "foodProfileId", "Name");
                ViewBag.allIngredient = _restaurantService.GetAllIngredient();
            }
            else
            {
                ViewBag.ShowFoodProfiles = false;
            }

            return View("AddEditIngredient", ingredient);
        }

        /// <summary>
        /// Deletes the ingredient.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteIngredient(int Id)
        {
            try
            {
                _restaurantService.DeleteIngredient(Id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Ingredient is already in use." });
            }
        }

        #endregion Ingredient

        #region Ingredient Nutrition Values

        /// <summary>
        /// Manages the ingredient nutrition values.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageIngredientNutritionValues(int id)
        {
            ViewBag.IngredientId = id;
            return View();
        }

        /// <summary>
        /// Loads the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadIngredientNutritionValues(int ingredientId)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var ingredientNutritionValues = _restaurantService.GetIngredientNutritionValues(ingredientId, skip, pageSize, out Total);

            var data = ingredientNutritionValues.Select(b => new
            {
                IngredientNutritionValueId = b.Id,
                IngredientName = b.Ingredient.Name,
                Weight = b.WeightInGrams,
                NutritionName = b.Nutrition.Name,
                NutritionValue = b.NutritionValue + (b.Nutrition.MeasurementUnit.Symbols != null ? " (" + b.Nutrition.MeasurementUnit.Symbols + ")" : ""),
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddIngredientNutritionValues(int ingredientId)
        {
            var existIngredientNutritionValues = _restaurantService.GetIngredientNutritionValues(ingredientId);
            IngredientNutritionValues ingredientNutritionValues = new IngredientNutritionValues();

            if (existIngredientNutritionValues != null && existIngredientNutritionValues.Count() > 0)
                ingredientNutritionValues.WeightInGrams = existIngredientNutritionValues.Select(n => n.WeightInGrams).FirstOrDefault();

            ingredientNutritionValues.IngredientId = ingredientId;

            ViewBag.Nutrition = _restaurantService.GetUnAssignedNutritions(ingredientId).Select(n => new { Id = n.Id, Name = n.Name }).ToList();

            return View("AddEditIngredientNutritionValues", ingredientNutritionValues);
        }

        /// <summary>
        /// Saves the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientNutritionValues">The ingredient nutrition values.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveIngredientNutritionValues(IngredientNutritionValues ingredientNutritionValues)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (ingredientNutritionValues.Id == 0)
                        _restaurantService.CreateIngredientNutritionValues(ingredientNutritionValues);
                    else
                        _restaurantService.ModifyIngredientNutritionValues(ingredientNutritionValues);
                    return RedirectToAction("ManageIngredientNutritionValues", "Admin", new { id = ingredientNutritionValues.IngredientId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    ViewBag.Nutrition = _restaurantService.GetUnAssignedNutritions(ingredientNutritionValues.IngredientId).Select(n => new { Id = n.Id, Name = n.Name }).ToList();
                    return View("AddEditIngredientNutritionValues", ingredientNutritionValues);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Nutrition = _restaurantService.GetUnAssignedNutritions(ingredientNutritionValues.IngredientId).Select(n => new { Id = n.Id, Name = n.Name }).ToList();
                return View("AddEditIngredientNutritionValues", ingredientNutritionValues);
            }
        }

        /// <summary>
        /// Edits the ingredient nutrition value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditIngredientNutritionValue(int id)
        {
            var ingredientNutritionValues = _restaurantService.GetIngredientNutritionValuesById(id);
            return View("AddEditIngredientNutritionValues", ingredientNutritionValues);
        }

        /// <summary>
        /// Deletes the ingredient nutrition values.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteIngredientNutritionValues(int Id)
        {
            try
            {
                _restaurantService.DeleteIngredientNutritionValues(Id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Ingredient NutritionValues is already in use." });
            }
        }

        #endregion Ingredient Nutrition Values

        /// <summary>
        /// Loads the ingredient food profiles.
        /// </summary>
        /// <param name="IngredientId">The ingredient identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadIngredientFoodProfiles(int IngredientId)
        {
            var ingredientFoodProfiles = _restaurantService.GetAllIngredientFoodProfiles(IngredientId);
            return PartialView("_IngredientFoodProfiles", ingredientFoodProfiles);
        }
        /// <summary>
        /// Deletes the ingredient food profile.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteIngredientFoodProfile(int Id)
        {
            try
            {
                _restaurantService.DeleteIngredientFoodProfile(Id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Ingredient food profile cannot be deleted" });
            }
        }
        /// <summary>
        /// Manages the hotel restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageHotelRestaurants()
        {
            return View();
        }
        /// <summary>
        /// Loads all hotel restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadAllHotelRestaurants()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetAllHotelsRestaurants(skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                HotelRestaurantId = b.Id,
                HotelName = b.Hotel.Name,
                RestaurantName = b.Restaurant.Name

            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the hotel restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddHotelRestaurants()
        {
            HotelRestaurant hotelRest = new HotelRestaurant();
            var restaurants = _hotelService.GetUnassignHotelRestaurants().Select(
                 r => new
                 {
                     RestaurantId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

            var hotels = _hotelService.GetAllHotels().Select(
               h => new
               {
                   HotelId = h.Id,
                   Name = h.Name
               }).ToList();
            ViewBag.hotel = new SelectList(hotels, "HotelId", "Name");

            return View("AddEditHotelRestaurants", hotelRest);
        }


        /// <summary>
        /// Saves the hotel restaurants.
        /// </summary>
        /// <param name="hotelRestaurants">The hotel restaurants.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult SaveHotelRestaurants(HotelRestaurant hotelRestaurants)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<HotelRestaurant> hotelRests = new List<HotelRestaurant>();

                    var RestaurantIds = Request.Form["RestaurantId"];
                    var HotelId = Request.Form["HotelId"];

                    if (RestaurantIds != null)
                    {
                        string[] restIds = RestaurantIds.Split(',');
                        foreach (var rest in restIds)
                        {
                            HotelRestaurant hotelRest = new HotelRestaurant()
                            {
                                HotelId = Convert.ToInt32(HotelId),
                                RestaurantId = Convert.ToInt32(rest)
                            };
                            hotelRests.Add(hotelRest);
                        }
                    }

                    if (hotelRestaurants.Id == 0)
                        _hotelService.CreateHotelRestaurants(hotelRests);
                    //else
                    //_restaurantService.ModifyChain(chainTable);

                    return RedirectToAction("ManageHotelRestaurants", "Admin", new { area = "admin" });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var restaurants = _hotelService.GetUnassignHotelRestaurants().Select(
                r => new
                {
                    RestaurantId = r.Id,
                    Name = r.Name
                }).ToList();
                    ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

                    var hotels = _hotelService.GetAllHotels().Select(
                       h => new
                       {
                           HotelId = h.Id,
                           Name = h.Name
                       }).ToList();
                    ViewBag.hotel = new SelectList(hotels, "HotelId", "Name");
                    return View("AddEditHotelRestaurants", hotelRestaurants);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var restaurants = _hotelService.GetUnassignHotelRestaurants().Select(
              r => new
              {
                  RestaurantId = r.Id,
                  Name = r.Name
              }).ToList();
                ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

                var hotels = _hotelService.GetAllHotels().Select(
                   h => new
                   {
                       HotelId = h.Id,
                       Name = h.Name
                   }).ToList();
                ViewBag.hotel = new SelectList(hotels, "HotelId", "Name");
                return View("AddEditHotelRestaurants", hotelRestaurants);
            }
        }

        /// <summary>
        /// Deletes the hotel restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteHotelRestaurant(int id)
        {
            try
            {
                _hotelService.DeleteHotelRestaurant(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This is already in use." });
            }

        }
        /// <summary>
        /// Gets all recent restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult GetAllRecentRestaurants()
        {
            var recentRestaurants = _restaurantService.GetTodaysOrders().OrderByDescending(c => c.ScheduledDate).Select(o => o.Restaurant).Distinct();
            return View(recentRestaurants);
        }

        /// <summary>
        /// Gets all recent hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult GetAllRecentHotels()
        {
            var recentHotels = _hotelService.GetTodaysBooking().Select(h => h.Room.Hotel).Distinct().OrderByDescending(c => c.CreationDate);
            return View(recentHotels);
        }



        /// <summary>
        /// Chains the setup.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainSetup()
        {
            var customer = GetAuthenticatedCustomer();
            var chains = _restaurantService.GetAllChains(customer.Id);
            return View(chains);
        }

        /// <summary>
        /// Creates the chain.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult CreateChain()
        {
            ChainTable chainTable = new ChainTable();
            var restaurants = _restaurantService.GetUnAssignChainRestaurant().Select(
                 r => new
                 {
                     RestaurantId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

            var hotels = _hotelService.GetUnAssignChainHotel().Select(
               h => new
               {
                   HotelId = h.Id,
                   Name = h.Name
               }).ToList();
            ViewBag.hotel = new MultiSelectList(hotels, "HotelId", "Name");

            return View("AddEditChain", chainTable);
        }

        /// <summary>
        /// Adds the edit chain.
        /// </summary>
        /// <param name="chainTable">The chain table.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult AddEditChain(ChainTable chainTable)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ChainDetailsTable> chainDetailsTables = new List<ChainDetailsTable>();

                    string RestaurantIds = Request.Form["RestaurantId"];
                    var HotelIds = Request.Form["HotelId"];

                    if (RestaurantIds != null)
                    {
                        string[] restIds = RestaurantIds.Split(',');
                        foreach (var rest in restIds)
                        {
                            ChainDetailsTable chainDetailsTable = new ChainDetailsTable()
                            {
                                RestaurantId = Convert.ToInt32(rest)
                            };
                            chainDetailsTables.Add(chainDetailsTable);
                        }
                    }

                    if (HotelIds != null)
                    {
                        string[] hotelIds = HotelIds.Split(',');
                        foreach (var htId in hotelIds)
                        {
                            ChainDetailsTable chainDetailsTable = new ChainDetailsTable()
                            {
                                HotelId = Convert.ToInt32(htId)
                            };
                            chainDetailsTables.Add(chainDetailsTable);
                        }
                    }

                    chainTable.ChainDetailsTables = chainDetailsTables;
                    if (chainTable.Id == 0)
                        _restaurantService.CreateChain(chainTable);
                    else
                        _restaurantService.ModifyChain(chainTable);

                    return RedirectToAction("ChainSetup", "Admin", new { area = "admin" });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var restaurants = _restaurantService.GetUnAssignChainRestaurant().Select(
                        r => new
                        {
                            RestaurantId = r.Id,
                            Name = r.Name
                        }).ToList();
                    ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

                    var hotels = _hotelService.GetUnAssignChainHotel().Select(
                       h => new
                       {
                           HotelId = h.Id,
                           Name = h.Name
                       }).ToList();
                    ViewBag.hotel = new MultiSelectList(hotels, "HotelId", "Name");
                    return View("AddEditChain", chainTable);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var restaurants = _restaurantService.GetUnAssignChainRestaurant().Select(
                    r => new
                    {
                        RestaurantId = r.Id,
                        Name = r.Name
                    }).ToList();
                ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name");

                var hotels = _hotelService.GetUnAssignChainHotel().Select(
                   h => new
                   {
                       HotelId = h.Id,
                       Name = h.Name
                   }).ToList();
                ViewBag.hotel = new MultiSelectList(hotels, "HotelId", "Name");

                return View("AddEditChain", chainTable);
            }
        }

        /// <summary>
        /// Edits the chain.
        /// </summary>
        /// <param name="chainId">The chain identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditChain(int chainId)
        {
            var chain = _restaurantService.GetChainById(chainId);

            var restChain = chain.ChainDetailsTables.Where(c => c.RestaurantId != null).Select(c => c.RestaurantId).ToArray();
            var restaurants = _restaurantService.GetAssignChainRestaurant(chain.Id).Select(
               r => new
               {
                   RestaurantId = r.Id,
                   Name = r.Name
               }).ToList();
            ViewBag.restaurant = new MultiSelectList(restaurants, "RestaurantId", "Name", restChain);

            var hotChain = chain.ChainDetailsTables.Where(c => c.HotelId != null).Select(c => c.HotelId).ToArray();
            var hotels = _hotelService.GetAssignChainHotel(chain.Id).Select(
               h => new
               {
                   HotelId = h.Id,
                   Name = h.Name
               }).ToList();
            ViewBag.hotel = new MultiSelectList(hotels, "HotelId", "Name", hotChain);

            return View("AddEditChain", chain);
        }

        /// <summary>
        /// Deletes the chain.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteChain(int Id)
        {
            try
            {
                _restaurantService.DeleteChain(Id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This chain is already in use." });
            }
        }

        /// <summary>
        /// Gets all chain automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult GetAllChainAutoComplete(string prefix)
        {
            var chains = _restaurantService.GetAllDefaultChainsByName(prefix).Select(c => new { Id = c.Id, Name = c.ChainName });
            return Json(chains, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lists the chains.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ListChains(int id)
        {
            var chains = _restaurantService.GetChainsById(id);
            return PartialView("_ChainsList", chains);
        }

        /// <summary>
        /// Manages the services.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageServices()
        {
            var customer = GetAuthenticatedCustomer();
            var services = _hotelService.GetAllServices();
            return View(services);
        }

        /// <summary>
        /// Adds the service.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddService()
        {
            Services services = new Services();
            return View(services);
        }

        /// <summary>
        /// Adds the edit service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddEditService(Services services)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (services.Id == 0)
                        _hotelService.CreateService(services);
                    else
                        _hotelService.ModifyService(services);

                    return RedirectToAction("ManageServices", "Admin", new { area = "admin" });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddService", services);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddService", services);
            }
        }

        /// <summary>
        /// Edits the service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditService(int Id)
        {
            var service = _hotelService.GetServiceById(Id);
            return View("AddService", service);
        }

        /// <summary>
        /// Deletes the service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteService(int Id)
        {
            try
            {
                _hotelService.DeleteService(Id);
                return RedirectToAction("ManageServices", "Admin", new { area = "admin" });
            }
            catch
            {
                return Json(new { failure = true, error = "This service is already in use." });
            }
        }
        //Chain Admin

        /// <summary>
        /// Chains the admin dashboard.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainAdminDashboard()
        {
            return View();
        }

        /// <summary>
        /// Gets the chain recent restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult GetChainRecentRestaurants()
        {
            var customer = GetAuthenticatedCustomer();
            var recentRestaurants = _restaurantService.GetChainRecentRestaurants(customer.Id).Distinct().ToList();
            return View("GetAllRecentRestaurants", recentRestaurants);
        }
        /// <summary>
        /// Gets the chain recent hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult GetChainRecentHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var recentHotels = _hotelService.GetChainRecentHotels(customer.Id).Distinct().ToList();
            return View("GetAllRecentHotels", recentHotels);
        }

        /// <summary>
        /// Chains the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainReviews()
        {
            return View();
        }

        /// <summary>
        /// Rests the chain reviews list.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult RestChainReviewsList()
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantReviews = _restaurantService.GetAllRestChainReviews(customer.Id);
            return PartialView("_ReviewsList", restaurantReviews);

        }
        /// <summary>
        /// Chains the reviews by restaurants.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainReviewsByRestaurants()
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantReviews = _restaurantService.GetAllRestChainReviews(customer.Id);
            return PartialView("_ReviewsByRestaurants", restaurantReviews);
        }
        /// <summary>
        /// Hotels the chain reviews list.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult HotelChainReviewsList()
        {
            var customer = GetAuthenticatedCustomer();
            var hotelReviews = _restaurantService.GetAllHotelChainReviews(customer.Id);
            return PartialView("_HotelReviewsList", hotelReviews);

        }
        /// <summary>
        /// Chains the reviews by hotels.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainReviewsByHotels()
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantReviews = _restaurantService.GetAllHotelChainReviews(customer.Id);
            return PartialView("_ReviewsByHotels", restaurantReviews);
        }
        /// <summary>
        /// Chains the recent order.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainRecentOrder()
        {
            return View();
        }

        /// <summary>
        /// Loads the chain recent order.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadChainRecentOrder()
        {
            var customer = GetAuthenticatedCustomer();

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //find search columns info
            var restaurantId = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var restId = restaurantId == "" ? 0 : Convert.ToInt32(restaurantId);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var ChainRecentOrder = _restaurantService.GetAllChainRecentOrder(customer.Id, skip, pageSize, out Total);
            var data = ChainRecentOrder.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Chains the hotel order.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainHotelOrder()
        {
            return View();
        }
        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadData()
        {
            var customer = GetAuthenticatedCustomer();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetChainHotelRecentOrders(customer.Id, skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                HotelName = b.Room.Hotel.Name,
                FromDate = b.From.ToString(),
                ToDate = b.To.ToString(),
                RoomNumber = b.Room.Number,
                Price = b.Room.RoomRate.FirstOrDefault().Price,
                Status = b.Status.Name
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Chains the admin rest automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult.</returns>
        [HttpPost]
        public JsonResult ChainAdminRestAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetAllRestOfChainAdminBySearchText(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });
            return Json(restaurants, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Chains the admin order history.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainAdminOrderHistory()
        {
            return View();
        }
        /// <summary>
        /// Loads the chain admin order history.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadChainAdminOrderHistory()
        {
            var customer = GetAuthenticatedCustomer();

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var restaurantId = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);
            var restId = restaurantId == "" ? 0 : Convert.ToInt32(restaurantId);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var chainAdminOrderHistory = _restaurantService.GetAllRecentOrdersOfChainAdmin(customer.Id, fromDate, toDate, restId, minAmt, maxAmt, skip, pageSize, out Total, 2);
            var data = chainAdminOrderHistory.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Chains the admin order queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ChainAdminOrderQueue()
        {
            return View();
        }
        /// <summary>
        /// Loads the chain admin qrder queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadChainAdminQrderQueue()
        {
            var customer = GetAuthenticatedCustomer();

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //find search columns info
            var restaurantId = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var restId = restaurantId == "" ? 0 : Convert.ToInt32(restaurantId);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var chainAdminOrderHistory = _restaurantService.GetAllRecentOrdersOfChainAdmin(customer.Id, "", "", restId, 0, 0, skip, pageSize, out Total, 1);
            var data = chainAdminOrderHistory.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RestaurantName = b.Restaurant.Name,
                BookingDate = (b.ScheduledDate != null) ? b.ScheduledDate.ToString() : b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #region Measurement Unit
        /// <summary>
        /// Manages the measurement unit.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageMeasurementUnit()
        {
            return View();
        }
        /// <summary>
        /// Loads the measurement units.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadMeasurementUnits()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var measurementUnits = _restaurantService.GetMeasurementUnits(skip, pageSize, out Total);
            var data = measurementUnits.Select(b => new
            {
                MeasurementUnitId = b.Id,
                UnitName = b.UnitName,
                Description = b.Description,
                Symbol = b.Symbols,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Adds the measurement unit.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddMeasurementUnit()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            return View(measurementUnit);
        }
        /// <summary>
        /// Saves the measurement unit.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveMeasurementUnit(MeasurementUnit measurementUnit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _restaurantService.CreateMeasurementUnits(measurementUnit);
                    return RedirectToAction("ManageMeasurementUnit", "Admin");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddMeasurementUnit", measurementUnit);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddMeasurementUnit", measurementUnit);
            }
        }

        /// <summary>
        /// Deletes the measurement unit.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteMeasurementUnit(int Id)
        {
            try
            {
                _restaurantService.DeleteMeasurementUnit(Id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This measurement unit is already in use." });
            }
        }

        #endregion Measurement Unit

        #region About You Ordr
        /// <summary>
        /// Manages the about youordr description.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageAboutYouOrdrDescription()
        {
            return View();
        }
        /// <summary>
        /// Loads the about youordr description.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult LoadAboutYouOrdrDescription()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var youOrdrDescInAllLang = _aboutYouOrdrService.GetAllLanguagesYouOrdrDescription(skip, pageSize, out Total);
            var data = youOrdrDescInAllLang.Select(b => new
            {
                AboutYouOrdrId = b.Id,
                LanguageName = b.Language.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Adds the about youordr description.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddAboutYouOrdrDescription()
        {
            AboutYouOrdr aboutYouOrdr = new AboutYouOrdr();
            ViewBag.Languages = _aboutYouOrdrService.GetUnassignedLanguages().ToList();
            return View("AddEditAboutYouOrdrDescription", aboutYouOrdr);
        }
        /// <summary>
        /// Saves the about youordr description.
        /// </summary>
        /// <param name="abtYouOrdr">The abt youordr.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveAboutYouOrdrDescription(AboutYouOrdr abtYouOrdr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (abtYouOrdr.Id == 0)
                    {
                        var result = _aboutYouOrdrService.CreateAboutYouOrdrDescription(abtYouOrdr);
                        return RedirectToAction("ManageAboutYouOrdrDescription", "Admin");
                    }
                    else
                    {
                        var result = _aboutYouOrdrService.ModifyAboutYouOrdrDescriotion(abtYouOrdr);
                        return RedirectToAction("ManageAboutYouOrdrDescription", "Admin");
                    }

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    ViewBag.Languages = _aboutYouOrdrService.GetUnassignedLanguages().ToList();
                    return View("AddEditAboutYouOrdrDescription", abtYouOrdr);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Languages = _aboutYouOrdrService.GetUnassignedLanguages().ToList();
                return View("AddEditAboutYouOrdrDescription", abtYouOrdr);
            }
        }

        /// <summary>
        /// Edits the add about youordr description.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditAddAboutYouOrdrDescription(int id)
        {
            var abtYouOrdr = _aboutYouOrdrService.GetYouOrdrDescriptionById(id);
            ViewBag.Languages = _aboutYouOrdrService.GetUnassignedLanguages().ToList();
            return View("AddEditAboutYouOrdrDescription", abtYouOrdr);
        }

        /// <summary>
        /// Deletes the about youordr description.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteAboutYouOrdrDescription(int Id)
        {
            try
            {
                _aboutYouOrdrService.DeleteAboutYouOrdrDesciption(Id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = true, error = "This about youOrdr description is already in use." });
            }
        }
        #endregion About You Ordr

        #region Nutrition

        /// <summary>
        /// Manages the nutrition.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageNutrition()
        {
            return View();
        }

        /// <summary>
        /// Loads the nutritions.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadNutritions()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var nutritions = _restaurantService.GetNutritions(skip, pageSize, out Total);
            var data = nutritions.Select(b => new
            {
                NutritionId = b.Id,
                Name = b.Name,
                MeasurementUnit = b.MeasurementUnit.UnitName + (b.MeasurementUnit.Symbols != null ? " (" + b.MeasurementUnit.Symbols + ")" : ""),
            });
            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the nutrition.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddNutrition()
        {
            Nutrition nutrition = new Nutrition();
            ViewBag.MesurmentUnits = _restaurantService.GetAllMeasurementUnits();
            return View(nutrition);
        }

        /// <summary>
        /// Saves the nutrition.
        /// </summary>
        /// <param name="nutrition">The nutrition.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveNutrition(Nutrition nutrition)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _restaurantService.CreateNutrition(nutrition);
                    return RedirectToAction("ManageNutrition", "Admin");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    ViewBag.MesurmentUnits = _restaurantService.GetAllMeasurementUnits();
                    return View("AddNutrition", nutrition);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.MesurmentUnits = _restaurantService.GetAllMeasurementUnits();
                return View("AddNutrition", nutrition);
            }
        }

        /// <summary>
        /// Deletes the nutrition.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult DeleteNutrition(int Id)
        {
            try
            {
                _restaurantService.DeleteNutrition(Id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Nutrition is already in use." });
            }
        }

        #endregion Nutrition  

    }
}