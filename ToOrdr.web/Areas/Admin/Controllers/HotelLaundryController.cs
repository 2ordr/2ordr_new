﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-10-2019
// ***********************************************************************
// <copyright file="HotelLaundryController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelLaundryController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelLaundryController : BaseController
    {

        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The payment service
        /// </summary>
        IPaymentService _paymentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HotelLaundryController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="paymentService">The payment service.</param>
        public HotelLaundryController(IHotelService hotelService, IPaymentService paymentService)
        {
            _hotelService = hotelService;
            _paymentService = paymentService;
        }

        /// <summary>
        /// Hotels the laundry admin dashboard.
        /// </summary>
        /// <returns>ActionResult Hotel LaundryAdmin Dashboard.</returns>
        public ActionResult HotelLaundryAdminDashboard()
        {
            return View();
        }

        /// <summary>
        /// Manages the laundry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage Laundry.</returns>
        public ActionResult ManageLaundry(int id)
        {
            ViewBag.HotelServiceId = id;
            var hotelId = GetActiveHotelId();
            var hotelLaundryServices = _hotelService.GetAllHotelLaundryServices(hotelId, id);
            return View("ManageLaundry", hotelLaundryServices);
        }

        /// <summary>
        /// Adds the laundry service.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Add LaundryService.</returns>
        public ActionResult AddLaundryService(int hotelServiceId)
        {
            ToOrdr.Core.Entities.Laundry LaundryService = new Core.Entities.Laundry();
            LaundryService.HotelServiceId = hotelServiceId;
            return View("AddEditLaundryService", LaundryService);
        }

        /// <summary>
        /// Saves the specified laundry.
        /// </summary>
        /// <param name="laundry">The laundry.</param>
        /// <param name="uploadFile">The upload file.</param>
        /// <returns>ActionResult Save laundry service.</returns>
        public ActionResult Save(Laundry laundry, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];

                    var laundryImageFilename = laundry.Image;
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadFile.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                laundryImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditLaundryService", laundry);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditLaundryService", laundry);
                        }
                    }

                    if (laundry.Id == 0)
                    {
                        laundry.Image = laundryImageFilename;
                        var laundryResult = _hotelService.CreateHotelLaundryService(laundry);
                        if (uploadFile != null)
                        {
                            ImageHelper.UploadImage(laundryResult.Id, ImagePathFor.LaundryService, uploadFile, api_path);
                        }

                        return RedirectToAction("ManageLaundry", new { id = laundryResult.HotelServiceId });
                    }
                    else
                    {
                        if (uploadFile != null)
                        {
                            ImageHelper.UploadImage(laundry.Id, ImagePathFor.LaundryService, uploadFile, api_path);
                        }
                        laundry.Image = laundryImageFilename;
                        var res = _hotelService.ModifyLaundryService(laundry);
                        return RedirectToAction("ManageLaundry", new { id = laundry.HotelServiceId });
                    }
                }
                else
                {
                    return View("AddEditLaundryService", laundry);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditLaundryService", laundry);
            }
        }

        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult edit laundry ervice.</returns>
        public ActionResult Edit(int id)
        {
            var laundryService = _hotelService.GetHotelLaundryServiceByID(id);
            return View("AddEditLaundryService", laundryService);
        }

        /// <summary>
        /// Deletes the laundry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete Laundry.</returns>
        [HttpPost]
        public ActionResult DeleteLaundry(int id)
        {
            try
            {
                var laundry = _hotelService.GetHotelLaundryServiceByID(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                string imgPath = @"\Images\Services\LaundryService\" + laundry.Id + @"\" + laundry.Image;

                _hotelService.DeleteLaundryService(id);
                if (!string.IsNullOrWhiteSpace(laundry.Image))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This laundry service cannot be deleted, bacause it is already in use" });
            }
        }

        // GET: Admin/HotelLaundry
        /// <summary>
        /// Laundries the queue.
        /// </summary>
        /// <returns>ActionResult Laundry Queue.</returns>
        public ActionResult LaundryQueue()
        {
            return View();
        }
        /// <summary>
        /// Loads the laundry orders.
        /// </summary>
        /// <returns>ActionResult Load Laundry Orders.</returns>
        [HttpPost]
        public ActionResult LoadLaundryOrders()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var statusesToShow = from LaundryOrderStatus e in Enum.GetValues(typeof(LaundryOrderStatus)) select new { Id = (int)e, Name = e.ToString() };
            statusesToShow = statusesToShow.Where(s => s.Id != 3);
            var laundryOrders = _hotelService.GetLaundryOrdersQueue(hotelId, skip, pageSize, out Total);
            var data = laundryOrders.Select(b => new
            {
                LaundryOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RoomNumber = b.Room.Number,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((LaundryOrderStatus)b.OrderStatus).ToString(),
                LaundryOrderStatuses = (b.PaymentStatus == true) ? b.OrderStatus == 1 ? statusesToShow.Where(s => s.Id >= b.OrderStatus && s.Id <= 2) : statusesToShow.Where(s => s.Id >= b.OrderStatus) : statusesToShow.Where(s => s.Id == 0),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Loads the laundry order item details.
        /// </summary>
        /// <param name="laundryOrderId">The laundry order identifier.</param>
        /// <param name="orderHistory">The order history.</param>
        /// <returns>ActionResult Load LaundryOrderItem Details.</returns>
        public ActionResult LoadLaundryOrderItemDetails(int laundryOrderId, int? orderHistory = 0)
        {
            var orderItemDetails = _hotelService.GetLaundryOrderItemDetails(laundryOrderId);
            ViewBag.OrdrHistory = orderHistory;
            return PartialView("_LaundryOrderItemDetails", orderItemDetails);
        }

        /// <summary>
        /// Updates the laundry order status.
        /// </summary>
        /// <param name="LaundryOrderId">The laundry order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Update LaundryOrder Status.</returns>
        [HttpPost]
        public ActionResult UpdateLaundryOrderStatus(int LaundryOrderId, int orderStatus)
        {
            try
            {
                var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var laundryOrder = _hotelService.GetLaundryOrderById(LaundryOrderId);
                if (laundryOrder.OrderStatus == 0 && orderStatus == 1)
                {
                    //capture payment from Quickpay
                    var captureStatus = _paymentService.CaptureCardPayment(quickpayToken, quickpayUrl, laundryOrder.QuickPayPaymentId, laundryOrder.OrderTotal);
                    if (captureStatus)
                    {
                        laundryOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyLaundryOrder(laundryOrder);
                        return Json(new { status = true, rowRemoveStatus = false, message = "Order payment captured successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order payment capture Failed!" });
                    }

                }
                if (laundryOrder.OrderStatus == 0 && orderStatus == 4)
                {
                    //Reject the order and release amount reserved for the order
                    var rejectedStatus = _paymentService.RejectHotelServiceOrderAndReleaseAmount(quickpayToken, quickpayUrl, laundryOrder.QuickPayPaymentId);
                    if (rejectedStatus)
                    {
                        laundryOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyLaundryOrder(laundryOrder);
                        return Json(new { status = true, rowRemoveStatus = true, message = "Order rejected and amount released successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order rejection Failed!" });
                    }
                }
                if (laundryOrder.OrderStatus == 0 && orderStatus == 2)
                {
                    return Json(new { status = false, rowRemoveStatus = false, message = "This order cannot be delivered since it is not confirmed yet" });
                }
                if (laundryOrder.OrderStatus != orderStatus)
                {
                    laundryOrder.OrderStatus = orderStatus;
                    _hotelService.ModifyLaundryOrder(laundryOrder);
                }

                if (orderStatus == 2 || orderStatus == 4)
                    return Json(new { status = true, rowRemoveStatus = true });
                else
                    return Json(new { status = true, rowRemoveStatus = false });
            }
            catch
            {
                return Json(new { status = false, rowRemoveStatus = false, message = "This order status cannot be Updated" });
            }
        }
        /// <summary>
        /// Gets the state of the errors from model.
        /// </summary>
        /// <returns>Dictionary&lt;System.String, ModelErrorCollection&gt;.</returns>
        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState() //IEnumerable<string>
        {
            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }

        /// <summary>
        /// Updates the laundry order item deliver status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update LaundryOrderItem Deliver Status.</returns>
        [HttpPost]
        public ActionResult UpdateLaundryOrderItemDeliverStatus(int id)
        {
            try
            {
                _hotelService.UpdateLaundryOrderItemDeliverStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        //public ActionResult LaundryBookingDetails(int id)
        //{
        //    var laundryBookingDetails = _hotelService.GetHotelLaundryBookingDetails(id);
        //    return PartialView("_LaundryBookingDetails", laundryBookingDetails);
        //}
        //GET: Admin/LaundryOrdersHistory
        //public ActionResult LaundryOrdersHistory()
        //{
        //    var hotelId = GetActiveHotelId();
        //    var LaundryBookings = _hotelService.GetAllLaundryBookings(hotelId);
        //    return View(LaundryBookings);
        //}

        /// <summary>
        /// Manages the laundry details.
        /// </summary>
        /// <param name="Laundry_ID">The laundry identifier.</param>
        /// <returns>ActionResult Manage LaundryDetails.</returns>
        public ActionResult ManageLaundryDetails(int Laundry_ID)
        {
            ViewBag.Laundry_ID = Laundry_ID;
            ViewBag.HotelServiceId = _hotelService.GetHotelLaundryServiceByID(Laundry_ID).HotelServiceId;
            var hotelLaundryDetails = _hotelService.GetHotelLaundryDetails(Laundry_ID);
            return View("ManageLaundryDetails", hotelLaundryDetails);
        }


        /// <summary>
        /// Creates the modify laundry details.
        /// </summary>
        /// <param name="Laundry_ID">The laundry identifier.</param>
        /// <returns>ActionResult Create Modify LaundryDetails.</returns>
        public ActionResult CreateModifyLaundryDetails(int Laundry_ID)
        {
            LaundryDetail LaundryItems = new LaundryDetail();
            LaundryItems.LaundryId = Laundry_ID;
            ViewBag.Garments = _hotelService.GetAllGarments(Laundry_ID).Select(
                 r => new
                 {
                     Id = r.Id,
                     Name = r.Name
                 });
            return View(LaundryItems);
        }

        /// <summary>
        /// Saves the laundry details.
        /// </summary>
        /// <param name="LaundryDetails">The laundry details.</param>
        /// <param name="uploadFile">The upload file.</param>
        /// <returns>ActionResult Save LaundryDetails.</returns>
        public ActionResult SaveLaundryDetails(LaundryDetail LaundryDetails, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];

                    var laundryDetailsImgFilename = LaundryDetails.Image;
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadFile.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                laundryDetailsImgFilename = filename;
                            }
                            else
                            {
                                ViewBag.Garments = _hotelService.GetAllGarments(LaundryDetails.GarmentId).Select(r => new { Id = r.Id, Name = r.Name });
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("CreateModifyLaundryDetails", LaundryDetails);
                            }
                        }
                        else
                        {
                            ViewBag.Garments = _hotelService.GetAllGarments(LaundryDetails.GarmentId).Select(r => new { Id = r.Id, Name = r.Name });
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("CreateModifyLaundryDetails", LaundryDetails);
                        }
                    }

                    if (LaundryDetails.Id == 0)
                    {
                        LaundryDetails.Image = laundryDetailsImgFilename;
                        var laundryDetailsResult = _hotelService.CreateHotelLaundryDetails(LaundryDetails);
                        if (uploadFile != null)
                        {
                            ImageHelper.UploadImage(laundryDetailsResult.LaundryId, ImagePathFor.LaundryDetails, uploadFile, api_path, laundryDetailsResult.Id);
                        }
                        return RedirectToAction("ManageLaundryDetails", new { Laundry_ID = laundryDetailsResult.LaundryId });
                    }
                    else
                    {
                        if (uploadFile != null)
                        {
                            ImageHelper.UploadImage(LaundryDetails.LaundryId, ImagePathFor.LaundryDetails, uploadFile, api_path, LaundryDetails.Id);
                        }
                        LaundryDetails.Image = laundryDetailsImgFilename;
                        _hotelService.ModifyHotelLaundryDetails(LaundryDetails);
                        return RedirectToAction("ManageLaundryDetails", new { Laundry_ID = LaundryDetails.LaundryId });
                    }
                }
                else
                {
                    ViewBag.Garments = _hotelService.GetAllGarments(LaundryDetails.GarmentId).Select(r => new { Id = r.Id, Name = r.Name });
                    return View("CreateModifyLaundryDetails", LaundryDetails);
                }

            }
            catch (Exception ex)
            {
                ViewBag.Garments = _hotelService.GetAllGarments(LaundryDetails.GarmentId).Select(r => new { Id = r.Id, Name = r.Name });
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("CreateModifyLaundryDetails", LaundryDetails);
            }
        }


        /// <summary>
        /// Edits the laundry details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit LaundryDetails.</returns>
        public ActionResult EditLaundryDetails(int id)
        {
            var laundry_details = _hotelService.GetHotelLaundryDetailsByID(id);
            return View("CreateModifyLaundryDetails", laundry_details);
        }
        /// <summary>
        /// Deletes the laundry details.
        /// </summary>
        /// <param name="LaundryDetailsID">The laundry details identifier.</param>
        /// <returns>ActionResult Delete LaundryDetails.</returns>
        [HttpPost]
        public ActionResult DeleteLaundryDetails(int LaundryDetailsID)
        {
            try
            {
                _hotelService.DeleteLaundryDetails(LaundryDetailsID);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This laundry details cannot be deleted, bacause it is already in use" });
            }
        }
        /// <summary>
        /// Laundries the reviews.
        /// </summary>
        /// <returns>ActionResult Laundry Reviews.</returns>
        public ActionResult LaundryReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the laundry reviews.
        /// </summary>
        /// <returns>ActionResult Load LaundryReviews.</returns>
        [HttpPost]
        public ActionResult LoadLaundryReviews()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var reviews = _hotelService.GetLaundryReviewsByHotel(hotelId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                LaundryReviewId = b.Id,
                LaundryName = b.Laundry.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        //Laundry Additional Groups
        /// <summary>
        /// Manages the laundry additional group.
        /// </summary>
        /// <returns>ActionResult Manage LaundryAdditionalGroup.</returns>
        public ActionResult ManageLaundryAdditionalGroup()
        {
            return View();
        }
        /// <summary>
        /// Loads the laundry additional groups.
        /// </summary>
        /// <returns>ActionResult Load LaundryAdditionalGroups.</returns>
        [HttpPost]
        public ActionResult LoadLaundryAdditionalGroups()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var laundryAddGrps = _hotelService.GetLaundryAdditionalGroups(hotelId, skip, pageSize, out Total);
            var data = laundryAddGrps.Select(b => new
            {
                LaundryAddGroupId = b.Id,
                GroupName = b.Name,
                GroupDescription = b.Description,
                MinSelected = b.MinSelected,
                MaxSelected = b.MaxSelected,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the laundry additional group.
        /// </summary>
        /// <returns>ActionResult Add LaundryAdditionalGroup.</returns>
        public ActionResult AddLaundryAdditionalGroup()
        {
            LaundryAdditionalGroup laundryAddGroup = new LaundryAdditionalGroup();
            laundryAddGroup.HotelId = GetActiveHotelId();
            return View("AddEditLaundryAdditionalGroup", laundryAddGroup);
        }

        /// <summary>
        /// Saves the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGroup">The laundry add group.</param>
        /// <returns>ActionResult Save LaundryAdditionalGroup.</returns>
        public ActionResult SaveLaundryAdditionalGroup(LaundryAdditionalGroup laundryAddGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (laundryAddGroup.MaxSelected < laundryAddGroup.MinSelected)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Invalid_MaximumSelected);
                        return View("AddEditLaundryAdditionalGroup", laundryAddGroup);
                    }
                    if (laundryAddGroup.Id == 0)
                    {
                        _hotelService.CreateLaundryAdditionalGroup(laundryAddGroup);
                        return RedirectToAction("ManageLaundryAdditionalGroup");
                    }
                    else
                    {
                        _hotelService.ModifyLaundryAdditionalGroup(laundryAddGroup);
                        return RedirectToAction("ManageLaundryAdditionalGroup");
                    }
                }
                else
                {
                    return View("AddEditLaundryAdditionalGroup", laundryAddGroup);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditLaundryAdditionalGroup", laundryAddGroup);
            }
        }

        /// <summary>
        /// Edits the laundry additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit LaundryAdditionalGroup.</returns>
        public ActionResult EditLaundryAdditionalGroup(int id)
        {
            var laundryAddGrp = _hotelService.GetLaundryAdditionalGroupById(id);
            return View("AddEditLaundryAdditionalGroup", laundryAddGrp);
        }
        /// <summary>
        /// Deletes the laundry additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete LaundryAdditionalGroup.</returns>
        [HttpPost]
        public ActionResult DeleteLaundryAdditionalGroup(int id)
        {
            try
            {
                _hotelService.DeleteLaundryAdditionalGroup(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This laundry additional group already in use." });
            }
        }

        //Spa Additional Element
        /// <summary>
        /// Manages the laundry additional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage LaundryAdditionalElements.</returns>
        public ActionResult ManageLaundryAdditionalElements(int id)
        {
            ViewBag.LaundryAdditionalGroupId = id;
            return View();
        }
        /// <summary>
        /// Loads the laundry additional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load LaundryAdditionalElements.</returns>
        [HttpPost]
        public ActionResult LoadLaundryAdditionalElements(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var laundryAddElem = _hotelService.GetLaundryAdditionalElements(id, skip, pageSize, out Total);
            var data = laundryAddElem.Select(b => new
            {
                LaundryAddElementId = b.Id,
                ElementName = b.Name,
                ElementDescription = b.Description,
                GroupName = b.LaundryAdditionalGroup.Name,
                Price = b.Price,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the laundry additional element.
        /// </summary>
        /// <param name="laundryAdditionalGroupId">The laundry additional group identifier.</param>
        /// <returns>ActionResult Add LaundryAdditionalElement.</returns>
        public ActionResult AddLaundryAdditionalElement(int laundryAdditionalGroupId)
        {
            LaundryAdditionalElement laundryAddElement = new LaundryAdditionalElement();
            laundryAddElement.LaundryAdditionalGroupId = laundryAdditionalGroupId;
            return View("AddEditLaundryAdditionalElement", laundryAddElement);
        }

        /// <summary>
        /// Saves the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElement">The laundry add element.</param>
        /// <returns>ActionResult Save LaundryAdditionalElement.</returns>
        public ActionResult SaveLaundryAdditionalElement(LaundryAdditionalElement laundryAddElement)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (laundryAddElement.Id == 0)
                    {
                        _hotelService.CreateLaundryAdditionalElement(laundryAddElement);
                        return RedirectToAction("ManageLaundryAdditionalElements", new { id = laundryAddElement.LaundryAdditionalGroupId });
                    }
                    else
                    {
                        _hotelService.ModifyLaundryAdditionalElement(laundryAddElement);
                        return RedirectToAction("ManageLaundryAdditionalElements", new { id = laundryAddElement.LaundryAdditionalGroupId });
                    }
                }
                else
                {
                    return View("AddEditLaundryAdditionalElement", laundryAddElement);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditLaundryAdditionalElement", laundryAddElement);
            }
        }

        /// <summary>
        /// Edits the laundry additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit LaundryAdditionalElement.</returns>
        public ActionResult EditLaundryAdditionalElement(int id)
        {
            var laundryAddElement = _hotelService.GetLaundryAdditionalElementById(id);
            return View("AddEditLaundryAdditionalElement", laundryAddElement);
        }
        /// <summary>
        /// Deletes the laundry additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete LaundryAdditionalElement.</returns>
        [HttpPost]
        public ActionResult DeleteLaundryAdditionalElement(int id)
        {
            try
            {
                _hotelService.DeleteLaundryAdditionalElement(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This laundry additional element already in use." });
            }
        }

        #region Orders History
        /// <summary>
        /// Laundries the order history.
        /// </summary>
        /// <returns>ActionResult LaundryOrder History.</returns>
        public ActionResult LaundryOrderHistory()
        {
            return View();
        }
        /// <summary>
        /// Loads the laundry order history.
        /// </summary>
        /// <returns>ActionResult Load LaundryOrder History.</returns>
        [HttpPost]
        public ActionResult LoadLaundryOrderHistory()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restOrderHistory = _hotelService.GetLaundryOrderHistory(fromDate, toDate, hotelId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = restOrderHistory.Select(b => new
            {
                LaundryOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Amount = b.OrderTotal,
                RoomNumber = b.Room.Number,
                OrderStatus = ((LaundryOrderStatus)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString(),
                TipsAmount = b.LaundryTips.Select(i => i.TipAmount).FirstOrDefault(),
                TipsPaid = b.LaundryTips.Select(i => i.PaymentStatus).FirstOrDefault(),
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #endregion Orders History

        #region Manage Room Housekeeping
        /// <summary>
        /// Manages the laundry detail additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage LaundryDetailAdditional.</returns>
        public ActionResult ManageLaundryDetailAdditional(int id)
        {
            ViewBag.LaundryId = _hotelService.GetHotelLaundryDetailsByID(id).LaundryId;
            ViewBag.LaundryDetailsId = id;
            return View();
        }

        /// <summary>
        /// Loads the laundry detail additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load LaundryDetailAdditional.</returns>
        [HttpPost]
        public ActionResult LoadLaundryDetailAdditional(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var laundryDetAdditionals = _hotelService.GetAllLaundryDetAdditionals(id, skip, pageSize, out Total);
            var data = laundryDetAdditionals.Select(b => new
            {
                LaundryDetAddId = b.Id,
                LaundryDetailsName = b.LaundryDetail.Garments.Name,
                LaundryAddGrpName = b.LaundryAdditionalGroup.Name,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the laundry detail additional.
        /// </summary>
        /// <param name="laundryDetailsId">The laundry details identifier.</param>
        /// <returns>ActionResult Add LaundryDetailAdditional.</returns>
        public ActionResult AddLaundryDetailAdditional(int laundryDetailsId)
        {
            LaundryDetailAdditional laundryDetAdd = new LaundryDetailAdditional();
            laundryDetAdd.LaundryDetailId = laundryDetailsId;
            var unassignLAG = _hotelService.GetUnassignLaundryAddGroups(laundryDetailsId, GetActiveHotelId()).Select(
                 r => new
                 {
                     LaundryAdditionalGroupId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.LaundryAdditionalGroups = new MultiSelectList(unassignLAG, "LaundryAdditionalGroupId", "Name");
            return View(laundryDetAdd);
        }


        /// <summary>
        /// Saves the laundry det additional.
        /// </summary>
        /// <param name="laundryDetAdditional">The laundry det additional.</param>
        /// <returns>ActionResult Save LaundryDetAdditional.</returns>
        [HttpPost]
        public ActionResult SaveLaundryDetAdditional(LaundryDetailAdditional laundryDetAdditional)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<LaundryDetailAdditional> laundryDetAddi = new List<LaundryDetailAdditional>();

                    var LaundryAddGrpIds = Request.Form["LaundryAdditionalGroupId"];

                    if (LaundryAddGrpIds != null)
                    {
                        string[] LAGIds = LaundryAddGrpIds.Split(',');
                        foreach (var eachLAGId in LAGIds)
                        {
                            LaundryDetailAdditional eachLDA = new LaundryDetailAdditional()
                            {
                                LaundryDetailId = laundryDetAdditional.LaundryDetailId,
                                LaundryAdditionalGroupId = Convert.ToInt32(eachLAGId),
                                IsActive = true
                            };
                            laundryDetAddi.Add(eachLDA);
                        }
                    }

                    if (laundryDetAdditional.Id == 0)
                        _hotelService.CreateLaundryDetAdditional(laundryDetAddi);

                    return RedirectToAction("ManageLaundryDetailAdditional", "HotelLaundry", new { area = "admin", id = laundryDetAdditional.LaundryDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignLAG = _hotelService.GetUnassignLaundryAddGroups(laundryDetAdditional.LaundryDetailId, GetActiveHotelId()).Select(
                                        r => new
                                        {
                                            LaundryAdditionalGroupId = r.Id,
                                            Name = r.Name
                                        }).ToList();
                    ViewBag.LaundryAdditionalGroups = new MultiSelectList(unassignLAG, "LaundryAdditionalGroupId", "Name");
                    return View(laundryDetAdditional);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignLAG = _hotelService.GetUnassignLaundryAddGroups(laundryDetAdditional.LaundryDetailId, GetActiveHotelId()).Select(
                                        r => new
                                        {
                                            LaundryAdditionalGroupId = r.Id,
                                            Name = r.Name
                                        }).ToList();
                ViewBag.LaundryAdditionalGroups = new MultiSelectList(unassignLAG, "LaundryAdditionalGroupId", "Name");
                return View(laundryDetAdditional);
            }
        }

        /// <summary>
        /// Updates the laundry det additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update LaundryDetAdditional.</returns>
        [HttpPost]
        public ActionResult UpdateLaundryDetAdditional(int id)
        {
            try
            {
                var laundrydetadd = _hotelService.GetLaundrydetAdditionalById(id);
                if (laundrydetadd.IsActive)
                    laundrydetadd.IsActive = false;
                else
                    laundrydetadd.IsActive = true;

                var updatedLDA = _hotelService.ModifyLaundryDetAdditional(laundrydetadd);
                return RedirectToAction("ManageLaundryDetailAdditional", "HotelLaundry", new { id = laundrydetadd.LaundryDetailId });
            }
            catch
            {
                return Json(new { failure = true, error = "Laundy detail additional group status cannot be updated." });
            }
        }
        /// <summary>
        /// Deletes the laundry det add.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete LaundryDetAdd.</returns>
        [HttpPost]
        public ActionResult DeleteLaundryDetAdd(int id)
        {
            try
            {
                _hotelService.DeleteLaundrydetAdditional(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This laundry detail additional already in use." });
            }
        }

        #endregion Manage Room Housekeeping

        //public ActionResult BookingStatus(bool status, int booking_id)
        //{
        //    var LaundryBooking = _hotelService.GetHotelLaundryBookingByID(booking_id);
        //    LaundryBooking.Laundry_Status = status;
        //    var BookingModified = _hotelService.ModifyHotelLaundryBooking(LaundryBooking);
        //    return RedirectToAction("LaundryQueue", "HotelLaundry", new { Area = "Admin" });
        //}

        //[HttpPost]
        //public ActionResult DeleteLaundryBooking(int Hotel_Laundry_Booking_ID)
        //{
        //    try
        //    {
        //        // _hotelService.DeleteTripBookings(id);
        //        var LaundryBooking = _hotelService.GetHotelLaundryBookingByID(Hotel_Laundry_Booking_ID);
        //        LaundryBooking.IsActive = false;
        //        _hotelService.ModifyHotelLaundryBooking(LaundryBooking);
        //        return RedirectToAction("LaundryQueue", "HotelLaundry", new { Area = "Admin" });
        //    }
        //    catch
        //    {
        //        return View("TripsQueue");
        //    }
        //}
    }
}