﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-11-2019
// ***********************************************************************
// <copyright file="HotelTaxiServiceController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelTaxiServiceController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelTaxiServiceController : BaseController
    {
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// Initializes a new instance of the <see cref="HotelTaxiServiceController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        public HotelTaxiServiceController(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }
        /// <summary>
        /// Manages the taxi.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageTaxi(int id)
        {
            ViewBag.HotelServiceId = id;
            var hotelId = GetActiveHotelId();
            var hotelTaxi = _hotelService.GetHotelTaxiDetails(hotelId, id);
            return View("ManageTaxi", hotelTaxi);
        }

        /// <summary>
        /// Adds the taxi.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult AddTaxi(int hotelServiceId)
        {
            TaxiDetail Taxi = new TaxiDetail();
            Taxi.HotelServiceId = hotelServiceId;
            return View("AddEditTaxi", Taxi);

        }
        /// <summary>
        /// Saves the specified taxi.
        /// </summary>
        /// <param name="Taxi">The taxi.</param>
        /// <param name="uploadTaxiImage">The upload taxi image.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Save(TaxiDetail Taxi, HttpPostedFileBase uploadTaxiImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];

                    var taxiImageFilename = Taxi.Image;
                    if (uploadTaxiImage != null)
                    {
                        if (uploadTaxiImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadTaxiImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadTaxiImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                taxiImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditTaxi", Taxi);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditTaxi", Taxi);
                        }
                    }

                    if (Taxi.Id == 0)
                    {
                        Taxi.Image = taxiImageFilename;
                        var taxi = _hotelService.CreateHotelTaxi(Taxi);
                        if (uploadTaxiImage != null)
                        {
                            ImageHelper.UploadImage(taxi.Id, ImagePathFor.TaxiService, uploadTaxiImage, api_path);
                        }

                        return RedirectToAction("ManageTaxi", new { id = taxi.HotelServiceId });
                    }
                    else
                    {
                        if (uploadTaxiImage != null)
                        {
                            ImageHelper.UploadImage(Taxi.Id, ImagePathFor.TaxiService, uploadTaxiImage, api_path);
                        }
                        Taxi.Image = taxiImageFilename;
                        var res = _hotelService.ModifyHotelTaxiDetails(Taxi);
                        return RedirectToAction("ManageTaxi", new { id = Taxi.HotelServiceId });
                    }
                }
                else
                {

                    return View("AddEditTaxi", Taxi);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditTaxi", Taxi);
            }
        }
        /// <summary>
        /// Edits the taxi.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditTaxi(int id)
        {
            var taxi = _hotelService.GetHotelTaxiDetailById(id);
            return View("AddEditTaxi", taxi);
        }
        /// <summary>
        /// Deletes the taxi.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteTaxi(int id)
        {
            try
            {
                _hotelService.DeleteTaxiDetails(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This taxi details cannot be deleted, bacause it is already in use" });
            }
        }
        /// <summary>
        /// Taxis the queue.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TaxiQueue()
        {
            return View();
        }

        /// <summary>
        /// Loads the taxi orders.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadTaxiOrders()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetTaxiBookings(hotelId, skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                TaxiBookingId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                TaxiName = b.TaxiDetails.Name,
                From = b.From.ToString(),
                To = b.To.ToString(),
                OrderDate = (b.ScheduleDate != null) ? b.ScheduleDate.ToString() : b.CreationDate.ToString(),
                Comment = b.Comment,
                Status =  ((TaxiBookingStatus)Convert.ToInt32(b.OrderStatus)).ToString(),
                TaxiBookingStatuses = from TaxiBookingStatus e in Enum.GetValues(typeof(TaxiBookingStatus)) select new { Id = (int)e, Name = e.ToString() }
            });
            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Updates the taxi booking status.
        /// </summary>
        /// <param name="taxiBookingId">The taxi booking identifier.</param>
        /// <param name="bookingStatus">The booking status.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult UpdateTaxiBookingStatus(int taxiBookingId, int bookingStatus)
        {
            try
            {
                var taxiBookingDet = _hotelService.GetHotelTaxiBooking(taxiBookingId);
                if (taxiBookingDet.OrderStatus != bookingStatus)
                    taxiBookingDet.OrderStatus = bookingStatus;
                _hotelService.UpdateTaxiBookingStatus(taxiBookingDet);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This taxi booking status cannot be Updated" });
            }
        }

        /// <summary>
        /// Taxis the reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult TaxiReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the taxi reviews.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadTaxiReviews()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var reviews = _hotelService.GetTaxiReviewsByHotel(hotelId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                TaxiReviewId = b.Id,
                TaxiName = b.TaxiDetail.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        // GET: Admin/HotelTaxiService
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Manages the taxi destinations.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult ManageTaxiDestinations()
        {
            return View();
        }

        /// <summary>
        /// Loads the taxi destinations.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult LoadTaxiDestinations()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var hotelId = GetActiveHotelId();
            var taxiDest = _hotelService.GetAllHotelTaxiDestination(hotelId, skip, pageSize, out Total);
            var data = taxiDest.Select(b => new
            {
                TaxiDestinationId = b.Id,
                HotelName = b.Hotel.Name,
                FromPlace = b.FromPlace,
                ToPlace = b.ToPlace,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the taxi destination.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult AddTaxiDestination()
        {
            var hotelId = GetActiveHotelId();
            TaxiDestination taxiDest = new TaxiDestination();
            taxiDest.HotelId = hotelId;
            return View("AddEditTaxiDestination", taxiDest);
        }


        /// <summary>
        /// Saves the taxi destination.
        /// </summary>
        /// <param name="taxiDestination">The taxi destination.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveTaxiDestination(TaxiDestination taxiDestination)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (taxiDestination.Id == 0)
                    {
                        _hotelService.CreateTaxiDestination(taxiDestination);
                        return RedirectToAction("ManageTaxiDestinations");
                    }
                    else
                    {
                        _hotelService.ModifyTaxiDestination(taxiDestination);
                        return RedirectToAction("ManageTaxiDestinations");
                    }
                }
                else
                {
                    return View("AddEditTaxiDestination", taxiDestination);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditTaxiDestination", taxiDestination);
            }
        }

        /// <summary>
        /// Edits the taxi destination.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult EditTaxiDestination(int id)
        {
            var taxiDest = _hotelService.GetTaxiDestinationById(id);
            return View("AddEditTaxiDestination", taxiDest);
        }
        /// <summary>
        /// Deletes the taxi destination.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult DeleteTaxiDestination(int id)
        {
            try
            {
                _hotelService.DeleteTaxiDestination(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This taxi destination already in use." });
            }
        }
    }
}