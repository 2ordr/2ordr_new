﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="CustomersController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    ///// Class CustomersController.
    /// Implements the <see cref="System.Web.Mvc.Controller" />
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    [Authorize]
    public class CustomersController : Controller
    {
        // GET: Admin/Customers
        /// <summary>
        /// Dashboards this instance.
        /// </summary>
        /// <returns>Customer dashboard.</returns>
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}