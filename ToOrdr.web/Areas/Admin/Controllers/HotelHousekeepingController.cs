﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-07-2019
// ***********************************************************************
// <copyright file="HotelHousekeepingController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelHousekeepingController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelHousekeepingController : BaseController
    {
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The payment service
        /// </summary>
        IPaymentService _paymentService;
        /// <summary>
        /// Initializes a new instance of the <see cref="HotelHousekeepingController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="paymentService">The payment service.</param>
        public HotelHousekeepingController(IHotelService hotelService, IPaymentService paymentService)
        {
            _hotelService = hotelService;
            _paymentService = paymentService;
        }

        #region Manage Room Housekeeping

        /// <summary>
        /// Manages the room housekeeping.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage RoomHousekeeping.</returns>
        public ActionResult ManageRoomHousekeeping(int id)
        {
            var roomDetails = _hotelService.GetRoomDetailById(id);
            ViewBag.RoomId = roomDetails.RoomId;
            ViewBag.RoomDetailsId = id;

            ViewBag.IsHousekeeping = roomDetails.HouseKeeping.Count();
            return View();
        }

        /// <summary>
        /// Loads the room housekeeping.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load RoomHousekeeping.</returns>
        [HttpPost]
        public ActionResult LoadRoomHousekeeping(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var spaRoomDetailHouseKeeping = _hotelService.GetAllRoomHouseKeeping(id, skip, pageSize, out Total);
            var data = spaRoomDetailHouseKeeping.Select(b => new
            {
                HouseKeepingId = b.Id,
                RoomDetailsName = b.RoomDetails.Name,
                HKFacilityName = b.HouseKeepingFacility.Name,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the room housekeeping.
        /// </summary>
        /// <param name="roomDetailsId">The room details identifier.</param>
        /// <returns>ActionResult Add RoomHousekeeping.</returns>
        public ActionResult AddRoomHousekeeping(int roomDetailsId)
        {
            HouseKeeping houseKeeping = new HouseKeeping();
            houseKeeping.RoomDetailsId = roomDetailsId;
            var unassignHKF = _hotelService.GetUnassignHKFacilities(roomDetailsId, GetActiveHotelId()).Select(
                 r => new
                 {
                     HouseKeepingFacilityId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.HouseKeepingFacilities = new MultiSelectList(unassignHKF, "HouseKeepingFacilityId", "Name");
            return View(houseKeeping);
        }

        /// <summary>
        /// Saves the room house keeping.
        /// </summary>
        /// <param name="houseKeeping">The house keeping.</param>
        /// <returns>ActionResult Save RoomHouseKeeping.</returns>
        [HttpPost]
        public ActionResult SaveRoomHouseKeeping(HouseKeeping houseKeeping)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<HouseKeeping> roomHouseKeeping = new List<HouseKeeping>();

                    var HKFacilitiesIds = Request.Form["HouseKeepingFacilityId"];

                    if (HKFacilitiesIds != null)
                    {
                        string[] HKFIds = HKFacilitiesIds.Split(',');
                        foreach (var eachHKFId in HKFIds)
                        {
                            HouseKeeping eachHKF = new HouseKeeping()
                            {
                                RoomDetailsId = houseKeeping.RoomDetailsId,
                                HouseKeepingFacilityId = Convert.ToInt32(eachHKFId),
                                IsActive = true
                            };
                            roomHouseKeeping.Add(eachHKF);
                        }
                    }

                    if (houseKeeping.Id == 0)
                        _hotelService.CreateRoomHouseKeeping(roomHouseKeeping);

                    return RedirectToAction("ManageRoomHousekeeping", "HotelHousekeeping", new { area = "admin", id = houseKeeping.RoomDetailsId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignHKF = _hotelService.GetUnassignHKFacilities(houseKeeping.RoomDetailsId, GetActiveHotelId()).Select(
                                        r => new
                                        {
                                            HouseKeepingFacilityId = r.Id,
                                            Name = r.Name
                                        }).ToList();
                    ViewBag.HouseKeepingFacilities = new MultiSelectList(unassignHKF, "HouseKeepingFacilityId", "Name");
                    return View(houseKeeping);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignHKF = _hotelService.GetUnassignHKFacilities(houseKeeping.RoomDetailsId, GetActiveHotelId()).Select(
                                        r => new
                                        {
                                            HouseKeepingFacilityId = r.Id,
                                            Name = r.Name
                                        }).ToList();
                ViewBag.HouseKeepingFacilities = new MultiSelectList(unassignHKF, "HouseKeepingFacilityId", "Name");
                return View(houseKeeping);
            }
        }

        /// <summary>
        /// Updates the room house keeping.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update RoomHouseKeeping.</returns>
        [HttpPost]
        public ActionResult UpdateRoomHouseKeeping(int id)
        {
            try
            {
                var roomHouseKeeping = _hotelService.GetHousekeepingById(id);
                if (roomHouseKeeping.IsActive)
                    roomHouseKeeping.IsActive = false;
                else
                    roomHouseKeeping.IsActive = true;

                var updatedroomHK = _hotelService.ModifyRoomHouseKeepingFacility(roomHouseKeeping);
                return RedirectToAction("ManageRoomHousekeeping", "HotelHousekeeping", new { id = roomHouseKeeping.RoomDetailsId });
            }
            catch
            {
                return View("ManageRoomHousekeeping");
            }
        }
        /// <summary>
        /// Deletes the room house keeping.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RoomHouseKeeping.</returns>
        [HttpPost]
        public ActionResult DeleteRoomHouseKeeping(int id)
        {
            try
            {
                _hotelService.DeleteRoomHouseKeeping(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This room house keeping facility already in use." });
            }
        }

        #endregion Manage Room Housekeeping

        #region  Housekeeping facilities

        /// <summary>
        /// Manages the housekeeping facility.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Manage HousekeepingFacility.</returns>
        public ActionResult ManageHousekeepingFacility(int hotelServiceId)// int roomId
        {
            ViewBag.HotelServiceId = hotelServiceId;
            return View();
        }

        /// <summary>
        /// Loads the housekeeping facility.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HousekeepingFacility.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingFacility(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var houseKeeping = _hotelService.GetHousekeepingByHotelServiceId(id, skip, pageSize, out Total);
            var data = houseKeeping.Select(b => new
            {
                HousekeepingFacilityId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the housekeeping facility.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Add HousekeepingFacility.</returns>
        public ActionResult AddHousekeepingFacility(int hotelServiceId)
        {
            HouseKeepingFacility houseKeepingFacilities = new HouseKeepingFacility();
            houseKeepingFacilities.HotelServiceId = hotelServiceId;
            return View("AddEditHousekeepingFacility", houseKeepingFacilities);
        }

        /// <summary>
        /// Saves the housekeeping facility.
        /// </summary>
        /// <param name="houseKeepingFacilities">The house keeping facilities.</param>
        /// <returns>ActionResult Save HousekeepingFacility.</returns>
        public ActionResult SaveHousekeepingFacility(HouseKeepingFacility houseKeepingFacilities)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (houseKeepingFacilities.Id == 0)
                    {
                        _hotelService.CreateHousekeepingFacilities(houseKeepingFacilities);
                        return RedirectToAction("ManageHousekeepingFacility", new { hotelServiceId = houseKeepingFacilities.HotelServiceId });
                    }
                    else
                    {
                        _hotelService.ModifyHousekeepingFacilities(houseKeepingFacilities);
                        return RedirectToAction("ManageHousekeepingFacility", new { hotelServiceId = houseKeepingFacilities.HotelServiceId });
                    }
                }
                else
                {
                    return View("AddEditHousekeepingFacility", houseKeepingFacilities);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHousekeepingFacility", houseKeepingFacilities);
            }
        }

        /// <summary>
        /// Edits the housekeeping facility.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HousekeepingFacility.</returns>
        public ActionResult EditHousekeepingFacility(int id)
        {
            var houseKeepingFacilities = _hotelService.GetHousekeepingFacilitiesById(id);
            return View("AddEditHousekeepingFacility", houseKeepingFacilities);
        }

        /// <summary>
        /// Deletes the housekeeping facility.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HousekeepingFacility.</returns>
        [HttpPost]
        public ActionResult DeleteHousekeepingFacility(int id)
        {
            try
            {
                _hotelService.DeleteHousekeepingFacilitie(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This House keeping facility already in use." });
            }
        }

        #endregion Housekeeping facilities

        #region Housekeeping facility Details

        /// <summary>
        /// Manages the housekeeping facility details.
        /// </summary>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>ActionResult Manage HousekeepingFacilityDetails.</returns>
        public ActionResult ManageHousekeepingFacilityDetails(int houseKeepingFacilityId)
        {
            ViewBag.HotelServiceId = _hotelService.GetHousekeepingFacilitiesById(houseKeepingFacilityId).HotelServiceId;
            ViewBag.HouseKeepingFacilityId = houseKeepingFacilityId;
            return View();
        }

        /// <summary>
        /// Loads the housekeeping fac details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HousekeepingFacDetails.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingFacDetails(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var houseKeepingFacDet = _hotelService.GetHousekeepingFacDetailsByHFId(id, skip, pageSize, out Total);
            var data = houseKeepingFacDet.Select(b => new
            {
                HouseKeepingFacDetailId = b.Id,
                FacilityName = b.HouseKeepingFacility.Name,
                Name = b.Name,
                Description = b.Description,
                Price = b.Price,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the housekeeping fac detail.
        /// </summary>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>ActionResult Add HousekeepingFacDetail.</returns>
        public ActionResult AddHousekeepingFacDetail(int houseKeepingFacilityId)
        {
            HouseKeepingFacilityDetail hKeepingFacilityDetail = new HouseKeepingFacilityDetail();
            hKeepingFacilityDetail.HouseKeepingFacilityId = houseKeepingFacilityId;
            return View("AddEditHouseKeepingFacilityDetail", hKeepingFacilityDetail);
        }

        /// <summary>
        /// Saves the housekeeping facility detail.
        /// </summary>
        /// <param name="hKeepingFacilityDetail">The h keeping facility detail.</param>
        /// <returns>ActionResult Save HousekeepingFacilityDetail.</returns>
        public ActionResult SaveHousekeepingFacilityDetail(HouseKeepingFacilityDetail hKeepingFacilityDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (hKeepingFacilityDetail.Id == 0)
                    {
                        _hotelService.CreateHousekeepingFacDetail(hKeepingFacilityDetail);
                        return RedirectToAction("ManageHousekeepingFacilityDetails", new { houseKeepingFacilityId = hKeepingFacilityDetail.HouseKeepingFacilityId });
                    }
                    else
                    {
                        _hotelService.ModifyHousekeepingFacDetail(hKeepingFacilityDetail);
                        return RedirectToAction("ManageHousekeepingFacilityDetails", new { houseKeepingFacilityId = hKeepingFacilityDetail.HouseKeepingFacilityId });
                    }
                }
                else
                {
                    return View("AddEditHouseKeepingFacilityDetail", hKeepingFacilityDetail);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHouseKeepingFacilityDetail", hKeepingFacilityDetail);
            }
        }

        /// <summary>
        /// Edits the housekeeping fac detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HousekeepingFacDetail.</returns>
        public ActionResult EditHousekeepingFacDetail(int id)
        {
            var hKeepingFacilityDetail = _hotelService.GetHousekeepingFacDetailById(id);
            return View("AddEditHouseKeepingFacilityDetail", hKeepingFacilityDetail);
        }

        /// <summary>
        /// Deletes the housekeeping fac detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HousekeepingFacDetail.</returns>
        [HttpPost]
        public ActionResult DeleteHousekeepingFacDetail(int id)
        {
            try
            {
                _hotelService.DeleteHousekeepingFacDetail(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Housekeeping facility detail already in use." });
            }
        }


        #endregion Housekeeping facility Details

        #region Manage housekeeping additionals
        /// <summary>
        /// Manages the housekeepingfacilities additional.
        /// </summary>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>ActionResult Manage HousekeepingfacilitiesAdditional.</returns>
        public ActionResult ManageHousekeepingfacilitiesAdditional(int houseKeepingFacilityId)
        {
            ViewBag.HotelServiceId = _hotelService.GetHousekeepingFacilitiesById(houseKeepingFacilityId).HotelServiceId;
            ViewBag.HousekeepingFacilityId = houseKeepingFacilityId;
            return View();
        }

        /// <summary>
        /// Loads the housekeeping fac additionals.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HousekeepingFacAdditionals.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingFacAdditionals(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var houseKeepingFacAdd = _hotelService.GetHousekeepingFacAdditionalByHKFId(id, skip, pageSize, out Total);
            var data = houseKeepingFacAdd.Select(b => new
            {
                HousekeepingFacAdditionalId = b.Id,
                Name = b.HousekeepingAdditionalGroup.Name,
                Description = b.HousekeepingAdditionalGroup.Description,
                MinSelected = b.HousekeepingAdditionalGroup.MinSelected,
                MaxSelected = b.HousekeepingAdditionalGroup.MaxSelected,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the housekeepingfacilities additional.
        /// </summary>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>ActionResult Add HousekeepingfacilitiesAdditional.</returns>
        public ActionResult AddHousekeepingfacilitiesAdditional(int houseKeepingFacilityId)
        {
            HouseKeepingfacilitiesAdditional houseKeepingFacAdditional = new HouseKeepingfacilitiesAdditional();
            houseKeepingFacAdditional.HouseKeepingFacilityId = houseKeepingFacilityId;

            ViewBag.HousekeepingAdditionalGroups = _hotelService.GetAllUnAssignHousekeepingAdditionalGroups(GetActiveHotelId(), houseKeepingFacilityId).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                }).ToList();

            return View("AddEditHousekeepingfacilitiesAdditional", houseKeepingFacAdditional);
        }

        /// <summary>
        /// Saves the housekeeping fac additional.
        /// </summary>
        /// <param name="houseKeepingFacAdditional">The house keeping fac additional.</param>
        /// <returns>ActionResult Save HousekeepingFacAdditional.</returns>
        public ActionResult SaveHousekeepingFacAdditional(HouseKeepingfacilitiesAdditional houseKeepingFacAdditional)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (houseKeepingFacAdditional.Id == 0)
                    {
                        _hotelService.CreateHousekeepingFacAdditionals(houseKeepingFacAdditional);
                        return RedirectToAction("ManageHousekeepingfacilitiesAdditional", new { houseKeepingFacilityId = houseKeepingFacAdditional.HouseKeepingFacilityId });
                    }
                    else
                    {
                        _hotelService.ModifyHousekeepingFacAdditional(houseKeepingFacAdditional);
                        return RedirectToAction("ManageHousekeepingfacilitiesAdditional", new { houseKeepingFacilityId = houseKeepingFacAdditional.HouseKeepingFacilityId });
                    }
                }
                else
                {
                    ViewBag.HousekeepingAdditionalGroups = _hotelService.GetAllUnAssignHousekeepingAdditionalGroups(GetActiveHotelId(), houseKeepingFacAdditional.HouseKeepingFacilityId).Select(
                        r => new
                        {
                            Id = r.Id,
                            Name = r.Name
                        }).ToList();
                    return View("AddEditHousekeepingfacilitiesAdditional", houseKeepingFacAdditional);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.HousekeepingAdditionalGroups = _hotelService.GetAllUnAssignHousekeepingAdditionalGroups(GetActiveHotelId(), houseKeepingFacAdditional.HouseKeepingFacilityId).Select(
                    r => new
                    {
                        Id = r.Id,
                        Name = r.Name
                    }).ToList();
                return View("AddEditHousekeepingfacilitiesAdditional", houseKeepingFacAdditional);
            }
        }

        /// <summary>
        /// Updates the housekeeping fac additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update HousekeepingFacAdditional.</returns>
        [HttpPost]
        public ActionResult UpdateHousekeepingFacAdditional(int id)
        {
            try
            {
                var housekeepingFacAdditional = _hotelService.GetHousekeepingFacAdditionalById(id);
                if (housekeepingFacAdditional.IsActive)
                    housekeepingFacAdditional.IsActive = false;
                else
                    housekeepingFacAdditional.IsActive = true;

                _hotelService.ModifyHousekeepingFacAdditional(housekeepingFacAdditional);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Housekeeping facilities additional already in use." });
            }
        }

        /// <summary>
        /// Deletes the housekeeping fac additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HousekeepingFacAdditional.</returns>
        [HttpPost]
        public ActionResult DeleteHousekeepingFacAdditional(int id)
        {
            try
            {
                _hotelService.DeleteHouseKeepingFacAdditional(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Housekeeping facilities additional already in use." });
            }
        }

        #endregion Manage housekeeping additionals

        #region Housekeeping additionals groups
        /// <summary>
        /// Manages the housekeeping additional group.
        /// </summary>
        /// <returns>ActionResult Manage HousekeepingAdditionalGroup.</returns>
        public ActionResult ManageHousekeepingAdditionalGroup()
        {
            return View();
        }

        /// <summary>
        /// Loads the housekeeping additional groups.
        /// </summary>
        /// <returns>ActionResult Load HousekeepingAdditionalGroups.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingAdditionalGroups()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var housekeepingAddGrps = _hotelService.GetHousekeepingAdditionalGroups(GetActiveHotelId(), skip, pageSize, out Total);
            var data = housekeepingAddGrps.Select(b => new
            {
                HousekeepingAddGroupId = b.Id,
                GroupName = b.Name,
                GroupDescription = b.Description,
                MinSelected = b.MinSelected,
                MaxSelected = b.MaxSelected,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the housekeeping additional group.
        /// </summary>
        /// <returns>ActionResult Add HousekeepingAdditionalGroup.</returns>
        public ActionResult AddHousekeepingAdditionalGroup()
        {
            HousekeepingAdditionalGroup housekeepingAddGroup = new HousekeepingAdditionalGroup();
            housekeepingAddGroup.HotelId = GetActiveHotelId();
            return View("AddEditHousekeepingAdditionalGroup", housekeepingAddGroup);
        }

        /// <summary>
        /// Saves the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGroup">The housekeeping add group.</param>
        /// <returns>ActionResult Save HousekeepingAdditionalGroup.</returns>
        public ActionResult SaveHousekeepingAdditionalGroup(HousekeepingAdditionalGroup housekeepingAddGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (housekeepingAddGroup.MaxSelected < housekeepingAddGroup.MinSelected)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Invalid_MaximumSelected);
                        return View("AddEditHousekeepingAdditionalGroup", housekeepingAddGroup);
                    }
                    if (housekeepingAddGroup.Id == 0)
                    {
                        _hotelService.CreateHousekeepingAdditionalGroup(housekeepingAddGroup);
                        return RedirectToAction("ManageHousekeepingAdditionalGroup");
                    }
                    else
                    {
                        _hotelService.ModifyHousekeepingAdditionalGroup(housekeepingAddGroup);
                        return RedirectToAction("ManageHousekeepingAdditionalGroup");
                    }
                }
                else
                {
                    return View("AddEditHousekeepingAdditionalGroup", housekeepingAddGroup);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHousekeepingAdditionalGroup", housekeepingAddGroup);
            }
        }

        /// <summary>
        /// Edits the housekeeping additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HousekeepingAdditionalGroup.</returns>
        public ActionResult EditHousekeepingAdditionalGroup(int id)
        {
            var housekeepingAddGroup = _hotelService.GetHousekeepingAdditionalGroupById(id);
            return View("AddEditHousekeepingAdditionalGroup", housekeepingAddGroup);
        }

        /// <summary>
        /// Deletes the housekeeping additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HousekeepingAdditionalGroup.</returns>
        [HttpPost]
        public ActionResult DeleteHousekeepingAdditionalGroup(int id)
        {
            try
            {
                _hotelService.DeleteHousekeepingAdditionalGroup(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Housekeeping additional group already in use." });
            }
        }

        #endregion Housekeeping additionals groups

        #region Housekeeping Additional Elements

        /// <summary>
        /// Manages the housekeeping additional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage HousekeepingAdditionalElements.</returns>
        public ActionResult ManageHousekeepingAdditionalElements(int id)
        {
            ViewBag.HousekeepingAdditionalGroupId = id;
            return View();
        }

        /// <summary>
        /// Loads the housekeeping additional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HousekeepingAdditionalElements.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingAdditionalElements(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var housekeepingAddElem = _hotelService.GetHousekeepingAdditionalElements(id, skip, pageSize, out Total);
            var data = housekeepingAddElem.Select(b => new
            {
                HousekeepingAddElementId = b.Id,
                ElementName = b.Name,
                ElementDescription = b.Description,
                GroupName = b.HousekeepingAdditionalGroup.Name,
                Price = b.Price,
                IsActive = b.IsActive
            });
            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the housekeeping additional element.
        /// </summary>
        /// <param name="HousekeepingAdditionalGroupId">The housekeeping additional group identifier.</param>
        /// <returns>ActionResult Add HousekeepingAdditionalElement.</returns>
        public ActionResult AddHousekeepingAdditionalElement(int HousekeepingAdditionalGroupId)
        {
            HousekeepingAdditionalElement housekeepingAddElement = new HousekeepingAdditionalElement();
            housekeepingAddElement.HousekeepingAdditionalGroupId = HousekeepingAdditionalGroupId;
            return View("AddEditHousekeepingAdditionalElement", housekeepingAddElement);
        }

        /// <summary>
        /// Saves the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElement">The housekeeping add element.</param>
        /// <returns>ActionResult Save HousekeepingAdditionalElement.</returns>
        public ActionResult SaveHousekeepingAdditionalElement(HousekeepingAdditionalElement housekeepingAddElement)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (housekeepingAddElement.Id == 0)
                    {
                        _hotelService.CreateHousekeepingAdditionalElement(housekeepingAddElement);
                        return RedirectToAction("ManageHousekeepingAdditionalElements", new { id = housekeepingAddElement.HousekeepingAdditionalGroupId });
                    }
                    else
                    {
                        _hotelService.ModifyHousekeepingAdditionalElement(housekeepingAddElement);
                        return RedirectToAction("ManageHousekeepingAdditionalElements", new { id = housekeepingAddElement.HousekeepingAdditionalGroupId });
                    }
                }
                else
                {
                    return View("AddEditHousekeepingAdditionalElement", housekeepingAddElement);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHousekeepingAdditionalElement", housekeepingAddElement);
            }
        }

        /// <summary>
        /// Edits the housekeeping additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HousekeepingAdditionalElement.</returns>
        public ActionResult EditHousekeepingAdditionalElement(int id)
        {
            var housekeepingAddElement = _hotelService.GetHousekeepingAdditionalElementById(id);
            return View("AddEditHousekeepingAdditionalElement", housekeepingAddElement);
        }

        /// <summary>
        /// Deletes the housekeeping additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HousekeepingAdditionalElement.</returns>
        [HttpPost]
        public ActionResult DeleteHousekeepingAdditionalElement(int id)
        {
            try
            {
                _hotelService.DeleteHousekeepingAdditionalElement(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Housekeeping additional element already in use." });
            }
        }

        #endregion Housekeeping additional elements

        #region HousekeepingQueue

        /// <summary>
        /// Housekeepings the queue.
        /// </summary>
        /// <returns>ActionResult HousekeepingQueue.</returns>
        public ActionResult HousekeepingQueue()
        {
            return View();
        }

        /// <summary>
        /// Loads the housekeeping orders.
        /// </summary>
        /// <returns>ActionResult LoadHousekeeping Orders.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingOrders()
        {
            var hotelId = GetActiveHotelId();

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var statusesToShow = from HousekeepingOrderStatus e in Enum.GetValues(typeof(HousekeepingOrderStatus)) select new { Id = (int)e, Name = e.ToString() };
            statusesToShow = statusesToShow.Where(s => s.Id != 3);

            var housekeepingOrders = _hotelService.GetHousekeepingOrdersQueue(hotelId, skip, pageSize, out Total);
            var data = housekeepingOrders.Select(b => new
            {
                HousekeepingOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                RoomNumber = b.Room.Number,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((HousekeepingOrderStatus)b.OrderStatus).ToString(),
                HousekeepingOrderStatuses = (b.PaymentStatus == true) ? b.OrderStatus == 1 ? statusesToShow.Where(s => s.Id >= b.OrderStatus && s.Id <= 2) : statusesToShow.Where(s => s.Id >= b.OrderStatus) : statusesToShow.Where(s => s.Id == 0),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the housekeeping order item details.
        /// </summary>
        /// <param name="housekeepingOrderId">The housekeeping order identifier.</param>
        /// <param name="orderHistory">The order history.</param>
        /// <returns>ActionResult Load HousekeepingOrder ItemDetails.</returns>
        public ActionResult LoadHousekeepingOrderItemDetails(int housekeepingOrderId, int? orderHistory = 0)
        {
            var orderItemDetails = _hotelService.GetHousekeepingOrderItemDetails(housekeepingOrderId);
            ViewBag.OrdrHistory = orderHistory;
            return PartialView("_HousekeepingOrderItemDetails", orderItemDetails);
        }

        /// <summary>
        /// Updates the housekeeping order status.
        /// </summary>
        /// <param name="HousekeepingOrderId">The housekeeping order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Update HousekeepingOrder Status.</returns>
        [HttpPost]
        public ActionResult UpdateHousekeepingOrderStatus(int HousekeepingOrderId, int orderStatus)
        {
            try
            {
                var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var housekeepingOrder = _hotelService.GetHousekeepingOrderById(HousekeepingOrderId);
                if (housekeepingOrder.OrderStatus == 0 && orderStatus == 1)
                {
                    //capture payment from Quickpay
                    var captureStatus = _paymentService.CaptureCardPayment(quickpayToken, quickpayUrl, housekeepingOrder.PaymentId.ToString(), housekeepingOrder.OrderTotal);
                    if (captureStatus)
                    {
                        housekeepingOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyHousekeepingOrder(housekeepingOrder);
                        return Json(new { status = true, rowRemoveStatus = false, message = "Order payment captured successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order payment capture Failed!" });
                    }

                }
                if (housekeepingOrder.OrderStatus == 0 && orderStatus == 4)
                {
                    //Reject the order and release amount reserved for the order
                    var rejectedStatus = _paymentService.RejectHotelServiceOrderAndReleaseAmount(quickpayToken, quickpayUrl, housekeepingOrder.PaymentId.ToString());
                    if (rejectedStatus)
                    {
                        housekeepingOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyHousekeepingOrder(housekeepingOrder);
                        return Json(new { status = true, rowRemoveStatus = true, message = "Order rejected and amount released successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order rejection Failed!" });
                    }
                }

                if (housekeepingOrder.OrderStatus == 0 && orderStatus == 2)
                {
                    return Json(new { status = false, rowRemoveStatus = false, message = "This order cannot be delivered since it has not confirmed yet" });
                }

                if (housekeepingOrder.OrderStatus != orderStatus)
                {
                    housekeepingOrder.OrderStatus = orderStatus;
                    _hotelService.ModifyHousekeepingOrder(housekeepingOrder);
                }

                if (orderStatus == 2 || orderStatus == 4)
                    return Json(new { status = true, rowRemoveStatus = true });
                else
                    return Json(new { status = true, rowRemoveStatus = false });
            }
            catch
            {
                return Json(new { status = false, rowRemoveStatus = false, message = "This order status cannot be Updated" });
            }
        }

        /// <summary>
        /// Updates the housekeeping order item deliver status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update HousekeepingOrderItem DeliverStatus.</returns>
        [HttpPost]
        public ActionResult UpdateHousekeepingOrderItemDeliverStatus(int id)
        {
            try
            {
                _hotelService.UpdateHousekeepingOrderItemDeliverStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Gets the state of the errors from model.
        /// </summary>
        /// <returns>Dictionary&lt;System.String, ModelErrorCollection&gt;.</returns>
        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState()
        {
            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }

        #endregion HousekeepingQueue

        #region Housekeeping Orders History
        /// <summary>
        /// Housekeepings the order history.
        /// </summary>
        /// <returns>ActionResult HousekeepingOrder History.</returns>
        public ActionResult HousekeepingOrderHistory()
        {
            return View();
        }
        /// <summary>
        /// Loads the housekeeping order history.
        /// </summary>
        /// <returns>ActionResult Load HousekeepingOrder History.</returns>
        [HttpPost]
        public ActionResult LoadHousekeepingOrderHistory()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var housekeepingOrderHistory = _hotelService.GetHousekeepingOrderHistory(fromDate, toDate, hotelId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = housekeepingOrderHistory.Select(b => new
            {
                HousekeepingOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Amount = b.OrderTotal,
                RoomNumber = b.Room.Number,
                OrderStatus = ((HousekeepingOrderStatus)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString(),
                TipsAmount = b.HousekeepingTips.Select(i => i.TipAmount).FirstOrDefault(),
                TipsPaid = b.HousekeepingTips.Select(i => i.PaymentStatus).FirstOrDefault()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #endregion Housekeeping Orders History
    }
}