﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-10-2019
// ***********************************************************************
// <copyright file="HotelSpaServiceController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelSpaServiceController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelSpaServiceController : BaseController
    {
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The payment service
        /// </summary>
        IPaymentService _paymentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HotelSpaServiceController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="paymentService">The payment service.</param>
        public HotelSpaServiceController(IHotelService hotelService, IPaymentService paymentService)
        {
            _hotelService = hotelService;
            _paymentService = paymentService;
        }

        // GET: Admin/HotelSpaService
        /// <summary>
        /// Manages the spa.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Manage Spa.</returns>
        public ActionResult ManageSpa(int hotelServiceId)
        {
            var hotelId = GetActiveHotelId();
            var hotelSpaServices = _hotelService.GetHotelSPAServiceByServiceId(hotelServiceId);
            ViewBag.HotelServiceId = hotelServiceId;
            return View(hotelSpaServices);
        }

        /// <summary>
        /// Adds the spa services.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Add SpaServices.</returns>
        public ActionResult AddSpaServices(int hotelServiceId)
        {
            SpaService newSpaService = new SpaService();
            newSpaService.HotelServiceId = hotelServiceId;
            return View("AddSpaServices", newSpaService);
        }
        /// <summary>
        /// Adds the spa services.
        /// </summary>
        /// <param name="SpaService">The spa service.</param>
        /// <param name="uploadSpaImage">The upload spa image.</param>
        /// <returns>ActionResult Add SpaServices.</returns>
        [HttpPost]
        public ActionResult AddSpaServices(SpaService SpaService, HttpPostedFileBase uploadSpaImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];

                    if (uploadSpaImage != null)
                    {
                        if (uploadSpaImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadSpaImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadSpaImage.FileName);

                            if (allowedExtensions.Contains(ext))
                            {
                                SpaService.Image = filename;
                                if (SpaService.Id == 0)
                                {
                                    var spa = _hotelService.CreateSpaService(SpaService);
                                    ImageHelper.UploadImage(spa.Id, ImagePathFor.SpaService, uploadSpaImage, api_path);
                                    return RedirectToAction("ManageSpa", "HotelSpaService", new { area = "Admin", hotelServiceId = spa.HotelServiceId });
                                }
                                else
                                {
                                    _hotelService.ModifySpaService(SpaService);
                                    ImageHelper.UploadImage(SpaService.Id, ImagePathFor.SpaService, uploadSpaImage, api_path);
                                    return RedirectToAction("ManageSpa", "HotelSpaService", new { area = "Admin", hotelServiceId = SpaService.HotelServiceId });
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image Extension is not Valid");
                                return View("AddSpaServices", SpaService);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Please Select Proper Image ");
                            return View("AddSpaServices", SpaService);
                        }
                    }
                    else
                    {
                        if (SpaService.Id == 0)
                        {
                            _hotelService.CreateSpaService(SpaService);
                            return RedirectToAction("ManageSpa", "HotelSpaService", new { area = "Admin", hotelServiceId = SpaService.HotelServiceId });
                        }
                        else
                        {
                            _hotelService.ModifySpaService(SpaService);
                            return RedirectToAction("ManageSpa", "HotelSpaService", new { area = "Admin", hotelServiceId = SpaService.HotelServiceId });
                        }

                    }
                }
                else
                {
                    return View("AddSpaServices", SpaService);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddSpaServices", SpaService);
            }
        }
        /// <summary>
        /// Edits the spa services.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaServices.</returns>
        public ActionResult EditSpaServices(int id)
        {
            var spaService = _hotelService.GetHotelSpaServiceById(id);
            if (spaService == null)
            {
                return HttpNotFound();
            }
            return View("AddSpaServices", spaService);
        }

        /// <summary>
        /// Deletes the spa service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaService.</returns>
        [HttpPost]
        public ActionResult DeleteSpaService(int id)
        {
            try
            {
                var spaService = _hotelService.GetHotelSpaServiceById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];

                string imgPath = @"\Images\Hotel\" + spaService.HotelServices.HotelId + @"\SpaService\" + spaService.Id + @"\" + spaService.Image;

                _hotelService.DeleteSpaService(id);
                if (!string.IsNullOrWhiteSpace(spaService.Image))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa Service already in use." });
            }
        }

        /// <summary>
        /// Gets the is late satus change spa orders.
        /// </summary>
        /// <returns>ActionResult Get IsLate Satus Change SpaOrders.</returns>
        public ActionResult GetIsLateSatusChangeSpaOrders()
        {
            var hotelId = GetActiveHotelId();
            ViewBag.SpaOrderIsLateStatusChangedCount = _hotelService.GetSpaOrderIsLateStatusChangedCount(hotelId);
            return PartialView("_GetIsLateStatusChangeSpaOrders");
        }

        /// <summary>
        /// Spas the queue.
        /// </summary>
        /// <returns>ActionResult SPAQueue.</returns>
        public ActionResult SPAQueue()
        {
            return View();
        }
        /// <summary>
        /// Loads the spa orders.
        /// </summary>
        /// <returns>ActionResult Load SpaOrders.</returns>
        [HttpPost]
        public ActionResult LoadSpaOrders()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var statusesToShow = from SpaOrderStatus e in Enum.GetValues(typeof(SpaOrderStatus)) select new { Id = (int)e, Name = e.ToString() };
            statusesToShow = statusesToShow.Where(s => s.Id != 3);
            var spaOrders = _hotelService.GetSpaOrder(hotelId, skip, pageSize, out Total);
            var data = spaOrders.Select(b => new
            {
                SpaOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((SpaOrderStatus)b.OrderStatus).ToString(),
                SpaOrderStatuses = (b.PaymentStatus == true) ? b.OrderStatus == 1 ? statusesToShow.Where(s => s.Id >= b.OrderStatus && s.Id <= 2) : statusesToShow.Where(s => s.Id >= b.OrderStatus) : statusesToShow.Where(s => s.Id == 0),
                PaymentStatus = b.PaymentStatus.ToString(),
                IsLateStatus = b.IsLate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Spas the order details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult SpaOrder Details.</returns>
        public ActionResult SpaOrderDetails(int id)
        {
            var spaOrderDetails = _hotelService.GetSpaOrderDetails(id);
            return View(spaOrderDetails);
        }

        /// <summary>
        /// Modifies the spa orders.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Modify SpaOrders.</returns>
        public ActionResult ModifySpaOrders(int orderId, int orderStatus)
        {
            try
            {
                var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];
                var spaOrder = _hotelService.GetSpaOrdersDetailsById(orderId);
                if (spaOrder.OrderStatus == 0 && orderStatus == 1)
                {
                    //capture payment from Quickpay
                    var captureStatus = _paymentService.CaptureCardPayment(quickpayToken, quickpayUrl, spaOrder.QuickPayPaymentId, spaOrder.OrderTotal);
                    if (captureStatus)
                    {
                        spaOrder.OrderStatus = orderStatus;
                        _hotelService.UpdateSpaOrderStatus(spaOrder);
                        return Json(new { status = true, rowRemoveStatus = false, message = "Order payment captured successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order payment capture Failed!" });
                    }

                }
                if (spaOrder.OrderStatus == 0 && orderStatus == 4)
                {
                    //Reject the order and release amount reserved for the order
                    var rejectedStatus = _paymentService.RejectHotelServiceOrderAndReleaseAmount(quickpayToken, quickpayUrl, spaOrder.QuickPayPaymentId);
                    if (rejectedStatus)
                    {
                        spaOrder.OrderStatus = orderStatus;
                        _hotelService.UpdateSpaOrderStatus(spaOrder);
                        return Json(new { status = true, rowRemoveStatus = true, message = "Order rejected and amount released successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order rejection Failed!" });
                    }
                }
                if (spaOrder.OrderStatus == 0 && orderStatus == 2)
                {
                    return Json(new { status = false, rowRemoveStatus = false, message = "This order cannot be delivered since it is not confirmed yet" });
                }

                if (spaOrder.OrderStatus != orderStatus)
                {
                    spaOrder.OrderStatus = orderStatus;
                    _hotelService.UpdateSpaOrderStatus(spaOrder);
                }

                if (orderStatus == 2 || orderStatus == 3 || orderStatus == 4)
                    return Json(new { status = true, rowRemoveStatus = true });
                else
                    return Json(new { status = true, rowRemoveStatus = false });
            }
            catch
            {
                return Json(new { status = false, rowRemoveStatus = false, message = "This order status cannot be Updated" });
            }
        }

        /// <summary>
        /// Manages the spa order time slot calendar.
        /// </summary>
        /// <returns>ActionResult Manage SpaOrderTimeSlot Calendar.</returns>
        public ActionResult ManageSpaOrderTimeSlotCalendar()
        {
            return View();
        }

        /// <summary>
        /// Updates the spa order item deliver status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update SpaOrderItem Deliver Status.</returns>
        [HttpPost]
        public ActionResult UpdateSpaOrderItemDeliverStatus(int id)
        {
            try
            {
                _hotelService.UpdateSpaOrderItemDeliverStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Gets the state of the errors from model.
        /// </summary>
        /// <returns>Dictionary&lt;System.String, ModelErrorCollection&gt;.</returns>
        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState() //IEnumerable<string>
        {
            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }

        /// <summary>
        /// Loads the spa order time slot calendar.
        /// </summary>
        /// <returns>ActionResult Load SpaOrderTimeSlot Calendar.</returns>
        public ActionResult LoadSpaOrderTimeSlotCalendar()
        {
            var hotelId = GetActiveHotelId();
            var spaOrderTimeSlot = _hotelService.GetSpaOrder(hotelId);

            var eventList = from i in spaOrderTimeSlot
                            from e in i.SpaOrderItems
                            select new
                            {
                                id = e.Id,
                                title = e.SpaServiceDetail.Name + " by " + e.SpaEmployeeDetail.SpaEmployee.Customer.FirstName + " " + e.SpaEmployeeDetail.SpaEmployee.Customer.LastName,
                                start = e.StartDateTime.ToString("s"),
                                end = e.EndDateTime.ToString("s"),
                                //description = e.SpaEmployeeDetail.SpaEmployee.Customer.FirstName + " " + e.SpaEmployeeDetail.SpaEmployee.Customer.LastName,
                                allDay = false
                            };

            var rows = eventList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult DeleteBooking(int id)
        //{
        //    try
        //    {
        //        _hotelService.DeleteSpaServiceBooking(id);
        //        return RedirectToAction("SPAQueue", "HotelSpaService", new { Area = "Admin" });
        //    }
        //    catch
        //    {
        //        return View("SPAQueue");
        //    }
        //}



        //public ActionResult ManageSpaServiceGroup(int spaServiceId)
        //{
        //    var SpaServiceGroups = _hotelService.GetHotelSPAServiceGroups(spaServiceId);
        //    ViewBag.spaServiceId = spaServiceId;
        //    return PartialView("_ManageSpaServiceGroup", SpaServiceGroups);
        //}
        //public ActionResult AddSPAServiceGroup(int spaServiceId)
        //{
        //    SPAServiceGroup newSpaServiceGroup = new SPAServiceGroup();
        //    newSpaServiceGroup.SPAServiceId = spaServiceId;
        //    return View("AddSPAServiceGroup", newSpaServiceGroup);
        //}
        //[HttpPost]
        //public ActionResult AddSPAServiceGroup(SPAServiceGroup SpaServiceGroup, HttpPostedFileBase uploadSpaImage)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var api_path = ConfigurationManager.AppSettings["APIURL"];

        //            if (uploadSpaImage != null)
        //            {
        //                if (uploadSpaImage.ContentLength <= 2000000)
        //                {
        //                    var allowedExtensions = new[] { ".jpg", ".png" };
        //                    var filename = Path.GetFileName(uploadSpaImage.FileName);
        //                    var ext = Path.GetExtension(uploadSpaImage.FileName);

        //                    if (allowedExtensions.Contains(ext))
        //                    {
        //                        SpaServiceGroup.Image = filename;
        //                        if (SpaServiceGroup.Id == 0)
        //                        {
        //                            var spaGroup = _hotelService.CreateSPAServiceGroup(SpaServiceGroup);
        //                            ImageHelper.UploadImage(spaGroup.Id, ImagePathFor.SpaServiceGroup, uploadSpaImage, api_path);
        //                            return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 2, SId = spaGroup.SPAServiceId });
        //                        }
        //                        else
        //                        {
        //                            _hotelService.ModifySpaServiceGroup(SpaServiceGroup);
        //                            ImageHelper.UploadImage(SpaServiceGroup.Id, ImagePathFor.SpaServiceGroup, uploadSpaImage, api_path);
        //                            return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 2, SId = SpaServiceGroup.SPAServiceId });
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ModelState.AddModelError("", "Image Extension is not Valid");
        //                        return View("AddSPAServiceGroup", SpaServiceGroup);
        //                    }
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError("", "Please Select Proper Image ");
        //                    return View("AddSPAServiceGroup", SpaServiceGroup);
        //                }
        //            }
        //            else
        //            {
        //                if (SpaServiceGroup.Id == 0)
        //                {
        //                    _hotelService.CreateSPAServiceGroup(SpaServiceGroup);
        //                    return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 2, SId = SpaServiceGroup.SPAServiceId });
        //                }
        //                else
        //                {
        //                    _hotelService.ModifySpaServiceGroup(SpaServiceGroup);
        //                    return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 2, SId = SpaServiceGroup.SPAServiceId });
        //                }

        //            }
        //        }
        //        else
        //        {
        //            return View("AddSPAServiceGroup", SpaServiceGroup);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(string.Empty, ex.Message);
        //        return View("AddSPAServiceGroup", SpaServiceGroup);
        //    }
        //}
        //public ActionResult EditSPAServiceGroup(int id)
        //{
        //    var spaServiceGroup = _hotelService.GetHotelSPAServiceGroupById(id);
        //    if (spaServiceGroup == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("AddSPAServiceGroup", spaServiceGroup);
        //}
        //[HttpPost]
        //public ActionResult DeleteSPAServiceGroup(int id)
        //{
        //    try
        //    {
        //        var spaServiceGroup = _hotelService.GetHotelSPAServiceGroupById(id);
        //        var api_path = ConfigurationManager.AppSettings["APIURL"];

        //        if (!string.IsNullOrWhiteSpace(spaServiceGroup.Image))
        //        {
        //            string imgPath = @"\Images\Services\SpaService\SpaServiceGroup\" + spaServiceGroup.Id + @"\" + spaServiceGroup.Image;
        //            ImageHelper.DeleteImage(api_path, imgPath);
        //        }

        //        _hotelService.DeleteSpaServiceGroup(id);
        //        return RedirectToAction("ManageSpa", "HotelSpaService", new { Area = "Admin", Type = 2, SId = spaServiceGroup.SPAServiceId });
        //    }
        //    catch
        //    {
        //        return Json(new { failure = true, error = "This SPA Service Group already in use." });
        //    }
        //}


        //public ActionResult ManageSpaServiceDetailsGroup(int spaServiceGroupId)
        //{
        //    var SpaServiceDetailGroup = _hotelService.GetHotelSPAServiceDetailsGroup(spaServiceGroupId);
        //    ViewBag.spaServiceGroupId = spaServiceGroupId;
        //    return PartialView("_ManageSpaServiceDetailsGroup", SpaServiceDetailGroup);
        //}
        //public ActionResult AddSPAServiceDetailGroup(int spaServiceGroupId)
        //{
        //    SPAServiceDetailsGroup newSpaServiceDtlGroup = new SPAServiceDetailsGroup();
        //    newSpaServiceDtlGroup.SPAServiceGroupId = spaServiceGroupId;
        //    return View("AddSPAServiceDetailGroup", newSpaServiceDtlGroup);
        //}
        //[HttpPost]
        //public ActionResult AddSPAServiceDetailGroup(SPAServiceDetailsGroup SpaServiceDetailsGroup, HttpPostedFileBase uploadSpaDtGrpImage)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var api_path = ConfigurationManager.AppSettings["APIURL"];

        //            if (uploadSpaDtGrpImage != null)
        //            {
        //                if (uploadSpaDtGrpImage.ContentLength <= 2000000)
        //                {
        //                    var allowedExtensions = new[] { ".jpg", ".png" };
        //                    var filename = Path.GetFileName(uploadSpaDtGrpImage.FileName);
        //                    var ext = Path.GetExtension(uploadSpaDtGrpImage.FileName);

        //                    if (allowedExtensions.Contains(ext))
        //                    {
        //                        SpaServiceDetailsGroup.Image = filename;
        //                        if (SpaServiceDetailsGroup.Id == 0)
        //                        {
        //                            var spaDtlGroup = _hotelService.CreateSPAServiceDetailsGroup(SpaServiceDetailsGroup);
        //                            ImageHelper.UploadImage(spaDtlGroup.Id, ImagePathFor.SPAServiceDetailsGroup, uploadSpaDtGrpImage, api_path);
        //                            return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 3, GpId = spaDtlGroup.SPAServiceGroupId });
        //                        }
        //                        else
        //                        {
        //                            _hotelService.ModifySPAServiceDetailsGroup(SpaServiceDetailsGroup);
        //                            ImageHelper.UploadImage(SpaServiceDetailsGroup.Id, ImagePathFor.SPAServiceDetailsGroup, uploadSpaDtGrpImage, api_path);
        //                            return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 3, GpId = SpaServiceDetailsGroup.SPAServiceGroupId });
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ModelState.AddModelError("", "Image Extension is not Valid");
        //                        return View("AddSPAServiceDetailGroup", SpaServiceDetailsGroup);
        //                    }
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError("", "Please Select Proper Image ");
        //                    return View("AddSPAServiceDetailGroup", SpaServiceDetailsGroup);
        //                }
        //            }
        //            else
        //            {
        //                if (SpaServiceDetailsGroup.Id == 0)
        //                {
        //                    _hotelService.CreateSPAServiceDetailsGroup(SpaServiceDetailsGroup);
        //                    return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 3, GpId = SpaServiceDetailsGroup.SPAServiceGroupId });
        //                }
        //                else
        //                {
        //                    _hotelService.ModifySPAServiceDetailsGroup(SpaServiceDetailsGroup);
        //                    return RedirectToAction("ManageSpa", "HotelSpaService", new { Type = 3, GpId = SpaServiceDetailsGroup.SPAServiceGroupId });
        //                }

        //            }
        //        }
        //        else
        //        {
        //            return View("AddSPAServiceDetailGroup", SpaServiceDetailsGroup);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(string.Empty, ex.Message);
        //        return View("AddSPAServiceDetailGroup", SpaServiceDetailsGroup);
        //    }
        //}
        //public ActionResult EditSPAServiceDetailGroup(int id)
        //{
        //    var spaServiceDetailGroup = _hotelService.GetHotelSPAServiceDetailsGroupById(id);
        //    if (spaServiceDetailGroup == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("AddSPAServiceDetailGroup", spaServiceDetailGroup);
        //}
        //[HttpPost]
        //public ActionResult DeleteSPAServiceDetailGroup(int id)
        //{
        //    try
        //    {
        //        var spaServiceDetailGroup = _hotelService.GetHotelSPAServiceDetailsGroupById(id);
        //        var api_path = ConfigurationManager.AppSettings["APIURL"];

        //        if (!string.IsNullOrWhiteSpace(spaServiceDetailGroup.Image))
        //        {
        //            string imgPath = @"\Images\Services\SpaService\SPAServiceDetailsGroup\" + spaServiceDetailGroup.Id + @"\" + spaServiceDetailGroup.Image;
        //            ImageHelper.DeleteImage(api_path, imgPath);
        //        }

        //        _hotelService.DeleteSPAServiceDetailsGroup(id);
        //        return RedirectToAction("ManageSpa", "HotelSpaService", new { Area = "Admin", Type = 3, GpId = spaServiceDetailGroup.SPAServiceGroupId });
        //    }
        //    catch
        //    {
        //        return Json(new { failure = true, error = "This SPA Service Group already in use." });
        //    }
        //}


        /// <summary>
        /// Manages the spa service details.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Manage SpaServiceDetails.</returns>
        public ActionResult ManageSpaServiceDetails(int spaServiceId, int hotelServiceId)
        {
            var SpaServiceDetails = _hotelService.GetSpaServiceDetails(spaServiceId);
            ViewBag.SpaServiceId = spaServiceId;
            ViewBag.HotelServiceId = hotelServiceId;
            return View(SpaServiceDetails);
        }
        /// <summary>
        /// Adds the spa service detail.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Add SpaServiceDetail.</returns>
        public ActionResult AddSpaServiceDetail(int spaServiceId, int hotelServiceId)
        {
            SpaServiceDetail newSpaServiceDetail = new SpaServiceDetail();
            ViewBag.HotelServiceId = hotelServiceId;

            newSpaServiceDetail.SpaServiceId = spaServiceId;
            return View("AddEditSpaServiceDetail", newSpaServiceDetail);
        }
        /// <summary>
        /// Saves the spa service detail.
        /// </summary>
        /// <param name="spaServiceDetail">The spa service detail.</param>
        /// <returns>ActionResult Save SpaServiceDetail.</returns>
        [HttpPost]
        public ActionResult SaveSpaServiceDetail(SpaServiceDetail spaServiceDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (spaServiceDetail.Id == 0)
                        _hotelService.AddSpaServiceDetail(spaServiceDetail);
                    else
                        _hotelService.ModifySpaServiceDetail(spaServiceDetail);

                    var hotelServiceId = _hotelService.GetHotelSpaServiceById(spaServiceDetail.SpaServiceId).HotelServiceId;
                    return RedirectToAction("ManageSpaServiceDetails", "HotelSpaService", new { spaServiceId = spaServiceDetail.SpaServiceId, hotelServiceId = hotelServiceId });
                }
                else
                {
                    var hotelServiceId = _hotelService.GetHotelSpaServiceById(spaServiceDetail.SpaServiceId).HotelServiceId;
                    ViewBag.HotelServiceId = hotelServiceId;
                    return View("AddEditSpaServiceDetail", spaServiceDetail);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var hotelServiceId = _hotelService.GetHotelSpaServiceById(spaServiceDetail.SpaServiceId).HotelServiceId;
                ViewBag.HotelServiceId = hotelServiceId;
                return View("AddEditSpaServiceDetail", spaServiceDetail);
            }
        }

        /// <summary>
        /// Edits the spa service detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaServiceDetail.</returns>
        public ActionResult EditSpaServiceDetail(int id)
        {
            var spaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
            ViewBag.HotelServiceId = spaServiceDetail.SpaService.HotelServiceId;

            return View("AddEditSpaServiceDetail", spaServiceDetail);
        }
        /// <summary>
        /// Deletes the spa service detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaServiceDetail.</returns>
        [HttpPost]
        public ActionResult DeleteSpaServiceDetail(int id)
        {
            try
            {
                var spaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
                var HotelServiceId = spaServiceDetail.SpaService.HotelServiceId;

                _hotelService.DeleteSpaServiceDetail(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This SPA Service Detail already in use." });
            }
        }

        /// <summary>
        /// Manages the spa details images.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <returns>ActionResult Manage SpaDetailsImages.</returns>
        public ActionResult ManageSpaDetailsImages(int spaServiceDetId)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(spaServiceDetId);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.SpaServiceDetailId = spaServiceDetId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            var spaDetailsImages = _hotelService.GetSpaServiceDetailImages(spaServiceDetId);
            return View(spaDetailsImages);
        }

        /// <summary>
        /// Adds the spa detail images.
        /// </summary>
        /// <param name="spaDetId">The spa det identifier.</param>
        /// <returns>ActionResult Add SpaDetailImages.</returns>
        public ActionResult AddSpaDetailImages(int spaDetId)
        {
            SpaServiceDetailImages spaDetImages = new SpaServiceDetailImages();
            spaDetImages.SpaServiceDetailId = spaDetId;
            return View(spaDetImages);
        }

        /// <summary>
        /// Saves the spa detail images.
        /// </summary>
        /// <param name="spaDetImages">The spa det images.</param>
        /// <param name="uploadSpaDetImages">The upload spa det images.</param>
        /// <returns>ActionResult Save SpaDetailImages.</returns>
        [HttpPost]
        public ActionResult SaveSpaDetailImages(SpaServiceDetailImages spaDetImages, HttpPostedFileBase[] uploadSpaDetImages)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    //iterating through multiple file collection   
                    var count = 1;
                    foreach (HttpPostedFileBase eachSpaDetImage in uploadSpaDetImages)
                    {
                        if (eachSpaDetImage != null)
                        {
                            if (eachSpaDetImage.ContentLength <= 2000000)
                            {
                                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                                var filename = Path.GetFileName(eachSpaDetImage.FileName);
                                filename = filename.Replace(" ", "_");
                                var ext = Path.GetExtension(eachSpaDetImage.FileName);
                                if (allowedExtensions.Contains(ext))
                                {
                                    if (count == 1)
                                    {
                                        var spaDetailImages = _hotelService.GetSpaServiceDetailImages(spaDetImages.SpaServiceDetailId).Where(i => i.IsPrimary == true).FirstOrDefault();
                                        if (spaDetailImages != null)
                                        {
                                            if (spaDetailImages.IsPrimary == true)
                                            {
                                                if (spaDetImages.IsPrimary)
                                                {
                                                    spaDetailImages.IsPrimary = false;
                                                    _hotelService.UpdateSpaDetImage(spaDetailImages);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            spaDetImages.IsPrimary = true;
                                        }
                                    }
                                    else
                                    {
                                        spaDetImages.IsPrimary = false;
                                    }
                                    spaDetImages.Image = filename;
                                    ImageHelper.UploadImage(spaDetImages.SpaServiceDetailId, ImagePathFor.SpaServiceDetailImages, eachSpaDetImage, api_path);
                                    _hotelService.AddSpaDetailImage(spaDetImages);
                                }
                                else
                                {
                                    ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                    return View("AddSpaDetailImages", spaDetImages);
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                                return View("AddSpaDetailImages", spaDetImages);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", " Please select proper Image");
                            return View("AddSpaDetailImages", spaDetImages);
                        }
                        count = count + 1;
                    }
                    return RedirectToAction("ManageSpaDetailsImages", new { spaServiceDetId = spaDetImages.SpaServiceDetailId });
                }
                else
                {
                    return View("AddSpaDetailImages", spaDetImages);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddSpaDetailImages", spaDetImages);
            }
        }

        /// <summary>
        /// Deletes the spa detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaDetailImage.</returns>
        [HttpPost]
        public ActionResult DeleteSpaDetailImage(int id)
        {
            var spaDetImage = _hotelService.GetSpaDetailImageById(id);
            var api_path = ConfigurationManager.AppSettings["APIURL"];

            if (!string.IsNullOrWhiteSpace(spaDetImage.Image))
            {
                string imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + spaDetImage.SpaServiceDetailId + @"\" + spaDetImage.Image;
                ImageHelper.DeleteImage(api_path, imgPath);
            }
            if (spaDetImage.IsPrimary == true)
            {
                var spaDetailImg = _hotelService.GetSpaServiceDetailImages(spaDetImage.SpaServiceDetailId).Where(r => r.Id != id).FirstOrDefault();
                if (spaDetailImg != null)
                {
                    spaDetailImg.IsPrimary = true;
                    _hotelService.UpdateSpaDetImage(spaDetailImg);
                    _hotelService.DeleteSpaServiceDetailImage(id);
                    return Json(new { IsPrimaryDeleted = true });
                }
            }

            _hotelService.DeleteSpaServiceDetailImage(id);
            return Json(new { IsPrimaryDeleted = false });
        }

        /// <summary>
        /// Updates the spa detail image primary.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update SpaDetailImagePrimary.</returns>
        [HttpPost]
        public ActionResult UpdateSpaDetailImagePrimary(int id)
        {
            try
            {
                var spaDetImage = _hotelService.GetSpaDetailImageById(id);

                var spaDetailImg = _hotelService.GetSpaServiceDetailImages(spaDetImage.SpaServiceDetailId).Where(i => i.IsPrimary == true).FirstOrDefault();
                if (spaDetailImg != null && spaDetailImg.Id != spaDetImage.Id)
                {
                    spaDetailImg.IsPrimary = false;
                    _hotelService.UpdateSpaDetImage(spaDetailImg);
                    spaDetImage.IsPrimary = true;

                    _hotelService.UpdateSpaDetImage(spaDetImage);
                    return RedirectToAction("ManageSpaDetailsImages", "HotelSpaService", new { spaServiceDetId = spaDetImage.SpaServiceDetailId });
                }
                else
                {
                    return Json(new { failure = true, error = "At least one image must be primary." });
                }


            }
            catch
            {
                return View("ManageSpaDetailsImages");
            }
        }

        /// <summary>
        /// Updates the spa detail image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update SpaDetailImage Active.</returns>
        [HttpPost]
        public ActionResult UpdateSpaDetailImageActive(int id)
        {
            try
            {
                var spaDetImage = _hotelService.GetSpaDetailImageById(id);
                if (spaDetImage.IsActive)
                    spaDetImage.IsActive = false;
                else
                    spaDetImage.IsActive = true;

                var updatedSpaDetImg = _hotelService.UpdateSpaDetImage(spaDetImage);
                return RedirectToAction("ManageSpaDetailsImages", "HotelSpaService", new { spaServiceDetId = spaDetImage.SpaServiceDetailId });
            }
            catch
            {
                return View("ManageSpaDetailsImages");
            }
        }

        //Spa Details Additional

        /// <summary>
        /// Manages the spa details additional.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <returns>ActionResult Manage SpaDetailsAdditional.</returns>
        public ActionResult ManageSpaDetailsAdditional(int spaServiceDetId)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(spaServiceDetId);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.SpaServiceDetailId = spaServiceDetId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            return View();
        }

        /// <summary>
        /// Loads the spa detail additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaDetailAdditional.</returns>
        [HttpPost]
        public ActionResult LoadSpaDetailAdditional(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var spaDetAdditionals = _hotelService.GetAllSpaDetAdditionals(id, skip, pageSize, out Total);
            var data = spaDetAdditionals.Select(b => new
            {
                SpaDetAddId = b.Id,
                SpaDetailsName = b.SpaServiceDetail.Name,
                SpaAddGrpName = b.SpaAdditionalGroup.Name,
                IsActive = b.IsActive
            });
            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the spa detail additional.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <returns>ActionResult Add SpaDetailAdditional.</returns>
        public ActionResult AddSpaDetailAdditional(int spaServiceDetailId)
        {
            SpaDetailAdditional spaDetAdd = new SpaDetailAdditional();
            spaDetAdd.SpaDetailId = spaServiceDetailId;
            var unassignSAG = _hotelService.GetUnassignSpaAddGroups(spaServiceDetailId, GetActiveHotelId()).Select(
                 r => new
                 {
                     SpaAdditionalGroupId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.SpaAdditionalGroups = new MultiSelectList(unassignSAG, "SpaAdditionalGroupId", "Name");
            return View(spaDetAdd);
        }

        /// <summary>
        /// Saves the spa det additional.
        /// </summary>
        /// <param name="spaDetAdditional">The spa det additional.</param>
        /// <returns>ActionResult Save SpaDetAdditional.</returns>
        [HttpPost]
        public ActionResult SaveSpaDetAdditional(SpaDetailAdditional spaDetAdditional)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<SpaDetailAdditional> spaDetAddi = new List<SpaDetailAdditional>();

                    var SpaAddGrpIds = Request.Form["SpaAdditionalGroupId"];

                    if (SpaAddGrpIds != null)
                    {
                        string[] SAGIds = SpaAddGrpIds.Split(',');
                        foreach (var eachSAGId in SAGIds)
                        {
                            SpaDetailAdditional eachSDA = new SpaDetailAdditional()
                            {
                                SpaDetailId = spaDetAdditional.SpaDetailId,
                                SpaAdditionalGroupId = Convert.ToInt32(eachSAGId),
                                IsActive = true
                            };
                            spaDetAddi.Add(eachSDA);
                        }
                    }

                    if (spaDetAdditional.Id == 0)
                        _hotelService.CreateSpaDetAdditional(spaDetAddi);

                    return RedirectToAction("ManageSpaDetailsAdditional", "HotelSpaService", new { area = "admin", spaServiceDetId = spaDetAdditional.SpaDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignSAG = _hotelService.GetUnassignSpaAddGroups(spaDetAdditional.SpaDetailId, GetActiveHotelId()).Select(
                                        r => new
                                        {
                                            SpaAdditionalGroupId = r.Id,
                                            Name = r.Name
                                        }).ToList();
                    ViewBag.SpaAdditionalGroups = new MultiSelectList(unassignSAG, "SpaAdditionalGroupId", "Name");
                    return View(spaDetAdditional);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignSAG = _hotelService.GetUnassignSpaAddGroups(spaDetAdditional.SpaDetailId, GetActiveHotelId()).Select(
                                         r => new
                                         {
                                             SpaAdditionalGroupId = r.Id,
                                             Name = r.Name
                                         }).ToList();
                ViewBag.SpaAdditionalGroups = new MultiSelectList(unassignSAG, "SpaAdditionalGroupId", "Name");
                return View(spaDetAdditional);
            }
        }

        /// <summary>
        /// Updates the spa det additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update SpaDetAdditional.</returns>
        [HttpPost]
        public ActionResult UpdateSpaDetAdditional(int id)
        {
            try
            {
                var spadetadd = _hotelService.GetSpadetAdditionalById(id);
                if (spadetadd.IsActive)
                    spadetadd.IsActive = false;
                else
                    spadetadd.IsActive = true;

                var updatedSDA = _hotelService.ModifySpaDetAdditional(spadetadd);
                return RedirectToAction("ManageSpaDetailsAdditional", "HotelSpaService", new { area = "admin", spaServiceDetId = updatedSDA.SpaDetailId });
            }
            catch
            {
                return View("ManageSpaDetailAdditional");
            }
        }

        /// <summary>
        /// Deletes the spa det add.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaDetAdditional.</returns>
        [HttpPost]
        public ActionResult DeleteSpaDetAdd(int id)
        {
            try
            {
                _hotelService.DeleteSpadetAdditional(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa detail additional already in use." });
            }
        }

        //Spa Additional Group

        /// <summary>
        /// Manages the spa additional group.
        /// </summary>
        /// <returns>ActionResult Manage SpaAdditionalGroup.</returns>
        public ActionResult ManageSpaAdditionalGroup()
        {
            return View();
        }
        /// <summary>
        /// Loads the spa addional groups.
        /// </summary>
        /// <returns>ActionResult LoadS paAddionalGroups.</returns>
        [HttpPost]
        public ActionResult LoadSpaAddionalGroups()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetSpaAdditionalGroups(GetActiveHotelId(), skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                SpaAddGroupId = b.Id,
                GroupName = b.Name,
                GroupDescription = b.Description,
                MinSelected = b.MinSelected,
                MaxSelected = b.MaxSelected,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa additional group.
        /// </summary>
        /// <returns>ActionResult Add SpaAdditionalGroup.</returns>
        public ActionResult AddSpaAdditionalGroup()
        {
            SpaAdditionalGroup spaAddGroup = new SpaAdditionalGroup();
            spaAddGroup.HotelId = GetActiveHotelId();
            return View("AddEditSpaAdditionalGroup", spaAddGroup);
        }

        /// <summary>
        /// Saves the spa additional group.
        /// </summary>
        /// <param name="spaAddGroup">The spa add group.</param>
        /// <returns>ActionResult Save SpaAdditionalGroup.</returns>
        public ActionResult SaveSpaAdditionalGroup(SpaAdditionalGroup spaAddGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (spaAddGroup.MaxSelected < spaAddGroup.MinSelected)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Invalid_MaximumSelected);
                        return View("AddEditSpaAdditionalGroup", spaAddGroup);
                    }
                    if (spaAddGroup.Id == 0)
                    {
                        _hotelService.CreateSpaAdditionalGroup(spaAddGroup);
                        return RedirectToAction("ManageSpaAdditionalGroup");
                    }
                    else
                    {
                        _hotelService.ModifySpaAdditionalGroup(spaAddGroup);
                        return RedirectToAction("ManageSpaAdditionalGroup");
                    }
                }
                else
                {
                    return View("AddEditSpaAdditionalGroup", spaAddGroup);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaAdditionalGroup", spaAddGroup);
            }
        }

        /// <summary>
        /// Edits the spa additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaAdditionalGroup.</returns>
        public ActionResult EditSpaAdditionalGroup(int id)
        {
            var spaAddGrp = _hotelService.GetSpaAdditionalGroupById(id);
            return View("AddEditSpaAdditionalGroup", spaAddGrp);
        }
        /// <summary>
        /// Deletes the spa additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaAdditionalGroup.</returns>
        [HttpPost]
        public ActionResult DeleteSpaAdditionalGroup(int id)
        {
            try
            {
                _hotelService.DeleteSpaAdditionalGroup(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa additional group already in use." });
            }
        }

        //Spa Additional Element
        /// <summary>
        /// Manages the spa additional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaAdditionalElements.</returns>
        public ActionResult ManageSpaAdditionalElements(int id)
        {
            ViewBag.SpaAdditionalGroupId = id;
            return View();
        }
        /// <summary>
        /// Loads the spa addional elements.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaAddionalElements.</returns>
        [HttpPost]
        public ActionResult LoadSpaAddionalElements(int id)
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var bookings = _hotelService.GetSpaAdditionalElements(id, skip, pageSize, out Total);
            var data = bookings.Select(b => new
            {
                SpaAddElementId = b.Id,
                ElementName = b.Name,
                ElementDescription = b.Description,
                GroupName = b.SpaAdditionalGroup.Name,
                Price = b.Price,
                IsActive = b.IsActive,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.SpaAdditionalElementImage, b.Image, b.SpaAdditionalGroupId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa additional element.
        /// </summary>
        /// <param name="spaAdditionalGroupId">The spa additional group identifier.</param>
        /// <returns>ActionResult Add SpaAdditionalElement.</returns>
        public ActionResult AddSpaAdditionalElement(int spaAdditionalGroupId)
        {
            SpaAdditionalElement spaAddElement = new SpaAdditionalElement();
            spaAddElement.SpaAdditionalGroupId = spaAdditionalGroupId;
            return View("AddEditSpaAdditionalElement", spaAddElement);
        }

        /// <summary>
        /// Saves the spa additional element.
        /// </summary>
        /// <param name="spaAddElement">The spa add element.</param>
        /// <param name="uploadSpaAdditionaElementImage">The upload spa additiona element image.</param>
        /// <returns>ActionResult Save SpaAdditionalElement.</returns>
        public ActionResult SaveSpaAdditionalElement(SpaAdditionalElement spaAddElement, HttpPostedFileBase uploadSpaAdditionaElementImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadSpaAdditionaElementImage != null)
                    {
                        if (uploadSpaAdditionaElementImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadSpaAdditionaElementImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadSpaAdditionaElementImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                spaAddElement.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditSpaAdditionalElement", spaAddElement);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditSpaAdditionalElement", spaAddElement);
                        }
                    }

                    if (spaAddElement.Id == 0)
                    {
                        var resSpaAddElement = _hotelService.CreateSpaAdditionalElement(spaAddElement);
                        if (uploadSpaAdditionaElementImage != null)
                        {
                            ImageHelper.UploadImage(resSpaAddElement.SpaAdditionalGroupId, ImagePathFor.SpaAdditionalElementImage, uploadSpaAdditionaElementImage, api_path, resSpaAddElement.Id);
                        }
                        return RedirectToAction("ManageSpaAdditionalElements", new { id = spaAddElement.SpaAdditionalGroupId });
                    }
                    else
                    {

                        var resSpaAddElement = _hotelService.ModifySpaAdditionalElement(spaAddElement);
                        if (uploadSpaAdditionaElementImage != null)
                        {
                            ImageHelper.UploadImage(resSpaAddElement.SpaAdditionalGroupId, ImagePathFor.SpaAdditionalElementImage, uploadSpaAdditionaElementImage, api_path, resSpaAddElement.Id);
                        }
                        return RedirectToAction("ManageSpaAdditionalElements", new { id = spaAddElement.SpaAdditionalGroupId });
                    }
                }
                else
                {
                    return View("AddEditSpaAdditionalElement", spaAddElement);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaAdditionalElement", spaAddElement);
            }
        }

        /// <summary>
        /// Edits the spa additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaAdditionalElement.</returns>
        public ActionResult EditSpaAdditionalElement(int id)
        {
            var spaAddElement = _hotelService.GetSpaAdditionalElementById(id);
            return View("AddEditSpaAdditionalElement", spaAddElement);
        }
        /// <summary>
        /// Deletes the spa additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaAdditionalElement.</returns>
        [HttpPost]
        public ActionResult DeleteSpaAdditionalElement(int id)
        {
            try
            {
                _hotelService.DeleteSpaAdditionalElement(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa additional element already in use." });
            }
        }

        //Spa Employee
        /// <summary>
        /// Manages the spa employee.
        /// </summary>
        /// <returns>ActionResult Manage SpaEmployee.</returns>
        public ActionResult ManageSpaEmployee()
        {
            return View();
        }

        /// <summary>
        /// Loads the spa employee.
        /// </summary>
        /// <returns>ActionResult Load SpaEmployee.</returns>
        [HttpPost]
        public ActionResult LoadSpaEmployee()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaEmployee = _hotelService.GetAllSpaEmployee(GetActiveHotelId(), skip, pageSize, out Total);
            var data = spaEmployee.Select(b => new
            {
                SpaEmployeeId = b.Id,
                Name = b.Customer.FirstName + " " + b.Customer.LastName,
                Age = b.Age,
                Education = b.Education,
                Experience = b.Experience,
                Skill = b.Skill,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa employee.
        /// </summary>
        /// <returns>ActionResult Add SpaEmployee.</returns>
        public ActionResult AddSpaEmployee()
        {
            var hotelId = GetActiveHotelId();
            SpaEmployee spaEmployee = new SpaEmployee();
            spaEmployee.HotelId = hotelId;
            ViewBag.spaCustomer = _hotelService.GetCustomerForSpa(hotelId).Select(s => new { Id = s.Id, Name = s.FirstName + " " + s.LastName }).ToList();
            return View("AddEditSpaEmployee", spaEmployee);
        }

        /// <summary>
        /// Saves the spa employee.
        /// </summary>
        /// <param name="spaEmployee">The spa employee.</param>
        /// <returns>ActionResult Save SpaEmployee.</returns>
        public ActionResult SaveSpaEmployee(SpaEmployee spaEmployee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (spaEmployee.Id == 0)
                    {
                        _hotelService.CreateSpaEmployee(spaEmployee);
                        return RedirectToAction("ManageSpaEmployee");
                    }
                    else
                    {
                        _hotelService.ModifySpaEmployee(spaEmployee);
                        return RedirectToAction("ManageSpaEmployee");
                    }
                }
                else
                {
                    return View("AddEditSpaEmployee", spaEmployee);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaEmployee", spaEmployee);
            }
        }

        /// <summary>
        /// Edits the spa employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaEmployee.</returns>
        public ActionResult EditSpaEmployee(int id)
        {
            var hotelId = GetActiveHotelId();
            var spaEmployee = _hotelService.GetSpaEmployeeById(id);
            return View("AddEditSpaEmployee", spaEmployee);
        }
        /// <summary>
        /// Deletes the spa employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaEmployee.</returns>
        [HttpPost]
        public ActionResult DeleteSpaEmployee(int id)
        {
            try
            {
                _hotelService.DeleteSpaEmployee(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa Employee already in use." });
            }
        }

        //Spa Employee Details

        /// <summary>
        /// Manages the spa employee details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaEmployeeDetails.</returns>
        public ActionResult ManageSpaEmployeeDetails(int id)
        {
            ViewBag.SpaEmployeeId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa employee details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaEmployeeDetails.</returns>
        [HttpPost]
        public ActionResult LoadSpaEmployeeDetails(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaEmployeeDetails = _hotelService.GetSpaEmployeeDetails(id, skip, pageSize, out Total);
            var data = spaEmployeeDetails.Select(b => new
            {
                SpaEmployeeDetailsId = b.Id,
                Name = b.SpaServiceDetail.Name,
                Description = b.SpaServiceDetail.Description,
                Duration = b.SpaServiceDetail.Duration,
                IsActive = b.SpaServiceDetail.IsActive.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the spa employee detail.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>ActionResult Add SpaEmployeeDetail.</returns>
        public ActionResult AddSpaEmployeeDetail(int spaEmployeeId)
        {
            SpaEmployeeDetails spaEmpDetails = new SpaEmployeeDetails();
            spaEmpDetails.SpaEmployeeId = spaEmployeeId;
            var hotelId = GetActiveHotelId();
            var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetails(spaEmployeeId, hotelId).Select(
                 r => new
                 {
                     SpaServiceDetailId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
            return View(spaEmpDetails);
        }


        /// <summary>
        /// Saves the spa employee detail.
        /// </summary>
        /// <param name="spaEmployeeDet">The spa employee det.</param>
        /// <returns>ActionResult Save SpaEmployeeDetail.</returns>
        [HttpPost]
        public ActionResult SaveSpaEmployeeDetail(SpaEmployeeDetails spaEmployeeDet)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<SpaEmployeeDetails> spaEmployeDetails = new List<SpaEmployeeDetails>();

                    var SpaDetailIds = Request.Form["SpaServiceDetailId"];

                    if (SpaDetailIds != null)
                    {
                        string[] spaDetIds = SpaDetailIds.Split(',');
                        foreach (var spaSerId in spaDetIds)
                        {
                            SpaEmployeeDetails spaEmpDet = new SpaEmployeeDetails()
                            {
                                SpaEmployeeId = spaEmployeeDet.SpaEmployeeId,
                                SpaDetailId = Convert.ToInt32(spaSerId)
                            };
                            spaEmployeDetails.Add(spaEmpDet);
                        }
                    }

                    if (spaEmployeeDet.Id == 0)
                        _hotelService.CreateSpaEmployeeDetails(spaEmployeDetails);

                    return RedirectToAction("ManageSpaEmployeeDetails", "HotelSpaService", new { area = "admin", id = spaEmployeeDet.SpaEmployeeId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetails(spaEmployeeDet.SpaEmployeeId, GetActiveHotelId()).Select(
                                                  r => new
                                                  {
                                                      SpaServiceDetailId = r.Id,
                                                      Name = r.Name
                                                  }).ToList();
                    ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
                    return View(spaEmployeeDet);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetails(spaEmployeeDet.SpaEmployeeId, GetActiveHotelId()).Select(
                                                  r => new
                                                  {
                                                      SpaServiceDetailId = r.Id,
                                                      Name = r.Name
                                                  }).ToList();
                ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
                return View(spaEmployeeDet);
            }
        }

        /// <summary>
        /// Deletes the spa employee details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaEmployeeDetails.</returns>
        [HttpPost]
        public ActionResult DeleteSpaEmployeeDetails(int id)
        {
            try
            {
                _hotelService.DeleteSpaEmployeeDetails(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa employee details already in use." });
            }
        }

        //Spa Employee TimeSlot
        /// <summary>
        /// Manages the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaEmployeeTimeSlot.</returns>
        public ActionResult ManageSpaEmployeeTimeSlot(int id)
        {
            ViewBag.SpaEmployeeId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaEmployeeTimeSlot.</returns>
        [HttpPost]
        public ActionResult LoadSpaEmployeeTimeSlot(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaEmployeeDetailsTimeSlot = _hotelService.GetSpaEmployeeTimeSlots(id, skip, pageSize, out Total);
            var data = spaEmployeeDetailsTimeSlot.Select(b => new
            {
                EmployeeTimeSlotId = b.Id,
                StartDateTime = b.StartDateTime,
                EndDateTime = b.EndDateTime,
                Description = b.Description,
                IsActive = b.IsActive.ToString(),
                CreationDate = b.CreationDate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>ActionResult Add SpaEmployeeTimeSlot.</returns>
        public ActionResult AddSpaEmployeeTimeSlot(int spaEmployeeId)
        {
            SpaEmployeeTimeSlot spaEmployeeTimeSlot = new SpaEmployeeTimeSlot()
            {
                SpaEmployeeId = spaEmployeeId,
                StartDateTime = DateTime.Now,
                EndDateTime = DateTime.Now
            };
            return View("AddEditEmployeeTimeSlot", spaEmployeeTimeSlot);
        }

        /// <summary>
        /// Saves the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeTimeSlot">The spa employee time slot.</param>
        /// <returns>ActionResult Save SpaEmployeeTimeSlot.</returns>
        public ActionResult SaveSpaEmployeeTimeSlot(SpaEmployeeTimeSlot spaEmployeeTimeSlot)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (spaEmployeeTimeSlot.StartDateTime > spaEmployeeTimeSlot.EndDateTime)
                    {
                        ModelState.AddModelError(string.Empty, "End Datetime must be greater than start Datetime");
                        return View("AddEditEmployeeTimeSlot", spaEmployeeTimeSlot);
                    }
                    if (spaEmployeeTimeSlot.Id == 0)
                    {
                        _hotelService.CreateSpaEmployeeTimeSlot(spaEmployeeTimeSlot);
                        return RedirectToAction("ManageSpaEmployeeTimeSlot", new { id = spaEmployeeTimeSlot.SpaEmployeeId });
                    }
                    else
                    {
                        _hotelService.ModifySpaEmployeeTimeSlot(spaEmployeeTimeSlot);
                        return RedirectToAction("ManageSpaEmployeeTimeSlot", new { id = spaEmployeeTimeSlot.SpaEmployeeId });
                    }
                }
                else
                {
                    return View("AddEditEmployeeTimeSlot", spaEmployeeTimeSlot);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditEmployeeTimeSlot", spaEmployeeTimeSlot);
            }
        }

        /// <summary>
        /// Edits the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaEmployeeTimeSlot.</returns>
        public ActionResult EditSpaEmployeeTimeSlot(int id)
        {
            var spaEmployeeTimeSlot = _hotelService.GetSpaEmployeeTimeSlotById(id);
            return View("AddEditEmployeeTimeSlot", spaEmployeeTimeSlot);
        }

        /// <summary>
        /// Deletes the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaEmployeeTimeSlot.</returns>
        [HttpPost]
        public ActionResult DeleteSpaEmployeeTimeSlot(int id)
        {
            try
            {
                _hotelService.DeleteSpaEmployeeTimeSlot(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa Employee TimeSlot already in use." });
            }
        }

        //[HttpGet]
        //public ActionResult LoadendDateTime(int SpaEmployeeDetailsId, DateTime startDate)
        //{
        //    try
        //    {
        //        TimeSpan? duration = _hotelService.GetSpaEmployeeDetailsById(SpaEmployeeDetailsId).SpaServiceDetail.Duration;

        //        TimeSpan time = new TimeSpan(duration.Value.Hours, duration.Value.Minutes, duration.Value.Seconds);

        //        DateTime endDate = startDate.Add(time);

        //        return Json(endDate, JsonRequestBehavior.AllowGet);
        //    }
        //    catch
        //    {
        //        return Json(new { error = "End Date is not valid for this start time" });
        //    }
        //}

        /// <summary>
        /// Manages the spa emp time slot calendar.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaEmpTimeSlotCalendar.</returns>
        public ActionResult ManageSpaEmpTimeSlotCalendar(int id)
        {
            ViewBag.SpaEmployeeId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa emp time slot calendar.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaEmpTimeSlotCalendar.</returns>
        public ActionResult LoadSpaEmpTimeSlotCalendar(int id)
        {

            var spaEmployeeTimeSlot = _hotelService.GetSpaEmplTimeSlotCalendar(id);

            var eventList = from e in spaEmployeeTimeSlot
                            select new
                            {
                                id = e.Id,
                                title = e.Description,
                                start = e.StartDateTime.ToString("s"),
                                end = e.EndDateTime.ToString("s"),
                                allDay = false

                            };

            var rows = eventList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Spas the reviews.
        /// </summary>
        /// <returns>ActionResult SpaReviews.</returns>
        public ActionResult SpaReviews()
        {
            return View("AllSpaReviews");
        }

        /// <summary>
        /// Loads the spa reviews.
        /// </summary>
        /// <returns>ActionResult Load SpaReviews.</returns>
        [HttpPost]
        public ActionResult LoadSpaReviews()
        {
            var hotelId = GetActiveHotelId();

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaReviews = _hotelService.GetAllHotelSpaServiceReviews(hotelId, skip, pageSize, out Total);
            var data = spaReviews.Select(b => new
            {
                SpaReviewId = b.Id,
                ServiceName = b.SpaServiceDetail.Name,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Reviews = b.Score,
                Comment = b.Comment

            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the spa rooms.
        /// </summary>
        /// <returns>ActionResult Manage SpaRooms.</returns>
        public ActionResult ManageSpaRooms()
        {
            return View();
        }

        /// <summary>
        /// Loads the spa rooms.
        /// </summary>
        /// <returns>ActionResult Load SpaRooms.</returns>
        [HttpPost]
        public ActionResult LoadSpaRooms()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var hotelId = GetActiveHotelId();
            var spaRoom = _hotelService.GetAllSpaRooms(hotelId, skip, pageSize, out Total);
            var data = spaRoom.Select(b => new
            {
                SpaRoomId = b.Id,
                HotelName = b.Hotel.Name,
                Number = b.Number,
                Description = b.Description,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the spa room.
        /// </summary>
        /// <returns>ActionResult Add SpaRoom.</returns>
        public ActionResult AddSpaRoom()
        {
            var hotelId = GetActiveHotelId();
            SpaRoom spaRoom = new SpaRoom();
            spaRoom.HotelId = hotelId;
            return View("AddEditSpaRoom", spaRoom);
        }


        /// <summary>
        /// Saves the spa room.
        /// </summary>
        /// <param name="spaRoom">The spa room.</param>
        /// <returns>ActionResult Save SpaRoom.</returns>
        public ActionResult SaveSpaRoom(SpaRoom spaRoom)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spaRoomExist = _hotelService.GetSpaRoom(spaRoom.HotelId, spaRoom.Number);

                    if (spaRoom.Id == 0)
                    {
                        if (spaRoomExist != null)
                        {
                            ModelState.AddModelError(string.Empty, Resources.Room_Num_Exist);
                            return View("AddEditSpaRoom", spaRoom);
                        }
                        _hotelService.CreateSpaRoom(spaRoom);
                        return RedirectToAction("ManageSpaRooms");
                    }
                    else
                    {
                        if (spaRoomExist != null && spaRoomExist.Id != spaRoom.Id)
                        {
                            ModelState.AddModelError(string.Empty, Resources.Room_Num_Exist);
                            return View("AddEditSpaRoom", spaRoom);
                        }
                        _hotelService.ModifySpaRoom(spaRoom);
                        return RedirectToAction("ManageSpaRooms");
                    }
                }
                else
                {
                    return View("AddEditSpaRoom", spaRoom);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaRoom", spaRoom);
            }
        }

        /// <summary>
        /// Edits the spa room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaRoom.</returns>
        public ActionResult EditSpaRoom(int id)
        {
            var spaRoom = _hotelService.GetSpaRoomById(id);
            return View("AddEditSpaRoom", spaRoom);
        }
        /// <summary>
        /// Deletes the spa room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaRoom.</returns>
        [HttpPost]
        public ActionResult DeleteSpaRoom(int id)
        {
            try
            {
                _hotelService.DeleteSpaRoom(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa Room already in use." });
            }
        }
        /// <summary>
        /// Manages the spa room details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaRoomDetails.</returns>
        public ActionResult ManageSpaRoomDetails(int id)
        {
            ViewBag.SpaRoomId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa room details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaRoomDetails.</returns>
        [HttpPost]
        public ActionResult LoadSpaRoomDetails(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var spaRoomDetails = _hotelService.GetAllSpaRoomDetails(id, skip, pageSize, out Total);
            var data = spaRoomDetails.Select(b => new
            {
                SpaRoomDetailId = b.Id,
                Number = b.SpaRoom.Number,
                SpaDetails = b.SpaServiceDetail.Name
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa room detail.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <returns>ActionResult Add SpaRoomDetail.</returns>
        public ActionResult AddSpaRoomDetail(int spaRoomId)
        {
            SpaRoomDetails spaRoomDet = new SpaRoomDetails();
            spaRoomDet.SpaRoomId = spaRoomId;
            var unassignSpaServiceDet = _hotelService.GetUnassignSpaDetails(spaRoomId, GetActiveHotelId()).Select(
                 r => new
                 {
                     SpaServiceDetailId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
            return View(spaRoomDet);
        }


        /// <summary>
        /// Saves the spa room detail.
        /// </summary>
        /// <param name="spaRoomDet">The spa room det.</param>
        /// <returns>ActionResult Save SpaRoomDetail.</returns>
        [HttpPost]
        public ActionResult SaveSpaRoomDetail(SpaRoomDetails spaRoomDet)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<SpaRoomDetails> spaRoomDetails = new List<SpaRoomDetails>();

                    var SpaDetailIds = Request.Form["SpaServiceDetailId"];

                    if (SpaDetailIds != null)
                    {
                        string[] spaDetIds = SpaDetailIds.Split(',');
                        foreach (var spaSerId in spaDetIds)
                        {
                            SpaRoomDetails spaServDet = new SpaRoomDetails()
                            {
                                SpaRoomId = spaRoomDet.SpaRoomId,
                                SpaServiceDetailId = Convert.ToInt32(spaSerId)
                            };
                            spaRoomDetails.Add(spaServDet);
                        }
                    }

                    if (spaRoomDet.Id == 0)
                        _hotelService.CreateSpaRoomDetails(spaRoomDetails);

                    return RedirectToAction("ManageSpaRoomDetails", "HotelSpaService", new { area = "admin", id = spaRoomDet.SpaRoomId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignSpaServiceDet = _hotelService.GetUnassignSpaDetails(spaRoomDet.SpaRoomId, GetActiveHotelId()).Select(
                                                  r => new
                                                  {
                                                      SpaServiceDetailId = r.Id,
                                                      Name = r.Name
                                                  }).ToList();
                    ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
                    return View(spaRoomDet);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignSpaServiceDet = _hotelService.GetUnassignSpaDetails(spaRoomDet.SpaRoomId, GetActiveHotelId()).Select(
                                            r => new
                                            {
                                                SpaServiceDetailId = r.Id,
                                                Name = r.Name
                                            }).ToList();
                ViewBag.SpaServiceDetails = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetailId", "Name");
                return View(spaRoomDet);
            }
        }

        /// <summary>
        /// Deletes the spa room detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaRoomDetail.</returns>
        [HttpPost]
        public ActionResult DeleteSpaRoomDetail(int id)
        {
            try
            {
                _hotelService.DeleteSpaRoomDetail(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa room details already in use." });
            }
        }

        #region Spa Service Details Offer

        /// <summary>
        /// Manages the spa service offers.
        /// </summary>
        /// <returns>ActionResult Manage SpaServiceOffers.</returns>
        public ActionResult ManageSpaServiceOffers()
        {
            return View();
        }

        /// <summary>
        /// Loads the spa service offers.
        /// </summary>
        /// <returns>ActionResult Load SpaServiceOffers.</returns>
        [HttpPost]
        public ActionResult LoadSpaServiceOffers()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaServiceOffers = _hotelService.GetSpaServiceOffer(GetActiveHotelId(), skip, pageSize, out Total);
            var data = spaServiceOffers.Select(b => new
            {
                SpaServiceOfferId = b.Id,
                SpaServiceDetailsName = b.SpaServiceDetail.Name,
                OfferDescription = b.Description,
                OfferPercentage = b.Percentage,
                IsActive = b.IsActive,
                OfferImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.SpaServiceDetailOfferImage, b.OfferImage, b.SpaServiceDetailId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa service offer.
        /// </summary>
        /// <returns>ActionResult Add SpaServiceOffer.</returns>
        public ActionResult AddSpaServiceOffer()
        {
            var HotelId = GetActiveHotelId();
            SpaServiceOffer spaServiceOffer = new SpaServiceOffer();
            ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(HotelId).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
            return View("AddEditSpaServiceOffer", spaServiceOffer);
        }

        /// <summary>
        /// Saves the spa service offer.
        /// </summary>
        /// <param name="spaServiceOffer">The spa service offer.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save SpaServiceOffer.</returns>
        public ActionResult SaveSpaServiceOffer(SpaServiceOffer spaServiceOffer, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.ContainsKey("SpaServiceDetail.Price"))
                    ModelState["SpaServiceDetail.Price"].Errors.Clear();

                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                spaServiceOffer.OfferImage = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                                return View("AddEditSpaServiceOffer", spaServiceOffer);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                            return View("AddEditSpaServiceOffer", spaServiceOffer);
                        }
                    }
                    else
                    {
                        if (spaServiceOffer.Id == 0)
                        {
                            ModelState.AddModelError("OfferImage", Resources.Rqd_OfferImage);
                            ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                            return View("AddEditSpaServiceOffer", spaServiceOffer);
                        }

                    }
                    if (spaServiceOffer.Id == 0)
                    {
                        var result = _hotelService.CreateSpaServiceOffer(spaServiceOffer);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.SpaServiceDetailId, ImagePathFor.SpaServiceDetailOfferImage, uploadImage, api_path, result.Id);
                        }
                        return RedirectToAction("ManageSpaServiceOffers");
                    }
                    else
                    {
                        var res = _hotelService.ModifySpaServiceOffer(spaServiceOffer);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.SpaServiceDetailId, ImagePathFor.SpaServiceDetailOfferImage, uploadImage, api_path, res.Id);
                        }
                        return RedirectToAction("ManageSpaServiceOffers");
                    }
                }
                else
                {
                    ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                    return View("AddEditSpaServiceOffer", spaServiceOffer);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                return View("AddEditSpaServiceOffer", spaServiceOffer);
            }
        }

        /// <summary>
        /// Edits the spa service offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaServiceOffer.</returns>
        public ActionResult EditSpaServiceOffer(int id)
        {
            var hotelId = GetActiveHotelId();
            var spaServiceOffer = _hotelService.GetDiscountedSpaServiceOfferById(id);
            ViewBag.UnassignedOfferSpaServices = _hotelService.GetHotelUnassignedOfferSpaServiceDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
            return View("AddEditSpaServiceOffer", spaServiceOffer);
        }
        /// <summary>
        /// Deletes the spa service offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaServiceOffer.</returns>
        [HttpPost]
        public ActionResult DeleteSpaServiceOffer(int id)
        {
            try
            {
                var spaServiceOffer = _hotelService.GetDiscountedSpaServiceOfferById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                string imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + spaServiceOffer.SpaServiceDetailId + @"\OfferImage\" + spaServiceOffer.Id + @"\" + spaServiceOffer.OfferImage;

                _hotelService.DeleteSpaServiceOffer(id);
                if (!string.IsNullOrWhiteSpace(spaServiceOffer.OfferImage))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa service offer already in use." });
            }
        }
        #endregion Spa Service Details Offer

        #region Spa Ingredient Categories

        /// <summary>
        /// Manages the spa ingredient categories.
        /// </summary>
        /// <returns>ActionResult Manage SpaIngredientCategories.</returns>
        public ActionResult ManageSpaIngredientCategories()
        {
            return View();
        }
        /// <summary>
        /// Loads the spa ingredient categories.
        /// </summary>
        /// <returns>ActionResult Load SpaIngredientCategories.</returns>
        [HttpPost]
        public ActionResult LoadSpaIngredientCategories()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaIngredientCategories = _hotelService.GetAllSpaIngredientCategories(skip, pageSize, out Total);
            var data = spaIngredientCategories.Select(b => new
            {
                SpaIngredientCatId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.SpaIngredientCategories, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the spa ingredient category.
        /// </summary>
        /// <returns>ActionResult Add SpaIngredientCategory.</returns>
        public ActionResult AddSpaIngredientCategory()
        {
            SpaIngredientCategory spaIngredientCat = new SpaIngredientCategory();
            return View("AddEditSpaIngredientCategory", spaIngredientCat);
        }

        /// <summary>
        /// Saves the spa ingredient category.
        /// </summary>
        /// <param name="spaIngredientCategory">The spa ingredient category.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save SpaIngredientCategory.</returns>
        public ActionResult SaveSpaIngredientCategory(SpaIngredientCategory spaIngredientCategory, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                spaIngredientCategory.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditSpaIngredientCategory", spaIngredientCategory);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditSpaIngredientCategory", spaIngredientCategory);
                        }
                    }
                    if (spaIngredientCategory.Id == 0)
                    {
                        var result = _hotelService.CreateSpaIngredientCategory(spaIngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.SpaIngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageSpaIngredientCategories");
                    }
                    else
                    {
                        var res = _hotelService.ModifySpaIngredientCategory(spaIngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.SpaIngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageSpaIngredientCategories");
                    }
                }
                else
                {
                    return View("AddEditSpaIngredientCategory", spaIngredientCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaIngredientCategory", spaIngredientCategory);
            }
        }

        /// <summary>
        /// Edits the spa ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaIngredientCategory.</returns>
        public ActionResult EditSpaIngredientCategory(int id)
        {
            var spaIngredientCategory = _hotelService.GetSpaIngredientCategoryById(id);
            return View("AddEditSpaIngredientCategory", spaIngredientCategory);
        }
        /// <summary>
        /// Deletes the spa ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaIngredientCategory.</returns>
        [HttpPost]
        public ActionResult DeleteSpaIngredientCategory(int id)
        {
            try
            {
                _hotelService.DeleteSpaIngredientCategory(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Spa ingredient category cannot be deleted because it is already in use." });
            }
        }
        #endregion Spa Ingredient Categories

        #region Spa Ingredients

        /// <summary>
        /// Manages the spa ingredients.
        /// </summary>
        /// <param name="spaIngredientCatId">The spa ingredient cat identifier.</param>
        /// <returns>ActionResult Manage SpaIngredients.</returns>
        public ActionResult ManageSpaIngredients(int spaIngredientCatId)
        {
            ViewBag.SpaIngredientCategoryId = spaIngredientCatId;
            return View();
        }

        /// <summary>
        /// Loads the spa ingredients.
        /// </summary>
        /// <param name="spaIngredientCatId">The spa ingredient cat identifier.</param>
        /// <returns>ActionResult Load SpaIngredients.</returns>
        [HttpPost]
        public ActionResult LoadSpaIngredients(int spaIngredientCatId)
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaIngredient = _hotelService.GetSpaIngredientBySpaIngCatId(spaIngredientCatId, skip, pageSize, out Total);

            var data = spaIngredient.Select(b => new
            {
                Name = b.Name,
                SpaIngredientCategory = b.SpaIngredientCategory.Name,
                Description = b.Description,
                IngredientImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.SpaIngredientImage, b.IngredientImage, b.Id, api_path),
                SpaIngredientId = b.Id
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the spa ingredient.
        /// </summary>
        /// <param name="spaIngredientCatId">The spa ingredient cat identifier.</param>
        /// <returns>ActionResult Add SpaIngredient.</returns>
        public ActionResult AddSpaIngredient(int spaIngredientCatId)
        {
            SpaIngredient spaIngredient = new SpaIngredient();
            spaIngredient.SpaIngredientCategoryId = spaIngredientCatId;
            return View("AddEditSpaIngredient", spaIngredient);
        }

        /// <summary>
        /// Saves the spa ingredient.
        /// </summary>
        /// <param name="spaIngredient">The spa ingredient.</param>
        /// <param name="uploadSpaIngredientImage">The upload spa ingredient image.</param>
        /// <returns>ActionResult Save SpaIngredient.</returns>
        public ActionResult SaveSpaIngredient(SpaIngredient spaIngredient, HttpPostedFileBase uploadSpaIngredientImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var spaIngrImageFilename = spaIngredient.IngredientImage;
                    if (uploadSpaIngredientImage != null)
                    {
                        if (uploadSpaIngredientImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", "jpeg", ".png" };
                            var filename = Path.GetFileName(uploadSpaIngredientImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadSpaIngredientImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                spaIngrImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditSpaIngredient", spaIngredient);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditSpaIngredient", spaIngredient);
                        }
                    }


                    if (spaIngredient.Id == 0)
                    {
                        spaIngredient.IngredientImage = spaIngrImageFilename;
                        var addedSpaIngredinet = _hotelService.CreateSpaIngredient(spaIngredient);
                        if (uploadSpaIngredientImage != null)
                        {
                            ImageHelper.UploadImage(addedSpaIngredinet.Id, ImagePathFor.SpaIngredientImage, uploadSpaIngredientImage, api_path);
                        }

                        return RedirectToAction("ManageSpaIngredients", new { spaIngredientCatId = spaIngredient.SpaIngredientCategoryId });
                    }
                    else
                    {
                        if (uploadSpaIngredientImage != null)
                        {
                            ImageHelper.UploadImage(spaIngredient.Id, ImagePathFor.SpaIngredientImage, uploadSpaIngredientImage, api_path);
                        }
                        spaIngredient.IngredientImage = spaIngrImageFilename;
                        var modifiedIngredient = _hotelService.ModifySpaIngredient(spaIngredient);
                        return RedirectToAction("ManageSpaIngredients", new { spaIngredientCatId = spaIngredient.SpaIngredientCategoryId });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    return View("AddEditSpaIngredient", spaIngredient);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaIngredient", spaIngredient);
            }
        }

        /// <summary>
        /// Edits the spa ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaIngredient.</returns>
        public ActionResult EditSpaIngredient(int id)
        {
            var spaIngredient = _hotelService.GetSpaIngredientById(id);
            return View("AddEditSpaIngredient", spaIngredient);
        }

        /// <summary>
        /// Deletes the spa ingredient.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult Delete SpaIngredient.</returns>
        public ActionResult DeleteSpaIngredient(int Id)
        {
            try
            {
                _hotelService.DeleteSpaIngredient(Id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Spa ingredient cannot be deleted because it is already in use." });
            }
        }
        #endregion Spa Ingredients

        #region Spa Service Detail Suggestion

        /// <summary>
        /// Manages the spa suggestions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaSuggestions.</returns>
        public ActionResult ManageSpaSuggestions(int id)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            ViewBag.SpaServiceDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa suggestions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult LoadSpaSuggestions.</returns>
        [HttpPost]
        public ActionResult LoadSpaSuggestions(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaServDetSuggestion = _hotelService.GetSpaServiceDetSuggestions(id, skip, pageSize, out Total);
            var data = spaServDetSuggestion.Select(b => new
            {
                SpaSuggestionId = b.Id,
                SpaServiceName = b.SpaServiceDetail.SpaService.Name,
                SpaServiceDetName = b.SpaServiceDetail.Name,
                SpaSuggestionName = b.SpaServiceDetail1.Name,
                SpaSuggestionDescription = b.SpaServiceDetail1.Description,
                SpaSuggestionPrice = b.SpaServiceDetail1.Price,
                IsActive = b.SpaServiceDetail1.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa suggestion.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <returns>ActionResult Add SpaSuggestion.</returns>
        public ActionResult AddSpaSuggestion(int spaServiceDetId)
        {
            SpaSuggestion spaServiceDetSuggestion = new SpaSuggestion();
            spaServiceDetSuggestion.SpaServiceDetailId = spaServiceDetId;
            var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetSuggetions(spaServiceDetId, GetActiveHotelId()).Select(
                r => new
                {
                    SpaServiceDetSuggestionId = r.Id,
                    Name = r.Name
                }).ToList();
            ViewBag.SpaServiceDetSuggestions = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetSuggestionId", "Name");
            return View(spaServiceDetSuggestion);
        }

        /// <summary>
        /// Saves the spa service det suggestion.
        /// </summary>
        /// <param name="spaServiceDetSuggestion">The spa service det suggestion.</param>
        /// <returns>ActionResult Save SpaServiceDetSuggestion.</returns>
        [HttpPost]
        public ActionResult SaveSpaServiceDetSuggestion(SpaSuggestion spaServiceDetSuggestion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<SpaSuggestion> spaSuggestionList = new List<SpaSuggestion>();

                    var SpaServDetSuggestionIds = Request.Form["SpaServiceDetSuggestionId"];

                    if (SpaServDetSuggestionIds != null)
                    {
                        string[] spaServDetSuggIds = SpaServDetSuggestionIds.Split(',');
                        foreach (var eachSpaServDetId in spaServDetSuggIds)
                        {
                            SpaSuggestion spaServDetSuggestion = new SpaSuggestion()
                            {
                                SpaServiceDetailId = spaServiceDetSuggestion.SpaServiceDetailId,
                                SpaServiceDetailSuggestionId = Convert.ToInt32(eachSpaServDetId)
                            };
                            spaSuggestionList.Add(spaServDetSuggestion);
                        }
                    }

                    if (spaServiceDetSuggestion.Id == 0)
                        _hotelService.CreateSpaServiceDetailSuggestion(spaSuggestionList);
                    return RedirectToAction("ManageSpaSuggestions", "HotelSpaService", new { area = "admin", id = spaServiceDetSuggestion.SpaServiceDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetSuggetions(spaServiceDetSuggestion.SpaServiceDetailId, GetActiveHotelId()).Select(
                        r => new
                        {
                            SpaServiceDetSuggestionId = r.Id,
                            Name = r.Name
                        }).ToList();
                    ViewBag.SpaServiceDetSuggestions = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetSuggestionId", "Name");

                    return View("AddSpaSuggestion", spaServiceDetSuggestion);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignSpaServiceDet = _hotelService.GetUnassignSpaServiceDetSuggetions(spaServiceDetSuggestion.SpaServiceDetailId, GetActiveHotelId()).Select(
                    r => new
                    {
                        SpaServiceDetSuggestionId = r.Id,
                        Name = r.Name
                    }).ToList();
                ViewBag.SpaServiceDetSuggestions = new MultiSelectList(unassignSpaServiceDet, "SpaServiceDetSuggestionId", "Name");

                return View("AddSpaSuggestion", spaServiceDetSuggestion);
            }
        }

        /// <summary>
        /// Deletes the spa suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaSuggestion.</returns>
        [HttpPost]
        public ActionResult DeleteSpaSuggestion(int id)
        {
            try
            {
                _hotelService.DeleteSpaServiceDetSuggestion(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa suggestion cannot be deleted because it is already in use." });
            }
        }

        #endregion Spa Service Detail Suggestion

        #region Hotel Spa Service Detail Ingredients

        /// <summary>
        /// Manages the spa service detail ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaServiceDetail Ingredients.</returns>
        public ActionResult ManageSpaServiceDetailIngredients(int id)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            ViewBag.SpaServiceDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa service detail ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaServiceDetail Ingredients.</returns>
        [HttpPost]
        public ActionResult LoadSpaServiceDetailIngredients(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaSerDetIngredients = _hotelService.GetSpaServiceDetailIngredients(id, skip, pageSize, out Total);
            var data = spaSerDetIngredients.Select(b => new
            {
                SpaServDetIngredientId = b.Id,
                SpaServiceDetailName = b.SpaServiceDetail.Name,
                SpaIngredientName = b.SpaIngredient.Name,
                SpaIngredientDesc = b.SpaIngredient.Description,
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the spa service detail ingredient.
        /// </summary>
        /// <param name="spaServDetailId">The spa serv detail identifier.</param>
        /// <returns>ActionResult Add SpaServiceDetail Ingredient.</returns>
        public ActionResult AddSpaServiceDetailIngredient(int spaServDetailId)
        {
            SpaServiceDetailIngredient spaSerDetIngr = new SpaServiceDetailIngredient();
            spaSerDetIngr.SpaServiceDetailId = spaServDetailId;

            var unassignSpaIngredients = _hotelService.GetUnassignSpaIngrdeients(spaServDetailId).Select(
                 r => new
                 {
                     SpaIngredientId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.SpaIngredients = new MultiSelectList(unassignSpaIngredients, "SpaIngredientId", "Name");
            return View(spaSerDetIngr);
        }


        /// <summary>
        /// Saves the spa service detail ingredient.
        /// </summary>
        /// <param name="spaSerDetIngredient">The spa ser det ingredient.</param>
        /// <returns>ActionResult Save SpaServiceDetail Ingredient.</returns>
        [HttpPost]
        public ActionResult SaveSpaServiceDetailIngredient(SpaServiceDetailIngredient spaSerDetIngredient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<SpaServiceDetailIngredient> spaServiceDetIngredients = new List<SpaServiceDetailIngredient>();

                    var SpaIngredientIds = Request.Form["SpaIngredientId"];

                    if (SpaIngredientIds != null)
                    {
                        string[] spaIngrIds = SpaIngredientIds.Split(',');
                        foreach (var eachSpaIngrId in spaIngrIds)
                        {
                            SpaServiceDetailIngredient spaSerDetIng = new SpaServiceDetailIngredient()
                            {
                                SpaServiceDetailId = spaSerDetIngredient.SpaServiceDetailId,
                                SpaIngredientsId = Convert.ToInt32(eachSpaIngrId)
                            };
                            spaServiceDetIngredients.Add(spaSerDetIng);
                        }
                    }
                    else
                    {
                        var unassignSpaIngredients = _hotelService.GetUnassignSpaIngrdeients(spaSerDetIngredient.SpaServiceDetailId).Select(
                             r => new
                             {
                                 SpaIngredientId = r.Id,
                                 Name = r.Name
                             }).ToList();
                        ViewBag.SpaIngredients = new MultiSelectList(unassignSpaIngredients, "SpaIngredientId", "Name");
                        ModelState.AddModelError("", Resources.Rqd_SpaServiceDetailIngredient);
                        return View("AddSpaServiceDetailIngredient", spaSerDetIngredient);
                    }

                    if (spaSerDetIngredient.Id == 0)
                        _hotelService.CreateSpaServiceDetailIngredient(spaServiceDetIngredients);

                    return RedirectToAction("ManageSpaServiceDetailIngredients", "HotelSpaService", new { area = "admin", id = spaSerDetIngredient.SpaServiceDetailId });
                }
                else
                {
                    var unassignSpaIngredients = _hotelService.GetUnassignSpaIngrdeients(spaSerDetIngredient.SpaServiceDetailId).Select(
                         r => new
                         {
                             SpaIngredientId = r.Id,
                             Name = r.Name
                         }).ToList();
                    ViewBag.SpaIngredients = new MultiSelectList(unassignSpaIngredients, "SpaIngredientId", "Name");
                    return View("AddSpaServiceDetailIngredient", spaSerDetIngredient);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignSpaIngredients = _hotelService.GetUnassignSpaIngrdeients(spaSerDetIngredient.SpaServiceDetailId).Select(
                     r => new
                     {
                         SpaIngredientId = r.Id,
                         Name = r.Name
                     }).ToList();
                ViewBag.SpaIngredients = new MultiSelectList(unassignSpaIngredients, "SpaIngredientId", "Name");
                return View("AddSpaServiceDetailIngredient", spaSerDetIngredient);
            }
        }

        /// <summary>
        /// Deletes the spa service detail ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaServiceDetail Ingredient.</returns>
        [HttpPost]
        public ActionResult DeleteSpaServiceDetailIngredient(int id)
        {
            try
            {
                _hotelService.DeleteSpaServiceDetailIngredient(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa service detail ingredients already in use." });
            }
        }
        #endregion Hotel Spa Service Detail Ingredients

        #region Spa Service Detail ExtraTime

        /// <summary>
        /// Manages the spa details extra time.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaDetails ExtraTime.</returns>
        public ActionResult ManageSpaDetailsExtraTime(int id)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            ViewBag.SpaServiceDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa details extra time.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaDetails ExtraTime.</returns>
        [HttpPost]
        public ActionResult LoadSpaDetailsExtraTime(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaServDetExtratimes = _hotelService.GetSpaServiceDetExtratimes(id, skip, pageSize, out Total);
            var data = spaServDetExtratimes.Select(b => new
            {
                SpaDetailExtraTimeId = b.Id,
                Name = b.Name,
                Duration = b.Duration,
                Price = b.Price,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the edit spa detail extratimes.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <returns>ActionResult AddEdit SpaDetail Extratimes.</returns>
        public ActionResult AddEditSpaDetailExtratimes(int spaServiceDetId)
        {
            ExtraTime time = new ExtraTime()
            {
                SpaServiceDetailId = spaServiceDetId
            };
            return View(time);
        }

        /// <summary>
        /// Saves the spa details extratime.
        /// </summary>
        /// <param name="extraTime">The extra time.</param>
        /// <returns>ActionResult Save SpaDetails Extratime.</returns>
        [HttpPost]
        public ActionResult SaveSpaDetailsExtratime(ExtraTime extraTime)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (extraTime.Id == 0)
                    {
                        _hotelService.CreateSpaServiceDetailExtratime(extraTime);
                        return RedirectToAction("ManageSpaDetailsExtraTime", "HotelSpaService", new { area = "admin", id = extraTime.SpaServiceDetailId });
                    }
                    else
                    {
                        _hotelService.ModifySpaServiceDetailExtratime(extraTime);
                        return RedirectToAction("ManageSpaDetailsExtraTime", "HotelSpaService", new { area = "admin", id = extraTime.SpaServiceDetailId });
                    }
                }
                else
                {
                    return View("AddEditSpaDetailExtratimes", extraTime);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaDetailExtratimes", extraTime);
            }
        }

        /// <summary>
        /// Edits the spa detail extratime.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaDetail Extratime.</returns>
        public ActionResult EditSpaDetailExtratime(int id)
        {
            var extraTime = _hotelService.GetSpaDetailExtratimeById(id);
            return View("AddEditSpaDetailExtratimes", extraTime);
        }

        /// <summary>
        /// Deletes the spa detail extratime.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaDetail Extratime.</returns>
        [HttpPost]
        public ActionResult DeleteSpaDetailExtratime(int id)
        {
            try
            {
                _hotelService.DeleteExtraTime(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa detail extratime cannot be deleted because it is already in use." });
            }
        }

        #endregion Spa Service Detail ExtraTime

        #region Spa Service Detail ExtraProcedure

        /// <summary>
        /// Manages the spa details extra procedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage SpaDetails ExtraProcedure.</returns>
        public ActionResult ManageSpaDetailsExtraProcedure(int id)
        {
            var SpaServiceDetail = _hotelService.GetSpaServiceDetailById(id);
            ViewBag.SpaServiceId = SpaServiceDetail.SpaServiceId;
            ViewBag.HotelServiceId = SpaServiceDetail.SpaService.HotelServiceId;
            ViewBag.SpaServiceDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the spa details extra procedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load SpaDetails ExtraProcedure.</returns>
        [HttpPost]
        public ActionResult LoadSpaDetailsExtraProcedure(int id)
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaServDetExtraProcedures = _hotelService.GetSpaServiceDetExtraprocedure(id, skip, pageSize, out Total);
            var data = spaServDetExtraProcedures.Select(b => new
            {
                SpaDetailExtraProcedureId = b.Id,
                Name = b.Name,
                Duration = b.Duration,
                Price = b.Price,
                IsActive = b.IsActive,
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.SpaExtraProcedureImage, b.Image, b.SpaServiceDetailId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the edit spa detail extraprocedure.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <returns>ActionResult Add Edit SpaDetail Extraprocedure.</returns>
        public ActionResult AddEditSpaDetailExtraprocedure(int spaServiceDetId)
        {
            ExtraProcedure procedure = new ExtraProcedure()
            {
                SpaServiceDetailId = spaServiceDetId
            };
            return View(procedure);
        }

        /// <summary>
        /// Saves the spa details extraprocedure.
        /// </summary>
        /// <param name="extraProcedure">The extra procedure.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save SpaDetails Extraprocedure.</returns>
        [HttpPost]
        public ActionResult SaveSpaDetailsExtraprocedure(ExtraProcedure extraProcedure, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        var filename = Path.GetFileName(uploadImage.FileName);
                        filename = filename.Replace(" ", "_");
                        extraProcedure.Image = filename;
                    }
                    if (extraProcedure.Id == 0)
                    {
                        var result = _hotelService.CreateSpaServiceDetailExtraprocedure(extraProcedure);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.SpaServiceDetailId, ImagePathFor.SpaExtraProcedureImage, uploadImage, api_path, result.Id);
                        }
                        return RedirectToAction("ManageSpaDetailsExtraProcedure", "HotelSpaService", new { area = "admin", id = extraProcedure.SpaServiceDetailId });
                    }
                    else
                    {
                        var res = _hotelService.ModifySpaServiceDetailExtraprocedure(extraProcedure);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.SpaServiceDetailId, ImagePathFor.SpaExtraProcedureImage, uploadImage, api_path, res.Id);
                        }
                        return RedirectToAction("ManageSpaDetailsExtraProcedure", "HotelSpaService", new { area = "admin", id = extraProcedure.SpaServiceDetailId });
                    }
                }
                else
                {
                    return View("AddEditSpaDetailExtraprocedure", extraProcedure);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditSpaDetailExtraprocedure", extraProcedure);
            }
        }

        /// <summary>
        /// Edits the spa detail extraprocedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit SpaDetail Extraprocedure.</returns>
        public ActionResult EditSpaDetailExtraprocedure(int id)
        {
            var extraTime = _hotelService.GetSpaDetailExtraprocedureById(id);
            return View("AddEditSpaDetailExtraprocedure", extraTime);
        }

        /// <summary>
        /// Deletes the spa detail extra procedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete SpaDetail ExtraProcedure.</returns>
        [HttpPost]
        public ActionResult DeleteSpaDetailExtraProcedure(int id)
        {
            try
            {
                _hotelService.DeleteExtraProcedure(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This spa detail extraprocedure cannot be deleted because it is already in use." });
            }
        }

        #endregion Spa Service Detail ExtraProcedure

        #region Spa Employee Dashboard

        /// <summary>
        /// Hotels the spa employee dashboard.
        /// </summary>
        /// <returns>ActionResult Hotel SpaEmployee Dashboard.</returns>
        public ActionResult HotelSpaEmployeeDashboard()
        {
            return View();
        }
        #endregion Spa Employee Dashboard

        #region Spa Orders History

        /// <summary>
        /// Spas the orders history.
        /// </summary>
        /// <returns>ActionResult SpaOrders History.</returns>
        public ActionResult SpaOrdersHistory()
        {
            return View();
        }
        /// <summary>
        /// Loads the spa orders history.
        /// </summary>
        /// <returns>ActionResult Load SpaOrdersHistory.</returns>
        [HttpPost]
        public ActionResult LoadSpaOrdersHistory()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaOrdersHistory = _hotelService.GetSpaOrderHistory(fromDate, toDate, hotelId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = spaOrdersHistory.Select(b => new
            {
                SpaOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((SpaOrderStatus)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString(),
                TipsAmount = b.SpaTips.Select(i => i.TipAmount).FirstOrDefault(),
                TipsPaid = b.SpaTips.Select(i => i.PaymentStatus).FirstOrDefault(),
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the spa order item details.
        /// </summary>
        /// <param name="spaOrderId">The spa order identifier.</param>
        /// <returns>ActionResult Load SpaOrderItemDetails.</returns>
        public ActionResult LoadSpaOrderItemDetails(int spaOrderId)
        {
            var spaOrderDetails = _hotelService.GetSpaOrderDetails(spaOrderId);
            return PartialView("_SpaOrderItemDetails", spaOrderDetails);
        }

        #endregion Spa Orders History
    }
}