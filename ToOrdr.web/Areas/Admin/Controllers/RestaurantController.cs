﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-08-2019
// ***********************************************************************
// <copyright file="RestaurantController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Core.Util;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;
using ToOrdr.Web.ViewModels;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class RestaurantController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class RestaurantController : BaseController
    {
        /// <summary>
        /// The restaurant service
        /// </summary>
        IRestaurantService _restaurantService;
        /// <summary>
        /// The customer service
        /// </summary>
        ICustomerService _customerService;
        /// <summary>
        /// The payment service
        /// </summary>
        IPaymentService _paymentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantController"/> class.
        /// </summary>
        /// <param name="restaurantService">The restaurant service.</param>
        /// <param name="customerService">The customer service.</param>
        /// <param name="paymentService">The payment service.</param>
        public RestaurantController(IRestaurantService restaurantService,
                                    ICustomerService customerService,
                                    IPaymentService paymentService)
        {
            _restaurantService = restaurantService;
            _customerService = customerService;
            _paymentService = paymentService;
        }

        // GET: Admin/Restaurant
        /// <summary>
        /// Dashboards this instance.
        /// </summary>
        /// <returns>ActionResult restaurant Dashboard.</returns>
        public ActionResult Dashboard()
        {
            return View();
        }


        #region Restaurant Cuisines

        /// <summary>
        /// Manages the restaurant cuisines.
        /// </summary>
        /// <returns>ActionResult Manage Restaurant Cuisines.</returns>
        public ActionResult ManageRestaurantCuisines()
        {
            return View();
        }
        /// <summary>
        /// Loads the restaurant cuisines.
        /// </summary>
        /// <returns>ActionResult Load RestaurantCuisines.</returns>
        public ActionResult LoadRestaurantCuisines()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restCuisines = _restaurantService.GetAllRestaurantCuisines(restaurantId, skip, pageSize, out Total);
            var data = restCuisines.Select(b => new
            {
                RestCuisineId = b.Id,
                CuisineName = b.Cuisine.Name,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                CuisineImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.RestaurantCuisines, b.Image, b.RestaurantId, api_path, b.Id)

            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the restaurant cuisine.
        /// </summary>
        /// <returns>ActionResult Add RestaurantCuisine.</returns>
        public ActionResult AddRestaurantCuisine()
        {
            var restId = GetActiveRestaurantId();
            RestaurantCuisine restCuisine = new RestaurantCuisine();
            restCuisine.RestaurantId = restId;
            ViewBag.UnassignedRestCuisines = _restaurantService.GetAllRestUnassignedCuisines(restId).ToList();
            return View(restCuisine);
        }

        /// <summary>
        /// Saves the restaurant cuisine.
        /// </summary>
        /// <param name="restCuisine">The rest cuisine.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save RestaurantCuisine.</returns>
        public ActionResult SaveRestaurantCuisine(RestaurantCuisine restCuisine, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                restCuisine.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddRestaurantCuisine", restCuisine);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddRestaurantCuisine", restCuisine);
                        }
                    }
                    if (restCuisine.Id == 0)
                    {
                        var result = _restaurantService.CreateRestaurantCuisine(restCuisine);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.RestaurantId, ImagePathFor.RestaurantCuisines, uploadImage, api_path, result.Id);
                        }
                        return RedirectToAction("ManageRestaurantCuisines");
                    }
                    else
                    {
                        return View("AddRestaurantCuisine", restCuisine);
                    }
                }
                else
                {
                    return View("AddRestaurantCuisine", restCuisine);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddRestaurantCuisine", restCuisine);
            }
        }

        /// <summary>
        /// Deletes the restaurant cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantCuisine.</returns>
        public ActionResult DeleteRestaurantCuisine(int id)
        {
            try
            {
                _restaurantService.DeleteRestaurantCuisine(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This restaurant cuisine is already in use." });
            }
        }

        /// <summary>
        /// Updates the restaurant cuisine status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update RestaurantCuisine Status.</returns>
        public ActionResult UpdateRestaurantCuisineStatus(int id)
        {
            try
            {
                var restCuisine = _restaurantService.GetRestaurantCuisineById(id);
                if (restCuisine.IsActive)
                    restCuisine.IsActive = false;
                else
                    restCuisine.IsActive = true;
                _restaurantService.ModifyRestCuisine(restCuisine);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This cuisine status cannot be updated" });
            }

        }
        #endregion Restaurant Cuisines

        #region Menu Item Offers

        /// <summary>
        /// Manages the menu item offers.
        /// </summary>
        /// <returns>ActionResult Manage MenuItemOffers.</returns>
        public ActionResult ManageMenuItemOffers()
        {
            return View();
        }

        /// <summary>
        /// Loads the menu items offer.
        /// </summary>
        /// <returns>ActionResult Load MenuItemsOffer.</returns>
        [HttpPost]
        public ActionResult LoadMenuItemsOffer()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var menuItemOffers = _restaurantService.GetOffersMenuItemsOfRest(GetActiveRestaurantId(), skip, pageSize, out Total);
            var data = menuItemOffers.Select(b => new
            {
                MenuItemOfferId = b.Id,
                MenuItemName = b.RestaurantMenuItem.ItemName,
                OfferDescription = b.Description,
                OfferPercentage = b.Percentage,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the menu item offer.
        /// </summary>
        /// <returns>ActionResult Add MenuItemOffer.</returns>
        public ActionResult AddMenuItemOffer()
        {
            var restaurantId = GetActiveRestaurantId();
            MenuItemOffer menuItemOffer = new MenuItemOffer();
            ViewBag.UnassignedOfferMenuItems = _restaurantService.GetUnassignedOfferMenuItemsOfRest(restaurantId).Select(s => new { Id = s.Id, Name = s.ItemName }).ToList();
            return View("AddEditMenuItemOffer", menuItemOffer);
        }

        /// <summary>
        /// Saves the menu item offer.
        /// </summary>
        /// <param name="menuItemOffer">The menu item offer.</param>
        /// <returns>ActionResult Save MenuItemOffer.</returns>
        public ActionResult SaveMenuItemOffer(MenuItemOffer menuItemOffer)
        {
            try
            {
                if (menuItemOffer.Id > 0)
                    ModelState["RestaurantMenuItem.Price"].Errors.Clear();

                if (ModelState.IsValid)
                {
                    if (menuItemOffer.Id == 0)
                    {
                        _restaurantService.CreateMenuItemOffer(menuItemOffer);
                        return RedirectToAction("ManageMenuItemOffers");
                    }
                    else
                    {
                        _restaurantService.ModifyMenuItemOffer(menuItemOffer);
                        return RedirectToAction("ManageMenuItemOffers");
                    }
                }
                else
                {
                    ViewBag.UnassignedOfferMenuItems = _restaurantService.GetUnassignedOfferMenuItemsOfRest(GetActiveRestaurantId()).Select(s => new { Id = s.Id, Name = s.ItemName }).ToList();
                    return View("AddEditMenuItemOffer", menuItemOffer);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.UnassignedOfferMenuItems = _restaurantService.GetUnassignedOfferMenuItemsOfRest(GetActiveRestaurantId()).Select(s => new { Id = s.Id, Name = s.ItemName }).ToList();
                return View("AddEditMenuItemOffer", menuItemOffer);
            }
        }

        /// <summary>
        /// Edits the menu item offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit MenuItemOffer.</returns>
        public ActionResult EditMenuItemOffer(int id)
        {
            var restaurantId = GetActiveRestaurantId();
            var menuItemOffer = _restaurantService.GetDiscountedMenuItemById(id);
            ViewBag.UnassignedOfferMenuItems = _restaurantService.GetUnassignedOfferMenuItemsOfRest(restaurantId).Select(s => new { Id = s.Id, Name = s.ItemName }).ToList();
            return View("AddEditMenuItemOffer", menuItemOffer);
        }
        /// <summary>
        /// Deletes the menu item offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete MenuItemOffer.</returns>
        [HttpPost]
        public ActionResult DeleteMenuItemOffer(int id)
        {
            try
            {
                _restaurantService.DeleteMenuItemOffer(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This menu item offer already in use." });
            }
        }
        #endregion Menu Item Offers

        /// <summary>
        /// Restaurants the reviews.
        /// </summary>
        /// <returns>ActionResult RestaurantReviews.</returns>
        public ActionResult RestaurantReviews()
        {
            return View();
        }

        /// <summary>
        /// Restaurants the order reviews.
        /// </summary>
        /// <returns>ActionResult RestaurantOrder Reviews.</returns>
        public ActionResult RestaurantOrderReviews()
        {
            return View();
        }
        /// <summary>
        /// Loads the rest order reviews.
        /// </summary>
        /// <returns>ActionResult Load RestOrderReviews.</returns>
        public ActionResult LoadRestOrderReviews()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            var reviews = _restaurantService.GetRestaurantOrderReviews(fromDate, toDate, restaurantId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                CustomerName = b.RestaurantOrder.Customer.FirstName + " " + b.RestaurantOrder.Customer.LastName,
                OrderNumber = "#" + b.RestaurantOrderId,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Restaurants the menu item reviews.
        /// </summary>
        /// <returns>ActionResult RestaurantMenuItem Reviews.</returns>
        public ActionResult RestaurantMenuItemReviews()
        {
            return View();
        }

        /// <summary>
        /// Loads the rest menu item reviews.
        /// </summary>
        /// <returns>ActionResult Load RestMenuItemReviews.</returns>
        public ActionResult LoadRestMenuItemReviews()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            var reviews = _restaurantService.GetRestaurantMenuItemReviews(fromDate, toDate, restaurantId, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                MenuItemName = b.RestaurantMenuItem.ItemName,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        //Login restaurant order queue
        /// <summary>
        /// Restaurants the orders queue.
        /// </summary>
        /// <returns>ActionResult RestaurantOrders Queue.</returns>
        public ActionResult RestaurantOrdersQueue()
        {
            return View();
        }

        /// <summary>
        /// Loads the restaurant order queue.
        /// </summary>
        /// <returns>ActionResult Load RestaurantOrderQueue.</returns>
        [HttpPost]
        public ActionResult LoadRestaurantOrderQueue()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var statusesToShow = from Status_Type e in Enum.GetValues(typeof(Status_Type)) select new { Id = (int)e, Name = e.ToString() };
            statusesToShow = statusesToShow.Where(s => s.Id < 6);
            var restOrderQueue = _restaurantService.GetRestOrdersQueue(restaurantId, skip, pageSize, out Total);
            var data = restOrderQueue.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.ScheduledDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                HotelName = b.RoomId != null ? b.Room.Hotel.Name : null,
                RoomNumber = b.RoomId != null ? b.Room.Number : null,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                RestOrderStatuses = (b.PaymentStatus == true) ? b.OrderStatus == 1 ? statusesToShow.Where(s => s.Id >= b.OrderStatus && s.Id <= 4) : statusesToShow.Where(s => s.Id >= b.OrderStatus) : statusesToShow.Where(s => s.Id == 0),
                PaymentStatus = b.PaymentStatus.ToString(),
                NextServingStatus = b.NextServingStatus
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates the order next serving status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update Order NextServing Status.</returns>
        [HttpPost]
        public ActionResult UpdateOrderNextServingStatus(int id)
        {
            try
            {
                _restaurantService.UpdateRestOrderNextServingStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Updates the rest order status.
        /// </summary>
        /// <param name="RestOrderId">The rest order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Update Restaurant OrderStatus.</returns>
        [HttpPost]
        public ActionResult UpdateRestOrderStatus(int RestOrderId, int orderStatus)
        {
            try
            {
                var restOrder = _restaurantService.GetOrdersDetailsById(RestOrderId);
                if (restOrder.OrderStatus == 0 && orderStatus == 1)
                {
                    //if (restOrder.CardId != null)
                    //{
                    var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                    var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];

                    //capture payment from Quickpay
                    var captureStatus = _paymentService.CaptureCardPayment(quickpayToken, quickpayUrl, restOrder.PaymentId, restOrder.OrderTotal);
                    if (captureStatus)
                    {
                        restOrder.OrderStatus = orderStatus;
                        _restaurantService.ModifyOrders(restOrder);
                        return Json(new { status = true, message = "Order payment captured successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, message = "Order payment capture Failed!" });
                    }
                    //}
                    //else
                    //{
                    //    var token = ConfigurationManager.AppSettings["PaypalAuthorizationKey"];
                    //    var baseAddress = ConfigurationManager.AppSettings["PaypalBaseURL"];

                    //    //capture payment from paypal
                    //    var paypalCaptureStatus = _paymentService.CapturePaypalPayment(restOrder, baseAddress, token);
                    //    if (paypalCaptureStatus.ToString() == "completed")
                    //    {
                    //        restOrder.OrderStatus = orderStatus;
                    //        _restaurantService.ModifyOrders(restOrder);
                    //        return Json(new { status = true, message = "Order payment captured successfully" + paypalCaptureStatus });
                    //    }
                    //    else
                    //    {
                    //        return Json(new { status = false, message = "Order payment capture" + paypalCaptureStatus });
                    //    }
                    //}
                }

                if (restOrder.OrderStatus == 0 && (orderStatus == 2 || orderStatus==3 || orderStatus==4))
                {
                    return Json(new { status = false, message = "This order status cannot be changed to preparing, ready and delivered since it is not confirmed yet" });
                }

                if (restOrder.OrderStatus != orderStatus)
                {
                    restOrder.OrderStatus = orderStatus;
                    _restaurantService.ModifyOrders(restOrder);

                }
                return Json(new { status = true, message = "Status updated successfully" });
            }
            catch
            {
                return Json(new { status = false, message = "This order status cannot be Updated" });
            }
        }

        /// <summary>
        /// Rejecteds the rest order status.
        /// </summary>
        /// <param name="RestOrderId">The rest order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Rejected Restaurant OrderStatus.</returns>
        public ActionResult RejectedRestOrderStatus(int RestOrderId, int orderStatus)
        {
            RestOrderComment restOrderComment = new RestOrderComment
            {
                OrderId = RestOrderId,
                StatusId = orderStatus
            };
            return PartialView("_RejectedRestOrderStatusPartial", restOrderComment);
        }

        /// <summary>
        /// Updates the rejected order status.
        /// </summary>
        /// <param name="restOrderComment">The rest order comment.</param>
        /// <returns>ActionResult Update Rejected OrderStatus.</returns>
        public ActionResult UpdateRejectedOrderStatus(RestOrderComment restOrderComment)
        {
            var order = _restaurantService.GetOrdersDetailsById(restOrderComment.OrderId);
            order.OrderStatus = restOrderComment.StatusId;
            order.Comment = restOrderComment.Comment;
            _restaurantService.ModifyOrders(order);

            return RedirectToAction("RestaurantOrdersQueue", "Restaurant", new { area = "Admin" });
        }

        /// <summary>
        /// Restaurants the recent orders.
        /// </summary>
        /// <returns>ActionResult Restaurant RecentOrders.</returns>
        public ActionResult RestaurantRecentOrders()
        {
            return View();
        }

        /// <summary>
        /// Loads the restaurant recent orders.
        /// </summary>
        /// <returns>ActionResult Load Restaurant RecentOrders.</returns>
        [HttpPost]
        public ActionResult LoadRestaurantRecentOrders()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restRecentOrders = _restaurantService.GetRestRecentOrders(restaurantId, skip, pageSize, out Total);
            var data = restRecentOrders.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.ScheduledDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Todayses the orders.
        /// </summary>
        /// <returns>ActionResult Todays Orders.</returns>
        public ActionResult TodaysOrders()
        {
            var restaurantId = GetActiveRestaurantId();
            var orders = _restaurantService.GetTodaysOrders(restaurantId);
            return PartialView("TodaysOrdersPartial", orders);
        }

        /// <summary>
        /// Todayses the top selling item.
        /// </summary>
        /// <returns>ActionResult Todays TopSelling Item.</returns>
        public ActionResult TodaysTopSellingItem()
        {
            var restaurantId = GetActiveRestaurantId();
            var topSellingItems = _restaurantService.TopLowSellingMenuItem(restaurantId, DateTime.Now.ToString(), DateTime.Now.ToString());
            return PartialView("_TodaysTopSellingItem", topSellingItems);
        }

        /// <summary>
        /// Loads the restaurant reviews.
        /// </summary>
        /// <returns>ActionResult Load RestaurantReviews.</returns>
        public ActionResult LoadRestaurantReviews()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            var reviews = _restaurantService.GetRestaurantAllReviews(fromDate, toDate, restaurantId, null, skip, pageSize, out Total);
            var data = reviews.Select(b => new
            {
                ReviewId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                Score = b.Score,
                Comment = b.Comment,
                ReviewDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the restaurant table groups.
        /// </summary>
        /// <returns>ActionResult Manage Restaurant TableGroups.</returns>
        public ActionResult ManageRestaurantTableGroups()
        {
            return View();
        }

        /// <summary>
        /// Loads the restaurant table groups.
        /// </summary>
        /// <returns>ActionResult Load RestaurantTable Groups.</returns>
        [HttpPost]
        public ActionResult LoadRestaurantTableGroups()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restRecentOrders = _restaurantService.GetRestaurantTableGroups(restaurantId, skip, pageSize, out Total);
            var data = restRecentOrders.Select(b => new
            {
                RestTableGroupId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy")
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the restaurant table group.
        /// </summary>
        /// <returns>ActionResult Add RestaurantTable Group.</returns>
        public ActionResult AddRestaurantTableGroup()
        {
            RestaurantTableGroup restTableGroup = new RestaurantTableGroup();
            restTableGroup.RestaurantId = GetActiveRestaurantId();
            return View("AddEditRestaurantTableGroup", restTableGroup);
        }

        /// <summary>
        /// Saves the restaurant table group.
        /// </summary>
        /// <param name="restTableGroup">The rest table group.</param>
        /// <returns>ActionResult Save Restaurant TableGroup.</returns>
        public ActionResult SaveRestaurantTableGroup(RestaurantTableGroup restTableGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (restTableGroup.Id == 0)
                    {
                        _restaurantService.CreateRestaurantTableGroup(restTableGroup);
                        return RedirectToAction("ManageRestaurantTableGroups");
                    }
                    else
                    {
                        _restaurantService.modifyRestaurantTableGroup(restTableGroup);
                        return RedirectToAction("ManageRestaurantTableGroups");
                    }
                }
                else
                {
                    return View("AddEditRestaurantTableGroup", restTableGroup);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRestaurantTableGroup", restTableGroup);
            }
        }

        /// <summary>
        /// Edits the restaurant table group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit RestaurantTableGroup.</returns>
        public ActionResult EditRestaurantTableGroup(int id)
        {
            var restTableGroup = _restaurantService.GetRestaurantTableGroupById(id);
            return View("AddEditRestaurantTableGroup", restTableGroup);
        }

        /// <summary>
        /// Deletes the restaurant table group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantTableGroup.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantTableGroup(int id)
        {
            try
            {
                _restaurantService.DeleteRestaurantTableGroup(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This restaurant table group already in use." });
            }
        }

        /// <summary>
        /// Manages the tables.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage Tables.</returns>
        public ActionResult ManageTables(int id)
        {
            ViewBag.RestaurantTableGroupId = id;
            var tableDetails = _restaurantService.GetRestaurantTableDetails(id);
            foreach (var eachTable in tableDetails)
            {
                eachTable.QRCodeImage = Url.Content("/Images/Restaurants/") + eachTable.RestaurantTableGroup.RestaurantId.ToString() + "/TableQRCodes/" + eachTable.Id + "/" + eachTable.QRCodeImage;
            }
            return View(tableDetails);
        }

        /// <summary>
        /// Creates the restaurant table.
        /// </summary>
        /// <param name="RestTableGroupId">The rest table group identifier.</param>
        /// <returns>ActionResult Create RestaurantTable.</returns>
        public ActionResult CreateRestaurantTable(int RestTableGroupId)
        {
            //var enumData = from RestaurantTableStatus e in Enum.GetValues(typeof(RestaurantTableStatus)) select new { ID = (int)e, Name = e.ToString() };
            //ViewBag.RestTableStatus = new SelectList(enumData, "ID", "Name");
            RestaurantTable table = new RestaurantTable();
            table.RestaurantTableGroupId = RestTableGroupId;
            return View("CreateEditRestaurantTable", table);
        }

        /// <summary>
        /// Saves the restaurant table.
        /// </summary>
        /// <param name="restaurantTable">The restaurant table.</param>
        /// <returns>ActionResult Save RestaurantTable.</returns>
        public ActionResult SaveRestaurantTable(RestaurantTable restaurantTable)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (restaurantTable.Id == 0)
                    {
                        var existsTable = _restaurantService.CheckExistsTableNumber(restaurantTable.TableNumber, GetActiveRestaurantId());
                        if (existsTable.Count() > 0 && existsTable != null)
                        {
                            ModelState.AddModelError(string.Empty, Resources.Exst_TableNumber);
                            return View("CreateEditRestaurantTable", restaurantTable);
                        }
                        restaurantTable.QRCodeImage = "QrCode.jpg";
                        var createdRestTable = _restaurantService.CreateRestaurantTable(restaurantTable);


                        string folderPath = "~/Images/Restaurants/" + createdRestTable.RestaurantTableGroup.RestaurantId.ToString() + "/TableQRCodes/" + createdRestTable.Id.ToString() + "/";
                        string imagePath = "~/Images/Restaurants/" + createdRestTable.RestaurantTableGroup.RestaurantId.ToString() + "/TableQRCodes/" + createdRestTable.Id.ToString() + "/QrCode.jpg";
                        var QRCodeImage = GenerateQRCode(createdRestTable.Id.ToString(), folderPath, imagePath);

                        return RedirectToAction("ManageTables", new { id = restaurantTable.RestaurantTableGroupId });
                    }
                    else
                    {
                        _restaurantService.ModifyRestaurantTable(restaurantTable);
                        return RedirectToAction("ManageTables", new { id = restaurantTable.RestaurantTableGroupId });
                    }
                }
                else
                {
                    return View("CreateEditRestaurantTable", restaurantTable);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("CreateEditRestaurantTable", restaurantTable);
            }
        }

        /// <summary>
        /// Downloads the table qr code.
        /// </summary>
        /// <param name="imgPath">The img path.</param>
        /// <param name="tableNo">The table no.</param>
        /// <returns>FilePathResult.</returns>
        public FilePathResult DownloadTableQRCode(string imgPath, int tableNo)
        {
            imgPath = Server.MapPath(imgPath);
            if (System.IO.File.Exists(imgPath))
            {
                var imgName = "TableNo_" + tableNo + "_QrCode.jpg";
                return File(imgPath, "multipart/form-data", imgName);
            }
            return File("/Images/no_logo.png", "multipart/form-data", "file_not_found.png");
        }

        /// <summary>
        /// Edits the restaurant table.
        /// </summary>
        /// <param name="table_id">The table identifier.</param>
        /// <returns>ActionResult Edit RestaurantTable.</returns>
        public ActionResult EditRestaurantTable(int table_id)
        {
            var table = _restaurantService.GetRestaurantTableById(table_id);
            if (table == null)
            {
                return HttpNotFound();
            }
            return View("CreateEditRestaurantTable", table);
        }

        /// <summary>
        /// Deletes the restaurant table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantTable.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantTable(int id)
        {
            try
            {
                var tabledetails = _restaurantService.GetRestaurantTableById(id);
                _restaurantService.DeleteRestaurantTable(id);
                return RedirectToAction("ManageTables", "Restaurant", new { id = tabledetails.RestaurantTableGroupId });

            }
            catch
            {
                return View("ManageTables");
            }
        }

        /// <summary>
        /// Gets the rest recent tables.
        /// </summary>
        /// <returns>ActionResult Get Restaurant RecentTables.</returns>
        public ActionResult GetRestRecentTables()
        {
            var restaurantId = GetActiveRestaurantId();
            var tblStatusChangedCount = _restaurantService.GetRestTablesStatusChangedCount(restaurantId);
            ViewBag.TableStatusChangedCount = tblStatusChangedCount;
            ViewBag.OrderNextServingStatusChangedCount = _restaurantService.GetOrderNextServingStatusChangedCount(restaurantId);
            return PartialView("_GetRestRecentTables");
        }

        /// <summary>
        /// Manages the tables status queue.
        /// </summary>
        /// <returns>ActionResult Manage TablesStatusQueue.</returns>
        public ActionResult ManageTablesStatusQueue()
        {
            return View();
        }

        /// <summary>
        /// Loads the tables status queue.
        /// </summary>
        /// <returns>ActionResult Load TablesStatusQueue.</returns>
        [HttpPost]
        public ActionResult LoadTablesStatusQueue()
        {
            var restId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var tblStatusChangedTables = _restaurantService.GetRestStatusChangedTables(restId, skip, pageSize, out Total);
            var data = tblStatusChangedTables.Select(b => new
            {
                TableId = b.Id,
                GroupName = b.RestaurantTableGroup.Name,
                TableNumber = b.TableNumber,
                TotalSeats = b.TotalSeats,
                IsActive = b.IsActive,
                TableStatus = ((RestaurantTableStatus)b.TableStatus).ToString()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates the rest table status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update RestTableStatus.</returns>
        public ActionResult UpdateRestTableStatus(int id)
        {
            try
            {
                var tableDetails = _restaurantService.GetRestaurantTableById(id);
                tableDetails.TableStatus = null;
                _restaurantService.ModifyRestaurantTable(tableDetails);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This table status cannot be change" });
            }
        }

        // Manage RestaurantTableMenu
        /// <summary>
        /// Manages the restaurant table menu.
        /// </summary>
        /// <param name="tableId">The table identifier.</param>
        /// <param name="tableNumber">The table number.</param>
        /// <returns>ActionResult Manage Restaurant TableMenu.</returns>
        public ActionResult ManageRestaurantTableMenu(int tableId, int? tableNumber = null)
        {
            ViewBag.TableId = tableId;
            ViewBag.RestaurantTableGroupId = _restaurantService.GetRestaurantTableById(tableId).RestaurantTableGroupId;
            var restaurantTableMenus = _restaurantService.GetRestaurantTableMenuByTableId(tableId);
            if (tableNumber == null)
                ViewBag.TableNumber = restaurantTableMenus.FirstOrDefault().RestaurantTable.TableNumber;
            else
                ViewBag.TableNumber = tableNumber;
            return View(restaurantTableMenus);
        }

        /// <summary>
        /// Creates the restaurant table menu.
        /// </summary>
        /// <param name="tableId">The table identifier.</param>
        /// <param name="tableNumber">The table number.</param>
        /// <returns>ActionResult Create RestaurantTableMenu.</returns>
        public ActionResult CreateRestaurantTableMenu(int tableId, int tableNumber)
        {
            var restaurantId = GetActiveRestaurantId();
            RestaurantTableMenu restaurantTableMenu = new RestaurantTableMenu()
            {
                RestaurantTableId = tableId
            };
            ViewBag.TableNumber = tableNumber;
            ViewBag.RestaurantMenu = _restaurantService.GetAllUnassignedRestTableMenu(restaurantId, tableId).Select(m => new { RestaurantMenuId = m.Id, MenuName = m.MenuName });
            return View("AddEditRestaurantTableMenu", restaurantTableMenu);
        }

        /// <summary>
        /// Adds the edit restaurant table menu.
        /// </summary>
        /// <param name="restaurantTableMenu">The restaurant table menu.</param>
        /// <returns>ActionResult AddEdit RestaurantTableMenu.</returns>
        [HttpPost]
        public ActionResult AddEditRestaurantTableMenu(RestaurantTableMenu restaurantTableMenu)
        {
            var restaurantId = GetActiveRestaurantId();
            try
            {
                if (ModelState.IsValid)
                {
                    if (restaurantTableMenu.Id > 0)
                    {
                        var resTableMenu = _restaurantService.ModifyRestaurantTableMenu(restaurantTableMenu);
                        return RedirectToAction("ManageRestaurantTableMenu", new { tableId = resTableMenu.RestaurantTableId });
                    }
                    else
                    {
                        var resTableMenu = _restaurantService.CreateRestaurantTableMenu(restaurantTableMenu);
                        return RedirectToAction("ManageRestaurantTableMenu", new { tableId = resTableMenu.RestaurantTableId });
                    }
                }
                else
                {
                    var restaurantMenu = _restaurantService.GetRestaurantTableMenuByTableId(restaurantTableMenu.RestaurantTableId);
                    ViewBag.TableNumber = restaurantMenu.FirstOrDefault().RestaurantTable.TableNumber;
                    ViewBag.RestaurantMenu = _restaurantService.GetAllUnassignedRestTableMenu(restaurantId, restaurantTableMenu.RestaurantTableId).Select(m => new { RestaurantMenuId = m.Id, MenuName = m.MenuName });
                    return View("AddEditRestaurantTableMenu", restaurantTableMenu);
                }
            }
            catch (Exception ex)
            {
                var restaurantMenu = _restaurantService.GetRestaurantTableMenuByTableId(restaurantTableMenu.RestaurantTableId);
                ViewBag.TableNumber = restaurantMenu.FirstOrDefault().RestaurantTable.TableNumber;
                ViewBag.RestaurantMenu = _restaurantService.GetAllUnassignedRestTableMenu(restaurantId, restaurantTableMenu.RestaurantTableId).Select(m => new { RestaurantMenuId = m.Id, MenuName = m.MenuName });
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditRestaurantTableMenu", restaurantTableMenu);
            }
        }

        /// <summary>
        /// Edits the restaurant table menu.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult Edit RestaurantTableMenu.</returns>
        public ActionResult EditRestaurantTableMenu(int Id)
        {
            var restaurantId = GetActiveRestaurantId();
            var restaurantTableMenu = _restaurantService.GetRestaurantTableMenuById(Id);

            ViewBag.TableNumber = restaurantTableMenu.RestaurantTable.TableNumber;
            ViewBag.RestaurantMenu = _restaurantService.GetAllUnassignedRestTableMenu(restaurantId, restaurantTableMenu.RestaurantTableId).Select(m => new { RestaurantMenuId = m.Id, MenuName = m.MenuName });
            return View("AddEditRestaurantTableMenu", restaurantTableMenu);
        }

        /// <summary>
        /// Deletes the restaurant table menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantTableMenu.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantTableMenu(int id)
        {
            _restaurantService.DeleteRestaurantTableMenu(id);
            return Json(new { status = true });
        }


        /// <summary>
        /// Manages the menu additional groups.
        /// </summary>
        /// <returns>ActionResult Manage MenuAdditionalGroups.</returns>
        public ActionResult ManageMenuAdditionalGroups()
        {
            var menuAdditionalGrps = _restaurantService.GetMenuAdditionalGroups(GetActiveRestaurantId());
            return View(menuAdditionalGrps);
        }

        /// <summary>
        /// Creates the menu additional group.
        /// </summary>
        /// <returns>ActionResult Create MenuAdditionalGroup.</returns>
        public ActionResult CreateMenuAdditionalGroup()
        {
            MenuAdditionalGroup menuAdditionalGroup = new MenuAdditionalGroup();
            menuAdditionalGroup.RestaurantId = GetActiveRestaurantId();
            return View("AddEditMenuAdditionalGroup", menuAdditionalGroup);
        }

        /// <summary>
        /// Saves the menu additional group.
        /// </summary>
        /// <param name="menuAdditionalGrp">The menu additional GRP.</param>
        /// <returns>ActionResult Save MenuAdditionalGroup.</returns>
        public ActionResult SaveMenuAdditionalGroup(MenuAdditionalGroup menuAdditionalGrp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (menuAdditionalGrp.MinimumSelected == null)
                    {
                        ModelState.AddModelError("MinimumSelected", Resources.Rqd_MinSelectedValue);
                        return View("AddEditMenuAdditionalGroup", menuAdditionalGrp);
                    }
                    if (menuAdditionalGrp.MaximumSelected == null)
                    {
                        ModelState.AddModelError("MinimumSelected", Resources.Rqd_MaxSelectedValue);
                        return View("AddEditMenuAdditionalGroup", menuAdditionalGrp);
                    }
                    if (menuAdditionalGrp.MaximumSelected < menuAdditionalGrp.MinimumSelected)
                    {
                        ModelState.AddModelError(string.Empty, Resources.Invalid_MaximumSelected);
                        return View("AddEditMenuAdditionalGroup", menuAdditionalGrp);
                    }
                    if (menuAdditionalGrp.Id == 0)
                    {
                        _restaurantService.CreateMenuAdditionalGroup(menuAdditionalGrp);
                        return RedirectToAction("ManageMenuAdditionalGroups");
                    }
                    else
                    {
                        _restaurantService.ModifyMenuAdditionalGroup(menuAdditionalGrp);
                        return RedirectToAction("ManageMenuAdditionalGroups");
                    }
                }
                else
                {
                    return View("AddEditMenuAdditionalGroup", menuAdditionalGrp);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditMenuAdditionalGroup", menuAdditionalGrp);
            }
        }

        /// <summary>
        /// Edits the menu additional group.
        /// </summary>
        /// <param name="group_id">The group identifier.</param>
        /// <returns>ActionResult Edit MenuAdditionalGroup.</returns>
        public ActionResult EditMenuAdditionalGroup(int group_id)
        {
            var menuAdditionalGroup = _restaurantService.GetMenuAdditionalGroupById(group_id);
            return View("AddEditMenuAdditionalGroup", menuAdditionalGroup);
        }

        /// <summary>
        /// Deletes the menu additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete MenuAdditionalGroup.</returns>
        public ActionResult DeleteMenuAdditionalGroup(int id)
        {
            try
            {
                _restaurantService.DeleteMenuAdditionalGroup(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Menu Additional Group is already in use." });
            }
        }

        /// <summary>
        /// Manages the menu additional elements.
        /// </summary>
        /// <param name="group_id">The group identifier.</param>
        /// <param name="grp_name">Name of the GRP.</param>
        /// <returns>ActionResult Manage MenuAdditionalElements.</returns>
        public ActionResult ManageMenuAdditionalElements(int group_id, string grp_name)
        {

            ViewBag.GroupId = group_id;
            var menuAdditionalElement = _restaurantService.GetMenuAdditionalElements(group_id);
            if (string.IsNullOrWhiteSpace(grp_name))
                ViewBag.MenuAdditionalGroup = menuAdditionalElement.FirstOrDefault().MenuAdditionalGroup.GroupName;
            else
                ViewBag.MenuAdditionalGroup = grp_name;
            return View(menuAdditionalElement);
        }

        /// <summary>
        /// Adds the menu additional element.
        /// </summary>
        /// <param name="grp_id">The GRP identifier.</param>
        /// <param name="grp_name">Name of the GRP.</param>
        /// <returns>ActionResult Add MenuAdditionalElement.</returns>
        public ActionResult AddMenuAdditionalElement(int grp_id, string grp_name)
        {
            ViewBag.MenuAdditionalGroup = grp_name;
            MenuAdditionalElement menuAdditionalElement = new MenuAdditionalElement();
            menuAdditionalElement.MenuAdditionalGroupId = grp_id;
            return View("AddEditMenuAdditionalElement", menuAdditionalElement);
        }

        /// <summary>
        /// Saves the menu additional element.
        /// </summary>
        /// <param name="menuAdditionalElement">The menu additional element.</param>
        /// <param name="uploadAdditionaElementImage">The upload additiona element image.</param>
        /// <returns>ActionResult Save MenuAdditionalElement.</returns>
        [HttpPost]
        public ActionResult SaveMenuAdditionalElement(MenuAdditionalElement menuAdditionalElement, HttpPostedFileBase uploadAdditionaElementImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadAdditionaElementImage != null)
                    {
                        if (uploadAdditionaElementImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadAdditionaElementImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadAdditionaElementImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                menuAdditionalElement.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image " + Resources.Invalid_ImageExtension);
                                return View("AddEditMenuAdditionalElement", menuAdditionalElement);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                            return View("AddEditMenuAdditionalElement", menuAdditionalElement);
                        }
                    }
                    if (menuAdditionalElement.Id > 0)
                    {
                        var resElement = _restaurantService.ModifyMenuAdditionalElement(menuAdditionalElement);
                        if (uploadAdditionaElementImage != null)
                        {
                            ImageHelper.UploadImage(resElement.MenuAdditionalGroupId, ImagePathFor.MenuAdditionalElementImage, uploadAdditionaElementImage, api_path, resElement.Id);
                        }
                        return RedirectToAction("ManageMenuAdditionalElements", new { group_id = resElement.MenuAdditionalGroupId });
                    }
                    else
                    {
                        var resElement = _restaurantService.CreateMenuAdditionalElement(menuAdditionalElement);
                        if (uploadAdditionaElementImage != null)
                        {
                            ImageHelper.UploadImage(resElement.MenuAdditionalGroupId, ImagePathFor.MenuAdditionalElementImage, uploadAdditionaElementImage, api_path, resElement.Id);
                        }
                        return RedirectToAction("ManageMenuAdditionalElements", new { group_id = resElement.MenuAdditionalGroupId });
                    }

                }
                else
                {
                    return View("AddEditMenuAdditionalElement", menuAdditionalElement);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditMenuAdditionalElement", menuAdditionalElement);
            }
        }

        /// <summary>
        /// Edits the menu additional element.
        /// </summary>
        /// <param name="element_id">The element identifier.</param>
        /// <returns>ActionResult Edit MenuAdditionalElement.</returns>
        public ActionResult EditMenuAdditionalElement(int element_id)
        {
            var menuAdditionalElement = _restaurantService.GetMenuAdditionalElementById(element_id);
            ViewBag.MenuAdditionalGroup = menuAdditionalElement.MenuAdditionalGroup.GroupName;
            return View("AddEditMenuAdditionalElement", menuAdditionalElement);
        }

        /// <summary>
        /// Deletes the menu additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="grp_id">The GRP identifier.</param>
        /// <returns>ActionResult Delete MenuAdditionalElement.</returns>
        [HttpPost]
        public ActionResult DeleteMenuAdditionalElement(int id, int grp_id)
        {
            try
            {
                _restaurantService.DeleteMenuAdditionalElement(id);
                return RedirectToAction("ManageMenuAdditionalElements", new { group_id = grp_id });
            }
            catch
            {
                return RedirectToAction("ManageMenuAdditionalElements", new { group_id = grp_id });
            }
        }

        /// <summary>
        /// Manages the menu.
        /// </summary>
        /// <returns>ActionResult ManageMenu.</returns>
        public ActionResult ManageMenu()
        {
            var language = _restaurantService.GetLanguageByCulture(cultureName);
            var restaurantId = GetActiveRestaurantId();
            int total;
            ViewBag.LanguageId = language.Id;
            var items = _restaurantService.GetAllRestaurantMenu(restaurantId, r => r.Id, 1, int.MaxValue, out total);
            return View(items);
        }

        /// <summary>
        /// Updates the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update RestaurantMenuGroup.</returns>
        [HttpPost]
        public ActionResult UpdateRestaurantMenuGroup(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();
                _restaurantService.UpdateMenuGroup(id, restaurantId);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Deletes the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenuGroup.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantMenuGroup(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();
                _restaurantService.DeleteRestaurantMenuGroup(id, restaurantId);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This menu group already in use." });
            }
        }

        /// <summary>
        /// Updates the restaurant menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update RestaurantMenuItem.</returns>
        [HttpPost]
        public ActionResult UpdateRestaurantMenuItem(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();
                _restaurantService.UpdateMenuItem(id, restaurantId);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Updates the menu itemfor sold out.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update MenuItemforSoldOut.</returns>
        [HttpPost]
        public ActionResult UpdateMenuItemforSoldOut(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();
                _restaurantService.UpdateMenuItemforSoldOut(id, restaurantId);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Deletes the restaurant menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenuItem.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantMenuItem(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();

                List<string> allItemImagePath = new List<string>();
                var itemImages = _restaurantService.GetItemImages(id);
                foreach (var itemImage in itemImages)
                {
                    string imgPath = "";
                    if (!string.IsNullOrWhiteSpace(itemImage.Image))
                    {
                        imgPath = @"\Images\MenuItem\" + itemImage.RestaurantMenuItemId + @"\" + itemImage.Image;
                    }
                    if (imgPath != "")
                        allItemImagePath.Add(imgPath);
                }

                _restaurantService.DeleteRestaurantMenuItem(id, restaurantId);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                if (allItemImagePath.Any())
                {
                    foreach (var itemImgPath in allItemImagePath)
                    {
                        ImageHelper.DeleteImage(api_path, itemImgPath);
                    }
                }
                return RedirectToAction("ManageMenu", "Restaurant");
            }
            catch
            {
                return Json(new { failure = true, error = "This item is already in use." });
            }
        }

        /// <summary>
        /// Creates the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Create RestaurantMenuGroup.</returns>
        public ActionResult CreateRestaurantMenuGroup(int id)
        {
            ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
            RestaurantMenuGroup group = new RestaurantMenuGroup
            {
                RestaurantMenuId = id,
            };

            return View(group);
        }

        /// <summary>
        /// Saves the restaurant menu group.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns>ActionResult Save RestaurantMenuGroup.</returns>
        public ActionResult SaveRestaurantMenuGroup(RestaurantMenuGroup group)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                if (ModelState.IsValid)
                {
                    var TranslationListCount = Convert.ToInt32(Request["hdnCountOfRows"]);
                    List<RestaurantMenuGroupTranslation> menuGroupTranslationList = new List<RestaurantMenuGroupTranslation>();
                    for (var i = 0; i < TranslationListCount; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(Request["txtGroupName_" + i]))
                        {
                            if (group.LanguageId == Convert.ToInt32(Request["ddlLanguages_" + i]))
                            {
                                ModelState.AddModelError(string.Empty, "Language chosen for menu group and menu group translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        for (var j = i + 1; j < TranslationListCount; j++)
                        {
                            if (Convert.ToInt32(Request["ddlLanguages_" + i]) == Convert.ToInt32(Request["ddlLanguages_" + j]))
                            {
                                ModelState.AddModelError(string.Empty, "language chosen in menu group translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request["txtGroupName_" + i]))
                        {
                            RestaurantMenuGroupTranslation eachMenuGroupTranslation = new RestaurantMenuGroupTranslation()
                            {
                                LanguageId = Convert.ToInt32(Request["ddlLanguages_" + i]),
                                TranslatedGroupName = Request["txtGroupName_" + i],
                                TranslatedGroupDescription = Request["txtGroupDesc_" + i]
                            };
                            menuGroupTranslationList.Add(eachMenuGroupTranslation);
                        }

                    }
                    group.RestaurantMenuGroupTranslations = menuGroupTranslationList;
                    _restaurantService.CreateRestaurantMenuGroup(group);
                    return Json(new { success = true });
                }
                else
                {
                    var errors = GetErrorsFromModelState();
                    return Json(new { success = false, errors = errors });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Edits the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit RestaurantMenuGroup.</returns>
        public ActionResult EditRestaurantMenuGroup(int id)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                var menuGroup = _restaurantService.GetRestaurantMenuGroupById(id);
                return View("EditRestaurantMenuGroup", menuGroup);
            }
            catch
            {
                return View("ManageManu ");
            }
        }

        /// <summary>
        /// Edits the restaurant menu group.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns>ActionResult Edit RestaurantMenuGroup.</returns>
        [HttpPost]
        public ActionResult EditRestaurantMenuGroup(RestaurantMenuGroup group)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                if (ModelState.IsValid)
                {
                    var TranslationListCount = Convert.ToInt32(Request["hdnCountOfRows"]);
                    List<RestaurantMenuGroupTranslation> menuGroupTranslationList = new List<RestaurantMenuGroupTranslation>();
                    for (var i = 0; i < TranslationListCount; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(Request["txtGroupName_" + i]))
                        {
                            var transLangId = Convert.ToInt32(Request["ddlLanguages_" + i]);
                            if (group.LanguageId == transLangId)
                            {
                                ModelState.AddModelError(string.Empty, "Language chosen for menu group and menu group translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }

                        for (var j = i + 1; j < TranslationListCount; j++)
                        {
                            if (Convert.ToInt32(Request["ddlLanguages_" + i]) == Convert.ToInt32(Request["ddlLanguages_" + j]))
                            {
                                ModelState.AddModelError(string.Empty, "language chosen in menu group translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request["txtGroupName_" + i]))
                        {
                            RestaurantMenuGroupTranslation eachMenuGroupTranslation = new RestaurantMenuGroupTranslation()
                            {
                                Id = Convert.ToInt32(Request["hdnTranslationId_" + i]),
                                RestaurantMenuGroupId = group.Id,
                                LanguageId = Convert.ToInt32(Request["ddlLanguages_" + i]),
                                TranslatedGroupName = Request["txtGroupName_" + i],
                                TranslatedGroupDescription = Request["txtGroupDesc_" + i]
                            };
                            menuGroupTranslationList.Add(eachMenuGroupTranslation);
                        }

                    }
                    _restaurantService.ModifyRestaurantMenuGroup(group, menuGroupTranslationList);
                    return Json(new { success = true });
                }
                else
                {
                    var errors = GetErrorsFromModelState();
                    return Json(new { success = false, errors = errors });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Deletes the restaurant menu group translation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenuGroupTranslation.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantMenuGroupTranslation(int id)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                _restaurantService.DeleteRestaurantMenuGroupTranslation(id);
                return PartialView("Success");
            }
            catch
            {
                return View("EditRestaurantMenuGroup");
            }
        }

        /// <summary>
        /// Creates the restaurant menu item.
        /// </summary>
        /// <param name="group_id">The group identifier.</param>
        /// <returns>ActionResult Create RestaurantMenuItem.</returns>
        public ActionResult CreateRestaurantMenuItem(int group_id)
        {
            ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
            RestaurantMenuItem item = new RestaurantMenuItem
            {
                RestaurantMenuGroupId = group_id,
            };

            return View(item);
        }

        /// <summary>
        /// Saves the restaurant menu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>ActionResult Save RestaurantMenuItem.</returns>
        public ActionResult SaveRestaurantMenuItem(RestaurantMenuItem item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var TranslationListCount = Convert.ToInt32(Request["hdnCountOfRows"]);
                    List<RestaurantMenuItemTranslation> menuItemTranslationList = new List<RestaurantMenuItemTranslation>();
                    for (var i = 0; i < TranslationListCount; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(Request["txtItemName_" + i]))
                        {
                            if (item.LanguageId == Convert.ToInt32(Request["ddlLanguages_" + i]))
                            {
                                ModelState.AddModelError(string.Empty, "Language chosen for menu item and menu item translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        for (var j = i + 1; j < TranslationListCount; j++)
                        {
                            if (Convert.ToInt32(Request["ddlLanguages_" + i]) == Convert.ToInt32(Request["ddlLanguages_" + j]))
                            {
                                ModelState.AddModelError(string.Empty, "language chosen in menu item translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request["txtItemName_" + i]))
                        {
                            RestaurantMenuItemTranslation eachMenuItemTranslation = new RestaurantMenuItemTranslation()
                            {
                                Id = Convert.ToInt32(Request["hdnTranslationId_" + i]),
                                RestaurantMenuItemId = item.Id,
                                LanguageId = Convert.ToInt32(Request["ddlLanguages_" + i]),
                                TranslatedItemName = Request["txtItemName_" + i],
                                TranslatedItemDescription = Request["txtItemDesc_" + i]
                            };
                            menuItemTranslationList.Add(eachMenuItemTranslation);
                        }

                    }

                    var restaurantId = GetActiveRestaurantId();
                    if (item.Id == 0)
                    {
                        item.RestaurantMenuItemTranslations = menuItemTranslationList;
                        var menuItem = _restaurantService.CreateRestaurantMenuItem(item, restaurantId);
                        return Json(new { success = true });
                    }
                    else
                    {
                        _restaurantService.ModifyMenuItem(item, menuItemTranslationList, restaurantId);
                        return Json(new { success = true });
                    }
                }
                else
                {
                    var errors = GetErrorsFromModelState();
                    return Json(new { success = false, errors = errors });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Edits the restaurant menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit RestaurantMenuItem.</returns>
        public ActionResult EditRestaurantMenuItem(int id)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                int? languageId = null;
                var item = _restaurantService.GetMenuItem(id, languageId);
                return View("CreateRestaurantMenuItem", item);
            }
            catch (Exception ex)
            {
                return RedirectToAction("ManageMenu");
            }

        }

        /// <summary>
        /// Deletes the restaurant menu item translation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenuItemTranslation.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantMenuItemTranslation(int id)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                _restaurantService.DeleteRestaurantMenuItemTranslation(id);
                return PartialView("Success");
            }
            catch
            {
                return View("CreateRestaurantMenuItem");
            }
        }

        /// <summary>
        /// Tops the selling.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult TopSelling.</returns>
        public ActionResult TopSelling(TopSellingItemVM filters)
        {
            return View(filters);
        }

        /// <summary>
        /// Tops the selling menu item.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult TopSellingMenuItem.</returns>
        public ActionResult TopSellingMenuItem(TopSellingItemVM filters)
        {
            var restaurantId = GetActiveRestaurantId();
            var topSellingItems = _restaurantService.TopLowSellingMenuItem(restaurantId, filters.FromDate, filters.ToDate);
            return PartialView("_TopSellingMenuItem", topSellingItems);
        }

        /// <summary>
        /// Tops the selling category wise.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult TopSelling CategoryWise.</returns>
        public ActionResult TopSellingCategoryWise(TopSellingItemVM filters)
        {
            var restaurantId = GetActiveRestaurantId();
            var topSellingItems = _restaurantService.TopLowSellingMenuItem(restaurantId, filters.FromDate, filters.ToDate);
            return PartialView("_TopSellingCategoryWise", topSellingItems);
        }

        /// <summary>
        /// Lows the selling.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult LowSelling.</returns>
        public ActionResult LowSelling(TopSellingItemVM filters)
        {
            return View(filters);
        }

        /// <summary>
        /// Lows the selling menu item.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult LowSellingMenuItem.</returns>
        public ActionResult LowSellingMenuItem(TopSellingItemVM filters)
        {
            var restaurantId = GetActiveRestaurantId();
            var lowSellingItems = _restaurantService.TopLowSellingMenuItem(restaurantId, filters.FromDate, filters.ToDate);
            return PartialView("_LowSellingMenuItem", lowSellingItems);
        }

        /// <summary>
        /// Lows the selling category wise.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <returns>ActionResult LowSellingCategoryWise.</returns>
        public ActionResult LowSellingCategoryWise(TopSellingItemVM filters)
        {
            var restaurantId = GetActiveRestaurantId();
            var lowSellingItems = _restaurantService.TopLowSellingMenuItem(restaurantId, filters.FromDate, filters.ToDate);
            return PartialView("_LowSellingCategoryWise", lowSellingItems);
        }

        /// <summary>
        /// Authorizeds the users.
        /// </summary>
        /// <returns>ActionResult AuthorizedUsers.</returns>
        public ActionResult AuthorizedUsers()
        {
            var restaurantId = GetActiveRestaurantId();

            var authorizedUsers = _restaurantService.RestaurantAuthorizedCustomers(restaurantId);
            return View(authorizedUsers);
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <returns>ActionResult Create CustomerRole.</returns>
        public ActionResult CreateCustomerRole()
        {
            CustomerRoleViewModel customerRole = new CustomerRoleViewModel();
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
            ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
            ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
            return View("CreateEditCustomer", customerRole);
        }

        /// <summary>
        /// Customerses the specified term.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <returns>ActionResult customers.</returns>
        [HttpPost]
        public ActionResult customers(string term)
        {
            var result = _restaurantService.GetCustomers(term).Select(r => new { id = r.Id, Name = r.FirstName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Customers the information.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult CustomerInfo.</returns>
        [HttpPost]
        public ActionResult CustomerInfo(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            Customer cust = new Customer()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                BirthDate = customer.BirthDate,
                Telephone = customer.Telephone,
                Email = customer.Email,
                Street = customer.Street,
                City = customer.City,
                Region = customer.Region,
                Country = customer.Country,
                Pincode = customer.Pincode,
                Password = customer.Password
            };
            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>ActionResult Create CustomerRole.</returns>
        [HttpPost]
        public ActionResult CreateCustomerRole(CustomerRoleViewModel customerRole)
        {
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    string passwordCode = Guid.NewGuid().ToString();
                    Customer newCustomer = new Customer()
                    {
                        FirstName = customerRole.Customer.FirstName,
                        LastName = customerRole.Customer.LastName,
                        BirthDate = customerRole.Customer.BirthDate,
                        Email = customerRole.Customer.Email,
                        Password = Hashing.HashPassword(passwordCode),
                        PhoneCode = customerRole.Customer.PhoneCode,
                        Telephone = customerRole.Customer.Telephone,
                        City = customerRole.Customer.City,
                        Street = customerRole.Customer.Street,
                        Street2 = customerRole.Customer.Street2,
                        Region = customerRole.Customer.Region,
                        CountryId = customerRole.Customer.CountryId,
                        Pincode = customerRole.Customer.Pincode,
                        CurrencyId = customerRole.Customer.CurrencyId
                    };

                    newCustomer.CreationDate = System.DateTime.Now;
                    newCustomer.ActiveStatus = true;

                    string activationCode = Guid.NewGuid().ToString();
                    newCustomer.ActivationCode = activationCode;

                    var customer = _customerService.CreateCustomer(newCustomer);
                    CustomerRole role = new CustomerRole()
                    {
                        CustomerId = customer.Id,
                        RoleId = customerRole.RoleId,
                        HotelId = null,
                        RestaurantId = GetActiveRestaurantId(),
                        ChainId = null
                    };
                    _restaurantService.CreateCustomerRole(role);

                    SendPasswordOnEmail(customer, passwordCode);
                    return RedirectToAction("AuthorizedUsers", "Restaurant");
                }
                else
                {
                    ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
                    ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                    ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
                ViewBag.Currencies = _customerService.GetAllCurrencies().Select(c => new { Id = c.Id, Code = c.Code + " - " + c.CurrencyName });
                ViewBag.Countries = _customerService.GetAllCountries().Select(c => new { Id = c.Id, Name = c.Name });
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Sends the password on email.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="passwordCode">The password code.</param>
        private void SendPasswordOnEmail(Customer customer, string passwordCode)
        {
            try
            {
                //todo: make it asynchronnious call and template based

                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("account@youordr.com", customer.Email.ToString()))
                {
                    mm.Subject = "Account Password";
                    string body = "Hello " + customer.FirstName.Trim() + ",";
                    body += "<br /><br />Please use below password to login your account";
                    body += "Password " + passwordCode;
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("account@youordr.com", "Certigoa2015!");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update CustomerRole.</returns>
        public ActionResult UpdateCustomerRole(int id)
        {
            var customerRole = _restaurantService.GetCustomerByRoleId(id);
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();

            RegisterCustomerVM vModel = new RegisterCustomerVM()
            {
                FirstName = customerRole.Customer.FirstName,
                LastName = customerRole.Customer.LastName
            };

            CustomerRoleViewModel model = new CustomerRoleViewModel()
            {
                Id = customerRole.Id,
                RoleId = customerRole.RoleId,
                CustomerId = customerRole.CustomerId,
                RestaurantId = customerRole.RestaurantId,
                Customer = vModel
            };

            ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
            return View("CreateEditCustomer", model);
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="customerRole">The customer role.</param>
        /// <returns>ActionResult Update CustomerRole.</returns>
        [HttpPost]
        public ActionResult UpdateCustomerRole(CustomerRoleViewModel customerRole)
        {
            var customerRoleId = GetAuthenticatedCustomer().CustomerRoles.Select(r => r.RoleId).FirstOrDefault();
            try
            {
                ModelState["Customer.CurrencyId"].Errors.Clear();
                ModelState["Customer.PhoneCode"].Errors.Clear();
                ModelState["Customer.Telephone"].Errors.Clear();
                ModelState["Customer.Email"].Errors.Clear();
                ModelState["Customer.LastName"].Errors.Clear();
                ModelState["Customer.CountryId"].Errors.Clear();
                if (ModelState.IsValid)
                {
                    CustomerRole custsRole = new CustomerRole()
                    {
                        Id = customerRole.Id,
                        CustomerId = customerRole.CustomerId,
                        RoleId = customerRole.RoleId,
                        HotelId = null,
                        RestaurantId = customerRole.RestaurantId,
                        ChainId = null
                    };

                    _restaurantService.UpdateCustomerRole(custsRole);
                    return RedirectToAction("AuthorizedUsers", "Restaurant");
                }
                else
                {
                    ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
                    return View("CreateEditCustomer", customerRole);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.Roles = _restaurantService.ManageRole(customerRoleId);
                return View("CreateEditCustomer", customerRole);
            }
        }

        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete CustomerRole.</returns>
        [HttpPost]
        public ActionResult DeleteCustomerRole(int id)
        {
            try
            {
                _restaurantService.DeleteCustomerRole(id);
                return RedirectToAction("AuthorizedUsers", "Restaurant");
            }
            catch
            {
                return View("AuthorizedUsers");
            }
        }

        /// <summary>
        /// Creates the restaurant menu.
        /// </summary>
        /// <returns>ActionResult Create RestaurantMenu.</returns>
        public ActionResult CreateRestaurantMenu()
        {
            var restaurantId = GetActiveRestaurantId();
            ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
            RestaurantMenu menu = new RestaurantMenu
            {
                RestaurantId = restaurantId
            };

            return View(menu);
        }

        /// <summary>
        /// Creates the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <param name="uploadMenuImage">The upload menu image.</param>
        /// <returns>ActionResult Create RestaurantMenu.</returns>
        [HttpPost]
        public ActionResult CreateRestaurantMenu(RestaurantMenu menu, HttpPostedFileBase uploadMenuImage)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadMenuImage != null)
                    {
                        if (uploadMenuImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadMenuImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadMenuImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                menu.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            var errors = GetErrorsFromModelState();
                            return Json(new { success = false, errors = errors });
                        }
                    }
                    var TranslationListCount = Convert.ToInt32(Request["hdnCountOfRows"]);
                    List<RestaurantMenuTranslation> menuTranslationList = new List<RestaurantMenuTranslation>();
                    for (var i = 0; i < TranslationListCount; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(Request["txtMenuName_" + i]))
                        {
                            if (menu.LanguageId == Convert.ToInt32(Request["ddlLanguages_" + i]))
                            {
                                ModelState.AddModelError(string.Empty, "Language chosen for menu and menu translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        for (var j = i + 1; j < TranslationListCount; j++)
                        {
                            if (Convert.ToInt32(Request["ddlLanguages_" + i]) == Convert.ToInt32(Request["ddlLanguages_" + j]))
                            {
                                ModelState.AddModelError(string.Empty, "language chosen in menu translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request["txtMenuName_" + i])) //&& !string.IsNullOrWhiteSpace(Request["txtMenuDesc_" + i])
                        {
                            RestaurantMenuTranslation eachMenuTranslation = new RestaurantMenuTranslation()
                            {
                                LanguageId = Convert.ToInt32(Request["ddlLanguages_" + i]),
                                TranslatedMenuName = Request["txtMenuName_" + i],
                                TranslatedMenuDescription = Request["txtMenuDesc_" + i]
                            };
                            menuTranslationList.Add(eachMenuTranslation);
                        }

                    }
                    menu.RestaurantMenuTranslations = menuTranslationList;
                    var resMenu = _restaurantService.CreateRestaurantMenu(menu);
                    if (uploadMenuImage != null)
                    {
                        ImageHelper.UploadImage(menu.RestaurantId, ImagePathFor.MenuImage, uploadMenuImage, api_path, resMenu.Id);
                    }
                    //ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                    return Json(new { success = true });
                }
                else
                {
                    var errors = GetErrorsFromModelState();
                    return Json(new { success = false, errors = errors });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Edits the restaurant menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit RestaurantMenu.</returns>
        public ActionResult EditRestaurantMenu(int id)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                var menu = _restaurantService.GetRestaurantMenuById(id);
                return View("EditRestaurantMenu", menu);
            }
            catch
            {
                return View("ManageManu ");
            }
        }

        /// <summary>
        /// Gets the state of the errors from model.
        /// </summary>
        /// <returns>Dictionary&lt;System.String, ModelErrorCollection&gt;.</returns>
        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState() //IEnumerable<string>
        {
            //return ModelState.SelectMany(x => x.Value.Errors
            //    .Select(error => error.ErrorMessage));

            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }
        /// <summary>
        /// Edits the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <param name="uploadMenuImage">The upload menu image.</param>
        /// <returns>ActionResult Edit RestaurantMenu.</returns>
        [HttpPost]
        public ActionResult EditRestaurantMenu(RestaurantMenu menu, HttpPostedFileBase uploadMenuImage)
        {
            try
            {
                ViewBag.Languages = _restaurantService.GetAllLanguages().ToList();
                if (ModelState.IsValid)
                {
                    ModelState.Clear();
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadMenuImage != null)
                    {
                        if (uploadMenuImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadMenuImage.FileName);
                            var ext = Path.GetExtension(uploadMenuImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                menu.Image = filename;
                                ImageHelper.UploadImage(menu.RestaurantId, ImagePathFor.MenuImage, uploadMenuImage, api_path, menu.Id);
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            var errors = GetErrorsFromModelState();
                            return Json(new { success = false, errors = errors });
                        }
                    }
                    var TranslationListCount = Convert.ToInt32(Request["hdnCountOfRows"]);
                    List<RestaurantMenuTranslation> menuTranslationList = new List<RestaurantMenuTranslation>();
                    for (var i = 0; i < TranslationListCount; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(Request["txtMenuName_" + i]))
                        {
                            if (menu.LanguageId == Convert.ToInt32(Request["ddlLanguages_" + i]))
                            {
                                ModelState.AddModelError(string.Empty, "Language chosen for menu and menu translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        for (var j = i + 1; j < TranslationListCount; j++)
                        {
                            if (Convert.ToInt32(Request["ddlLanguages_" + i]) == Convert.ToInt32(Request["ddlLanguages_" + j]))
                            {
                                ModelState.AddModelError(string.Empty, "language chosen in menu translation shouldn't be same");
                                var errors = GetErrorsFromModelState();
                                return Json(new { success = false, errors = errors });
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request["txtMenuName_" + i]))//&& !string.IsNullOrWhiteSpace(Request["txtMenuDesc_" + i]
                        {
                            RestaurantMenuTranslation eachMenuTranslation = new RestaurantMenuTranslation()
                            {
                                Id = Convert.ToInt32(Request["hdnTranslationId_" + i]),
                                RestaurantMenuId = menu.Id,
                                LanguageId = Convert.ToInt32(Request["ddlLanguages_" + i]),
                                TranslatedMenuName = Request["txtMenuName_" + i],
                                TranslatedMenuDescription = Request["txtMenuDesc_" + i]
                            };
                            menuTranslationList.Add(eachMenuTranslation);
                        }

                    }
                    _restaurantService.ModifyRestaurantMenu(menu, menuTranslationList);
                    return Json(new { success = true });
                }
                else
                {
                    var errors = GetErrorsFromModelState();
                    return Json(new { success = false, errors = errors });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }

        /// <summary>
        /// Deletes the restaurant menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenu.</returns>
        public ActionResult DeleteRestaurantMenu(int id)
        {
            try
            {
                var restaurantId = GetActiveRestaurantId();
                _restaurantService.DeleteRestaurantMenu(id, restaurantId);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This menu already in use." });
            }
        }

        /// <summary>
        /// Deletes the restaurant menu translation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete RestaurantMenuTranslation.</returns>
        [HttpPost]
        public ActionResult DeleteRestaurantMenuTranslation(int id)
        {
            try
            {
                _restaurantService.DeleteRestaurantMenuTranslation(id);
                return PartialView("Success");
            }
            catch
            {
                return View("EditRestaurantMenu");
            }
        }

        //Login restaurant order history
        /// <summary>
        /// Restaurants the order history.
        /// </summary>
        /// <returns>ActionResult RestaurantOrder History.</returns>
        public ActionResult RestaurantOrderHistory()
        {
            return View();
        }

        /// <summary>
        /// Loads the rest order history.
        /// </summary>
        /// <returns>ActionResult Load RestOrderHistory.</returns>
        [HttpPost]
        public ActionResult LoadRestOrderHistory()
        {
            var restaurantId = GetActiveRestaurantId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var restOrderHistory = _restaurantService.GetRestaurantOrderHistory(fromDate, toDate, restaurantId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = restOrderHistory.Select(b => new
            {
                RestOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.ScheduledDate.ToString(),
                Amount = b.OrderTotal,
                TableNumber = b.CustomerTableNumber != null ? b.RestaurantTable.TableNumber : 0,
                OrderStatus = ((Status_Type)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString(),
                TipsAmount = b.RestaurantTips.Select(i => i.TipAmount).FirstOrDefault(),
                TipsPaid = b.RestaurantTips.Select(i => i.PaymentStatus).FirstOrDefault(),
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the rest order item details.
        /// </summary>
        /// <param name="OrderId">The order identifier.</param>
        /// <returns>ActionResult Load RestOrderItem Details.</returns>
        public ActionResult LoadRestOrderItemDetails(int OrderId)
        {
            ViewBag.OrdersQueue = false;
            var orderItemDetails = _restaurantService.GetRestOrderItemDetails(OrderId);
            return PartialView("_RestOrderItemDetails", orderItemDetails);
        }

        /// <summary>
        /// Loads the rest orders queue item details.
        /// </summary>
        /// <param name="OrderId">The order identifier.</param>
        /// <returns>ActionResult Load RestOrdersQueue ItemDetails.</returns>
        public ActionResult LoadRestOrdersQueueItemDetails(int OrderId)
        {
            ViewBag.OrdersQueue = true;
            var orderItemDetails = _restaurantService.GetRestOrderItemDetails(OrderId);
            return PartialView("_RestOrderItemDetails", orderItemDetails);
        }

        /// <summary>
        /// Updates the order item served status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update OrderItemServed Status.</returns>
        [HttpPost]
        public ActionResult UpdateOrderItemServedStatus(int id)
        {
            try
            {
                _restaurantService.UpdateRestOrderItemServedStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }


        /// <summary>
        /// Financials this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Financial()
        {
            return View();
        }
        /// <summary>
        /// Todayses the order details.
        /// </summary>
        /// <returns>JsonResult Todays OrderDetails.</returns>
        public JsonResult TodaysOrderDetails()
        {
            var restaurantId = GetActiveRestaurantId();
            var orders = _restaurantService.GetTodaysOrders(restaurantId);
            var hourlyOrders = orders.GroupBy(r => r.CreationDate.Hour)
               .Select(
                  g => new
                  {
                      Key = g.Key,
                      TotalOrders = g.Count(),
                      Amount = g.Sum(o => o.RestaurantOrderItems.Sum(i => i.RestaurantMenuItem.Price * i.Qty))
                  }).OrderBy(or => or.Key);

            var ret = new[]
        {  
            new { label="Hours", data = hourlyOrders.Select(x=>new int[]{ x.Key})},
            new { label="Amount", data = hourlyOrders.Select(x=>new int[]{ Convert.ToInt32(x.Amount)})},
            new { label="Orders", data = hourlyOrders.Select(x=>new int[]{ Convert.ToInt32(x.TotalOrders)})},
        };
            return Json(ret, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Monthlies the order details.
        /// </summary>
        /// <returns>JsonResult MonthlyOrderDetails.</returns>
        public JsonResult MonthlyOrderDetails()
        {
            var restaurantId = GetActiveRestaurantId();
            var monthlyOrders = _restaurantService.GetLastTwelveMonthsOrders(restaurantId);
            var ret = new[]
        {  
            new { label="Month", data = monthlyOrders.Select(x=>new int[]{ x.Month})},
            new { label="Amount", data = monthlyOrders.Select(x=>new int[]{ Convert.ToInt32(x.TotalAmount)})},
        };
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Items the ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult ItemIngredients.</returns>
        [HttpGet]
        public ActionResult ItemIngredients(int id)
        {
            ViewBag.itemId = id;
            var ingredient = _restaurantService.GetItemIngredient(id);
            return PartialView("_MenuItemIngredients", ingredient);
        }

        /// <summary>
        /// Manages the item ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage ItemIngredients.</returns>
        public ActionResult ManageItemIngredients(int id)
        {
            ViewBag.itemId = id;
            return View();
        }

        /// <summary>
        /// Loads the menu item ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load MenuItemIngredients.</returns>
        [HttpPost]
        public ActionResult LoadMenuItemIngredients(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var menuItemIngredients = _restaurantService.GetMenuItemIngredients(id, skip, pageSize, out Total);
            var data = menuItemIngredients.Select(b => new
            {
                MenuItemIngredientId = b.Id,
                Name = b.Ingredient.Name,
                Description = b.Ingredient.Description,
                Weight = b.Weight
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the item ingredient.
        /// </summary>
        /// <param name="restMenuItemId">The rest menu item identifier.</param>
        /// <returns>ActionResult Add ItemIngredient.</returns>
        public ActionResult AddItemIngredient(int restMenuItemId)
        {
            MenuItemIngredient ingredient = new MenuItemIngredient()
            {
                RestaurantMenuItemId = restMenuItemId
            };
            ViewBag.ingredient = _restaurantService.GetAllUnAssignedIngredient(restMenuItemId);
            return View(ingredient);
        }

        /// <summary>
        /// Adds the edit item ingredient.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>ActionResult AddEdit ItemIngredient.</returns>
        [HttpPost]
        public ActionResult AddEditItemIngredient(MenuItemIngredient data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (data.Id == 0)
                    {
                        List<MenuItemIngredientNutrition> list = new List<MenuItemIngredientNutrition>();
                        var ingredientNutrition = _restaurantService.GetIngredientNutritionValues(data.IngredientId);
                        foreach (var nutritionvalue in ingredientNutrition)
                        {
                            MenuItemIngredientNutrition newMMenuItemIngredientNutrition = new MenuItemIngredientNutrition()
                            {
                                NutritionValue = System.Math.Round(data.Weight * Convert.ToDouble(nutritionvalue.NutritionValue == "Tr" ? 0 : Convert.ToDouble(nutritionvalue.NutritionValue)) / nutritionvalue.WeightInGrams, 2),
                                NutritionId = nutritionvalue.NutritionId
                            };
                            list.Add(newMMenuItemIngredientNutrition);

                        }
                        data.MenuItemIngredientNutrition = list;
                        var result = _restaurantService.AddItemIngredient(data);
                        return RedirectToAction("ManageItemIngredients", "Restaurant", new { area = "Admin", id = result.RestaurantMenuItemId });
                    }
                    else
                    {
                        var menuItemIngredientNutrition = _restaurantService.GetMenuItemIngredientNutrition(data.Id);
                        data.MenuItemIngredientNutrition = menuItemIngredientNutrition.ToList();
                        var ingredientNutrition = _restaurantService.GetIngredientNutritionValues(data.IngredientId);

                        foreach (var exisNutritionvalues in data.MenuItemIngredientNutrition)
                        {
                            foreach (var nutritionvalue in ingredientNutrition)
                            {
                                if (exisNutritionvalues.MenuItemIngredient.IngredientId == nutritionvalue.IngredientId && exisNutritionvalues.NutritionId == nutritionvalue.NutritionId)
                                {
                                    exisNutritionvalues.NutritionValue = System.Math.Round(data.Weight * Convert.ToDouble(nutritionvalue.NutritionValue == "Tr" ? 0 : Convert.ToDouble(nutritionvalue.NutritionValue)) / nutritionvalue.WeightInGrams, 2);
                                    _restaurantService.UpdateMenuItemIngredientNutrition(exisNutritionvalues);
                                }
                            }
                        }
                        var result = _restaurantService.UpdateItemIngredient(data);
                        return RedirectToAction("ManageItemIngredients", "Restaurant", new { area = "Admin", id = result.RestaurantMenuItemId });
                    }
                }
                else
                {
                    ViewBag.ingredient = _restaurantService.GetAllUnAssignedIngredient(data.RestaurantMenuItemId);
                    return View("AddItemIngredient", data);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.ingredient = _restaurantService.GetAllUnAssignedIngredient(data.RestaurantMenuItemId);
                return View("AddItemIngredient", data);
            }
        }

        /// <summary>
        /// Edits the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ItemIngredient.</returns>
        public ActionResult EditItemIngredient(int id)
        {
            var itemIngredient = _restaurantService.GetIngredientById(id);
            return View("AddItemIngredient", itemIngredient);
        }

        /// <summary>
        /// Deletes the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ItemIngredient.</returns>
        [HttpPost]
        public ActionResult DeleteItemIngredient(int id)
        {
            try
            {
                _restaurantService.DeleteItemIngredient(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This MenuItem Ingredient already in use." });
            }
        }

        /// <summary>
        /// Deletes the menu item ingredient nutrient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete MenuItemIngredientNutrient.</returns>
        [HttpPost]
        public ActionResult DeleteMenuItemIngredientNutrient(int id)
        {
            try
            {
                _restaurantService.DeleteMenuItemIngredientNutrient(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This Ingredient Nutrient already in use." });
            }
        }

        /// <summary>
        /// Loads the menu item ingredient nutrients.
        /// </summary>
        /// <param name="MenuItemIngredientId">The menu item ingredient identifier.</param>
        /// <returns>ActionResult Load MenuItemIngredientNutrients.</returns>
        public ActionResult LoadMenuItemIngredientNutrients(int MenuItemIngredientId)
        {
            var menuItemIngredientNutrientsDetails = _restaurantService.GetMenuItemIngredientNutrition(MenuItemIngredientId);
            return PartialView("_MenuItemIngredientNutrients", menuItemIngredientNutrientsDetails);
        }



        /// <summary>
        /// Items the sizes.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult ItemSizes.</returns>
        [HttpGet]
        public ActionResult ItemSizes(int id)
        {
            ViewBag.itemId = id;
            var itemsizes = _restaurantService.GetItemSize(id);
            return PartialView("_MenuItemSizes", itemsizes);
        }

        /// <summary>
        /// Manages the item sizes.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage ItemSizes.</returns>
        public ActionResult ManageItemSizes(int id)
        {
            ViewBag.itemId = id;
            var itemSizes = _restaurantService.GetItemSize(id);
            return View(itemSizes);
        }

        /// <summary>
        /// Adds the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Add ItemSize.</returns>
        public ActionResult AddItemSize(int id)
        {
            MenuItemSize itemSize = new MenuItemSize()
            {
                RestaurantMenuItemId = id
            };
            return View(itemSize);
        }

        /// <summary>
        /// Adds the size of the edit item.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        /// <returns>ActionResult AddEdit ItemSize.</returns>
        [HttpPost]
        public ActionResult AddEditItemSize(MenuItemSize itemSize)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (itemSize.IsDefault == true)
                    {
                        var items = _restaurantService.GetItemSize(itemSize.RestaurantMenuItemId);
                        MenuItemSize menuItemSize = new MenuItemSize();
                        foreach (var item in items)
                        {
                            if (item.IsDefault == true)
                            {
                                item.IsDefault = false;
                                menuItemSize = item;
                            }
                        }
                        if (menuItemSize.Id != 0)
                            _restaurantService.UpdateItemSize(menuItemSize);
                    }
                    if (itemSize.Id == 0)
                    {
                        var result = _restaurantService.AddItemSize(itemSize);
                        return RedirectToAction("ManageItemSizes", new { id = result.RestaurantMenuItemId });
                    }
                    else
                    {
                        var result = _restaurantService.UpdateItemSize(itemSize);
                        return RedirectToAction("ManageItemSizes", new { id = result.RestaurantMenuItemId });
                    }
                }
                else
                {
                    return View("AddItemSize", itemSize);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddItemSize", itemSize);
            }
        }

        /// <summary>
        /// Edits the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ItemSize.</returns>
        public ActionResult EditItemSize(int id)
        {
            var itemSize = _restaurantService.GetItemSizeById(id);
            return View("AddItemSize", itemSize);
        }

        /// <summary>
        /// Deletes the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ItemSize.</returns>
        [HttpPost]
        public ActionResult DeleteItemSize(int id)
        {
            var itemSize = _restaurantService.GetItemSizeById(id);
            _restaurantService.DeleteItemSize(id);
            return RedirectToAction("ManageItemSizes", new { id = itemSize.RestaurantMenuItemId });
        }

        /// <summary>
        /// Items the additionals.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult ItemAdditionals.</returns>
        [HttpGet]
        public ActionResult ItemAdditionals(int id)
        {
            ViewBag.itemId = id;
            var itemAdditionals = _restaurantService.GetItemAdditionals(id);
            return PartialView("_MenuItemAdditionals", itemAdditionals);
        }

        /// <summary>
        /// Updates the menu item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update MenuItemAdditional.</returns>
        [HttpPost]
        public ActionResult UpdateMenuItemAdditional(int id)
        {
            //ViewBag.itemId = id;
            //var itemAdditionals = _restaurantService.GetItemAdditionals(id);
            var itemAdditional = _restaurantService.UpdateMenuItemAdditional(id);
            return RedirectToAction("ManageItemAdditional", new { id = itemAdditional.RestaurantMenuItemId });

        }
        /// <summary>
        /// Manages the item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage ItemAdditional.</returns>
        public ActionResult ManageItemAdditional(int id)
        {
            ViewBag.itemId = id;
            var itemAdditionals = _restaurantService.GetItemAdditionals(id);
            return View(itemAdditionals);
        }

        /// <summary>
        /// Adds the item additionals.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>ActionResult Add ItemAdditionals.</returns>
        public ActionResult AddItemAdditionals(int menuItemId)
        {
            MenuItemAdditional itemAdditional = new MenuItemAdditional()
            {
                RestaurantMenuItemId = menuItemId
            };
            ViewBag.MenuAdditionalGroup = _restaurantService.GetAllMenuAdditionalGroup(GetActiveRestaurantId(), menuItemId);
            return View(itemAdditional);
        }

        /// <summary>
        /// Adds the edit item additional.
        /// </summary>
        /// <param name="itemAdditional">The item additional.</param>
        /// <returns>ActionResult AddEdit ItemAdditional .</returns>
        public ActionResult AddEditItemAdditional(MenuItemAdditional itemAdditional)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (itemAdditional.Id == 0)
                    {
                        _restaurantService.AddItemAdditional(itemAdditional);
                        return RedirectToAction("ManageItemAdditional", new { id = itemAdditional.RestaurantMenuItemId });
                    }
                    else
                    {
                        _restaurantService.ModifyItemAdditional(itemAdditional);
                        return RedirectToAction("ManageItemAdditional", new { id = itemAdditional.RestaurantMenuItemId });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    ViewBag.MenuAdditionalGroup = _restaurantService.GetAllMenuAdditionalGroup(GetActiveRestaurantId(), itemAdditional.RestaurantMenuItemId);
                    return View("AddItemAdditionals", itemAdditional);
                }
            }
            catch (Exception ex)
            {
                ViewBag.MenuAdditionalGroup = _restaurantService.GetAllMenuAdditionalGroup(GetActiveRestaurantId(), itemAdditional.RestaurantMenuItemId);
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddItemAdditionals", itemAdditional);
            }
        }

        /// <summary>
        /// Edits the item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ItemAdditional.</returns>
        public ActionResult EditItemAdditional(int id)
        {
            var itemAdditional = _restaurantService.GetItemAdditionalById(id);
            ViewBag.MenuAdditionalGroup = _restaurantService.GetAllMenuAdditionalGroup(GetActiveRestaurantId(), itemAdditional.RestaurantMenuItemId);

            return View("AddItemAdditionals", itemAdditional);
        }

        /// <summary>
        /// Deletes the item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ItemAdditional.</returns>
        [HttpPost]
        public ActionResult DeleteItemAdditional(int id)
        {
            var itemAdditional = _restaurantService.GetItemAdditionalById(id);
            _restaurantService.DeleteItemAdditional(id);
            return RedirectToAction("ManageItemAdditional", new { id = itemAdditional.RestaurantMenuItemId });
        }

        /// <summary>
        /// Items the images.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult ItemImages.</returns>
        [HttpGet]
        public ActionResult ItemImages(int id)
        {
            ViewBag.itemId = id;
            var itemImages = _restaurantService.GetItemImages(id);
            return PartialView("_MenuItemImages", itemImages);
        }

        /// <summary>
        /// Manages the item images.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage ItemImages.</returns>
        public ActionResult ManageItemImages(int id)
        {
            ViewBag.itemId = id;
            var itemImages = _restaurantService.GetItemImages(id);
            return View(itemImages);
        }

        /// <summary>
        /// Adds the item image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Add ItemImage.</returns>
        public ActionResult AddItemImage(int id)
        {
            RestaurantMenuItemImage itemImages = new RestaurantMenuItemImage()
            {
                RestaurantMenuItemId = id
            };
            return View(itemImages);
        }

        /// <summary>
        /// Adds the item image.
        /// </summary>
        /// <param name="itemImage">The item image.</param>
        /// <param name="uploaditemImage">The uploaditem image.</param>
        /// <returns>ActionResult Add ItemImage.</returns>
        [HttpPost]
        public ActionResult AddItemImage(RestaurantMenuItemImage itemImage, HttpPostedFileBase uploaditemImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploaditemImage != null)
                    {
                        if (uploaditemImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploaditemImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploaditemImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                itemImage.Image = filename;
                                ImageHelper.UploadImage(itemImage.RestaurantMenuItemId, ImagePathFor.MenuItemBackground, uploaditemImage, api_path);

                                var itemImages = _restaurantService.GetItemImages(itemImage.RestaurantMenuItemId).Where(i => i.IsPrimary == true).FirstOrDefault();
                                if (itemImages != null)
                                {
                                    if (itemImage.IsPrimary == true)
                                    {
                                        itemImages.IsPrimary = false;
                                        _restaurantService.UpdateItemImages(itemImages);
                                    }
                                }
                                else
                                {
                                    itemImage.IsPrimary = true;
                                }

                                _restaurantService.AddItemImages(itemImage);
                                return RedirectToAction("ManageItemImages", new { id = itemImage.Id });
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddItemImage", itemImage);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                            return View("AddItemImage", itemImage);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", " Please select proper Image");
                        return View("AddItemImage", itemImage);
                    }
                }
                else
                {
                    return View("AddItemImage", itemImage);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddItemImage", itemImage);
            }
        }

        /// <summary>
        /// Deletes the item image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ItemImage.</returns>
        [HttpPost]
        public ActionResult DeleteItemImage(int id)
        {
            var itemImage = _restaurantService.GetItemImageByImageId(id);
            var api_path = ConfigurationManager.AppSettings["APIURL"];

            if (!string.IsNullOrWhiteSpace(itemImage.Image))
            {
                string imgPath = @"\Images\MenuItem\" + itemImage.RestaurantMenuItemId + @"\" + itemImage.Image;
                ImageHelper.DeleteImage(api_path, imgPath);
            }

            _restaurantService.DeleteItemImage(itemImage.Id);
            if (itemImage.IsPrimary == true)
            {
                var defaultImage = _restaurantService.GetItemImages(itemImage.RestaurantMenuItemId).FirstOrDefault();
                if (defaultImage != null)
                {
                    defaultImage.IsPrimary = true;
                    _restaurantService.UpdateItemImages(defaultImage);
                }
            }
            return Json(new { status = true });
        }

        /// <summary>
        /// Updates the item image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update ItemImage Active.</returns>
        [HttpPost]
        public ActionResult UpdateItemImageActive(int id)
        {
            try
            {
                var itemImage = _restaurantService.UpdateItemImageActive(id);
                return RedirectToAction("ManageItemImages", "Restaurant", new { id = itemImage.RestaurantMenuItemId });
            }
            catch
            {
                return View("ManageItemImages");
            }
        }

        /// <summary>
        /// Updates the item image primary.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update ItemImage Primary.</returns>
        [HttpPost]
        public ActionResult UpdateItemImagePrimary(int id)
        {
            try
            {
                var itemImage = _restaurantService.GetItemImageByImageId(id);

                var itemImages = _restaurantService.GetItemImages(itemImage.RestaurantMenuItemId).Where(i => i.IsPrimary == true).FirstOrDefault();
                if (itemImages != null && itemImages.Id != itemImage.Id)
                {
                    itemImages.IsPrimary = false;
                    _restaurantService.UpdateItemImages(itemImages);

                    if (itemImage.IsPrimary == true)
                        itemImage.IsPrimary = false;
                    else itemImage.IsPrimary = true;

                    _restaurantService.UpdateItemImages(itemImage);
                    return RedirectToAction("ManageItemImages", "Restaurant", new { id = itemImage.RestaurantMenuItemId });
                }
                else
                {
                    return Json(new { failure = true, error = "At least one image must be primary." });
                }
            }
            catch
            {
                return View("ManageItemImages");
            }
        }


        // Manage Menu Item Suggestions

        /// <summary>
        /// Manages the menu item suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage MenuItemSuggestion.</returns>
        public ActionResult ManageMenuItemSuggestion(int id)
        {
            ViewBag.itemId = id;
            return View();
        }

        /// <summary>
        /// Loads the menu item suggestions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load MenuItemSuggestions.</returns>
        [HttpPost]
        public ActionResult LoadMenuItemSuggestions(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var meuItemSuggestion = _restaurantService.GetRestaurantMenuItemSuggestions(id, skip, pageSize, out Total);
            var data = meuItemSuggestion.Select(b => new
            {
                SuggestionId = b.Id,
                MenuName = b.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.MenuName,
                MenuGroupName = b.RestaurantMenuItem.RestaurantMenuGroup.GroupName,
                MenuItemName = b.RestaurantMenuItem.ItemName,
                Description = b.RestaurantMenuItem.ItemDescription,
                Price = b.RestaurantMenuItem.Price,
                IsActive = b.RestaurantMenuItem.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the menu item suggestion.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>ActionResult Add MenuItemSuggestion.</returns>
        public ActionResult AddMenuItemSuggestion(int menuItemId)
        {
            MenuItemSuggestion menuItemSuggestion = new MenuItemSuggestion();
            menuItemSuggestion.RestaurantMenuItemId = menuItemId;
            var unassignMenuItem = _restaurantService.GetUnassignMenuItemSuggetions(menuItemId, GetActiveRestaurantId()).Select(
                r => new
                {
                    menuItemSuggestionId = r.Id,
                    Name = r.ItemName
                }).ToList();
            ViewBag.MenuItemSuggestions = new MultiSelectList(unassignMenuItem, "menuItemSuggestionId", "Name");
            return View(menuItemSuggestion);
        }

        /// <summary>
        /// Saves the menu item suggestion.
        /// </summary>
        /// <param name="menuItemSuggestion">The menu item suggestion.</param>
        /// <returns>ActionResult Save MenuItemSuggestion.</returns>
        [HttpPost]
        public ActionResult SaveMenuItemSuggestion(MenuItemSuggestion menuItemSuggestion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<MenuItemSuggestion> MenuItemSuggestionList = new List<MenuItemSuggestion>();

                    var MenuItemSuggestionIds = Request.Form["menuItemSuggestionId"];

                    if (MenuItemSuggestionIds != null)
                    {
                        string[] itemIds = MenuItemSuggestionIds.Split(',');
                        foreach (var itemId in itemIds)
                        {
                            MenuItemSuggestion itemSuggestion = new MenuItemSuggestion()
                            {
                                RestaurantMenuItemId = menuItemSuggestion.RestaurantMenuItemId,
                                menuItemSuggestionId = Convert.ToInt32(itemId)
                            };
                            MenuItemSuggestionList.Add(itemSuggestion);
                        }
                    }

                    if (menuItemSuggestion.Id == 0)
                        _restaurantService.CreateMenuItemSuggestion(MenuItemSuggestionList);
                    return RedirectToAction("ManageMenuItemSuggestion", "Restaurant", new { area = "admin", id = menuItemSuggestion.RestaurantMenuItemId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignMenuItem = _restaurantService.GetUnassignMenuItemSuggetions(menuItemSuggestion.RestaurantMenuItemId, GetActiveRestaurantId()).Select(
                               r => new
                               {
                                   menuItemSuggestionId = r.Id,
                                   Name = r.ItemName
                               }).ToList();
                    ViewBag.MenuItemSuggestions = new MultiSelectList(unassignMenuItem, "menuItemSuggestionId", "Name");
                    return View("AddMenuItemSuggestion", menuItemSuggestion);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignMenuItem = _restaurantService.GetUnassignMenuItemSuggetions(menuItemSuggestion.RestaurantMenuItemId, GetActiveRestaurantId()).Select(
                               r => new
                               {
                                   menuItemSuggestionId = r.Id,
                                   Name = r.ItemName
                               }).ToList();
                ViewBag.MenuItemSuggestions = new MultiSelectList(unassignMenuItem, "menuItemSuggestionId", "Name");
                return View("AddMenuItemSuggestion", menuItemSuggestion);
            }
        }

        /// <summary>
        /// Deletes the menu item suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete MenuItemSuggestion.</returns>
        [HttpPost]
        public ActionResult DeleteMenuItemSuggestion(int id)
        {
            try
            {
                _restaurantService.DeleteMenuItemSuggestion(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This MenuItem Suggestion already in use." });
            }
        }


        //Recent orders of all restaurants for authorized user
        /// <summary>
        /// Gets all recent orders.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="page">The page.</param>
        /// <returns>ActionResult Get AllRecentOrders.</returns>
        public ActionResult GetAllRecentOrders(string searchText, int? restaurantId, int? page)
        {
            ViewBag.SearchText = searchText;
            ViewBag.RestaurantId = restaurantId;
            var customer = GetAuthenticatedCustomer();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int total;
            var recentOrders = _restaurantService.GetAllRecentOrdersOfAuthRestaurants(customer.Id, pageNumber, pageSize, restaurantId, out total);
            if (restaurantId != null && page == null)
                return PartialView("_AllRecentOrdersResult", new StaticPagedList<RestaurantOrder>(recentOrders.ToList(), pageNumber, pageSize, total));
            else

                return View("AllRecentOrdersOfAuthRestaurants", new StaticPagedList<RestaurantOrder>(recentOrders.ToList(), pageNumber, pageSize, total));

        }

        /// <summary>
        /// Restaurants the automatic complete.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>JsonResult Restaurant AutoComplete.</returns>
        [HttpPost]
        public JsonResult RestaurantAutoComplete(string prefix)
        {
            var customer = GetAuthenticatedCustomer();
            var restaurants = _restaurantService.GetAllRestaurantsByNameOfOwner(customer.Id, prefix).Select(
                r => new
                {
                    Id = r.Id,
                    Name = r.Name
                });

            return Json(restaurants, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets all reviews.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="page">The page.</param>
        /// <returns>ActionResult Get AllReviews.</returns>
        public ActionResult GetAllReviews(string searchText, int? restaurantId, int? page)
        {
            ViewBag.SearchText = searchText;
            ViewBag.RestaurantId = restaurantId;
            var customer = GetAuthenticatedCustomer();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int total;
            var reviews = _restaurantService.GetAllReviewsOfAuthRestaurants(customer.Id, pageNumber, pageSize, restaurantId, out total);
            if (restaurantId != null && page == null)
                return PartialView("_AllRestaurantReviews", new StaticPagedList<RestaurantReview>(reviews.ToList(), pageNumber, pageSize, total));
            else

                return View("AllReviewsOfAuthRestaurants", new StaticPagedList<RestaurantReview>(reviews.ToList(), pageNumber, pageSize, total));

        }

        /// <summary>
        /// Gets the rest recent orders.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>ActionResult Get Restaurant Recent Orders.</returns>
        public ActionResult GetRestRecentOrders(int? page)
        {
            var customer = GetAuthenticatedCustomer();
            var restaurantId = GetActiveRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int total;
            var recentOrders = _restaurantService.GetAllRecentOrdersOfAuthRestaurants(customer.Id, pageNumber, pageSize, restaurantId, out total);

            return View("AllRecentOrdersOfAuthRestaurants", new StaticPagedList<RestaurantOrder>(recentOrders.ToList(), pageNumber, pageSize, total));

        }

        #region Restaurant Receptionist

        /// <summary>
        /// Receptions the dashboard.
        /// </summary>
        /// <returns>ActionResult Restaurant Reception Dashboard.</returns>
        public ActionResult ReceptionDashboard()
        {
            return View();
        }
        #endregion Restaurant Receptionist

    }
}