﻿// ***********************************************************************
// Assembly         : ToOrdr.web
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-07-2019
// ***********************************************************************
// <copyright file="HotelExcursionController.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Web.Controllers;

namespace ToOrdr.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Class HotelExcursionController.
    /// Implements the <see cref="ToOrdr.Web.Controllers.BaseController" />
    /// </summary>
    /// <seealso cref="ToOrdr.Web.Controllers.BaseController" />
    public class HotelExcursionController : BaseController
    {
        /// <summary>
        /// The hotel service
        /// </summary>
        IHotelService _hotelService;
        /// <summary>
        /// The payment service
        /// </summary>
        IPaymentService _paymentService;
        /// <summary>
        /// Initializes a new instance of the <see cref="HotelExcursionController"/> class.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <param name="paymentService">The payment service.</param>
        public HotelExcursionController(IHotelService hotelService, IPaymentService paymentService)
        {
            _hotelService = hotelService;
            _paymentService = paymentService;
        }
        // GET: Admin/HotelExcursion
        /// <summary>
        /// Manages the excursion.
        /// </summary>
        /// <returns>ActionResult Manage Excursion.</returns>
        public ActionResult ManageExcursion()
        {
            return View();
        }
        /// <summary>
        /// Loads the excursion.
        /// </summary>
        /// <returns>ActionResult Load Excursion.</returns>
        [HttpPost]
        public ActionResult LoadExcursion()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excursions = _hotelService.GetExcursions(skip, pageSize, out Total);
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];

            var data = excursions.Select(b => new
            {
                ExcursionId = b.Id,
                ExcursionName = b.Name,
                ExcursionDescription = b.Description,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the excursion.
        /// </summary>
        /// <returns>ActionResult Add Excursion.</returns>
        public ActionResult AddExcursion()
        {
            Excursion hotelExcursion = new Excursion();
            return View("AddEditExcursion", hotelExcursion);
        }

        /// <summary>
        /// Saves the excursion.
        /// </summary>
        /// <param name="excursion">The excursion.</param>
        /// <param name="uploadFile">The upload file.</param>
        /// <returns>ActionResult Save new created Excursion.</returns>
        public ActionResult SaveExcursion(Excursion excursion, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (excursion.Id == 0)
                    {

                        var excursionResult = _hotelService.CreateExcursion(excursion);
                        return RedirectToAction("ManageExcursion");
                    }
                    else
                    {
                        var res = _hotelService.ModifyExcursion(excursion);
                        return RedirectToAction("ManageExcursion");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    return View("AddEditExcursion", excursion);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditExcursion", excursion);
            }
        }

        /// <summary>
        /// Edits the excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit Excursion.</returns>
        public ActionResult EditExcursion(int id)
        {
            var escursionService = _hotelService.GetExcursionById(id);
            return View("AddEditExcursion", escursionService);
        }

        /// <summary>
        /// Deletes the excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete Excursion.</returns>
        [HttpPost]
        public ActionResult DeleteExcursion(int id)
        {
            try
            {
                _hotelService.DeleteExcursion(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This excursion service cannot be deleted, bacause it is already in use" });
            }
        }


        /// <summary>
        /// Manages the hotel excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage HotelExcursion.</returns>
        public ActionResult ManageHotelExcursion(int id)
        {
            ViewBag.HotelServiceId = id;
            return View();
        }

        /// <summary>
        /// Loads the hotel excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HotelExcursion.</returns>
        [HttpPost]
        public ActionResult LoadHotelExcursion(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var hotelExcursion = _hotelService.GetAllHotelExcursions(id, skip, pageSize, out Total);
            var data = hotelExcursion.Select(b => new
            {
                HotelExcursionId = b.Id,
                HotelServiceName = b.HotelServices.Services.ServiceName,
                ExcursionName = b.Excursion.Name,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the hotel excursion.
        /// </summary>
        /// <param name="HotelServiceId">The hotel service identifier.</param>
        /// <returns>ActionResult Add HotelExcursion.</returns>
        public ActionResult AddHotelExcursion(int HotelServiceId)
        {
            HotelExcursion hotelExcursion = new HotelExcursion();
            hotelExcursion.HotelServiceId = HotelServiceId;
            var unassignExcursion = _hotelService.GetUnassignExcursion(HotelServiceId).Select(
                 r => new
                 {
                     ExcursionId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.Excursions = new MultiSelectList(unassignExcursion, "ExcursionId", "Name");
            return View(hotelExcursion);
        }


        /// <summary>
        /// Saves the hotel excursion.
        /// </summary>
        /// <param name="hotelExcursion">The hotel excursion.</param>
        /// <returns>ActionResult Save HotelExcursion.</returns>
        [HttpPost]
        public ActionResult SaveHotelExcursion(HotelExcursion hotelExcursion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<HotelExcursion> listHotelExcursion = new List<HotelExcursion>();

                    var ExcursionIds = Request.Form["ExcursionId"];

                    if (ExcursionIds != null)
                    {
                        string[] listExcursionIds = ExcursionIds.Split(',');
                        foreach (var eachExcId in listExcursionIds)
                        {
                            HotelExcursion eachHotelExcursion = new HotelExcursion()
                            {
                                HotelServiceId = hotelExcursion.HotelServiceId,
                                ExcursionId = Convert.ToInt32(eachExcId)
                            };
                            listHotelExcursion.Add(eachHotelExcursion);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Please select excursions to be added");
                        var unassignExcursion = _hotelService.GetUnassignExcursion(hotelExcursion.HotelServiceId).Select(
                                                  r => new
                                                  {
                                                      ExcursionId = r.Id,
                                                      Name = r.Name
                                                  }).ToList();
                        ViewBag.Excursions = new MultiSelectList(unassignExcursion, "ExcursionId", "Name");
                        return View("AddHotelExcursion", hotelExcursion);
                    }

                    _hotelService.CreateHotelExcursion(listHotelExcursion);

                    return RedirectToAction("ManageHotelExcursion", "HotelExcursion", new { area = "admin", id = hotelExcursion.HotelServiceId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "");
                    var unassignExcursion = _hotelService.GetUnassignExcursion(hotelExcursion.HotelServiceId).Select(
                                              r => new
                                              {
                                                  ExcursionId = r.Id,
                                                  Name = r.Name
                                              }).ToList();
                    ViewBag.Excursions = new MultiSelectList(unassignExcursion, "ExcursionId", "Name");
                    return View("AddHotelExcursion", hotelExcursion);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignExcursion = _hotelService.GetUnassignExcursion(hotelExcursion.HotelServiceId).Select(
                                          r => new
                                          {
                                              ExcursionId = r.Id,
                                              Name = r.Name
                                          }).ToList();
                ViewBag.Excursions = new MultiSelectList(unassignExcursion, "ExcursionId", "Name");
                return View("AddHotelExcursion", hotelExcursion);
            }
        }
        /// <summary>
        /// Deletes the hotel excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HotelExcursion.</returns>
        [HttpPost]
        public ActionResult DeleteHotelExcursion(int id)
        {
            try
            {
                _hotelService.DeleteHotelExcursion(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This hotel excursion already in use." });
            }
        }
        /// <summary>
        /// Updates the hotel excursion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update HotelExcursion.</returns>
        [HttpPost]
        public ActionResult UpdateHotelExcursion(int id)
        {
            try
            {
                _hotelService.ModifyHotelExcursionIsActiveStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "Hotel Excursion IsActive status not updated" });
            }
        }

        #region Hotel Excursion Details
        /// <summary>
        /// Manages the hotel excursion details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage HotelExcursionDetails.</returns>
        public ActionResult ManageHotelExcursionDetails(int id)
        {
            var hotelExcursion = _hotelService.GetHotelExcursionById(id);
            ViewBag.HotelExcursionId = id;
            ViewBag.HotelServiceId = hotelExcursion.HotelServices.Id;
            return View();
        }

        /// <summary>
        /// Loads the hotel excursion details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load HotelExcursionDetails.</returns>
        [HttpPost]
        public ActionResult LoadHotelExcursionDetails(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var hotelExcursionDet = _hotelService.GetAllExcursionDetails(id, skip, pageSize, out Total);
            var data = hotelExcursionDet.Select(b => new
            {
                HotelExcursionDetId = b.Id,
                HotelExcursionName = b.HotelExcursion.Excursion.Name,
                ExcursionDetName = b.Name,
                ExcursionDetDescription = b.Description,
                DurationInDays = b.DurationInDays,
                DurationHours = b.DurationHours != null ? b.DurationHours.ToString() : "",
                WeekDay = ((WeekDays)b.WeekDay).ToString(),
                StartTime = b.StartTime.ToString(),
                Price = b.Price,
                IsActive = b.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the hotel excursion detail.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <returns>ActionResult Add HotelExcursionDetail.</returns>
        public ActionResult AddHotelExcursionDetail(int hotelExcursionId)
        {
            ExcursionDetail hotelExcursionDet = new ExcursionDetail();
            hotelExcursionDet.HotelExcursionId = hotelExcursionId;
            ViewBag.WeedkDaysList = from WeekDays e in Enum.GetValues(typeof(WeekDays)) select new { Id = (int)e, Name = e.ToString() };
            return View("AddEditHotelExcursionDetail", hotelExcursionDet);

        }

        /// <summary>
        /// Saves the hotel excursion detail.
        /// </summary>
        /// <param name="excursionDetail">The excursion detail.</param>
        /// <returns>ActionResult Save HotelExcursionDetail.</returns>
        public ActionResult SaveHotelExcursionDetail(ExcursionDetail excursionDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (excursionDetail.Id == 0)
                    {
                        var excursionDetResult = _hotelService.CreateExcursionDetail(excursionDetail);
                        return RedirectToAction("ManageHotelExcursionDetails", new { id = excursionDetail.HotelExcursionId });
                    }
                    else
                    {
                        var res = _hotelService.ModifyExcursionDetail(excursionDetail);
                        return RedirectToAction("ManageHotelExcursionDetails", new { id = excursionDetail.HotelExcursionId });
                    }
                }
                else
                {
                    ViewBag.WeedkDaysList = from WeekDays e in Enum.GetValues(typeof(WeekDays)) select new { Id = (int)e, Name = e.ToString() };
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    return View("AddEditHotelExcursionDetail", excursionDetail);
                }
            }
            catch (Exception ex)
            {
                ViewBag.WeedkDaysList = from WeekDays e in Enum.GetValues(typeof(WeekDays)) select new { Id = (int)e, Name = e.ToString() };
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditHotelExcursionDetail", excursionDetail);
            }
        }
        /// <summary>
        /// Edits the hotel excursion detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit HotelExcursionDetail.</returns>
        public ActionResult EditHotelExcursionDetail(int id)
        {
            var excursionDetail = _hotelService.GetExcursionDetailById(id);
            ViewBag.WeedkDaysList = from WeekDays e in Enum.GetValues(typeof(WeekDays)) select new { Id = (int)e, Name = e.ToString() };
            return View("AddEditHotelExcursionDetail", excursionDetail);
        }
        /// <summary>
        /// Deletes the hotel excursion detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete HotelExcursionDetail.</returns>
        public ActionResult DeleteHotelExcursionDetail(int id)
        {
            try
            {
                _hotelService.DeleteExcursionDetail(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This hotel excursion detail already in use." });
            }
        }
        #endregion Hotel Excursion Details

        #region Excursion Detail Images
        /// <summary>
        /// Manages the excursion detail images.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <returns>ActionResult Manage ExcursionDetailImages.</returns>
        public ActionResult ManageExcursionDetailImages(int excursionDetId)
        {
            var excursionDetail = _hotelService.GetExcursionDetailById(excursionDetId);
            ViewBag.HotelExcursionId = excursionDetail.HotelExcursionId;
            ViewBag.ExcursionDetailId = excursionDetId;
            var excDetailsImages = _hotelService.GetExcursionDetailImages(excursionDetId);
            return View(excDetailsImages);
        }

        /// <summary>
        /// Adds the excursion detail images.
        /// </summary>
        /// <param name="excDetId">The exc det identifier.</param>
        /// <returns>ActionResult Add ExcursionDetailImages.</returns>
        public ActionResult AddExcursionDetailImages(int excDetId)
        {
            ExcursionDetailImages excDetImages = new ExcursionDetailImages();
            excDetImages.ExcursionDetailId = excDetId;
            return View(excDetImages);
        }

        /// <summary>
        /// Saves the excursion detail images.
        /// </summary>
        /// <param name="excDetImages">The exc det images.</param>
        /// <param name="uploadExcursionDetImages">The upload excursion det images.</param>
        /// <returns>ActionResult Save ExcursionDetailImages.</returns>
        [HttpPost]
        public ActionResult SaveExcursionDetailImages(ExcursionDetailImages excDetImages, HttpPostedFileBase[] uploadExcursionDetImages)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    //iterating through multiple file collection   
                    var count = 1;
                    foreach (HttpPostedFileBase eachExcDetImage in uploadExcursionDetImages)
                    {
                        if (eachExcDetImage != null)
                        {
                            if (eachExcDetImage.ContentLength <= 2000000)
                            {
                                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                                var filename = Path.GetFileName(eachExcDetImage.FileName);
                                filename = filename.Replace(" ", "_");
                                var ext = Path.GetExtension(eachExcDetImage.FileName);
                                if (allowedExtensions.Contains(ext))
                                {
                                    if (count == 1)
                                    {
                                        var excDetailImages = _hotelService.GetExcursionDetailImages(excDetImages.ExcursionDetailId).Where(i => i.IsPrimary == true).FirstOrDefault();
                                        if (excDetailImages != null)
                                        {
                                            if (excDetailImages.IsPrimary == true)
                                            {
                                                if (excDetImages.IsPrimary)
                                                {
                                                    excDetailImages.IsPrimary = false;
                                                    _hotelService.UpdateExcursionDetImage(excDetailImages);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            excDetImages.IsPrimary = true;
                                        }
                                    }
                                    else
                                    {
                                        excDetImages.IsPrimary = false;
                                    }
                                    excDetImages.Image = filename;
                                    ImageHelper.UploadImage(excDetImages.ExcursionDetailId, ImagePathFor.ExcursionDetailImages, eachExcDetImage, api_path);
                                    _hotelService.AddExcursionDetailImage(excDetImages);
                                }
                                else
                                {
                                    ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                    return View("AddExcursionDetailImages", excDetImages);
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                                return View("AddExcursionDetailImages", excDetImages);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", " Please select proper Image");
                            return View("AddExcursionDetailImages", excDetImages);
                        }
                        count = count + 1;
                    }
                    return RedirectToAction("ManageExcursionDetailImages", new { excursionDetId = excDetImages.ExcursionDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    return View("AddExcursionDetailImages", excDetImages);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddExcursionDetailImages", excDetImages);
            }
        }

        /// <summary>
        /// Deletes the excursion detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionDetailImage.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionDetailImage(int id)
        {
            var excDetImage = _hotelService.GetExcursionDetailImageById(id);
            var api_path = ConfigurationManager.AppSettings["APIURL"];

            if (!string.IsNullOrWhiteSpace(excDetImage.Image))
            {
                string imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + excDetImage.ExcursionDetailId + @"\" + excDetImage.Image;
                ImageHelper.DeleteImage(api_path, imgPath);
            }
            if (excDetImage.IsPrimary == true)
            {
                var excDetailImg = _hotelService.GetExcursionDetailImages(excDetImage.ExcursionDetailId).Where(r => r.Id != id).FirstOrDefault();
                if (excDetailImg != null)
                {
                    excDetailImg.IsPrimary = true;
                    _hotelService.UpdateExcursionDetImage(excDetailImg);
                    _hotelService.DeleteExcursionDetailImage(id);
                    return Json(new { IsPrimaryDeleted = true });
                }
            }

            _hotelService.DeleteExcursionDetailImage(id);
            return Json(new { IsPrimaryDeleted = false });
        }

        /// <summary>
        /// Updates the excursion detail image primary.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update ExcursionDetailImage Primary.</returns>
        [HttpPost]
        public ActionResult UpdateExcursionDetailImagePrimary(int id)
        {
            try
            {
                var excDetImage = _hotelService.GetExcursionDetailImageById(id);

                var excDetailImg = _hotelService.GetExcursionDetailImages(excDetImage.ExcursionDetailId).Where(i => i.IsPrimary == true).FirstOrDefault();
                if (excDetailImg != null && excDetailImg.Id != excDetImage.Id)
                {
                    excDetailImg.IsPrimary = false;
                    _hotelService.UpdateExcursionDetImage(excDetailImg);
                    excDetImage.IsPrimary = true;

                    _hotelService.UpdateExcursionDetImage(excDetImage);
                    return RedirectToAction("ManageExcursionDetailImages", "HotelExcursion", new { excursionDetId = excDetImage.ExcursionDetailId });
                }
                else
                {
                    return Json(new { failure = true, error = "At least one image must be primary." });
                }


            }
            catch
            {
                return View("ManageExcursionDetailImages");
            }
        }

        /// <summary>
        /// Updates the excursion detail image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update ExcursionDetailImage Active.</returns>
        [HttpPost]
        public ActionResult UpdateExcursionDetailImageActive(int id)
        {
            try
            {
                var excDetImage = _hotelService.GetExcursionDetailImageById(id);
                if (excDetImage.IsActive)
                    excDetImage.IsActive = false;
                else
                    excDetImage.IsActive = true;

                var updatedExcDetImg = _hotelService.UpdateExcursionDetImage(excDetImage);
                return RedirectToAction("ManageExcursionDetailImages", "HotelExcursion", new { excursionDetId = excDetImage.ExcursionDetailId });
            }
            catch
            {
                return View("ManageExcursionDetailImages");
            }
        }

        #endregion Excursion Detail Images

        #region Excursion Detail Gallery

        /// <summary>
        /// Manages the excursion detail gallery.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <returns>ActionResult Manage ExcursionDetailGallery.</returns>
        public ActionResult ManageExcursionDetailGallery(int excursionDetId)
        {
            var excursionDetail = _hotelService.GetExcursionDetailById(excursionDetId);
            ViewBag.HotelExcursionId = excursionDetail.HotelExcursionId;
            ViewBag.ExcursionDetailId = excursionDetId;
            var excDetailsGallery = _hotelService.GetExcursionDetailGallery(excursionDetId);
            return View(excDetailsGallery);
        }

        /// <summary>
        /// Adds the excursion detail gallery.
        /// </summary>
        /// <param name="excDetId">The exc det identifier.</param>
        /// <returns>ActionResult Add ExcursionDetailGallery.</returns>
        public ActionResult AddExcursionDetailGallery(int excDetId)
        {
            ExcursionDetailGallery excDetGallery = new ExcursionDetailGallery();
            excDetGallery.ExcursionDetailId = excDetId;
            return View(excDetGallery);
        }

        /// <summary>
        /// Saves the excursion detail gallery.
        /// </summary>
        /// <param name="excDetGallery">The exc det gallery.</param>
        /// <param name="uploadExcursionDetGalleryImages">The upload excursion det gallery images.</param>
        /// <param name="uploadExcursionDetGalleryVideo">The upload excursion det gallery video.</param>
        /// <returns>ActionResult Save ExcursionDetailGallery.</returns>
        [HttpPost]
        public ActionResult SaveExcursionDetailGallery(ExcursionDetailGallery excDetGallery, HttpPostedFileBase[] uploadExcursionDetGalleryImages, HttpPostedFileBase[] uploadExcursionDetGalleryVideo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadExcursionDetGalleryImages.Count(i => i != null) < 1 && uploadExcursionDetGalleryVideo.Count(v => v != null) < 1)
                    {
                        ModelState.AddModelError("", " Please select proper Image/Video");
                        return View("AddExcursionDetailGallery", excDetGallery);
                    }
                    else
                    {
                        //iterating through multiple file collection for images 
                        foreach (HttpPostedFileBase eachExcDetImage in uploadExcursionDetGalleryImages)
                        {
                            if (eachExcDetImage != null)
                            {
                                if (eachExcDetImage.ContentLength <= 2000000)
                                {
                                    excDetGallery.Image = Path.GetFileName(eachExcDetImage.FileName);
                                    var result = _hotelService.AddExcursionDetailGallery(excDetGallery);
                                    ImageHelper.UploadImage(excDetGallery.ExcursionDetailId, ImagePathFor.ExcursionDetailGallery, eachExcDetImage, api_path, result.Id);
                                }
                                else
                                {
                                    ModelState.AddModelError("", "Image " + Resources.ImageUploadSizeExceeded);
                                    return View("AddExcursionDetailGallery", excDetGallery);
                                }
                            }
                        }

                        //iterating through multiple file collection for videos 
                        foreach (HttpPostedFileBase eachExcDetVideo in uploadExcursionDetGalleryVideo)
                        {
                            if (eachExcDetVideo != null)
                            {
                                if (eachExcDetVideo.ContentLength <= 5000000)
                                {
                                    excDetGallery.Image = null;
                                    excDetGallery.Video = Path.GetFileName(eachExcDetVideo.FileName);
                                    var result = _hotelService.AddExcursionDetailGallery(excDetGallery);
                                    ImageHelper.UploadImage(excDetGallery.ExcursionDetailId, ImagePathFor.ExcursionDetailGallery, eachExcDetVideo, api_path, result.Id);
                                }
                                else
                                {
                                    ModelState.AddModelError("", "Video " + Resources.ImageUploadSizeExceeded);
                                    return View("AddExcursionDetailGallery", excDetGallery);
                                }
                            }
                        }
                    }
                    return RedirectToAction("ManageExcursionDetailGallery", new { excursionDetId = excDetGallery.ExcursionDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    return View("AddExcursionDetailGallery", excDetGallery);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddExcursionDetailGallery", excDetGallery);
            }
        }

        /// <summary>
        /// Updates the excursion detail gallery.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Update ExcursionDetailGallery.</returns>
        [HttpPost]
        public ActionResult UpdateExcursionDetailGallery(int id)
        {
            try
            {
                var excDetGallery = _hotelService.GetExcursionDetailGalleryById(id);
                if (excDetGallery.IsActive)
                    excDetGallery.IsActive = false;
                else
                    excDetGallery.IsActive = true;

                var updatedExcDetGallery = _hotelService.UpdateExcursionDetGallery(excDetGallery);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Image/Video is already in use." });
            }
        }

        /// <summary>
        /// Deletes the excursion detail gallery.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionDetailGallery.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionDetailGallery(int id)
        {
            try
            {
                var excDetGallery = _hotelService.GetExcursionDetailGalleryById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];

                if (!string.IsNullOrWhiteSpace(excDetGallery.Image))
                {
                    string imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + excDetGallery.ExcursionDetailId + @"\Gallery\" + excDetGallery.Id + @"\" + excDetGallery.Image;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }
                if (!string.IsNullOrWhiteSpace(excDetGallery.Video))
                {
                    string imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + excDetGallery.ExcursionDetailId + @"\Gallery\" + excDetGallery.Id + @"\" + excDetGallery.Video;
                    ImageHelper.DeleteImage(api_path, imgPath);
                }

                _hotelService.DeleteExcursionDetailGallery(id);
                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This Image/Video is already in use." });
            }
        }

        #endregion Excursion Detail Gallery

        #region Excursion Ingredient Categories
        /// <summary>
        /// Manages the excursion ingredient categories.
        /// </summary>
        /// <returns>ActionResult Manage ExcursionIngredientCategories.</returns>
        public ActionResult ManageExcursionIngredientCategories()
        {
            return View();
        }
        /// <summary>
        /// Loads the excursion ingredient categories.
        /// </summary>
        /// <returns>ActionResult Load ExcursionIngredientCategories.</returns>
        [HttpPost]
        public ActionResult LoadExcursionIngredientCategories()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excIngredientCategories = _hotelService.GetAllExcursionIngredientCategories(skip, pageSize, out Total);
            var data = excIngredientCategories.Select(b => new
            {
                ExcursionIngredientCatId = b.Id,
                Name = b.Name,
                Description = b.Description,
                IsActive = b.IsActive,
                CreationDate = b.CreationDate.ToString("dd/MM/yyyy"),
                Image = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.ExcursionIngredientCategories, b.Image, b.Id, api_path)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the excursion ingredient category.
        /// </summary>
        /// <returns>ActionResult Add ExcursionIngredientCategory.</returns>
        public ActionResult AddExcursionIngredientCategory()
        {
            ExcursionIngredientCategory excIngredientCat = new ExcursionIngredientCategory();
            return View("AddEditExcursionIngredientCategory", excIngredientCat);
        }

        /// <summary>
        /// Saves the excursion ingredient category.
        /// </summary>
        /// <param name="excIngredientCategory">The exc ingredient category.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save ExcursionIngredientCategory.</returns>
        public ActionResult SaveExcursionIngredientCategory(ExcursionIngredientCategory excIngredientCategory, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                excIngredientCategory.Image = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditExcursionIngredientCategory", excIngredientCategory);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditExcursionIngredientCategory", excIngredientCategory);
                        }
                    }
                    if (excIngredientCategory.Id == 0)
                    {
                        var result = _hotelService.CreateExcursionIngredientCategory(excIngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.Id, ImagePathFor.ExcursionIngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageExcursionIngredientCategories");
                    }
                    else
                    {
                        var res = _hotelService.ModifyExcursionIngredientCategory(excIngredientCategory);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.Id, ImagePathFor.ExcursionIngredientCategories, uploadImage, api_path);
                        }
                        return RedirectToAction("ManageExcursionIngredientCategories");
                    }
                }
                else
                {
                    return View("AddEditExcursionIngredientCategory", excIngredientCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditExcursionIngredientCategory", excIngredientCategory);
            }
        }

        /// <summary>
        /// Edits the excursion ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ExcursionIngredientCategory.</returns>
        public ActionResult EditExcursionIngredientCategory(int id)
        {
            var excIngredientCategory = _hotelService.GetExcursionIngredientCategoryById(id);
            return View("AddEditExcursionIngredientCategory", excIngredientCategory);
        }
        /// <summary>
        /// Deletes the excursion ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionIngredientCategory.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionIngredientCategory(int id)
        {
            try
            {
                var excIngredientCategory = _hotelService.GetExcursionIngredientCategoryById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                string imgPath = @"\Images\ExcursionIngredientCategories\" + excIngredientCategory.Id + @"\" + excIngredientCategory.Image;

                _hotelService.DeleteExcursionIngredientCategory(id);
                if (!string.IsNullOrWhiteSpace(excIngredientCategory.Image))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }


                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This excursion ingredient category cannot be deleted because it is already in use." });
            }
        }

        #endregion Excursion Ingredient Categories

        #region Excursion Ingredients
        /// <summary>
        /// Manages the excursion ingredients.
        /// </summary>
        /// <param name="excIngredientCatId">The exc ingredient cat identifier.</param>
        /// <returns>ActionResult Manage ExcursionIngredients.</returns>
        public ActionResult ManageExcursionIngredients(int excIngredientCatId)
        {
            ViewBag.ExcursionIngredientCategoryId = excIngredientCatId;
            return View();
        }

        /// <summary>
        /// Loads the excursion ingredients.
        /// </summary>
        /// <param name="excIngredientCatId">The exc ingredient cat identifier.</param>
        /// <returns>ActionResult Load ExcursionIngredients.</returns>
        [HttpPost]
        public ActionResult LoadExcursionIngredients(int excIngredientCatId)
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excursionIngredient = _hotelService.GetExcursionIngredientByExcIngCatId(excIngredientCatId, skip, pageSize, out Total);

            var data = excursionIngredient.Select(b => new
            {
                Name = b.Name,
                ExcIngredientCategory = b.ExcursionIngredientCategory.Name,
                Description = b.Description,
                IngredientImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.ExcursionIngredient, b.IngredientImage, b.Id, api_path),
                ExcIngredientId = b.Id
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the excursion ingredient.
        /// </summary>
        /// <param name="excIngredientCatId">The exc ingredient cat identifier.</param>
        /// <returns>ActionResult Add ExcursionIngredient.</returns>
        public ActionResult AddExcursionIngredient(int excIngredientCatId)
        {
            ExcursionIngredient excIngredient = new ExcursionIngredient();
            excIngredient.ExcursionIngredientCategoryId = excIngredientCatId;
            return View("AddEditExcursionIngredient", excIngredient);
        }

        /// <summary>
        /// Saves the excursion ingredient.
        /// </summary>
        /// <param name="excIngredient">The exc ingredient.</param>
        /// <param name="uploadExcIngredientImage">The upload exc ingredient image.</param>
        /// <returns>ActionResult Save ExcursionIngredient.</returns>
        public ActionResult SaveExcursionIngredient(ExcursionIngredient excIngredient, HttpPostedFileBase uploadExcIngredientImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    var excIngrImageFilename = excIngredient.IngredientImage;
                    if (uploadExcIngredientImage != null)
                    {
                        if (uploadExcIngredientImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", "jpeg", ".png" };
                            var filename = Path.GetFileName(uploadExcIngredientImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadExcIngredientImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                excIngrImageFilename = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                return View("AddEditExcursionIngredient", excIngredient);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            return View("AddEditExcursionIngredient", excIngredient);
                        }
                    }


                    if (excIngredient.Id == 0)
                    {
                        excIngredient.IngredientImage = excIngrImageFilename;
                        var addedExcIngredinet = _hotelService.CreateExcursionIngredient(excIngredient);
                        if (uploadExcIngredientImage != null)
                        {
                            ImageHelper.UploadImage(addedExcIngredinet.Id, ImagePathFor.ExcursionIngredient, uploadExcIngredientImage, api_path);
                        }

                        return RedirectToAction("ManageExcursionIngredients", new { excIngredientCatId = excIngredient.ExcursionIngredientCategoryId });
                    }
                    else
                    {
                        if (uploadExcIngredientImage != null)
                        {
                            ImageHelper.UploadImage(excIngredient.Id, ImagePathFor.ExcursionIngredient, uploadExcIngredientImage, api_path);
                        }
                        excIngredient.IngredientImage = excIngrImageFilename;
                        var modifiedIngredient = _hotelService.ModifyExcursionIngredient(excIngredient);
                        return RedirectToAction("ManageExcursionIngredients", new { excIngredientCatId = excIngredient.ExcursionIngredientCategoryId });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    return View("AddEditExcursionIngredient", excIngredient);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("AddEditExcursionIngredient", excIngredient);
            }
        }

        /// <summary>
        /// Edits the excursion ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ExcursionIngredient.</returns>
        public ActionResult EditExcursionIngredient(int id)
        {
            var excIngredient = _hotelService.GetExcursionIngredientById(id);
            return View("AddEditExcursionIngredient", excIngredient);
        }

        /// <summary>
        /// Deletes the excursion ingredient.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionIngredient.</returns>
        public ActionResult DeleteExcursionIngredient(int Id)
        {
            try
            {
                var excIngredient = _hotelService.GetExcursionIngredientById(Id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                string imgPath = @"\Images\ExcursionIngredients\" + excIngredient.Id + @"\" + excIngredient.IngredientImage;

                _hotelService.DeleteExcursionIngredient(Id);
                if (!string.IsNullOrWhiteSpace(excIngredient.IngredientImage))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }

                return Json(new { status = true });
            }
            catch
            {
                return Json(new { status = false, error = "This excursion ingredient cannot be deleted because it is already in use." });
            }
        }
        #endregion Excursion Ingredients

        #region Excursion Offers
        /// <summary>
        /// Manages the excursion offers.
        /// </summary>
        /// <returns>ActionResult Manage ExcursionOffers.</returns>
        public ActionResult ManageExcursionOffers()
        {
            return View();
        }

        /// <summary>
        /// Loads the excursion offers.
        /// </summary>
        /// <returns>ActionResult Load ExcursionOffers.</returns>
        [HttpPost]
        public ActionResult LoadExcursionOffers()
        {
            var api_path = System.Configuration.ConfigurationManager.AppSettings["APIURL"];
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excursionOffers = _hotelService.GetExcursionOffer(GetActiveHotelId(), skip, pageSize, out Total);
            var data = excursionOffers.Select(b => new
            {
                ExcursionOfferId = b.Id,
                ExcursionDetailName = b.ExcursionDetail.Name,
                OfferDescription = b.Description,
                OfferPercentage = b.Percentage,
                IsActive = b.IsActive,
                OfferImage = ToOrdr.Core.Helpers.ImageHelper.GetPath(ToOrdr.Core.Helpers.ImagePathFor.ExcursionOffer, b.OfferImage, b.ExcursionDetailId, api_path, b.Id)
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the excursion offer.
        /// </summary>
        /// <returns>ActionResult Add ExcursionOffer.</returns>
        public ActionResult AddExcursionOffer()
        {
            var HotelId = GetActiveHotelId();
            ExcursionOffer excursionOffer = new ExcursionOffer();
            ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(HotelId).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
            return View("AddEditExcursionOffer", excursionOffer);
        }

        /// <summary>
        /// Saves the excursion offer.
        /// </summary>
        /// <param name="excursionOffer">The excursion offer.</param>
        /// <param name="uploadImage">The upload image.</param>
        /// <returns>ActionResult Save ExcursionOffer.</returns>
        public ActionResult SaveExcursionOffer(ExcursionOffer excursionOffer, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var api_path = ConfigurationManager.AppSettings["APIURL"];
                    if (uploadImage != null)
                    {
                        if (uploadImage.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var filename = Path.GetFileName(uploadImage.FileName);
                            filename = filename.Replace(" ", "_");
                            var ext = Path.GetExtension(uploadImage.FileName);
                            if (allowedExtensions.Contains(ext))
                            {
                                excursionOffer.OfferImage = filename;
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.Invalid_ImageExtension);
                                ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                                return View("AddEditExcursionOffer", excursionOffer);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.ImageUploadSizeExceeded);
                            ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                            return View("AddEditExcursionOffer", excursionOffer);
                        }
                    }
                    else
                    {
                        if (excursionOffer.Id == 0)
                        {
                            ModelState.AddModelError("OfferImage", Resources.Rqd_OfferImage);
                            ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                            return View("AddEditExcursionOffer", excursionOffer);
                        }

                    }
                    if (excursionOffer.Id == 0)
                    {
                        var result = _hotelService.CreateExcursionOffer(excursionOffer);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(result.ExcursionDetailId, ImagePathFor.ExcursionOffer, uploadImage, api_path, result.Id);
                        }
                        return RedirectToAction("ManageExcursionOffers");
                    }
                    else
                    {
                        var res = _hotelService.ModifyExcursionOffer(excursionOffer);
                        if (uploadImage != null)
                        {
                            ImageHelper.UploadImage(res.ExcursionDetailId, ImagePathFor.ExcursionOffer, uploadImage, api_path, res.Id);
                        }
                        return RedirectToAction("ManageExcursionOffers");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                    return View("AddEditExcursionOffer", excursionOffer);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                return View("AddEditExcursionOffer", excursionOffer);
            }
        }

        /// <summary>
        /// Edits the excursion offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Edit ExcursionOffer.</returns>
        public ActionResult EditExcursionOffer(int id)
        {
            var hotelId = GetActiveHotelId();
            var excursionOffer = _hotelService.GetExcursionOfferById(id);
            ViewBag.UnassignedOfferExcursionDetails = _hotelService.GetUnassignedOfferExcursionDetails(GetActiveHotelId()).Select(s => new { Id = s.Id, Name = s.Name }).ToList();
            return View("AddEditExcursionOffer", excursionOffer);
        }
        /// <summary>
        /// Deletes the excursion offer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionOffer.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionOffer(int id)
        {
            try
            {
                var excursionOffer = _hotelService.GetExcursionOfferById(id);
                var api_path = ConfigurationManager.AppSettings["APIURL"];
                string imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + excursionOffer.ExcursionDetailId + @"\OfferImage\" + excursionOffer.Id + @"\" + excursionOffer.OfferImage;

                _hotelService.DeleteExcursionOffer(id);
                if (!string.IsNullOrWhiteSpace(excursionOffer.OfferImage))
                {
                    ImageHelper.DeleteImage(api_path, imgPath);
                }

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This excursion offer already in use." });
            }
        }
        #endregion Excursion Offers

        #region Hotel Excursion Detail Ingredients
        /// <summary>
        /// Manages the excursion detail ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage ExcursionDetail Ingredients.</returns>
        public ActionResult ManageExcursionDetailIngredients(int id)
        {
            var excursionDetail = _hotelService.GetExcursionDetailById(id);
            ViewBag.HotelExcursionId = excursionDetail.HotelExcursionId;
            ViewBag.ExcursionDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the excursion detail ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load ExcursionDetail Ingredients.</returns>
        [HttpPost]
        public ActionResult LoadExcursionDetailIngredients(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excursionDetIngredients = _hotelService.GetExcursionDetailIngredients(id, skip, pageSize, out Total);
            var data = excursionDetIngredients.Select(b => new
            {
                ExcursionDetIngredientId = b.Id,
                ExcDetailName = b.ExcursionDetail.Name,
                ExcIngredientName = b.ExcursionIngredient.Name,
                ExcIngredientDesc = b.ExcursionIngredient.Description,
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Adds the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>ActionResult Add ExcursionDetail Ingredient.</returns>
        public ActionResult AddExcursionDetailIngredient(int excursionDetailId)
        {
            ExcursionDetailIngredient excursionDetIngr = new ExcursionDetailIngredient();
            excursionDetIngr.ExcursionDetailsId = excursionDetailId;

            var unassignExcIngredients = _hotelService.GetUnassignExcursionIngrdeients(excursionDetailId).Select(
                 r => new
                 {
                     ExcIngredientId = r.Id,
                     Name = r.Name
                 }).ToList();
            ViewBag.ExcursionIngredients = new MultiSelectList(unassignExcIngredients, "ExcIngredientId", "Name");
            return View(excursionDetIngr);
        }


        /// <summary>
        /// Saves the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetIngredient">The excursion det ingredient.</param>
        /// <returns>ActionResult Save ExcursionDetailIngredient.</returns>
        [HttpPost]
        public ActionResult SaveExcursionDetailIngredient(ExcursionDetailIngredient excursionDetIngredient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ExcursionDetailIngredient> excursionDetIngredientsList = new List<ExcursionDetailIngredient>();

                    var excIngredientIds = Request.Form["ExcIngredientId"];

                    if (excIngredientIds != null)
                    {
                        string[] excIngrIds = excIngredientIds.Split(',');
                        foreach (var eachExcIngrId in excIngrIds)
                        {
                            ExcursionDetailIngredient excursionDetIng = new ExcursionDetailIngredient()
                            {
                                ExcursionDetailsId = excursionDetIngredient.ExcursionDetailsId,
                                ExcursionIngredientsId = Convert.ToInt32(eachExcIngrId)
                            };
                            excursionDetIngredientsList.Add(excursionDetIng);
                        }
                    }
                    else
                    {
                        var unassignExcIngredients = _hotelService.GetUnassignExcursionIngrdeients(excursionDetIngredient.ExcursionDetailsId).Select(
                             r => new
                             {
                                 ExcIngredientId = r.Id,
                                 Name = r.Name
                             }).ToList();
                        ViewBag.ExcursionIngredients = new MultiSelectList(unassignExcIngredients, "ExcIngredientId", "Name");
                        ModelState.AddModelError("", Resources.Rqd_ExcursionDetailIngredient);
                        return View("AddExcursionDetailIngredient", excursionDetIngredient);
                    }

                    _hotelService.CreateExcursionDetailIngredient(excursionDetIngredientsList);

                    return RedirectToAction("ManageExcursionDetailIngredients", "HotelExcursion", new { area = "admin", id = excursionDetIngredient.ExcursionDetailsId });
                }
                else
                {
                    var unassignExcIngredients = _hotelService.GetUnassignExcursionIngrdeients(excursionDetIngredient.ExcursionDetailsId).Select(
                         r => new
                         {
                             ExcIngredientId = r.Id,
                             Name = r.Name
                         }).ToList();
                    ViewBag.ExcursionIngredients = new MultiSelectList(unassignExcIngredients, "ExcIngredientId", "Name");
                    ModelState.AddModelError("", "Validation Error");
                    return View("AddExcursionDetailIngredient", excursionDetIngredient);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignExcIngredients = _hotelService.GetUnassignExcursionIngrdeients(excursionDetIngredient.ExcursionDetailsId).Select(
                     r => new
                     {
                         ExcIngredientId = r.Id,
                         Name = r.Name
                     }).ToList();
                ViewBag.ExcursionIngredients = new MultiSelectList(unassignExcIngredients, "ExcIngredientId", "Name");
                return View("AddExcursionDetailIngredient", excursionDetIngredient);
            }
        }

        /// <summary>
        /// Deletes the excursion detail ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionDetailIngredient.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionDetailIngredient(int id)
        {
            try
            {
                _hotelService.DeleteExcursionDetailIngredient(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This excursion detail ingredients already in use." });
            }
        }
        #endregion Hotel Excursion Detail Ingredients

        #region Excursion Suggestion
        /// <summary>
        /// Manages the excursion suggestions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Manage Excursion Suggestions.</returns>
        public ActionResult ManageExcursionSuggestions(int id)
        {
            var excursionDetail = _hotelService.GetExcursionDetailById(id);
            ViewBag.HotelExcursionId = excursionDetail.HotelExcursionId;
            ViewBag.ExcursionDetailId = id;
            return View();
        }

        /// <summary>
        /// Loads the excursion suggestions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Load Excursion Suggestions.</returns>
        [HttpPost]
        public ActionResult LoadExcursionSuggestions(int id)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var excursionDetSuggestion = _hotelService.GetExcursionDetSuggestions(id, skip, pageSize, out Total);
            var data = excursionDetSuggestion.Select(b => new
            {
                ExcSuggestionId = b.Id,
                ExcName = b.ExcursionDetail.HotelExcursion.Excursion.Name,
                ExcDetName = b.ExcursionDetail.Name,
                ExcSuggestionName = b.ExcursionDetail1.Name,
                ExcSuggestionDescription = b.ExcursionDetail1.Description,
                ExcSuggestionPrice = b.ExcursionDetail1.Price,
                IsActive = b.ExcursionDetail1.IsActive
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds the excursion suggestion.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <returns>ActionResult Add ExcursionSuggestion.</returns>
        public ActionResult AddExcursionSuggestion(int excursionDetId)
        {
            ExcursionSuggestion excDetSuggestion = new ExcursionSuggestion();
            excDetSuggestion.ExcursionDetailId = excursionDetId;
            var unassignExcursionDet = _hotelService.GetUnassignExcursionDetSuggetions(excursionDetId, GetActiveHotelId()).Select(
                r => new
                {
                    ExcursionDetSuggestionId = r.Id,
                    Name = r.Name
                }).ToList();
            ViewBag.ExcursionDetSuggestions = new MultiSelectList(unassignExcursionDet, "ExcursionDetSuggestionId", "Name");
            return View(excDetSuggestion);
        }

        /// <summary>
        /// Saves the excursion det suggestion.
        /// </summary>
        /// <param name="excursionDetSuggestion">The excursion det suggestion.</param>
        /// <returns>ActionResult Save ExcursionDetails Suggestion.</returns>
        [HttpPost]
        public ActionResult SaveExcursionDetSuggestion(ExcursionSuggestion excursionDetSuggestion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ExcursionSuggestion> excSuggestionList = new List<ExcursionSuggestion>();

                    var ExcDetSuggestionIds = Request.Form["ExcursionDetSuggestionId"];

                    if (ExcDetSuggestionIds != null)
                    {
                        string[] excDetSuggIds = ExcDetSuggestionIds.Split(',');
                        foreach (var eachExcDetId in excDetSuggIds)
                        {
                            ExcursionSuggestion excDetSuggestion = new ExcursionSuggestion()
                            {
                                ExcursionDetailId = excursionDetSuggestion.ExcursionDetailId,
                                ExcursionDetailSuggestionId = Convert.ToInt32(eachExcDetId)
                            };
                            excSuggestionList.Add(excDetSuggestion);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, Resources.Rqd_ExcursionDetailSuggestion);
                        var unassignExcursionDet = _hotelService.GetUnassignExcursionDetSuggetions(excursionDetSuggestion.ExcursionDetailId, GetActiveHotelId()).Select(
                            r => new
                            {
                                ExcursionDetSuggestionId = r.Id,
                                Name = r.Name
                            }).ToList();
                        ViewBag.ExcursionDetSuggestions = new MultiSelectList(unassignExcursionDet, "ExcursionDetSuggestionId", "Name");
                        return View("AddExcursionSuggestion", excursionDetSuggestion);
                    }

                    _hotelService.CreateExcursionDetailSuggestion(excSuggestionList);
                    return RedirectToAction("ManageExcursionSuggestions", "HotelExcursion", new { area = "admin", id = excursionDetSuggestion.ExcursionDetailId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Validation Error");
                    var unassignExcursionDet = _hotelService.GetUnassignExcursionDetSuggetions(excursionDetSuggestion.ExcursionDetailId, GetActiveHotelId()).Select(
                        r => new
                        {
                            ExcursionDetSuggestionId = r.Id,
                            Name = r.Name
                        }).ToList();
                    ViewBag.ExcursionDetSuggestions = new MultiSelectList(unassignExcursionDet, "ExcursionDetSuggestionId", "Name");

                    return View("AddExcursionSuggestion", excursionDetSuggestion);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                var unassignExcursionDet = _hotelService.GetUnassignExcursionDetSuggetions(excursionDetSuggestion.ExcursionDetailId, GetActiveHotelId()).Select(
                    r => new
                    {
                        ExcursionDetSuggestionId = r.Id,
                        Name = r.Name
                    }).ToList();
                ViewBag.ExcursionDetSuggestions = new MultiSelectList(unassignExcursionDet, "ExcursionDetSuggestionId", "Name");

                return View("AddExcursionSuggestion", excursionDetSuggestion);
            }
        }

        /// <summary>
        /// Deletes the excursion suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult Delete ExcursionSuggestion.</returns>
        [HttpPost]
        public ActionResult DeleteExcursionSuggestion(int id)
        {
            try
            {
                _hotelService.DeleteExcursionDetSuggestion(id);

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false, error = "This excursion suggestion cannot be deleted because it is already in use." });
            }
        }
        #endregion Excursion Suggestion

        #region Excursion Orders Queue
        /// <summary>
        /// Gets the is late satus change orders.
        /// </summary>
        /// <returns>ActionResult Get IsLate Satus Change Orders.</returns>
        public ActionResult GetIsLateSatusChangeOrders()
        {
            var hotelId = GetActiveHotelId();
            ViewBag.ExcursionOrderIsLateStatusChangedCount = _hotelService.GetExcursionOrderIsLateStatusChangedCount(hotelId);
            ViewBag.SpaOrderIsLateStatusChangedCount = _hotelService.GetSpaOrderIsLateStatusChangedCount(hotelId);
            return PartialView("_GetIsLateStatusChangeOrders");
        }

        /// <summary>
        /// Excursions the orders queue.
        /// </summary>
        /// <returns>ActionResult Excursion OrdersQueue.</returns>
        public ActionResult ExcursionOrdersQueue()
        {
            return View();
        }
        /// <summary>
        /// Loads the excursion orders queue.
        /// </summary>
        /// <returns>ActionResult Load Excursion OrdersQueue.</returns>
        [HttpPost]
        public ActionResult LoadExcursionOrdersQueue()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;
            var statusesToShow = from ExcursionOrderStatus e in Enum.GetValues(typeof(ExcursionOrderStatus)) select new { Id = (int)e, Name = e.ToString() };
            statusesToShow = statusesToShow.Where(s => s.Id != 3);
            var excursionOrders = _hotelService.GetExcursionOrdersQueue(hotelId, skip, pageSize, out Total);
            var data = excursionOrders.Select(b => new
            {
                ExcursionOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((ExcursionOrderStatus)b.OrderStatus).ToString(),
                ExcursionOrderStatuses = (b.PaymentStatus == true) ? b.OrderStatus == 1 ? statusesToShow.Where(s => s.Id >= b.OrderStatus && s.Id <= 2) : statusesToShow.Where(s => s.Id >= b.OrderStatus) : statusesToShow.Where(s => s.Id == 0),
                PaymentStatus = b.PaymentStatus.ToString(),
                IsLateStatus = b.IsLate
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Loads the excursion order item details.
        /// </summary>
        /// <param name="excursionOrderId">The excursion order identifier.</param>
        /// <param name="orderHistory">The order history.</param>
        /// <returns>ActionResult Load Excursion OrderItemDetails.</returns>
        public ActionResult LoadExcursionOrderItemDetails(int excursionOrderId, int? orderHistory = 0)
        {
            var orderItemDetails = _hotelService.GetExcursionOrderItemDetails(excursionOrderId);
            ViewBag.OrdrHistory = orderHistory;
            return PartialView("_ExcursionOrderItemDetails", orderItemDetails);
        }

        /// <summary>
        /// Updates the excursion order status.
        /// </summary>
        /// <param name="excursionOrderId">The excursion order identifier.</param>
        /// <param name="orderStatus">The order status.</param>
        /// <returns>ActionResult Update Excursion OrderStatus.</returns>
        [HttpPost]
        public ActionResult UpdateExcursionOrderStatus(int excursionOrderId, int orderStatus)
        {
            try
            {
                var excursionOrder = _hotelService.GetExcursionOrdersDetailsById(excursionOrderId);
                if (excursionOrder.OrderStatus == 0 && orderStatus == 1)
                {
                    var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                    var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];

                    //capture payment from Quickpay
                    var captureStatus = _paymentService.CaptureCardPayment(quickpayToken, quickpayUrl, excursionOrder.QuickPayPaymentId, excursionOrder.OrderTotal);
                    if (captureStatus)
                    {
                        excursionOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyExcursionOrder(excursionOrder);
                        return Json(new { status = true, rowRemoveStatus = false, message = "Order payment captured successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order payment capture Failed!" });
                    }

                }
                if (excursionOrder.OrderStatus == 0 && orderStatus == 4)
                {
                    var quickpayToken = ConfigurationManager.AppSettings["QuickPayAuthToken"];
                    var quickpayUrl = ConfigurationManager.AppSettings["QuickPayAPIAddress"];

                    //Reject the order and release amount reserved for the order
                    var rejectedStatus = _paymentService.RejectHotelServiceOrderAndReleaseAmount(quickpayToken, quickpayUrl, excursionOrder.QuickPayPaymentId);
                    if (rejectedStatus)
                    {
                        excursionOrder.OrderStatus = orderStatus;
                        _hotelService.ModifyExcursionOrder(excursionOrder);
                        return Json(new { status = true, rowRemoveStatus = true, message = "Order rejected and amount released successfully" });
                    }
                    else
                    {
                        return Json(new { status = false, rowRemoveStatus = false, message = "Order rejection Failed!" });
                    }
                }

                if (excursionOrder.OrderStatus == 0 && orderStatus == 2)
                {
                    return Json(new { status = false, rowRemoveStatus = false, message = "This order cannot be delivered since it has not confirmed yet" });
                }

                if (excursionOrder.OrderStatus != orderStatus)
                {
                    excursionOrder.OrderStatus = orderStatus;
                    _hotelService.ModifyExcursionOrder(excursionOrder);
                }

                if (orderStatus == 2 || orderStatus == 4)
                    return Json(new { status = true, rowRemoveStatus = true });
                else
                    return Json(new { status = true, rowRemoveStatus = false });
            }
            catch
            {
                return Json(new { status = false, rowRemoveStatus = false, message = "This order status cannot be Updated" });
            }
        }
        /// <summary>
        /// Gets the state of the errors from model.
        /// </summary>
        /// <returns>Dictionary&lt;System.String, ModelErrorCollection&gt;.</returns>
        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState() //IEnumerable<string>
        {
            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }

        /// <summary>
        /// Updates the excursion order item deliver status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult update delivery status.</returns>
        [HttpPost]
        public ActionResult UpdateExcursionOrderItemDeliverStatus(int id)
        {
            try
            {
                _hotelService.UpdateExcursionOrderItemDeliverStatus(id);
                return Json(new { success = true });
            }
            catch
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
            }
        }
        #endregion Excursion Orders Queue

        #region Excursion Orders History

        /// <summary>
        /// Excursions the orders history.
        /// </summary>
        /// <returns>ActionResult Excursion OrdersHistory.</returns>
        public ActionResult ExcursionOrdersHistory()
        {
            return View();
        }
        /// <summary>
        /// Loads the excursion orders history.
        /// </summary>
        /// <returns>ActionResult Load Excursion OrdersHistory.</returns>
        [HttpPost]
        public ActionResult LoadExcursionOrdersHistory()
        {
            var hotelId = GetActiveHotelId();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //find search columns info
            var fromDate = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var toDate = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var minAmount = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var maxAmount = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var minAmt = minAmount == "" ? 0 : Convert.ToDouble(minAmount);
            var maxAmt = maxAmount == "" ? 0 : Convert.ToDouble(maxAmount);

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int Total = 0;

            var spaOrdersHistory = _hotelService.GetExcursionOrderHistory(fromDate, toDate, hotelId, minAmt, maxAmt, skip, pageSize, out Total);
            var data = spaOrdersHistory.Select(b => new
            {
                ExcursionOrderId = b.Id,
                CustomerName = b.Customer.FirstName + " " + b.Customer.LastName,
                BookingDate = b.CreationDate.ToString(),
                Amount = b.OrderTotal,
                OrderStatus = ((ExcursionOrderStatus)b.OrderStatus).ToString(),
                PaymentStatus = b.PaymentStatus.ToString(),
                TipsAmount = b.ExcursionTips.Select(i => i.TipAmount).FirstOrDefault(),
                TipsPaid = b.ExcursionTips.Select(i => i.PaymentStatus).FirstOrDefault()
            });

            return Json(new { draw = draw, recordsFiltered = Total, recordsTotal = Total, data = data }, JsonRequestBehavior.AllowGet);
        }

        #endregion Excursion Orders History

    }
}