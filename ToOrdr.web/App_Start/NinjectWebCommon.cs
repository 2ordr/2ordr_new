[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ToOrdr.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ToOrdr.Web.App_Start.NinjectWebCommon), "Stop")]

namespace ToOrdr.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using ToOrdr.Core.Data;
    using ToOrdr.Core.Services.Interfaces;
    using ToOrdr.Core.Services.Implementations;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IRepository<>)).To(typeof(EfRepository<>));

            kernel.Bind<IRestaurantService>().To<RestaurantService>();
            kernel.Bind<ICustomerService>().To<CustomerService>();
            kernel.Bind<IRestaurantMenusService>().To<RestaurantMenusService>();
            kernel.Bind<IRoleService>().To<RoleService>();
            kernel.Bind<IHotelService>().To<HotelService>();
            kernel.Bind<IPaymentService>().To<PaymentService>();
            kernel.Bind<IAboutYouOrdrService>().To<AboutYouOrdrService>();
        }
    }
}
