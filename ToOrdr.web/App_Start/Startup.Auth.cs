﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;

namespace ToOrdr.Web
{
	public partial class Startup
	{
		public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/SignIn")
            });


            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


			// rajesh
            //app.UseFacebookAuthentication(
            //    appId: "2012877402276182",
            //    appSecret: "94ce0ca6e6959ef3c654da832b1f36e3");

            ////Manoj
            //app.UseFacebookAuthentication(
            //   appId: "779853518880224",
            //   appSecret: "3a78e3a1c05ec13e61ff9bc457d45e4b");

            //developmentyouordr@gmail.com
            app.UseFacebookAuthentication(
               appId: "199104870754608",
               appSecret: "fa604c76e89af55c0597a1f1b517ef25");

            //account : manojgawas49@gmail.com
            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "794203325713-us80udiua909iqo3l79ao996spjobcjj.apps.googleusercontent.com",
            //    ClientSecret = "Dnwu9L0ojqNBagflyYll64do"
            //});

            ////account : donotreplycertigoa@gmail.com
            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "328233123653-h6du77mstkpajqseuddh0nmf4ti3v4ht.apps.googleusercontent.com",
            //    ClientSecret = "007lDReV4__mhzP6KDxH-RaG"
            //});

            //account : developmentyouordr@gmail.com
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "498040139201-ao40ng8mf72q1dnt0djk37hb2fqfar19.apps.googleusercontent.com",
                ClientSecret = "RYK7T-KkCEqaPMOYPAeSjJKr"
            });

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "883209732812-8n3e10mav7t1cqsch4bfffcouc58nlc7.apps.googleusercontent.com",
            //    ClientSecret = "Lv2Pifg_-4ipaz8xDRuluEt3"
            //});


            // //2ORDR
            //app.UseFacebookAuthentication(
            //    appId: "174215759824387",
            //    appSecret: "3983755fc793019e1485b45f85e5cf52");

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
	}
}