﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartupAttribute(typeof(ToOrdr.Web.Startup))]
namespace ToOrdr.Web
{
    
    public partial class Startup
    {
        public void Configuration(IAppBuilder  app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}