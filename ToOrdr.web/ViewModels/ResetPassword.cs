﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ToOrdr.Localization;

namespace ToOrdr.Web.ViewModels
{
    public class ResetPassword
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceName = "Rqd_Password", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$", ErrorMessageResourceName = "Valid_Password", ErrorMessageResourceType = typeof(Resources))]
        public string Password { get; set; }
        
        [DataType(DataType.Password)]
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        public string ConfirmPassword { get; set; }

        public string Id { get; set; }
    }
}