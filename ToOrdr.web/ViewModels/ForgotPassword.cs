﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToOrdr.Web.ViewModels
{
    public class ForgotPassword
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}