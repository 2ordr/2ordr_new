﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Web.ViewModels
{
    public class CustomerSignUpVM
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        public string ConfirmPassword { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        //public string Country { get; set; }
        public int CountryId { get; set; }

        public string Pincode { get; set; }

    }

    public class CustomerSignInVM
    {
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(100, ErrorMessageResourceName = "Max_Email", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }   
}