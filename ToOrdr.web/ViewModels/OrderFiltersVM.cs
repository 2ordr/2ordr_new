﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToOrdr.Web.ViewModels
{
    public class OrderFiltersVM
    {
        public string FromDate { get; set; }

        public string ToDate { get; set; }
        public string SearchText { get; set; }
        public int RestaurantId { get; set; }
        
        [Display(Name="Min Amount")]
        public Nullable<Double> MinAmount { get; set; }

        [Display(Name = "Max Amount")]
        public Nullable<Double> MaxAmount { get; set; }

    }

    public class RestaurantOrderHistoryFiltersVM
    {
        public string FromDate { get; set; }

        public string ToDate { get; set; }

        [Display(Name = "Min Amount")]
        public Nullable<Double> MinAmount { get; set; }

        [Display(Name = "Max Amount")]
        public Nullable<Double> MaxAmount { get; set; }

    }
}