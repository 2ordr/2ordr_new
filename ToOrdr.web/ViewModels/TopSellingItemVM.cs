﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToOrdr.Web.ViewModels
{
    public class TopSellingItemVM
    {
        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public bool category { get; set; }


    }


    public class RestOrderComment
    {
        public int OrderId { get; set; }

        public int StatusId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }


    }
}