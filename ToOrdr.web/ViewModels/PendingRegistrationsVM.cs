﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Web.ViewModels
{
    public class PendingRegistrationsVM
    {
        public virtual IEnumerable<Restaurant> Restaurant { get; set; }
        public virtual IEnumerable<Hotel> Hotel { get; set; }
    }
}