﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;

namespace ToOrdr.Web.ViewModels
{
    public class RestaurantSearch
    {
        public string Keywords { get; set; }

        public IEnumerable<Cuisine> Cuisines { get; set; }

        public string[] SelectedCuisinesId { get; set; }
    }
}