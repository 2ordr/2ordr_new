﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;


namespace ToOrdr.Web.ViewModels
{
    public enum GenderType
    {
        Male = 1,
        Female = 2
    }

    public class CustomerRoleVM
    {
        public virtual IEnumerable<CustomerRole> CustomerRoleRest { get; set; }
        public virtual IEnumerable<CustomerRole> CustomerRoleHotel { get; set; }
    }

    public class CustomerRoleViewModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoleId { get; set; }
        public Nullable<int> HotelId { get; set; }
        public Nullable<int> RestaurantId { get; set; }
        public Nullable<int> ChainId { get; set; }

        public string RestaurantName { get; set; }
        public string ChainTableName { get; set; }
        public string HotelName { get; set; }
        public virtual RegisterCustomerVM Customer { get; set; }
    }

    public class RegisterCustomerVM
    {
        [Display(Name = "Name")]
        [Required(ErrorMessageResourceName = "Rqd_Customer_ForeName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(50, ErrorMessageResourceName = "Max_Customer_ForeName", ErrorMessageResourceType = typeof(Resources))]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessageResourceName = "Rqd_LastName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(50, ErrorMessageResourceName = "Max_LastName", ErrorMessageResourceType = typeof(Resources))]
        public string LastName { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        public string Gender { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Pincode { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_Telephone", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(15, ErrorMessageResourceName = "Max_Telephone", ErrorMessageResourceType = typeof(Resources))]
        public string Telephone { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessageResourceName = "Rqd_Email", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(100, ErrorMessageResourceName = "Max_Email", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CountryId", ErrorMessageResourceType = typeof(Resources))]
        public int CountryId { get; set; }

        [Required(ErrorMessageResourceName = "Rqd_PhoneCode", ErrorMessageResourceType = typeof(Resources))]
        public string PhoneCode { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_CurrencyId", ErrorMessageResourceType = typeof(Resources))]
        public int CurrencyId { get; set; }
    }
}