﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToOrdr.Core.Entities;
using ToOrdr.Localization;

namespace ToOrdr.Web.ViewModels
{
    public class RestaurantPreferenceViewModel
    {
        public RestaurantPreferenceViewModel()
        {
            Groups = new List<RestaurantPreferenceGroupViewModel>();
        }
        public int Id { get; set; }
        public List<RestaurantPreferenceGroupViewModel> Groups { get; set; }
    }


    public class RestaurantPreferenceGroupViewModel
    {
        public RestaurantPreferenceGroupViewModel()
        {
            SubGroups = new List<RestaurantPreferenceSubGroupViewModel>();
        }

        public string GroupName { get; set; }
        public string Description { get; set; }

        public List<RestaurantPreferenceSubGroupViewModel> SubGroups { get; set; }
    }

    public class RestaurantPreferenceSubGroupViewModel
    {
        public int SubGroupId { get; set; }

        public string SubGroupName { get; set; }

        public string Description { get; set; }

        public bool? ItemValue { get; set; }

    }
}