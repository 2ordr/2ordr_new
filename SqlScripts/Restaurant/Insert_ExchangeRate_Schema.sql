
--UPDATE Currency Table--

  Update [dbo].[Currency] set CurrencyName='Krone' ,Code=N'DKK', Symbol=N'kr' where Id=1
  Update [dbo].[Currency] set CurrencyName='United States Dollar' ,Code=N'USD', Symbol=N'$' where Id=2
  --Update  [dbo].[Currency] set CurrencyName='Euro' ,Code=N'EUR', Symbol=N'€' where Id=3
  --Update  [dbo].[Currency] set CurrencyName='Yen' ,Code=N'JPY', Symbol=N'¥' where Id=4
  Update  [dbo].[Currency] set CurrencyName='Swedish kroner' ,Code=N'SEK', Symbol=N'kr' where Id=5
  Update  [dbo].[Currency] set CurrencyName='Norweigan kroner' ,Code=N'NOK', Symbol=N'kr' where Id=6
  Update  [dbo].[Currency] set CurrencyName='Pound' ,Code=N'GBP', Symbol=N'£' where Id=7
  
  Update  [dbo].[Currency] set CurrencyName='Belarusian rouble' ,Code=N'BYR', Symbol=N'p' where Id=8
  Update  [dbo].[Currency] set CurrencyName='Ukranian' ,Code=N'UAH', Symbol=N'₴' where Id=9
  Update  [dbo].[Currency] set CurrencyName='Canadian dollar' ,Code=N'CAD', Symbol=N'$' where Id=10
  Update  [dbo].[Currency] set CurrencyName='Australian dollar' ,Code=N'AUD', Symbol=N'$' where Id=11
  
  DELETE FROM [dbo].[Currency] WHERE Id > 12
  
  
INSERT INTO [dbo].[ExchangeRate]
           ([FromCurrency]
           ,[ToCurrency]
           ,[BaseRate]
           ,[CreationDate])
     VALUES
           (1,2,0.16,GETDATE()),
           (1,3,0.13,GETDATE()),
           (1,4,17.39,GETDATE()),
           (1,5,1.38,GETDATE()),
           (1,6,1.26,GETDATE()),
           (1,7,0.12,GETDATE()),
           (1,8,0.33,GETDATE()),
           (1,9,4.34,GETDATE()),
           (1,10,0.20,GETDATE()),
           (1,11,0.22,GETDATE()),
           
           (2,1,6.45,GETDATE()),
           (2,3,0.86,GETDATE()),
           (2,4,12.10,GETDATE()),
           (2,5,8.92,GETDATE()),
           (2,6,8.15,GETDATE()),
           (2,7,0.76,GETDATE()),
           (2,8,2.12,GETDATE()),
           (2,9,27.96,GETDATE()),
           (2,10,1.30,GETDATE()),
           (2,11,1.40,GETDATE()),
           
           (3,1,7.46,GETDATE()),
           (3,2,1.16,GETDATE()),
           (3,4,129.76,GETDATE()),
           (3,5,10.32,GETDATE()),
           (3,6,9.43,GETDATE()),
           (3,7,0.88,GETDATE()),
           (3,8,2.45,GETDATE()),
           (3,9, 32.36,GETDATE()),
           (3,10,1.50,GETDATE()),
           (3,11,1.62,GETDATE()),
           
           (4,1,0.057,GETDATE()),
           (4,2,0.0089,GETDATE()),
           (4,3,0.0077,GETDATE()),
           (4,5,0.080,GETDATE()),
           (4,6,0.073,GETDATE()),
           (4,7,0.0068,GETDATE()),
           (4,8,0.019,GETDATE()),
           (4,9,0.25,GETDATE()),
           (4,10,0.012,GETDATE()),
           (4,11,0.013,GETDATE()),
           
           (5,1,0.72,GETDATE()),
           (5,2,0.11,GETDATE()),
           (5,3,0.097,GETDATE()),
           (5,4,12.57,GETDATE()),
           (5,6,0.91,GETDATE()),
           (5,7,0.085,GETDATE()),
           (5,8,0.24,GETDATE()),
           (5,9,3.13,GETDATE()),
           (5,10,0.15,GETDATE()),
           (5,11, 0.16,GETDATE()),
           
           (6,1,0.79,GETDATE()),
           (6,2,0.12,GETDATE()),
           (6,3,0.11,GETDATE()),
           (6,4,13.76,GETDATE()),
           (6,5,1.09,GETDATE()),
           (6,7,0.093,GETDATE()),
           (6,8,0.26,GETDATE()),
           (6,9,3.43,GETDATE()),
           (6,10,0.16,GETDATE()),
           (6,11,0.17,GETDATE()),
           
           (7,1,8.51,GETDATE()),
           (7,2,1.32,GETDATE()),
           (7,3,1.14,GETDATE()),
           (7,4,147.94,GETDATE()),
           (7,5,11.77,GETDATE()),
           (7,6,10.75,GETDATE()),
           (7,8,2.79,GETDATE()),
           (7,9,36.90,GETDATE()),
           (7,10,1.71,GETDATE()),
           (7,11,1.85,GETDATE()),
           
           (8,1,3.05,GETDATE()),
           (8,2,0.47,GETDATE()),
           (8,3,0.41,GETDATE()),
           (8,4,52.96,GETDATE()),
           (8,5,4.21,GETDATE()),
           (8,6,3.85,GETDATE()),
           (8,7,0.36,GETDATE()),
           (8,9,13.19,GETDATE()),
           (8,10,0.61,GETDATE()),
           (8,11,0.66,GETDATE()),
           
           (9,1,0.23,GETDATE()),
           (9,2,0.036,GETDATE()),
           (9,3,0.031,GETDATE()),
           (9,4,4.01,GETDATE()),
           (9,5,0.32,GETDATE()),
           (9,6,0.29,GETDATE()),
           (9,7,0.027,GETDATE()),
           (9,8,0.076,GETDATE()),
           (9,10,0.046,GETDATE()),
           (9,11,0.050,GETDATE()),
           
           (10,1,4.97,GETDATE()),
           (10,2,0.77,GETDATE()),
           (10,3,0.67,GETDATE()),
           (10,4,86.42,GETDATE()),
           (10,5,6.88,GETDATE()),
           (10,6,6.28,GETDATE()),
           (10,7,0.58,GETDATE()),
           (10,8,1.63,GETDATE()),
           (10,9,21.56,GETDATE()),
           (10,11,1.08,GETDATE()),
           
           (11,1,4.59,GETDATE()),
           (11,2,0.71,GETDATE()),
           (11,3,0.62,GETDATE()),
           (11,4,79.85,GETDATE()),
           (11,5,6.35,GETDATE()),
           (11,6,5.80,GETDATE()),
           (11,7,0.54,GETDATE()),
           (11,8,1.51,GETDATE()),
           (11,9,19.92,GETDATE()),
           (11,10,0.92,GETDATE())           
GO