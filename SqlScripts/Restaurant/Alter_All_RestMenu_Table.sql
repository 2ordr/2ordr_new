
/*[RestaurantMenuItem]*/

UPDATE [dbo].[RestaurantMenuItem] SET IsActive = 1 where IsActive IS NULL
GO

ALTER TABLE [dbo].[RestaurantMenuItem]
ALTER COLUMN [IsActive] [bit]  NOT NULL
GO

/*[RestaurantMenuGroup]*/

ALTER TABLE [dbo].[RestaurantMenuGroup] 
ADD CONSTRAINT Default_IsActive DEFAULT(1) for [IsActive]
GO

UPDATE [dbo].[RestaurantMenuGroup] SET IsActive = 1 where IsActive IS NULL
GO

ALTER TABLE [dbo].[RestaurantMenuGroup] 
ALTER COLUMN [IsActive] [bit]  NOT NULL 
GO


/*[RestaurantMenu]*/

ALTER TABLE [dbo].[RestaurantMenu] 
ADD CONSTRAINT Default_IsActiveRest_Menu DEFAULT(1) for [IsActive]
GO

UPDATE [dbo].[RestaurantMenu] SET IsActive = 1 where IsActive IS NULL
GO

ALTER TABLE [dbo].[RestaurantMenu] 
ALTER COLUMN [IsActive] [bit]  NOT NULL 
GO

