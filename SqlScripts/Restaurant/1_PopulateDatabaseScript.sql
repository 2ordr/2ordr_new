 /*Customer Details*/  
INSERT INTO [dbo].[Customer]
           ([FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Email]
           ,[Password]
           ,[ActiveStatus]
           ,[ResetPasswordCode]
           ,[ActivationCode]
           ,[ProviderKey]
           ,[ProviderName]
           ,[LastUpdated]
           ,[CreationDate])
  VALUES
	   ('rest','Nobel','2001-8-9','MG-Road','','Panjim','Goa','India',403501,1234567890,'rest@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Andrew','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,2234567890,'andrew@gmail.com','qwerty',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Alena','Helly',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,6244567890,'hellydays776@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Eugene','Martul',GETDATE(),'','','','Goa','Denmark',403501,7254567890,'ifgen363@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Rajesh','Naik','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'rajesh5@certigoa.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Anastasia','Suhovei','1990-7-2','','','','','',403501,8234567890,'a.suhovei+1@besk.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('HSV','heben','1990-7-2','Curti','','','','',403501,8234567890,'a.suhovei@besk.coh','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('HSV','hebrvehehrbvrvrvrbrvrvvrbrhrbrvrbehhrbebjr','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'hshvsb@hdb.gh','123123',1,null,null,null,null,GETDATE(),GETDATE())
	   
GO
	   
 
 /* Restaurant_Details*/
 INSERT INTO [dbo].[Restaurant]
           ([Name]
           ,[Description]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[CostForTwo]
           ,[IsPartnered]
           ,[CreationDate]
           ,[Location]
           ,[Longitude]
           ,[Latitude]
           ,[BackgroundImage]
           ,[LogoImage]
           ,[OpeningHours]
           ,[ClosingHours])
     VALUES
	( 'Charolais Kroen', 'Denmark Best', 'Fonnesbechsgade 19A','','Herning','Goa','Denmark','7400','4597213550','','ka@asdf.com','http://www.charolaiskroen.dk/',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','07:00:00','23:00:00' ),
	( 'Estuary Cafe', 'Best Indian', 'MG Road','','Panaji','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'Cafe Lilliput', 'Best Indian', 'MG Road','','Margao','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'Viva Panjim', 'Best Indian', 'MG Road','','Anjuna','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','08:00:00','23:00:00'),
	( 'Mosaic Restaurant', 'Best Indian', 'MG Road','','Ponda','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','10:00:00','23:00:00'),
	( 'AZ.U.R - The Marriott', 'Best Indian', 'MG Road','','Miramar','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','09:00:00','23:00:00' )

GO
 /*Restaurant Reviews*/
INSERT INTO [dbo].[RestaurantReview]
           ([RestaurantId]
           ,[CustomerId]
           ,[Score]
           ,[Comment])
     values
		   (1,1,4,'This is really great place'),
		   (1,2,3,'My favorite restaurant!'),
		   (1,3,4,'My favorite restaurant!'),
		   (2,1,4,'This is really great place'),
		   (2,2,3,'My favorite restaurant!'),
		   (2,3,4,'My favorite restaurant!'),
		   (3,1,4,'This is really great place'),
		   (3,2,3,'My favorite restaurant!'),
		   (4,3,4,'My favorite restaurant!'),
		   (5,1,4,'This is really great place'),
		   (6,2,3,'My favorite restaurant!'),
		   (1,4,4,'I like this one..')
GO

/*Restaurant Menu*/	
INSERT INTO [dbo].[RestaurantMenu]
           ([RestaurantId]
           ,[MenuName]
           ,[MenuDescription]
           ,[IsActive]
           )
     VALUES
           (1,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (1,'Future Menu', 'This will active in future during vacations',0),
		  (2,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (3,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (3,'Old Menu', 'Menu for special occasions',0)
GO

/*Restaurant Menu Group*/
INSERT INTO [dbo].[RestaurantMenuGroup]
           ([RestaurantMenuId]
           ,[GroupName]
           ,[GroupDescription]
           ,[IsActive])
     VALUES
           (1,'Forret', 'Forret',1),
		  (1,'Stegeretter', 'Stegeretter descripton',1),
		  (1,'Desserter', 'Desserter descripton',1),
		  (1,'B�rnemenu', 'currently not active ',0),
		  (3,'Breakfast', 'Good Morning with deliciious breakfast',1),
		  (3,'Starters', 'Start with deliciious breakfast',1),
		  (3,'Sea Food', 'Start with deliciious breakfast',1),
		  (3,'Indian Curries', 'Start with deliciious breakfast',1)
GO

/*Restaurant Menu Item*/
INSERT INTO [dbo].[RestaurantMenuItem]
           ([RestaurantMenuGroupId]
           ,[ItemName]
           ,[ItemDescription]
           ,[ImagePrimary]
           ,[Price]
           ,[TaxId]
           ,[IsActive]
           )
     VALUES
           (1,'Gravad Laks', 'Pickled Salmon','Image1.jpg',95,null,1),
		  (1,'Scampi Fritti', 'med pikant peberdressing Deep-fried scampi with piquant green madagascar dressing','Image1.jpg',95,null,1),
		  (2,'Morbrad', 'Reelt stykke k�d- fuldt afpareret Extremely tender','Image1.jpg',299,null,1),
		  (2,'Ribeye', 'Grov mamoreret, kraftig smag The marbling enhances the taste of beef ','Image1.jpg',299,null,1),
		  (3,'Banan aux four', 'Bagt banan med jordb�ris, fl�deskum, chocoladesauce og n�dder','Image1.jpg',65,null,1),
		  (5,'Plain Omlet', null,'Image1.jpg',60,null,1),
		  (5,'Fried Egg', 'Healty, tasty fries','Image1.jpg',70,null,1),
		  (6,'Fish Finger', 'Start with deliciious dish','Image1.jpg',150,null,1)
GO

/*Menu Additional Group*/
INSERT INTO [dbo].[MenuAdditionalGroup]
           ([GroupName]
           ,[GroupDescription]
           ,[MinimumSelected]
           ,[MaximumSelected]
           ,[IsActive]
           )
     VALUES
           ('SELECT OIL', 'Tell us how you want fished to be fried',1,1,1),
	  ('Cooking', 'Degree of Roasting',1,1,1)
GO

/*Menu Additional Group*/
INSERT INTO [dbo].[MenuAdditionalElement]
           ([MenuAdditionalGroupId]
           ,[AdditionalElementName]
           ,[AdditionalElementDescription]
           ,[AdditionalCost]
           ,[IsActive]
           )
     VALUES
           (1,'Palm Oil', 'Fried with Palm Oil',10,1),
		  (1,'Soya Oil', 'Fried with Soya Oil',14,1),
		  (1,'Olive Oil', 'Fried with Olive Oil',21,1),
		  (2,'50�C - Rare', 'Description here',10,1),
		  (2,'55�C - Medium', 'Description here',14,1),
		  (2,'60�C - Medium Well', 'Description here',21,1)
GO	

/*Menu Item Additional*/
INSERT INTO [dbo].[MenuItemAdditional]
           ([RestaurantMenuItemId]
           ,[MenuAdditionalGroupId]
           ,[IsActive]
           )
     VALUES
           (1,1, 1),
 		  (1,2, 1),
		  (3,1,1)
GO

/*Menu Item Size*/
INSERT INTO [dbo].[MenuItemSize]
           ([RestaurantMenuItemId]
           ,[SizeName]
           ,[SizePrice]
           ,[SizeCalories]
           ,[IsDefault]
           ,[IsActive])
     VALUES
           (1,'Half', 100,50,1,1),
	  (1,'Full', 195,115,0,1)
GO

/*Ingredient*/
INSERT INTO [dbo].[Ingredient]
           ([ParentId]
           ,[Name]
           ,[Description])
     VALUES
           (null,'Salmon', 'raw salmon'),
		  (null,'Egg', 'Whole Egg'),
		  (2,'Egg Yolk', 'Yolk of egg')
GO

/*Menu Item Ingredient*/
INSERT INTO [dbo].[MenuItemIngredient]
           ([RestaurantMenuItemId]
           ,[IngredientId]
           ,[Qty])
     VALUES
			(1,1,'100gms'),
			(2,2,'2Nos')
GO

/*Customer Ingredient Preference*/
INSERT INTO [dbo].[CustomerIngredientPreference]
           ([CustomerId]
           ,[IngredientId]
           ,[Include])
     VALUES
			(5,2,0),
			(5,1,1)
GO

/*Food Life Style*/
INSERT INTO [dbo].[FoodLifeStyle]
           ([Name]
           ,[Description])
     VALUES
			('Dairy Free','No to all dairy Items'),
			('Egg Free', 'No to all egg items'),
			('Fish Free', 'No to all egg items'),
			('Onion Free', 'No to all onion items')
GO

/*Life Style Ingredient*/
INSERT INTO [dbo].[LifeStyleIngredient]
           ([LifeStyleId]
           ,[IngredientId]
           ,[Include])
     VALUES
           (2,2,0),
           (3,2,0),
           (1,2,0)
      
GO
/*Customer Life Style Preference*/
INSERT INTO [dbo].[CustomerLifeStylePreference]
           ([LifeStyleId]
           ,[CustomerId])
     VALUES
           (2,5),
           (2,2),
           (2,1)
GO

/*Allergen*/
INSERT INTO [dbo].[Allergen]
           ([Name]
           ,[Description])
     VALUES
			('Egg','Egg Products'),
			('Fish','Fish Products'),
			('Gluten','Gluten Products'),
			('Milk','Milk Products'),
			('Peanuts','Peanut Products'),
			('Soya','Soya Products'),
			('Sulphites','Sulphite Products'),
			('Nuts','Nuts')
GO

/*Allergen Ingredient*/
INSERT INTO [dbo].[AllergenIngredient]
           ([AllergenID]
           ,[IngredientID])
     VALUES
			(1,2),
			(1,3),
			(2,1)
GO

/*Customer Allergen Preference*/
INSERT INTO [dbo].[CustomerAllergenPreference]
           ([AllergenId]
           ,[CustomerId])
     VALUES
           (1,5),
           (1,2),
           (2,4),
           (1,2),
           (3,3)
GO

/*Menu Item Review*/
INSERT INTO [dbo].[MenuItemReview]
           ([RestaurantMenuItemId]
           ,[Score]
           ,[CustomerId]
           ,[Comment])
     VALUES
			(1,4,1,'It was delicious'),
			(1,3,1,'It was OK'),
			(2,5,2,'It was delicious, I liked it very much'),
			(3,3,2,'It was delicious, I liked it very much'),
			(4,1,1,'It was OK'),
			(4,4,2,'It was delicious, I liked it very much')
GO

/*Cuisine*/
INSERT INTO [dbo].[Cuisine]
           ([Name]
           ,[IsActive])
     VALUES
			('Austrian',1),
			('German',1),
			('Indian',1),
			('Italian',0),
			('Czech Wine',0)
GO

/*Restaurant Cuisine*/
INSERT INTO [dbo].[RestaurantCuisine]
           ([RestaurantId]
           ,[CuisineId]
           ,[IsActive])
     VALUES
			(1,1,1),
			(1,2,1),
			(2,1,1),
			(2,1,3),
			(3,2,1),
			(4,1,1),
			(5,2,1),
			(6,3,1)
GO

/*Restaurant Table*/
INSERT INTO [dbo].[RestaurantTable]
           ([TableNumber]
           ,[RestaurantId]
           ,[TotalSeats]
           ,[Description]
           ,[IsActive])
     VALUES
			(101,1,6,'description if any here',1),
			(102,1,6,'description if any here',1),
			(201,2,6,'description if any here',1),
			(202,2,6,'description if any here',1)
GO
/*Restaurant Cart*/
INSERT INTO [dbo].[RestaurantCart]
           ([CustomerId])
     VALUES
			(1),
			(2),
			(5)
GO

/*Restaurant Cart Item*/
INSERT INTO [dbo].[RestaurantCartItem]
           ([RestaurantCartId]
           ,[RestaurantMenuItemId]
           ,[Qty])
     VALUES
			(1,1,1),
			(1,2,4),
			(3,1,1),
			(3,2,4)
GO

/*Restaurant Cart Item Additional*/
INSERT INTO [dbo].[RestaurantCartItemAdditional]
           ([CartItemId]
           ,[MenuAdditionalElementId])
     VALUES
			(1,2),
			(3,1)
GO

/*Restaurant Order*/
INSERT INTO [dbo].[RestaurantOrder]
           ([CustomerId]
           ,[RestaurantId]
           ,[CustomerTableNumber]
           ,[OrderStatus])
     VALUES
			(5,1,1,1),
			(5,1,1,2),
			(5,2,1,1),
			(5,2,1,2),
			(2,1,1,1),
			(2,1,1,2),
			(3,3,1,1)
GO

/*Restaurant Order Item*/
INSERT INTO [dbo].[RestaurantOrderItem]
           ([RestaurantOrderId]
           ,[RestaurantMenuItemId]
           ,[Qty])
     VALUES
			(1,1,1),
			(1,2,4),
			(3,1,1),
			(3,2,4) 
GO

/*Restaurant Order Item Additional*/
INSERT INTO [RestaurantOrderItemAdditional]
           ([RestaurantOrderItemId]
           ,[MenuAdditionalElementId])
     VALUES
			(2,2),
			(3,1)
GO

/*Restaurant Order Review*/
INSERT INTO [dbo].[RestaurantOrderReview]
           ([RestaurantOrderId]
           ,[Score]
           ,[Comment]
           )
     VALUES
			(1,4, 'It was good' ),
			(2,3, 'Food was ok' ),
			(3,5, 'Superb' ),
			(4,1, 'Food was not good. I will never visit this restaurant again.' ),
			(5,5, 'Very good food' )
GO

/*Customer Credit Card*/
INSERT INTO [dbo].[CustomerCreditCard]
           ([CustomerId]
           ,[CardHolderName]
           ,[CardNumber]
           ,[CardExpiryDate]
           ,[CVVNumber]
           ,[IsPrimary]
           ,[CardLabel]
           ,[CardLabelColor]
           ,[SpendingLimitEnabled]
           ,[SpendingLimitAmount]
           ,[Currency]
           ,[Duration])
     VALUES
			(1,'Panki Leo','2223000010476528','2022/10/12','1234',1,'Partner Card','#12323',1,1200,'EURO',1),
			(5,'Rajesh Naik','2343000010476528','2022/02/16','1234',1,'Mycard','#12323',1,1200,'EURO',1),
			(5,'Rajesh Naik','1143000010476528','2018/03/16','1224',0,'Wify card','#22323',null,null,null,null)

	   
GO

/*Paypal Details*/
INSERT INTO [dbo].[PaypalDetails]
           ([CustomerId]
           ,[PaypalEmail]
           ,[Password])
     VALUES
           (1,'test@gmail.com','pass123'),
           (2,'test2@gmail.com','pass123'),
           (4,'test3@gmail.com','pass123'),
           (5,'test4@gmail.com','pass123')
GO

/*Customer Favorite Restaurant*/
INSERT INTO [dbo].[CustomerFavoriteRestaurant]
           ([CustomerId]
           ,[RestaurantId])
     VALUES
           (1,1),
           (1,2),
           (2,3)
GO




	   






   




