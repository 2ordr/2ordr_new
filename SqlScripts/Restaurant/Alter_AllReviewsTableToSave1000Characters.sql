  
  Alter table [HotelReview]
  Alter column [Comment] nvarchar(1000)
  Go
  
  Alter table [HotelServicesReviews]
  Alter column [Comment] nvarchar(1000)
  Go
  
  Alter table [MenuItemReview]
  Alter column [Comment] nvarchar(1000)
  Go
  
  Alter table [RestaurantReview]
  Alter column [Comment] nvarchar(1000)
  Go
  
  Alter table [RestaurantOrderReview]
  Alter column [Comment] nvarchar(1000)
  Go
  
  Alter table [RoomReview]
  Alter column [Comment] nvarchar(1000)
  Go