
  Alter table [RestaurantCart]
  Add TableId int NULL  constraint [FK_RestaurantCart_TableId] foreign key(TableId) references RestaurantTable(Id)
  Go
  
  
  Alter table [RestaurantCart]
  Add RoomId int NULL  constraint [FK_RestaurantCart_RoomId] foreign key(RoomId) references Room(Id)
  Go
  