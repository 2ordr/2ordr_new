


CREATE TABLE [dbo].[AllergenCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_AllergenCategories]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_AllergenCategories_ID] PRIMARY KEY(Id),
 )
GO

INSERT INTO [dbo].[AllergenCategories]
           ([Name],[Description],[IsActive])
    VALUES
           ('Test' ,'test',1)
GO


  Alter Table [Allergen]
  Add AllergenCategoryId int NOT NULL constraint[DF_AKID] default 1
  Go
  
  Alter Table [Allergen]
  Drop Constraint [DF_AKID]
  Go
  
  Alter Table [Allergen]
  Add constraint [FK_Allergen_AllergenCategoryId] foreign key(AllergenCategoryId)
  References AllergenCategories(Id)
  Go