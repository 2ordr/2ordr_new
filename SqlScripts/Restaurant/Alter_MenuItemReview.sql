
Alter table [MenuItemReview]
ADD RestaurantId int  Constraint FK_Restaurant_RestaurantId  Foreign Key (RestaurantId)
REFERENCES Restaurant(Id)
Go	

update MenuItemReview set RestaurantId=1 where RestaurantId is null
Go

Alter table [MenuItemReview]
Alter column RestaurantId int Not null
Go