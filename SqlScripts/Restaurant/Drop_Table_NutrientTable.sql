
DROP TABLE [dbo].[IngredientNutrition] 
GO


DROP TABLE [dbo].[Nutrient]
GO

CREATE TABLE [dbo].[MenuItemIngredientNutrition]
 (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [MenuItemIngredientId] [int] NOT NULL,
  [Name] [nvarchar](300) NOT NULL,
  [NutritionValue] [float] NOT NULL,
  [DailyPercentageValue ] [float] NULL,
 CONSTRAINT [PK_MenuItemIngredientNutrition_ID] PRIMARY KEY([Id]),
 
 CONSTRAINT [FK_MenuItemIngredientNutrition_MenuItemIngredientId] FOREIGN KEY([MenuItemIngredientId])
   REFERENCES [dbo].[MenuItemIngredient] ([Id])      
 )
GO