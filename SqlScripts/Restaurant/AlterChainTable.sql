

ALTER TABLE [ChainTable]
DROP CONSTRAINT [FK_ChainTable_CustID]
GO

ALTER TABLE [ChainTable]
DROP COLUMN [CustomerId];
GO


ALTER TABLE [ChainTable]
ADD [Description] NVARCHAR(300) NULL;
GO

ALTER TABLE [CustomerRole]
ADD [ChainId] INT NULL;
GO

ALTER TABLE [CustomerRole]
ADD CONSTRAINT [FK_CustomerRole_CustID] FOREIGN KEY([ChainId])
REFERENCES [ChainTable] ([Id])
GO


