

Alter Table [dbo].[MenuItemReview] 
ADD [CreationDate] [datetime]  NULL
GO

Update dbo.MenuItemReview set CreationDate = GETDATE()where CreationDate is NULL
GO

Alter table [dbo].[MenuItemReview]
ALTER Column [CreationDate] [datetime] NOT NULL
GO
