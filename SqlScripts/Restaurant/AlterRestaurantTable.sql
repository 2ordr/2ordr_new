
ALTER TABLE [Restaurant]
DROP COLUMN [Country];



ALTER TABLE [Restaurant]
ADD CountryId INT NULL;



ALTER TABLE [Restaurant]
ADD CONSTRAINT [FK_REST_COUNTRY_ID] FOREIGN KEY([CountryId])
REFERENCES [Country] ([Id])
GO