
Alter table [RestaurantMenuItem]
ADD LanguageId INT NOT NULL Default(1);
Go

ALTER TABLE [RestaurantMenuItem]
ADD CONSTRAINT [FK_RESTMENUITEM_LANGUAGE_ID] FOREIGN KEY(LanguageId)
REFERENCES [Language] ([Id])
GO

CREATE TABLE [dbo].[RestaurantMenuItemTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[TranslatedItemName] [nvarchar](100) NOT NULL,
	[TranslatedItemDescription] [nvarchar](255) NULL
 CONSTRAINT [PK_Restaurant_Menu_Item_Translation] PRIMARY KEY(Id),
 Constraint FK_Language_RMIT_languageID Foreign Key (LanguageId) References Language(Id),
 Constraint FK_RestaurantMenuItem_ItemId foreign Key(RestaurantMenuItemId)References RestaurantMenuItem(Id))
GO