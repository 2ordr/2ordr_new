
ALTER TABLE [dbo].[RestaurantOrder]
 ADD [IsActive] [bit] NOT NULL constraint [DF_RestOrderIsActive] default 1
GO

--view to show all restaurants served orders and hotels served orders
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Alter view [dbo].[VW_CustomerOrdersHistory] As 
select distinct 'RO_'+cast(ro.Id as varchar) as Id, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber,
ro.IsActive
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.OrderStatus=4

GO