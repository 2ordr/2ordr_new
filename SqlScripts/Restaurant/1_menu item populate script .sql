/*Adding new restaurant and populating restaurant menu,group and items script */
 /* Restaurant_Details*/
 INSERT INTO [dbo].[Restaurant]
           ([Name]
           ,[Description]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[CostForTwo]
           ,[IsPartnered]
           ,[CreationDate]
           ,[Location]
           ,[Longitude]
           ,[Latitude]
           ,[BackgroundImage]
           ,[LogoImage]
           ,[OpeningHours]
           ,[ClosingHours])
     VALUES
			( 'Hotel Navtara', 'Goa Best Restaurant', 'Calangute - Mapusa Rd, Oppsoite Bodgeshwar Temple','','Mapusa','Goa','India','403507','08326512660','','ka@asdf.com','http://www.navtara.in/',200,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','07:00:00','23:00:00' )

GO
/*Restaurant Menu*/
INSERT INTO [dbo].[RestaurantMenu]
           ([RestaurantId]
           ,[MenuName]
           ,[MenuDescription]
           ,[IsActive]
           )
     VALUES
           ((Select MAX(Id) from Restaurant),'Navtara Menu', 'This is currently active Menu of the restaurant',1)  
Go


Declare @res_Menu_id int
Select @res_Menu_id = MAX(Id) from RestaurantMenu

/*Restaurant Menu Group*/
INSERT INTO [dbo].[RestaurantMenuGroup]
           ([RestaurantMenuId]
           ,[GroupName]
           ,[GroupDescription]
           ,[IsActive])
     VALUES
		  (@res_Menu_id,'Goan', '',1),
		  (@res_Menu_id,'Sout Indian', '',1),
		  (@res_Menu_id,'Sandwiches & Burgers', '',1),
		  (@res_Menu_id,'Pizza', '',1),
		  (@res_Menu_id,'Pav Bhaji(11am to Onwards)', '',1),
		  (@res_Menu_id,'Chaat(4pm to 9pm)', '',1),
		  (@res_Menu_id,'Thali(South Indian)', '',1),
		  (@res_Menu_id,'Starters', '',1),
		  (@res_Menu_id,'Soups', '',1),
		  (@res_Menu_id,'North Indian', '',1),
		  (@res_Menu_id,'Tandoori(Indian Bread)', '',1),
		  (@res_Menu_id,'Rice', '',1),
		  (@res_Menu_id,'Salad & Raita', '',1),
		  (@res_Menu_id,'Chinese Vegetables', '',1),
		  (@res_Menu_id,'Noodles', '',1),
		  (@res_Menu_id,'Fried Rice', '',1),
		  (@res_Menu_id,'Juices (Seasonal)', '',1),
		  (@res_Menu_id,'Milk Shakes(Seasonal)', '',1),
		  (@res_Menu_id,'Falooda', '',1),
		  (@res_Menu_id,'Lassi', '',1),
		  (@res_Menu_id,'Kulfi', '',1),
		  (@res_Menu_id,'Ice Creams', '',1),
		  (@res_Menu_id,'Desserts', '',1),
		  (@res_Menu_id,'Sweets', '',1),
		  (@res_Menu_id,'Cold Beverages', '',1),
		  (@res_Menu_id,'Hot Beverages', '',1),
		  (@res_Menu_id,'Extras', '',1)
 
Go

Declare @menuGroup_id int
Select @menuGroup_id = MAX(Id) from RestaurantMenuGroup

/*Restaurant Menu Item*/
INSERT INTO [dbo].[RestaurantMenuItem]
           ([RestaurantMenuGroupId]
           ,[ItemName]
           ,[ItemDescription]
           ,[ImagePrimary]
           ,[Price]
           ,[TaxId]
           ,[IsActive]
           )
     VALUES
		--Goan
	--Goan
	  (@menuGroup_id - 26,'Bun', '','Image1.jpg',20,null,1),
	  (@menuGroup_id - 26,'Chapati', '','Image1.jpg',20,null,1),
	  (@menuGroup_id - 26,'Potato Vada', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 26,'Veg Samosa', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 26,'Sukha Bhaji Pav', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 26,'Patal Bhaji Pav', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 26,'Mix Bhaji Pav', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 26,'Sukha Bhaji Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 26,'Patal Bhaji Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 26,'Mix Bhaji Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 26,'Alsande Bhaji Pav', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 26,'Chana Bhaji Pav', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 26,'Alsande Bhaji Puri', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 26,'Chana Bhaji Puri', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 26,'Veg Xacuti Pav', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 26,'Veg Xacuti Puri', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 26,'Mashroom Xacuti Pav', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 26,'Mashroom Xacuti Puri', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 26,'Mashroom Xacuti Paratha', '','Image1.jpg',80,null,1),
	  
	  --South Indian
	  (@menuGroup_id - 25,'Upama', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 25,'Idli', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 25,'Vada', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 25,'Idli/Vada', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 25,'Dahi Vada', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 25,'Cutlet', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 25,'Sada Dosa', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 25,'Masala Dosa', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 25,'Mysore Dosa', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 25,'Mysore Masala Dosa', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Ghee Masala Dosa', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Butter Masala Dosa', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Palak Dosa', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 25,'Palak Masala Dosa', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Schezwan Dosa', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Schezwan Masala Dosa', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 25,'Rava Dosa', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 25,'Rava Masala Dosa', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 25,'Paper Dosa', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 25,'Paper Masala Dosa', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 25,'Cheese Dosa', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 25,'Cheese Masala Dosa', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 25,'Mashroom Chilly Fry Dosa', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 25,'Paneer Dosa', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 25,'Onion Uttapam', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Tomato Uttapam', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 25,'Ghee Onion Uttapam', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 25,'Butter Onion Uttapam', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 25,'Cheese Onion Uttapam', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 25,'Mashroom Chilly Uttapam', '','Image1.jpg',110,null,1),
	  
	  --Sandwiches & Burgers
	  (@menuGroup_id - 24,'Bread Butter ', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 24,'Bread Jam', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 24,'Toast Butter', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 24,'Toast Jam', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 24,'Veg. Sandwich', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 24,'Veg. Grilled Sandwich', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 24,'Cheese Sandwich', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 24,'Cheese Grilled Sandwich', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 24,'Veg. Cheese Sandwich', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 24,'Cheesy-Chilly Grilled Sandwich', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 24,'CTC Toast(Cheese)', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 24,'CTC Toast(Tomato)', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 24,'CTC Toast(Capcicum)', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 24,'Club Sandwich', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 24,'Veg. Cheese Burger', '','Image1.jpg',90,null,1),
	  
	  --Pizza
	  (@menuGroup_id - 23,'Margherita Pizza', '','Image1.jpg',140,null,1),
	  (@menuGroup_id - 23,'A la Italiano(Mushroom)', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 23,'A la Italiano(Capsicum)', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 23,'A la Italiano(Baby Corn)', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 23,'Paneer Dhamak(Paneer)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Paneer Dhamak(Capsicum)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Paneer Dhamak(Onion)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Passareli Pizza(Chineese)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Passareli Pizza(Tomato)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Passareli Pizza(Capsicum)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Passareli Pizza(Babycorn)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Primavera Pizza(Chineese)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Primavera Pizza(Tomato)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Primavera Pizza(Capsicum)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Primavera Pizza(Babycorn)', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 23,'Navtara Premium(Bell Pepper)', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 23,'Navtara Premium(Olive)', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 23,'Navtara Premium(Corn Mushroom)', '','Image1.jpg',90,null,1),

	  --Pav Bhaji
	  (@menuGroup_id - 22,'Special Pav Bhaji', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 22,'Jain Pav Bhaji', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 22,'Cheese Pav Bhaji', '','Image1.jpg',130,null,1),
	  (@menuGroup_id - 22,'Paneer Pav Bhaji', '','Image1.jpg',130,null,1),
	  (@menuGroup_id - 22,'Bhaji Only', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 22,'Butter Pav', '','Image1.jpg',10,null,1),
	  
	  --Chaat
	  (@menuGroup_id - 21,'Bhel Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Shev Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Shev Batata Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Shev Dahi Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Shev Batata Dahi Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Masala Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Pani Puri', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 21,'Ragada Patties', '','Image1.jpg',60,null,1),

	  	  --Lunch & Dinner
	  --thali
	  (@menuGroup_id - 20,'Veg Thali', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 20,'Special Thali', '','Image1.jpg',150,null,1),
	
		--Starters
	  (@menuGroup_id - 19,'Fried Papad', '','Image1.jpg',10,null,1),
	  (@menuGroup_id - 19,'Roasted Papad', '','Image1.jpg',10,null,1),
	  (@menuGroup_id - 19,'Masala Papad(Fried)', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 19,'Masala Papad(Roasted)', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 19,'French fries', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 19,'Chinese Bhel', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 19,'Paneer Sticks', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 19,'Cheese Pakode', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 19,'Paneer Pakode', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 19,'Paneer Tikka', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 19,'Paneer 65', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 19,'Spring Roll', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 19,'Mushroom Tika', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 19,'Mushroom Pepper', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 19,'Veg. Lollipop', '','Image1.jpg',200,null,1),
	  
	  --Soups
	  (@menuGroup_id - 18,'Sweet Corn Veg.', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Cream of Veg', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Cream of Palak', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Cream of Tomato', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Mushroom', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Hot "n" Sour Veg', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 18,'Veg. Manchow', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 18,'Caldo Verde(Portugues Potalo Soup)', '','Image1.jpg',90,null,1),
	  
	  --North Indian
	  (@menuGroup_id - 17,'Aloo Gobi', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Aloo Palak', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Aloo Mutter', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Aloo Methi', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Aloo Jeera', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Aloo Dum Punjabi', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Bhindi Masal', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Baigan Bhartha', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 17,'Babycorn Masala', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Babycorn Capsicum', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Chana Masal', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 17,'Chole Bhature', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Dal Fry', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 17,'Dal Palak', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 17,'Dal Makhani', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 17,'Dal Tadka', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 17,'Dal Methi', '','Image1.jpg',150,null,1),
	  (@menuGroup_id - 17,'Kaju Masala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Kadhai La Jawab', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Kadhai Hyderabadi', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Mushroom Masala', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Mushroom Keema', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Mushroom Methi Mutter', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Mushroom Tikka Masala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Mushroom Tawa Masala', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 17,'Malai Kofta', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Paneer Kofta', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Shahi', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Palak', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Mutter', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Methi', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Bhurji', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Kadhai', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Lababdar', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Makhanwala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Tika', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Tawa Masala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Butter Masala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Paneer Babycorn Masala', '','Image1.jpg',210,null,1),
	  (@menuGroup_id - 17,'Veg. Kofta', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Keema', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Kadhai', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Handi', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Jalfrezi', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Chilly-Milly', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Hyderabadi ', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Kolhapuri', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Makhanwala', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 17,'Veg. Maharaja', '','Image1.jpg',210,null,1),
	  
	  	  --Tandoori(indian Bread)
	  (@menuGroup_id - 16,'Roti', '','Image1.jpg',20,null,1),
	  (@menuGroup_id - 16,'Butter Roti', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Garlic Roti', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Missi Roti', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Paratha', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Butter Paratha', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Kerala Paratha(8am to 11am)', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 16,'Stuffed Paratha(Aloo)', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 16,'Stuffed Paratha(Gobi)', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 16,'Stuffed Paratha(Veg Methi)', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 16,'Naan', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 16,'Butter Naan', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 16,'Butter Garlic Naan', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 16,'Cheese Stuffed Naan', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 16,'Cheese Stuffed Garlic Naan', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 16,'Onion Kulcha', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 16,'Butter Onion Kulcha', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 16,'Paneer Lulcha', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 16,'Butter Kulcha', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 16,'Bhatura', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 16,'Puri(3pcs)', '','Image1.jpg',30,null,1),
	   
	   ---Rice
	  (@menuGroup_id - 15,'Plain Rice', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 15,'Basmati Rice', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 15,'Curd Rice', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 15,'Tomato Rice', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 15,'Ghee Rice', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 15,'Jeera Rice', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 15,'Dal Khichadi Rice', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 15,'Veg pulao', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 15,'Peas Pulao', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 15,'Veg. Biryani', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 15,'Hyderabadi Biryani', '','Image1.jpg',170,null,1),

	   ---Salad & Raita
	  (@menuGroup_id - 14,'Mixed Salad', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 14,'Onion Salad', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 14,'Mixed Vegetable Raita', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 14,'Raita(Aloo)', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 14,'Raita(Boondi)', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 14,'Raita(Pineapple)', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 14,'Paneer Veg Salad', '','Image1.jpg',100,null,1),
	  
		--Chinese Vegetables
	  (@menuGroup_id - 13,'Cauliflower Manchurian', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 13,'Veg Manchurian', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 13,'Mushroom Manchurian', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 13,'Babycorn Manchurian', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 13,'Paneer Schezwan', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 13,'Paneer Chilly', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 13,'Paneer Garlic', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 13,'Paneer Ginger', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 13,'Paneer 65', '','Image1.jpg',190,null,1),
	  (@menuGroup_id - 13,'Idli Chilly Fry(Dry)', '','Image1.jpg',130,null,1),
	  (@menuGroup_id - 13,'Mushroom Ginger Garlic', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 13,'Mushroom With Babycorn', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 13,'Veg. 65', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 13,'Gobi 65', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 13,'Veg Crispy(Dry)', '','Image1.jpg',200,null,1),
	  
	  ---Noodles
	  (@menuGroup_id - 12,'Hakka Noddles', '','Image1.jpg',140,null,1),
	  (@menuGroup_id - 12,'Mushroom Hakka Noodles', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 12,'Hong Kong Noodles', '','Image1.jpg',140,null,1),
	  (@menuGroup_id - 12,'Singapore Noodles', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 12,'Schezwan Noodles', '','Image1.jpg',150,null,1),
	  
	  --Fried Rice
	  (@menuGroup_id - 11,'Veg. Fried Rice', '','Image1.jpg',160,null,1),
	  (@menuGroup_id - 11,'Burnt Garlic Fried Rice', '','Image1.jpg',170,null,1),
	  (@menuGroup_id - 11,'Mushroom Fried Rice', '','Image1.jpg',180,null,1),
	  (@menuGroup_id - 11,'Paneer Fried Rice', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 11,'Schezwan Fried Rice', '','Image1.jpg',170,null,1),
	  (@menuGroup_id - 11,'Triple Schezwan(Fried Rice)', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 11,'Triple Schezwan(Noodles)', '','Image1.jpg',200,null,1),
	  (@menuGroup_id - 11,'Triple Schezwan(Gravy)', '','Image1.jpg',200,null,1),
	  
	    --Juices
	  (@menuGroup_id - 10,'Fresh Lime', '','Image1.jpg',40,null,1),
	  (@menuGroup_id - 10,'Sweet Lime', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 10,'Orange', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 10,'Water Melon', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 10,'Pineapple', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 10,'Apple', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 10,'Mango', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 10,'Grape', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 10,'Strawbwrry', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 10,'Mixed Fruit', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 10,'Carrot', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 10,'Pomegranate', '','Image1.jpg',120,null,1),
	  
	      --Milk Shakes
	  (@menuGroup_id - 9,'Banana', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Papaya', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Guava', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 9,'Chickoo', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Mash Melon', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Apple', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Custard Apple(Sitaphal)', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Mango', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 9,'Mango Mastani', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 9,'Strawberry', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Vanilla', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Rose', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Kesar', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Butterscotch', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 9,'Chocolate', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Cold Coffee(With Vanilla ice-cream Rs.30.00 Extra)', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 9,'Bournvita(With Vanilla ice-cream Rs.30.00 Extra)', '','Image1.jpg',90,null,1),
	  
	    ---Falooda
	  (@menuGroup_id - 8,'Kulfi', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 8,'Royal', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 8,'Special', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 8,'Maharaja', '','Image1.jpg',130,null,1),
	  
	  	      --Lassi
	  (@menuGroup_id - 7,'Butter Milk(Masala Chaas)', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 7,'Sweet', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 7,'Salted', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 7,'Guava', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 7,'Chickoo', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 7,'Grape', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 7,'Papaya', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 7,'Banana With Honey', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 7,'Mango', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 7,'Strawberry', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 7,'Sitaphal(Seasonal)', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 7,'Choclate', '','Image1.jpg',80,null,1),
	  
		--Kulfi	
	  (@menuGroup_id - 6,'Malai', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 6,'Pista', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 6,'Mango', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 6,'Kesar Pista', '','Image1.jpg',90,null,1),
	  
		 --Ice Creame
	  (@menuGroup_id - 5,'Vanilla', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 5,'Strawberry', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 5,'Pista', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 5,'Mango', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 5,'Chocolate', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 5,'Butterscotch', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 5,'Gadbad', '','Image1.jpg',110,null,1),
	  (@menuGroup_id - 5,'Chocolate Nut Sandae', '','Image1.jpg',100,null,1),
	  (@menuGroup_id - 5,'Vanilla with Hot Chocolate Sauce', '','Image1.jpg',100,null,1),
	  
	  --Dessarts
	  (@menuGroup_id - 4,'Caramel Custard', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 4,'Fruit Salad', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 4,'Fruit Salad with Custard', '','Image1.jpg',70,null,1),
	  (@menuGroup_id - 4,'Fruit Salad with Ice Creame', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 4,'Jelly', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 4,'Jelly with Ice Creame', '','Image1.jpg',80,null,1),
	  (@menuGroup_id - 4,'Fruit Salad & Jelly with Ice Creame', '','Image1.jpg',90,null,1),
	  (@menuGroup_id - 4,'Mango & Creame(Seasonal)', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 4,'Strawberry & Creame(Seasonal)', '','Image1.jpg',120,null,1),
	  (@menuGroup_id - 4,'Chocolate Brownie With Ice Creame', '','Image1.jpg',80,null,1),
	  
	  	 --Sweets
	  (@menuGroup_id - 3,'Shira', '','Image1.jpg',50,null,1),
	  (@menuGroup_id - 3,'Srikhand(with 3 puri Rs.30.00 Extra)', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 3,'Gulab Jamun(With Vanilla Ice Creame Rs.30.00 extra)', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 3,'Rasgulla', '','Image1.jpg',60,null,1),
	  (@menuGroup_id - 3,'Rasmalai', '','Image1.jpg',90,null,1),
	  
	  --cold Beverages
	  (@menuGroup_id - 2,'Soda', '','Image1.jpg',10,null,1),
	  (@menuGroup_id - 2,'Bottled Water', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 2,'Cola', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 2,'Lime', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 2,'Orange', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 2,'Mango', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 2,'Kokum Cooler', '','Image1.jpg',50,null,1),
		 
		 --Hot Beverages
	  (@menuGroup_id - 1,'Tea', '','Image1.jpg',20,null,1),
	  (@menuGroup_id - 1,'Green Tea', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Special Tea', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Masala Tea', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Filter Coffee', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Nescafe', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Bournvita', '','Image1.jpg',30,null,1),
	  (@menuGroup_id - 1,'Chocolate', '','Image1.jpg',30,null,1),
	  
	  		 --Extras
	  (@menuGroup_id,'Jam(2 pieces)', '','Image1.jpg',20,null,1),
	  (@menuGroup_id,'Butter(2 pieces)', '','Image1.jpg',20,null,1),
	  (@menuGroup_id,'Cheese', '','Image1.jpg',30,null,1)
Go