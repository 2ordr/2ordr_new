CREATE TABLE [dbo].[RestaurantTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantOrderId] [int] NOT NULL,
	[TipAmount] [float] NOT NULL,
	[Comment][nvarchar](500) NULL,
	[CardId][int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[PaymentId] [nvarchar] (100) NULL,
 CONSTRAINT [PK_RestaurantTips_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_RestaurantTips_OrderId] FOREIGN KEY(RestaurantOrderId) REFERENCES RestaurantOrder(Id),
 CONSTRAINT [FK_RestaurantTips_CardId] FOREIGN KEY(CardId)REFERENCES CustomerCreditCard(Id)
 )
GO


