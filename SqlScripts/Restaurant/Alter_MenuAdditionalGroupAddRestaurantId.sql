
  Alter table [MenuAdditionalGroup]
  Add RestaurantId int NOT NULL constraint [Def_RestId] default(1)
  Go
  Alter table [MenuAdditionalGroup]
  Drop constraint [Def_RestId]
  Go
  Alter table [MenuAdditionalGroup]
  Add Constraint [FK_MenuAdditionalGroup_RestaurantId] FOREIGN KEY(RestaurantId)
  REFERENCES Restaurant(Id)
  Go
  