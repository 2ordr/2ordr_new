
ALTER TABLE [dbo].[RestaurantMenuItemImages]
DROP CONSTRAINT [FK_Restaurant_Menu_Items_Id] 
GO
   
ALTER TABLE [dbo].[RestaurantMenuItemImages]  WITH CHECK ADD  CONSTRAINT [FK_Restaurant_Menu_Items_Id] FOREIGN KEY([RestaurantMenuItemId])
REFERENCES [dbo].[RestaurantMenuItem] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RestaurantMenuItemImages] CHECK CONSTRAINT [FK_Restaurant_Menu_Items_Id]
GO


