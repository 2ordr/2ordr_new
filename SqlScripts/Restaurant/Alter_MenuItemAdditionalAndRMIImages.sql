
  Alter table [MenuItemAdditional]
  Alter column [IsActive] bit NOT NULL
  Go
  
  Alter table  dbo.RestaurantMenuItemImages
  Alter column IsActive bit NOT NULL
  Go
  
  Alter table  dbo.RestaurantMenuItemImages
  Alter column IsPrimary bit NOT NULL
  Go
  
  CREATE TABLE [dbo].[FoodProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(100),
	[Description] nvarchar(500),
	[IsActive] bit NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_FoodProfile DEFAULT GETUTCDate(),
CONSTRAINT [PK_FoodProfile_ID] Primary Key (Id)
)
Go

GO