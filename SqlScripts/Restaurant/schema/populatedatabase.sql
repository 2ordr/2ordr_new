 /*Customer Details*/  
INSERT INTO [dbo].[Customer]
           ([FirstName]
           ,[LastName]
           ,[BirthDate]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Email]
           ,[Password]
           ,[ActiveStatus]
           ,[ResetPasswordCode]
           ,[ActivationCode]
           ,[ProviderKey]
           ,[ProviderName]
           ,[LastUpdated]
           ,[CreationDate])
  VALUES
	   ('rest','Nobel','2001-8-9','MG-Road','','Panjim','Goa','India',403501,1234567890,'rest@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Andrew','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,2234567890,'admin@certigoa.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Alena','Helly',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,6244567890,'hellydays776@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Eugene','Martul',GETDATE(),'','','','Goa','Denmark',403501,7254567890,'ifgen363@gmail.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Rajesh','Naik','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'rajesh5@certigoa.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('Anastasia','Suhovei','1990-7-2','','','','','',403501,8234567890,'a.suhovei+1@besk.com','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('HSV','heben','1990-7-2','Curti','','','','',403501,8234567890,'a.suhovei@besk.coh','123123',1,null,null,null,null,GETDATE(),GETDATE()),
	   ('HSV','hebrvehehrbvrvrvrbrvrvvrbrhrbrvrbehhrbebjr','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'hshvsb@hdb.gh','123123',1,null,null,null,null,GETDATE(),GETDATE())
	   
GO


	   
 
 /* Restaurant_Details*/
 INSERT INTO [dbo].[Restaurant]
           ([Name]
           ,[Description]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[CostForTwo]
           ,[IsPartnered]
           ,[CreationDate]
           ,[Location]
           ,[Longitude]
           ,[Latitude]
           ,[BackgroundImage]
           ,[LogoImage]
           ,[OpeningHours]
           ,[ClosingHours])
     VALUES
	( 'Charolais Kroen', 'Denmark Best', 'Fonnesbechsgade 19A','','Herning','Goa','Denmark','7400','4597213550','','ka@asdf.com','http://www.charolaiskroen.dk/',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','07:00:00','23:00:00' ),
	( 'Estuary Cafe', 'Best Indian', 'MG Road','','Panaji','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'Cafe Lilliput', 'Best Indian', 'MG Road','','Margao','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'Viva Panjim', 'Best Indian', 'MG Road','','Anjuna','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','08:00:00','23:00:00'),
	( 'Mosaic Restaurant', 'Best Indian', 'MG Road','','Ponda','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','10:00:00','23:00:00'),
	( 'AZ.U.R - The Marriott', 'Best Indian', 'MG Road','','Miramar','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','09:00:00','23:00:00' ),
	( 'Hotel Navtara', 'Goa Best Restaurant', 'Calangute - Mapusa Rd, Oppsoite Bodgeshwar Temple','','Mapusa','Goa','India','403507','08326512660','','ka@asdf.com','http://www.navtara.in/',200,0,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','07:00:00','23:00:00' )


GO

Insert into dbo.Role(Name)
values 
	('Admin'),
	('RestaurantOwner'),
	('RestaurantEmployee')
	
Go


Insert into dbo.CustomerRole(CustomerId,RoleId,RestaurantId)
values	
	(1,2,1),
	(2,1,1)

Go


select * from customerRole

 /*Restaurant Reviews*/
INSERT INTO [dbo].[RestaurantReview]
           ([RestaurantId]
           ,[CustomerId]
           ,[Score]
           ,[Comment])
     values
		   (1,1,4,'This is really great place'),
		   (1,2,3,'My favorite restaurant!'),
		   (1,3,4,'My favorite restaurant!'),
		   (2,1,4,'This is really great place'),
		   (2,2,3,'My favorite restaurant!'),
		   (2,3,4,'My favorite restaurant!'),
		   (3,1,4,'This is really great place'),
		   (3,2,3,'My favorite restaurant!'),
		   (4,3,4,'My favorite restaurant!'),
		   (5,1,4,'This is really great place'),
		   (6,2,3,'My favorite restaurant!'),
		   (1,4,4,'I like this one..')
GO

/*Restaurant Menu*/	
INSERT INTO [dbo].[RestaurantMenu]
           ([RestaurantId]
           ,[MenuName]
           ,[MenuDescription]
           ,[IsActive]
           )
     VALUES
           (1,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (1,'Future Menu', 'This will active in future during vacations',0),
		  (2,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (3,'Default Menu', 'This is currently active Menu of the restaurant',1),
		  (3,'Old Menu', 'Menu for special occasions',0),
		  ((Select MAX(Id) from Restaurant),'Navtara Menu', 'This is currently active Menu of the restaurant',1) 
GO

Declare @res_Menu_id int
Select @res_Menu_id = MAX(Id) from RestaurantMenu
/*Restaurant Menu Group*/
INSERT INTO [dbo].[RestaurantMenuGroup]
           ([RestaurantMenuId]
           ,[GroupName]
           ,[GroupDescription]
           ,[IsActive])
     VALUES
           (1,'Forret', 'Forret',1),
		  (1,'Stegeretter', 'Stegeretter descripton',1),
		  (1,'Desserter', 'Desserter descripton',1),
		  (1,'B�rnemenu', 'currently not active ',0),
		  (3,'Breakfast', 'Good Morning with deliciious breakfast',1),
		  (3,'Starters', 'Start with deliciious breakfast',1),
		  (3,'Sea Food', 'Start with deliciious breakfast',1),
		  (3,'Indian Curries', 'Start with deliciious breakfast',1),
		  (@res_Menu_id,'Goan', '',1),
		  (@res_Menu_id,'Sout Indian', '',1),
		  (@res_Menu_id,'Sandwiches & Burgers', '',1),
		  (@res_Menu_id,'Pizza', '',1),
		  (@res_Menu_id,'Pav Bhaji(11am to Onwards)', '',1),
		  (@res_Menu_id,'Chaat(4pm to 9pm)', '',1),
		  (@res_Menu_id,'Thali(South Indian)', '',1),
		  (@res_Menu_id,'Starters', '',1),
		  (@res_Menu_id,'Soups', '',1),
		  (@res_Menu_id,'North Indian', '',1),
		  (@res_Menu_id,'Tandoori(Indian Bread)', '',1),
		  (@res_Menu_id,'Rice', '',1),
		  (@res_Menu_id,'Salad & Raita', '',1),
		  (@res_Menu_id,'Chinese Vegetables', '',1),
		  (@res_Menu_id,'Noodles', '',1),
		  (@res_Menu_id,'Fried Rice', '',1),
		  (@res_Menu_id,'Juices (Seasonal)', '',1),
		  (@res_Menu_id,'Milk Shakes(Seasonal)', '',1),
		  (@res_Menu_id,'Falooda', '',1),
		  (@res_Menu_id,'Lassi', '',1),
		  (@res_Menu_id,'Kulfi', '',1),
		  (@res_Menu_id,'Ice Creams', '',1),
		  (@res_Menu_id,'Desserts', '',1),
		  (@res_Menu_id,'Sweets', '',1),
		  (@res_Menu_id,'Cold Beverages', '',1),
		  (@res_Menu_id,'Hot Beverages', '',1),
		  (@res_Menu_id,'Extras', '',1)
GO

Declare @menuGroup_id int
Select @menuGroup_id = MAX(Id) from RestaurantMenuGroup
/*Restaurant Menu Item*/
INSERT INTO [dbo].[RestaurantMenuItem]
           ([RestaurantMenuGroupId]
           ,[ItemName]
           ,[ItemDescription]
           ,[ImagePrimary]
           ,[Price]
           ,[TaxId]
           ,[IsActive]
           )
     VALUES
           (1,'Gravad Laks', 'Pickled Salmon','Image1.jpg',95,null,1),
		  (1,'Scampi Fritti', 'med pikant peberdressing Deep-fried scampi with piquant green madagascar dressing','Image1.jpg',95,null,1),
		  (2,'Morbrad', 'Reelt stykke k�d- fuldt afpareret Extremely tender','Image1.jpg',299,null,1),
		  (2,'Ribeye', 'Grov mamoreret, kraftig smag The marbling enhances the taste of beef ','Image1.jpg',299,null,1),
		  (3,'Banan aux four', 'Bagt banan med jordb�ris, fl�deskum, chocoladesauce og n�dder','Image1.jpg',65,null,1),
		  (5,'Plain Omlet', null,'Image1.jpg',60,null,1),
		  (5,'Fried Egg', 'Healty, tasty fries','Image1.jpg',70,null,1),
		  (6,'Fish Finger', 'Start with deliciious dish','Image1.jpg',150,null,1),
		  --Goan
		  (@menuGroup_id - 26,'Bun', '','Image1.jpg',20,null,1),
		  (@menuGroup_id - 26,'Chapati', '','Image1.jpg',20,null,1),
		  (@menuGroup_id - 26,'Potato Vada', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 26,'Veg Samosa', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 26,'Sukha Bhaji Pav', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 26,'Patal Bhaji Pav', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 26,'Mix Bhaji Pav', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 26,'Sukha Bhaji Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 26,'Patal Bhaji Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 26,'Mix Bhaji Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 26,'Alsande Bhaji Pav', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 26,'Chana Bhaji Pav', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 26,'Alsande Bhaji Puri', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 26,'Chana Bhaji Puri', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 26,'Veg Xacuti Pav', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 26,'Veg Xacuti Puri', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 26,'Mashroom Xacuti Pav', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 26,'Mashroom Xacuti Puri', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 26,'Mashroom Xacuti Paratha', '','Image1.jpg',80,null,1),
		  
		  --South Indian
		  (@menuGroup_id - 25,'Upama', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 25,'Idli', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 25,'Vada', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 25,'Idli/Vada', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 25,'Dahi Vada', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 25,'Cutlet', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 25,'Sada Dosa', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 25,'Masala Dosa', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 25,'Mysore Dosa', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 25,'Mysore Masala Dosa', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Ghee Masala Dosa', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Butter Masala Dosa', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Palak Dosa', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 25,'Palak Masala Dosa', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Schezwan Dosa', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Schezwan Masala Dosa', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 25,'Rava Dosa', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 25,'Rava Masala Dosa', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 25,'Paper Dosa', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 25,'Paper Masala Dosa', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 25,'Cheese Dosa', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 25,'Cheese Masala Dosa', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 25,'Mashroom Chilly Fry Dosa', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 25,'Paneer Dosa', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 25,'Onion Uttapam', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Tomato Uttapam', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 25,'Ghee Onion Uttapam', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 25,'Butter Onion Uttapam', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 25,'Cheese Onion Uttapam', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 25,'Mashroom Chilly Uttapam', '','Image1.jpg',110,null,1),
		  
		  --Sandwiches & Burgers
		  (@menuGroup_id - 24,'Bread Butter ', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 24,'Bread Jam', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 24,'Toast Butter', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 24,'Toast Jam', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 24,'Veg. Sandwich', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 24,'Veg. Grilled Sandwich', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 24,'Cheese Sandwich', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 24,'Cheese Grilled Sandwich', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 24,'Veg. Cheese Sandwich', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 24,'Cheesy-Chilly Grilled Sandwich', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 24,'CTC Toast(Cheese)', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 24,'CTC Toast(Tomato)', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 24,'CTC Toast(Capcicum)', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 24,'Club Sandwich', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 24,'Veg. Cheese Burger', '','Image1.jpg',90,null,1),
		  
		  --Pizza
		  (@menuGroup_id - 23,'Margherita Pizza', '','Image1.jpg',140,null,1),
		  (@menuGroup_id - 23,'A la Italiano(Mushroom)', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 23,'A la Italiano(Capsicum)', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 23,'A la Italiano(Baby Corn)', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 23,'Paneer Dhamak(Paneer)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Paneer Dhamak(Capsicum)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Paneer Dhamak(Onion)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Passareli Pizza(Chineese)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Passareli Pizza(Tomato)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Passareli Pizza(Capsicum)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Passareli Pizza(Babycorn)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Primavera Pizza(Chineese)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Primavera Pizza(Tomato)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Primavera Pizza(Capsicum)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Primavera Pizza(Babycorn)', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 23,'Navtara Premium(Bell Pepper)', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 23,'Navtara Premium(Olive)', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 23,'Navtara Premium(Corn Mushroom)', '','Image1.jpg',90,null,1),

		  --Pav Bhaji
		  (@menuGroup_id - 22,'Special Pav Bhaji', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 22,'Jain Pav Bhaji', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 22,'Cheese Pav Bhaji', '','Image1.jpg',130,null,1),
		  (@menuGroup_id - 22,'Paneer Pav Bhaji', '','Image1.jpg',130,null,1),
		  (@menuGroup_id - 22,'Bhaji Only', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 22,'Butter Pav', '','Image1.jpg',10,null,1),
		  
		  --Chaat
		  (@menuGroup_id - 21,'Bhel Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Shev Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Shev Batata Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Shev Dahi Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Shev Batata Dahi Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Masala Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Pani Puri', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 21,'Ragada Patties', '','Image1.jpg',60,null,1),

	  		  --Lunch & Dinner
		  --thali
		  (@menuGroup_id - 20,'Veg Thali', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 20,'Special Thali', '','Image1.jpg',150,null,1),
		
			--Starters
		  (@menuGroup_id - 19,'Fried Papad', '','Image1.jpg',10,null,1),
		  (@menuGroup_id - 19,'Roasted Papad', '','Image1.jpg',10,null,1),
		  (@menuGroup_id - 19,'Masala Papad(Fried)', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 19,'Masala Papad(Roasted)', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 19,'French fries', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 19,'Chinese Bhel', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 19,'Paneer Sticks', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 19,'Cheese Pakode', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 19,'Paneer Pakode', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 19,'Paneer Tikka', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 19,'Paneer 65', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 19,'Spring Roll', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 19,'Mushroom Tika', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 19,'Mushroom Pepper', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 19,'Veg. Lollipop', '','Image1.jpg',200,null,1),
		  
		  --Soups
		  (@menuGroup_id - 18,'Sweet Corn Veg.', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Cream of Veg', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Cream of Palak', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Cream of Tomato', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Mushroom', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Hot "n" Sour Veg', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 18,'Veg. Manchow', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 18,'Caldo Verde(Portugues Potalo Soup)', '','Image1.jpg',90,null,1),
		  
		  --North Indian
		  (@menuGroup_id - 17,'Aloo Gobi', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Aloo Palak', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Aloo Mutter', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Aloo Methi', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Aloo Jeera', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Aloo Dum Punjabi', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Bhindi Masal', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Baigan Bhartha', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 17,'Babycorn Masala', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Babycorn Capsicum', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Chana Masal', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 17,'Chole Bhature', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Dal Fry', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 17,'Dal Palak', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 17,'Dal Makhani', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 17,'Dal Tadka', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 17,'Dal Methi', '','Image1.jpg',150,null,1),
		  (@menuGroup_id - 17,'Kaju Masala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Kadhai La Jawab', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Kadhai Hyderabadi', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Mushroom Masala', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Mushroom Keema', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Mushroom Methi Mutter', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Mushroom Tikka Masala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Mushroom Tawa Masala', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 17,'Malai Kofta', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Paneer Kofta', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Shahi', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Palak', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Mutter', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Methi', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Bhurji', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Kadhai', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Lababdar', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Makhanwala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Tika', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Tawa Masala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Butter Masala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Paneer Babycorn Masala', '','Image1.jpg',210,null,1),
		  (@menuGroup_id - 17,'Veg. Kofta', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Keema', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Kadhai', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Handi', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Jalfrezi', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Chilly-Milly', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Hyderabadi ', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Kolhapuri', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Makhanwala', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 17,'Veg. Maharaja', '','Image1.jpg',210,null,1),
		  
	  		  --Tandoori(indian Bread)
		  (@menuGroup_id - 16,'Roti', '','Image1.jpg',20,null,1),
		  (@menuGroup_id - 16,'Butter Roti', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Garlic Roti', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Missi Roti', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Paratha', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Butter Paratha', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Kerala Paratha(8am to 11am)', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 16,'Stuffed Paratha(Aloo)', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 16,'Stuffed Paratha(Gobi)', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 16,'Stuffed Paratha(Veg Methi)', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 16,'Naan', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 16,'Butter Naan', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 16,'Butter Garlic Naan', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 16,'Cheese Stuffed Naan', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 16,'Cheese Stuffed Garlic Naan', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 16,'Onion Kulcha', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 16,'Butter Onion Kulcha', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 16,'Paneer Lulcha', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 16,'Butter Kulcha', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 16,'Bhatura', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 16,'Puri(3pcs)', '','Image1.jpg',30,null,1),
		   
		   ---Rice
		  (@menuGroup_id - 15,'Plain Rice', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 15,'Basmati Rice', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 15,'Curd Rice', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 15,'Tomato Rice', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 15,'Ghee Rice', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 15,'Jeera Rice', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 15,'Dal Khichadi Rice', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 15,'Veg pulao', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 15,'Peas Pulao', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 15,'Veg. Biryani', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 15,'Hyderabadi Biryani', '','Image1.jpg',170,null,1),

		   ---Salad & Raita
		  (@menuGroup_id - 14,'Mixed Salad', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 14,'Onion Salad', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 14,'Mixed Vegetable Raita', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 14,'Raita(Aloo)', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 14,'Raita(Boondi)', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 14,'Raita(Pineapple)', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 14,'Paneer Veg Salad', '','Image1.jpg',100,null,1),
		  
			--Chinese Vegetables
		  (@menuGroup_id - 13,'Cauliflower Manchurian', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 13,'Veg Manchurian', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 13,'Mushroom Manchurian', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 13,'Babycorn Manchurian', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 13,'Paneer Schezwan', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 13,'Paneer Chilly', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 13,'Paneer Garlic', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 13,'Paneer Ginger', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 13,'Paneer 65', '','Image1.jpg',190,null,1),
		  (@menuGroup_id - 13,'Idli Chilly Fry(Dry)', '','Image1.jpg',130,null,1),
		  (@menuGroup_id - 13,'Mushroom Ginger Garlic', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 13,'Mushroom With Babycorn', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 13,'Veg. 65', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 13,'Gobi 65', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 13,'Veg Crispy(Dry)', '','Image1.jpg',200,null,1),
		  
		  ---Noodles
		  (@menuGroup_id - 12,'Hakka Noddles', '','Image1.jpg',140,null,1),
		  (@menuGroup_id - 12,'Mushroom Hakka Noodles', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 12,'Hong Kong Noodles', '','Image1.jpg',140,null,1),
		  (@menuGroup_id - 12,'Singapore Noodles', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 12,'Schezwan Noodles', '','Image1.jpg',150,null,1),
		  
		  --Fried Rice
		  (@menuGroup_id - 11,'Veg. Fried Rice', '','Image1.jpg',160,null,1),
		  (@menuGroup_id - 11,'Burnt Garlic Fried Rice', '','Image1.jpg',170,null,1),
		  (@menuGroup_id - 11,'Mushroom Fried Rice', '','Image1.jpg',180,null,1),
		  (@menuGroup_id - 11,'Paneer Fried Rice', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 11,'Schezwan Fried Rice', '','Image1.jpg',170,null,1),
		  (@menuGroup_id - 11,'Triple Schezwan(Fried Rice)', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 11,'Triple Schezwan(Noodles)', '','Image1.jpg',200,null,1),
		  (@menuGroup_id - 11,'Triple Schezwan(Gravy)', '','Image1.jpg',200,null,1),
		  
			--Juices
		  (@menuGroup_id - 10,'Fresh Lime', '','Image1.jpg',40,null,1),
		  (@menuGroup_id - 10,'Sweet Lime', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 10,'Orange', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 10,'Water Melon', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 10,'Pineapple', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 10,'Apple', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 10,'Mango', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 10,'Grape', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 10,'Strawbwrry', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 10,'Mixed Fruit', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 10,'Carrot', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 10,'Pomegranate', '','Image1.jpg',120,null,1),
		  
			  --Milk Shakes
		  (@menuGroup_id - 9,'Banana', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Papaya', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Guava', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 9,'Chickoo', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Mash Melon', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Apple', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Custard Apple(Sitaphal)', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Mango', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 9,'Mango Mastani', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 9,'Strawberry', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Vanilla', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Rose', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Kesar', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Butterscotch', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 9,'Chocolate', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Cold Coffee(With Vanilla ice-cream Rs.30.00 Extra)', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 9,'Bournvita(With Vanilla ice-cream Rs.30.00 Extra)', '','Image1.jpg',90,null,1),
		  
			---Falooda
		  (@menuGroup_id - 8,'Kulfi', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 8,'Royal', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 8,'Special', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 8,'Maharaja', '','Image1.jpg',130,null,1),
		  
	  			  --Lassi
		  (@menuGroup_id - 7,'Butter Milk(Masala Chaas)', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 7,'Sweet', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 7,'Salted', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 7,'Guava', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 7,'Chickoo', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 7,'Grape', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 7,'Papaya', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 7,'Banana With Honey', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 7,'Mango', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 7,'Strawberry', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 7,'Sitaphal(Seasonal)', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 7,'Choclate', '','Image1.jpg',80,null,1),
		  
			--Kulfi	
		  (@menuGroup_id - 6,'Malai', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 6,'Pista', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 6,'Mango', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 6,'Kesar Pista', '','Image1.jpg',90,null,1),
		  
			 --Ice Creame
		  (@menuGroup_id - 5,'Vanilla', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 5,'Strawberry', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 5,'Pista', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 5,'Mango', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 5,'Chocolate', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 5,'Butterscotch', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 5,'Gadbad', '','Image1.jpg',110,null,1),
		  (@menuGroup_id - 5,'Chocolate Nut Sandae', '','Image1.jpg',100,null,1),
		  (@menuGroup_id - 5,'Vanilla with Hot Chocolate Sauce', '','Image1.jpg',100,null,1),
		  
		  --Dessarts
		  (@menuGroup_id - 4,'Caramel Custard', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 4,'Fruit Salad', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 4,'Fruit Salad with Custard', '','Image1.jpg',70,null,1),
		  (@menuGroup_id - 4,'Fruit Salad with Ice Creame', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 4,'Jelly', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 4,'Jelly with Ice Creame', '','Image1.jpg',80,null,1),
		  (@menuGroup_id - 4,'Fruit Salad & Jelly with Ice Creame', '','Image1.jpg',90,null,1),
		  (@menuGroup_id - 4,'Mango & Creame(Seasonal)', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 4,'Strawberry & Creame(Seasonal)', '','Image1.jpg',120,null,1),
		  (@menuGroup_id - 4,'Chocolate Brownie With Ice Creame', '','Image1.jpg',80,null,1),
		  
	  		 --Sweets
		  (@menuGroup_id - 3,'Shira', '','Image1.jpg',50,null,1),
		  (@menuGroup_id - 3,'Srikhand(with 3 puri Rs.30.00 Extra)', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 3,'Gulab Jamun(With Vanilla Ice Creame Rs.30.00 extra)', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 3,'Rasgulla', '','Image1.jpg',60,null,1),
		  (@menuGroup_id - 3,'Rasmalai', '','Image1.jpg',90,null,1),
		  
		  --cold Beverages
		  (@menuGroup_id - 2,'Soda', '','Image1.jpg',10,null,1),
		  (@menuGroup_id - 2,'Bottled Water', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 2,'Cola', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 2,'Lime', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 2,'Orange', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 2,'Mango', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 2,'Kokum Cooler', '','Image1.jpg',50,null,1),
			 
			 --Hot Beverages
		  (@menuGroup_id - 1,'Tea', '','Image1.jpg',20,null,1),
		  (@menuGroup_id - 1,'Green Tea', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Special Tea', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Masala Tea', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Filter Coffee', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Nescafe', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Bournvita', '','Image1.jpg',30,null,1),
		  (@menuGroup_id - 1,'Chocolate', '','Image1.jpg',30,null,1),
		  
	  			 --Extras
		  (@menuGroup_id,'Jam(2 pieces)', '','Image1.jpg',20,null,1),
		  (@menuGroup_id,'Butter(2 pieces)', '','Image1.jpg',20,null,1),
		  (@menuGroup_id,'Cheese', '','Image1.jpg',30,null,1)
GO

/*Menu Additional Group*/
INSERT INTO [dbo].[MenuAdditionalGroup]
           ([GroupName]
           ,[GroupDescription]
           ,[MinimumSelected]
           ,[MaximumSelected]
           ,[IsActive]
           )
     VALUES
           ('SELECT OIL', 'Tell us how you want fished to be fried',1,1,1),
	  ('Cooking', 'Degree of Roasting',1,1,1)
GO

/*Menu Additional Group*/
INSERT INTO [dbo].[MenuAdditionalElement]
           ([MenuAdditionalGroupId]
           ,[AdditionalElementName]
           ,[AdditionalElementDescription]
           ,[AdditionalCost]
           ,[IsActive]
           )
     VALUES
           (1,'Palm Oil', 'Fried with Palm Oil',10,1),
		  (1,'Soya Oil', 'Fried with Soya Oil',14,1),
		  (1,'Olive Oil', 'Fried with Olive Oil',21,1),
		  (2,'50�C - Rare', 'Description here',10,1),
		  (2,'55�C - Medium', 'Description here',14,1),
		  (2,'60�C - Medium Well', 'Description here',21,1)
GO	

/*Menu Item Additional*/
INSERT INTO [dbo].[MenuItemAdditional]
           ([RestaurantMenuItemId]
           ,[MenuAdditionalGroupId]
           ,[IsActive]
           )
     VALUES
           (1,1, 1),
 		  (1,2, 1),
		  (3,1,1)
GO

/*Menu Item Size*/
INSERT INTO [dbo].[MenuItemSize]
           ([RestaurantMenuItemId]
           ,[SizeName]
           ,[SizePrice]
           ,[SizeCalories]
           ,[IsDefault]
           ,[IsActive])
     VALUES
           (1,'Half', 100,50,1,1),
	  (1,'Full', 195,115,0,1)
GO

/*Ingredient*/
INSERT INTO [dbo].[Ingredient]
           ([ParentId]
           ,[Name]
           ,[Description])
     VALUES
           (null,'Salmon', 'raw salmon'),
		  (null,'Egg', 'Whole Egg'),
		  (2,'Egg Yolk', 'Yolk of egg')
GO

/*Menu Item Ingredient*/
INSERT INTO [dbo].[MenuItemIngredient]
           ([RestaurantMenuItemId]
           ,[IngredientId]
           ,[Qty])
     VALUES
			(1,1,'100gms'),
			(2,2,'2Nos')
GO

/*Customer Ingredient Preference*/
INSERT INTO [dbo].[CustomerIngredientPreference]
           ([CustomerId]
           ,[IngredientId]
           ,[Include])
     VALUES
			(5,2,0),
			(5,1,1)
GO

/*Food Life Style*/
INSERT INTO [dbo].[FoodLifeStyle]
           ([Name]
           ,[Description])
     VALUES
			('Dairy Free','No to all dairy Items'),
			('Egg Free', 'No to all egg items'),
			('Fish Free', 'No to all egg items'),
			('Onion Free', 'No to all onion items')
GO

/*Life Style Ingredient*/
INSERT INTO [dbo].[LifeStyleIngredient]
           ([LifeStyleId]
           ,[IngredientId]
           ,[Include])
     VALUES
           (2,2,0),
           (3,2,0),
           (1,2,0)
      
GO
/*Customer Life Style Preference*/
INSERT INTO [dbo].[CustomerLifeStylePreference]
           ([LifeStyleId]
           ,[CustomerId])
     VALUES
           (2,5),
           (2,2),
           (2,1)
GO

/*Allergen*/
INSERT INTO [dbo].[Allergen]
           ([Name]
           ,[Description])
     VALUES
			('Egg','Egg Products'),
			('Fish','Fish Products'),
			('Gluten','Gluten Products'),
			('Milk','Milk Products'),
			('Peanuts','Peanut Products'),
			('Soya','Soya Products'),
			('Sulphites','Sulphite Products'),
			('Nuts','Nuts')
GO

/*Allergen Ingredient*/
INSERT INTO [dbo].[AllergenIngredient]
           ([AllergenID]
           ,[IngredientID])
     VALUES
			(1,2),
			(1,3),
			(2,1)
GO

/*Customer Allergen Preference*/
INSERT INTO [dbo].[CustomerAllergenPreference]
           ([AllergenId]
           ,[CustomerId])
     VALUES
           (1,5),
           (1,2),
           (2,4),
           (1,2),
           (3,3)
GO

/*Menu Item Review*/
INSERT INTO [dbo].[MenuItemReview]
           ([RestaurantMenuItemId]
           ,[Score]
           ,[CustomerId]
           ,[Comment]
           ,[CreationDate])
     VALUES
			(1,4,1,'It was delicious','2017-11-29 08:04:24.277'),
			(1,3,1,'It was OK','2017-11-29 08:04:24.277'),
			(2,5,2,'It was delicious, I liked it very much','2017-11-29 08:04:24.277'),
			(3,3,2,'It was delicious, I liked it very much','2017-11-29 08:04:24.277'),
			(4,1,1,'It was OK','2017-11-29 08:04:24.277'),
			(4,4,2,'It was delicious, I liked it very much','2017-11-29 08:04:24.277')
GO

/*Cuisine*/
INSERT INTO [dbo].[Cuisine]
           ([Name]
           ,[IsActive])
     VALUES
			('Austrian',1),
			('German',1),
			('Indian',1),
			('Italian',0),
			('Czech Wine',0)
GO

/*Restaurant Cuisine*/
INSERT INTO [dbo].[RestaurantCuisine]
           ([RestaurantId]
           ,[CuisineId]
           ,[IsActive])
     VALUES
			(1,1,1),
			(1,2,1),
			(2,1,1),
			(2,1,3),
			(3,2,1),
			(4,1,1),
			(5,2,1),
			(6,3,1)
GO

/*Restaurant Table*/
INSERT INTO [dbo].[RestaurantTable]
           ([TableNumber]
           ,[RestaurantId]
           ,[TotalSeats]
           ,[Description]
           ,[IsActive])
     VALUES
			(101,1,6,'description if any here',1),
			(102,1,6,'description if any here',1),
			(201,2,6,'description if any here',1),
			(202,2,6,'description if any here',1)
GO
/*Restaurant Cart*/
INSERT INTO [dbo].[RestaurantCart]
           ([CustomerId])
     VALUES
			(1),
			(2),
			(5)
GO

/*Restaurant Cart Item*/
INSERT INTO [dbo].[RestaurantCartItem]
           ([RestaurantCartId]
           ,[RestaurantMenuItemId]
           ,[Qty])
     VALUES
			(1,1,1),
			(1,2,4),
			(3,1,1),
			(3,2,4)
GO

/*Restaurant Cart Item Additional*/
INSERT INTO [dbo].[RestaurantCartItemAdditional]
           ([CartItemId]
           ,[MenuAdditionalElementId])
     VALUES
			(1,2),
			(3,1)
GO

/*Restaurant Order*/
INSERT INTO [dbo].[RestaurantOrder]
           ([CustomerId]
           ,[RestaurantId]
           ,[CustomerTableNumber]
           ,[OrderStatus])
     VALUES
			(5,1,1,1),
			(5,1,1,2),
			(5,2,1,1),
			(5,2,1,2),
			(2,1,1,1),
			(2,1,1,2),
			(3,3,1,1)
GO

/*Restaurant Order Item*/
INSERT INTO [dbo].[RestaurantOrderItem]
           ([RestaurantOrderId]
           ,[RestaurantMenuItemId]
           ,[Qty])
     VALUES
			(1,1,1),
			(1,2,4),
			(3,1,1),
			(3,2,4) 
GO

/*Restaurant Order Item Additional*/
INSERT INTO [RestaurantOrderItemAdditional]
           ([RestaurantOrderItemId]
           ,[MenuAdditionalElementId])
     VALUES
			(2,2),
			(3,1)
GO

/*Restaurant Order Review*/
INSERT INTO [dbo].[RestaurantOrderReview]
           ([RestaurantOrderId]
           ,[Score]
           ,[Comment]
           )
     VALUES
			(1,4, 'It was good' ),
			(2,3, 'Food was ok' ),
			(3,5, 'Superb' ),
			(4,1, 'Food was not good. I will never visit this restaurant again.' ),
			(5,5, 'Very good food' )
GO

/*Customer Credit Card*/
INSERT INTO [dbo].[CustomerCreditCard]
           ([CustomerId]
           ,[CardHolderName]
           ,[CardNumber]
           ,[CardExpiryDate]
           ,[CVVNumber]
           ,[IsPrimary]
           ,[CardLabel]
           ,[CardLabelColor]
           ,[SpendingLimitEnabled]
           ,[SpendingLimitAmount]
           ,[Currency]
           ,[Duration])
     VALUES
			(1,'Panki Leo','2223000010476528','2022/10/12','1234',1,'Partner Card','#12323',1,1200,'EURO',1),
			(5,'Rajesh Naik','2343000010476528','2022/02/16','1234',1,'Mycard','#12323',1,1200,'EURO',1),
			(5,'Rajesh Naik','1143000010476528','2018/03/16','1224',0,'Wify card','#22323',null,null,null,null)

	   
GO

/*Paypal Details*/
INSERT INTO [dbo].[PaypalDetails]
           ([CustomerId]
           ,[PaypalEmail]
           ,[Password])
     VALUES
           (1,'test@gmail.com','pass123'),
           (2,'test2@gmail.com','pass123'),
           (4,'test3@gmail.com','pass123'),
           (5,'test4@gmail.com','pass123')
GO

/*Customer Favorite Restaurant*/
INSERT INTO [dbo].[CustomerFavoriteRestaurant]
           ([CustomerId]
           ,[RestaurantId])
     VALUES
           (1,1),
           (1,2),
           (2,3)
GO




	   






   




