--adding orders for last restaurant
declare @id int
declare @restaurantId int
declare @customerId int
declare @tableId int
declare @cardId int
set @id=1
select @restaurantId=MAX(Id)from Restaurant
print @restaurantId
select @customerId=MAX(Id)from Customer 
print @customerId
INSERT INTO [2ordr].[dbo].[RestaurantTable]([TableNumber],[RestaurantId],[TotalSeats],[Description],[IsActive])VALUES(101,@restaurantId,5,'',1)
set @tableId=(select top 1 Id from dbo.RestaurantTable where RestaurantId=@restaurantId)
print @tableId
select @cardId=MAX(Id)from CustomerCreditCard
print @cardId
while @id <=20
begin
 
INSERT INTO [2ordr].[dbo].[RestaurantOrder]
           ([CustomerId]
           ,[RestaurantId]
           ,[CustomerTableNumber]
           ,[OrderStatus]
           ,[CardId])
     VALUES
           (@customerId
           ,@restaurantId
           ,@tableId
           ,1
           ,@cardId)
	   select @id = @id + 1
end
GO


--Edit order id's

declare @id int
set @id=1000 --add newly added first order id here
while @id <=1020 --add newly added last order id here
begin

INSERT INTO [2ordr].[dbo].[RestaurantOrderItem]
           ([RestaurantOrderId]
           ,[RestaurantMenuItemId]
           ,[Qty])
     VALUES
           (@id
           ,15
           ,4)
	   select @id = @id + 1
end
GO