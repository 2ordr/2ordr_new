USE [2ordr]
GO


CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT PK_Role PRIMARY KEY CLUSTERED (id)
)
GO


CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthDate] [date] NULL,
	[Street] [nvarchar](100) NULL,
	[Street2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[Region] [nvarchar](100) NULL,
	[Country] [nvarchar](50) NULL,
	[Pincode] [nvarchar](100) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[ActiveStatus] [bit] NULL,
	[ResetPasswordCode] [nvarchar](max) NULL,
	[ActivationCode] [nvarchar](max) NULL,
	[ProviderKey] nvarchar(100) null,
	[ProviderName] nvarchar(50) null,
	[LastUpdated] [datetime] NULL,
	[CreationDate] [datetime] NULL,
CONSTRAINT PK_CU_Det PRIMARY KEY CLUSTERED (Id)
)
GO





CREATE TABLE [dbo].[Restaurant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](3000) NULL,
	[Street] [nvarchar](100) NULL,
	[Street2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[Region] [nvarchar](100) NULL,
	[Country] [nvarchar](50) NULL,
	[Pincode] [nvarchar](100) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Fax] [nvarchar](15) NULL,
	[Email] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[CostForTwo] [int] NULL,  -- Some value, and based on this, string will be genrated $$$$ etc
	[IsPartnered] [bit] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_REST_DET_CRDate default GetUTCDate(),
	[Location] [geography] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[BackgroundImage] [nvarchar](100) NULL,
	LogoImage nvarchar(100),
	[OpeningHours] [time] Null,
	[ClosingHours] [time] Null,
constraint PK_Rest_DET primary key (Id)
)
GO

CREATE TABLE [dbo].[RestaurantReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantId] [int] NOT NULL,
	[CustomerId] Int Not Null,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_Rest_REV_CR DEFAULT GETUTCDate(),
Constraint PK_Rest_Rev Primary Key (Id),
Constraint FK_Rest_REV_RestID Foreign Key (RestaurantId)
	References Restaurant(Id),
Constraint FK_Rest_Rev_CustID Foreign Key(CustomerId)
	REFERENCES Customer(Id)
)

GO



CREATE TABLE [dbo].[RestaurantMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantId] int not null,
	[MenuName] [nvarchar](100) NOT NULL,
	[MenuDescription] [nvarchar](255),
	[IsActive] [bit],
	[CreationDate] [datetime] NOT NULL constraint DF_RM_Create DEFAULT GETUTCDate() ,
 CONSTRAINT PK_Restaurant_Menu PRIMARY KEY CLUSTERED (Id),
 constraint FK_Restaurant_Menus_Restaurant_ID Foreign key (RestaurantId)
	REFERENCES dbo.Restaurant(Id)
 )
GO

CREATE TABLE [dbo].[RestaurantMenuGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuId] int NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[GroupDescription] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[CreationDate] [datetime]  NOT NULL  constraint DF_Menu_groups_creation DEFAULT GETUTCDate() ,
 CONSTRAINT [PK_Restaurant_Menu_Groups] PRIMARY KEY CLUSTERED (	Id ASC),
 constraint [FK_Menu_Groups_Menu_ID] FOREIGN Key (RestaurantMenuId)
	REFERENCES dbo.RestaurantMenu(Id)
  )
GO


CREATE TABLE [dbo].[RestaurantMenuItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuGroupId] [int] NOT NULL,
	[ItemName] [nvarchar](100) NOT NULL,
	[ItemDescription] [nvarchar](255) NULL,
	[ImagePrimary] [nvarchar](100) NULL,
	[Price] [int] NULL,
	[TaxId] [int] NULL,
	[IsActive] [bit] NULL constraint DF_Menu_Items DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint DF_Menu_Items_creation DEFAULT GETUTCDate(),
	
	constraint PK_Restaurant_Menu_Items Primary Key CLUSTERED (Id),
	constraint FK_Restaurant_Menu_Items_GroupId FOREIGN KEY (RestaurantMenuGroupId)
		REFERENCES dbo.RestaurantMenuGroup(Id)
	)
GO



CREATE TABLE [dbo].[MenuAdditionalGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[GroupDescription] [nvarchar](255) NOT NULL,
	[MinimumSelected] [int] NULL,
	[MaximumSelected] [int] NULL,
	[IsActive] [bit] NULL constraint DF_Additional_Group DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint DF_Menu_AdditionalGrp_creation DEFAULT GETUTCDate(),
	
	constraint PK_Options_Groups Primary Key CLUSTERED (Id)
	)
GO


CREATE TABLE [dbo].[MenuAdditionalElement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuAdditionalGroupId] [int] NOT NULL,
	[AdditionalElementName] [nvarchar](100) NOT NULL,
	[AdditionalElementDescription] [nvarchar](255) NOT NULL,
	[AdditionalCost] [int] NULL,
	[IsActive] [bit] NULL constraint DF_Option_Group_Item DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint DF_Menu_OptionsItems_creation DEFAULT GETUTCDate(),
	
	constraint PK_Menu_Additional_Element_ID Primary Key CLUSTERED (Id),
	constraint FK_Menu_Additional_Element_Group_ID FOREIGN KEY (MenuAdditionalGroupId)
		REFERENCES dbo.MenuAdditionalGroup(Id)
	)
GO


CREATE TABLE [dbo].[MenuItemAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[MenuAdditionalGroupId] [int] NOT NULL,
	[IsActive] [bit] NULL constraint DF_Menu_Additional_Option DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint DF_Menu_Items_OptionCr DEFAULT GETUTCDate(),
	
	constraint PK_Item_Option Primary Key CLUSTERED (Id),
	constraint FK_Item_Option_Menu_Item FOREIGN KEY (RestaurantMenuItemId)
		REFERENCES dbo.RestaurantMenuItem(Id),
	constraint FK_Item_Option_Options_group FOREIGN KEY (MenuAdditionalGroupId)
		REFERENCES dbo.MenuAdditionalGroup(Id)

	)
GO

CREATE TABLE [dbo].[MenuItemSize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[SizeName] [nvarchar](100) NOT NULL,
	[SizePrice] [decimal] NULL ,
	[SizeCalories] [int] NULL,
	[IsDefault] Bit Null,
	[IsActive] [bit] NULL constraint DF_ITMSIZEVLD DEFAULT 1,	
	constraint PK_Item_Size Primary Key CLUSTERED (Id),
	constraint FK_Item_Size_Menu_Item FOREIGN KEY (RestaurantMenuItemId)
		REFERENCES dbo.RestaurantMenuItem(Id)
	)
GO

Create Table dbo.Ingredient(
	Id Int Identity(1,1) Not Null,
	ParentId int,
	Name nvarchar(50) Not Null,
	Description nvarchar(100)
Constraint PK_Ingredient PRIMARY KEY CLUSTERED (Id),
Constraint FK_Ingredient_Self_Parent Foreign Key (ParentId)
	References dbo.Ingredient(Id)
)
GO


CREATE TABLE [dbo].[MenuItemIngredient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[IngredientId] [int] NOT NULL,
	[Qty] [nvarchar](25) NULL ,
	constraint PK_Item_Ingredient Primary Key CLUSTERED (Id),
	constraint FK_Item_Ingredient_Menu_Item FOREIGN KEY (RestaurantMenuItemId)
		REFERENCES dbo.RestaurantMenuItem(Id),
	Constraint FK_Item_Ingredient_IngredientID FOREIGN KEY(IngredientId)
		References Ingredient(id)
	)
GO

Create Table dbo.CustomerIngredientPreference(
	Id int not null Identity(1,1),
	CustomerId int not null,
	IngredientId int not null,
	Include Bit null,
	Constraint PK_Ingredient_Preference PRIMARY Key (Id),
	Constraint FK_Ingredient_Pref_CustID FOREIGN Key (CustomerId)
		REFERENCES Customer(Id),
	Constraint FK_Ing_Pref_IngredientId Foreign Key (IngredientId)
		REFERENCES Ingredient(Id)

)
GO

Create Table dbo.FoodLifeStyle(
	Id int not null Identity(1,1),
	Name nvarchar(50) not null,
	Description nvarchar(100) null,
	Constraint PK_LifeStyle_Id PRIMARY Key (Id)
)
GO

Create Table dbo.LifeStyleIngredient(
	Id int not null Identity(1,1),
	LifeStyleId int not null,
	IngredientId int not null,
	Include bit null,
	Constraint PK_Lifestyle_Ingredient PRIMARY Key (Id),
	Constraint FK_LF_ING_LifeStyle_Id FOREIGN Key(LifeStyleId)
		REFERENCES FoodLifeStyle(Id),
	Constraint FK_LF_ING_IngId FOREIGN Key (IngredientId)
		REFERENCES Ingredient(Id)
)
GO

Create Table CustomerLifeStylePreference(
	Id int not null IDENTITY(1,1),
	LifeStyleId int not null,
	CustomerId int not null,
	Constraint PK_Lifestyle_pref PRIMARY KEY(Id),
	Constraint FK_LifeStyle_PREF_LIfestyeID FOREIGN key(LifeStyleId)
		REFERENCES FoodLifeStyle(Id),
	Constraint FK_LF_PREF_CUSTID FOREIGN KEY(CustomerId)
		REFERENCES Customer(Id)
)


Create Table Allergen(
	Id Int not null IDENTITY(1,1),
	Name nvarchar(50) not null,
	Description nvarchar(100),
	Constraint PK_Allergen PRIMARY Key (Id)
)
GO


Create Table AllergenIngredient(
	Id int not null Identity(1,1),
	AllergenID int not null,
	IngredientID int not null,
	Constraint PK_Allergen_Ing PRIMARY Key(Id),
	Constraint FK_Allergn_ING_ALLERGNID Foreign Key(AllergenId)
		REFERENCES Allergen(Id),
	Constraint FK_ALLRN_ING_INGID Foreign Key(IngredientId)
		REFERENCES Ingredient(Id)
)
GO

Create table CustomerAllergenPreference(
	Id int not null Identity(1,1),
	AllergenId int not null,
	CustomerId int not null,
	constraint Pk_Allergen_PREf PRIMARY Key (Id),
	Constraint FK_ALLERGEN_PREF FOREIGN Key (AllergenId)
		REFERENCES Allergen(Id),
	Constraint FK_Allergen_Pref_CustId FOREIGN Key(CustomerId)
		REFERENCES Customer(Id)
)
Go


CREATE TABLE [dbo].[MenuItemReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[CustomerId] int not null,
	[Comment] [nvarchar](500) NULL ,
	[CreationDate] [datetime] NOT NULL,
	constraint PK_Item_Review Primary Key CLUSTERED (Id),
	constraint FK_Item_Reviews_Menu_Item FOREIGN KEY (RestaurantMenuItemId)
		REFERENCES dbo.RestaurantMenuItem(Id),
	constraint FK_MENUITEMREV_CUSTID FOREIGN KEY (CustomerId)
		REFERENCES dbo.Customer(Id)
	)
GO


CREATE TABLE [dbo].[Cuisine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL Constraint DF_Cuisines_DT default getUtcDate(),
Constraint PK_Cusines PRIMARY Key (Id)
)
GO


CREATE TABLE [dbo].[RestaurantCuisine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantId] [int] NOT NULL,
	[CuisineId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL Constraint DF_Restaurant_CUIS_Dt default getUtcDate(),
Constraint PK_Rest_Cuisine PRIMARY Key(Id),
Constraint FK_Rest_CUIS_Rest_ID FOREIGN Key (RestaurantId)
	REFERENCES Restaurant(Id),
Constraint FK_Rest_cuis_Cuisne_Id Foreign key(CuisineId)
	REFERENCES Cuisine(Id)
)
GO

CREATE TABLE [dbo].[RestaurantTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TableNumber] [int] NOT NULL,
	[RestaurantId] [int] NOT NULL,
	[TotalSeats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL Constraint DF_REST_TABLE Default getUtcDate(),
Constraint PK_REST_Table Primary Key (Id),
Constraint FK_REST_TAB_REST_ID FOREIGN KEY(RestaurantId)
	REFERENCES Restaurant(Id)
)
GO


Create Table dbo.RestaurantCart (
	Id int not null IDENTITY (1,1),
	CustomerId int Not null,
	CreationDate DateTime Not Null Constraint DF_REST_CART DEFAULT getUtcDate(),
	Constraint PK_REST_CART Primary Key (Id),
	Constraint FK_REST_CART_CUST_ID FOREIGN KEY(CustomerId)
		References Customer(Id)
)

Create Table dbo.RestaurantCartItem (
	Id int not null Identity(1,1),
	RestaurantCartId int not null,
	RestaurantMenuItemId int not null,
	Qty int not null, 
	Constraint PK_REST_CART_ITEM PRIMARY Key (Id),
	Constraint FK_REST_CART_ITM_CARTID FOREIGN KEY(RestaurantCartId)
		REFERENCES RestaurantCart(Id) ON DELETE CASCADE,
	constraint FK_Restaurant_cart_Itm_Menu_itm_Id foreign Key (RestaurantMenuItemId)
		REFERENCES RestaurantMenuItem(Id) ON DELETE CASCADE
)


Create table dbo.RestaurantCartItemAdditional(
	Id int not null Identity (1,1),
	CartItemId int not null,
	MenuAdditionalElementId int not null,
	constraint PK_REST_CART_ITEM_ADDNAL primary key(Id),
	constraint FK_REST_CART_ADD_ITMID foreign Key (CartItemID)
		REFERENCES RestaurantCartItem(Id) ON DELETE CASCADE,
	Constraint FK_REST_CART_ADD_ELEMENTID FOREIGN Key(MenuAdditionalElementId)
		REFERENCES MenuAdditionalElement(Id)
)

GO


-- Restaurant Orders

CREATE TABLE [dbo].[RestaurantOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RestaurantId] [int] NOT NULL,
	[CustomerTableNumber] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL constraint DF_RO_Create DEFAULT GETUTCDate(),
	[OrderStatus] [int] NULL,
	[ScheduledDate] [Datetime] NULL,
CONSTRAINT PK_RO PRIMARY KEY CLUSTERED (Id),
constraint FK_RO_CU Foreign key (CustomerId)
	REFERENCES dbo.Customer(Id),
Constraint FK_RO_RestID Foreign Key(RestaurantId)
	References Restaurant(Id)
)
GO

Create Table dbo.RestaurantOrderItem(
	Id int not null Identity (1,1),
	RestaurantOrderId int not null,
	RestaurantMenuItemId int not null,
	Qty int not null,
Constraint PK_REST_ORDER_ITEM primary Key (Id),
Constraint FK_REST_ORD_ITEM_ORDERID Foreign Key (RestaurantOrderId)
	References RestaurantOrder(Id),
Constraint FK_REST_ORD_ITEM_MENUID FOREIGN Key (RestaurantMenuItemId)
	References RestaurantMenuItem(Id)
)
GO


Create Table dbo.RestaurantOrderItemAdditional(
	Id int not null Identity (1,1),
	RestaurantOrderItemId Int not null,
	MenuAdditionalElementId int not null,
Constraint PK_REST_ORD_ITEM_ADD PRIMARY Key (Id),
Constraint FK_REST_ORD_ITEM_ADD_ORDERITEMID FOREIGN Key (RestaurantOrderItemId)
	REFERENCES RestaurantOrderItem(Id),
Constraint FK_Rest_ORD_ITEM_ADD_MENUID FOREIGN Key(MenuAdditionalElementId)
	References MenuAdditionalElement(Id)
)

GO





CREATE TABLE [dbo].[RestaurantOrderReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantOrderId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_RO_REV DEFAULT GETUTCDate(),
Constraint PK_RO_Rev Primary Key (Id),
Constraint FK_RO_REV_OrderID Foreign Key (RestaurantOrderId)
	References RestaurantOrder(Id)
)

GO



CREATE TABLE [dbo].[CustomerCreditCard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CardHolderName] [nvarchar](100) NOT NULL,
	[CardNumber] [nvarchar](20) NOT NULL,
	[CardExpiryDate] [datetime] NOT NULL,
	[CVVNumber] [nvarchar](4) NOT NULL,
	[IsPrimary] [bit],
	[CardLabel] [nvarchar](50) NULL,
	[CardLabelColor] [nvarchar](20) NULL,
	[SpendingLimitEnabled] [bit] NULL,
	[SpendingLimitAmount] [int] NULL,
	[Currency] [nvarchar](20) NULL,
	[Duration] [int] NULL,
	[CreationDate] [datetime] NOT NULL constraint DF_CUST_CRED_CARD DEFAULT GETUTCDate(),
constraint PK_CUST_CREDI_CARD primary KEY ( Id),
CONSTRAINT FK_CUST_CREDI_CARD_CUSTID foreign key(CustomerId)
	REFERENCES Customer(Id)
)
GO


CREATE TABLE [dbo].[PaypalDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] Int Not Null,
	[PaypalEmail] nvarchar(100) NOT NULL,
	[Password] nvarchar(25),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_Paypal_CR DEFAULT GETUTCDate(),
Constraint PK_Paypal Primary Key (Id),
Constraint FK_Paypal_Cust_ID Foreign Key (CustomerId)
	REFERENCES Customer(Id)
)
GO





CREATE TABLE [dbo].[CustomerFavoriteRestaurant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] Int Not Null,
	[RestaurantId] int not null,
Constraint PK_Cust_fav_Rest Primary Key (Id),
Constraint FK_Cust_Fav_Rest_Cust_ID Foreign Key (CustomerId)
	REFERENCES Customer(Id),
Constraint FK_Cust_Fav_Rest_Rest_ID Foreign Key (RestaurantId)
	REFERENCES Restaurant(Id)
)
GO



/****** Object:  Table [dbo].[Customer_Hotel_Bookings]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Bookings](
	[Customer_Hotel_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Customer_Reviews]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customer_Reviews](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Review_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Details](
	[Hotel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Name] [nvarchar](500) NOT NULL,
	[Hotel_Description] [nvarchar](3000) NOT NULL,
	[Address_Street] [nvarchar](100) NOT NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NOT NULL,
	[Address_Region] [nvarchar](100) NOT NULL,
	[Address_Country] [nvarchar](50) NOT NULL,
	[Address_Code] [nvarchar](100) NOT NULL,
	[Contact_Telephone] [nvarchar](15) NOT NULL,
	[Contact_Fax] [nvarchar](15) NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Contact_Website] [nvarchar](100) NULL,
	[Star_Rating] [int] NOT NULL,
	[Status_Partnered] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
	[Location] [geography] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
 CONSTRAINT [PK_Hotel_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[CustomerRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[HotelId] [int] NULL,
	[RestaurantId] [int] NULL,
	CONSTRAINT PK_Customer_Role PRIMARY KEY CLUSTERED (Id),
	
	constraint FK_Customer_Customer_Id Foreign key (CustomerId)
	REFERENCES dbo.Customer(Id),
	
	constraint FK_Role_Role_Id Foreign key (RoleId)
	REFERENCES dbo.Role(Id),
	
	constraint FK_Hotel_Details_Hotel_ID Foreign key (HotelId)
	REFERENCES dbo.Hotel_Details(Hotel_Id),
	
	constraint FK_Restaurant_Restaurant_ID Foreign key (RestaurantId)
	REFERENCES dbo.Restaurant(Id)
	)
Go




/****** Object:  Table [dbo].[Hotel_Types]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Types](
	[Hotel_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Type] [nvarchar](100) NOT NULL,
	[Valid_Type] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotels]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotels](
	[Hotels_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Hotel_Type_ID] [int] NOT NULL,
	[Hotel_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Hotels_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotels_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
/****** Object:  Table [dbo].[Hotel_Rooms]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Rooms](
	[Room_ID] [int] IDENTITY(1,1) NOT NULL,
	[Room_Description] [nvarchar](500) NULL,
	[Hotels_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Room_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotels_Room_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
	/*Hotels Room Views*/
Create view [dbo].[Hotels_Room_VW] AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID

GO
/****** Object:  Table [dbo].[Hotel_Customisation_Types]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customisation_Types](
	[Hotel_Customisation_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Customisation_Type_Description] [nvarchar](500) NOT NULL,
	[Hotel_Customisation_Type_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Customisation_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Customisation_Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customisations](
	[Hotel_Customisation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Hotel_Customisation_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Hotel_Customisation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotel_Bookings_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*Hotel Bookings View*/
Create view [dbo].[Hotel_Bookings_VW] AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description,
	CHB.Customer_Hotel_Booking_ID,
	HCT.Hotel_Customisation_Type_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID
inner join dbo.Customer_Hotel_Bookings CHB on H.Hotel_ID=CHB.Hotel_ID
inner join dbo.Hotel_Customisations HC on HR.Room_ID=HC.Room_ID
inner join dbo.Hotel_Customisation_Types HCT on HC.Hotel_Customisation_Type_ID=HCT.Hotel_Customisation_Type_ID

GO
/****** Object:  Table [dbo].[Restaurant_Menu_Items_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Restaurant_Menu_Items_Customisations](
	[Restaurant_Menu_Items_Customisations_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisation_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisations_Icon] [nvarchar](500) NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Restaurant_Menu_Items_Customisations_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Customer_Hotel_Booking_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Booking_Customisations](
	[Customer_Hotel_Booking_Customisations_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Hotel_Customisation_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Booking_Customisations_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer_Hotel_Preferences]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Preferences](
	[Customer_Hotel_Preference_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Is_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Preference_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer_Reserved_Table]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Reserved_Table](
	[Customer_Reserved_Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Table_Reservation_ID] [int] NOT NULL,
	[Table_ID] [int] NOT NULL,
	[Seat_Reserved] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Reserved_Table_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  Table [dbo].[Customer_Table_Reservations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Table_Reservations](
	[Customer_Table_Reservation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[No_Of_Seats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
	[Reservation_DateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Table_Reservation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Database_Scripts]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Database_Scripts](
	[Script_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Script_Name] [nvarchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Script_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry](
	[Hotel_Laundry_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Laundry_Name] [nvarchar](300) NOT NULL,
	[Laundry_Description] [nvarchar](800) NULL,
	[Laundry_Rate] [float] NOT NULL,
	[Laundry_Hours] [float] NOT NULL,
	[Laundry_Available_From] [time](7) NOT NULL,
	[Laundry_Available_To] [time](7) NOT NULL,
	[Laundry_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Booking](
	[Hotel_Laundry_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Laundry_Booking_DateTime] [datetime] NOT NULL,
	[Laundry_Status] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Booking_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Booking_Details](
	[Hotel_Laundry_Booking_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_Booking_ID] [int] NOT NULL,
	[Hotel_Laundry_Details_ID] [int] NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Booking_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Details](
	[Hotel_Laundry_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_ID] [int] NOT NULL,
	[Laundry_Item] [nvarchar](300) NOT NULL,
	[Laundry_Item_Description] [nvarchar](800) NULL,
	[Laundry_Item_Rate] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_SPA_Service]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_SPA_Service](
	[Hotel_SPA_Service_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Service_Name] [nvarchar](300) NOT NULL,
	[Service_Description] [nvarchar](800) NULL,
	[Service_Rate] [float] NOT NULL,
	[Service_Hours] [float] NOT NULL,
	[Service_Available_From] [time](7) NOT NULL,
	[Service_Available_To] [time](7) NOT NULL,
	[Service_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_SPA_Service] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_SPA_Service_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_SPA_Service_Booking](
	[Hotel_SPA_Service_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_SPA_Service_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Booking_Date_Time] [datetime] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_SPA_Service_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip](
	[Hotel_Trip_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Trip_Name] [nvarchar](300) NOT NULL,
	[Trip_Description] [nvarchar](1000) NULL,
	[Trip_Rate] [float] NOT NULL,
	[Trip_Days] [float] NOT NULL,
	[Trip_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip_Booking](
	[Hotel_Trip_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Booking_Date_Time] [datetime] NOT NULL,
	[Number_Of_Seats] [int] NOT NULL,
	[Booking_Status] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip_Details](
	[Hotel_Trip_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL,
	[Trip_Place] [nvarchar](300) NOT NULL,
	[Trip_Place_Description] [nvarchar](1000) NULL,
	[Trip_Rate_Per_Person] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] ADD  CONSTRAINT DF_Creation_Date_Hotel_customisations DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] ADD  CONSTRAINT DF_Creation_Date_Bookings DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] ADD  CONSTRAINT DF_Creation_Date_hotel_pref DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customer_Reviews] ADD  CONSTRAINT DF_Creation_Date_cust_review DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customisation_Types] ADD  CONSTRAINT DF_Creation_Date_cust_types DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customisations] ADD  CONSTRAINT DF_Creation_Date_customisa DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Details] ADD  CONSTRAINT DF_Creation_Date_Hoteld DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry] ADD  CONSTRAINT DF_Creation_Date_hotelLaund DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking] ADD  CONSTRAINT DF_Creation_Date_landbook DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details] ADD  CONSTRAINT DF_Creation_Date_land_details DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Rooms] ADD  CONSTRAINT DF_Creation_Date_htl_room DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service] ADD  CONSTRAINT DF_Creation_Date_spa_ser DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] ADD  CONSTRAINT DF_Creation_Date_spa_ser_b DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip] ADD  CONSTRAINT DF_Creation_Date_trips DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] ADD  CONSTRAINT DF_Creation_Date_trip_book DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip_Details] ADD  CONSTRAINT DF_Creation_Date_trp_det DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Types] ADD  CONSTRAINT DF_Creation_Date_htl_type DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotels] ADD  CONSTRAINT DF_Creation_Date_htl DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY([Customer_Hotel_Booking_ID])
REFERENCES [dbo].[Customer_Hotel_Bookings] ([Customer_Hotel_Booking_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Hotel_Customisations_Hotel_Customisation_ID] FOREIGN KEY([Hotel_Customisation_ID])
REFERENCES [dbo].[Hotel_Customisations] ([Hotel_Customisation_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Hotel_Customisations_Hotel_Customisation_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY([Room_ID])
REFERENCES [dbo].[Hotel_Rooms] ([Room_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Rooms_Room_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY([Hotel_Customisation_Type_ID])
REFERENCES [dbo].[Hotel_Customisation_Types] ([Hotel_Customisation_Type_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID]
GO

ALTER TABLE [dbo].[Hotel_Customer_Reviews]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customer_Reviews_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY([Customer_Hotel_Booking_ID])
REFERENCES [dbo].[Customer_Hotel_Bookings] ([Customer_Hotel_Booking_ID])
GO
ALTER TABLE [dbo].[Hotel_Customer_Reviews] CHECK CONSTRAINT [FK_dbo.Hotel_Customer_Reviews_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID]
GO
ALTER TABLE [dbo].[Hotel_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY([Hotel_Customisation_Type_ID])
REFERENCES [dbo].[Hotel_Customisation_Types] ([Hotel_Customisation_Type_ID])
GO
ALTER TABLE [dbo].[Hotel_Customisations] CHECK CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID]
GO
ALTER TABLE [dbo].[Hotel_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY([Room_ID])
REFERENCES [dbo].[Hotel_Rooms] ([Room_ID])
GO
ALTER TABLE [dbo].[Hotel_Customisations] CHECK CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Rooms_Room_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Hotel_Laundry_Booking_Hotel_Laundry_Booking_ID] FOREIGN KEY([Hotel_Laundry_Booking_ID])
REFERENCES [dbo].[Hotel_Laundry_Booking] ([Hotel_Laundry_Booking_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Hotel_Laundry_Booking_Hotel_Laundry_Booking_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Details_Hotel_Laundry_Details_ID] FOREIGN KEY([Hotel_Laundry_Details_ID])
REFERENCES [dbo].[Hotel_Laundry_Details] ([Hotel_Laundry_Details_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Details_Hotel_Laundry_Details_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID] FOREIGN KEY([Hotel_Laundry_ID])
REFERENCES [dbo].[Hotel_Laundry] ([Hotel_Laundry_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID]
GO
ALTER TABLE [dbo].[Hotel_Rooms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Rooms_dbo.Hotels_Hotels_ID] FOREIGN KEY([Hotels_ID])
REFERENCES [dbo].[Hotels] ([Hotels_ID])
GO
ALTER TABLE [dbo].[Hotel_Rooms] CHECK CONSTRAINT [FK_dbo.Hotel_Rooms_dbo.Hotels_Hotels_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Hotel_SPA_Service_Hotel_SPA_Service_ID] FOREIGN KEY([Hotel_SPA_Service_ID])
REFERENCES [dbo].[Hotel_SPA_Service] ([Hotel_SPA_Service_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Hotel_SPA_Service_Hotel_SPA_Service_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY([Hotel_Trip_ID])
REFERENCES [dbo].[Hotel_Trip] ([Hotel_Trip_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Hotel_Trip_Hotel_Trip_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Details_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY([Hotel_Trip_ID])
REFERENCES [dbo].[Hotel_Trip] ([Hotel_Trip_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Details_dbo.Hotel_Trip_Hotel_Trip_ID]
GO
ALTER TABLE [dbo].[Hotels]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotels] CHECK CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotels]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Types_Hotel_Type_ID] FOREIGN KEY([Hotel_Type_ID])
REFERENCES [dbo].[Hotel_Types] ([Hotel_Type_ID])
GO
ALTER TABLE [dbo].[Hotels] CHECK CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Types_Hotel_Type_ID]
GO



