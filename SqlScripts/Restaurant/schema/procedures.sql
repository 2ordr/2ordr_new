
-- SP Customer Ingredient Preference
CREATE PROCEDURE [dbo].[uspCustomerIngredientPreference]
	@customerId int 
AS
With CustPrefCTE as (
     select * from CustomerIngredientPreference where CustomerId=@customerId
 ),
PrefCTE as (
	select i.Id,i.Name,i.ParentId,i.Description, p.Include,i.IngredientImage  from Ingredient i left outer join CustPrefCTE p
	on i.Id=p.IngredientId
 ), CTE
as (
 Select  Id,Name, Include, ParentId, Description,IngredientImage, 1 as [Level] from PrefCTE where ParentId is null
 union all

 select b.ID,b.Name, b.Include ,b.ParentId, b.Description,b.IngredientImage, CTE.[Level] +1 as [Level] from PrefCTE b join CTE
 on b.ParentID=CTE.ID
)
select * from CTE

RETURN 0
GO

--SP Customer Allergen Preference
CREATE PROCEDURE [dbo].[uspCustomerAllergenPreference]
	@customerId int 
AS
With CustPrefCTE as (
     select * from CustomerAllergenPreference where CustomerId=@customerId
 ),
PrefCTE as (

	select i.Id,i.Name,i.Description, p.CustomerId,i.AllergenImage  from Allergen i left outer join CustPrefCTE p
	on i.Id=p.AllergenId
	 )

select * from PrefCTE

RETURN 0
Go

--SP Customer Preferred Restaurant
Create Proc dbo.CustomerPreferredRestaurant
  @customerId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct restMenu.RestaurantId from CustomerIngredientPreference cp 
	join Ingredient  i 
	join MenuItemIngredient mi on mi.IngredientId = i.Id
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	join RestaurantMenu restMenu on restMenu.Id=restMnGr.RestaurantMenuId
	on cp.IngredientId=i.Id
	where cp.CustomerId=@customerId
End
Go






CREATE PROCEDURE [dbo].[RestaurantSearch]
	@keywords nvarchar(100) = null,
	@SearchDescriptions bit = 0,

    @latitude float = null,
	@longitude float = null,
	@radius float=10000,

	@cuisineIds nvarchar(100) = null, --list of cuisine ids seperated by comma eg. 1,2,3

	@ratingsMin float = null,
	@ratingsMax float = null,

	@priceForTwoMin float = null,
	@priceForTwoMax float = null,

	@selection int = null,

	@openingHour int = null,
	@closingHour int = null,

	@considerMyPreferences bit = null,


	@sort int = null,
	@sortOrder int = 1,

	@pageStart int = 1,
	@limit int = 20


AS
BEGIN
    
	SET NOCOUNT ON
	print 'coming'
	Create Table #KeywordRestaurant
	(
		RestaurantId int not null
	)

	Declare
		@SearchKeywords bit,
		@sql nvarchar(max)


	SET @Keywords = isnull(@Keywords, '')
	SET @Keywords = rtrim(ltrim(@Keywords))

	IF @Keywords != ''
	Begin
		Set @SearchKeywords = 1
		SET @Keywords = '%' + @Keywords + '%'
	End

	Set @sql= '
	Insert into #KeywordRestaurant(RestaurantId)
	Select r.Id FROM Restaurant r with (NOLOCK)
		WHERE '

	If @searchKeywords = 1
	BEGIN
		SET @sql = @sql + 'PATINDEX(@Keywords, r.[Name]) > 0 ' 
		
		IF @SearchDescriptions = 1
		BEGIN
			
			SET @sql = @sql + '
			UNION
			SELECT r.Id
			FROM Restaurant r with (NOLOCK)
			WHERE PATINDEX(@Keywords, r.[Description]) > 0 '
			
		End
		print @sql +  @keywords
		EXEC sp_executesql @sql, N'@Keywords nvarchar(4000)', @Keywords
	End

	-- Filter by cuisine ids
	
	Set @cuisineIds = isnull(@cuisineIds,'')
	
	Create table #FilteredCuisinesIds
	(
		CuisineId int not null
	)
	
	INSERT INTO #FilteredCuisinesIds (CuisineId)
	SELECT CAST(data as int) FROM [splitstring_to_table](@cuisineIds, ',')	
	
	DECLARE @CuisinesCount int	
	SET @CuisinesCount = (SELECT COUNT(1) FROM #FilteredCuisinesIds)


	
	CREATE TABLE #DisplayOrderTmp 
	(
		[Id] int IDENTITY (1, 1) NOT NULL,
		[RestaurantId] int NOT NULL
	)


	set @sql = '
	INSERT INTO #DisplayOrderTmp ([RestaurantId])
	SELECT r.Id
	FROM
		Restaurant r with (NOLOCK)'


   IF @SearchKeywords = 1
	BEGIN
		SET @sql = @sql + '
		JOIN #KeywordRestaurant kw
			ON  r.Id = kw.RestaurantId'
	END

	IF @CuisinesCount > 0
	BEGIN
		SET @sql = @sql + '
		LEFT JOIN RestaurantCuisine rc with (NOLOCK)
			ON r.Id = rc.RestaurantId'
	END
	

	set @sql = @sql+ '
	Where 
	  1=1'
	
	IF @latitude is not null and @longitude is not null
	Begin
	  DECLARE @CurrentLocation geography; 
	  SET @CurrentLocation  = geography::Point(@latitude,@longitude,4326)

	  Set @sql=@sql + '
		and @CurrentLocation.STDistance([Location]) <= @radius
	   '
	End

	IF @CuisinesCount > 0
	BEGIN
		SET @sql = @sql + '
		AND rc.CuisineId IN (SELECT CuisineId FROM #FilteredCuisinesIds)'
	END


	EXEC sp_executesql @sql, N'@Keywords nvarchar(4000), @CurrentLocation geography, @radius float',  @Keywords,@CurrentLocation,@radius
	select * from #DisplayOrderTmp

RETURN 0
END

GO