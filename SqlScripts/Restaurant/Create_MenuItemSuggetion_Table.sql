

CREATE TABLE [dbo].[MenuItemSuggestion](
	[Id] [int] IDENTITY(1,1) NOT NULL,	 
	[RestaurantMenuItemId] [int] NOT NULL,
	[menuItemSuggestionId] [int] NOT NULL,	
 CONSTRAINT [PK_MenuItemSuggestion_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_MenuItemSuggestion_RestaurantMenuItemId] FOREIGN KEY([RestaurantMenuItemId])
   REFERENCES [dbo].[RestaurantMenuItem] ([Id]),
 CONSTRAINT [FK_MenuItemSuggestion_menuItemSuggestionId] FOREIGN KEY([menuItemSuggestionId])
   REFERENCES [dbo].[RestaurantMenuItem] ([Id])   
 )
GO