Alter table dbo.RestaurantMenuItem
Drop column ImagePrimary
Go

CREATE TABLE [dbo].[RestaurantMenuItemImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
	[Image] [nvarchar](100) NULL,
	[IsPrimary] [bit] NULL,
	[IsActive] [bit] NULL constraint DF_Menu_Item_Images DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint DF_Menu_Item_Images_creation DEFAULT GETUTCDate(),
	constraint PK_Restaurant_Menu_Item_Images Primary Key CLUSTERED (Id),
	constraint FK_Restaurant_Menu_Items_Id FOREIGN KEY (RestaurantMenuItemId)
		REFERENCES dbo.RestaurantMenuItem(Id)
	)
GO