
CREATE TABLE [dbo].[RestaurantTableGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RestaurantTableGroup_CreationDate]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_RestaurantTableGroup_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_RestaurantTableGroup_ResturantId] FOREIGN KEY([RestaurantId])
   REFERENCES [dbo].[Restaurant] ([Id])
 )
GO

Alter Table [RestaurantTable]
Drop Constraint FK_REST_TAB_REST_ID
Go

EXEC sp_RENAME 'RestaurantTable.RestaurantId' , 'RestaurantTableGroupId', 'COLUMN'
Go


--Delete all records from below tables one by one

Delete From [RestaurantTableMenu]
Go
Delete From dbo.RestaurantOrderReview
Go
Delete from dbo.RestaurantOrderItemAdditional
Go
Delete from dbo.RestaurantOrderItem
Go
Delete from dbo.RestaurantOrder
Go

Delete from [RestaurantTable]
Go

Alter table [RestaurantTable]
ADD CONSTRAINT [FK_RestaurantTable_ResturantTableGroupId] FOREIGN KEY([RestaurantTableGroupId])
REFERENCES [dbo].[RestaurantTableGroup] ([Id])
Go