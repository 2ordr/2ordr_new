
  Alter Table [RestaurantTips]
  ADD [CreationDate] [datetime] NOT NULL constraint [DF_RestTips_CreationDate] default '2019-03-12 09:37:42'
  Go
  
  Alter Table [RestaurantTips]
  Drop constraint [DF_RestTips_CreationDate]
  Go
  