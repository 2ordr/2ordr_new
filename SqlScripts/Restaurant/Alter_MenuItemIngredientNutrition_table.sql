
ALTER TABLE [dbo].[MenuItemIngredientNutrition]
DROP COLUMN [Name],[DailyPercentageValue ]
GO


ALTER TABLE [dbo].[MenuItemIngredientNutrition]
ADD [NutritionId] [int] NOT NULL,
CONSTRAINT [FK_MenuItemIngredientNutrition_NutritionId] FOREIGN KEY([NutritionId]) REFERENCES [dbo].[Nutrition] ([Id])
GO
