

CREATE TABLE [dbo].[Nutrition]
(
 [Id] [int] IDENTITY(1,1) NOT NULL,
 [Name] [nvarchar](300) NOT NULL,
 [MeasurementUnitId] [int] NOT NULL
 CONSTRAINT[PK_Nutrition_ID] PRIMARY KEY([Id]),
 CONSTRAINT[FK_Nutrition_MeasurementUnitId] FOREIGN KEY([MeasurementUnitId]) REFERENCES [dbo].[MeasurementUnit] ([Id])
)
GO

CREATE TABLE [dbo].[IngredientNutritionValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IngredientId] [int] NOT NULL,
	[WeightInGrams] [float] NOT NULL,
	[NutritionId] [int] NOT NULL,
	[NutritionValue] [nvarchar](300) NOT NULL,
	[CreationDate] [Datetime] NOT NULL CONSTRAINT [DF_IngredientNutritionValues_CreationDate] DEFAULT GETDATE(),
 CONSTRAINT [PK_IngredientNutritionValues_ID] PRIMARY KEY([Id]),
 CONSTRAINT [FK_IngredientNutritionValues_IngredientId] FOREIGN KEY([IngredientId]) REFERENCES [dbo].[Ingredient] ([Id]),
 CONSTRAINT [FK_IngredientNutritionValues_NutritionId] FOREIGN KEY([NutritionId]) REFERENCES [dbo].[Nutrition] ([Id]),
 )
GO