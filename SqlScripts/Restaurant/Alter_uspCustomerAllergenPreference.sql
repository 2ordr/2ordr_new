USE [2ordr]
GO
/****** Object:  StoredProcedure [dbo].[uspCustomerAllergenPreference]    Script Date: 01/18/2018 10:03:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SP Customer Allergen Preference
ALTER PROCEDURE [dbo].[uspCustomerAllergenPreference]
	@customerId int 
AS
With CustPrefCTE as (
     select * from CustomerAllergenPreference where CustomerId=@customerId
 ),
PrefCTE as (

	select i.Id,i.Name,i.Description, p.CustomerId,i.AllergenImage  from Allergen i left outer join CustPrefCTE p
	on i.Id=p.AllergenId
	 )

select * from PrefCTE

RETURN 0
