--SP Customer Preferred Menu Groups
Create Proc [dbo].[CustomerPreferredMenuGroups]
  @customerId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct restMnGr.Id from CustomerIngredientPreference cp 
	join Ingredient  i 
	join MenuItemIngredient mi on mi.IngredientId = i.Id
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	join RestaurantMenu restMenu on restMenu.Id=restMnGr.RestaurantMenuId
	on cp.IngredientId=i.Id
	where cp.CustomerId=@customerId
End

--exec [CustomerPreferredMenuGroups] 9