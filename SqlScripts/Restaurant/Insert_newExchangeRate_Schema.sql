  
INSERT INTO [dbo].[ExchangeRate]
           ([FromCurrency]
           ,[ToCurrency]
           ,[BaseRate]
           ,[CreationDate])
     VALUES
           (1,1,1,GETDATE()),
           (2,2,1,GETDATE()),
           (3,3,1,GETDATE()),
           (4,4,1,GETDATE()),
           (5,5,1,GETDATE()),
           (6,6,1,GETDATE()),
           (7,7,1,GETDATE()),
           (8,8,1,GETDATE()),
           (9,9,1,GETDATE()),
           (10,10,1,GETDATE()),
           (11,11,1,GETDATE())                                                                                                      
GO