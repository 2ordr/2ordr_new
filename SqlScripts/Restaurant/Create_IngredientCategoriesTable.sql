

CREATE TABLE [dbo].[IngredientCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_IngredientCategories]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_IngredientCategories_ID] PRIMARY KEY(Id),
 )
GO