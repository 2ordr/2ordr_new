

Alter table [RestaurantCartItem]
  ADD ServeLevel int 
Go

UPDATE [RestaurantCartItem] SET [ServeLevel]=1 WHERE [ServeLevel] IS NULL

ALTER TABLE [RestaurantCartItem]
  ALTER COLUMN [ServeLevel] INT NOT NULL
GO  


Alter table [RestaurantMenuGroup]
  ADD ServeLevel int 
Go

UPDATE [RestaurantMenuGroup] SET [ServeLevel]=1 WHERE [ServeLevel] IS NULL

Alter table [RestaurantMenuGroup]
  ALTER column ServeLevel int NOT NULL
Go

ALTER TABLE [RestaurantOrderItem]
  ADD  [ServeLevel] INT 
GO

UPDATE [RestaurantOrderItem] SET [ServeLevel]=1 WHERE [ServeLevel] IS NULL

Alter table [RestaurantOrderItem]
  ALTER column ServeLevel int NOT NULL
Go

