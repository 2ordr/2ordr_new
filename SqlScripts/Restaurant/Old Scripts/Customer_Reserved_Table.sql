USE [2ORDR]
GO

/****** Object:  Table [dbo].[Customer_Reserved_Table]    Script Date: 09/08/2017 15:59:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer_Reserved_Table](
	[Customer_Reserved_Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Table_Reservation_ID] [int] NOT NULL,
	[Table_ID] [int] NOT NULL,
	[Seat_Reserved] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Reserved_Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Customer_Reserved_Table]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Customer_Table_Reservations_Customer_Table_Reservation_ID] FOREIGN KEY([Customer_Table_Reservation_ID])
REFERENCES [dbo].[Customer_Table_Reservations] ([Customer_Table_Reservation_ID])
GO

ALTER TABLE [dbo].[Customer_Reserved_Table] CHECK CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Customer_Table_Reservations_Customer_Table_Reservation_ID]
GO

ALTER TABLE [dbo].[Customer_Reserved_Table]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Restaurant_Tables_Table_ID] FOREIGN KEY([Table_ID])
REFERENCES [dbo].[Restaurant_Tables] ([Table_ID])
GO

ALTER TABLE [dbo].[Customer_Reserved_Table] CHECK CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Restaurant_Tables_Table_ID]
GO


