/* change restaurant types table  to restaurant cuisines and change its dependencies */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'started','10_sql')

Go
EXEC sp_rename 'Restaurant_Types', 'Restaurant_Cuisines'

ALTER TABLE  dbo.Restaurants 
DROP CONSTRAINT [FK_dbo.Restaurants_dbo.Restaurant_Types_Restaurant_Type_ID]

ALTER TABLE dbo.Restaurants
ADD CONSTRAINT [FK_dbo.Restaurant_dbo.Restaurant_Cuisines_Restaurant_Type_ID]
FOREIGN KEY(Restaurant_Type_ID) REFERENCES [dbo].[Restaurant_Cuisines](Restaurant_Type_ID)

/* End of your sql commands */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'ended','10_sql')

Go