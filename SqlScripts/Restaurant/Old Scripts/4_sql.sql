/* standard sql script format, insert your sql command between two inserts and change script name , format number_sql */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'started','4_sql')

Go



/* Insert your sql commands below this line */

CREATE TABLE [dbo].[Hotel_Trip](
	[Hotel_Trip_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL, 
	[Trip_Name] [nvarchar](300) NOT NULL,
	[Trip_Description] [nvarchar](1000) NULL,
	[Trip_Rate] [float] NOT NULL,
	[Trip_Days] [float] NOT NULL,
	[Trip_Photo][nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Trip] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Trip_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY  
(
	[Hotel_ID] 
)REFERENCES [dbo].[Hotel_Details]([Hotel_ID])
)ON [PRIMARY]
GO


CREATE TABLE [dbo].[Hotel_Trip_Details](
	[Hotel_Trip_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL, 
	[Trip_Place] [nvarchar](300) NOT NULL,
	[Trip_Place_Description] [nvarchar](1000) NULL,
	[Trip_Rate_Per_Person] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Trip_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Details_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Trip_Details_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY  
(
	[Hotel_Trip_ID] 
)REFERENCES [dbo].[Hotel_Trip]([Hotel_Trip_ID])
)ON [PRIMARY]
GO


CREATE TABLE [dbo].[Hotel_Trip_Booking](
	[Hotel_Trip_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL, 
	[Booking_Date_Time] [datetime] NOT NULL,
	[Number_Of_Seats][int]NOT NULL,
	[Booking_Status] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Trip_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Booking_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY  
(
	[Hotel_Trip_ID] 
)REFERENCES [dbo].[Hotel_Trip]([Hotel_Trip_ID]),
CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID])

)ON [PRIMARY]
GO


/* End of your sql commands */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'ended','4_sql')

Go