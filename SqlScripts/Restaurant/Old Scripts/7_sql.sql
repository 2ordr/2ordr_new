 

ALTER TABLE [dbo].[Customer_Restaurant_Order_Items]
   DROP CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Items_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID]  
   

ALTER TABLE [dbo].[Customer_Restaurant_Order_Items]
   ADD CONSTRAINT [FK_dbo.Customer_Restaurant_Order_Items_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID]
   FOREIGN KEY ([Customer_Restaurant_Order_ID]) REFERENCES [dbo].[Customer_Restaurant_Orders] ([Customer_Restaurant_Order_ID]) ON DELETE CASCADE