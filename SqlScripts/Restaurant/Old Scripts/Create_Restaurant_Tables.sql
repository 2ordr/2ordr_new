USE [2ORDR]
GO

/****** Object:  Table [dbo].[Restaurant_Tables]    Script Date: 09/08/2017 15:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Restaurant_Tables](
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[Table_Number] [int] NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[No_Of_Seats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[Status_Active] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Restaurant_Tables]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Restaurant_Tables_dbo.Restaurant_Details_Restaurant_ID] FOREIGN KEY([Restaurant_ID])
REFERENCES [dbo].[Restaurant_Details] ([Restaurant_ID])
GO

ALTER TABLE [dbo].[Restaurant_Tables] CHECK CONSTRAINT [FK_dbo.Restaurant_Tables_dbo.Restaurant_Details_Restaurant_ID]
GO

ALTER TABLE [dbo].[Restaurant_Tables] ADD  DEFAULT (getdate()) FOR [Creation_Date]
GO


