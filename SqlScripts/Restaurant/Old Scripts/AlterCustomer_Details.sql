
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN 	[Customer_DOB] [date] NULL;
 	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Address_Street] [nvarchar](100) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Address_Street2] [nvarchar](100) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Address_City] [nvarchar](100) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN 	[Address_Region] [nvarchar](100) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Address_Country] [nvarchar](50) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Address_Code] [nvarchar](100) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Contact_Telephone] [nvarchar](15) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Status_Active] [bit] NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[ResetPasswordCode] [nvarchar](max) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[ActivationCode] [nvarchar](max) NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[LastUpdated] [datetime] NULL;
	
ALTER TABLE  [2ORDR].[dbo].[Customer_Details] ALTER COLUMN	[Creation_Date] [datetime] NULL;