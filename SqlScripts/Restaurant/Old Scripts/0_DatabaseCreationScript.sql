USE [2ordr]
GO

CREATE TABLE [dbo].[Customer_Details](
	[Customer_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ForeName] [nvarchar](50) NOT NULL,
	[Customer_SurName] [nvarchar](50) NOT NULL,
	[Customer_DOB] [date] NULL,
	[Address_Street] [nvarchar](100) NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NULL,
	[Address_Region] [nvarchar](100) NULL,
	[Address_Country] [nvarchar](50) NULL,
	[Address_Code] [nvarchar](100) NULL,
	[Contact_Telephone] [nvarchar](15) NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Status_Active] [bit] NULL,
	[ResetPasswordCode] [nvarchar](max) NULL,
	[ActivationCode] [nvarchar](max) NULL,
	[Provider_Key] nvarchar(100) null,
	[Provider_Name] nvarchar(50) null,
	[LastUpdated] [datetime] NULL,
	[Creation_Date] [datetime] NULL,
CONSTRAINT PK_CU_Det PRIMARY KEY CLUSTERED (Customer_ID)
)
GO


CREATE TABLE [dbo].[Restaurant_Details](
	[Restaurant_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Name] [nvarchar](500) NOT NULL,
	[Restaurant_Description] [nvarchar](3000) NULL,
	[Street] [nvarchar](100) NULL,
	[Street2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[Region] [nvarchar](100) NULL,
	[Country] [nvarchar](50) NULL,
	[Address_Code] [nvarchar](100) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Fax] [nvarchar](15) NULL,
	[Email] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[Cost_Rating] [int] NULL,
	[Status_Partnered] [bit] NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_REST_DET_CRDate default GetUTCDate(),
	[Location] [geography] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[Image_Primary] [nvarchar](100) NULL,
	[Open_Hours] [int] Null,
	[Close_Hours] [Int] Null,
constraint PK_Rest_DET primary key (Restaurant_ID)
)
GO

CREATE TABLE [dbo].[Restaurant_Review](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[Customer_ID] Int Not Null,
	[Review_Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_Rest_REV_CR DEFAULT GETUTCDate(),
Constraint PK_Rest_Rev Primary Key (Review_ID),
Constraint FK_Rest_REV_RestID Foreign Key (Restaurant_Id)
	References Restaurant_Details(Restaurant_ID),
Constraint FK_Rest_Rev_CustID Foreign Key(Customer_Id)
	REFERENCES Customer_Details(Customer_ID)
)

GO






CREATE TABLE [dbo].[Restaurant_Menu](
	[Restaurant_Menu_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_ID] int not null,
	[Restaurant_Menu_Name] [nvarchar](100) NOT NULL,
	[Restaurant_Menu_Description] [nvarchar](255),
	[Restaurant_Menu_Valid] [bit],
	[Creation_Date] [datetime] NOT NULL constraint DF_RM_Create DEFAULT GETUTCDate() ,
 CONSTRAINT PK_Restaurant_Menu PRIMARY KEY CLUSTERED (Restaurant_Menu_ID),
 constraint FK_Restaurant_Menus_Restaurant_ID Foreign key (Restaurant_ID)
	REFERENCES dbo.Restaurant_Details(Restaurant_ID)
 )
	
GO

CREATE TABLE [dbo].[Restaurant_Menu_Group](
	[Restaurant_Menu_Group_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_ID] int NOT NULL,
	[Menu_Group_Name] [nvarchar](100) NOT NULL,
	[Menu_Group_Description] [varchar](255) NULL,
	[Restaurant_Menu_Group_Valid] [bit] NULL,
	[Creation_Date] [datetime]  NOT NULL  constraint DF_Menu_groups_creation DEFAULT GETUTCDate() ,
 CONSTRAINT [PK_Restaurant_Menu_Groups] PRIMARY KEY CLUSTERED (	[Restaurant_Menu_Group_ID] ASC),
 constraint [FK_Menu_Groups_Menu_ID] FOREIGN Key (Restaurant_Menu_ID)
	REFERENCES dbo.Restaurant_Menu(Restaurant_Menu_Id)
  )
GO


CREATE TABLE [dbo].[Restaurant_Menu_Item](
	[Restaurant_Menu_Item_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Group_ID] [int] NOT NULL,
	[Menu_Item_Name] [nvarchar](100) NOT NULL,
	[Menu_Item_Description] [nvarchar](255) NULL,
	[Image_Primary] [nvarchar](100) NULL,
	[Menu_Item_Price] [int] NULL,
	[Menu_Item_Tax_Id] [int] NULL,
	[Menu_Item_Valid] [bit] NULL constraint DF_Menu_Items DEFAULT 1,
	[Menu_Item_Calories] [int] null,
	[Creation_Date] [datetime] NOT NULL constraint DF_Menu_Items_creation DEFAULT GETUTCDate(),
	
	constraint PK_Restaurant_Menu_Items Primary Key CLUSTERED (Restaurant_Menu_Item_Id),
	constraint FK_Restaurant_Menu_Items_GroupId FOREIGN KEY (Restaurant_Menu_Group_ID)
		REFERENCES dbo.Restaurant_Menu_Group(Restaurant_Menu_Group_Id)
	)
GO




CREATE TABLE [dbo].[Menu_Additional_Group](
	[Menu_Additional_Group_ID] [int] IDENTITY(1,1) NOT NULL,
	[Additional_Group_Name] [nvarchar](100) NOT NULL,
	[Additional_Group_Description] [nvarchar](255) NOT NULL,
	[Minimum_Selected] [int] NULL,
	[Maximum_Selected] [int] NULL,
	[Additional_Group_Valid] [bit] NULL constraint DF_Additional_Group DEFAULT 1,
	[Creation_Date] [datetime] NOT NULL constraint DF_Menu_AdditionalGrp_creation DEFAULT GETUTCDate(),
	
	constraint PK_Options_Groups Primary Key CLUSTERED (Menu_Additional_Group_ID)
	)
GO


CREATE TABLE [dbo].[Menu_Additional_Element](
	[Menu_Additional_Element_ID] [int] IDENTITY(1,1) NOT NULL,
	[Menu_Additional_Group_ID] [int] NOT NULL,
	[Additional_Element_Name] [nvarchar](100) NOT NULL,
	[Additional_Element_Description] [nvarchar](255) NOT NULL,
	[Additional_Cost] [int] NULL,
	[Additional_Item_Valid] [bit] NULL constraint DF_Option_Group_Item DEFAULT 1,
	[Creation_Date] [datetime] NOT NULL constraint DF_Menu_OptionsItems_creation DEFAULT GETUTCDate(),
	
	constraint PK_Menu_Additional_Element_ID Primary Key CLUSTERED (Menu_Additional_Element_ID),
	constraint FK_Menu_Additional_Element_Group_ID FOREIGN KEY (Menu_Additional_Group_ID)
		REFERENCES dbo.Menu_Additional_Group(Menu_Additional_Group_ID)
	)
GO


CREATE TABLE [dbo].[Menu_Item_Additional](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Menu_Additional_Group_ID] [int] NOT NULL,
	[Additional_Valid] [bit] NULL constraint DF_Menu_Additional_Option DEFAULT 1,
	[Creation_Date] [datetime] NOT NULL constraint DF_Menu_Items_OptionCr DEFAULT GETUTCDate(),
	
	constraint PK_Item_Option Primary Key CLUSTERED (ID),
	constraint FK_Item_Option_Menu_Item FOREIGN KEY (Restaurant_Menu_Item_Id)
		REFERENCES dbo.Restaurant_Menu_Item(Restaurant_Menu_Item_Id),
	constraint FK_Item_Option_Options_group FOREIGN KEY (Menu_Additional_Group_ID)
		REFERENCES dbo.Menu_Additional_Group(Menu_Additional_Group_ID)

	)
GO

CREATE TABLE [dbo].[Menu_Item_Size](
	[Item_Size_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Item_Size_Name] [nvarchar](100) NOT NULL,
	[Item_Size_Price] [decimal] NULL ,
	[Item_Size_Calories] [int] NULL,
	[IsDefault] Bit Null,
	[Item_Size_Valid] [bit] NULL constraint DF_ITMSIZEVLD DEFAULT 1,	
	constraint PK_Item_Size Primary Key CLUSTERED (Item_Size_ID),
	constraint FK_Item_Size_Menu_Item FOREIGN KEY (Restaurant_Menu_Item_Id)
		REFERENCES dbo.Restaurant_Menu_Item(Restaurant_Menu_Item_Id)
	)
GO

Create Table dbo.Ingredient(
	Ingredient_ID Int Identity(1,1) Not Null,
	Ingredient_Parent_ID int,
	Ingredient_Name nvarchar(50) Not Null,
	Ingredient_Desc nvarchar(100)
Constraint PK_Ingredient PRIMARY KEY CLUSTERED (Ingredient_ID),
Constraint FK_Ingredient_Self_Parent Foreign Key (Ingredient_Parent_ID)
	References dbo.Ingredient(Ingredient_ID)
)
GO


CREATE TABLE [dbo].[Menu_Item_Ingredient](
	[Menu_Ingredient_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Ingredient_ID] [int] NOT NULL,
	[Ingredient_Qty] [nvarchar](25) NULL ,
	constraint PK_Item_Ingredient Primary Key CLUSTERED (Menu_Ingredient_ID),
	constraint FK_Item_Ingredient_Menu_Item FOREIGN KEY (Restaurant_Menu_Item_Id)
		REFERENCES dbo.Restaurant_Menu_Item(Restaurant_Menu_Item_Id),
	Constraint FK_Item_Ingredient_IngredientID FOREIGN KEY(Ingredient_ID)
		References Ingredient(Ingredient_ID)
	)
GO

Create Table dbo.Customer_Ingredient_Preference(
	Ingredient_Preference_Id int not null Identity(1,1),
	Customer_ID int not null,
	Ingredient_Id int not null,
	Include Bit null,
	Constraint PK_Ingredient_Preference PRIMARY Key (Ingredient_Preference_Id),
	Constraint FK_Ingredient_Pref_CustID FOREIGN Key (Customer_ID)
		REFERENCES Customer_Details(Customer_ID),
	Constraint FK_Ing_Pref_IngredientId Foreign Key (Ingredient_ID)
		REFERENCES Ingredient(Ingredient_ID)

)
GO

Create Table dbo.FoodLifeStyle(
	LifeStyle_Id int not null Identity(1,1),
	LifeStyle_Name nvarchar(50) not null,
	LifeStyle_Description nvarchar(100) null,
	Constraint PK_LifeStyle_Id PRIMARY Key (LifeStyle_ID)
)
GO

Create Table dbo.LifeStyle_Ingredient(
	LifeStyle_Ingredient_Id int not null Identity(1,1),
	LifeStyle_ID int not null,
	Ingredient_ID int not null,
	Include bit null,
	Constraint PK_Lifestyle_Ingredient PRIMARY Key (LifeStyle_Ingredient_Id),
	Constraint FK_LF_ING_LifeStyle_Id FOREIGN Key(LifeStyle_ID)
		REFERENCES FoodLifeStyle(LifeStyle_ID),
	Constraint FK_LF_ING_IngId FOREIGN Key (Ingredient_ID)
		REFERENCES Ingredient(Ingredient_ID)
)
GO

Create Table Customer_LifeStyle_Preference(
	LifeStyle_Preference_ID int not null IDENTITY(1,1),
	LifeStyle_ID int not null,
	Customer_Id int not null,
	Constraint PK_Lifestyle_pref PRIMARY KEY(LifeStyle_Preference_ID),
	Constraint FK_LifeStyle_PREF_LIfestyeID FOREIGN key(LifeStyle_ID)
		REFERENCES FoodLifeStyle(LifeStyle_ID),
	Constraint FK_LF_PREF_CUSTID FOREIGN KEY(Customer_ID)
		REFERENCES Customer_Details(Customer_ID)
)


Create Table Allergen(
	Allergen_ID Int not null IDENTITY(1,1),
	Allergen_Name nvarchar(50) not null,
	Allergen_Desc nvarchar(100),
	Constraint PK_Allergen PRIMARY Key (Allergen_ID)
)
GO


Create Table Allergen_Ingredient(
	Allergen_Ingredient_ID int not null Identity(1,1),
	Allergen_ID int not null,
	Ingredient_ID int not null,
	Constraint PK_Allergen_Ing PRIMARY Key(Allergen_Ingredient_ID),
	Constraint FK_Allergn_ING_ALLERGNID Foreign Key(Allergen_ID)
		REFERENCES Allergen(Allergen_ID),
	Constraint FK_ALLRN_ING_INGID Foreign Key(Ingredient_ID)
		REFERENCES Ingredient(Ingredient_ID)
)
GO

Create table Customer_Allergen_Preference(
	Allergen_Preference_ID int not null Identity(1,1),
	Allergen_ID int not null,
	Customer_ID int not null,
	constraint Pk_Allergen_PREf PRIMARY Key (Allergen_Preference_ID),
	Constraint FK_ALLERGEN_PREF FOREIGN Key (Allergen_ID)
		REFERENCES Allergen(Allergen_ID),
	Constraint FK_Allergen_Pref_CustId FOREIGN Key(Customer_ID)
		REFERENCES Customer_Details(Customer_Id)
)
Go


CREATE TABLE [dbo].[Menu_Item_Review](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Comment] [nvarchar](500) NULL ,
	constraint PK_Item_Review Primary Key CLUSTERED (Review_ID),
	constraint FK_Item_Reviews_Menu_Item FOREIGN KEY (Restaurant_Menu_Item_Id)
		REFERENCES dbo.Restaurant_Menu_Item(Restaurant_Menu_Item_Id)
	)
GO


CREATE TABLE [dbo].[Cuisine](
	[Cuisine_ID] [int] IDENTITY(1,1) NOT NULL,
	[Cuisine_Name] [nvarchar](100) NOT NULL,
	[Cuisine_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL Constraint DF_Cuisines_DT default getUtcDate(),
Constraint PK_Cusines PRIMARY Key (Cuisine_ID)
)
GO


CREATE TABLE [dbo].[Restaurant_Cuisine](
	[Restaurant_Cuisine_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[Cuisine_ID] [int] NOT NULL,
	[Restaurant_Cuisine_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL Constraint DF_Restaurant_CUIS_Dt default getUtcDate(),
Constraint PK_Rest_Cuisine PRIMARY Key(Restaurant_Cuisine_ID),
Constraint FK_Rest_CUIS_Rest_ID FOREIGN Key (Restaurant_ID)
	REFERENCES Restaurant_Details(Restaurant_ID),
Constraint FK_Rest_cuis_Cuisne_Id Foreign key(Cuisine_ID)
	REFERENCES Cuisine(Cuisine_Id)
)
GO

CREATE TABLE [dbo].[Restaurant_Table](
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[Table_Number] [int] NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[No_Of_Seats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[Status_Active] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL Constraint DF_REST_TABLE Default getUtcDate(),
Constraint PK_REST_Table Primary Key (Table_ID),
Constraint FK_REST_TAB_REST_ID FOREIGN KEY(Restaurant_ID)
	REFERENCES Restaurant_Details(Restaurant_Id)
)
GO


Create Table dbo.Restaurant_Cart (
	Restaurant_Cart_ID int not null IDENTITY (1,1),
	Customer_ID int Not null,
	Creation_Date DateTime Not Null Constraint DF_REST_CART DEFAULT getUtcDate(),
	Constraint PK_REST_CART Primary Key (Restaurant_Cart_ID),
	Constraint FK_REST_CART_CUST_ID FOREIGN KEY(Customer_ID)
		References Customer_Details(Customer_ID)
)

Create Table dbo.Restaurant_Cart_Item (
	Cart_Item_id int not null Identity(1,1),
	Restaurant_Cart_Id int not null,
	Restaurant_Menu_Item_Id int not null,
	Qty int not null, 
	Constraint PK_REST_CART_ITEM PRIMARY Key (Cart_Item_id),
	Constraint FK_REST_CART_ITM_CARTID FOREIGN KEY(Restaurant_Cart_ID)
		REFERENCES Restaurant_Cart(Restaurant_Cart_ID) ON DELETE CASCADE,
	constraint FK_Restaurant_cart_Itm_Menu_itm_Id foreign Key (Restaurant_Menu_Item_Id)
		REFERENCES Restaurant_Menu_Item(Restaurant_Menu_Item_ID)
)


Create table dbo.Restaurant_Cart_Item_Additional(
	Additional_ID int not null Identity (1,1),
	Cart_Item_ID int not null,
	Menu_Additional_Element_ID int not null,
	constraint PK_REST_CART_ITEM_ADDNAL primary key(Additional_ID),
	constraint FK_REST_CART_ADD_ITMID foreign Key (Cart_Item_ID)
		REFERENCES Restaurant_Cart_Item(Cart_Item_Id) ON DELETE CASCADE,
	Constraint FK_REST_CART_ADD_ELEMENTID FOREIGN Key(Menu_Additional_Element_ID)
		REFERENCES Menu_Additional_Element(Menu_Additional_Element_ID)
)

GO


-- Restaurant Orders

CREATE TABLE [dbo].[Restaurant_Order](
	[Restaurant_Order_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Restaurant_ID] [int] NOT NULL,
	[Customer_Table_Number] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL constraint DF_RO_Create DEFAULT GETUTCDate(),
	[Order_Status] [int] NULL,
CONSTRAINT PK_RO PRIMARY KEY CLUSTERED (Restaurant_Order_ID),
constraint FK_RO_CU Foreign key (Customer_ID)
	REFERENCES dbo.Customer_Details(Customer_ID),
Constraint FK_RO_RestID Foreign Key(Restaurant_Id)
	References Restaurant_Details(Restaurant_Id)
)
GO

Create Table dbo.Restaurant_Order_Item(
	Restaurant_Order_Item_Id int not null Identity (1,1),
	Restaurant_Order_Id int not null,
	Restaurant_Menu_Item_ID int not null,
	Qty int not null,
Constraint PK_REST_ORDER_ITEM primary Key (Restaurant_Order_Item_Id),
Constraint FK_REST_ORD_ITEM_ORDERID Foreign Key (Restaurant_Order_Id)
	References Restaurant_Order(Restaurant_Order_Id),
Constraint FK_REST_ORD_ITEM_MENUID FOREIGN Key (Restaurant_Menu_Item_ID)
	References Restaurant_Menu_Item(Restaurant_Menu_Item_ID)
)
GO


Create Table dbo.Restaurant_Order_Item_Additional(
	Order_Additional_Id int not null Identity (1,1),
	Restaurant_Order_Item_Id Int not null,
	Menu_Additional_Element_ID int not null,
Constraint PK_REST_ORD_ITEM_ADD PRIMARY Key (Order_Additional_Id),
Constraint FK_REST_ORD_ITEM_ADD_ORDERITEMID FOREIGN Key (Restaurant_Order_Item_ID)
	REFERENCES Restaurant_Order_Item(Restaurant_Order_Item_Id),
Constraint FK_Rest_ORD_ITEM_ADD_MENUID FOREIGN Key(Menu_Additional_Element_ID)
	References Menu_Additional_Element(Menu_Additional_Element_ID)
)

GO





CREATE TABLE [dbo].[Restaurant_Order_Review](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Order_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_RO_REV DEFAULT GETUTCDate(),
Constraint PK_RO_Rev Primary Key (Review_ID),
Constraint FK_RO_REV_OrderID Foreign Key (Restaurant_Order_Id)
	References Restaurant_Order(Restaurant_Order_ID)
)

GO



CREATE TABLE [dbo].[Customer_Credit_Card](
	[Customer_Credit_Card_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Card_Holder_Name] [nvarchar](100) NOT NULL,
	[Card_Number] [nvarchar](20) NOT NULL,
	[Card_Expiry_Date] [datetime] NOT NULL,
	[CVV_Number] [nvarchar](4) NOT NULL,
	[IsPrimary] [bit],
	[Card_Label] [nvarchar](50) NULL,
	[Card_Label_Color] [nvarchar](20) NULL,
	[Spending_Limit_Enabled] [bit] NULL,
	[Spending_Limit_Amount] [int] NULL,
	[Currency] [nvarchar](20) NULL,
	[Duration] [int] NULL,
	[Creation_Date] [datetime] NOT NULL constraint DF_CUST_CRED_CARD DEFAULT GETUTCDate(),
constraint PK_CUST_CREDI_CARD primary KEY ( Customer_Credit_Card_ID),
CONSTRAINT FK_CUST_CREDI_CARD_CUSTID foreign key(Customer_ID)
	REFERENCES Customer_Details(Customer_ID)
)
GO

















	

/****** Object:  Table [dbo].[Customer_Hotel_Bookings]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Bookings](
	[Customer_Hotel_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Customer_Reviews]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customer_Reviews](
	[Review_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Review_Score] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Review_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Details](
	[Hotel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Name] [nvarchar](500) NOT NULL,
	[Hotel_Description] [nvarchar](3000) NOT NULL,
	[Address_Street] [nvarchar](100) NOT NULL,
	[Address_Street2] [nvarchar](100) NULL,
	[Address_City] [nvarchar](100) NOT NULL,
	[Address_Region] [nvarchar](100) NOT NULL,
	[Address_Country] [nvarchar](50) NOT NULL,
	[Address_Code] [nvarchar](100) NOT NULL,
	[Contact_Telephone] [nvarchar](15) NOT NULL,
	[Contact_Fax] [nvarchar](15) NULL,
	[Contact_Email] [nvarchar](100) NOT NULL,
	[Contact_Website] [nvarchar](100) NULL,
	[Star_Rating] [int] NOT NULL,
	[Status_Partnered] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
	[Location] [geography] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
 CONSTRAINT [PK_Hotel_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Types]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Types](
	[Hotel_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Type] [nvarchar](100) NOT NULL,
	[Valid_Type] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotels]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotels](
	[Hotels_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Hotel_Type_ID] [int] NOT NULL,
	[Hotel_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Hotels_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotels_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
/****** Object:  Table [dbo].[Hotel_Rooms]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Rooms](
	[Room_ID] [int] IDENTITY(1,1) NOT NULL,
	[Room_Description] [nvarchar](500) NULL,
	[Hotels_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Room_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotels_Room_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
	/*Hotels Room Views*/
Create view [dbo].[Hotels_Room_VW] AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID

GO
/****** Object:  Table [dbo].[Hotel_Customisation_Types]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customisation_Types](
	[Hotel_Customisation_Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Customisation_Type_Description] [nvarchar](500) NOT NULL,
	[Hotel_Customisation_Type_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Customisation_Types] PRIMARY KEY CLUSTERED 
(
	[Hotel_Customisation_Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Customisations](
	[Hotel_Customisation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Room_ID] [int] NOT NULL,
	[Hotel_Customisation_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Hotel_Customisation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Hotel_Bookings_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*Hotel Bookings View*/
Create view [dbo].[Hotel_Bookings_VW] AS
select
	HR.Hotels_ID,
	HR.Room_ID,
	HR.Room_Description,
	CHB.Customer_Hotel_Booking_ID,
	HCT.Hotel_Customisation_Type_Description
from dbo.Hotels H
inner join dbo.Hotel_Rooms HR on H.Hotels_ID=HR.Hotels_ID
inner join dbo.Customer_Hotel_Bookings CHB on H.Hotel_ID=CHB.Hotel_ID
inner join dbo.Hotel_Customisations HC on HR.Room_ID=HC.Room_ID
inner join dbo.Hotel_Customisation_Types HCT on HC.Hotel_Customisation_Type_ID=HCT.Hotel_Customisation_Type_ID

GO
/****** Object:  Table [dbo].[Restaurant_Menu_Items_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Restaurant_Menu_Items_Customisations](
	[Restaurant_Menu_Items_Customisations_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restaurant_Menu_Item_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisation_ID] [int] NOT NULL,
	[Restaurant_Menu_Items_Customisations_Icon] [nvarchar](500) NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Restaurant_Menu_Items_Customisations_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Restaurant_Customisations_VW]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






GO
/****** Object:  Table [dbo].[Customer_Credit_Cards]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



GO
/****** Object:  Table [dbo].[Customer_Hotel_Booking_Customisations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Booking_Customisations](
	[Customer_Hotel_Booking_Customisations_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Hotel_Booking_ID] [int] NOT NULL,
	[Hotel_Customisation_ID] [int] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Booking_Customisations_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer_Hotel_Preferences]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Hotel_Preferences](
	[Customer_Hotel_Preference_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Hotel_Customisation_Type_ID] [int] NOT NULL,
	[Is_Valid] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Hotel_Preference_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer_Reserved_Table]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Reserved_Table](
	[Customer_Reserved_Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Table_Reservation_ID] [int] NOT NULL,
	[Table_ID] [int] NOT NULL,
	[Seat_Reserved] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Reserved_Table_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Customer_Role]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Id] [int] NOT NULL,
	[Role_Id] [int] NOT NULL,
	[Hotel_ID] [int] NULL,
	[Restaurant_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer_Table_Reservations]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Table_Reservations](
	[Customer_Table_Reservation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[No_Of_Seats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
	[Reservation_DateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Table_Reservation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Database_Scripts]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Database_Scripts](
	[Script_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Script_Name] [nvarchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Script_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry](
	[Hotel_Laundry_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Laundry_Name] [nvarchar](300) NOT NULL,
	[Laundry_Description] [nvarchar](800) NULL,
	[Laundry_Rate] [float] NOT NULL,
	[Laundry_Hours] [float] NOT NULL,
	[Laundry_Available_From] [time](7) NOT NULL,
	[Laundry_Available_To] [time](7) NOT NULL,
	[Laundry_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Booking](
	[Hotel_Laundry_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Laundry_Booking_DateTime] [datetime] NOT NULL,
	[Laundry_Status] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Booking_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Booking_Details](
	[Hotel_Laundry_Booking_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_Booking_ID] [int] NOT NULL,
	[Hotel_Laundry_Details_ID] [int] NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Booking_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Laundry_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Laundry_Details](
	[Hotel_Laundry_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_ID] [int] NOT NULL,
	[Laundry_Item] [nvarchar](300) NOT NULL,
	[Laundry_Item_Description] [nvarchar](800) NULL,
	[Laundry_Item_Rate] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Laundry_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_SPA_Service]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_SPA_Service](
	[Hotel_SPA_Service_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Service_Name] [nvarchar](300) NOT NULL,
	[Service_Description] [nvarchar](800) NULL,
	[Service_Rate] [float] NOT NULL,
	[Service_Hours] [float] NOT NULL,
	[Service_Available_From] [time](7) NOT NULL,
	[Service_Available_To] [time](7) NOT NULL,
	[Service_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_SPA_Service] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_SPA_Service_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_SPA_Service_Booking](
	[Hotel_SPA_Service_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_SPA_Service_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Booking_Date_Time] [datetime] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_SPA_Service_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip](
	[Hotel_Trip_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL,
	[Trip_Name] [nvarchar](300) NOT NULL,
	[Trip_Description] [nvarchar](1000) NULL,
	[Trip_Rate] [float] NOT NULL,
	[Trip_Days] [float] NOT NULL,
	[Trip_Photo] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip_Booking]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip_Booking](
	[Hotel_Trip_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Booking_Date_Time] [datetime] NOT NULL,
	[Number_Of_Seats] [int] NOT NULL,
	[Booking_Status] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel_Trip_Details]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel_Trip_Details](
	[Hotel_Trip_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Trip_ID] [int] NOT NULL,
	[Trip_Place] [nvarchar](300) NOT NULL,
	[Trip_Place_Description] [nvarchar](1000) NULL,
	[Trip_Rate_Per_Person] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Hotel_Trip_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Trip_Details_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Restaurant_Tables]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Role]    Script Date: 10/5/2017 5:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Role_Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[Role_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Cuisines] ADD  CONSTRAINT DF_Creation_Date_CUISINE DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Credit_Cards] ADD  CONSTRAINT DF_Creation_Date_Cards DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Details] ADD  CONSTRAINT DF_LastUpdated_Date_Customer DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[Customer_Details] ADD  CONSTRAINT DF_Creation_Date_Customer DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] ADD  CONSTRAINT DF_Creation_Date_Hotel_customisations DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] ADD  CONSTRAINT DF_Creation_Date_Bookings DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] ADD  CONSTRAINT DF_Creation_Date_hotel_pref DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Orders] ADD  CONSTRAINT DF_Creation_Date_rest_ord DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Restaurant_Preferences] ADD  CONSTRAINT DF_Creation_Date_rest_pref DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Customer_Table_Reservations] ADD  CONSTRAINT DF_Creation_Date_table_reser DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customer_Reviews] ADD  CONSTRAINT DF_Creation_Date_cust_review DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customisation_Types] ADD  CONSTRAINT DF_Creation_Date_cust_types DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Customisations] ADD  CONSTRAINT DF_Creation_Date_customisa DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Details] ADD  CONSTRAINT DF_Creation_Date_Hoteld DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry] ADD  CONSTRAINT DF_Creation_Date_hotelLaund DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking] ADD  CONSTRAINT DF_Creation_Date_landbook DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details] ADD  CONSTRAINT DF_Creation_Date_land_details DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Rooms] ADD  CONSTRAINT DF_Creation_Date_htl_room DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service] ADD  CONSTRAINT DF_Creation_Date_spa_ser DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] ADD  CONSTRAINT DF_Creation_Date_spa_ser_b DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip] ADD  CONSTRAINT DF_Creation_Date_trips DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] ADD  CONSTRAINT DF_Creation_Date_trip_book DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Trip_Details] ADD  CONSTRAINT DF_Creation_Date_trp_det DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotel_Types] ADD  CONSTRAINT DF_Creation_Date_htl_type DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Hotels] ADD  CONSTRAINT DF_Creation_Date_htl DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Order_Reviews] ADD  CONSTRAINT DF_Creation_Date_cust_rev DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Details] ADD  CONSTRAINT DF_Creation_Date_rest_det DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Menu_Items] ADD  CONSTRAINT DF_Creation_Date_rest_menu DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Menu_Items_Customisation] ADD  CONSTRAINT DF_Creation_Date_rest_me_cust DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Menu_Items_Customisations] ADD  CONSTRAINT DF_Creation_Date_rest_m_cus DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Menu_Types] ADD  CONSTRAINT DF_Creation_Date_rest_me_type DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Menus] ADD  CONSTRAINT DF_Creation_Date_rest_menu2 DEFAULT (getdate()) FOR [Creation_Date]
GO
ALTER TABLE [dbo].[Restaurant_Tables] ADD  CONSTRAINT DF_Creation_Date_rest_table DEFAULT (getdate()) FOR [Creation_Date]
GO

ALTER TABLE [dbo].[Customer_Credit_Cards] CHECK CONSTRAINT [FK_dbo.Customer_Credit_Cards_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY([Customer_Hotel_Booking_ID])
REFERENCES [dbo].[Customer_Hotel_Bookings] ([Customer_Hotel_Booking_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Hotel_Customisations_Hotel_Customisation_ID] FOREIGN KEY([Hotel_Customisation_ID])
REFERENCES [dbo].[Hotel_Customisations] ([Hotel_Customisation_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Booking_Customisations] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Booking_Customisations_dbo.Hotel_Customisations_Hotel_Customisation_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY([Room_ID])
REFERENCES [dbo].[Hotel_Rooms] ([Room_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Bookings] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Bookings_dbo.Hotel_Rooms_Room_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY([Hotel_Customisation_Type_ID])
REFERENCES [dbo].[Hotel_Customisation_Types] ([Hotel_Customisation_Type_ID])
GO
ALTER TABLE [dbo].[Customer_Hotel_Preferences] CHECK CONSTRAINT [FK_dbo.Customer_Hotel_Preferences_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID]
GO
ALTER TABLE [dbo].[Customer_Reserved_Table]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Customer_Table_Reservations_Customer_Table_Reservation_ID] FOREIGN KEY([Customer_Table_Reservation_ID])
REFERENCES [dbo].[Customer_Table_Reservations] ([Customer_Table_Reservation_ID])
GO
ALTER TABLE [dbo].[Customer_Reserved_Table] CHECK CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Customer_Table_Reservations_Customer_Table_Reservation_ID]
GO
ALTER TABLE [dbo].[Customer_Reserved_Table]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Restaurant_Tables_Table_ID] FOREIGN KEY([Table_ID])
REFERENCES [dbo].[Restaurant_Tables] ([Table_ID])
GO
ALTER TABLE [dbo].[Customer_Reserved_Table] CHECK CONSTRAINT [FK_dbo.Customer_Reserved_Table_dbo.Restaurant_Tables_Table_ID]
GO


ALTER TABLE [dbo].[Restaurant_Orders]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Restaurant_Orders] CHECK CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Restaurant_Orders]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Restaurant_Details_Restaurant_ID] FOREIGN KEY([Restaurant_ID])
REFERENCES [dbo].[Restaurant_Details] ([Restaurant_ID])
GO
ALTER TABLE [dbo].[Restaurant_Orders] CHECK CONSTRAINT [FK_dbo.Customer_Restaurant_Orders_dbo.Restaurant_Details_Restaurant_ID]
GO

ALTER TABLE [dbo].[Customer_Role]  WITH CHECK ADD FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Customer_Role]  WITH CHECK ADD FOREIGN KEY([Restaurant_ID])
REFERENCES [dbo].[Restaurant_Details] ([Restaurant_ID])
GO
ALTER TABLE [dbo].[Customer_Role]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Role_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_Id])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Role] CHECK CONSTRAINT [FK_dbo.Customer_Role_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Customer_Role]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Role_dbo.Role_Role_Id] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Role] ([Role_Id])
GO
ALTER TABLE [dbo].[Customer_Role] CHECK CONSTRAINT [FK_dbo.Customer_Role_dbo.Role_Role_Id]
GO
ALTER TABLE [dbo].[Customer_Table_Reservations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Table_Reservations_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Customer_Table_Reservations] CHECK CONSTRAINT [FK_dbo.Customer_Table_Reservations_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_Customer_Reviews]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customer_Reviews_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID] FOREIGN KEY([Customer_Hotel_Booking_ID])
REFERENCES [dbo].[Customer_Hotel_Bookings] ([Customer_Hotel_Booking_ID])
GO
ALTER TABLE [dbo].[Hotel_Customer_Reviews] CHECK CONSTRAINT [FK_dbo.Hotel_Customer_Reviews_dbo.Customer_Hotel_Bookings_Customer_Hotel_Booking_ID]
GO
ALTER TABLE [dbo].[Hotel_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID] FOREIGN KEY([Hotel_Customisation_Type_ID])
REFERENCES [dbo].[Hotel_Customisation_Types] ([Hotel_Customisation_Type_ID])
GO
ALTER TABLE [dbo].[Hotel_Customisations] CHECK CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Customisation_Types_Hotel_Customisation_Type_ID]
GO
ALTER TABLE [dbo].[Hotel_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Rooms_Room_ID] FOREIGN KEY([Room_ID])
REFERENCES [dbo].[Hotel_Rooms] ([Room_ID])
GO
ALTER TABLE [dbo].[Hotel_Customisations] CHECK CONSTRAINT [FK_dbo.Hotel_Customisations_dbo.Hotel_Rooms_Room_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Hotel_Laundry_Booking_Hotel_Laundry_Booking_ID] FOREIGN KEY([Hotel_Laundry_Booking_ID])
REFERENCES [dbo].[Hotel_Laundry_Booking] ([Hotel_Laundry_Booking_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Hotel_Laundry_Booking_Hotel_Laundry_Booking_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Details_Hotel_Laundry_Details_ID] FOREIGN KEY([Hotel_Laundry_Details_ID])
REFERENCES [dbo].[Hotel_Laundry_Details] ([Hotel_Laundry_Details_ID])
GO
ALTER TABLE [dbo].[Hotel_Laundry_Booking_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Details_Hotel_Laundry_Details_ID]
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID] FOREIGN KEY([Hotel_Laundry_ID])
REFERENCES [dbo].[Hotel_Laundry] ([Hotel_Laundry_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Hotel_Laundry_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID]
GO
ALTER TABLE [dbo].[Hotel_Rooms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Rooms_dbo.Hotels_Hotels_ID] FOREIGN KEY([Hotels_ID])
REFERENCES [dbo].[Hotels] ([Hotels_ID])
GO
ALTER TABLE [dbo].[Hotel_Rooms] CHECK CONSTRAINT [FK_dbo.Hotel_Rooms_dbo.Hotels_Hotels_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Hotel_SPA_Service_Hotel_SPA_Service_ID] FOREIGN KEY([Hotel_SPA_Service_ID])
REFERENCES [dbo].[Hotel_SPA_Service] ([Hotel_SPA_Service_ID])
GO
ALTER TABLE [dbo].[Hotel_SPA_Service_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Hotel_SPA_Service_Hotel_SPA_Service_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Customer_Details_Customer_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY([Hotel_Trip_ID])
REFERENCES [dbo].[Hotel_Trip] ([Hotel_Trip_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Booking] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Booking_dbo.Hotel_Trip_Hotel_Trip_ID]
GO
ALTER TABLE [dbo].[Hotel_Trip_Details]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotel_Trip_Details_dbo.Hotel_Trip_Hotel_Trip_ID] FOREIGN KEY([Hotel_Trip_ID])
REFERENCES [dbo].[Hotel_Trip] ([Hotel_Trip_ID])
GO
ALTER TABLE [dbo].[Hotel_Trip_Details] CHECK CONSTRAINT [FK_dbo.Hotel_Trip_Details_dbo.Hotel_Trip_Hotel_Trip_ID]
GO
ALTER TABLE [dbo].[Hotels]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY([Hotel_ID])
REFERENCES [dbo].[Hotel_Details] ([Hotel_ID])
GO
ALTER TABLE [dbo].[Hotels] CHECK CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Details_Hotel_ID]
GO
ALTER TABLE [dbo].[Hotels]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Types_Hotel_Type_ID] FOREIGN KEY([Hotel_Type_ID])
REFERENCES [dbo].[Hotel_Types] ([Hotel_Type_ID])
GO
ALTER TABLE [dbo].[Hotels] CHECK CONSTRAINT [FK_dbo.Hotels_dbo.Hotel_Types_Hotel_Type_ID]
GO



ALTER TABLE [dbo].[Restaurant_Order_Reviews] CHECK CONSTRAINT [FK_dbo.Restaurant_Order_Reviews_dbo.Restaurant_Order_ID]
GO
ALTER TABLE [dbo].[Restaurant_Menu_Items_Customisations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Restaurant_Menu_Items_Customisations_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID] FOREIGN KEY([Restaurant_Menu_Item_ID])
REFERENCES [dbo].[Restaurant_Menu_Items] ([Restaurant_Menu_Item_ID])
GO
ALTER TABLE [dbo].[Restaurant_Menu_Items_Customisations] CHECK CONSTRAINT [FK_dbo.Restaurant_Menu_Items_Customisations_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID]
GO

ALTER TABLE [dbo].[Restaurant_Menus]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID] FOREIGN KEY([Restaurant_Menu_Item_ID])
REFERENCES [dbo].[Restaurant_Menu_Items] ([Restaurant_Menu_Item_ID])
GO
ALTER TABLE [dbo].[Restaurant_Menus] CHECK CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Items_Restaurant_Menu_Item_ID]
GO
ALTER TABLE [dbo].[Restaurant_Menus]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Types_Restaurant_Menu_Type_ID] FOREIGN KEY([Restaurant_Menu_Type_ID])
REFERENCES [dbo].[Restaurant_Menu_Types] ([Restaurant_Menu_Type_ID])
GO
ALTER TABLE [dbo].[Restaurant_Menus] CHECK CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Menu_Types_Restaurant_Menu_Type_ID]
GO
ALTER TABLE [dbo].[Restaurant_Tables]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Restaurant_Tables_dbo.Restaurant_Details_Restaurant_ID] FOREIGN KEY([Restaurant_ID])
REFERENCES [dbo].[Restaurant_Details] ([Restaurant_ID])
GO
ALTER TABLE [dbo].[Restaurant_Tables] CHECK CONSTRAINT [FK_dbo.Restaurant_Tables_dbo.Restaurant_Details_Restaurant_ID]
GO
