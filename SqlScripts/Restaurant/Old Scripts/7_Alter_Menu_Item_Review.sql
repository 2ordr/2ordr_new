
Alter table [dbo].[Menu_Item_Review]
ADD [Customer_ID] [int] NOT NULL

Go

ALTER TABLE [dbo].[Menu_Item_Review]  WITH CHECK ADD  CONSTRAINT [FK_Menu_Item_CustID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO