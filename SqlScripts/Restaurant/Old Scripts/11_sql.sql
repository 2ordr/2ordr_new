INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'started','11_sql')
Go

EXEC sp_rename 'Restaurant_Cuisines', 'Cuisines' 
GO
SP_RENAME 'Cuisines.Restaurant_Type_ID','Cuisines_ID' 
GO
SP_RENAME 'Cuisines.Restaurant_Type','Cuisines_Name' 
GO
SP_RENAME 'Cuisines.Valid_Type','Cuisines_Valid_Type' 
GO

EXEC sp_rename 'Restaurants', 'Restaurant_Cuisines' 
GO
SP_RENAME 'Restaurant_Cuisines.Restaurants_ID','Restaurant_Cuisines_ID' 
GO
SP_RENAME 'Restaurant_Cuisines.Restaurant_ID','Restaurant_ID' 
GO
SP_RENAME 'Restaurant_Cuisines.Restaurant_Type_ID','Cuisines_ID' 
GO 
SP_RENAME 'Restaurant_Cuisines.Restaurant_Valid','Restaurant_Cuisines_Valid' 
GO
ALTER TABLE  Restaurant_Cuisines
DROP CONSTRAINT [FK_dbo.Restaurant_dbo.Restaurant_Cuisines_Restaurant_Type_ID]
ALTER TABLE  Restaurant_Cuisines
DROP CONSTRAINT [FK_dbo.Restaurants_dbo.Restaurant_Details_Restaurant_ID]
ALTER TABLE Restaurant_Cuisines
ADD CONSTRAINT [FK_dbo.Restaurant_Cuisines_dbo.Cuisines_Cuisines_ID]
FOREIGN KEY(Cuisines_ID) REFERENCES [dbo].[Cuisines](Cuisines_ID)
ALTER TABLE Restaurant_Cuisines
ADD CONSTRAINT [FK_dbo.Restaurant_Cuisines_dbo.Restaurant_Details_Restaurant_ID]
FOREIGN KEY(Restaurant_ID) REFERENCES [dbo].[Restaurant_Details](Restaurant_ID)
ALTER TABLE  Restaurant_Cuisines
DROP CONSTRAINT [PK__Restaura__E72A99BC0519C6AF]
ALTER TABLE [dbo].[Restaurant_Cuisines] ADD CONSTRAINT [PK_Restaurant_Cuisines] PRIMARY KEY CLUSTERED 
(
	[Restaurant_Cuisines_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF,  IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'ended','11_sql')