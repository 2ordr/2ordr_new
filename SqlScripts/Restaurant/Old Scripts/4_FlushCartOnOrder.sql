ALTER TABLE [dbo].[Restaurant_Cart_Item] 
DROP CONSTRAINT [FK_Restaurant_cart_Itm_Menu_itm_Id] 
GO


ALTER TABLE [dbo].[Restaurant_Cart_Item]  WITH CHECK ADD  CONSTRAINT [FK_Restaurant_cart_Itm_Menu_itm_Id] FOREIGN KEY([Restaurant_Menu_Item_Id])
REFERENCES [dbo].[Restaurant_Menu_Item] ([Restaurant_Menu_Item_ID])
ON DELETE CASCADE
GO

