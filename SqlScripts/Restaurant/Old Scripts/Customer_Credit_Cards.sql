USE [2ORDR]
GO

/****** Object:  Table [dbo].[Customer_Credit_Cards]    Script Date: 21/09/2017 09:01:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer_Credit_Cards]
(
	[Customer_Credit_Card_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Card_Holder_Name] [nvarchar](100) NOT NULL,
	[Card_Number] [nvarchar](20) NOT NULL,	
	[Card_Expiry_Date] [datetime] NOT NULL,
	[CVV_Number] [nvarchar](4) NOT NULL,
	[IsPrimery] [bit] NOT NULL,
	[Card_Label] [nvarchar](100) NULL,
	[Card_Label_Color] [nvarchar](100) NULL,
	[Spending_Limit_Enabled] [bit] NULL,
	[Currency] [nvarchar](100) NULL,
	[Duration] [int] NULL,
	[Duration_Type] [nvarchar](100) NULL,
	[Creation_Date] [datetime] NOT NULL,
	
PRIMARY KEY CLUSTERED 
(
	[Customer_Credit_Card_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Customer_Credit_Cards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Credit_Cards_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO

ALTER TABLE [dbo].[Customer_Credit_Cards] CHECK CONSTRAINT [FK_dbo.Customer_Credit_Cards_dbo.Customer_Details_Customer_ID]
GO

ALTER TABLE [dbo].[Customer_Credit_Cards] ADD  DEFAULT (getdate()) FOR [Creation_Date]
GO


