CREATE TABLE [dbo].[Hotel_SPA_Service](
	[Hotel_SPA_Service_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL, 
	[Service_Name] [nvarchar](300) NOT NULL,
	[Service_Description] [nvarchar](800) NULL,
	[Service_Rate] [float] NOT NULL,
	[Service_Hours] [float] NOT NULL,
	[Service_Available_From] time(7) NOT NULL,
	[Service_Available_To] time(7) NOT NULL,
	[Service_Photo][nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_SPA_Service] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_SPA_Service_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY  
(
	[Hotel_ID] 
)REFERENCES [dbo].[Hotel_Details]([Hotel_ID])
)ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_SPA_Service_Booking](
	[Hotel_SPA_Service_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_SPA_Service_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL, 
	[Booking_Date_Time] [datetime] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_SPA_Service_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_SPA_Service_Booking_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Hotel_SPA_Service_Hotel_SPA_Service_ID] FOREIGN KEY  
(
	[Hotel_SPA_Service_ID] 
)REFERENCES [dbo].[Hotel_SPA_Service]([Hotel_SPA_Service_ID]),
CONSTRAINT [FK_dbo.Hotel_SPA_Service_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID])

)ON [PRIMARY]
GO