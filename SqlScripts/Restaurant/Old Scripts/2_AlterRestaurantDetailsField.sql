Alter table [dbo].[Restaurant_Details]
ADD [Night_Price] float
GO

exec SP_RENAME 'Restaurant_Details.Image_Primary','Restaurant_Background'
GO


Alter table [dbo].[Restaurant_Details]
ADD [Restaurant_Logo] nvarchar(100)

Go

update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest1.jpg',[Restaurant_Logo]='no_logo1.png' where Restaurant_ID=1
update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest2.jpg',[Restaurant_Logo]='icon2.png' where Restaurant_ID=2
update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest3.jpg',[Restaurant_Logo]='no_logo3.png' where Restaurant_ID=3
update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest4.jpg',[Restaurant_Logo]='no_logo4.png' where Restaurant_ID=4
update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest5.jpg',[Restaurant_Logo]='no_logo5.png' where Restaurant_ID=5
update [dbo].[Restaurant_Details] set [Restaurant_Background]='rest6.jpg',[Restaurant_Logo]='no_logo6.png' where Restaurant_ID=6
GO

