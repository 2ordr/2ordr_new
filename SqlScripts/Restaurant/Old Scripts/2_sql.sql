/* standard sql script format, insert your sql command between two inserts and change script name , format number_sql */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'started','2_sql')

Go



/* Insert your sql commands below this line */


Alter Table Customer_credit_cards
Add Spending_Limit_Amount integer



/* End of your sql commands */

INSERT INTO [dbo].[Database_Scripts]
     ([Date],
      [Status],
      [Script_Name])
VALUES
(GETDATE(),'ended','2_sql')

Go