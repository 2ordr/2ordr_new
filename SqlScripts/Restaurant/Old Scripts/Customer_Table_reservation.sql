USE [2ORDR]
GO

/****** Object:  Table [dbo].[Customer_Table_Reservations]    Script Date: 09/08/2017 15:58:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer_Table_Reservations](
	[Customer_Table_Reservation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[No_Of_Seats] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Creation_Date] [datetime] NOT NULL,
	[Reservation_DateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Customer_Table_Reservation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Customer_Table_Reservations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customer_Table_Reservations_dbo.Customer_Details_Customer_ID] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer_Details] ([Customer_ID])
GO

ALTER TABLE [dbo].[Customer_Table_Reservations] CHECK CONSTRAINT [FK_dbo.Customer_Table_Reservations_dbo.Customer_Details_Customer_ID]
GO

ALTER TABLE [dbo].[Customer_Table_Reservations] ADD  DEFAULT (getdate()) FOR [Creation_Date]
GO


