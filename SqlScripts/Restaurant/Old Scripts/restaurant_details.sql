ALTER table dbo.Restaurant_Details ADD
[Location] [geography] NULL,
[Longitude] [float]  NULL,
[Latitude] [float] NULL


update dbo.Restaurant_Details 
set 
Location=geography::STGeomFromText ('POINT(-121.527200 45.712113)', 4326),
Longitude=-121.527200,
Latitude= 45.712113
where Restaurant_ID=1

update dbo.Restaurant_Details 
set 
Location=geography::STGeomFromText ('POINT(-121.517265 45.714240)', 4326),
Longitude=-121.517265,
Latitude= 45.714240
where Restaurant_ID=2

update dbo.Restaurant_Details 
set 
Location=geography::STGeomFromText ('POINT(-121.511536 45.714825)', 4326),
Longitude=-121.511536,
Latitude=45.714825
where Restaurant_ID=3


declare @id int
set @id=4
while @id >=4 and @id <= 51
begin
update dbo.Restaurant_Details 
set 
Location=geography::STGeomFromText ('POINT(-111.511536 23.714825)', 4326),
Longitude=-111.511536,
Latitude=23.714825
where Restaurant_ID=@id
select @id = @id + 1
end
GO