

ALTER TABLE dbo.Restaurant_Menus
DROP CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurants_Restaurants_ID]

sp_RENAME 'Restaurant_Menus.Restaurants_ID', 'Restaurant_ID' , 'COLUMN'

ALTER TABLE dbo.Restaurant_Menus
ADD CONSTRAINT [FK_dbo.Restaurant_Menus_dbo.Restaurant_Details_Restaurant_ID]
FOREIGN KEY(Restaurant_ID) REFERENCES [dbo].[Restaurant_Details]([Restaurant_ID])