CREATE TABLE [dbo].[Hotel_Laundry](
	[Hotel_Laundry_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_ID] [int] NOT NULL, 
	[Laundry_Name] [nvarchar](300) NOT NULL,
	[Laundry_Description] [nvarchar](800) NULL,
	[Laundry_Hours] [float] NOT NULL,
	[Laundry_Available_From] time(7) NOT NULL,
	[Laundry_Available_To] time(7) NOT NULL,
	[Laundry_Photo][nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Laundry] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Laundry_dbo.Hotel_Details_Hotel_ID] FOREIGN KEY  
(
	[Hotel_ID] 
)REFERENCES [dbo].[Hotel_Details]([Hotel_ID])
)ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Laundry_Details](
	[Hotel_Laundry_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_ID] [int] NOT NULL,
	[Laundry_Item] [nvarchar](300) NOT NULL,
	[Laundry_Item_Description] [nvarchar](800) NULL,
	[Laundry_Item_Rate] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Laundry_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Details_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID] FOREIGN KEY  
(
	[Hotel_Laundry_ID] 
)REFERENCES [dbo].[Hotel_Laundry]([Hotel_Laundry_ID])
)ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Laundry_Booking](
	[Hotel_Laundry_Booking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NOT NULL, 
	[Laundry_Booking_DateTime] [datetime] NOT NULL,
	[Laundry_Status] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL default GETDATE(),
 CONSTRAINT [PK_Hotel_Laundry_Booking] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Customer_Details_Customer_ID] FOREIGN KEY  
(
	[Customer_ID] 
)REFERENCES [dbo].[Customer_Details]([Customer_ID])
)ON [PRIMARY]
GO

CREATE TABLE [dbo].[Hotel_Laundry_Booking_Details](
	[Hotel_Laundry_Booking_Details_ID] [int] IDENTITY(1,1) NOT NULL,
	[Hotel_Laundry_Booking_ID] [int] NOT NULL,
	[Hotel_Laundry_Details_ID] [int] NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Quantity] [int] NOT NULL
 CONSTRAINT [PK_Hotel_Laundry_Booking_Details] PRIMARY KEY CLUSTERED 
(
	[Hotel_Laundry_Booking_Details_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
,
CONSTRAINT [FK_dbo.Hotel_Laundry_Booking_dbo.Hotel_Laundry_Booking_Hotel_Laundry_Booking_ID] FOREIGN KEY  
(
	[Hotel_Laundry_Booking_ID] 
)REFERENCES [dbo].[Hotel_Laundry_Booking]([Hotel_Laundry_Booking_ID]),
CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Details_Hotel_Laundry_Details_ID] FOREIGN KEY  
(
	[Hotel_Laundry_Details_ID] 
)REFERENCES [dbo].[Hotel_Laundry_Details]([Hotel_Laundry_Details_ID])

)ON [PRIMARY]
GO

ALTER TABLE [dbo].[Hotel_Laundry_Details]
   DROP CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID]  
   

ALTER TABLE [dbo].[Hotel_Laundry_Details]
   ADD CONSTRAINT [FK_dbo.Hotel_Laundry_Details_dbo.Hotel_Laundry_Hotel_Laundry_ID]
   FOREIGN KEY ([Hotel_Laundry_ID]) REFERENCES [dbo].[Hotel_Laundry] ([Hotel_Laundry_ID]) ON DELETE CASCADE
   
   
     