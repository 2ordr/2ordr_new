

/****** Object: SqlProcedure [dbo].[usp_Customer_Ingredient_Preference] Script Date: 11/7/2017 5:35:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Customer_Ingredient_Preference]
	@customer_ID int 
AS
With CustPrefCTE as (
     select * from Customer_Ingredient_Preference where Customer_ID=@customer_ID
 ),
PrefCTE as (
	select i.Ingredient_ID,i.Ingredient_Name,i.Ingredient_Parent_ID, p.Include  from Ingredient i left outer join CustPrefCTE p
	on i.Ingredient_ID=p.Ingredient_Id
 ), CTE
as (
 Select  Ingredient_ID,Ingredient_Name, Include, Ingredient_Parent_ID, 1 as [Level] from PrefCTE where Ingredient_Parent_ID is null
 union all

 select b.Ingredient_ID,b.Ingredient_Name, b.Include ,b.Ingredient_Parent_ID, CTE.[Level] +1 as [Level] from PrefCTE b join CTE
 on b.Ingredient_Parent_ID=CTE.Ingredient_ID
)

select * from CTE



RETURN 0
