 
 /* Restaurant_Details*/
 
INSERT INTO [dbo].[Restaurant_Details]
	--([Restaurant_ID],
	 ([Restaurant_Name],
	 [Restaurant_Description],
	 [Street],
	 [Street2],
	 [City],
	 [Region],
	 [Country],
	 [Address_Code],
	 [Telephone],
	 [Fax],
	 [Email],
	 [Website],
	 [Cost_Rating],
	 [Status_Partnered],
	 Latitude,
	 Longitude,
	 Open_Hours,
	 Close_Hours,
	 Image_Primary
	 
	 )
VALUES 
	( 'Charolais Kroen', 'Denmark Best', 'Fonnesbechsgade 19A','','7400 Herning','Goa','Denmark','7400','45 97 21 35 50','','ka@asdf.com','http://www.charolaiskroen.dk/',2,0,56.133095,8.979354,12,23,'Image1.jpg' ),
	( 'Estuary Cafe', 'Best Indian', 'MG Road','','Panaji','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',4,0,15.397603,73.002426,9,18,'Image2.jpg'),
	( 'Cafe Lilliput', 'Best Indian', 'MG Road','','Margao','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0,15.397603,75.002426,7,20,'Image1.jpg'),
	( 'Viva Panjim', 'Best Indian', 'MG Road','','Anjuna','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',3,0,15.397603,72.002426,9,18,'Image2.jpg'),
	( 'Mosaic Restaurant', 'Best Indian', 'MG Road','','Ponda','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0,15.397603,74.002426,7,20,'Image1.jpg'),
	( 'AZ.U.R - The Marriott', 'Best Indian', 'MG Road','','Miramar','Goa','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',5,0,15.397603,73.002426,7,20,'Image2.jpg')

Update Restaurant_Details set Location= geography::Point([Latitude], [Longitude], 4326)
	

	
	
	


	
INSERT INTO [dbo].[Cuisine]
     ([Cuisine_Name],
      [Cuisine_Valid]
      )
VALUES
	  ('Austrian',1),
	  ('German',1),
	  ('Indian',1),
	  ('Italian',0),
	  ('Czech Wine',0)
	 
	 
INSERT INTO [dbo].[Restaurant_Cuisine]
     ([Restaurant_ID],
      [Cuisine_ID],
      [Restaurant_Cuisine_Valid]
      )
VALUES
	  (1,1,1),
	  (1,2,1),
	  (2,1,1),
	  (2,1,3),
	  (3,2,1),
	  (4,1,1),
	  (5,2,1),
	  (6,3,1)
	   

	   
/*Customer Details*/  
	
INSERT INTO [dbo].[Customer_Details]
      (
	  [Customer_ForeName],
      [Customer_SurName],
      [Customer_DOB],
      [Address_Street],
      [Address_Street2],
      [Address_City],
      [Address_Region],
      [Address_Country],
      [Address_Code],
      [Contact_Telephone],
      [Contact_Email],
      [Status_Active],
      [Creation_Date],
	  [Password]
	  )
VALUES
	   ('rest','Nobel','2001-8-9','MG-Road','','Panjim','Goa','India',403501,1234567890,'rest@gmail.com',1,GETDATE(),'123123'),
	   ('Andrew','Nobel',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,2234567890,'andrew@gmail.com',0,GETDATE(),'123123'),
	   ('Alena','Helly',GETDATE(),'MG-Road','','Panjim','Goa','India',403501,6244567890,'hellydays776@gmail.com',1,GETDATE(),'qwerty'),
	   ('Eugene','Martul',GETDATE(),'','','','Goa','Denmark',403501,7254567890,'ifgen363@gmail.com',1,GETDATE(),'qwerty123'),
	   ('Rajesh','Naik','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'rajesh5@certigoa.com',1,GETDATE(),'123123'),
	   ('Anastasia','Suhovei','1990-7-2','','','','','',403501,8234567890,'a.suhovei+1@besk.com',1,GETDATE(),'Poiuy1'),
	   ('HSV','heben','1990-7-2','Curti','','','','',403501,8234567890,'a.suhovei@besk.coh',1,GETDATE(),'Poiuy1'),
	   ('HSV','hebrvehehrbvrvrvrbrvrvvrbrhrbrvrbehhrbebjr','1990-7-2','Curti','','Ponda','Goa','India',403501,8234567890,'hshvsb@hdb.gh',1,GETDATE(),'Poiuy1')
	   

	   
Insert into dbo.Customer_Credit_Card (
	[Customer_ID],
	[Card_Holder_Name],
	[Card_Number],
	[Card_Expiry_Date],
	[CVV_Number],
	[IsPrimary],
	[Card_Label],
	[Card_Label_Color],
	[Spending_Limit_Enabled],
	[Spending_Limit_Amount],
	[Currency],
	[Duration]

)
VALUES
	(1,'Panki Leo','2223000010476528','2022/10/12','1234',1,'Partner Card','#12323',1,1200,'EURO',1),
	(5,'Rajesh Naik','2343000010476528','2022/02/16','1234',1,'Mycard','#12323',1,1200,'EURO',1),
	(5,'Rajesh Naik','1143000010476528','2018/03/16','1224',0,'Wify card','#22323',null,null,null,null)
	 
	   
	   
	   
	   
	   
Insert into dbo.Restaurant_Review
  (
	[Restaurant_ID],
	[Customer_ID],
	[Review_Score],
	[Comment]
  )
values
   (1,1,4,'This is really great place'),
   (1,2,3,'My favorite restaurant!'),
   (1,3,4,'My favorite restaurant!'),
   (2,1,4,'This is really great place'),
   (2,2,3,'My favorite restaurant!'),
   (2,3,4,'My favorite restaurant!'),
   (3,1,4,'This is really great place'),
   (3,2,3,'My favorite restaurant!'),
   (4,3,4,'My favorite restaurant!'),
   (5,1,4,'This is really great place'),
   (6,2,3,'My favorite restaurant!'),
   (1,4,4,'I like this one..')

	   
	   
	   
	   
-- Restaurnt Orders and Reviews





	   
	  
INSERT INTO [dbo].[Restaurant_Menu](
	[Restaurant_ID],
	[Restaurant_Menu_Name],
	[Restaurant_Menu_Description],
	[Restaurant_Menu_Valid])
 VALUES
	  (1,'Default Menu', 'This is currently active Menu of the restaurant',1),
	  (1,'Future Menu', 'This will active in future during vacations',0),
	  (2,'Default Menu', 'This is currently active Menu of the restaurant',1),
	  (3,'Default Menu', 'This is currently active Menu of the restaurant',1),
	  (3,'Old Menu', 'Menu for special occasions',0)
	  

	  
INSERT INTO [dbo].[Restaurant_Menu_Group](
	[Restaurant_Menu_ID] ,
	[Menu_Group_Name],
	[Menu_Group_Description],
	[Restaurant_Menu_Group_Valid])
 VALUES
	  (1,'Forret', 'Forret',1),
	  (1,'Stegeretter', 'Stegeretter descripton',1),
	  (1,'Desserter', 'Desserter descripton',1),
	  (1,'Børnemenu', 'currently not active ',0),
	  (3,'Breakfast', 'Good Morning with deliciious breakfast',1),
	  (3,'Starters', 'Start with deliciious breakfast',1),
	  (3,'Sea Food', 'Start with deliciious breakfast',1),
	  (3,'Indian Curries', 'Start with deliciious breakfast',1)

	  

INSERT INTO [dbo].[Restaurant_Menu_Item](
	[Restaurant_Menu_Group_ID],
	[Menu_Item_Name] ,
	[Menu_Item_Description] ,
	[Menu_Item_Price] ,
	[Menu_Item_Tax_Id] ,
	[Menu_Item_Valid] ,
	[Menu_Item_Calories],
	Image_Primary )
 VALUES
	  (1,'Gravad Laks', 'Pickled Salmon',95,null,1,null,'Image1.jpg'),
	  (1,'Scampi Fritti', 'med pikant peberdressing Deep-fried scampi with piquant green madagascar dressing',95,null,1,100,'Image1.jpg'),
	  (2,'Morbrad', 'Reelt stykke kød- fuldt afpareret Extremely tender',299,null,1,20,'Image1.jpg'),
	  (2,'Ribeye', 'Grov mamoreret, kraftig smag The marbling enhances the taste of beef ',299,null,1,152,'Image1.jpg'),
	  (3,'Banan aux four', 'Bagt banan med jordbæris, flødeskum, chocoladesauce og nødder',65,null,1,50,'Image1.jpg'),
	  (5,'Plain Omlet', null,60,null,1,68,'Image1.jpg'),
	  (5,'Fried Egg', 'Healty, tasty fries',70,null,1,75,'Image1.jpg'),
	  (6,'Fish Finger', 'Start with deliciious dish',150,null,1,1000,'Image1.jpg')


INSERT INTO [dbo].[Menu_Additional_Group](
	[Additional_Group_Name] ,
	[Additional_Group_Description],
	[Minimum_Selected] ,
	[Maximum_Selected] ,
	[Additional_Group_Valid]
	)
 VALUES
	  ('SELECT OIL', 'Tell us how you want fished to be fried',1,1,1),
	  ('Cooking', 'Degree of Roasting',1,1,1)
	 

INSERT INTO [dbo].[Menu_Additional_Element](  
	[Menu_Additional_Group_ID],
	[Additional_Element_Name],
	[Additional_Element_Description] ,
	[Additional_Cost],
	[Additional_Item_Valid]	
	)
 VALUES
	  (1,'Palm Oil', 'Fried with Palm Oil',10,1),
	  (1,'Soya Oil', 'Fried with Soya Oil',14,1),
	  (1,'Olive Oil', 'Fried with Olive Oil',21,1),
	  (2,'50°C - Rare', 'Description here',10,1),
	  (2,'55°C - Medium', 'Description here',14,1),
	  (2,'60°C - Medium Well', 'Description here',21,1)
	  
	  
	  

INSERT INTO [dbo].[Menu_Item_Additional](  
	[Restaurant_Menu_Item_ID] ,
	[Menu_Additional_Group_ID],
	[Additional_Valid]	
	)
 VALUES
	  (1,1, 1),
 	  (1,2, 1),
	  (3,1,1)

INSERT INTO [dbo].[Menu_Item_Size](  
	[Restaurant_Menu_Item_ID],
	[Item_Size_Name],
	[Item_Size_Price],
	[Item_Size_Calories],
	[IsDefault], 
	[Item_Size_Valid]
	)
 VALUES
	  (1,'Half', 100,50,1,1),
	  (1,'Full', 195,115,0,1)

INSERT INTO [dbo].[Ingredient](  
	Ingredient_Parent_ID,
	Ingredient_Name,
	Ingredient_Desc 
	)
 VALUES
	  (null,'Salmon', 'raw salmon'),
	  (null,'Egg', 'Whole Egg'),
	  (2,'Egg Yolk', 'Yolk of egg')
	  

Insert Into dbo.Menu_Item_Ingredient(
	[Restaurant_Menu_Item_ID],
	[Ingredient_ID],
	[Ingredient_Qty]
	)
VALUES
	(1,1,'100gms'),
	(2,2,'2Nos')


Insert Into dbo.Customer_Ingredient_Preference(
	Customer_ID,
	Ingredient_Id,
	Include
	)
VALUES
	(5,2,0),
	(5,1,1)

Insert Into dbo.FoodLifeStyle(
	
	LifeStyle_Name,
	LifeStyle_Description
	)
VALUES
	('Dairy Free','No to all dairy Items'),
	('Egg Free', 'No to all egg items'),
	('Fish Free', 'No to all egg items'),
	('Onion Free', 'No to all onion items')
	

Insert Into LifeStyle_Ingredient(
	LifeStyle_ID,
	Ingredient_ID,
	Include
	)
VALUES
	(2,2,0)
	


Insert Into Customer_LifeStyle_Preference(
	LifeStyle_ID,
	Customer_Id
	)
VALUES
	(2,5)


Insert Into Allergen(
	Allergen_Name,
	Allergen_Desc
	)
VALUES
	('Egg','Egg Products'),
	('Fish','Fish Products'),
	('Gluten','Gluten Products'),
	('Milk','Milk Products'),
	('Peanuts','Peanut Products'),
	('Soya','Soya Products'),
	('Sulphites','Sulphite Products'),
	('Nuts','Nuts')


Insert Into Allergen_Ingredient(
	Allergen_ID,
	Ingredient_ID 
	)
VALUES
	(1,2),
	(1,3),
	(2,1)

Insert Into Customer_Allergen_Preference(
	Allergen_ID,
	Customer_ID
	)
VALUES
	(1,5)
	
INSERT INTO [dbo].[Menu_Item_Review](  
	[Restaurant_Menu_Item_ID],
	[Review_Score],
	[Comment]  
	)
 VALUES
	  (1,4, 'It was delicious'),
	  (1,3,'It was OK'),
	  (2,5,'It was delicious, I liked it very much'),
	  (3,3,'It was delicious, I liked it very much'),
	  (4,1,'It was OK'),
	  (4,4,'It was delicious, I liked it very much')


Insert into Restaurant_table (
	[Table_Number],
	[Restaurant_ID],
	[No_Of_Seats],
	[Description],
	[Status_Active]
)
VALUES
	(101,1,6,'description if any here',1),
	(102,1,6,'description if any here',1),
	(201,2,6,'description if any here',1),
	(202,2,6,'description if any here',1)



Insert into Restaurant_Cart (
	Customer_ID 
)
VALUES
(1),
(2),
(5)


Insert Into Restaurant_Cart_Item(
	Restaurant_Cart_Id,
	Restaurant_Menu_Item_Id,
	Qty
)
VALUES
(1,1,1),
(1,2,4),
(3,1,1),
(3,2,4)


Insert into dbo.Restaurant_Cart_Item_Additional(
	Cart_Item_ID,
	Menu_Additional_Element_ID
)
values
(1,2),
(3,1)



Insert into dbo.Restaurant_Order
  (
	Customer_Id,
	Restaurant_Id,
	Customer_Table_Number,
	Order_Status
  )
values
   (5,1,1,1),
   (5,1,1,2),
   (5,2,1,1),
   (5,2,1,2),
   (2,1,1,1),
   (2,1,1,2),
   (3,3,1,1)


   
   
Insert into dbo.Restaurant_Order_Item
(
	Restaurant_Order_Id,
	Restaurant_Menu_Item_ID,
	Qty
)   
VALUES
(1,1,1),
(1,2,4),
(3,1,1),
(3,2,4) 

Insert into dbo.Restaurant_Order_Item_Additional(
	Restaurant_Order_Item_Id,
	Menu_Additional_Element_ID
)
values
(1,2),
(3,1)




Insert into dbo.Restaurant_Order_Review
	(
	 Restaurant_Order_ID,
	 Review_Score,
	 Comment
	)
VALUES
	(1,4, 'It was good' ),
	(2,3, 'Food was ok' ),
	(3,5, 'Superb' ),
	(4,1, 'Food was not good. I will never visit this restaurant again.' ),
	(5,5, 'Very good food' )






