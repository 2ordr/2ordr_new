CREATE TABLE [dbo].[CustomerFavoriteRestaurant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] Int Not Null,
	[Restaurant_Id] int not null,
Constraint PK_Cust_fav_Rest Primary Key (Id),
Constraint FK_Cust_Fav_Rest_Cust_ID Foreign Key (Customer_ID)
	REFERENCES Customer_Details(Customer_ID),
Constraint FK_Cust_Fav_Rest_Rest_ID Foreign Key (Restaurant_ID)
	REFERENCES Restaurant_Details(Restaurant_ID)
)
GO


Insert into CustomerFavoriteRestaurant (
  Customer_ID,
  Restaurant_Id
)
values
	(5,1),
	(5,3),
	(1,1),
	(2,2)