--[Customer_Restaurant_Orders]

ALTER TABLE [dbo].[Customer_Restaurant_Orders]
ADD [Order_Status] int null


--[Customer_Restaurant_Order_Items]

ALTER TABLE [dbo].[Customer_Restaurant_Order_Items]
ADD [Quantity] int null,
    [Rate] float null

