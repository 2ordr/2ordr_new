 

ALTER TABLE [dbo].[Restaurant_Customer_Reviews]
   DROP CONSTRAINT [FK_dbo.Restaurant_Customer_Reviews_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID]  
   

ALTER TABLE [dbo].[Restaurant_Customer_Reviews]
   ADD CONSTRAINT [FK_dbo.Restaurant_Customer_Reviews_dbo.Customer_Restaurant_Orders_Customer_Restaurant_Order_ID]
   FOREIGN KEY ([Customer_Restaurant_Order_ID]) REFERENCES [dbo].[Customer_Restaurant_Orders] ([Customer_Restaurant_Order_ID]) ON DELETE CASCADE