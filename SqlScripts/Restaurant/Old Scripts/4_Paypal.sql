CREATE TABLE [dbo].[PaypalDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] Int Not Null,
	[PaypalEmail] nvarchar(100) NOT NULL,
	[Password] nvarchar(25),
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_Paypal_CR DEFAULT GETUTCDate(),
Constraint PK_Paypal Primary Key (Id),
Constraint FK_Paypal_Cust_ID Foreign Key (Customer_ID)
	REFERENCES Customer_Details(Customer_ID)
)
GO


Insert into PayPalDetails (
  Customer_ID,
  PaypalEmail,
  Password
)
values
	(5,'rajesh@certigoa.com','123123'),
	(1,'ram@certigoa.com','123123')