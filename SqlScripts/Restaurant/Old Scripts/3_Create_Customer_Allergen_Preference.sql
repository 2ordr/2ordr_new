
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Customer_Allergen_Preference]
	@customer_ID int 
AS
With CustPrefCTE as (
     select * from Customer_Allergen_Preference where Customer_ID=@customer_ID
 ),
PrefCTE as (

	select i.Allergen_ID,i.Allergen_Name,i.Allergen_Desc, p.Customer_ID  from Allergen i left outer join CustPrefCTE p
	on i.Allergen_ID=p.Allergen_ID
	 )

select * from PrefCTE

RETURN 0

 