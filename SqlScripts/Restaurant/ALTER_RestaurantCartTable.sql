Alter table [RestaurantCart]
  Add [RestaurantId] [int] NOT NULL
 Go
  
Alter table [RestaurantCart]
 ADD CONSTRAINT [FK_RC_RestID] FOREIGN KEY([RestaurantId])
 REFERENCES [dbo].[Restaurant] ([Id])
  Go