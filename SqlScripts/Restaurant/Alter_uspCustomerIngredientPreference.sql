USE [2ordr]
GO
/****** Object:  StoredProcedure [dbo].[uspCustomerIngredientPreference]    Script Date: 11/27/2017 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- Stored Procedures
ALTER PROCEDURE [dbo].[uspCustomerIngredientPreference]
	@customerId int 
AS
With CustPrefCTE as (
     select * from CustomerIngredientPreference where CustomerId=@customerId
 ),
PrefCTE as (
	select i.Id,i.Name,i.ParentId,i.Description, p.Include,i.IngredientImage  from Ingredient i left outer join CustPrefCTE p
	on i.Id=p.IngredientId
 ), CTE
as (
 Select  Id,Name, Include, ParentId, Description,IngredientImage, 1 as [Level] from PrefCTE where ParentId is null
 union all

 select b.ID,b.Name, b.Include ,b.ParentId, b.Description,b.IngredientImage, CTE.[Level] +1 as [Level] from PrefCTE b join CTE
 on b.ParentID=CTE.ID
)
select * from CTE

RETURN 0


