
    --Restaurant isActive field constraint change
  Alter table [Restaurant]
  drop constraint DF__Restauran__IsAct__15702A09 --change to your constraint name
  Go
  
  Alter table [Restaurant]
  Drop column IsActive 
  Go
  
  Alter table [Restaurant]
  ADD IsActive bit NOT NULL default 1
  Go
  
  --Hotel isActive field constraint change
   Alter table [Hotel]
  drop constraint DF__Hotel__IsActive__147C05D0 --change to your constraint name
  Go
  
  Alter table [Hotel]
  Drop column IsActive 
  Go
  
  Alter table [Hotel]
  ADD IsActive bit NOT NULL default 1
  Go