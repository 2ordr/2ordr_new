

CREATE TABLE [dbo].[Nutrient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	CONSTRAINT [PK_Nutrient_ID] PRIMARY KEY([Id]),
 )
GO

CREATE TABLE [dbo].[IngredientNutrition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IngredientId] [int] NOT NULL,
	[IngredientBaseAmount] [float] NOT NULL,
	[MeasurementUnitId] [int] NOT NULL,
	[NutrientId] [int] NOT NULL,
	[NutritionValue] [float] NOT NULL,
	[DailyPercentageValue ] [float] NULL,	 
 CONSTRAINT [PK_IngredientNutrition_ID] PRIMARY KEY([Id]),
 
 CONSTRAINT [FK_IngredientNutrition_IngredientId] FOREIGN KEY([IngredientId])
   REFERENCES [dbo].[Ingredient] ([Id]),
   
    CONSTRAINT [FK_IngredientNutrition_MeasurementUnitId] FOREIGN KEY([MeasurementUnitId])
   REFERENCES [dbo].[MeasurementUnit] ([Id]),
   
   CONSTRAINT [FK_IngredientNutrition_NutrientId] FOREIGN KEY([NutrientId])
   REFERENCES [dbo].[Nutrient] ([Id]),
 )
GO