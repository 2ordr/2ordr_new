CREATE TABLE [dbo].[RestaurantMenuTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[RestaurantMenuId] [int] NOT NULL,
	[TranslatedMenuName] [nvarchar](100) NOT NULL,
	[TranslatedMenuDescription] [nvarchar](255) NULL
 CONSTRAINT [PK_Restaurant_Menu_Translation] PRIMARY KEY(Id),
 Constraint FK_Language_languageID Foreign Key (LanguageId) References Language(Id),
 Constraint FK_RestaurantMenu_MenuI foreign Key(RestaurantMenuId)References RestaurantMenu(Id))
GO