

CREATE TABLE [dbo].[ChainTable](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[ChainName] [nvarchar](300) NOT NULL,
		[CustomerId][int] NOT NULL,
	    [IsActive] [bit] NOT NULL Default(1),
		[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ChainTable DEFAULT GETUTCDate()
 CONSTRAINT [PK_Chain_Table] PRIMARY KEY(Id),
 CONSTRAINT FK_ChainTable_CustID Foreign Key ([CustomerId]) 
         REFERENCES [Customer](Id)
 )
GO


CREATE TABLE [dbo].[ChainDetailsTable](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[ChainId] [int] NOT NULL,
		[RestaurantId][int]  NULL,
		[HotelId][int]  NULL  
 CONSTRAINT [PK_ChainDetailsTable] PRIMARY KEY(Id),
 CONSTRAINT FK_ChainDetailsTable_ChainID Foreign Key ([ChainId]) 
         REFERENCES [ChainTable](Id),
 CONSTRAINT FK_ChainDetailsTable_RestID Foreign Key ([RestaurantId]) 
         REFERENCES [Restaurant](Id),
 CONSTRAINT FK_ChainDetailsTable_HotelID Foreign Key ([HotelId]) 
         REFERENCES [Hotel](Id)        
)
GO