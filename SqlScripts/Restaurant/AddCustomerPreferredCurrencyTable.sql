CREATE TABLE [dbo].[CustomerPreferedCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] Int Not Null,
	[CurrencyId] Int Not null,
Constraint PK_Cust_Pref_Currency Primary Key (Id),
Constraint FK_Cust_Pref_Curr_CurrencyId Foreign Key (CurrencyId)
	References Currency(Id),
Constraint FK_Cust_Pref_Cur_CustID Foreign Key(CustomerId)
	REFERENCES Customer(Id)
)
GO
