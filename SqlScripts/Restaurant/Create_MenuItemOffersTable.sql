

CREATE TABLE [dbo].[MenuItemOffers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuItemId] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Percentage] [float] NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1)
 CONSTRAINT [PK_MenuItemOffers_ID] PRIMARY KEY([Id]),
 CONSTRAINT [FK_MenuItemOffers_MenuItemId] FOREIGN KEY([MenuItemId])
   REFERENCES [dbo].[RestaurantMenuItem] ([Id])
 )
GO

ALTER TABLE [dbo].[RestaurantOrder]
 DROP CONSTRAINT [DF_RO_Create]
GO


ALTER TABLE [dbo].[RestaurantCart]
 DROP CONSTRAINT [DF_REST_CART]
GO





