
Alter table [dbo].[RestaurantOrder]
ALTER Column [CustomerTableNumber] [int] NULL
GO

ALTER TABLE [dbo].[RestaurantOrder]  WITH CHECK ADD  CONSTRAINT [FK_REST_ORD_TBL_NO] FOREIGN KEY([CustomerTableNumber])
REFERENCES [dbo].[RestaurantTable] ([Id])
GO