CREATE TABLE [dbo].[Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(100),
	[LanguageCulture] nvarchar(100),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_LANGUAGE DEFAULT GETUTCDate(),
CONSTRAINT [PK_LANGUAGE_ID] Primary Key (Id)
)
GO