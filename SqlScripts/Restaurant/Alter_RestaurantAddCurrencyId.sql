  
ALTER TABLE [Restaurant]
ADD CurrencyId INT NOT NULL constraint [DEF_CURRENCY_ID] default(1)
Go

ALTER TABLE [Restaurant]
ADD CONSTRAINT [FK_REST_CURRENCY_ID] FOREIGN KEY([CurrencyId])
REFERENCES [Currency] ([Id])
GO

ALTER TABLE [Restaurant]
DROP constraint [DEF_CURRENCY_ID]
Go
