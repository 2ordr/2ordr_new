
  Alter Table [RestaurantOrder]
  Add [NextServingStatus][bit] NOT NULL constraint [DF_RestOrder_NextServingStatus] default 0
  Go
  
  Alter Table [RestaurantOrderItem]
  Add [ServedStatus][bit] NOT NULL constraint [DF_RestOrderItem_ServedStatus] default 0
  Go
  