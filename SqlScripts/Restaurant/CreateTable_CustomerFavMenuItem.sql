
CREATE TABLE [dbo].[CustomerFavoriteMenuItem]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RestaurantMenuItemId] [int] NOT NULL,
 CONSTRAINT [PK_Cust_fav_MenuItem] PRIMARY KEY(Id), 
 CONSTRAINT [FK_Cust_fav_MenuItem_CustId] FOREIGN KEY(CustomerId)
		REFERENCES Customer(Id),
 CONSTRAINT [FK_Cust_fav_MenuItem_ItemId] FOREIGN KEY(RestaurantMenuItemId)
		REFERENCES RestaurantMenuItem(Id)
)
GO


