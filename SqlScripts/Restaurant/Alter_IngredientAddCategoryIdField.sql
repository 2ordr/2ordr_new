
  
INSERT INTO [dbo].[IngredientCategories]
           ([Name],[Description],[IsActive])
    VALUES
           ('Fruits' ,'diff fruits',1)
GO

Alter table [Ingredient]
Drop constraint FK_Ingredient_Self_Parent
Go

Exec sp_rename 'Ingredient.ParentId','IngredientCategoryId','COLUMN'
Go

update [Ingredient] set IngredientCategoryId=1
Go

Alter table [Ingredient]
Alter column IngredientCategoryId int NOT NULL
Go

Alter table [Ingredient]
ADD Constraint [FK_Ingredient_IngredientCategoriesID] foreign key(IngredientCategoryId)
References dbo.IngredientCategories(Id)
Go

CREATE TABLE [dbo].[IngredientFoodProfiles](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[IngredientId][int] NOT NULL,
		[FoodProfileId][int] NOT NULL,
 CONSTRAINT [PK_IngredientFoodProfiles_Id] PRIMARY KEY(Id),
 CONSTRAINT FK_IngredientFoodProfiles_IngredientId Foreign Key ([IngredientId]) 
 REFERENCES [Ingredient](Id),
 CONSTRAINT FK_IngredientFoodProfiles_FoodProfileId Foreign Key ([FoodProfileId]) 
 REFERENCES [FoodProfile](Id)
 )
GO