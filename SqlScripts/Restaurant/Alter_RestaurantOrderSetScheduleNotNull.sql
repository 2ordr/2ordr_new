
  Update [dbo].[RestaurantOrder] set [ScheduledDate]=GETUTCDATE() where [ScheduledDate] IS NULL
  Go
  
  Alter Table [RestaurantOrder]
  Alter column [ScheduledDate] datetime NOT NULL
  Go 