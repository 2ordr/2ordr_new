
  Alter table [IngredientCategories]
  ADD [Image] nvarchar(100) NULL
  Go
  
  Alter table [AllergenCategories]
  ADD [Image] nvarchar(100) NULL
  Go
  
  Alter table [FoodProfile]
  ADD [Image] nvarchar(100) NULL
  Go
  