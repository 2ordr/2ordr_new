CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(100),
	[PhoneCode] nvarchar(100),
	[Image] nvarchar(100),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_COUNTRY DEFAULT GETUTCDate(),
CONSTRAINT [PK_COUNTRY_ID] Primary Key (Id)
)
GO