
Alter table [RestaurantMenuGroup]
ADD LanguageId INT NOT NULL Default(1);
Go

ALTER TABLE [RestaurantMenuGroup]
ADD CONSTRAINT [FK_RESTMENUGroup_LANGUAGE_ID] FOREIGN KEY(LanguageId)
REFERENCES [Language] ([Id])
GO

CREATE TABLE [dbo].[RestaurantMenuGroupTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[RestaurantMenuGroupId] [int] NOT NULL,
	[TranslatedGroupName] [nvarchar](100) NOT NULL,
	[TranslatedGroupDescription] [nvarchar](255) NULL
 CONSTRAINT [PK_Restaurant_Menu_Group_Translation] PRIMARY KEY(Id),
 Constraint FK_Language_RMGT_languageID Foreign Key (LanguageId) References Language(Id),
 Constraint FK_RestaurantMenuGroup_GroupId foreign Key(RestaurantMenuGroupId)References RestaurantMenuGroup(Id))
GO