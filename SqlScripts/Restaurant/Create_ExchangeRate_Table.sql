

CREATE TABLE[dbo].[ExchangeRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FromCurrency] [int] NOT NULL,
	[ToCurrency] [int] NOT NULL,
	[BaseRate]  [float] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_ExchangeRate_CreationDate]  DEFAULT (GETUTCDATE()),
	CONSTRAINT [PK_ExchangeRate_ID] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_ExchangeRate_FromCurrency] FOREIGN KEY ([FromCurrency])
	    REFERENCES [dbo].[Currency] ([Id]),
	CONSTRAINT [FK_ExchangeRate_ToCurrency] FOREIGN KEY ([ToCurrency])
	    REFERENCES [dbo].[Currency] ([Id])
	)
GO	
