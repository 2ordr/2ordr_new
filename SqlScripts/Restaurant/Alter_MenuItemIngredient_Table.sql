

EXEC sp_rename 'dbo.MenuItemIngredient.Qty', 'Weight', 'COLUMN';
GO

ALTER TABLE [dbo].[MenuItemIngredient]
 ALTER COLUMN [Weight] [FLOAT] NOT NULL
GO

ALTER TABLE [dbo].[MenuItemIngredient]
 DROP CONSTRAINT [FK_MenuItemIngredient_MeasurementUnit]
GO 

ALTER TABLE [dbo].[MenuItemIngredient]
 DROP COLUMN [MeasurementUnitId]
GO 