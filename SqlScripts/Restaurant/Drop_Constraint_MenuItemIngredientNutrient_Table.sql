

ALTER TABLE [dbo].[MenuItemIngredientNutrition] 
  DROP  CONSTRAINT [FK_MenuItemIngredientNutrition_MenuItemIngredientId]
GO

ALTER TABLE [dbo].[MenuItemIngredientNutrition] 
  ADD  CONSTRAINT [FK_MenuItemIngredientNutrition_MenuItemIngredientId] FOREIGN KEY([MenuItemIngredientId])
REFERENCES [dbo].[MenuItemIngredient] ([Id])
ON DELETE CASCADE
GO