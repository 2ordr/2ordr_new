

Alter Table [dbo].[RestaurantOrder] 
ADD [CardId] [int]  NULL
GO

Update dbo.RestaurantOrder set CardId = 1
GO

Alter table [dbo].[RestaurantOrder]
ALTER Column [CardId] [int] NOT NULL
GO
 
ALTER TABLE [dbo].[RestaurantOrder]  WITH CHECK ADD CONSTRAINT [FK_RO_CardId] FOREIGN KEY([CardId])
REFERENCES [dbo].[CustomerCreditCard] ([Id])
GO