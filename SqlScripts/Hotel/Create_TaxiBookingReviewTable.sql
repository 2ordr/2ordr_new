CREATE TABLE [dbo].[TaxiBookingReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaxiBookingId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_TaxiBookingRev_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_Taxi_Booking_Review] PRIMARY KEY (Id),
 CONSTRAINT [FK_Taxi_Booking_Rev_TaxiBookingID] FOREIGN KEY([TaxiBookingId])
REFERENCES dbo.TaxiBooking ([Id]))
GO