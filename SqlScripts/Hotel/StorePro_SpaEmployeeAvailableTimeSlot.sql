USE [2ordr]
GO
/****** Object:  StoredProcedure [dbo].[SpaEmployeeAvailableTimeSlot]    Script Date: 01/15/2019 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

       
ALTER PROCEDURE [dbo].[SpaEmployeeAvailableTimeSlot]
    -- Add the parameters for the stored procedure here
@spaEmployeeDetailsId int,
@date datetime,
@extratimeId int,
@extraProcedureIds varchar(2000)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    -- Insert statements for procedure here
    IF @spaEmployeeDetailsId <> 0
    BEGIN
    IF NOT EXISTS (SELECT * FROM dbo.SpaEmployeeDetails where Id=@spaEmployeeDetailsId)
    BEGIN
    RAISERROR('SPA Employee details not found',16,16)
    END
    ELSE
    BEGIN
    DECLARE @EXTRATIME TIME(7)
    DECLARE @EXTRPROTIME TIME(7)
    DECLARE @SPAEMPID int
	SET @EXTRATIME= cast('00:00:00.00' as  datetime);
	SET @EXTRPROTIME= cast('00:00:00.00' as  datetime);
	SET @SPAEMPID=(SELECT SpaEmployeeId FROM dbo.SpaEmployeeDetails where Id=@spaEmployeeDetailsId) -- get Employee Id 
	
			IF @extratimeId <> 0
			BEGIN
			  IF NOT EXISTS (SELECT * FROM dbo.ExtraTime where Id=@extratimeId)
				BEGIN
				RAISERROR('Extra time not found for this id %d',16,16 ,@extratimeId)
				END
			   ELSE
				BEGIN
				SET @EXTRATIME=(SELECT Duration FROM dbo.ExtraTime where Id=@extratimeId) -- set Extratime Duration 
				END
			END
		  
		-- Delete all Temp table if already exist
        IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#TempEmpShift%')
        BEGIN
            DROP TABLE #TempEmpShift
        END 
        IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#BookedTimeSlots%') 
        BEGIN
            DROP TABLE #BookedTimeSlots
        END
        IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#TempTimeSlot%') 
        BEGIN
            DROP TABLE #TempTimeSlot
        END
          IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#TempExtraProcedure%') 
		BEGIN
			DROP TABLE #TempExtraProcedure
		END

        --Create all Temp table if !Exist
        CREATE TABLE #TempTimeSlot(
                r_start datetime,
                r_end datetime
              )
              
         CREATE TABLE #TempEmpShift(
                Id int not null IDENTITY (1,1) , 
                EmpShift_start datetime,
                EmpShift_end datetime
              )
              
         CREATE TABLE #BookedTimeSlots(
                Id int not null IDENTITY (1,1) , 
                Spa_start datetime,
                Spa_end datetime
              )
         Declare @Duration time(7);    
              SET @Duration=(SELECT s.Duration FROM SpaServiceDetail as s
                   join SpaEmployeeDetails as m
                  on m.SpaDetailId=s.Id and
                     m.Id=@spaEmployeeDetailsId)
                    
              SET @Duration=@Duration+cast(@EXTRATIME as datetime)
		          
		        --Extra procedure store in #TempExtraProcedure table with row wise
		        IF @extraProcedureIds <> ''
		        BEGIN
				CREATE TABLE #TempExtraProcedure (procedureIds INT)
				declare @temptable VARCHAR(1000) = @extraProcedureIds
			    Declare @SQL VARCHAR(1000) 
				SELECT @SQL = CONVERT(VARCHAR(1000),' select ' + REPLACE(ISNULL(@temptable,' NULL '),',', ' AS Col UNION ALL SELECT ')) 
				INSERT #TempExtraProcedure (procedureIds)
				EXEC (@SQL)
				
				--Get EtraProcedure using Id and add duration in Total @Duration
				SET NOCOUNT ON  
				DECLARE @p_Id int  
				DECLARE Procedure_CURSOR CURSOR  
				LOCAL  FORWARD_ONLY  FOR  
				SELECT procedureIds FROM #TempExtraProcedure;
				OPEN Procedure_CURSOR  
				FETCH NEXT FROM Procedure_CURSOR INTO  @p_Id  
				WHILE @@FETCH_STATUS = 0  
				BEGIN 
				BEGIN 
				
						IF NOT EXISTS (SELECT * FROM dbo.ExtraProcedure where Id=@p_Id )
						BEGIN
						RAISERROR('ExtraProcedure not found for ID %d',16,16,@p_Id)
						END
					   ELSE
						BEGIN
						SET @EXTRPROTIME=(SELECT Duration FROM dbo.ExtraProcedure where Id=@p_Id)
						  SET @Duration=@Duration+cast(@EXTRPROTIME as datetime);
						END
				END
				FETCH NEXT FROM Procedure_CURSOR INTO @p_Id    
				END  
				CLOSE Procedure_CURSOR  
				DEALLOCATE Procedure_CURSOR 
				END
			 
        --Insert Employee timeslot in #TempEmpShift table
        
        INSERT into #TempEmpShift  
        SELECT  s.StartDateTime as EmpShift_start,s.EndDateTime as EmpShift_end
        FROM [dbo].[SpaEmployeeTimeSlot] as s
         join [dbo].[SpaEmployeeDetails] as d
        on 
        s.SpaEmployeeId=d.SpaEmployeeId AND
        d.Id=@spaEmployeeDetailsId AND
        datediff(day, s.StartDateTime, @date) = 0
         
		--Insert Spa Employee timeslot provided to SpOrderItems 
        
        INSERT into #BookedTimeSlots  
        SELECT s.StartDateTime as Spa_start,s.EndDateTime as Spa_end
        FROM [dbo].[SpaOrderItems] as s
        join [dbo].[SpaEmployeeDetails] as d
        on
        s.SpaEmployeeDetailsId=d.Id AND 
        d.SpaEmployeeId=@SPAEMPID AND
        datediff(day, s.StartDateTime, @date) = 0
         
        --Insert Spa Employee timeslot provided to SpaCartItems 
         
        INSERT into #BookedTimeSlots 
        SELECT s.StartDateTime as Spa_start,s.EndDateTime as Spa_end
        FROM [dbo].[SpaCartItems] as s
        join [dbo].[SpaEmployeeDetails] as d
        on
        s.SpaEmployeeDetailsId=d.Id AND 
        d.SpaEmployeeId=@SPAEMPID and
        datediff(day, s.StartDateTime, @date) = 0
         
         
         Declare @Id int
         Declare @Ids int 
          set @Id= (select COUNT(*) from #BookedTimeSlots)
          set @Ids= 0;
			--returns unreserved dates 
			IF @Id>0
			BEGIN
				WHILE(@Id>0)
				BEGIN
				   -- code block run when condition is TRUE
				IF (@Ids = 0)
				BEGIN
				  	INSERT INTO #TempTimeSlot
					select k.EmpShift_start as r_start,g.Spa_start as r_end
					from
					(select EmpShift_start ,EmpShift_end
						,row_number() over (order by EmpShift_start asc) rowid
						from #TempEmpShift) k
						left join (select Spa_start ,Spa_end
						,row_number() over (order by Spa_start asc) rowid
						from #BookedTimeSlots) g
						on k.rowid = g.rowid 
				     set @Ids= @Ids +1
				END
				ELSE IF (@Ids =1) 
				BEGIN
				
					INSERT INTO #TempTimeSlot
					select k.Spa_end as r_start,l.Spa_start as r_end
					from
					(select Spa_start ,Spa_end
						,row_number() over (order by Spa_start asc) rowid
						from #BookedTimeSlots) k
						left join (select Spa_start ,Spa_end
						,row_number() over (order by Spa_start asc) rowid
						from #BookedTimeSlots) l
						on k.rowid = l.rowid - 1
					 set @Ids= @Ids +1
				END
				ELSE
				BEGIN
				    INSERT INTO #TempTimeSlot
					select h.Spa_end as r_start,k.EmpShift_end as r_end 
					from (select TOP 1 * from #BookedTimeSlots as k order by Spa_start DESC) as h 
					left join  #TempEmpShift as k 
					on h.Id!=0
					 break
				END
				END
				END
				ELSE
				BEGIN
				 INSERT INTO #TempTimeSlot
					select k.EmpShift_start as r_start,k.EmpShift_end as r_end 
					from  #TempEmpShift as k 
				END
				delete from #TempTimeSlot where r_end is null
				
			CREATE TABLE #AvailableTimeSlots(
                StartTime datetime,
                EndTime datetime
              )
				SET NOCOUNT ON  
				DECLARE @s_date datetime  
				DECLARE @e_date datetime  
				DECLARE @k time(2)
				DECLARE @j datetime
				  
				DECLARE EMP_CURSOR CURSOR  
				LOCAL  FORWARD_ONLY  FOR  
				SELECT * FROM   #TempTimeSlot as a where convert(time(2),a.r_end-a.r_start )>@Duration
				OPEN EMP_CURSOR  
				FETCH NEXT FROM EMP_CURSOR INTO  @s_date ,@e_date  
				WHILE @@FETCH_STATUS = 0  
				BEGIN  
				BEGIN
				set @k= cast('00:00:00.00' as  datetime)
				set @j=@s_date+@Duration;
				WHILE(@e_date>@j)
				BEGIN
				    INSERT INTO #AvailableTimeSlots(StartTime,EndTime)
					VALUES(@s_date + @k,@j); 
					set @k= @k+(cast('00:15:00.00' as datetime))
					set @j=@s_date+@Duration+@k
					IF @e_date<@j
					BREAK;
				END
				END
				FETCH NEXT FROM EMP_CURSOR INTO  @s_date ,@e_date   
				END  
				CLOSE EMP_CURSOR  
				DEALLOCATE EMP_CURSOR 
				SELECT * from #AvailableTimeSlots as av where av.StartTime >= @date
    END 
    END
    ELSE
	BEGIN
    RAISERROR('spaEmployeeDetailsId should not zero or negative',16,16)
	END
END
--EXEC [SpaEmployeeAvailableTimeSlot] @spaEmployeeDetailsId =10,@date ='2019-01-16',@extratimeId=0 ,@extraProcedureIds=''