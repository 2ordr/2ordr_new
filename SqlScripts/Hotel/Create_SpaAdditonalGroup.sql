CREATE TABLE [dbo].[SpaAdditionalGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[MinSelected] [int] NULL,
	[MaxSelected] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_SpaAdditionalGroup]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_SpaAdditionalGroup_ID] PRIMARY KEY(Id))
GO