CREATE TABLE [dbo].[TripReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TripId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_TRIP_REVIEW DEFAULT GETUTCDate(),
CONSTRAINT [PK_TRIP_REVIEW_ID] Primary Key (Id),
CONSTRAINT [FK_TRIP_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_TRIP_REVIEW_TRIP_ID] Foreign Key(TripId)
	REFERENCES [Trip](Id)
)
GO