CREATE TABLE [dbo].[HotelRoomImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[Image] [nvarchar](100)NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL
 CONSTRAINT [PK_Hotel_Room_Images] PRIMARY KEY(Id),
 CONSTRAINT FK_Room_RoomId Foreign key(RoomId) references Room(Id))