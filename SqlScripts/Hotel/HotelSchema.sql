


DROP TABLE dbo.Customer_Hotel_Booking_Customisations
GO

DROP TABLE dbo.Customer_Hotel_Preferences
GO
DROP TABLE dbo.Hotel_Customer_Reviews
GO
DROP TABLE dbo.Hotel_Customisations
GO
DROP TABLE dbo.Hotel_Customisation_Types
GO


DROP TABLE dbo.Hotel_Laundry_Booking_Details
GO
DROP TABLE dbo.Hotel_Laundry_Details
GO
DROP TABLE dbo.Hotel_Laundry_Booking
GO
DROP TABLE dbo.Hotel_Laundry
GO

DROP TABLE dbo.Hotel_SPA_Service_Booking
GO
DROP TABLE dbo.Hotel_SPA_Service
GO

DROP TABLE dbo.Hotel_Trip_Details
GO
DROP TABLE dbo.Hotel_Trip_Booking
GO
DROP TABLE dbo.Hotel_Trip
GO


ALTER TABLE [dbo].[CustomerRole]
DROP CONSTRAINT [FK_Hotel_Details_Hotel_ID];  
GO 

DROP TABLE dbo.Customer_Hotel_Bookings
GO
DROP TABLE dbo.Hotel_Rooms
GO
DROP TABLE dbo.Hotels
GO
DROP TABLE dbo.Hotel_Types
GO
DROP TABLE dbo.Hotel_Details
GO

--- Hotel ---

CREATE TABLE [dbo].[Hotel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](3000) NOT NULL,
	[Street] [nvarchar](100) NOT NULL,
	[Street2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NOT NULL,
	[Region] [nvarchar](100) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[Pincode] [nvarchar](100) NOT NULL,
	[Telephone] [nvarchar](15) NOT NULL,
	[Fax] [nvarchar](15) NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Website] [nvarchar](100) NULL,
	[CostForTwo] [int] NOT NULL,
	[IsPartnered] [bit] NOT NULL,
	[Status] [bit] NOT NULL,
	[Location] [geography] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[BackgroundImage] [nvarchar](100) NULL,
	[LogoImage] nvarchar(100),
	[OpeningHours] [time] Null,
	[ClosingHours] [time] Null,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_HOTEL_CRDate default GetUTCDate(),
 CONSTRAINT [PK_HOTEL_DET] PRIMARY KEY (Id) 
) 
GO


-- Favourite Hotel --

CREATE TABLE [dbo].[CustomerFavoriteHotel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
CONSTRAINT [PK_CUST_FAV_HOTEL_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_CUST_FAV_HOTEL_CUSTID] foreign key(CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_CUST_FAV_HOTEL_HOTELID] foreign key(HotelId)
	REFERENCES Hotel(Id),	
)
GO


-- Hotel Restaurant --

CREATE TABLE [dbo].[HotelRestaurant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[RestaurantId] [int] NOT NULL,
CONSTRAINT [PK_HOTEL_REST_ID] Primary Key (Id),
CONSTRAINT [FK_HOTEL_REST_RESTTID] Foreign Key (RestaurantId)
	REFERENCES Restaurant(Id),
CONSTRAINT [FK_HOTEL_REST_HOTELID] Foreign Key(HotelId)
	REFERENCES Hotel(Id)
)
GO

-- Hotel Review --

CREATE TABLE [dbo].[HotelReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_HOTEL_REV DEFAULT GETUTCDate(),
CONSTRAINT [PK_HOTEL_REV_ID] Primary Key (Id),
CONSTRAINT [FK_HOTEL_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_HOTEL_REV_HOTELID] Foreign Key(HotelId)
	REFERENCES Hotel(Id)
)
GO


-- Hotel Room Booking --


CREATE TABLE [dbo].[Status](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NOT NULL
CONSTRAINT [PK_STATUS_ID] PRIMARY KEY (Id)
)
GO


CREATE TABLE [dbo].[RoomType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ROOM_TYPE_CRDate default GetUTCDate(),
 CONSTRAINT [PK_ROOM_TYPE_ID] PRIMARY KEY (Id),
 )
GO


CREATE TABLE [dbo].[Room](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[RoomTypeId] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Rate] [float] NOT NULL,
	[Capacity] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ROOM_CRDate default GetUTCDate(),
 CONSTRAINT [PK_ROOM_ID] PRIMARY KEY (Id),
 CONSTRAINT [FK_ROOM_HOTELID] foreign key(HotelId)
	REFERENCES Hotel(Id),
 CONSTRAINT [FK_ROOM_TYPEID] foreign key(RoomTypeId)
	REFERENCES RoomType(Id)
 )
GO

 
CREATE TABLE [dbo].[CustomerHotelBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[CreditCardId] [int] NOT NULL,
	[From] [time](7) NOT NULL,
	[To] [time](7) NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL constraint DF_CUST_HOTEL_BOOKING DEFAULT GETUTCDate(),
CONSTRAINT [PK_CUST_HOTEL_BOOKING_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_CUST_HOTEL_BOOKING_CUSTID] foreign key(CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_CUST_HOTEL_BOOKING_HOTELID] foreign key(HotelId)
	REFERENCES Hotel(Id),
CONSTRAINT [FK_CUST_HOTEL_BOOKING_ROOMID] foreign key(RoomId)
	REFERENCES Room(Id),
CONSTRAINT [FK_CUST_HOTEL_BOOKING_CARDID] foreign key(CreditCardId)
	REFERENCES CustomerCreditCard(Id),
CONSTRAINT [FK_CUST_HOTEL_BOOKING_STATUSID] foreign key(StatusId)
	REFERENCES [Status](Id)	
)
GO


CREATE TABLE [dbo].[RoomReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ROOM_REV DEFAULT GETUTCDate(),
CONSTRAINT [PK_ROOM_REV_ID] Primary Key (Id),
CONSTRAINT [FK_ROOM_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_ROOM_REV_HOTELID] Foreign Key(HotelId)
	REFERENCES Hotel(Id),
CONSTRAINT [FK_ROOM_REV_ROOMID] Foreign Key(RoomId)
	REFERENCES Room(Id)
)
GO

-- Hotel Services Master--


CREATE TABLE [dbo].[HotelService](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [HotelId] [int] NOT NULL,
    [Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
    [CreationDate] [datetime] NOT NULL CONSTRAINT DF_HotelService_CreDT DEFAULT GETUTCDate(),
CONSTRAINT [PK_HOTELSERVICE_ID] Primary Key (Id),
CONSTRAINT [FK_LAUNDRY_SERVICE_HOTELID] Foreign Key(HotelId)
	REFERENCES Hotel(Id)
)
GO


-- Hotel Laundry Service --


CREATE TABLE [dbo].[Laundry](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [ServiceId] [int] NOT NULL,
    [Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[From] [time](7) NOT NULL,
	[To] [time](7) NOT NULL,
    [CreationDate] [datetime] NOT NULL CONSTRAINT DF_LAUNDRY DEFAULT GETUTCDate(),
CONSTRAINT [PK_LAUNDRY_ID] Primary Key (Id),
CONSTRAINT [FK_LAUNDRY_SERVICE_ID] Foreign Key (ServiceId)
	REFERENCES HotelService(Id)
)
GO


CREATE TABLE [dbo].[LaundryDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Hours] [float] NOT NULL,
	[Rate] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_LAUNDRY_DETAIL DEFAULT GETUTCDate(),
CONSTRAINT [PK_LAUNDRY_DETAIL_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_LAUNDRY_LAUNID] Foreign Key (LaundryId)
	REFERENCES Laundry(Id)
)
GO



CREATE TABLE [dbo].[LaundryBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[LaundryId] [int] NOT NULL,
	[BookingDate] [datetime] NOT NULL CONSTRAINT DF_LAUNDRY_BOOK_DT DEFAULT GETUTCDate(),
	[StatusId] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_LAUNDRY_BOOKING DEFAULT GETUTCDate(),
CONSTRAINT [PK_LAUNDRY_BOOKING_ID] Primary Key (Id),
CONSTRAINT [FK_LAUNDRY_BOOKING_CUSTID] Foreign Key (CustomerId)
    REFERENCES Customer(Id),
CONSTRAINT [FK_LAUNDRY_BOOKING_LAUNID] Foreign Key (LaundryId)
	REFERENCES Laundry(Id),
CONSTRAINT [FK_LAUNDRY_BOOKING_STATUSID] foreign key(StatusId)
	REFERENCES [Status](Id)	
 )
GO



CREATE TABLE [dbo].[LaundryBookingDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryBookingId] [int] NOT NULL,
	[LaundryDetailsId] [int] NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Quantity] [int] NOT NULL,
	[Amount] [float] NOT NULL,
CONSTRAINT [PK_LAUNDRY_BOOKING_DETAIL_ID] Primary Key (Id),
CONSTRAINT [FK_LAUNDRY_BOOKING_DT_BOOKINGID] Foreign Key (LaundryBookingId)
	REFERENCES LaundryBooking(Id),
CONSTRAINT [FK_LAUNDRY_BOOKING_DT_LAUNDTID] Foreign Key (LaundryDetailsId)
	REFERENCES LaundryDetail(Id),
)
GO


-- Hotel SPA Service --


CREATE TABLE [dbo].[SPAService](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [ServiceId] [int] NOT NULL,
    [Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Image] [nvarchar](400) NOT NULL,
	[From] [time](7) NOT NULL,
	[To] [time](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
    [CreationDate] [datetime] NOT NULL CONSTRAINT DF_SPASERVICE DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPASERVICE_ID] Primary Key (Id),
CONSTRAINT [FK_SPAService_SERVICEID] Foreign Key(ServiceId)
	REFERENCES HotelService(Id)
)
GO


CREATE TABLE [dbo].[SPAServiceDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SPAServiceId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Rate] [float] NOT NULL,
	[Hours] [float] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_SPAService_Detail DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPAService_Detail_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_SPAServiceDetail_SPAId] Foreign Key (SPAServiceId)
	REFERENCES SPAService(Id)
)
GO
 
 
 
CREATE TABLE [dbo].[SPAServiceBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[SPAServiceId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[BookingDate] [datetime] NOT NULL CONSTRAINT DF_SPAService_BookDT DEFAULT GETUTCDate(),
	[Amount] [float] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_SPAService_CreDT DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPASERVICE_BOOKING_ID] Primary Key (Id),
CONSTRAINT [FK_SPASERVICE_BOOKING_SPAID] Foreign Key (SPAServiceId)
	REFERENCES SPAService(Id),
CONSTRAINT [FK_SPASERVICE_BOOKING_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_SPASERVICE_BOOKING_STATUSID] foreign key(StatusId)
	REFERENCES [Status](Id)	
 )
GO


CREATE TABLE [dbo].[SPAServiceBookingDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SPAServiceBookingId] [int] NOT NULL,
	[SPAServiceDetailId] [int] NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Quantity] [int] NOT NULL,
	[Amount] [float] NOT NULL,
CONSTRAINT [PK_SPAService_BOOKING_DETAIL_ID] Primary Key (Id),
CONSTRAINT [FK_SPAService_BOOKING_DT_BOOKINGID] Foreign Key (SPAServiceBookingId)
	REFERENCES SPAServiceBooking(Id),
CONSTRAINT [FK_SPAService_BOOKING_DT_LAUNDTID] Foreign Key (SPAServiceDetailId)
	REFERENCES SPAServiceDetail(Id),
)
GO


-- Hotel Trip Service --

 
CREATE TABLE [dbo].[Trip](
	[Id] [int] IDENTITY(1,1) NOT NULL,	
	[ServiceId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Days] [float] NOT NULL,
	[Rate] [float] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_Trip_CreDT DEFAULT GETUTCDate(),
CONSTRAINT [PK_Trip_Id] PRIMARY KEY (Id),
CONSTRAINT [FK_Trip_ServiceId] Foreign Key (ServiceId)
	REFERENCES HotelService(Id)
)
GO


CREATE TABLE [dbo].[TripDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [int] NOT NULL,
	[Place] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_TripDetails_CreDT DEFAULT GETUTCDate(),
 CONSTRAINT [PK_TripDetails_Id] PRIMARY KEY (Id),
 CONSTRAINT [FK_TripDetails_TripId] Foreign Key(TripID)
	REFERENCES [Trip](Id)  
)
GO



CREATE TABLE [dbo].[TripBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TripId] [int] NOT NULL,
	[BookingDate] [datetime] NOT NULL,
	[Seats] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_TripBooking_CreDT DEFAULT GETUTCDate(),
 CONSTRAINT [PK_TripBooking] PRIMARY KEY (Id),
 CONSTRAINT [FK_TripBooking_CustId] Foreign Key (CustomerId)
	REFERENCES [Customer](Id),
 CONSTRAINT [FK_TripBooking_TripId] Foreign Key(TripId)
	REFERENCES [Trip](Id),
 CONSTRAINT [FK_TripBooking_StatusId] foreign key(StatusId)
	REFERENCES [Status](Id)	
)
GO


-- Hotel Taxi Service --

 
CREATE TABLE [dbo].[TaxiDetils](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Number] [nvarchar](50) NOT NULL,
	[RatePerKm] [float] NOT NULL,
	[Seats] [int] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_TaxiDetails_CreDT DEFAULT GETUTCDate(),
CONSTRAINT [PK_TaxiDetails_Id] PRIMARY KEY (Id),
CONSTRAINT [FK_TaxiDetails_ServiceId] Foreign Key (ServiceId)
	REFERENCES HotelService(Id)
)
GO


CREATE TABLE [dbo].[TaxiBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TaxiId] [int] NOT NULL,
	[From] [datetime] NOT NULL,
	[To] [datetime] NOT NULL,
	[Seats] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_TaxiBooking_CreDT DEFAULT GETUTCDate(),
CONSTRAINT [PK_TaxiBooking] PRIMARY KEY (Id),
CONSTRAINT [FK_TaxiBooking_CustId] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_TaxiBooking_TaxiId] Foreign Key(TaxiId)
	REFERENCES TaxiDetils(Id),
CONSTRAINT [FK_TaxiBooking_StatusId] foreign key(StatusId)
	REFERENCES [Status](Id)	
)
GO

--Alter Constraint of CustomerRole

ALTER TABLE [dbo].[CustomerRole]  
ADD CONSTRAINT [FK_Hotel_ID] FOREIGN KEY([HotelId])
    REFERENCES [dbo].[Hotel] ([Id])
GO