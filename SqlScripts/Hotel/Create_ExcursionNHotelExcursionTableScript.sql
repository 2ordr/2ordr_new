CREATE TABLE [dbo].[Excursion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Image] [nvarchar](400) NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Excursion]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_Excursion_ID] PRIMARY KEY(Id),
 )
GO

  CREATE TABLE [dbo].[HotelExcursion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelServiceId] [int] NOT NULL,
	[ExcursionId] [int] NOT NULL,
 CONSTRAINT [PK_HotelExcursion] PRIMARY KEY (Id),
 CONSTRAINT [FK_HotelExcursion_HotelServiceId] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id]),
 CONSTRAINT [FK_HotelExcursion_ExcursionId] FOREIGN KEY([ExcursionId])
REFERENCES [dbo].[Excursion] ([Id]))
GO

