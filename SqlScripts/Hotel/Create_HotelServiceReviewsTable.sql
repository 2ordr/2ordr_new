CREATE TABLE [dbo].[HotelServicesReviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelServiceId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_HOTEL_SERVICE_REVIEW DEFAULT GETUTCDate(),
CONSTRAINT [PK_HOTEL_SERVICE_REVIEW_ID] Primary Key (Id),
CONSTRAINT [FK_HOTEL_SERVICE_REVIEW_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_HOTEL_SERVICE_REVIEW_HOTEL_SERVICE_ID] Foreign Key(HotelServiceId)
	REFERENCES [HotelServices](Id)
)
GO
