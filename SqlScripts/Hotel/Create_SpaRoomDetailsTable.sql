
  Drop table [SpaRoomEmployee]
  Go
  
  CREATE TABLE [dbo].[SpaRoomDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaRoomId] [int] NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
 CONSTRAINT [PK_Spa_Room_Details] PRIMARY KEY (Id),
 CONSTRAINT [FK_Spa_Room_Details_SpaRoomID] FOREIGN KEY([SpaRoomId])
REFERENCES [dbo].[SpaRoom] ([Id]),
 CONSTRAINT [FK_Spa_Room_Details_SpaServiceDetailID] FOREIGN KEY([SpaServiceDetailId])
REFERENCES [dbo].[SpaServiceDetail] ([Id]))
GO
