
Alter Table [LaundryAdditionalGroup]
Drop constraint [FK_LaundryAdditionalGroup_HotelId]
Go

EXEC sp_RENAME 'LaundryAdditionalGroup.HotelId' , 'LaundryId', 'COLUMN'
Go

 Truncate table dbo.LaundryCartItemsAdditional
 Go
 
 Truncate table dbo.LaundryOrderItemsAdditional
 Go
 
 Delete from  dbo.LaundryAdditionalElement
 Go
 
Delete from [LaundryAdditionalGroup]
Go

Alter Table [LaundryAdditionalGroup]
Add constraint [FK_LaundryAdditionalGroup_LaundryId] FOREIGN KEY (LaundryId) REFERENCES [Laundry] (Id)
Go

 ALter Table [LaundryOrder]
 Add  [QuickPayPaymentId] nvarchar(100) NULL
 Go 