

ALTER TABLE [dbo].[SpaOrderItems]
   DROP CONSTRAINT [FK_Spa_Order_Items_SpaOrder_ID] 
   GO
   
ALTER TABLE [dbo].[SpaOrderItems]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Order_Items_SpaOrder_ID] FOREIGN KEY([SpaOrderId])
REFERENCES [dbo].[SpaOrder] ([Id])
ON DELETE CASCADE
GO
   
	
ALTER TABLE [dbo].[SpaOrderItemsAdditional]
   DROP CONSTRAINT [FK_Spa_Order_Items_Additional_SpaOrderItems_ID] 
   GO
   
ALTER TABLE [dbo].[SpaOrderItemsAdditional]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Order_Items_Additional_SpaOrderItems_ID] FOREIGN KEY([SpaOrderItemsId])  
REFERENCES [dbo].[SpaOrderItems] ([Id])
 ON DELETE CASCADE
GO
   
	
ALTER TABLE [dbo].[SpaOrderItemsEmployee]
DROP CONSTRAINT [FK_Spa_Order_Items_Employee_SpaOrderItems_ID] 
GO

ALTER TABLE [dbo].[SpaOrderItemsEmployee]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Order_Items_Employee_SpaOrderItems_ID] FOREIGN KEY([SpaOrderItemsId])
REFERENCES [dbo].[SpaOrderItems] ([Id])
ON DELETE CASCADE
GO
   
 