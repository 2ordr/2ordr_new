

CREATE TABLE [dbo].[SpaTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaOrderId] [int] NOT NULL,
	[TipAmount] [float] NOT NULL,
	[CardId][int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[Comment][nvarchar](500) NULL,
 CONSTRAINT [PK_SpaTips_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaTips_OrderId] FOREIGN KEY([SpaOrderId]) REFERENCES SpaOrder(Id),
 CONSTRAINT [FK_SpaTips_CardId] FOREIGN KEY(CardId)REFERENCES CustomerCreditCard(Id)
 )
GO


CREATE TABLE [dbo].[LaundryTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryOrderId] [int] NOT NULL,
	[TipAmount] [float] NOT NULL,
	[CardId][int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[Comment][nvarchar](500) NULL,
 CONSTRAINT [PK_LaundryTips_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_LaundryTips_OrderId] FOREIGN KEY([LaundryOrderId]) REFERENCES LaundryOrder(Id),
 CONSTRAINT [FK_LaundryTips_CardId] FOREIGN KEY(CardId)REFERENCES CustomerCreditCard(Id)
 )
GO


CREATE TABLE [dbo].[HousekeepingTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingOrderId] [int] NOT NULL,
	[TipAmount] [float] NOT NULL,
	[CardId][int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[Comment][nvarchar](500) NULL,
 CONSTRAINT [PK_HousekeepingTips_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_HousekeepingTips_OrderId] FOREIGN KEY([HousekeepingOrderId]) REFERENCES HousekeepingOrder(Id),
 CONSTRAINT [FK_HousekeepingTips_CardId] FOREIGN KEY(CardId)REFERENCES CustomerCreditCard(Id)
 )
GO


