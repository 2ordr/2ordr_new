
ALTER TABLE [dbo].[HousekeepingCartItems]
ADD [Quantity] [int] NOT NULL
GO

ALTER TABLE [dbo].[HousekeepingOrderItems]
ADD [Quantity] [int] NOT NULL
GO