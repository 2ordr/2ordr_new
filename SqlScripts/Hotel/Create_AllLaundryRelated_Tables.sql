

CREATE TABLE [dbo].[LaundryCart](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[Total] [float] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Laundry_CART_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_Laundry_CART] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_CART_CUST_Id] FOREIGN KEY([CustomerId])REFERENCES [Customer] ([Id]),
 CONSTRAINT [FK_Laundry_Cart_RoomId] FOREIGN KEY([RoomId])REFERENCES [Room] ([Id]))
 GO
 
 CREATE TABLE [dbo].[LaundryCartItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryCartId] [int] NOT NULL,
	[LaundryDetailId] [int] NOT NULL,
	[Quantity] int NOT NULL,
	[ServeLevel] int NOT NULL DEFAULT(1),
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
	[ScheduledDate] [Datetime] NULL,
 CONSTRAINT [PK_Laundry_Cart_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Cart_Items_LaundryCart_Id] FOREIGN KEY([LaundryCartId])REFERENCES [LaundryCart] ([Id]),
 CONSTRAINT [FK_Laundry_Cart_Items_LaundryDetailId] FOREIGN KEY([LaundryDetailId])REFERENCES [LaundryDetail] ([Id]))
 GO
 
  CREATE TABLE [dbo].[LaundryCartItemsAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryCartItemId] [int] NOT NULL,
	[LaundryAdditionalElementId] [int] NOT NULL,
	[Quantity] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Laundry_Cart_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Cart_Items_Additional_LaundryCartItem_Id] FOREIGN KEY([LaundryCartItemId])REFERENCES [LaundryCartItems] ([Id]),
 CONSTRAINT [FK_Laundry_Cart_Items_Additional_LaundryAdditionalElement_Id] FOREIGN KEY([LaundryAdditionalElementId])REFERENCES [LaundryAdditionalElement] ([Id]))
 GO