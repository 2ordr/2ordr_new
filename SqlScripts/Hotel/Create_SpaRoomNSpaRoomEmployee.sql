CREATE TABLE [dbo].[SpaRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_SpaRoom_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_Spa_Room] PRIMARY KEY (Id),
 CONSTRAINT [FK_Spa_Room_HotelID] FOREIGN KEY([HotelId])
REFERENCES [dbo].[Hotel] ([Id]))
GO

CREATE TABLE [dbo].[SpaRoomEmployee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaRoomId] [int] NOT NULL,
	[SpaEmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Spa_Room_Employee] PRIMARY KEY (Id),
 CONSTRAINT [FK_Spa_Room_Employee_SpaRoomID] FOREIGN KEY([SpaRoomId])
REFERENCES [dbo].[SpaRoom] ([Id]),
 CONSTRAINT [FK_Spa_Room_Employee_SpaEmployeeID] FOREIGN KEY([SpaEmployeeId])
REFERENCES [dbo].[SpaEmployee] ([Id]))
GO
