ALTER TABLE [dbo].[HotelRoomImages]
   DROP CONSTRAINT [FK_Room_RoomId] 
   GO
   
ALTER TABLE [dbo].[HotelRoomImages]  WITH CHECK ADD  CONSTRAINT [FK_Room_RoomId] FOREIGN KEY([RoomId])
	REFERENCES [dbo].[Room] ([Id])
	ON DELETE CASCADE
	GO

