

CREATE TABLE [dbo].[LaundryOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[CardId] [int] NOT NULL,
	[OrderStatus] [int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[OrderTotal] [float] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_LaundryOrder_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_LaundryOrder] PRIMARY KEY(Id),
 CONSTRAINT [FK_LaundryOrder_CUST_Id] FOREIGN KEY([CustomerId])REFERENCES [dbo].[Customer] ([Id]),
 CONSTRAINT [FK_LaundryOrder_RoomID] FOREIGN KEY([RoomId])REFERENCES [dbo].[Room] ([Id]),
 CONSTRAINT [FK_LaundryOrder_CardId] FOREIGN KEY([CardId])REFERENCES [dbo].[CustomerCreditCard] ([Id]))
 Go
 
  CREATE TABLE [dbo].[LaundryOrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryOrderId] [int] NOT NULL,
	[LaundryDetailId] [int] NOT NULL,
	[Quantity] int NOT NULL,
	[ServeLevel] int NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
	[ScheduledDate] [datetime] NULL,
	[LaundryDetailOrderStatus] [bit] NOT NULL,
 CONSTRAINT [PK_Laundry_Order_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Order_Items_LaundryOrder_ID] FOREIGN KEY([LaundryOrderId])REFERENCES [LaundryOrder] ([Id]),
 CONSTRAINT [FK_Laundry_Order_Items_LaundryDetailID] FOREIGN KEY([LaundryDetailId])REFERENCES [LaundryDetail] ([Id]))
 Go
 
 CREATE TABLE [dbo].[LaundryOrderItemsAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryOrderItemsId] [int] NOT NULL,
	[LaundryAdditionalElementId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Laundry_Order_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Order_Items_Additional_LaundryOrderItems_ID] FOREIGN KEY([LaundryOrderItemsId])REFERENCES [LaundryOrderItems] ([Id]),
 CONSTRAINT [FK_Laundry_Order_Items_Additional_LaundryAdditionalElementId] FOREIGN KEY([LaundryAdditionalElementId])REFERENCES [LaundryAdditionalElement] ([Id]))
 Go