

CREATE TABLE [dbo].[ConciergeGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_ConciergeGroup]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_ConciergeGroup_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ConciergeGroup_HotelId] FOREIGN KEY([HotelId])
   REFERENCES [dbo].[Hotel]([Id])
 )
GO


CREATE TABLE [dbo].[Concierge](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConciergeGroupId] [int] NOT NULL,
	[Question] [nvarchar](300) NOT NULL,
	[Answer] [nvarchar](500) NOT NULL,	
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Concierge]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_Concierge_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_Concierge_ConciergeGroupId] FOREIGN KEY([ConciergeGroupId])
   REFERENCES [dbo].[ConciergeGroup]([Id])
 )
GO