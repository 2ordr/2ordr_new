
DELETE FROM [dbo].[SpaCart]
GO 

ALTER TABLE [dbo].[SpaCartItems]
ADD [StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL
GO	