INSERT INTO [SpaService]
           ([HotelServiceId],[Name],[Description],[Image],[IsActive])
     VALUES
           (1,'Sauna','sauna','sauna.jpg',1),
           (1,'Manicure','Manicure','Manicure.jpg',1),
           (1,'Massage','Massage','Massage.jpg',1)
GO

INSERT INTO [SpaServiceDetail]
           ([SPAServiceId],[Name],[Description],[Price],[Duration],[IsActive])
     VALUES
           (3 ,'Thai Massage','Thai Massage',105,2.30,1),
           (3 ,'Spanish Massage','Spanish Massage',105,2,1),
           (3 ,'Anti-Cellulite Massage','Anti-Cellulite Massage',85,2,1)      
GO


