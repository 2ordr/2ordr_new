CREATE TABLE [dbo].[SpaEmployee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Age] [int] NOT NULL,
	[Education] [nvarchar](200) NULL,
	[Experience] [float] NULL,
	[Skill] [nvarchar](800) NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_SpaEmployee]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_SpaEmployee_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SSpaEmployee_CustomerId] FOREIGN KEY([CustomerId])REFERENCES [dbo].[Customer] ([Id]))
GO