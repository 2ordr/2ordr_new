

CREATE TABLE [dbo].[LaundryAdditionalGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[MinSelected] [int] NULL,
	[MaxSelected] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_LaundryAdditionalGroup]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_LaundryAdditionalGroup_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_LaundryAdditionalGroup_HotelId] FOREIGN KEY([HotelId])
   REFERENCES [dbo].[Hotel] ([Id])
 )
GO