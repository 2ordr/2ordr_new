
/* Hotel Details*/
 INSERT INTO [dbo].[Hotel]
           ([Name]
           ,[Description]
           ,[Street]
           ,[Street2]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Pincode]
           ,[Telephone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[CostForTwo]
           ,[IsPartnered]
           ,[Status]
           ,[CreationDate]
           ,[Location]
           ,[Longitude]
           ,[Latitude]
           ,[BackgroundImage]
           ,[LogoImage]
           ,[OpeningHours]
           ,[ClosingHours])
     VALUES
	( 'Taj Lake Palace', 'Lake Palace', '','','Udaipur','Rajasthan','India','7400','4597213550','','ka@asdf.com','http://www.charolaiskroen.dk/',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','07:00:00','23:00:00' ),
	( 'The Oberoi Amarvilas', 'Best In Agra', 'MG Road','','Agra','Utter Pradesh','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'HOTEL IL PELLICANO', 'Best Italy hotel', '','','Località Sbarcatello','Porto Ercole','Italy','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','12:00:00','23:00:00' ),
	( 'JAWAI', 'Best Indian', 'Bilaspur','','Pali-Marwar','Rajasthan','India','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','08:00:00','23:00:00'),
	( 'HÔTEL CRILLON LE BRAVE', 'Best Indian', 'MG Road','','Ponda','Crillon-le-Brave','France','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','10:00:00','23:00:00'),
	( 'BERKELEY RIVER LODGE', 'Best Indian', ' Berkeley River','',' Berkeley',' Berkeley','Australia','403510','123123','234324','ka@asdf.com','http://222.asdf.com',50,0,1,GETDATE(),geography::Point(56.133095, 8.979354, 4326),8.979354,56.133095,'background.jpg','logo.jpg','09:00:00','23:00:00' )

GO


INSERT INTO [dbo].[Currency]
           ([Country]
           ,[CurrencyName]
           ,[Code]
           ,[Symbol]
           ,[CreationDate])
         VALUES
          ('United States','Dollar', 'USD', '$',GETDATE()),
          ('Spain','Euro', 'EUR', '€',GETDATE()),
		  ('Germany','Euro', 'EUR', '€',GETDATE()),
		  ('Japan','Yen', 'JPY', '¥',GETDATE()),
          ('United Kingdom','Pound', 'GBP', '£',GETDATE()),          
		  ('Australia','Dollars', 'AUD', '$',GETDATE()),
		  ('Canada','Dollar', 'KYD', '$',GETDATE()),
		  ('Switzerland','Franc', 'CHF', 'CHF',GETDATE()),
		  ('China','Yuan Renminbi', 'CNY', '¥',GETDATE()),
		  ('Sweden','krona', 'SEK', 'kr',GETDATE()),
		  ('New Zealand','Dollar', 'NZD', '$',GETDATE()),
		  ('Mexico','peso', 'MXN', '$',GETDATE()),
		  ('Singapore','dollar', 'SGD', '$',GETDATE()),
		  ('Hong Kong ','dollar', 'HKD', '$',GETDATE()),
		  ('Norway','Krone', 'NOK', 'kr',GETDATE()),
		  ('Denmark','Krone', 'DKK', 'kr',GETDATE())
 GO
 
 
  
 INSERT INTO [dbo].[Country]
           ([Name]
           ,[PhoneCode]
           ,[Image]          
           ,[CreationDate])
         VALUES
          ('United States','1', 'UState.jpg',GETDATE()),
          ('Spain','34','Spain.jpg',GETDATE()),
		  ('Germany','49', 'Germany.jpg',GETDATE()),
		  ('Japan','81', 'Japan.jpg',GETDATE()),
          ('United Kingdom','44', 'UKingdom.jpg',GETDATE()),          
		  ('Australia','61', 'AUD.jpg',GETDATE()),
		  ('Canada','1', 'KYD.jpg',GETDATE()),
		  ('Switzerland','41', 'Switzerland.jpg',GETDATE()),
		  ('China','86', 'CNY.jpg',GETDATE()),
		  ('Sweden','46', 'Sweden.jpg',GETDATE()),
		  ('New Zealand','64', 'NZealand.jpg',GETDATE()),
		  ('Mexico','52', 'Mexico.jpg',GETDATE()),
		  ('Singapore','65', 'Singapore.jpg',GETDATE()),
		  ('Hong Kong ','852', 'HKD.jpg',GETDATE()),
		  ('Norway','47', 'Norway.jpg',GETDATE()),
		  ('Denmark','45', 'DKK.jpg',GETDATE()),
		  ('Belarus','375', 'Belarus.jpg',GETDATE())
 GO