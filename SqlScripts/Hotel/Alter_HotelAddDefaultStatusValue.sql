  Alter table Hotel
  Drop column [Status]
  Go
  
  Alter table [Hotel]
  ADD [Status] bit not null DEFAULT 0 
  Go
  
  Alter table Restaurant
  Drop column [ActiveStatus]
  Go
  
  Alter table [Restaurant]
  ADD [ActiveStatus] bit not null DEFAULT 0 
  Go
