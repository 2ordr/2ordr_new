CREATE TABLE [dbo].[HouseKeepingFacilityDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HouseKeepingFacilityId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Price][float]NULL,
	[IsActive][bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint [DF_HouseKeepingFacDetails_CreationDate] DEFAULT (getUtcDate()),
 CONSTRAINT [PK_HouseKeepingFacDetails] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeepingFacDetails_HouseKeepingFacId] FOREIGN KEY([HouseKeepingFacilityId])
REFERENCES [dbo].[HouseKeepingFacilities] ([Id]))
GO