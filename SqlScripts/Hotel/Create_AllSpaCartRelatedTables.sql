CREATE TABLE [dbo].[SpaCart](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[Total] [float] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Spa_CART_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_Spa_CART] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_CART_CUST_ID] FOREIGN KEY([CustomerId])REFERENCES [Customer] ([Id]),
 CONSTRAINT [FK_Spa_Cart_HotelID] FOREIGN KEY([HotelId])REFERENCES [Hotel] ([Id]))
 Go
 
 CREATE TABLE [dbo].[SpaCartItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaCartId] [int] NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[ServeLevel] int NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Spa_Cart_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Cart_Items_SpaCart_ID] FOREIGN KEY([SpaCartId])REFERENCES [SpaCart] ([Id]),
 CONSTRAINT [FK_Spa_Cart_Items_SpaServiceDetailID] FOREIGN KEY([SpaServiceDetailId])REFERENCES [SpaServiceDetail] ([Id]))
 Go
 
  CREATE TABLE [dbo].[SpaCartItemsAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaCartItemsId] [int] NOT NULL,
	[SpaAdditionalElementId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Spa_Cart_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Cart_Items_Additional_SpaCartItems_ID] FOREIGN KEY([SpaCartItemsId])REFERENCES [SpaCartItems] ([Id]),
 CONSTRAINT [FK_Spa_Cart_Items_Additional_SpaAdditionalElementId] FOREIGN KEY([SpaAdditionalElementId])REFERENCES [SpaAdditionalElement] ([Id]))
 Go