ALTER TABLE [dbo].[SpaCartItems]
   DROP CONSTRAINT [FK_Spa_Cart_Items_SpaCart_ID] 
   GO
   
ALTER TABLE [dbo].[SpaCartItems]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Cart_Items_SpaCart_ID] FOREIGN KEY([SpaCartId])
	REFERENCES [dbo].[SpaCart] ([Id])
	ON DELETE CASCADE
	GO
	
ALTER TABLE [dbo].[SpaCartItemsAdditional]
   DROP CONSTRAINT [FK_Spa_Cart_Items_Additional_SpaCartItems_ID] 
   GO
   
ALTER TABLE [dbo].[SpaCartItemsAdditional]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Cart_Items_Additional_SpaCartItems_ID] FOREIGN KEY([SpaCartItemsId])
	REFERENCES [dbo].[SpaCartItems] ([Id])
	ON DELETE CASCADE
	GO