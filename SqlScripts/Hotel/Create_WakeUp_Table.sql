
CREATE TABLE [dbo].[WakeUp]
(
     [Id] [int] IDENTITY(1,1) NOT NULL,
     [CustomerId] [int] NOT NULL,
     [RoomId] [int] NOT NULL,
     [Method] [int] NOT NULL,
     [ScheduledDate] [datetime] NOT NULL,
     [IsRepeated] [bit] NOT NULL CONSTRAINT[DF_WakeUp_IsRepeated] DEFAULT 0,
     [Interval] [time](7) NULL,
     [Comment] [nvarchar](300) NULL,
     [IsCompleted] [bit] NOT NULL CONSTRAINT[DF_WakeUp_IsCompleted] DEFAULT 0,
     [CreationDate] [datetime] NOT NULL CONSTRAINT[DF_WakeUp_CreationDate] DEFAULT GETDATE(),
     
     CONSTRAINT[PK_WakeUp_Id] PRIMARY KEY([Id]),
     CONSTRAINT[FK_WakeUp_CustomerId] FOREIGN KEY([CustomerId]) REFERENCES [dbo].[Customer] ([Id]),
     CONSTRAINT[FK_WakeUp_RoomId] FOREIGN KEY([RoomId]) REFERENCES [dbo].[Room] ([Id])
)
GO