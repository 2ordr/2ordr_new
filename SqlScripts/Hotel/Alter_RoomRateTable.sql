/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Id]
      ,[RoomId]
      ,[RoomCapacityId]
      ,[RoomCategoryId]
      ,[Description]
      ,[Price]
      ,[CreationDate]
  FROM [2ordr].[dbo].[RoomRate]
  
  Alter table [RoomRate]
  Drop constraint [FK_RoomRate_ROOM_ID],[DF_RoomRate_CreationDate]
  Go
  
  Alter table [RoomRate]
  Drop column [Description],[CreationDate]
  Go
 
   Alter table [RoomRate]
   Add CONSTRAINT [FK_RoomRate_ROOM_ID] FOREIGN KEY([RoomId])
   REFERENCES [Room] ([Id]) ON DELETE CASCADE
   Go