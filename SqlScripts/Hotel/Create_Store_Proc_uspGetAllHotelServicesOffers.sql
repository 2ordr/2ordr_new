USE [2ordr]
GO
/****** Object:  StoredProcedure [dbo].[[uspGetAllHotelServicesOffers]]    Script Date: 04/11/2019 09:29:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 22/03/2019
-- Description:	Get customer all hotels/restaurants orders history
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllHotelServicesOffers] As
Begin
select OfferType,HotelId,HotelName,OfferId,OfferDetailsId,Description,Percentage,OfferImage,IsActive from  (
select distinct 'Spa' as OfferType,h.Id as HotelId, h.Name as HotelName, so.Id as OfferId,so.SpaServiceDetailId as OfferDetailsId, so.Description, so.Percentage,so.OfferImage,so.IsActive 
from SpaServiceOffers as so
inner join SpaServiceDetail as sd on sd.Id=so.SpaServiceDetailId
inner join SpaService as ss on ss.Id=sd.SpaServiceId
inner join HotelServices hs on hs.Id=ss.HotelServiceId
inner join Hotel h on h.Id=hs.HotelId
where so.IsActive=1
Union All

select distinct 'Excursion' as OfferType,h.Id as HotelId,h.Name as HotelName,exo.Id as OfferId,exo.ExcursionDetailId as OfferDetailsId, exo.Description, exo.Percentage,exo.OfferImage, exo.IsActive
from ExcursionOffers as exo
inner join ExcursionDetails as exd on exd.Id=exo.ExcursionDetailId
inner join HotelExcursion hex on hex.Id=exd.HotelExcursionId
inner join HotelServices hs on hs.Id=hex.HotelServiceId
inner join Hotel h on h.Id=hs.HotelId
where exo.IsActive=1
)a

End

-- EXEC [uspGetAllHotelServicesOffers] 