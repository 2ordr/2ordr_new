

CREATE TABLE [dbo].[ExtraTime]
  (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [SpaServiceDetailId] [int] NOT NULL,
  [Name] [nvarchar](300) NOT NULL,
  [Duration] [time] (7) NOT NULL,
  [Price] [float] NULL,
  [IsActive] [bit] NOT NULL DEFAULT(1),
  [CreationDate] [datetime] NOT NULL ,
 CONSTRAINT [PK_ExtraTime_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExtraTime_SpaServiceDetailId] FOREIGN KEY([SpaServiceDetailId])
   REFERENCES [dbo].[SpaServiceDetail]([Id])
 )
GO

CREATE TABLE [dbo].[ExtraProcedure]
  (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [SpaServiceDetailId] [int] NOT NULL,
  [Name] [nvarchar](300) NOT NULL,
  [Duration] [time](7) NOT NULL,
  [Price] [float] NULL,
  [IsActive] [bit] NOT NULL DEFAULT(1),
  [CreationDate] [datetime] NOT NULL ,
 CONSTRAINT [PK_ExtraProcedure _ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExtraProcedure _SpaServiceDetailId] FOREIGN KEY([SpaServiceDetailId])
   REFERENCES [dbo].[SpaServiceDetail]([Id])
 )
GO