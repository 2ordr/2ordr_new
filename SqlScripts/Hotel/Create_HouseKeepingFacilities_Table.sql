
CREATE TABLE [dbo].[HouseKeepingFacilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomDetailsId] [int] NOT NULL,
	[Name] nvarchar(200) NOT NULL,
	[Description] nvarchar(500) NOT NULL,
	[Price] [int] NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_HouseKeeping_Facilities_CreationDate]  DEFAULT (getutcdate())
 CONSTRAINT [PK_HouseKeeping_Facilities] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeeping_Facilities_RoomDetailsId] FOREIGN KEY([RoomDetailsId])
REFERENCES [dbo].[RoomDetails] ([Id]))
GO
