
CREATE TABLE [dbo].[TaxiDestination](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[FromPlace] nvarchar(500) NOT NULL,
	[ToPlace] nvarchar(500) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_TaxiDestination_CreationDate]  DEFAULT (getutcdate())
 CONSTRAINT [PK_TaxiDestination] PRIMARY KEY (Id),
 CONSTRAINT [FK_TaxiDestination_HotelId] FOREIGN KEY([HotelId])
REFERENCES [dbo].[Hotel] ([Id]))
GO
