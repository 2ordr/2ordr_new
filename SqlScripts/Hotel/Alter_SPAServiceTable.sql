--modify SPAService
Alter table SPAService
Drop column [From],[To]
Go

--SPAServiceDetail
Alter table SPAServiceDetail
Drop constraint FK_SPAServiceDetail_SPAGRP_Id
Go

Alter table SPAServiceDetail 
Drop column Image
Go

sp_RENAME 'SPAServiceDetail.SPAServiceDetailsGroupId', 'SPAServiceId' , 'COLUMN'
GO

sp_RENAME 'SPAServiceDetail.Rate', 'Price' , 'COLUMN'
GO
sp_RENAME 'SPAServiceDetail.Hours', 'Duration' , 'COLUMN'
GO

Alter table SPAServiceDetail 
ADD CONSTRAINT FK_SPAServiceDetails_SPAServiceId Foreign key(SPAServiceId) references SPAService(Id)
Go

--Drop unused tables
Drop table dbo.SPAServiceDetailsGroup
Go

Drop table dbo.SPAServiceGroup
Go


--Drop bookings tables
Drop table dbo.SPAServiceBookingDetail
Go

Drop table dbo.SPAServiceBooking
Go

--Drop spa tables 
Drop table dbo.HotelSPAServiceReview
Go
Drop table dbo.CustomerFavoriteSpaService
Go
Drop table dbo.CustomerSpaServicePreference
Go

Alter table dbo.SPAServiceDetail
Drop constraint FK_SPAServiceDetails_SPAServiceId
Go

Drop table dbo.SPAServiceDetail
Go

Drop table dbo.SPAService
Go


--Creation of new spa tables

CREATE TABLE [dbo].[SpaService](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelServiceId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Image] [nvarchar](400) NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_SpaSERVICE]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_SpaService_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaService_HotelServiceID] FOREIGN KEY([HotelServiceId])REFERENCES [dbo].[HotelServices] ([Id]))
GO

CREATE TABLE [dbo].[SpaServiceDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SPAServiceId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Price] [float] NOT NULL,
	[Duration] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT [DF_SpsService_Detail]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_SpaService_Detail_ID] PRIMARY KEY (Id),
 CONSTRAINT [FK_SpaServiceDetails_SpaServiceId] FOREIGN KEY([SpaServiceId])
REFERENCES [dbo].[SpaService] ([Id]))

GO

CREATE TABLE [dbo].[SpaServiceReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_HOTEL_Spa_SERVICE_REV DEFAULT GETUTCDate(),
CONSTRAINT [PK_HOTEL_SPA_SERVICE_REV_ID] Primary Key (Id),
CONSTRAINT [FK_HOTEL_SPA_SERVICE_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_HOTEL_SPA_SERVICE_REV_SPASERVICE_DTLID] Foreign Key(SpaServiceDetailId)
	REFERENCES [SpaServiceDetail](Id)
)
GO

Create table CustomerSpaServicePreference(
	Id int not null Identity(1,1),
	SpaServiceDetailId int not null,
	CustomerId int not null,
	constraint Pk_CUST_SPASERVICE_PREF PRIMARY Key (Id),
	Constraint FK_SPA_SERV_DETAIL_ID FOREIGN Key (SpaServiceDetailId)
		REFERENCES SpaServiceDetail(Id),
	Constraint FK_CUST_SPA_SERVICE_PREF_CUSTID FOREIGN Key(CustomerId)
		REFERENCES Customer(Id)
)
Go

CREATE TABLE [dbo].[CustomerFavoriteSpaService]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
 CONSTRAINT [PK_CUST_FAV_SPASERVICE] PRIMARY KEY(Id), 
 CONSTRAINT [FK_CUSTFAV_SPASERVICE_CUSTID] FOREIGN KEY(CustomerId)
		REFERENCES Customer(Id),
 CONSTRAINT [FK_CUSTFAV_SPASERVICE_SPASERDTLID] FOREIGN KEY(SpaServiceDetailId)
		REFERENCES SpaServiceDetail(Id)
)
GO