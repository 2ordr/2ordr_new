CREATE TABLE [dbo].[LaundryReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[LaundryId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_LAUNDRY_REVIEW DEFAULT GETUTCDate(),
CONSTRAINT [PK_LAUNDRY_REVIEW_ID] Primary Key (Id),
CONSTRAINT [FK_LAUNDRY_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_LAUNDRY_REVIEW_LAUNDRY_ID] Foreign Key(LaundryId)
	REFERENCES [Laundry](Id)
)
GO