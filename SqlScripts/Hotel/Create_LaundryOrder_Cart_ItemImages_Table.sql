

CREATE TABLE [dbo].[LaundryCartItemImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryCartItemId] [int] NOT NULL,
	[Image] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Laundry_Cart_Items_Images_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Cart_Items_Images_LaundryCartItems_ID] FOREIGN KEY([LaundryCartItemId])REFERENCES [LaundryCartItems] ([Id]),
 )
 Go
 
 
 
 CREATE TABLE [dbo].[LaundryOrderItemImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryOrderItemId] [int] NOT NULL,
	[Image] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Laundry_Order_Items_Images_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_Laundry_Order_Items_Images_LaundryOrderItems_ID] FOREIGN KEY([LaundryOrderItemId])REFERENCES [LaundryOrderItems] ([Id]),
 )
 Go