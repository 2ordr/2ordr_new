

EXEC sp_rename 'EmployeeTimeSlot', 'SpaDetailEmployeeTimeSlot'
Go

ALTER TABLE [SpaDetailEmployeeTimeSlot]
Drop CONSTRAINT [FK_EmployeeTimeSlot_SpaEmployeeId]
GO

ALTER TABLE [SpaDetailEmployeeTimeSlot]
Drop column [SpaEmployeeId]  
GO

ALTER TABLE [SpaDetailEmployeeTimeSlot]
ADD  [SpaEmployeeDetailsId] INT NOT NULL DEFAULT 1
GO

ALTER TABLE [SpaDetailEmployeeTimeSlot]
ADD CONSTRAINT [FK_SpaDetail_EmployeeTimeSlot_SpaEmployeeDetailId] FOREIGN KEY([SpaEmployeeDetailsId])
REFERENCES [SpaEmployeeDetails] ([Id])
GO