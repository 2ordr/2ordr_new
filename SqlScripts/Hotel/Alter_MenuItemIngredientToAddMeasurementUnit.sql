
  Update [MenuItemIngredient] SET Qty=1
  Go
  
  Alter Table [MenuItemIngredient]
  Alter column [Qty] float NULL
  Go
  
  Alter Table [MenuItemIngredient]
  Add MeasurementUnitId int NOT NULL constraint [DF_Measurment_unit] default 1
  Go
  
  Alter Table [MenuItemIngredient]
  Drop constraint [DF_Measurment_unit]
  Go
  
  INSERT INTO [dbo].[MeasurementUnit]
           ([UnitName],[Description],[Symbols],[IsActive])
     VALUES
           ('Gram','Grams','g',1)
  GO

  Alter Table [MenuItemIngredient]
  Add Constraint [FK_MenuItemIngredient_MeasurementUnit] FOREIGN KEY(MeasurementUnitId) 
  REFERENCES MeasurementUnit(Id)
  Go
  
  
  