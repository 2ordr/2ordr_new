

ALTER TABLE [ChainDetailsTable] 
  DROP CONSTRAINT [FK_ChainDetailsTable_ChainID] 
GO   

ALTER TABLE [ChainDetailsTable] 
  ADD CONSTRAINT [FK_ChainDetailsTable_ChainID] 
  FOREIGN KEY ([ChainId]) 
  REFERENCES [ChainTable]([Id]) 
  ON DELETE CASCADE;