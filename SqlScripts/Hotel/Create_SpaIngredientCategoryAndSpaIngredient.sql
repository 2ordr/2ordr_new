
CREATE TABLE [dbo].[SpaIngredientCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Image][nvarchar](100)NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SpaIngredientCategories_ID] PRIMARY KEY(Id),
 )
GO

CREATE TABLE [dbo].[SpaIngredients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaIngredientCategoryId] [int]NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IngredientImage][nvarchar](100)NULL
 CONSTRAINT [PK_SpaIngredients_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaIngredients_SpaIngreCatId] FOREIGN KEY ([SpaIngredientCategoryId]) REFERENCES SpaIngredientCategories(Id)
 )
GO