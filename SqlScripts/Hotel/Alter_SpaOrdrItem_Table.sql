



DROP TABLE [dbo].[SpaOrderItemsEmployee] 
GO

TRUNCATE TABLE [dbo].[SpaCartItemsAdditional]
GO

DELETE FROM  [dbo].[SpaOrderItems]
GO

DELETE FROM [dbo].[SpaOrder]
GO


ALTER TABLE [dbo].[SpaOrderItems]
ADD [SpaEmployeeDetailsId] [int] NOT NULL 
    CONSTRAINT[FK_SpaOrderItems_SpaEmployeeDetailsId] FOREIGN KEY([SpaEmployeeDetailsId])
               REFERENCES [dbo].[SpaEmployeeDetails] ([Id]),
	[SpaRoomId] [int] NOT NULL 
	CONSTRAINT [FK_SpaOrderItems_SpaRoomId] FOREIGN KEY([SpaRoomId])
               REFERENCES [dbo].[SpaRoom] ([Id]),
    [ExtraTimeId] [int] NOT NULL 
    CONSTRAINT [FK_SpaOrderItems_ExtraTimeId] FOREIGN KEY([ExtraTimeId])
               REFERENCES [dbo].[ExtraTime] ([Id])
GO


