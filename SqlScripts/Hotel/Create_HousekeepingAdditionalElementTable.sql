

 Drop table [HouseKeepingAdditionals]
 Go
 
 
CREATE TABLE [dbo].[HousekeepingAdditionalElements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingAdditionalGroupId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Price][float] NULL,
	[IsActive][bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint [DF_HouseKeepingAdditionalElement_CreationDate] DEFAULT (getUtcDate()),
 CONSTRAINT [PK_HouseKeepingAdditionalElement] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeepingAdditionalElement_HouseKeepingAddGrpID] FOREIGN KEY([HousekeepingAdditionalGroupId])
REFERENCES [dbo].[HousekeepingAdditionalGroup] ([Id]))
GO