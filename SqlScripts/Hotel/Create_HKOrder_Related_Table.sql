
CREATE TABLE [dbo].[HousekeepingOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[CardId] [int] NOT NULL,
	[OrderStatus] [int] NULL,
	[PaymentStatus] [bit] NOT NULL DEFAULT(0),
	[PaymentId] [int] NULL,
	[OrderTotal] [float] NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_HkOrder] PRIMARY KEY(Id),
 CONSTRAINT [FK_HkOrder_CUST_Id] FOREIGN KEY([CustomerId])REFERENCES [dbo].[Customer] ([Id]),
 CONSTRAINT [FK_HkOrder_RoomId] FOREIGN KEY([RoomId])REFERENCES [dbo].[Room] ([Id]),
 CONSTRAINT [FK_HkOrder_CardId] FOREIGN KEY([CardId])REFERENCES [dbo].[CustomerCreditCard] ([Id]))
 Go
 
  CREATE TABLE [dbo].[HousekeepingOrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingOrderId] [int] NOT NULL,
	[HouseKeepingFacilityDetailsId] [int] NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
	[ScheduleDate] [datetime] NOT NULL,
	[HousekeepingOrderStatus] [bit] NOT NULL DEFAULT(0),
 CONSTRAINT [PK_Hk_Order_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Hk_Order_Items_HkOrder_ID] FOREIGN KEY([HousekeepingOrderId])REFERENCES [HousekeepingOrderItems] ([Id]),
 CONSTRAINT [FK_Hk_Order_Items_HKFacilityDetailsIdID] FOREIGN KEY([HouseKeepingFacilityDetailsId])REFERENCES [HouseKeepingFacilityDetails] ([Id]))
 Go
 
 CREATE TABLE [dbo].[HousekeepingOrderItemAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingOrdertemId] [int] NOT NULL,
	[HousekeepingAdditionalElementId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Hk_Order_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Hk_Order_Items_Additional_HkOrdertem_ID] FOREIGN KEY([HousekeepingOrdertemId])REFERENCES [HousekeepingOrderItems] ([Id]),
 CONSTRAINT [FK_Hk_Order_Items_Additional_HkAdditionalElementId] FOREIGN KEY([HousekeepingAdditionalElementId])REFERENCES [HousekeepingAdditionalElements] ([Id]))
 Go