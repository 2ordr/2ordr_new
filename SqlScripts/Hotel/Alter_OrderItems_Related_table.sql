


ALTER TABLE [dbo].[SpaOrder]
 DROP constraint [DF_SpaOrderIsActive]
GO

ALTER TABLE [dbo].[SpaOrder]
 DROP COLUMN [IsActive]
GO

ALTER TABLE [dbo].[LaundryOrder]
 DROP  constraint [DF_LaundryOrderIsActive] 
GO

ALTER TABLE [dbo].[LaundryOrder]
 DROP COLUMN [IsActive] 
GO

ALTER TABLE [dbo].[HousekeepingOrder]
 DROP  constraint [DF_HousekeepingOrderIsActive] 
GO

ALTER TABLE [dbo].[HousekeepingOrder]
 DROP COLUMN [IsActive]  
GO



ALTER TABLE [dbo].[SpaOrderItems]
 ADD [IsActive] [bit] NOT NULL constraint [DF_SpaOrderItemsIsActive] default 1
GO

ALTER TABLE [dbo].[LaundryOrderItems]
 ADD [IsActive] [bit] NOT NULL constraint [DF_LaundryOrderItemsIsActive] default 1
GO

ALTER TABLE [dbo].[HousekeepingOrderItems]
 ADD [IsActive] [bit] NOT NULL constraint [DF_HousekeepingOrderItemsIsActive] default 1
GO