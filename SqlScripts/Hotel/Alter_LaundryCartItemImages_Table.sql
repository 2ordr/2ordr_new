
ALTER TABLE [dbo].[LaundryCartItemImages] 
DROP  CONSTRAINT [FK_Laundry_Cart_Items_Images_LaundryCartItems_ID]
GO

ALTER TABLE [dbo].[LaundryCartItemImages] 
ADD  CONSTRAINT [FK_Laundry_Cart_Items_Images_LaundryCartItems_ID] FOREIGN KEY([LaundryCartItemId])
REFERENCES [dbo].[LaundryCartItems] ([Id])
ON DELETE CASCADE
GO

 