CREATE TABLE [dbo].[EmployeeTimeSlot](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaEmployeeId] [int] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[Description] nvarchar(500),
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_EmployeeTimeSlot_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_EmployeeTimeSlot] PRIMARY KEY (Id),
 CONSTRAINT [FK_EmployeeTimeSlot_SpaEmployeeId] FOREIGN KEY([SpaEmployeeId])
REFERENCES [dbo].[SpaEmployee] ([Id]))
GO