
DROP TABLE [dbo].[SpaCartExtraTime] 
GO

DROP TABLE [dbo].[SpaCartItemsEmployee] 
GO

TRUNCATE TABLE [dbo].[SpaCartItemsAdditional]
GO

DELETE FROM [dbo].[SpaCartItems]
GO

DELETE FROM  [dbo].[SpaCart]
GO


ALTER TABLE [dbo].[SpaCartItems]
ADD [SpaEmployeeDetailsId] [int] NOT NULL 
    CONSTRAINT[FK_SpaCartItems_SpaEmployeeDetailsId] FOREIGN KEY([SpaEmployeeDetailsId])
               REFERENCES [dbo].[SpaEmployeeDetails] ([Id]),
	[SpaRoomId] [int] NOT NULL 
	CONSTRAINT [FK_SpaCartItems_SpaRoomId] FOREIGN KEY([SpaRoomId])
               REFERENCES [dbo].[SpaRoom] ([Id]),
    [ExtraTimeId] [int] NOT NULL 
    CONSTRAINT [FK_SpaCartItems_ExtraTimeId] FOREIGN KEY([ExtraTimeId])
               REFERENCES [dbo].[ExtraTime] ([Id])
GO


