  Alter Table [SpaTips]
  ADD [CreationDate] [datetime] NOT NULL constraint [DF_SpaTips_CreationDate] default '2019-03-12 13:37:42'
  Go
  
  Alter Table [SpaTips]
  Drop constraint [DF_SpaTips_CreationDate]
  Go
  
  Alter Table [LaundryTips]
  ADD [CreationDate] [datetime] NOT NULL constraint [DF_LaundryTips_CreationDate] default '2019-03-12 14:37:42'
  Go
  
  Alter Table [LaundryTips]
  Drop constraint [DF_LaundryTips_CreationDate]
  Go
  
  Alter Table [HousekeepingTips]
  ADD [CreationDate] [datetime] NOT NULL constraint [DF_HousekeepingTips_CreationDate] default '2019-03-13 09:37:42'
  Go
  
  Alter Table [HousekeepingTips]
  Drop constraint [DF_HousekeepingTips_CreationDate]
  Go
  
  Alter Table [ExcursionTips]
  ADD [CreationDate] [datetime] NOT NULL constraint [DF_ExcursionTips_CreationDate] default '2019-03-12 09:37:42'
  Go
  
  Alter Table [ExcursionTips]
  Drop constraint [DF_ExcursionTips_CreationDate]
  Go