

ALTER TABLE [LaundryCartItems]
DROP CONSTRAINT [FK_Laundry_Cart_Items_LaundryCart_Id]
Go

ALTER TABLE [dbo].[LaundryCartItems]  
ADD  CONSTRAINT [FK_Laundry_Cart_Items_LaundryCart_Id] FOREIGN KEY([LaundryCartId])
REFERENCES [dbo].[LaundryCart] ([Id])
ON DELETE CASCADE
GO
   

ALTER TABLE [LaundryCartItemsAdditional]
DROP CONSTRAINT [FK_Laundry_Cart_Items_Additional_LaundryCartItem_Id]
Go   

ALTER TABLE [dbo].[LaundryCartItemsAdditional]  
ADD  CONSTRAINT [FK_Laundry_Cart_Items_Additional_LaundryCartItem_Id] FOREIGN KEY([LaundryCartItemId])
REFERENCES [dbo].[LaundryCartItems] ([Id])
ON DELETE CASCADE
GO