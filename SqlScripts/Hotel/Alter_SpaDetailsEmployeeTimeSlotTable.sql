

EXEC sp_rename 'SpaDetailEmployeeTimeSlot', 'SpaEmployeeTimeSlot'
GO

ALTER TABLE [dbo].[SpaEmployeeTimeSlot]
DROP CONSTRAINT [FK_SpaDetail_EmployeeTimeSlot_SpaEmployeeDetailId]
GO

ALTER TABLE [dbo].[SpaEmployeeTimeSlot]
DROP CONSTRAINT [DF__SpaDetail__SpaEm__0B7CAB7B] --CONSTRAINT AS PER LOCAL DATABASE
GO


ALTER TABLE [dbo].[SpaEmployeeTimeSlot]
DROP COLUMN [SpaEmployeeDetailsId]
GO

TRUNCATE TABLE [dbo].[SpaEmployeeTimeSlot]
GO


ALTER TABLE [dbo].[SpaEmployeeTimeSlot]
ADD [SpaEmployeeId] [int] NOT NULL 
GO

ALTER TABLE [dbo].[SpaEmployeeTimeSlot]
ADD CONSTRAINT [FK_SpaEmployee_TimeSlot_SpaEmployeeId] FOREIGN KEY([SpaEmployeeId])
REFERENCES [dbo].[SpaEmployee]([Id])
GO