

ALTER TABLE [LaundryOrderItems]
DROP CONSTRAINT [FK_Laundry_Order_Items_LaundryOrder_ID]
Go

ALTER TABLE [dbo].[LaundryOrderItems]  
ADD  CONSTRAINT [FK_Laundry_Order_Items_LaundryOrder_ID] FOREIGN KEY([LaundryOrderId])
REFERENCES [dbo].[LaundryOrder] ([Id])
ON DELETE CASCADE
GO
   

ALTER TABLE [LaundryOrderItemsAdditional]
DROP CONSTRAINT [FK_Laundry_Order_Items_Additional_LaundryOrderItems_ID]
Go   

ALTER TABLE [dbo].[LaundryOrderItemsAdditional]  
ADD  CONSTRAINT [FK_Laundry_Order_Items_Additional_LaundryOrderItems_ID] FOREIGN KEY([LaundryOrderItemsId])
REFERENCES [dbo].[LaundryOrderItems] ([Id])
ON DELETE CASCADE
GO

