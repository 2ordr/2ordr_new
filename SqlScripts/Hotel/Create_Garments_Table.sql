

CREATE TABLE [dbo].[Garments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Garments]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_Garments_ID] PRIMARY KEY(Id),
 )
GO