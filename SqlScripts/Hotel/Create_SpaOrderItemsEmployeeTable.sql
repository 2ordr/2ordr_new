  CREATE TABLE [dbo].[SpaOrderItemsEmployee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaOrderItemsId] [int] NOT NULL,
	[SpaEmployeeDetailsId] [int] NOT NULL,
 CONSTRAINT [PK_Spa_Order_Items_Employee] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Order_Items_Employee_SpaOrderItems_ID] FOREIGN KEY([SpaOrderItemsId])REFERENCES [SpaOrderItems] ([Id]),
 CONSTRAINT [FK_Spa_Order_Items_Employee_SpaEmployeeDetailsId] FOREIGN KEY([SpaEmployeeDetailsId])REFERENCES [SpaEmployeeDetails] ([Id]))
 Go