
  Alter Table [SpaAdditionalGroup]
  Add [HotelId] [int] NOT NULL constraint [DF_SpaAdditionalGroup_HotelId] default 1
  Go
  
  Alter Table [SpaAdditionalGroup]
  Drop constraint [DF_SpaAdditionalGroup_HotelId]
  Go
  
  Alter Table [SpaAdditionalGroup]
  Add constraint [FK_SpaAdditionalGroup_HotelId] FOREIGN KEY(HotelId)
  REFERENCES Hotel(Id)
  Go
  
  --Spa Employee
  Alter Table [SpaEmployee]
  Add [HotelId] [int] NOT NULL constraint [DF_SpaEmployee_HotelId] default 1
  Go
  
  Alter Table [SpaEmployee]
  Drop constraint [DF_SpaEmployee_HotelId]
  Go
  
  Alter Table [SpaEmployee]
  Add constraint [FK_SpaEmployee_HotelId] FOREIGN KEY(HotelId)
  REFERENCES Hotel(Id)
  Go
  