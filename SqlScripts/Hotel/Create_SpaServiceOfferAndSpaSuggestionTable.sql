CREATE TABLE [dbo].[SpaServiceOffers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Percentage] [float] NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1)
 CONSTRAINT [PK_SpaServiceOffers_ID] PRIMARY KEY([Id]),
 CONSTRAINT [FK_SpaServiceOffers_SpaServiceDetailId] FOREIGN KEY([SpaServiceDetailId])
   REFERENCES dbo.[SpaServiceDetail] ([Id])
 )
GO

CREATE TABLE [dbo].[SpaSuggestion](
	[Id] [int] IDENTITY(1,1) NOT NULL,	 
	[SpaServiceDetailId] [int] NOT NULL,
	[SpaServiceDetailSuggestionId] [int] NOT NULL,	
 CONSTRAINT [PK_SpaSuggestion_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaSuggestion_SpaServiceDetailId] FOREIGN KEY([SpaServiceDetailId])
   REFERENCES [dbo].[SpaServiceDetail] ([Id]),
 CONSTRAINT [FK_SpaSuggestion_SpaServiceDetailSuggestionId] FOREIGN KEY([SpaServiceDetailSuggestionId])
   REFERENCES [dbo].[SpaServiceDetail] ([Id])   
 )
GO