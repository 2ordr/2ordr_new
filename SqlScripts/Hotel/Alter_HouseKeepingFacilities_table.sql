
ALTER TABLE [dbo].[HouseKeepingFacilities]
ADD [HotelServiceId] [int] NOT NULL,
    CONSTRAINT [FK_HouseKeepingFacilities_HotelServiceID] FOREIGN KEY([HotelServiceId])
    REFERENCES [dbo].[HotelServices]([Id])
GO