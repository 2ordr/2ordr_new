
ALTER TABLE [dbo].[SpaCartItemsEmployee]
DROP CONSTRAINT [FK_Spa_Cart_Items_Employee_SpaCartItems_ID] 
GO
   
ALTER TABLE [dbo].[SpaCartItemsEmployee]  WITH CHECK ADD  CONSTRAINT [FK_Spa_Cart_Items_Employee_SpaCartItems_ID] FOREIGN KEY([SpaCartItemsId])
REFERENCES [dbo].[SpaCartItems] ([Id])
ON DELETE CASCADE
GO
