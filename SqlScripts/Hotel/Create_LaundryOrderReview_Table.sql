

CREATE TABLE [dbo].[LaundryOrderReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryOrderId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Laundry_Order_Review] PRIMARY KEY (Id),
 CONSTRAINT [FK_Laundry_Order_Review_LaundryOrderId] FOREIGN KEY([LaundryOrderId])
REFERENCES [dbo].[LaundryOrder] ([Id]))
GO