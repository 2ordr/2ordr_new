CREATE TABLE [dbo].[SpaOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[CardId] [int] NOT NULL,
	[OrderStatus] [int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[ScheduledDate] [datetime] NULL,
	[OrderTotal] [float] NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_SpaOrder_CreationDate]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_SpaOrder] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaOrder_CUST_Id] FOREIGN KEY([CustomerId])REFERENCES [dbo].[Customer] ([Id]),
 CONSTRAINT [FK_SpaOrder_HotelID] FOREIGN KEY([HotelId])REFERENCES [dbo].[Hotel] ([Id]),
 CONSTRAINT [FK_SpaOrder_CardId] FOREIGN KEY([CardId])REFERENCES [dbo].[CustomerCreditCard] ([Id]))
 Go
 
  CREATE TABLE [dbo].[SpaOrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaOrderId] [int] NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[ServeLevel] int NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Spa_Order_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Order_Items_SpaOrder_ID] FOREIGN KEY([SpaOrderId])REFERENCES [SpaOrder] ([Id]),
 CONSTRAINT [FK_Spa_Order_Items_SpaServiceDetailID] FOREIGN KEY([SpaServiceDetailId])REFERENCES [SpaServiceDetail] ([Id]))
 Go
 
 CREATE TABLE [dbo].[SpaOrderItemsAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaOrderItemsId] [int] NOT NULL,
	[SpaAdditionalElementId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Spa_Order_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Order_Items_Additional_SpaOrderItems_ID] FOREIGN KEY([SpaOrderItemsId])REFERENCES [SpaOrderItems] ([Id]),
 CONSTRAINT [FK_Spa_Order_Items_Additional_SpaAdditionalElementId] FOREIGN KEY([SpaAdditionalElementId])REFERENCES [SpaAdditionalElement] ([Id]))
 Go