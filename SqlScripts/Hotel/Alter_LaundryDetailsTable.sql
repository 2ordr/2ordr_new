
Delete from LaundryDetailAdditional
Go


Delete from LaundryDetail
Go

Alter Table [dbo].[LaundryDetail]
Drop Column [Name]
Go

Alter Table [dbo].[LaundryDetail]
Add [GarmentId] int NOT NULL 
Go

Alter Table [dbo].[LaundryDetail]
Add Constraint [FK_LaundryDetail_GarmentId] Foreign Key([GarmentId])
References [dbo].[Garments]([Id])
Go
