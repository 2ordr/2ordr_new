



ALTER TABLE [dbo].[SpaCartExtraProcedure]  
DROP  CONSTRAINT [FK_SpaCartExtraProcedure_SpaCartItemId]
GO


ALTER TABLE [dbo].[SpaCartExtraProcedure]  
ADD  CONSTRAINT [FK_SpaCartExtraProcedure_SpaCartItemId]FOREIGN KEY([SpaCartItemId])
REFERENCES [dbo].[SpaCartItems] ([Id])
ON DELETE CASCADE
GO