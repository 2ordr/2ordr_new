
CREATE TABLE [dbo].[SpaDetailAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaDetailId] [int] NOT NULL,
	[SpaAdditionalGroupId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_SPADETAILS_ADDITIONAL_DATE DEFAULT GETUTCDATE(),
	
	CONSTRAINT PK_SPADETAILS_ADDITIONAL_ID PRIMARY KEY CLUSTERED (Id),
	CONSTRAINT FK_SPADETAILS_ADDITIONAL_SPADETAIL_ID FOREIGN KEY (SpaDetailId)
		REFERENCES [dbo].[SpaServiceDetail](Id),
	CONSTRAINT FK_SPADETAILS_ADDITIONAL_groupId FOREIGN KEY (SpaAdditionalGroupId)
		REFERENCES [dbo].[SpaAdditionalGroup](Id),
	)
GO

ALTER TABLE [dbo].[CustomerCreditCard]
  DROP CONSTRAINT DF_Customer_Create_Cart_IsActive
GO

ALTER TABLE [dbo].[CustomerCreditCard]
  DROP CONSTRAINT DF_Customer_Create_Cart_IsPrimary
GO

ALTER TABLE [dbo].[CustomerCreditCard] ADD CONSTRAINT DF_Customer_Create_Card_IsPrimary DEFAULT ((0)) FOR [IsPrimary]
GO

ALTER TABLE [dbo].[CustomerCreditCard] ADD CONSTRAINT DF_Customer_Create_Card_IsActive DEFAULT ((1)) FOR [IsActive]
GO