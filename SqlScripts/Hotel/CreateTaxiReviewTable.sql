CREATE TABLE [dbo].[TaxiReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TaxiId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_TAXI_REVIEW DEFAULT GETUTCDate(),
CONSTRAINT [PK_TAXI_REVIEW_ID] Primary Key (Id),
CONSTRAINT [FK_TAXI_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_TAXI_REVIEW_TAXI_ID] Foreign Key(TaxiId)
	REFERENCES [TaxiDetails](Id)
)
GO