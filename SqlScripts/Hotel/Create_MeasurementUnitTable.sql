CREATE TABLE [dbo].[MeasurementUnit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnitName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Symbols][nvarchar](20)NULL,
	[IsActive] [bit] NOT NULL DEFAULT(1),
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_MeasurementUnit]  DEFAULT (GETUTCDATE()),
 CONSTRAINT [PK_MeasurementUnit_ID] PRIMARY KEY(Id),
 )
GO