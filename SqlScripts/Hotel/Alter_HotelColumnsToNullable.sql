
  Alter table [Hotel]
  Alter column [Description] nvarchar(3000)
  Go
  Alter table [Hotel]
  Alter column [Street] nvarchar(100)
  Go
    Alter table [Hotel]
  Alter column [City] nvarchar(100)
  Go
    Alter table [Hotel]
  Alter column [Region] nvarchar(100)
  Go
    Alter table [Hotel]
  Alter column [Country] nvarchar(50)
  Go
    Alter table [Hotel]
  Alter column [Pincode] nvarchar(100)
  Go
    Alter table [Hotel]
  Alter column [Telephone] nvarchar(15)
  Go
    Alter table [Hotel]
  Alter column [Email] nvarchar(100)
  Go
   Alter table [Hotel]
  Alter column [CostForTwo] int
  Go