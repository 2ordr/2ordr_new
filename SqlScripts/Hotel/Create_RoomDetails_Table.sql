

CREATE TABLE [dbo].[RoomDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(200) NOT NULL,
	[Description] nvarchar(500) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Room_Details] PRIMARY KEY (Id))
GO
