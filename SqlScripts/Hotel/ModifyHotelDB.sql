

ALTER TABLE [dbo].[SPAServiceBookingDetail]
DROP CONSTRAINT [FK_SPAService_BOOKING_DT_SPADT_Id];  
GO

DROP TABLE dbo.SPAServiceDetail
GO

CREATE TABLE [dbo].[SPAServiceDetailsGroup](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [SPAServiceGroupId] [int] NOT NULL,
    [Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
    [CreationDate] [datetime] NOT NULL CONSTRAINT DF_SPASERVICE_DETAILS_GROUP DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPASERVICE_DETAILS_GROUP_ID] Primary Key (Id),
CONSTRAINT [FK_SPAService_DETAILS_GROUP_GROUPID] Foreign Key([SPAServiceGroupId])
	REFERENCES SPAServiceGroup(Id)
)
GO


CREATE TABLE [dbo].[SPAServiceDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SPAServiceDetailsGroupId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Rate] [float] NOT NULL,
	[Hours] [float] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_SPAService_Detail DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPAService_Detail_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_SPAServiceDetail_SPAGRP_Id] Foreign Key ([SPAServiceDetailsGroupId])
	REFERENCES [SPAServiceDetailsGroup](Id)
)
GO


ALTER TABLE [dbo].[SPAServiceBookingDetail]  
ADD CONSTRAINT [FK_SPAService_BOOKING_DT_SPADT_Id] FOREIGN KEY(SPAServiceDetailId)
    REFERENCES [dbo].[SPAServiceDetail] ([Id])
GO