CREATE TABLE [dbo].[SpaServiceDetailImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[Image] [nvarchar](100)NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL
 CONSTRAINT [PK_SpaServiceDetail_SpaServiceDetailImages] PRIMARY KEY(Id),
 CONSTRAINT FK_SpaServiceDetailImages_SpaServiceDetailId Foreign key(SpaServiceDetailId) references SpaServiceDetail(Id))
 Go
 
 --Alter hotel services table 
 
  Alter table [HotelServices]
  ADD OpeningHours time(7),
  ClosingHours time(7)
  Go
  