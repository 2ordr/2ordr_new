
/****** Object:  View [dbo].[VW_ServicesReviews]    Script Date: 08/01/2018 11:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[VW_ServicesReviews] As
select hs.HotelId,hs.ServiceId,sv.ServiceName, td.HotelServiceId, TaxiId as ServiceDetailId,td.Name as ServiceDetailName,td.Image, Score,Comment,tr.CreationDate from dbo.TaxiReview as tr
inner join dbo.TaxiDetails as td on tr.TaxiId=td.Id
inner join dbo.HotelServices as hs on hs.Id=td.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id

Union All

select hs.HotelId,hs.ServiceId, sv.ServiceName, t.HotelServiceId, TripId as ServiceDetailId,t.Name as ServiceDetailName,t.Image, Score,Comment,tr.CreationDate from dbo.TripReview as tr
inner join dbo.Trip as t on tr.TripId=t.Id
inner join dbo.HotelServices as hs on hs.Id=t.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id

Union All

select hs.HotelId,hs.ServiceId,sv.ServiceName,l.HotelServiceId, LaundryId as ServiceDetailId,l.Name as ServiceDetailName,l.Image, Score,Comment,lr.CreationDate from dbo.LaundryReview as lr
inner join dbo.Laundry as l on lr.LaundryId=l.Id
inner join dbo.HotelServices as hs on hs.Id=l.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id

Union All

select hs.HotelId,hs.ServiceId,sv.ServiceName,s.HotelServiceId, SPAServiceDetailId as ServiceDetailId,sd.Name as ServiceDetailName,sd.Image, Score,Comment,sr.CreationDate from dbo.HotelSPAServiceReview as sr
inner join dbo.SPAServiceDetail as sd on sr.SPAServiceDetailId =sd.Id
inner join dbo.SPAServiceDetailsGroup as sdg on sd.SPAServiceDetailsGroupId=sdg.Id
inner join dbo.SPAServiceGroup as sg on sdg.SPAServiceGroupId=sg.Id
inner join dbo.SPAService as s on sg.SPAServiceId=s.Id
inner join dbo.HotelServices as hs on hs.Id=s.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id


GO


