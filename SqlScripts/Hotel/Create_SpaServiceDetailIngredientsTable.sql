CREATE TABLE [dbo].[SpaServiceDetailIngredients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaServiceDetailId] [int] NOT NULL,
	[SpaIngredientsId] [int] NOT NULL,
 CONSTRAINT [PK_SpaServiceDetailIngredients_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaServiceDetailIngredients_SpaSerDetId] FOREIGN KEY(SpaServiceDetailId) REFERENCES SpaServiceDetail(Id),
 CONSTRAINT [FK_SpaServiceDetailIngredients_SpaIngrId] FOREIGN KEY(SpaIngredientsId)REFERENCES SpaIngredients(Id)
 )
GO