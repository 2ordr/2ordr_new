
--CREATE TABLE [HouseKeepingfacilitiesAdditional]--

CREATE TABLE [dbo].[HouseKeepingfacilitiesAdditional]
				(
				[Id] [int] IDENTITY(1,1) NOT NULL,
				[HouseKeepingFacilityId] [int] NOT NULL,
				[HouseKeepingAdditionalGroupId] [int] NOT NULL,
				[IsActive] [bit] NOT NULL DEFAULT(0),
				[CreationDate] [datetime] NOT NULL ,
CONSTRAINT [PK_HKeepingfacAdditional_ID] PRIMARY KEY(Id),
CONSTRAINT [FK_HKeepingfacAdditional_HKeepingFacDetailsId] FOREIGN KEY([HousekeepingFacilityId])
   REFERENCES [dbo].[HouseKeepingFacilities]([Id]),	 
CONSTRAINT [FK_HKeepingfacAdditional_HKeepingAdditionalGroupId] FOREIGN KEY([HouseKeepingAdditionalGroupId])
   REFERENCES [dbo].HousekeepingAdditionalGroup([Id]) 
				)
GO


--MODIFY TABLE [Housekeeping]--

ALTER TABLE [dbo].[HouseKeepingFacilities]
DROP CONSTRAINT [FK_HouseKeepingFacilities_HotelServiceID],
     COLUMN [HotelServiceId]
GO


Alter Table [LaundryAdditionalGroup]
Drop constraint [FK_LaundryAdditionalGroup_LaundryId]
Go

EXEC sp_RENAME 'LaundryAdditionalGroup.LaundryId' , 'HotelId', 'COLUMN'
Go

 Truncate table dbo.LaundryCartItemsAdditional
 Go
 
 Truncate table dbo.LaundryOrderItemsAdditional
 Go
 
 Delete from  dbo.LaundryAdditionalElement
 Go
 
Delete from [LaundryAdditionalGroup]
Go

Alter Table [LaundryAdditionalGroup]
Add constraint [FK_LaundryAdditionalGroup_HotelId] FOREIGN KEY (HotelId) REFERENCES [Hotel] (Id)
Go

