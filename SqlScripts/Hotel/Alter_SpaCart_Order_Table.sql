
ALTER TABLE [dbo].[SpaCartItems]
ADD [OfferPrice] [float] 
GO

ALTER TABLE [dbo].[SpaOrderItems]
ADD [OfferPrice] [float],
    [DeliveryStatus] [bit] NOT NULL DEFAULT(0)
GO