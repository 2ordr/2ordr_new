

 
 CREATE TABLE [dbo].[LaundryAdditionalElement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LaundryAdditionalGroupId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Price] [int] NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_LaundryAdditionalElement_Creation DEFAULT GETUTCDATE(),
	CONSTRAINT PK_Laundry_Additional_Element_ID Primary Key (Id),
	CONSTRAINT FK_Laundry_Additional_Element_Group_ID FOREIGN KEY (LaundryAdditionalGroupId)
		REFERENCES [dbo].[LaundryAdditionalGroup](Id))
GO