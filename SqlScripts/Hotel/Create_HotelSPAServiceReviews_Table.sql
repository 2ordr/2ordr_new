

CREATE TABLE [dbo].[HotelSPAServiceReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[SPAServiceDetailId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_HOTEL_SPA_SERVICE_REV DEFAULT GETUTCDate(),
CONSTRAINT [PK_HOTEL_SPA_SERVICE_REV_ID] Primary Key (Id),
CONSTRAINT [FK_HOTEL_SPA_SERVICE_REV_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_HOTEL_SPA_SERVICE_REV_SPASERVICE_DTLID] Foreign Key(SPAServiceDetailId)
	REFERENCES [SPAServiceDetail](Id)
)
GO
