ALTER TABLE [dbo].[Trip]
Drop CONSTRAINT [FK_Trip_ServiceId]
Go

ALTER TABLE [dbo].[Laundry]
Drop CONSTRAINT [FK_LAUNDRY_SERVICE_ID]
Go

ALTER TABLE [dbo].[SPAService]
Drop CONSTRAINT [FK_SPAService_SERVICEID]
Go

ALTER TABLE [dbo].[TaxiDetails] 
Drop Constraint [FK_TaxiDetails_ServiceId] 
Go

EXEC sp_rename 'Trip.ServiceId', 'HotelServiceId', 'COLUMN'
Go
EXEC sp_rename 'Laundry.ServiceId', 'HotelServiceId', 'COLUMN'
Go
EXEC sp_rename 'SPAService.ServiceId', 'HotelServiceId', 'COLUMN'
Go
EXEC sp_rename 'TaxiDetails.ServiceId', 'HotelServiceId', 'COLUMN'
Go



ALTER TABLE [dbo].[Trip]  WITH CHECK ADD  CONSTRAINT [FK_Trip_HotelServiceId] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id])
GO


ALTER TABLE [dbo].[Laundry]  WITH CHECK ADD  CONSTRAINT [FK_LAUNDRY_HOTELSERVICE_ID] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id])
GO

ALTER TABLE [dbo].[SPAService]  WITH CHECK ADD  CONSTRAINT [FK_SPAService_HotelSERVICEID] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id])
GO

ALTER TABLE [dbo].[TaxiDetails]  WITH CHECK ADD  CONSTRAINT [FK_TaxiDetails_Hotel_ServiceId] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id])
GO