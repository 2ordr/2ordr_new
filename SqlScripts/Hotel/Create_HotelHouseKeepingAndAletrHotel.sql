
  Alter Table [Hotel]
  Drop column [HouseKeepingTelephone]
  Go
  
  CREATE TABLE [dbo].[HotelHouseKeepingInfo](
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [HotelId] [int] NOT NULL,
  [Description] [nvarchar](500) NOT NULL,
  [HouseKeepingContact] [nvarchar](15)NOT NULL,
  [Image][nvarchar](100) NULL 
 CONSTRAINT [PK_HouseKeepingContact_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_HouseKeepingContact_HotelId] FOREIGN KEY([HotelId])
 REFERENCES [dbo].[Hotel]([Id])
 )
GO
