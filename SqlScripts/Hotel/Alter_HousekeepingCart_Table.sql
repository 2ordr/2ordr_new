

DROP TABLE [dbo].[HousekeepingCartItemAdditional]
GO

DROP TABLE [dbo].[HousekeepingCartItems]
GO

DROP TABLE [dbo].[HousekeepingCart]
GO


CREATE TABLE [dbo].[HousekeepingCart](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[Total] [float] NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Hkeeping_CART] PRIMARY KEY(Id),
 CONSTRAINT [FK_Hkeeping_CART_CUST_ID] FOREIGN KEY([CustomerId])REFERENCES [Customer] ([Id]),
 CONSTRAINT [FK_Hkeeping_Cart_RoomId] FOREIGN KEY([RoomId])	REFERENCES [dbo].[Room] ([Id])
 )
 Go
 
 CREATE TABLE [dbo].[HousekeepingCartItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingCartId] [int] NOT NULL,
	[HouseKeepingFacilityDetailsId] [int] NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
	[ScheduleDate] [datetime] NULL,
 CONSTRAINT [PK_Hkeeping_Cart_Items] PRIMARY KEY(Id),
 CONSTRAINT [FK_Hkeeping_Cart_Items_HousekeepingCart_ID] FOREIGN KEY([HousekeepingCartId])REFERENCES [HousekeepingCart] ([Id])
 ON DELETE CASCADE,
 CONSTRAINT [FK_Hkeeping_Cart_Items_HouseKeepingFacilityDetailsID] FOREIGN KEY([HouseKeepingFacilityDetailsId])REFERENCES [HouseKeepingFacilityDetails] ([Id]))
 Go
 
  CREATE TABLE [dbo].[HousekeepingCartItemAdditional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HousekeepingCartItemId] [int] NOT NULL,
	[HousekeepingAdditionalElementId] [int] NOT NULL,
	[Qty] int NOT NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK_Hkeeping_Cart_Items_Additonal] PRIMARY KEY(Id),
 CONSTRAINT [FK_Hkeeping_Cart_Items_Additional_HousekeepingCartItem_ID] FOREIGN KEY([HousekeepingCartItemId])REFERENCES [HousekeepingCartItems] ([Id])
 ON DELETE CASCADE,
 CONSTRAINT [FK_Hkeeping_Cart_Items_Additional_HousekeepingAdditionalElementId] FOREIGN KEY([HousekeepingAdditionalElementId])REFERENCES [HousekeepingAdditionalElements] ([Id]))
 Go