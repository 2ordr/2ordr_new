CREATE TABLE [dbo].[SpaOrderReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaOrderId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Spa_Order_Review] PRIMARY KEY (Id),
 CONSTRAINT [FK_Spa_Order_Rview_SpaOrderID] FOREIGN KEY([SpaOrderId])
REFERENCES [dbo].[SpaOrder] ([Id]))
GO