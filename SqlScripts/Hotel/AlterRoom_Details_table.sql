
 Alter Table [dbo].[Room]
  Drop constraint [DF__Room__RoomStatus__65570293]  --Delete constraint as per your DB
  Go


  Alter Table [dbo].[Room]
  Drop column [RoomStatus]
  Go
  
  Alter Table [dbo].[RoomDetails]
  Drop constraint [DF__RoomDetai__Statu__00FF1D08]  --Delete constraint as per your DB
  Go
  
  Alter Table [dbo].[RoomDetails]
  Drop column [Status]
  Go
  
  Alter Table [dbo].[SpaServiceDetail]
  Drop column [Duration] 
  Go
  
  Alter Table [dbo].[SpaServiceDetail]
  Add [Duration] time  NULL
  Go