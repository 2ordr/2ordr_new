


ALTER TABLE [dbo].[SpaCartItemsEmployee]
ADD [SpaRoomId] INT NOT NULL
GO


ALTER TABLE [dbo].[SpaCartItemsEmployee]
ADD CONSTRAINT [FK_SpaCartItems_Employee_SpaRoomId] FOREIGN KEY([SpaRoomId])
REFERENCES [SpaRoom] ([Id])
GO




ALTER TABLE [dbo].[SpaOrderItemsEmployee]
ADD [SpaRoomId] INT NOT NULL
GO

ALTER TABLE [dbo].[SpaOrderItemsEmployee]
ADD CONSTRAINT [FK_SpaOrderItems_Employee_SpaRoomId] FOREIGN KEY([SpaRoomId])
REFERENCES [SpaRoom] ([Id])
GO
