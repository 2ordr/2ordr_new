
sp_RENAME 'SpaServiceDetail.SPAServiceId', 'SpaServiceId' , 'COLUMN'
GO

Alter Table [dbo].[SpaServiceDetail]
Add CONSTRAINT [FK_SpaServiceDetails_SpaServiceId] FOREIGN KEY([SpaServiceId])
REFERENCES [dbo].[SpaService] ([Id])
GO 

 
 CREATE TABLE [dbo].[SpaAdditionalElement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaAdditionalGroupId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Price] [int] NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_SpaAdditionalElement_creation DEFAULT GETUTCDATE(),
	
	CONSTRAINT PK_Spa_Additional_Element_ID Primary Key (Id),
	CONSTRAINT FK_Spa_Additional_Element_Group_ID FOREIGN KEY (SpaAdditionalGroupId)
		REFERENCES [dbo].[SpaAdditionalGroup](Id)
	)
GO