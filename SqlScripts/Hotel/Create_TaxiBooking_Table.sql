
Drop Table [dbo].[TaxiBooking]
GO

CREATE TABLE [dbo].[TaxiBooking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TaxiDetailsId] [int] NOT NULL,
	[From] [nvarchar] (500) NOT NULL,
	[To] [nvarchar] (500) NOT NULL,
	[ScheduleDate] [datetime] NULL,
	[Comment] [nvarchar] (500) NULL,
	[OrderStatus] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_TaxiBooking_CreationDate]  DEFAULT (getutcdate())
 CONSTRAINT [PK_TaxiBooking] PRIMARY KEY (Id),
  CONSTRAINT [FK_TaxiBooking_CustomerId] FOREIGN KEY([CustomerId])
 REFERENCES [dbo].[Customer]([Id]),
 CONSTRAINT [FK_TaxiBooking_TaxiDtlId] FOREIGN KEY([TaxiDetailsId])
 REFERENCES [dbo].[TaxiDetails]([Id]))
GO


