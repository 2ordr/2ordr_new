  CREATE TABLE [dbo].[SpaCartItemsEmployee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpaCartItemsId] [int] NOT NULL,
	[SpaEmployeeDetailsId] [int] NOT NULL,
 CONSTRAINT [PK_Spa_Cart_Items_Employee] PRIMARY KEY(Id),
 CONSTRAINT [FK_Spa_Cart_Items_Employee_SpaCartItems_ID] FOREIGN KEY([SpaCartItemsId])REFERENCES [SpaCartItems] ([Id]),
 CONSTRAINT [FK_Spa_Cart_Items_Employee_SpaEmployeeDetailsId] FOREIGN KEY([SpaEmployeeDetailsId])REFERENCES [SpaEmployeeDetails] ([Id]))
 Go