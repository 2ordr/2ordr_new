  
  Alter table [CustomerHotelBooking]
  drop constraint FK_CUST_HOTEL_BOOKING_HOTELID
  Go
  
  Alter table [CustomerHotelBooking]
  Drop column [HotelId]
  Go
  
  Alter table CustomerHotelBooking
  Alter column [To] datetime not null
  Go
  
  Alter table CustomerHotelBooking
  Alter column [From] datetime not null
  Go