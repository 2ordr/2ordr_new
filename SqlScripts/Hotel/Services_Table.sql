Create table Services(
	Id int not null Identity(1,1),
	ServiceName nvarchar(200) not null,
	ServiceDescription nvarchar(800) null,
	constraint Pk_Services PRIMARY Key (Id)
)
Go

INSERT [dbo].[Services] ([ServiceName], [ServiceDescription]) VALUES ( N'SPA Service', N'SPA Service')
Go

Alter table dbo.HotelServices
drop column Name, Description
Go

Alter table dbo.HotelServices
Add ServiceId int not null default 1 Constraint FK_Service_ID FOREIGN Key (ServiceId)
		REFERENCES Services(Id),IsActive bit not null default 0
Go

Alter table dbo.HotelServices
Drop constraint DF__HotelServ__Servi__22FF2F51 --Please replace constraint name with your constraint name
Go

