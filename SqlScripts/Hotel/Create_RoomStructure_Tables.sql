
--------- Modify Room Table

ALTER TABLE [dbo].[Room]
  DROP CONSTRAINT [FK_ROOM_TYPEID]
  Go
  
ALTER TABLE [dbo].[Room]
  DROP COLUMN [RoomTypeId],[Rate],[Adults],[Childrens]
  Go
  
ALTER TABLE [dbo].[Room]
 ADD [QRCodeImage] [nvarchar](400),
	 [RoomStatus] [bit] NOT NULL DEFAULT 0
 GO
 
 
--------- Modify Room Details Table

Delete from [HouseKeepingFacilities]
GO

Delete from [RoomDetails]
GO
 
ALTER TABLE [RoomDetails]
ADD [RoomId] INT NOT NULL,
	[Status] [bit] NOT NULL DEFAULT 0,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RoomDetails_CreationDate]  DEFAULT (GETUTCDATE())
GO

ALTER TABLE [RoomDetails]
ADD CONSTRAINT [FK_ROOMDETAILS_ROOM_ID] FOREIGN KEY([RoomId])
REFERENCES [Room] ([Id])
GO
 
 --------- Create Amenities Table
 
 CREATE TABLE [dbo].[Amenities]
 (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar] (200) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Amenities_CreationDate]  DEFAULT (GETUTCDATE())
 CONSTRAINT [PK_Amenities] PRIMARY KEY (Id),
  )
GO

----------Create RoomAmenities Table

CREATE TABLE [dbo].[RoomAmenities]
 (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] INT NOT NULL,
	[AmenitiesId] INT NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RoomAmenities_CreationDate]  DEFAULT (GETUTCDATE())
 CONSTRAINT [PK_RoomAmenities] PRIMARY KEY (Id),
 CONSTRAINT [FK_RoomAmenities_ROOM_ID] FOREIGN KEY([RoomId])
 REFERENCES [Room] ([Id]),
 CONSTRAINT [FK_RoomAmenities_AMENITIES_ID] FOREIGN KEY([AmenitiesId])
 REFERENCES [Amenities] ([Id])
  )
GO

----------Create RoomCategory Table

CREATE TABLE [dbo].[RoomCategory]
 (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar] (200) NOT NULL,
	[Description] [nvarchar] (500) NULL,
	[Beds] INT DEFAULT 0,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RoomCategory_CreationDate]  DEFAULT (GETUTCDATE())
 CONSTRAINT [PK_RoomCategory] PRIMARY KEY (Id)
  )
GO

----------Create RoomCapacity Table

CREATE TABLE [dbo].[RoomCapacity]
 (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Adults] INT,
	[Childrens] INT,
	[Description] [nvarchar] (500) NULL,
	[IsActive] [bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RoomCapacity_CreationDate]  DEFAULT (GETUTCDATE())
 CONSTRAINT [PK_RoomCapacity] PRIMARY KEY (Id)
  )
GO

----------Create RoomRate Table

CREATE TABLE [dbo].[RoomRate]
 (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] INT NOT NULL,
	[RoomCapacityId] INT NOT NULL,
	[RoomCategoryId] INT NOT NULL,
	[Description] [nvarchar] (500) NULL,
	[Price] [float],
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RoomRate_CreationDate]  DEFAULT (GETUTCDATE())
 CONSTRAINT [PK_RoomRate] PRIMARY KEY (Id),
 CONSTRAINT [FK_RoomRate_ROOM_ID] FOREIGN KEY([RoomId])
    REFERENCES [Room] ([Id]),
 CONSTRAINT [FK_RoomRate_CAPACITY_ID] FOREIGN KEY([RoomCapacityId])
    REFERENCES [RoomCapacity] ([Id]),
 CONSTRAINT [FK_RoomRate_CATEGORY_ID] FOREIGN KEY([RoomCategoryId])
    REFERENCES [RoomCategory] ([Id])
  )
GO