 
 
 INSERT INTO [2ordr].[dbo].[RoomDetails](
		  [Name],
          [Description],
          [IsActive])
      VALUES('BedRoom','bedroom',1),
            ('LivingRoom','LivingRoom',1),
			('BathRoom','BathRoom',1)
	  GO
 
 INSERT INTO [dbo].[HouseKeepingFacilities](
		  [RoomDetailsId],
		  [Name],
		  [Description],
		  [Price])
      VALUES(1,'Clean All Room','Clean All Room',100),
			(2,'Clean some Places','Clean some places',50),
			(3,'Extra Towel','extra Towel',50),
			(3,'Soap','extra Towel',10)