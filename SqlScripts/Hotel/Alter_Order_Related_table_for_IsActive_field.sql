

ALTER TABLE [dbo].[SpaOrder]
 ADD [IsActive] [bit] NOT NULL constraint [DF_SpaOrderIsActive] default 1
GO

ALTER TABLE [dbo].[LaundryOrder]
 ADD [IsActive] [bit] NOT NULL constraint [DF_LaundryOrderIsActive] default 1
GO

ALTER TABLE [dbo].[HousekeepingOrder]
 ADD [IsActive] [bit] NOT NULL constraint [DF_HousekeepingOrderIsActive] default 1
GO