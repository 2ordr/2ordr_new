
Drop table [HouseKeepingFacilities]
Go

CREATE TABLE [dbo].[HouseKeepingFacilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelServiceId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[FullCleanPrice][float] NULL,
	[SomeCleanPrice][float] NULL,
	[IsActive][bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint [DF_HouseKeepingFacilities_CreationDate] DEFAULT (getUtcDate()),
 CONSTRAINT [PK_HouseKeepingFacilities] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeepingFacilities_HotelServiceID] FOREIGN KEY([HotelServiceId])
REFERENCES [dbo].[HotelServices] ([Id]))
GO

CREATE TABLE [dbo].[HouseKeepingAdditionals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HouseKeepingFacilityId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Price][float] NULL,
	[IsActive][bit] NOT NULL DEFAULT 1,
	[CreationDate] [datetime] NOT NULL constraint [DF_HouseKeepingAdditionals_CreationDate] DEFAULT (getUtcDate()),
 CONSTRAINT [PK_HouseKeepingAdditionals] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeepingAdditionals_HouseKeepingFacilityID] FOREIGN KEY([HouseKeepingFacilityId])
REFERENCES [dbo].[HouseKeepingFacilities] ([Id]))
GO

CREATE TABLE [dbo].[HouseKeeping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomDetailsId] [int] NOT NULL,
	[HouseKeepingFacilityId] [int] NOT NULL,
	[IsActive][bit] NOT NULL DEFAULT 1,
 CONSTRAINT [PK_HouseKeeping] PRIMARY KEY (Id),
 CONSTRAINT [FK_HouseKeeping_RoomDetailsId] FOREIGN KEY([RoomDetailsId])
 REFERENCES [RoomDetails] ([Id]),
 CONSTRAINT [FK_HouseKeeping_HouseKeepingFacilityID] FOREIGN KEY([HouseKeepingFacilityId])
REFERENCES [dbo].[HouseKeepingFacilities] ([Id]))
GO




  
  
  
  
  
  