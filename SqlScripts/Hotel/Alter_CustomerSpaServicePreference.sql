
  
  Delete from [CustomerSpaServicePreference]
  Go
  
  Alter Table [CustomerSpaServicePreference]
  Drop Constraint FK_SPA_SERV_DETAIL_ID
  Go
  
  Alter Table [CustomerSpaServicePreference]
  Drop column SpaServiceDetailId
  Go
  
  Alter Table [CustomerSpaServicePreference]
  Add SpaIngredientId int NOT NULL Constraint [FK_CustomerSpaServicePreference_SpaIngId] Foreign Key (SpaIngredientId) References SpaIngredients(Id),
  [Include] bit NULL
  Go