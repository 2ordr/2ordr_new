
ALTER TABLE [dbo].[HousekeepingOrderItems]
DROP CONSTRAINT [FK_Hk_Order_Items_HkOrder_ID]
GO

ALTER TABLE [dbo].[HousekeepingOrderItems]
ADD CONSTRAINT [FK_Hk_Order_Items_HkOrder_ID]FOREIGN KEY([HousekeepingOrderId])REFERENCES [HousekeepingOrder] ([Id])
GO
