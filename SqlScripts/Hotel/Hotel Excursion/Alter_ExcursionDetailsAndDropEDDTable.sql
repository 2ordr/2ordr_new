
Drop table dbo.ExcursionDetailsDuration
Go

Delete from dbo.ExcursionOffers
Go
Delete from dbo.ExcursionReviews
Go
Delete from ExcursionDetails
Go

Alter Table dbo.ExcursionDetails
Add [DurationInDays] int NOT NULL,
[DurationHours] time(7) NULL,
[WeekDay] int NOT NULL,
[StartTime] time(7) NOT NULL,
[Price] float NOT NULL 
Go