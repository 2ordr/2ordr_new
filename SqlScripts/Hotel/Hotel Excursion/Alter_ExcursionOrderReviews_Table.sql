

ALTER TABLE [dbo].[ExcursionOrderReview]
DROP CONSTRAINT [FK_Excursion_Order_Rview_SpaOrderID]
GO

ALTER TABLE [dbo].[ExcursionOrderReview]
ADD CONSTRAINT [FK_Excursion_Order_Review_ExcursionOrderId] FOREIGN KEY([ExcursionOrderId])REFERENCES [dbo].[ExcursionOrder] ([Id])
GO