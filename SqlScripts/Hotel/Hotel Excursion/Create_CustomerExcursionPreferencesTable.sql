Create table CustomerExcursionPreferences(
	Id int not null Identity(1,1),
	ExcursionIngredientId int not null,
	CustomerId int not null,
	[Include] bit NULL,
	constraint Pk_CustomerExcursionPreferences_Id PRIMARY Key (Id),
	Constraint FK_CustomerExcursionPreferences_ExcursionIngredientId FOREIGN Key (ExcursionIngredientId)
		REFERENCES ExcursionIngredients(Id),
	Constraint FK_CustomerExcursionPreferences_CUSTID FOREIGN Key(CustomerId)
		REFERENCES Customer(Id)
)
Go