
  
  Alter Table [Excursion]
  Drop column [Image]
  Go
  
  Alter Table [HotelExcursion]
  Add  IsActive bit NOT NULL default 1,
  CreationDate DateTime NOT NULL Constraint [DF_HotelExcursion_CreationDate] default getDate()
  Go
  
  