CREATE TABLE [dbo].[ExcursionCart](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[Total] [float] NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ExcursionCart_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionCart_CustomerId] FOREIGN KEY([CustomerId])REFERENCES [Customer] ([Id]),
  CONSTRAINT [FK_ExcursionCart_HotelId] FOREIGN KEY([HotelId])REFERENCES [Hotel] ([Id])
 )
 Go
 
 CREATE TABLE [dbo].[ExcursionCartItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionCartId] [int] NOT NULL,
	[ExcursionDetailsId] [int] NOT NULL,
	[ScheduleDate] [datetime] NOT NULL,
	[Total] [float] NOT NULL,
	[OfferPrice] [float] NULL,
	[Comment] nvarchar(500) NULL,
 CONSTRAINT [PK_ExcursionCartItems_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionCartItems_ExcursionCartID] FOREIGN KEY([ExcursionCartId])REFERENCES [ExcursionCart] ([Id])
 ON DELETE CASCADE,
 CONSTRAINT [FK_ExcursionCartItems_ExcursionDetailsID] FOREIGN KEY([ExcursionDetailsId])REFERENCES [ExcursionDetails] ([Id]))
 Go
 