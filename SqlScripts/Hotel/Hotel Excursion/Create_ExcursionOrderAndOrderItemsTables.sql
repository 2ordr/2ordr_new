CREATE TABLE [dbo].[ExcursionOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[CardId] [int] NOT NULL,
	[OrderStatus] [int] NULL,
	[PaymentStatus] [bit] NOT NULL CONSTRAINT [DF_ExcursionOrder_PaymentStatus] DEFAULT(0),
	[QuickPayPaymentId] [nvarchar](100) NULL,
	[OrderTotal] [float] NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ExcursionOrder_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionOrder_CustomerId] FOREIGN KEY([CustomerId])REFERENCES [dbo].[Customer] ([Id]),
 CONSTRAINT [FK_ExcursionOrder_HotelId] FOREIGN KEY([HotelId])REFERENCES [dbo].[Hotel] ([Id]),
 CONSTRAINT [FK_ExcursionOrder_CardId] FOREIGN KEY([CardId])REFERENCES [dbo].[CustomerCreditCard] ([Id]))
 Go
 
  CREATE TABLE [dbo].[ExcursionOrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionOrderId] [int] NOT NULL,
	[ExcursionDetailsId] [int] NOT NULL,
	[ScheduleDate] [datetime] NOT NULL,
	[Comment] nvarchar(500) NULL,
	[Total] [float] NULL,
	[OfferPrice] [float] NULL,
	[DeliveryStatus] [bit] NOT NULL CONSTRAINT [DF_ExcursionOrderItems_DeliveryStatus] DEFAULT(0),
	[IsActive][bit] NOT NULL CONSTRAINT [DF_ExcursionOrderItems_IsActive] DEFAULT(1),
 CONSTRAINT [PK_ExcursionOrderItems_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionOrderItems_ExcursionOrderId] FOREIGN KEY([ExcursionOrderId])REFERENCES [ExcursionOrder] ([Id]),
 CONSTRAINT [FK_ExcursionOrderItems_ExcursionDetailsId] FOREIGN KEY([ExcursionDetailsId])REFERENCES [ExcursionDetails] ([Id]))
 Go