CREATE TABLE [dbo].[ExcursionDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelExcursionId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_ExcursionDetail_IsActive] default 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ExcursionDetails_CreationDate DEFAULT getDate(),
CONSTRAINT [PK_ExcursionDetails_ID] Primary Key (Id),
CONSTRAINT [FK_ExcursionDetails_HotelExcursionId] Foreign Key (HotelExcursionId)
	REFERENCES HotelExcursion(Id)
)
GO

CREATE TABLE [dbo].[ExcursionReviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ExcursionDetailsId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] nvarchar(500),
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ExcursionReviews_REVIEW DEFAULT getDate(),
CONSTRAINT [PK_ExcursionReviews_ID] Primary Key (Id),
CONSTRAINT [FK_ExcursionReviews_CUSTID] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_ExcursionReviews_ExcursioDetID] Foreign Key(ExcursionDetailsId)
	REFERENCES [ExcursionDetails](Id)
)
GO

Alter Table dbo.CustomerHotelRoom
Add IsActive [bit] NOT NULL Constraint [DF_CustomerHotelRoom_IsActive] default 1
Go
