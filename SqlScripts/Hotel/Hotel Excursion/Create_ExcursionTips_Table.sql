 
CREATE TABLE [dbo].[ExcursionTips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionOrderId] [int] NOT NULL,
	[TipAmount] [float] NOT NULL,
	[CardId] [int] NULL,
	[PaymentStatus] [bit] NOT NULL,
	[Comment] [nvarchar](500) NULL,
 CONSTRAINT [PK_ExcursionTips_ID] PRIMARY KEY (Id), 
 CONSTRAINT [FK_ExcursionTips_ExcursionOrderId] FOREIGN KEY ([ExcursionOrderId]) REFERENCES [dbo].[ExcursionOrder] ([Id]),
 CONSTRAINT [FK_ExcursionTips_CardId] FOREIGN KEY([CardId])REFERENCES [dbo].[CustomerCreditCard] ([Id])
 )
GO


