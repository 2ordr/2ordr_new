

CREATE TABLE [dbo].[ExcursionOrderReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionOrderId] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Excursion_Order_Review] PRIMARY KEY (Id),
 CONSTRAINT [FK_Excursion_Order_Rview_SpaOrderID] FOREIGN KEY([ExcursionOrderId])
REFERENCES [dbo].[ExcursionOrder] ([Id]))
GO