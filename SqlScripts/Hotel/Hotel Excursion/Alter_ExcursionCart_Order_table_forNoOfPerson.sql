

ALTER TABLE [dbo].[ExcursionCartItems]
ADD [NoOfPerson] [int] NOT NULL CONSTRAINT [DF_ExcursionCartItems_NoOfPerson] DEFAULT(1)
GO

ALTER TABLE [dbo].[ExcursionOrderItems]
ADD [NoOfPerson] [int] NOT NULL CONSTRAINT [DF_ExcursionOrderItems_NoOfPerson] DEFAULT(1)
GO

ALTER TABLE [dbo].[ExcursionCartItems]
DROP CONSTRAINT [DF_ExcursionCartItems_NoOfPerson]
GO

ALTER TABLE [dbo].[ExcursionOrderItems]
DROP CONSTRAINT [DF_ExcursionOrderItems_NoOfPerson]
GO