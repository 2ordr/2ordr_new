
CREATE TABLE [dbo].[ExcursionSuggestion](
	[Id] [int] IDENTITY(1,1) NOT NULL,	 
	[ExcursionDetailId] [int] NOT NULL,
	[ExcursionDetailSuggestionId] [int] NOT NULL,	
 CONSTRAINT [PK_ExcursionSuggestion_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionSuggestion_ExcursionDetailId] FOREIGN KEY([ExcursionDetailId])
   REFERENCES [dbo].[ExcursionDetails] ([Id]),
 CONSTRAINT [FK_ExcursionSuggestion_ExcursionDetailSuggestionId] FOREIGN KEY([ExcursionDetailSuggestionId])
   REFERENCES [dbo].[ExcursionDetails] ([Id])   
 )
GO