CREATE TABLE [dbo].[ExcursionDetailImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionDetailId] [int] NOT NULL,
	[Image] [nvarchar](100) NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ExcursionDetailImages_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionDetailImages_ExcursionDetailId] FOREIGN KEY(ExcursionDetailId) REFERENCES ExcursionDetails(Id))
 Go
 
 CREATE TABLE [dbo].[ExcursionDetailGallery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionDetailId] [int] NOT NULL,
	[Image] [nvarchar](100) NOT NULL,
	[Video] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ExcursionDetailGallery_Id] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionDetailGallery_ExcursionDetailId] FOREIGN KEY(ExcursionDetailId) REFERENCES ExcursionDetails(Id))
 Go
 