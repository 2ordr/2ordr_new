CREATE TABLE [dbo].[ExcursionIngredientCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Image] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT DF_ExcursionIngredientCategories_IsActive default 1,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_ExcursionIngredientCategories_CreationDate DEFAULT getDate(),
 CONSTRAINT [PK_ExcursionIngredientCategories_ID] PRIMARY KEY (Id)
 )
GO

CREATE TABLE [dbo].[ExcursionIngredients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionIngredientCategoryId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IngredientImage] [nvarchar](100) NULL,
 CONSTRAINT [PK_ExcursionIngredients_ID] PRIMARY KEY (Id),
 CONSTRAINT [FK_ExcursionIngredients_ExcursionIngredientCategoryId] Foreign Key (ExcursionIngredientCategoryId)
	REFERENCES ExcursionIngredientCategories(Id)
 )
GO

CREATE TABLE [dbo].[ExcursionDetailIngredients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionDetailsId] [int] NOT NULL,
	[ExcursionIngredientsId] [int] NOT NULL,
CONSTRAINT [PK_ExcursionDetailIngredients_ID] Primary Key (Id),
CONSTRAINT [FK_ExcursionDetailIngredients_ExcursionDetailsId] Foreign Key (ExcursionDetailsId)
	REFERENCES ExcursionDetails(Id),
CONSTRAINT [FK_ExcursionDetailIngredients_ExcursionIngredientsId] Foreign Key(ExcursionIngredientsId)
	REFERENCES [ExcursionIngredients](Id)
)
GO