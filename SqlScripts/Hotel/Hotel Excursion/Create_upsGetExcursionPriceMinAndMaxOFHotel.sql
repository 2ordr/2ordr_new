SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 22/03/2019
-- Description:	Get customer all hotels/restaurants active orders 
-- =============================================
Create PROC [upsGetExcursionPricMinAndMaxOfHotel]
@hotelId int
As
Begin

  Select min(Price)as Excursion_Price_Min, MAX(Price) as Excursion_Price_Max from [ExcursionDetails] as ed
  inner join HotelExcursion as he on he.Id=ed.HotelExcursionId
  inner join HotelServices as hs on hs.Id=he.HotelServiceId
  Where hs.HotelId=@hotelId and hs.IsActive=1 and he.IsActive=1 and ed.IsActive=1
  
End

--EXEC [upsGetExcursionPricMinAndMaxOfHotel] 2