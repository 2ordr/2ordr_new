CREATE TABLE [dbo].[CustomerFavoriteExcursion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ExcursionDetailId] [int] NOT NULL,
CONSTRAINT [PK_CustomerFavoriteExcursion_ID] Primary Key (Id),
CONSTRAINT [FK_CustomerFavoriteExcursion_CustomerId] Foreign Key (CustomerId)
	REFERENCES Customer(Id),
CONSTRAINT [FK_CustomerFavoriteExcursion_ExcursionDetailId] Foreign Key(ExcursionDetailId)
	REFERENCES ExcursionDetails(Id)
)
GO