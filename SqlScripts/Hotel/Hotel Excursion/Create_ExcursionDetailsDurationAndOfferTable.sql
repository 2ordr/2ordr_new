
CREATE TABLE [dbo].[ExcursionDetailsDuration](
	[Id] [int] IDENTITY(1,1) NOT NULL,	 
	[ExcursionDetailId] [int] NOT NULL,
	[DurationInDays][float] NULL,
	[StartWeekDay][int] NOT NULL,
	[StartTime]	[time](7)NOT NULL,
	[EndTime][time](7)NOT NULL,
	[Price][float]NOT NULL,
	[IsActive][bit]NOT NULL Constraint [DF_ExcursionDetailsDuration_IsActive] default 1,
 CONSTRAINT [PK_ExcursionDetailsDuration_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_ExcursionDetailsDuration_ExcursionDetailId] FOREIGN KEY([ExcursionDetailId])
   REFERENCES [dbo].[ExcursionDetails] ([Id])
 )
GO

CREATE TABLE [dbo].[ExcursionOffers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExcursionDetailId] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Percentage] [float] NOT NULL,
	[OfferImage][nvarchar](200)NOT NULL,
	[IsActive] [bit] NOT NULL Constraint [DF_ExcursionOffers_IsActive] DEFAULT(1)
 CONSTRAINT [PK_ExcursionOffers_ID] PRIMARY KEY([Id]),
 CONSTRAINT [FK_ExcursionOffers_ExcursionDetailId] FOREIGN KEY([ExcursionDetailId])
   REFERENCES [dbo].[ExcursionDetails] ([Id])
 )
GO