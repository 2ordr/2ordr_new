
CREATE TABLE [dbo].[RestaurantTableMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantTableId] [int] NOT NULL,
	[RestaurantMenuId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL Constraint DF_REST_TABLE_MENU Default getUtcDate(),
Constraint PK_REST_TABLE_MENU Primary Key (Id),
Constraint FK_REST_TMENU_REST_TABLEID FOREIGN KEY(RestaurantTableId)
	REFERENCES [RestaurantTable](Id),
Constraint FK_REST_TMENU_REST_MENUID FOREIGN KEY(RestaurantMenuId)
	REFERENCES [RestaurantMenu](Id)
)
GO