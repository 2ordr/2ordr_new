

CREATE TABLE [dbo].[SpaCartExtraTime]
	(
	 [Id] [int] IDENTITY(1,1) NOT NULL,
	 [SpaCartItemId] [int] NOT NULL,
	 [ExtraTimeId] [int] NOT NULL,	
CONSTRAINT [PK_SpaCartExtraTime_ID] PRIMARY KEY(Id),
CONSTRAINT [FK_SpaCartExtraTime_SpaCartItemId] FOREIGN KEY([SpaCartItemId])
   REFERENCES [dbo].[SpaCartItems]([Id]),	 
CONSTRAINT [FK_SpaCartExtraTime_ExtraTimeId] FOREIGN KEY([ExtraTimeId])
   REFERENCES [dbo].[ExtraTime]([Id]), 
	)
GO


CREATE TABLE [dbo].[SpaCartExtraProcedure]
	(
	 [Id] [int] IDENTITY(1,1) NOT NULL,
	 [SpaCartItemId] [int] NOT NULL,
	 [ExtraprocedureId] [int] NOT NULL,	
CONSTRAINT [PK_SpaCartExtraProcedure_ID] PRIMARY KEY(Id),
CONSTRAINT [FK_SpaCartExtraProcedure_SpaCartItemId] FOREIGN KEY([SpaCartItemId])
   REFERENCES [dbo].[SpaCartItems]([Id]),	 
CONSTRAINT [FK_SpaCartExtraProcedure_ExtraProcedureId] FOREIGN KEY([ExtraprocedureId])
   REFERENCES [dbo].[ExtraProcedure]([Id]), 
	)
GO