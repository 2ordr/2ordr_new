Create table CustomerSpaServicePreference(
	Id int not null Identity(1,1),
	SPAServiceDetailId int not null,
	CustomerId int not null,
	constraint Pk_CUST_SPASERVICE_PREF PRIMARY Key (Id),
	Constraint FK_SPA_SERV_DETAIL_ID FOREIGN Key (SPAServiceDetailId)
		REFERENCES SPAServiceDetail(Id),
	Constraint FK_CUST_SPA_SERVICE_PREF_CUSTID FOREIGN Key(CustomerId)
		REFERENCES Customer(Id)
)
Go