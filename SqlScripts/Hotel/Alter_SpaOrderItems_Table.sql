
DELETE FROM [dbo].[SpaOrder]
GO 

ALTER TABLE [dbo].[SpaOrderItems]
ADD [StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL
GO	