
ALTER TABLE [dbo].[SPAServiceBookingDetail]
DROP CONSTRAINT [FK_SPAService_BOOKING_DT_LAUNDTID];  
GO 


DROP TABLE dbo.SPAServiceDetail
GO

CREATE TABLE [dbo].[SPAServiceGroup](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [SPAServiceId] [int] NOT NULL,
    [Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
    [CreationDate] [datetime] NOT NULL CONSTRAINT DF_SPASERVICE_GROUP DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPASERVICE_GROUP_ID] Primary Key (Id),
CONSTRAINT [FK_SPAService_GROUP_SERVICEID] Foreign Key(SPAServiceId)
	REFERENCES SPAService(Id)
)
GO


CREATE TABLE [dbo].[SPAServiceDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SPAServiceGroupId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](800) NULL,
	[Rate] [float] NOT NULL,
	[Hours] [float] NOT NULL,
	[Image] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Creation_Date] [datetime] NOT NULL CONSTRAINT DF_SPAService_Detail DEFAULT GETUTCDate(),
CONSTRAINT [PK_SPAService_Detail_ID] PRIMARY KEY (Id),
CONSTRAINT [FK_SPAServiceDetail_SPAGRP_Id] Foreign Key (SPAServiceGroupId)
	REFERENCES SPAServiceGroup(Id)
)
GO

ALTER TABLE [dbo].[SPAServiceBookingDetail]  
ADD CONSTRAINT [FK_SPAService_BOOKING_DT_SPADT_Id] FOREIGN KEY(SPAServiceDetailId)
    REFERENCES [dbo].[SPAServiceDetail] ([Id])
GO


CREATE TABLE [dbo].[Currency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Country] [nvarchar](500) NOT NULL,
	[CurrencyName] [nvarchar](500) NOT NULL,
	[Code] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](100) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT DF_CURRENCY_CRDate default GetUTCDate(),
 CONSTRAINT [PK_CURRENCY] PRIMARY KEY (Id) 
) 
GO
