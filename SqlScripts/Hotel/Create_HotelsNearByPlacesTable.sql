CREATE TABLE [dbo].[HotelsNearByPlaces](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Place] [nvarchar](500) NOT NULL,
	[Distance] [nvarchar](30) NULL,
	[Image] [nvarchar](100) NULL,
 CONSTRAINT [PK_HotelsNearByPlaces_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_HotelsNearByPlaces_HotelId] foreign key(HotelId)References Hotel(Id)
 )
GO