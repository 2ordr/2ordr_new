
Alter table [Restaurant]
  Drop column [IsPartnered]
Go

Alter table [Hotel]
  Drop column [IsPartnered]
Go