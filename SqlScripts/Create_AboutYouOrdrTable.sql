CREATE TABLE [dbo].[AboutYouOrdr]
  (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [LanguageId] [int] NOT NULL,
  [Description] [nvarchar](300) NOT NULL,
  [IsActive] [bit] NOT NULL DEFAULT(1),
  [CreationDate] [datetime] NOT NULL ,
 CONSTRAINT [PK_AboutYouOrdr_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_AboutYouOrdr_LanguageId] FOREIGN KEY([LanguageId])
 REFERENCES [dbo].[Language]([Id])
 )
GO