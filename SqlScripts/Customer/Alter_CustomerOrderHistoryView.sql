
/****** Object:  View [dbo].[VW_CustomerOrdersHistory]    Script Date: 12/20/2018 10:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj Parab
-- Create date: 20-12-2018
-- Description: Shows hotels/restaurants orders history
-- =============================================
ALTER view [dbo].[VW_CustomerOrdersHistory] As 
select distinct 'RO_'+cast(ro.Id as varchar) as Id, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber,
ro.IsActive,ro.OrderTotal,
CASE WHEN EXISTS (SELECT 1 FROM dbo.RestaurantTips rt WHERE rt.RestaurantOrderId = ro.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.OrderStatus=4


GO


