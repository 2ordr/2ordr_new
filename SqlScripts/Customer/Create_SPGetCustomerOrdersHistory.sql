/****** Object:  StoredProcedure [dbo].[GetCustomerAllPlans]    Script Date: 12/17/2018 10:12:15 ******/
--Customer restaurants/hotels orders history

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetCustomerOrdersHistory]
  @customerId int
As
Begin
	SET NOCOUNT ON
	select * from dbo.VW_CustomerOrdersHistory
	where CustomerId=@customerId order by StartDateTime
End

-- EXEC [GetCustomerOrdersHistory] 9