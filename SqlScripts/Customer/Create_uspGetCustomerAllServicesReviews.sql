
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 22/03/2019
-- Description:	Get customer all hotels services reviews
-- =============================================
CREATE PROCEDURE [uspGetCustomerAllServicesReviews]
@customerId int,
@hotelId INT
AS
BEGIN
select HotelId,ServiceId,ServiceName,HotelServiceId,ServiceDetailId,ServiceDetailName, Score,Comment,CreationDate  from(
select hs.HotelId,hs.ServiceId,sv.ServiceName,l.HotelServiceId, LaundryId as ServiceDetailId,l.Name as ServiceDetailName, Score,Comment,lr.CreationDate from dbo.LaundryReview as lr
inner join dbo.Laundry as l on lr.LaundryId=l.Id
inner join dbo.HotelServices as hs on hs.Id=l.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id
where lr.CustomerId=@customerId and hs.HotelId=@hotelId

Union All

select hs.HotelId,hs.ServiceId,sv.ServiceName,s.HotelServiceId, SPAServiceDetailId as ServiceDetailId,sd.Name as ServiceDetailName,Score,Comment,sr.CreationDate from dbo.SpaServiceReview as sr
inner join dbo.SpaServiceDetail as sd on sr.SpaServiceDetailId =sd.Id
inner join dbo.SpaService as s on sd.SpaServiceId=s.Id
inner join dbo.HotelServices as hs on hs.Id=s.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id
where sr.CustomerId=@customerId and hs.HotelId=@hotelId

Union All

select hs.HotelId,hs.ServiceId,sv.ServiceName,he.HotelServiceId, ExcursionDetailsId as ServiceDetailId,ed.Name as ServiceDetailName,Score,Comment,er.CreationDate from dbo.ExcursionReviews as er
inner join dbo.ExcursionDetails as ed on er.ExcursionDetailsId =ed.Id
inner join dbo.HotelExcursion as he on ed.HotelExcursionId=he.Id
inner join dbo.HotelServices as hs on hs.Id=he.HotelServiceId
inner join dbo.Services as sv on hs.ServiceId=sv.Id
where er.CustomerId=@customerId and hs.HotelId=@hotelId
)a order by CreationDate desc
END
GO

--EXEC [uspGetCustomerAllServicesReviews] 9,1
