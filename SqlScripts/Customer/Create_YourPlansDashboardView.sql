
--View to get all todays and later orders of customer 

/****** Object:  View [dbo].[VW_ServicesReviews]    Script Date: 12/06/2018 11:55:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[VW_YourPlansDashboard] As 
select distinct 'RO_'+cast(ro.Id as varchar) as Id, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.ScheduledDate>=GetDate()
Union All

select distinct 'SOI_'+cast(soi.Id as varchar) as Id,so.CustomerId,so.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, ssd.Name as Activity, 
soi.StartDateTime as StartDateTime, soi.EndDateTime as EndDateTime, ssd.Duration as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from SpaOrder as so
inner join SpaOrderItems as soi on so.Id=soi.SpaOrderId 
inner join SpaServiceDetail as ssd on ssd.Id=soi.SpaServiceDetailId
inner join Hotel as h on h.Id=so.HotelId
where soi.StartDateTime>=GetDate()

GO


