
CREATE TABLE [dbo].[CustomerLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[LogTelephone] [nvarchar](15) NOT NULL,	
	[LogEmail] [nvarchar](100) NOT NULL,
	[CreationDate] [datetime] NOT NULL constraint [DF_CustomerLog_CreationDate] DEFAULT (GETUTCDATE()),
CONSTRAINT [PK_CustomerLog] PRIMARY KEY (Id),
CONSTRAINT [FK_CustomerLog_CustomerId] FOREIGN KEY([CustomerId])
 REFERENCES [dbo].[Customer]([Id]))
GO