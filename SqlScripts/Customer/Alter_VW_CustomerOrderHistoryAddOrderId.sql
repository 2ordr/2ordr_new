USE [2ordr]
GO

/****** Object:  View [dbo].[VW_CustomerOrdersHistory]    Script Date: 01/15/2019 18:26:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--'RO_'+cast(ro.Id as varchar) as Id

ALTER view [dbo].[VW_CustomerOrdersHistory] As 
select distinct 'RO_' as OrderType,ro.Id as OrderId, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber,
ro.IsActive,ro.OrderTotal,c.Symbol as CurrencySymbol,
CASE WHEN EXISTS (SELECT 1 FROM dbo.RestaurantTips rt WHERE rt.RestaurantOrderId = ro.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join Currency as c on r.CurrencyId=c.Id
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.OrderStatus=4

GO


