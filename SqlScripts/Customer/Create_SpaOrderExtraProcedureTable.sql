CREATE TABLE [dbo].[SpaOrderExtraProcedure]
  (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [SpaOrderItemId] [int] NOT NULL,
  [ExtraProcedureId] [int] NOT NULL,
 CONSTRAINT [PK_SpaOrderExtraProcedure_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_SpaOrderExtraProcedure_SpaOrderItemId] FOREIGN KEY([SpaOrderItemId])
 REFERENCES [dbo].[SpaOrderItems]([Id]),
 CONSTRAINT[FK_SpaOrderExtraProcedure_ExtraProcedureId] FOREIGN KEY([ExtraProcedureId]) 
 REFERENCES dbo.ExtraProcedure(Id)
 )
GO