USE [2ordr]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCustomerPreferredMenuItems]    Script Date: 11/28/2018 16:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SP Customer Preferred Menu Item based on ingredient preferences
ALTER Proc [dbo].[uspGetCustomerPreferredMenuItems]
  @customerId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct rmi.* from CustomerIngredientPreference cp 
	join Ingredient  i 
	join MenuItemIngredient mi on mi.IngredientId = i.Id
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	on cp.IngredientId=i.Id
	where cp.CustomerId=@customerId  AND (cp.Include = 1 OR cp.Include is null) 
	AND rmi.Id NOT IN(
	select  distinct rmi.Id from CustomerAllergenPreference cp 
	join Allergen  a
	join AllergenIngredient ai on ai.AllergenID=a.Id 
	join MenuItemIngredient mi on mi.IngredientId = ai.IngredientID
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	on cp.AllergenId=a.Id
	where cp.CustomerId=@customerId
	)
End

--exec [uspGetCustomerPreferredMenuItems] 9