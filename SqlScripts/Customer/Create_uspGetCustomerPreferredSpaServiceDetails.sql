
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 2/1/2019
-- Description:	Get preferred Spa service details based on customer Spa ingredient preferences
-- =============================================
CREATE Proc [dbo].[uspGetCustomerPreferredSpaServiceDetails]
  @customerId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct ssd.* from CustomerSpaServicePreference csp 
	join SpaIngredients  si 
	join SpaServiceDetailIngredients ssdi on ssdi.SpaIngredientsId = si.Id
	join SpaServiceDetail ssd on ssd.Id=ssdi.SpaServiceDetailId 
	on csp.SpaIngredientId=si.Id
	where csp.CustomerId=@customerId  AND (csp.Include = 1 OR csp.Include is null) 
	AND ssd.IsActive=1
End

--Exec [uspGetCustomerPreferredSpaServiceDetails] 9
