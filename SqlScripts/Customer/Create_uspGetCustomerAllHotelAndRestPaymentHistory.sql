
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 17/04/2019
-- Description:	Get customer all hotels/restaurants payment history
-- =============================================
ALter PROCEDURE [dbo].[uspGetCustomerAllHotelAndRestPaymentHistory]
  @customerId int,
  @custPrefCurrencyId int,
  @year int = 0
As
Begin
If @year != 0
Begin
    SELECT year(CreationDate)as [Year],
    MONTH(CreationDate) as [Month],
    ROUND(SUM(OrderTotal*BaseRate),2) as [PreferredCurrencyTotalAmount] from (
	select ro.CreationDate,ro.OrderTotal,er.BaseRate from RestaurantOrder as ro  
	inner join Restaurant as r on r.Id=ro.RestaurantId
	inner join Currency as c on c.Id=r.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id
	where CustomerId=@customerId and year(ro.CreationDate)=@year and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
	
	select so.CreationDate,so.OrderTotal,er.BaseRate from SpaOrder as so 
	inner join Hotel as h on h.id=so.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(so.CreationDate)=@year and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
	
	select lo.CreationDate,lo.OrderTotal,er.BaseRate from LaundryOrder as lo  
	inner join Room as r on r.Id=lo.RoomId
	inner join Hotel as h on h.Id=r.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(lo.CreationDate)=@year and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
		
	select eo.CreationDate,eo.OrderTotal,er.BaseRate from ExcursionOrder as eo  
	inner join Hotel as h on h.id=eo.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(eo.CreationDate)=@year and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
		
	select ho.CreationDate,ho.OrderTotal,er.BaseRate from HousekeepingOrder as ho  
	inner join Room as r on r.Id=ho.RoomId
	inner join Hotel as h on h.Id=r.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(ho.CreationDate)=@year and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	)a GROUP BY year(CreationDate),MONTH(CreationDate)order by MONTH(CreationDate)
	
End
ELSE
Begin
    SELECT year(CreationDate)as [Year],
    MONTH(CreationDate) as [Month],
    ROUND(SUM(OrderTotal*BaseRate),2) as [PreferredCurrencyTotalAmount] from (
	select ro.CreationDate,ro.OrderTotal,er.BaseRate from RestaurantOrder as ro  
	inner join Restaurant as r on r.Id=ro.RestaurantId
	inner join Currency as c on c.Id=r.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id
	where CustomerId=@customerId and year(ro.CreationDate)=year(getdate()) and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
 	UNION ALL
 	
	select so.CreationDate,so.OrderTotal,er.BaseRate from SpaOrder as so  
	inner join Hotel as h on h.id=so.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(so.CreationDate)=year(getdate()) and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
	
	select lo.CreationDate,lo.OrderTotal,er.BaseRate from LaundryOrder as lo  
	inner join Room as r on r.Id=lo.RoomId
	inner join Hotel as h on h.Id=r.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(lo.CreationDate)=year(getdate()) and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
	
	select eo.CreationDate,eo.OrderTotal,er.BaseRate from ExcursionOrder as eo  
	inner join Hotel as h on h.id=eo.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(eo.CreationDate)=year(getdate()) and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	
	UNION ALL
	
	select ho.CreationDate,ho.OrderTotal,er.BaseRate from HousekeepingOrder as ho  
	inner join Room as r on r.Id=ho.RoomId
	inner join Hotel as h on h.Id=r.HotelId
	inner join Currency as c on c.Id=h.CurrencyId
	inner join ExchangeRate as er on er.FromCurrency=c.Id 
	where CustomerId=@customerId and year(ho.CreationDate)=year(getdate()) and PaymentStatus=1 and er.ToCurrency=@custPrefCurrencyId
	)a GROUP BY year(CreationDate),MONTH(CreationDate) order by MONTH(CreationDate)
End
	

End

-- EXEC [uspGetCustomerAllHotelAndRestPaymentHistory] 9,2,2018 
