CREATE TABLE [dbo].[CustomerHotelRoom]
  (
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [RoomId] [int] NOT NULL,
  [CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerHotelRoom_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_CustomerHotelRoom_RoomId] FOREIGN KEY([RoomId])
 REFERENCES [dbo].[Room]([Id]),
 CONSTRAINT[FK_CustomerHotelRoom_CustomerId] FOREIGN KEY([CustomerId]) 
 REFERENCES dbo.Customer(Id)
 )
GO