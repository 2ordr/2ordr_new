
ALTER TABLE [Customer]
DROP COLUMN [Country],[PhoneCountryCode];



ALTER TABLE [Customer]
ADD CountryId INT NULL;



ALTER TABLE [Customer]
ADD CONSTRAINT [FK_CUST_COUNTRY_ID] FOREIGN KEY([CountryId])
REFERENCES [Country] ([Id])
GO