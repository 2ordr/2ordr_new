
  Alter table [CustomerCreditCard]
  Drop column [CardNumber],[CardHolderName],[CardExpiryDate],[CVVNumber],[IsPrimary],[CardLabel],[CardLabelColor],[SpendingLimitEnabled],[SpendingLimitAmount],[Currency],[Duration]
  Go
  
  Alter table [CustomerCreditCard]
  ADD QuickPayCardId int 
  Go
  
  