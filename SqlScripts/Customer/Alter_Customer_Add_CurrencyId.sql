
DROP TABLE [dbo].[CustomerPreferedCurrency] 
GO

ALTER TABLE [dbo].[Customer]
  ADD [CurrencyId] [int] NOT NULL CONSTRAINT[DF_CURRENCYID] Default(1)
GO

ALTER TABLE [dbo].[Customer]
  DROP CONSTRAINT[DF_CURRENCYID]
GO

ALTER TABLE [dbo].[Customer] 
ADD CONSTRAINT [FK_CUST_CURRENCY_ID] Foreign key([CurrencyId])
REFERENCES [dbo].[Currency](Id) 
GO
