USE [2ordr]
GO

/****** Object:  View [dbo].[VW_CustomerOrdersHistory]    Script Date: 02/15/2019 11:13:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--'RO_'+cast(ro.Id as varchar) as Id

ALTER view [dbo].[VW_CustomerOrdersHistory] As 
select distinct 'Restaurant' as OrderType,ro.Id as OrderId, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber,
ro.IsActive,ro.OrderTotal,c.Symbol as CurrencySymbol,
CASE WHEN EXISTS (SELECT 1 FROM dbo.RestaurantTips rt WHERE rt.RestaurantOrderId = ro.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join Currency as c on r.CurrencyId=c.Id
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.OrderStatus=4 and
ro.IsActive=1
Union All

select distinct 'Spa' as OrderType,soi.Id as OrderId, so.CustomerId, so.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, ssd.Name as Activity, 
soi.StartDateTime as StartDateTime, soi.EndDateTime as EndDateTime, ssd.Duration as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,
soi.IsActive,so.OrderTotal,c.Symbol as CurrencySymbol,
CASE WHEN EXISTS (SELECT 1 FROM dbo.SpaTips st WHERE st.SpaOrderId = so.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from SpaOrder as so
inner join Hotel as h on h.Id=so.HotelId
inner join Currency as c on h.CurrencyId=c.Id
inner join SpaOrderItems as soi on so.Id=soi.SpaOrderId
inner join SpaServiceDetail as ssd on ssd.Id=soi.SpaServiceDetailId
where so.OrderStatus=2 and
soi.IsActive=1
Union All

select distinct 'Laundry' as OrderType,loi.Id as OrderId, lo.CustomerId, rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, gr.Name as Activity, 
loi.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,
loi.IsActive,lo.OrderTotal,c.Symbol as CurrencySymbol,
CASE WHEN EXISTS (SELECT 1 FROM dbo.LaundryTips lt WHERE lt.LaundryOrderId = lo.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from LaundryOrder as lo
inner join Room as rm on rm.Id=lo.RoomId
inner join Hotel as h on h.Id=rm.HotelId
inner join Currency as c on h.CurrencyId=c.Id
inner join LaundryOrderItems as loi on lo.Id=loi.LaundryOrderId
inner join LaundryDetail as ld on ld.Id=loi.LaundryDetailId
inner join Garments as gr on gr.Id=ld.GarmentId
where lo.OrderStatus=2 and
loi.IsActive=1
Union All

select distinct 'Housekeeping' as OrderType,hkoi.Id as OrderId, hko.CustomerId, rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, hkfd.Name as Activity, 
hkoi.ScheduleDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,
hkoi.IsActive,hko.OrderTotal,c.Symbol as CurrencySymbol,
CASE WHEN EXISTS (SELECT 1 FROM dbo.HousekeepingTips hkt WHERE hkt.HousekeepingOrderId = hko.Id )
     THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT)
END AS TipsPaid
from HousekeepingOrder as hko
inner join Room as rm on rm.Id=hko.RoomId
inner join Hotel as h on h.Id=rm.HotelId
inner join Currency as c on h.CurrencyId=c.Id
inner join HousekeepingOrderItems as hkoi on hko.Id=hkoi.HousekeepingOrderId
inner join HouseKeepingFacilityDetails as hkfd on hkfd.Id=hkoi.HouseKeepingFacilityDetailsId
where hko.OrderStatus=2 and
hkoi.IsActive=1


GO


