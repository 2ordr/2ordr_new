-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suraj
-- Create date: 7/2/2019
-- Description:	Get preferred excursion details based on customer excursion ingredient preferences
-- =============================================
CREATE Proc [dbo].[uspGetCustomerPreferredExcursionDetails]
  @customerId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct ed.* from CustomerExcursionPreferences cep 
	join ExcursionIngredients  ei 
	join ExcursionDetailIngredients edi on edi.ExcursionIngredientsId = ei.Id
	join ExcursionDetails ed on ed.Id=edi.ExcursionDetailsId 
	on cep.ExcursionIngredientId=ei.Id
	where cep.CustomerId=@customerId  AND (cep.Include = 1 OR cep.Include is null) 
	AND ed.IsActive=1
End

--Exec [uspGetCustomerPreferredExcursionDetails] 9
