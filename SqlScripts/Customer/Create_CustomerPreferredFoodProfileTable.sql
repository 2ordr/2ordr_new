
CREATE TABLE [dbo].[CustomerPreferredFoodProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[FoodProfileId][int] NOT NULL,
 CONSTRAINT [PK_CustomerPreferredFoodProfile_ID] PRIMARY KEY(Id),
 CONSTRAINT [FK_CustomerPreferredFoodProfile_CustId] FOREIGN KEY(CustomerId)REFERENCES Customer(Id),
 CONSTRAINT [FK_CustomerPreferredFoodProfile_FoodProfileId] FOREIGN KEY(FoodProfileId)REFERENCES FoodProfile(Id)
 )
GO