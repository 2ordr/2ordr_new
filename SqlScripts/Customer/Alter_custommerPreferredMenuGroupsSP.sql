
/****** Object:  StoredProcedure [dbo].[CustomerPreferredMenuGroups]    Script Date: 11/28/2018 14:12:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SP Customer Preferred Menu Groups
ALTER Proc [dbo].[CustomerPreferredMenuGroups]
  @customerId int,
  @restMenuId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct restMnGr.* from CustomerIngredientPreference cp 
	join Ingredient  i 
	join MenuItemIngredient mi on mi.IngredientId = i.Id
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	on cp.IngredientId=i.Id
	where cp.CustomerId=@customerId AND restMnGr.RestaurantMenuId=@restMenuId AND (cp.Include = 1 OR cp.Include is null) AND
	restMnGr.Id NOT IN(
	select  distinct restMnGr.Id from CustomerAllergenPreference cp 
	join Allergen  a
	join AllergenIngredient ai on ai.AllergenID=a.Id 
	join MenuItemIngredient mi on mi.IngredientId = ai.IngredientID
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	on cp.AllergenId=a.Id
	where cp.CustomerId=@customerId AND restMnGr.RestaurantMenuId=@restMenuId
	)
End
Go
--exec [dbo].[CustomerPreferredMenuGroups] 9,32