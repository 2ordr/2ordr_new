USE [2ordr]
GO

/****** Object:  View [dbo].[VW_YourPlansDashboard]    Script Date: 02/20/2019 15:08:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER view [dbo].[VW_YourPlansDashboard] As 
select row_number() over ( order by Id) as YourPlanId,OrderType,Id,CustomerId,OrgId,Name,Icon,Activity,StartDateTime,EndDateTime,Duration,DisheshCount,PhoneNumber from  (
select distinct 'Restaurant' as OrderType,'RO_'+cast(ro.Id as varchar) as Id, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.ScheduledDate>=GetDate()
Union All

select distinct 'Spa' as OrderType, 'SOI_'+cast(soi.Id as varchar) as Id,so.CustomerId,so.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, ssd.Name as Activity, 
soi.StartDateTime as StartDateTime, soi.EndDateTime as EndDateTime, ssd.Duration as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from SpaOrder as so
inner join SpaOrderItems as soi on so.Id=soi.SpaOrderId 
inner join SpaServiceDetail as ssd on ssd.Id=soi.SpaServiceDetailId
inner join Hotel as h on h.Id=so.HotelId
where soi.StartDateTime>=GetDate()
Union All

select distinct 'Laundry' as OrderType, 'LOI_'+cast(loi.Id as varchar) as Id,lo.CustomerId,rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, gr.Name as Activity, 
loi.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from LaundryOrder as lo
inner join LaundryOrderItems as loi on lo.Id=loi.LaundryOrderId 
inner join LaundryDetail as ld on ld.Id=loi.LaundryDetailId
inner join Garments as gr on gr.Id=ld.GarmentId
inner join Room as rm on rm.Id=lo.RoomId
inner join Hotel as h on h.Id=rm.HotelId
where loi.ScheduledDate>=GetDate()
Union All

select distinct 'Housekeeping' as OrderType, 'HOI_'+cast(hkoi.Id as varchar) as Id,hko.CustomerId,rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, hkfd.Name as Activity, 
hkoi.ScheduleDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from HousekeepingOrder as hko
inner join HousekeepingOrderItems as hkoi on hko.Id=hkoi.HousekeepingOrderId
inner join HouseKeepingFacilityDetails as hkfd on hkfd.Id=hkoi.HouseKeepingFacilityDetailsId
inner join Room as rm on rm.Id=hko.RoomId
inner join Hotel as h on h.Id=rm.HotelId
where hkoi.ScheduleDate>=GetDate()
Union All

select distinct 'WakeUp' as OrderType, 'WKO_'+cast(wkp.Id as varchar) as Id, wkp.CustomerId, rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, 'WakeUp' as Activity, 
wkp.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from WakeUp as wkp
inner join Room as rm on rm.Id=wkp.RoomId
inner join Hotel as h on h.Id=rm.HotelId 
where wkp.ScheduledDate>=GetDate()
Union All

select distinct 'Excursion' as OrderType, 'EXOI_'+cast(exoi.Id as varchar) as Id,exo.CustomerId,exo.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, exd.Name as Activity, 
exoi.ScheduleDate as StartDateTime,
   CASE WHEN(exd.DurationHours IS NOT NULL)
     THEN CAST(exoi.ScheduleDate+ exd.DurationInDays+ exd.DurationHours AS datetime)
     ELSE CAST(exoi.ScheduleDate+ exd.DurationInDays AS datetime)
END AS EndDateTime,
exd.DurationHours as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber
from ExcursionOrder as exo
inner join ExcursionOrderItems as exoi on exoi.ExcursionOrderId=exo.Id
inner join ExcursionDetails as exd on exd.Id=exoi.ExcursionDetailsId
inner join Hotel as h on h.Id=exo.HotelId
where exoi.ScheduleDate>=GetDate()
)a 

GO


