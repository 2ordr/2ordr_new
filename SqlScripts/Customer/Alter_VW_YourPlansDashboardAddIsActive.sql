USE [2ordr]
GO

/****** Object:  View [dbo].[VW_YourPlansDashboard]    Script Date: 03/20/2019 13:16:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--'RO_'+cast(ro.Id as varchar) as Id
ALTER view [dbo].[VW_YourPlansDashboard] As 
select row_number() over ( order by OrderId) as YourPlanId,OrderType,OrderId,OrderItemId,CustomerId,OrgId,Name,Icon,Activity,StartDateTime,EndDateTime,Duration,DisheshCount,PhoneNumber,IsActive from  (
select distinct 'Restaurant' as OrderType,ro.Id as OrderId,ro.Id as OrderItemId, ro.CustomerId, ro.RestaurantId as OrgId, r.Name as Name,r.LogoImage as Icon, 'Meal' as Activity, 
ro.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
COUNT(roi.RestaurantOrderId) OVER (PARTITION BY roi.RestaurantOrderId) as DisheshCount,
NULL as PhoneNumber,ro.IsActive
from RestaurantOrder as ro
inner join Restaurant as r on r.Id=ro.RestaurantId
inner join RestaurantOrderItem as roi on ro.Id=roi.RestaurantOrderId 
where ro.ScheduledDate>=GetDate() and ro.OrderStatus < 4 and ro.IsActive=1
Union All

select distinct 'Spa' as OrderType, so.Id as OrderId, soi.Id as OrderItemId, so.CustomerId,so.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, ssd.Name as Activity, 
soi.StartDateTime as StartDateTime, soi.EndDateTime as EndDateTime, ssd.Duration as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,soi.IsActive
from SpaOrder as so
inner join SpaOrderItems as soi on so.Id=soi.SpaOrderId 
inner join SpaServiceDetail as ssd on ssd.Id=soi.SpaServiceDetailId
inner join Hotel as h on h.Id=so.HotelId
where soi.StartDateTime>=GetDate() and so.OrderStatus < 2 and soi.IsActive=1
Union All

select distinct 'Laundry' as OrderType, lo.Id as OrderId, loi.Id as OrderItemId,lo.CustomerId,rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, l.Name +' '+ gr.Name as Activity, 
loi.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,loi.IsActive
from LaundryOrder as lo
inner join LaundryOrderItems as loi on lo.Id=loi.LaundryOrderId 
inner join LaundryDetail as ld on ld.Id=loi.LaundryDetailId
inner join Laundry as l on l.Id=ld.LaundryId
inner join Garments as gr on gr.Id=ld.GarmentId
inner join Room as rm on rm.Id=lo.RoomId
inner join Hotel as h on h.Id=rm.HotelId
where loi.ScheduledDate>=GetDate() and lo.OrderStatus < 2 and loi.IsActive=1
Union All

select distinct 'Housekeeping' as OrderType, hko.Id as OrderId, hkoi.Id as OrderItemId,hko.CustomerId,rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, hkfd.Name as Activity, 
hkoi.ScheduleDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,hkoi.IsActive
from HousekeepingOrder as hko
inner join HousekeepingOrderItems as hkoi on hko.Id=hkoi.HousekeepingOrderId
inner join HouseKeepingFacilityDetails as hkfd on hkfd.Id=hkoi.HouseKeepingFacilityDetailsId
inner join Room as rm on rm.Id=hko.RoomId
inner join Hotel as h on h.Id=rm.HotelId
where hkoi.ScheduleDate>=GetDate() and hko.OrderStatus < 2 and hkoi.IsActive=1
Union All

select distinct 'WakeUp' as OrderType, wkp.Id as OrderId, wkp.Id as OrderItemId, wkp.CustomerId, rm.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, 'WakeUp' as Activity, 
wkp.ScheduledDate as StartDateTime, NULL as EndDateTime, NULL as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,1 as IsActive 
from WakeUp as wkp
inner join Room as rm on rm.Id=wkp.RoomId
inner join Hotel as h on h.Id=rm.HotelId 
where wkp.ScheduledDate>=GetDate()
Union All

select distinct 'Excursion' as OrderType, exo.Id as OrderId, exoi.Id as OrderItemId,exo.CustomerId,exo.HotelId as OrgId, h.Name as Name,h.LogoImage as Icon, exd.Name as Activity, 
exoi.ScheduleDate as StartDateTime,
   CASE WHEN(exd.DurationHours IS NOT NULL)
     THEN CAST(exoi.ScheduleDate+ exd.DurationInDays+ exd.DurationHours AS datetime)
     ELSE CAST(exoi.ScheduleDate+ exd.DurationInDays AS datetime)
END AS EndDateTime,
exd.DurationHours as Duration,  
NULL as DisheshCount,
NULL as PhoneNumber,exoi.IsActive
from ExcursionOrder as exo
inner join ExcursionOrderItems as exoi on exoi.ExcursionOrderId=exo.Id
inner join ExcursionDetails as exd on exd.Id=exoi.ExcursionDetailsId
inner join Hotel as h on h.Id=exo.HotelId
where exoi.ScheduleDate>=GetDate() and exo.OrderStatus < 2 and exoi.IsActive=1
)a 




GO


