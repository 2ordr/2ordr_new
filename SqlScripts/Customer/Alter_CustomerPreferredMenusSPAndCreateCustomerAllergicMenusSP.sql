
/****** Object:  StoredProcedure [dbo].[CustomerPreferredMenus]    Script Date: 11/27/2018 15:49:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SP Customer Preferred Menus based on ingredient preferences
ALTER Proc [dbo].[CustomerPreferredMenus]
  @customerId int,
  @restaurantId int 
As
Begin
	
	SET NOCOUNT ON

	select  distinct restMenu.* from CustomerIngredientPreference cp 
	join Ingredient  i 
	join MenuItemIngredient mi on mi.IngredientId = i.Id
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	join RestaurantMenu restMenu on restMenu.Id=restMnGr.RestaurantMenuId
	on cp.IngredientId=i.Id
	where cp.CustomerId=@customerId AND restMenu.RestaurantId=@restaurantId AND (cp.Include = 1 OR cp.Include is null)
End
Go
--exec [CustomerPreferredMenus] 9,1


--SP Customer Allergic Menus based on allergen preferences
Create Proc [dbo].[CustomerAllergicMenus]
  @customerId int, 
  @restaurantId int
As
Begin
	
	SET NOCOUNT ON

	select  distinct restMenu.Id from CustomerAllergenPreference cp 
	join Allergen  a
	join AllergenIngredient ai on ai.AllergenID=a.Id 
	join MenuItemIngredient mi on mi.IngredientId = ai.IngredientID
	join RestaurantMenuItem rmi on rmi.Id=mi.RestaurantMenuItemId 
	join RestaurantMenuGroup restMnGr on restMnGr.Id=rmi.RestaurantMenuGroupId
	join RestaurantMenu restMenu on restMenu.Id=restMnGr.RestaurantMenuId
	on cp.AllergenId=a.Id
	where cp.CustomerId=@customerId AND restMenu.RestaurantId=@restaurantId
End
GO
--exec [CustomerAllergicMenus] 9,1

