﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ToOrdr.Core.Helpers
{

    public enum ImagePathFor
    {
        RestaurantCuisines,
        Cuisines,
        RestaurantBackground,
        RestaurantLogo,
        MenuItemBackground,
        ProfileImage,
        HotelBackground,
        HotelLogo,
        HotelHouseKeepingInfoImage,
        MenuImage,
        MenuAdditionalElementImage,
        AllergenImage,
        IngredientImage,
        SpaService,
        TripService,
        SpaServiceGroup,
        LaundryService,
        LaundryDetails,
        TaxiService,
        SPAServiceDetailsGroup,
        SpaServiceDetailImages,
        SpaServiceDetailOfferImage,
        SpaIngredientCategories,
        SpaIngredientImage,
        SpaAdditionalElementImage,
        SpaExtraProcedureImage,
        SpaExtraTimeImage,
        HotelRooms,
        ExcursionService,
        IngredientCategories,
        AllergenCategories,
        FoodProfiles,
        HotelNearByPlaces,
        Garments,
        ExcursionIngredientCategories,
        ExcursionIngredient,
        ExcursionOffer,
        ExcursionDetailImages,
        ExcursionDetailGallery,
        LaundryOrderItemImages,
        RoomDetailsImages
    }

    public class ImageHelper
    {
        public static string GetPath(ImagePathFor pathFor, string filename, int id, string api_path, int? menuId = 0)
        {
            switch (pathFor)
            {
                case ImagePathFor.RestaurantCuisines:
                    return string.Format(api_path + @"/Images/Restaurant/{0}/Cuisines/{1}/{2}", id, menuId, filename);
                case ImagePathFor.Cuisines:
                    return string.Format(api_path + @"Images/Cuisines/{0}/{1}", id, filename);
                case ImagePathFor.RestaurantBackground:
                    return string.Format(api_path + @"Images/Restaurant/{0}/Background/{1}", id, filename);
                case ImagePathFor.RestaurantLogo:
                    return string.Format(api_path + @"/Images/Restaurant/{0}/Logo/{1}", id, filename);
                case ImagePathFor.ProfileImage:
                    return string.Format(api_path + @"/Images/Customers/{0}/{1}", id, filename);
                case ImagePathFor.HotelBackground:
                    return string.Format(api_path + @"Images/Hotel/{0}/Background/{1}", id, filename);
                case ImagePathFor.HotelLogo:
                    return string.Format(api_path + @"/Images/Hotel/{0}/Logo/{1}", id, filename);
                case ImagePathFor.HotelHouseKeepingInfoImage:
                    return string.Format(api_path + @"/Images/Hotel/{0}/HouseKeepingInfoImage/{1}/{2}", id, menuId, filename);
                case ImagePathFor.HotelNearByPlaces:
                    return string.Format(api_path + @"/Images/Hotel/{0}/NearByPlaces/{1}/{2}", id, menuId, filename);
                case ImagePathFor.MenuImage:
                    return string.Format(api_path + @"/Images/Restaurant/{0}/Menu/{1}/{2}", id, menuId, filename);
                case ImagePathFor.MenuItemBackground:
                    return string.Format(api_path + @"/Images/MenuItem/{0}/{1}", id, filename);
                case ImagePathFor.MenuAdditionalElementImage:
                    return string.Format(api_path + @"/Images/MenuAdditionalGroups/{0}/MenuAdditionalElements/{1}/{2}", id, menuId, filename);
                case ImagePathFor.AllergenImage:
                    return string.Format(api_path + @"/Images/Preferences/Allergens/{0}/{1}", id, filename);
                case ImagePathFor.IngredientImage:
                    return string.Format(api_path + @"/Images/Preferences/Ingredients/{0}/{1}", id, filename);
                case ImagePathFor.SpaService:
                    return string.Format(api_path + @"/Images/Services/SpaService/{0}/{1}", id, filename);
                case ImagePathFor.TripService:
                    return string.Format(api_path + @"/Images/Services/TripService/{0}/{1}", id, filename);
                case ImagePathFor.SpaServiceGroup:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaServiceGroup/{0}/{1}", id, filename);
                case ImagePathFor.SPAServiceDetailsGroup:
                    return string.Format(api_path + @"/Images/Services/SpaService/SPAServiceDetailsGroup/{0}/{1}", id, filename);
                case ImagePathFor.LaundryService:
                    return string.Format(api_path + @"/Images/Services/LaundryService/{0}/{1}", id, filename);
                case ImagePathFor.LaundryDetails:
                    return string.Format(api_path + @"/Images/Services/LaundryService/{0}/LaundryDetails/{1}/{2}", id, menuId, filename);
                case ImagePathFor.SpaServiceDetailImages:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaServiceDetails/{0}/{1}", id, filename);
                case ImagePathFor.SpaServiceDetailOfferImage:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaServiceDetails/{0}/OfferImage/{1}/{2}", id, menuId, filename);
                case ImagePathFor.SpaIngredientCategories:
                    return string.Format(api_path + @"/Images/SpaIngredientCategories/{0}/{1}", id, filename);
                case ImagePathFor.SpaIngredientImage:
                    return string.Format(api_path + @"/Images/SpaIngredients/{0}/{1}", id, filename);
                case ImagePathFor.SpaAdditionalElementImage:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaAdditionalGroups/{0}/SpaAdditionalElements/{1}/{2}", id, menuId, filename);
                case ImagePathFor.SpaExtraProcedureImage:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaServiceDetails/{0}/ExtraProcedures/{1}/{2}", id,menuId, filename);
                case ImagePathFor.SpaExtraTimeImage:
                    return string.Format(api_path + @"/Images/Services/SpaService/SpaServiceDetails/{0}/ExtraTimes/{1}/{2}", id, menuId, filename);
                case ImagePathFor.TaxiService:
                    return string.Format(api_path + @"/Images/Services/TaxiService/{0}/{1}", id, filename);
                case ImagePathFor.HotelRooms:
                    return string.Format(api_path + @"/Images/Hotel/Rooms/{0}/{1}", id, filename);
                case ImagePathFor.ExcursionService:
                    return string.Format(api_path + @"/Images/Services/ExcursionService/{0}/{1}", id, filename);
                case ImagePathFor.IngredientCategories:
                    return string.Format(api_path + @"/Images/IngredientCategories/{0}/{1}", id, filename);
                case ImagePathFor.AllergenCategories:
                    return string.Format(api_path + @"/Images/AllergenCategories/{0}/{1}", id, filename);
                case ImagePathFor.FoodProfiles:
                    return string.Format(api_path + @"/Images/FoodProfiles/{0}/{1}", id, filename);
                case ImagePathFor.Garments:
                    return string.Format(api_path + @"Images/Garments/{0}/{1}", id, filename);
                case ImagePathFor.ExcursionIngredientCategories:
                    return string.Format(api_path + @"/Images/ExcursionIngredientCategories/{0}/{1}", id, filename);
                case ImagePathFor.ExcursionIngredient:
                    return string.Format(api_path + @"/Images/ExcursionIngredients/{0}/{1}", id, filename);
                case ImagePathFor.ExcursionOffer:
                    return string.Format(api_path + @"/Images/Services/Excursion/ExcursionDetails/{0}/OfferImage/{1}/{2}", id, menuId, filename);
                case ImagePathFor.ExcursionDetailImages:
                    return string.Format(api_path + @"/Images/Services/Excursion/ExcursionDetails/{0}/{1}", id, filename);
                case ImagePathFor.ExcursionDetailGallery:
                    return string.Format(api_path + @"/Images/Services/Excursion/ExcursionDetails/{0}/Gallery/{1}/{2}", id, menuId, filename);
                case ImagePathFor.RoomDetailsImages:
                    return string.Format(api_path + @"/Images/Hotel/Rooms/{0}/RoomDetails/{1}/{2}", id, menuId, filename);
                default:
                    return string.Format(@"~/Images/{0}/Logo/{1}", id, filename);

            }
        }

        public static void UploadImage(int Id, ImagePathFor pathFor, HttpPostedFileBase UploadFile, string api_path, int? menuId = 0)
        {
            using (HttpClient client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    byte[] fileBytes = new byte[UploadFile.InputStream.Length + 1]; UploadFile.InputStream.Read(fileBytes, 0, fileBytes.Length);
                    var fileContent = new ByteArrayContent(fileBytes);
                    fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = UploadFile.FileName.Replace(" ", "_") };
                    content.Add(fileContent);
                    string imgPath = "";
                    switch (pathFor)
                    {
                        case ImagePathFor.RestaurantCuisines:
                            imgPath = @"\Images\Restaurant\" + Id + @"\Cuisines\" + menuId;
                            break;
                        case ImagePathFor.Cuisines:
                            imgPath = @"\Images\Cuisines\" + Id;
                            break;
                        case ImagePathFor.RestaurantBackground:
                            imgPath = @"\Images\Restaurant\" + Id + @"\Background\";
                            break;

                        case ImagePathFor.RestaurantLogo:
                            imgPath = @"\Images\Restaurant\" + Id + @"\Logo\";
                            break;

                        case ImagePathFor.HotelBackground:
                            imgPath = @"\Images\Hotel\" + Id + @"\Background\";
                            break;

                        case ImagePathFor.HotelLogo:
                            imgPath = @"\Images\Hotel\" + Id + @"\Logo\";
                            break;

                        case ImagePathFor.HotelHouseKeepingInfoImage:
                            imgPath = @"\Images\Hotel\" + Id + @"\HouseKeepingInfoImage\" + menuId;
                            break;

                        case ImagePathFor.HotelNearByPlaces:
                            imgPath = @"\Images\Hotel\" + Id + @"\NearByPlaces\" + menuId;
                            break;

                        case ImagePathFor.MenuItemBackground:
                            imgPath = @"\Images\MenuItem\" + Id;
                            break;

                        case ImagePathFor.MenuImage:
                            imgPath = @"\Images\Restaurant\" + Id + @"\Menu\" + menuId;
                            break;
                        case ImagePathFor.AllergenImage:
                            imgPath = @"\Images\Preferences\Allergens\" + Id;
                            break;
                        case ImagePathFor.IngredientImage:
                            imgPath = @"\Images\Preferences\Ingredients\" + Id;
                            break;
                        case ImagePathFor.MenuAdditionalElementImage:
                            imgPath = @"\Images\MenuAdditionalGroups\" + Id + @"\MenuAdditionalElements\" + menuId;
                            break;
                        case ImagePathFor.SpaService:
                            imgPath = @"\Images\Services\SpaService\" + Id;
                            break;
                        case ImagePathFor.TripService:
                            imgPath = @"\Images\Services\TripService\" + Id;
                            break;
                        case ImagePathFor.SpaServiceGroup:
                            imgPath = @"\Images\Services\SpaService\SpaServiceGroup\" + Id;
                            break;
                        case ImagePathFor.LaundryService:
                            imgPath = @"\Images\Services\LaundryService\" + Id;
                            break;
                        case ImagePathFor.SPAServiceDetailsGroup:
                            imgPath = @"\Images\Services\SpaService\SPAServiceDetailsGroup\" + Id;
                            break;
                        case ImagePathFor.LaundryDetails:
                            imgPath = @"\Images\Services\LaundryService\" + Id + @"\LaundryDetails\" + menuId;
                            break;
                        case ImagePathFor.SpaServiceDetailImages:
                            imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + Id;
                            break;
                        case ImagePathFor.SpaServiceDetailOfferImage:
                            imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + Id + @"\OfferImage\" + menuId;
                            break;
                        case ImagePathFor.SpaIngredientCategories:
                            imgPath = @"\Images\SpaIngredientCategories\" + Id;
                            break;
                        case ImagePathFor.SpaIngredientImage:
                            imgPath = @"\Images\SpaIngredients\" + Id;
                            break;
                        case ImagePathFor.SpaAdditionalElementImage:
                            imgPath = @"\Images\Services\SpaService\SpaAdditionalGroups\" + Id + @"\SpaAdditionalElements\" + menuId;
                            break;
                        case ImagePathFor.SpaExtraProcedureImage:
                            imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + Id + @"\ExtraProcedures\" + menuId;
                            break;
                        case ImagePathFor.SpaExtraTimeImage:
                            imgPath = @"\Images\Services\SpaService\SpaServiceDetails\" + Id + @"\ExtraTimes\" + menuId;
                            break;
                        case ImagePathFor.TaxiService:
                            imgPath = @"\Images\Services\TaxiService\" + Id;
                            break;
                        case ImagePathFor.HotelRooms:
                            imgPath = @"\Images\Hotel\Rooms\" + Id;
                            break;
                        case ImagePathFor.ExcursionService:
                            imgPath = @"\Images\Services\ExcursionService\" + Id;
                            break;
                        case ImagePathFor.IngredientCategories:
                            imgPath = @"\Images\IngredientCategories\" + Id;
                            break;
                        case ImagePathFor.AllergenCategories:
                            imgPath = @"\Images\AllergenCategories\" + Id;
                            break;
                        case ImagePathFor.FoodProfiles:
                            imgPath = @"\Images\FoodProfiles\" + Id;
                            break;
                        case ImagePathFor.Garments:
                            imgPath = @"\Images\Garments\" + Id;
                            break;
                        case ImagePathFor.ExcursionIngredientCategories:
                            imgPath = @"\Images\ExcursionIngredientCategories\" + Id;
                            break;
                        case ImagePathFor.ExcursionIngredient:
                            imgPath = @"\Images\ExcursionIngredients\" + Id;
                            break;
                        case ImagePathFor.ExcursionOffer:
                            imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + Id + @"\OfferImage\" + menuId;
                            break;
                        case ImagePathFor.ExcursionDetailImages:
                            imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + Id;
                            break;
                        case ImagePathFor.ExcursionDetailGallery:
                            imgPath = @"\Images\Services\Excursion\ExcursionDetails\" + Id + @"\Gallery\" + menuId;
                            break;
                        case ImagePathFor.RoomDetailsImages:
                            imgPath = @"\Images\Hotel\Rooms\" + Id + @"\RoomDetails\" + menuId;
                            break;
                    }

                    var requestUri = api_path + "restaurants/UploadImage?imgPath=" + imgPath;
                    HttpResponseMessage response = client.PostAsync(requestUri, content).Result;
                }
            }
        }


        public static void DeleteImage(string api_path, string imgPath)
        {
            using (HttpClient client = new HttpClient())
            {
                var requestUri = api_path + "restaurants/DeleteImage?imgPath=" + imgPath;
                HttpResponseMessage response = client.DeleteAsync(requestUri).Result;
            }
        }


        public static string GetOrderItemImagePath(ImagePathFor pathFor, int customerId, int roomId, int laundryDtlId, string filename, string api_path)
        {
            switch (pathFor)
            {
                case ImagePathFor.LaundryOrderItemImages:
                    return string.Format(api_path + @"/Images/Customers/{0}/Room/{1}/LaundryOrder/{2}/{3}", customerId, roomId, laundryDtlId, filename);
                default:
                    return string.Format(@"~/Images/{0}/Logo/{1}", customerId, filename);
            }
        }
    }
}
