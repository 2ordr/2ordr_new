﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Helpers
{
    public class ValidStartDateTime : ValidationAttribute
    {
        protected override ValidationResult
                IsValid(object value, ValidationContext validationContext)
        {
            DateTime _startDateTime = Convert.ToDateTime(value);
            if (_startDateTime > DateTime.Now)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult
                    ("Start date time should be greater than current date time.");
            }
        }
    }

    public class ValidEndDateTime : ValidationAttribute
    {
        protected override ValidationResult
                IsValid(object value, ValidationContext validationContext)
        {
            DateTime _endDateTime = Convert.ToDateTime(value);
            if (_endDateTime > DateTime.Now)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult
                    ("End date time should be greater than current date time.");
            }
        }
    }

    public class ValidScheduledDateTime : ValidationAttribute
    {
        protected override ValidationResult
                IsValid(object value, ValidationContext validationContext)
        {
            DateTime _scheduledDateTime = Convert.ToDateTime(value);
            if (_scheduledDateTime > DateTime.Now)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult
                    ("Scheduled date time should be greater than current date time.");
            }
        }
    }
}
