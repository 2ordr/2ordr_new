﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
   // [MetadataType(typeof(CustomerCreditCardMetadata))]
    public partial class CustomerCreditCard : EntityBase
    {
        public Duration_Type Duration_Type_Details { get; set; }
    }
    [MetadataType(typeof(CustomerCreditCardMetadata))]
    public class CustomerPaymentCards
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }

        public System.DateTime CardExpiryDate { get; set; }
        public string CVVNumber { get; set; }

        public bool IsPrimary { get; set; }

        public string CardLabel { get; set; }

        public string CardLabelColor { get; set; }
        public Nullable<bool> SpendingLimitEnabled { get; set; }

        public int SpendingLimitAmount { get; set; }

        public string Currency { get; set; }

        public Nullable<int> Duration { get; set; }
        public string CardType { get; set; }
    }
    public class CustomerCreditCardMetadata
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        [Display(Name="Card Holder Name")]
        [Required(ErrorMessageResourceName = "Rqd_CardHolderName", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(ErrorMessageResourceName = "Max_CardHolderName", ErrorMessageResourceType = typeof(Resources))]
        public string CardHolderName { get; set; }

        [Display(Name = "Card Number")]
        [Required(ErrorMessageResourceName = "Rqd_CardNumber", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(ErrorMessageResourceName = "Max_CardNumber", ErrorMessageResourceType = typeof(Resources))]
        public string CardNumber { get; set; }

        [Display(Name = "Card Expiry Date")]
        [Required(ErrorMessageResourceName = "Rqd_CardExpiryDate", ErrorMessageResourceType = typeof(Resources))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/yyyy}")]
        public System.DateTime CardExpiryDate { get; set; }

        [Display(Name = "CVV Number")]
        [Required(ErrorMessageResourceName = "Rqd_CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(4, ErrorMessageResourceName = "Max_CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "CVVNumber", ErrorMessageResourceType = typeof(Resources))]
        public string CVVNumber { get; set; }

        [Display(Name = "Is Primary")]
        public Nullable<bool> IsPrimary { get; set; }

        [Display(Name = "Card Label")]
        public string CardLabel { get; set; }

        [Display(Name = "Card Label Color")]
        public string CardLabelColor { get; set; }

        [Display(Name = "Spending Limit Enabled")]
        public Nullable<bool> SpendingLimitEnabled { get; set; }

        [Display(Name = "Spending Limit Amount")]
        public int SpendingLimitAmount { get; set; }

        public string Currency { get; set; }

        public Nullable<int> Duration { get; set; }
        [Display(Name="Card Type")]
        public string CardType { get; set; }
    }
    public class CardExpiryResponse
    {
        public bool Status { get; set; }
        public string ResponseMessage { get; set; }
    }
    public enum Duration_Type
    {
        Days,
        Weeks,
        Months
    }


    public partial class PaypalDetail : EntityBase { }

    public partial class Currency : EntityBase { }
}
