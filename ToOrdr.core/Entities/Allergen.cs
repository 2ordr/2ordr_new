﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class AllergenMetadata
    {
        [Required]
        public string Name;

        [Display(Name = "Image")]
        public string AllergenImage { get; set; }
    }


    [MetadataType(typeof(AllergenMetadata))]
    public partial class Allergen : EntityBase
    {

    }
    public class AllergenCategoryMetadata
    {
        [Required]
        public string Name { get; set; }
    }
    [MetadataType(typeof(AllergenCategoryMetadata))]
    public partial class AllergenCategory : EntityBase
    {

    }

}
