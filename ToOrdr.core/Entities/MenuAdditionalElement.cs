﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(MenuAdditionalElementMetadata))]
    public partial class MenuAdditionalElement : EntityBase
    {
    }

    public class MenuAdditionalElementMetadata
    {
        [Required]
        [Display(Name="Element Name")]
        public string AdditionalElementName { get; set; }
        [Display(Name = "Element Description")]
        public string AdditionalElementDescription { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceName = "Invalid_AssitionalElementCost", ErrorMessageResourceType = typeof(Resources))]
        [Required]
        [Display(Name = "Element Cost")]
        public int AdditionalCost { get; set; }
    }
}
