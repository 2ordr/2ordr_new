﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class RestaurantTableMenuMetaData
    {
        [Required]
        [Display(Name = "Restaurant Menu")]
        public int RestaurantMenuId { get; set; }
    }

    [MetadataType(typeof(RestaurantTableMenuMetaData))]
    public partial class RestaurantTableMenu : EntityBase
    {

    }
}
