﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public partial class Customer_Restaurant_Preference : EntityBase
    {
        public IEnumerable<Customer_IngredientPreference> Ingredient_Preference { get; set; }

        public IEnumerable<Customer_AllergenPreference> Allergen_Preference { get; set; }

        public IEnumerable<CustomerLifeStylePreference> LifeStyle_Preference { get; set; }
    }


    public class Customer_IngredientPreference
    {
        public int Ingredient_Id { get; set; }
        public int? Parent_Id { get; set; }
        public string Ingredient_Name { get; set; }
        public string Ingredient_Desc { get; set; }
        public string Ingredient_Image { get; set; }
        public Nullable<bool> Include { get; set; }

        public bool IsGroup { get; set; }
    }

    public class Customer_AllergenPreference
    {
        public int? Id { get; set; }
        public int Allergen_ID { get; set; }
        public string Allergen_Name { get; set; }
        public string Allergen_Desc { get; set; }
        public string Allergen_Image { get; set; }
    }

    public class CustomerPreferences
    {
        public IEnumerable<Preferences> CustomerPreference { get; set; }
    }

    public class Preferences
    {
        public string Id { get; set; }
        public bool? ItemValue { get; set; }
    }

    public partial  class CustomerPreferredFoodProfile:EntityBase
    {

    }

}
