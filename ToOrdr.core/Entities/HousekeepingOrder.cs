﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum HousekeepingOrderStatus
    {
        Pending = 0,
        Confirmed = 1,
        Delivered = 2,
        Canceled = 3,
        Rejected = 4
    }
    public partial class HousekeepingOrder : EntityBase
    {
    }

    public partial class HousekeepingOrderItem : EntityBase
    {
    }

    public partial class HousekeepingOrderItemsAdditional : EntityBase
    {

    }

    public partial class HousekeepingTip : EntityBase
    {

    }
}
