﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class LaundryMetadata
    {
        [Required(ErrorMessage = "Please enter laundry service name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

    }
    [MetadataType(typeof(LaundryMetadata))]
    public partial class Laundry : EntityBase
    {
    }

    public class LaundryAdditionalGroupMetadata
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Display(Name = "Min Selected")]
        [Range(0, 1, ErrorMessageResourceName = "Valid_MinValue", ErrorMessageResourceType = typeof(Resources))]
        public int MinSelected { get; set; }
        [Display(Name = "Max Selected")]
        public int MaxSelected { get; set; }
    }

    [MetadataType(typeof(LaundryAdditionalGroupMetadata))]
    public partial class LaundryAdditionalGroup : EntityBase
    {

    }

    public class LaundryAdditionalElementsMetadata
    {
        [Required]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please Select Proper Price")]
        public Nullable<int> Price { get; set; }
    }

    [MetadataType(typeof(LaundryAdditionalElementsMetadata))]
    public partial class LaundryAdditionalElement : EntityBase
    {

    }

}
