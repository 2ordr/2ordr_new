﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class ExtraTimeMetadata
    {
        [Required(ErrorMessage = "Please enter valid name")]
        public string Name { get; set; }
        public double Price { get; set; }
        public System.TimeSpan Duration { get; set; }
    }

    [MetadataType(typeof(ExtraTimeMetadata))]
    public partial class ExtraTime : EntityBase
    {

    }

    public class ExtraProcedureMetadata
    {
        [Required(ErrorMessage = "Please enter valid name")]
        public string Name { get; set; }
        public double Price { get; set; }
        public System.TimeSpan Duration { get; set; }
    }

    [MetadataType(typeof(ExtraProcedureMetadata))]
    public partial class ExtraProcedure : EntityBase
    {

    }
}
