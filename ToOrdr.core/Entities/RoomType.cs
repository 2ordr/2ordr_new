﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(RoomTypeMetadata))]
    public partial class RoomType : EntityBase
    {
    }
    public class RoomTypeMetadata
    {
        [Required]
        public string Name { get; set; }
    }

}
