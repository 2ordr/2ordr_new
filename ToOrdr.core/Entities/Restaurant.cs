﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class RestaurantMetaData
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        [Display(Name = "Pin Code")]
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        [Display(Name = "Cost For Two")]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Valid_CostForTwoValue", ErrorMessageResourceType = typeof(Resources))]
        public int CostForTwo { get; set; }
        [Display(Name = "Background Image")]
        public string BackgroundImage { get; set; }
        [Display(Name = "Logo")]
        public string LogoImage { get; set; }
        [Required]
        [Display(Name = "Opening Hours")]
        public System.TimeSpan OpeningHours { get; set; }
        [Required]
        [Display(Name = "Closing Hours")]
        public System.TimeSpan ClosingHours { get; set; }
        [Display(Name = "Star Rating")]
        [Range(1, 7, ErrorMessageResourceName = "Valid_StarRating", ErrorMessageResourceType = typeof(Resources))]
        public int StarRating { get; set; }
        [Display(Name = "Price Range")]
        [Range(1, 5, ErrorMessageResourceName = "Valid_PriceRange", ErrorMessageResourceType = typeof(Resources))]
        public int PriceRange { get; set; }      
    }

    [MetadataType(typeof(RestaurantMetaData))]
    public partial class Restaurant : EntityBase
    {
    }

    public class RestaurantGroupByStarRating
    {
        public int StarRating { get; set; }
        public ICollection<Restaurant> Restaurant { get; set; }
    }

    public class RestaurantGroupByPriceRange
    {
        public int PriceRange { get; set; }
        public ICollection<Restaurant> Restaurant { get; set; }
    }

    public class RestaurantGroupByRadius
    {
        public int Distance { get; set; }
        public ICollection<Restaurant> Restaurant { get; set; }
    }
}
