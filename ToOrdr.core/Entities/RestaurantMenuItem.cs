﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(RestaurantMenuItemMetadata))]
    public partial class RestaurantMenuItem : EntityBase
    {
    }

    public class RestaurantMenuItemMetadata
    {
        [Display(Name = "ItemName", ResourceType=typeof(Resources))]
        [Required(ErrorMessageResourceName = "Rqd_ItemName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "Max_ItemName", ErrorMessageResourceType = typeof(Resources))]
        public string ItemName { get; set; }

        [Display(Name = "ItemDescription", ResourceType = typeof(Resources))]
        public string ItemDescription { get; set; }

        [Range(1, float.MaxValue, ErrorMessageResourceName = "Invalid_MenutItemPrice", ErrorMessageResourceType = typeof(Resources))]
        [Required]
        [Display(Name="Price",ResourceType=typeof(Resources))]
        public double Price { get; set; }

        [Display(Name="TaxId",ResourceType=typeof(Resources))]
        public Nullable<int> TaxId { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Resources))]
        public Nullable<bool> IsActive { get; set; }        
    }
    public class RestaurantFavoriteMenuItems
    {
        public int Id { get; set; }
        public int RestaurantMenuGroupId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public ICollection<RestaurantMenuItemImage> RestaurantMenuItemImages { get; set; }
        public Restaurant Restaurant { get; set; }
    }

    public class MenuItemOfferMetadata
    {
        [Required]
        public int MenuItemId { get; set; }

        [Required]
        [Range(0.1, double.MaxValue, ErrorMessageResourceName = "Valid_PercentageValue", ErrorMessageResourceType = typeof(Resources))]
        public double Percentage { get; set; }
    }
    [MetadataType(typeof(MenuItemOfferMetadata))]
    public partial class MenuItemOffer: EntityBase
    {

    }
}
