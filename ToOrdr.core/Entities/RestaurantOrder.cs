﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public partial class RestaurantOrder : EntityBase
    {
        [EnumDataType(typeof(Status_Type))]
        public Status_Type Status_Type_List { get; set; }
    }

    public enum OrderType
    {
        Table=0,
        Room=1
    }
    public enum Status_Type
    {
        Pending = 0,
        Confirmed = 1,
        Preparing = 2,
        Ready = 3,
        Served = 4,
        Rejected = 5,
        Canceled = 6
    }

    public class LastTwelveMonthsEarnings
    {
        public int Month { get; set; }
        public DateTime OrderDate { get; set; }
        public Nullable<double> TotalAmount { get; set; }
    }
    public class RestaurantWiseMonthEarnings
    {
        public string RestaurantName { get; set; }
        public Nullable<double> TotalAmount { get; set; }
    }
    public class CustomerOrderDetailsInfo
    {
        public int TotalOrders { get; set; }
        public Nullable<DateTime> LastOrder { get; set; }
    }

    public class CustomerMonthlyPayments
    {
        public int Month { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<RestaurantOrder> RestaurantOrder { get; set; }
    }

    public class CustomerMonthlyPaymentHistory
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
    }

    public class CustomerYearlyPayments
    {
        public int Year { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public double PreferredCurrencyTotalAmount { get; set; }
        public string PreferredCurrencySymbol { get; set; }
        public virtual ICollection<RestaurantOrder> RestaurantOrder { get; set; }
    }

    public partial class RestaurantTip:EntityBase
    {

    }
}
