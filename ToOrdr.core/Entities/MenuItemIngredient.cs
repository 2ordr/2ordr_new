﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class MenuItemIngredientMetaData
    {
        [Required(ErrorMessageResourceName = "Rqd_Ingredient", ErrorMessageResourceType = typeof(Resources))]
        public int IngredientId { get; set; }

        [Display(Name = "Weight(g)")]
        [Range(1, Int32.MaxValue, ErrorMessageResourceName = "Zero_Weight", ErrorMessageResourceType = typeof(Resources))]
        public double Weight { get; set; }
    }

    [MetadataType(typeof(MenuItemIngredientMetaData))]
    public partial class MenuItemIngredient : EntityBase
    {
    }

    public partial class MenuItemIngredientNutrition : EntityBase
    {

    }
}
