﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class SpaServiceDetailMetaData
    {
        [Required(ErrorMessage = "Please Enter Service Details Name")]
        public string Name { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessageResourceName = "Rqd_SpaServiceDetailPrice", ErrorMessageResourceType = typeof(Resources))]
        public double Price { get; set; }

    }

    [MetadataType(typeof(SpaServiceDetailMetaData))]
    public partial class SpaServiceDetail : EntityBase
    {
    }

    public partial class SpaDetailAdditional : EntityBase
    {

    }

    public class SpaServiceOfferMetadata
    {
        [Required]
        public int SpaServiceDetailId { get; set; }

        [Required]
        [Range(0.1, double.MaxValue, ErrorMessageResourceName = "Rqd_OfferPercentage", ErrorMessageResourceType = typeof(Resources))]
        public double Percentage { get; set; }

        [Display(Name="Offer Image")]
        public string OfferImage { get; set; }
    }

    [MetadataType(typeof(SpaServiceOfferMetadata))]
    public partial class SpaServiceOffer:EntityBase
    {

    }

    public partial class SpaSuggestion:EntityBase
    {

    }

    public class SpaIngredientCategoryMetadata
    {
        [Required]
        public string Name { get; set; }

    }

    [MetadataType(typeof(SpaIngredientCategoryMetadata))]
    public partial class SpaIngredientCategory:EntityBase
    {

    }

    public class SpaIngredientMetadata
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(SpaIngredientMetadata))]
    public partial class SpaIngredient:EntityBase
    {

    }


    public class SpaServiceDetailIngredientMetadata
    {
        [Required(ErrorMessage = "Please select spa ingredient")]
        public int SpaIngredientsId { get; set; }
    }

    [MetadataType(typeof(SpaServiceDetailIngredientMetadata))]
    public partial class SpaServiceDetailIngredient : EntityBase
    {

    }
}
