﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(RestaurantMenuGroupMetadata))]
    public partial class RestaurantMenuGroup : EntityBase
    {
    }

    public class RestaurantMenuGroupMetadata
    {
        [Display(Name = "GroupName", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "Rqd_GroupName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "Max_GroupName", ErrorMessageResourceType = typeof(Resources))]
        public string GroupName { get; set; }

        [Display(Name = "GroupDescription", ResourceType = typeof(Resources))]
        public string GroupDescription { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Resources))]
        public Nullable<bool> IsActive { get; set; }

        [Range(1,int.MaxValue,ErrorMessageResourceName="Rqd_ServeLevel",ErrorMessageResourceType=typeof(Resources))]
        public int ServeLevel { get; set; }
    }
    public class RestaurantMenuGroupsOfMenu
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public string MyPreference { get; set; }
        public ICollection<RestaurantMenuItem> RestaurantMenuItems { get; set; }
    }
}
