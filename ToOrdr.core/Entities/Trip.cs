﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class HotelTripsMetadata
    {
        [Required(ErrorMessage = "Please Enter Trip Name")]
        [Display(Name = "Trip Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Trip Desription")]
        [Display(Name = "Trip Description")]
        public string Description { get; set; }

        [Range(1, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [Display(Name = "Trip Rate")]
        public double Rate { get; set; }

        [Range(0.1, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [Display(Name = "Trip Days")]
        public double Days { get; set; }

        [Display(Name = "Trip Image")]
        public string Image { get; set; }
        [Display(Name="Trip Date")]
        public Nullable<System.DateTime> TripDate { get; set; }
        public bool IsActive { get; set; }
    }
    [MetadataType(typeof(HotelTripsMetadata))]
    public partial class Trip : EntityBase
    {
    }
}
