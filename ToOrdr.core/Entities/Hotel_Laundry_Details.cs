﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class HotelLaundryDetailsMetadata
    {
        [Required(ErrorMessage = "Please Enter Item Name")]
        [Display(Name = "Item Name")]
        public string Laundry_Item { get; set; }

        /*[Required(ErrorMessage = "Please Enter Item Description")]
        [Display(Name = "Item Description")]
        public string Laundry_Item_Description { get; set; }*/

        [Range(1, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [Display(Name = "Rate Per Item")]
        public double Laundry_Item_Rate { get; set; }
        public bool IsActive { get; set; }
    }
    [MetadataType(typeof(HotelLaundryDetailsMetadata))]
    public partial class Hotel_Laundry_Details : EntityBase
    {
    }
}

