﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum SpaOrderStatus
    {
        Pending = 0,
        Confirmed = 1,
        Delivered = 2,
        Canceled = 3,
        Rejected=4
    }
    public partial class SpaOrder : EntityBase
    {
    }

    public partial class SpaOrderItem : EntityBase
    {
    }

    public partial class SpaOrderItemsAdditional : EntityBase
    {
    }

    public partial class SpaOrderExtraProcedure : EntityBase
    {
    }

    public partial class SpaTip : EntityBase
    {

    }
}
