﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum SpaEmployeeType
    {
        Anyone = 0,
        Male = 1,
        Female = 2
    }
    public partial class SpaCart : EntityBase
    {
    }

    public partial class SpaCartItem : EntityBase
    {
        public Nullable<int> SpaEmployeeGender { get; set; }
    }

    public partial class SpaCartItemsAdditional : EntityBase
    {
    }

    public partial class SpaCartExtraProcedure : EntityBase
    {
    }
}
