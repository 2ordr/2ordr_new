﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(RestaurantMenuMetadata))]
    public partial class RestaurantMenu : EntityBase
    {
    }

    public class RestaurantMenuMetadata
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "Rqd_ManuName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "Max_MenuName", ErrorMessageResourceType = typeof(Resources))]
        public string MenuName { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string MenuDescription { get; set; }

        [Display(Name = "Image", ResourceType = typeof(Resources))]
        public string Image { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Resources))]
        public Nullable<bool> IsActive { get; set; }
    }
}
