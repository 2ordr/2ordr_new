﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum HotelServiceType
    {
        Spa,
        Laundry,
        Housekeeping,
        Excursion
    }
    public enum HotelServicesIsLateEnum
    {
        Spa,
        Excursion
    }
    public class HotelServiceMetadata
    {
        [Required]
        [Display(Name = "Service Name")]
        public int ServiceId { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(HotelServiceMetadata))]
    public partial class HotelServices : EntityBase
    {
    }
}
