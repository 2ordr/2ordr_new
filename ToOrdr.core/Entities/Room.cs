﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ToOrdr.Core.Entities
{
    public class HotelRoomsMetadata
    {
        [Required(ErrorMessage="Please enter valid room number")]
        [Display(Name = "Room Number")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Room number contains only number and letters")]
        public string Number { get; set; }

        [Display(Name = "QRCode Image")]
        public string QRCodeImage { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

    }

    [MetadataType(typeof(HotelRoomsMetadata))]
    public partial class Room : EntityBase
    {

    }

    [MetadataType(typeof(RoomCategoryMetadata))]
    public partial class RoomCategory:EntityBase
    {

    }

    public class RoomCategoryMetadata
    {
        [Required]
        public string Name { get; set; }
    }
    [MetadataType(typeof(RoomCapacityMetadata))]
    public partial class RoomCapacity: EntityBase
    {

    }
    public class RoomCapacityMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Number of adults value shouldn't be empty or zero")]
        public int Adults { get; set; }
    }
    public partial class RoomAmenities:EntityBase
    {

    }
    
    [MetadataType(typeof(RoomRateMetadata))]
    public partial class RoomRate:EntityBase
    {

    }

    public class RoomRateMetadata
    {
        [Required]
        [Display(Name = "Room Capacity")]
        public int RoomCapacityId { get; set; }
        [Required]
        [Display(Name = "Room Category")]
        public int RoomCategoryId { get; set; }
        [Required]
        public Nullable<double> Price { get; set; }

    }

    public class RoomAndRateDetails
    {
        public Room Room { get; set; }
        public RoomRate RoomRate { get; set; }
    }


    public class CustomerHotelRoomMetadata : EntityBase
    {
        [Required]
        public int RoomId { get; set; }
    }

    [MetadataType(typeof(CustomerHotelRoomMetadata))]
    public partial class CustomerHotelRoom : EntityBase
    {

    }

}
