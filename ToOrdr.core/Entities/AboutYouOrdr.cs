﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{

    public class AboutYouOrdrMetaData
    {
        [Required]
        public int LanguageId { get; set; }

        [Required]
        public string Description { get; set; }
    }

    [MetadataType(typeof(AboutYouOrdrMetaData))]
    public partial class AboutYouOrdr : EntityBase
    {
    }
}
