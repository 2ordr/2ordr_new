﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class MenuItemAdditionalMetadata
    {
        [Required(ErrorMessage = "Please Select valid group")]
        public int MenuAdditionalGroupId { get; set; }
    }

    [MetadataType(typeof(MenuItemAdditionalMetadata))]
    public partial class MenuItemAdditional : EntityBase
    {

    }
}
