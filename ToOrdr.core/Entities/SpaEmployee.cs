﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2
    }

    public partial class SpaEmployee : EntityBase
    {
    }

    public partial class SpaEmployeeDetails : EntityBase
    {
    }

    public partial class SpaEmployeeTimeSlot : EntityBase
    {

    }


    public class SpaEmployeeAvailableTimeSlot : EntityBase
    {
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
    }
}
