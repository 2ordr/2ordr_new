﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public partial class VW_YourPlansDashboard :EntityBase
    {
    }

    public partial class VW_CustomerOrdersHistory:EntityBase
    {

    }

    public class CustomerAllHotelAndRestActiveOrders
    {
        public Nullable<long> YourPlanId { get; set; }
        public string OrderType { get; set; }
        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public int CustomerId { get; set; }
        public int OrgId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Activity { get; set; }
        public Nullable<System.DateTime> StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public Nullable<int> DisheshCount { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
        public int IsActive { get; set; }
    }

    public class CustomerAllHotelAndRestOrderHistory
    {
        public string OrderType { get; set; }
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public int OrgId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Activity { get; set; }
        public Nullable<System.DateTime> StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public Nullable<System.TimeSpan> Duration { get; set; }
        public Nullable<int> DisheshCount { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public string CurrencySymbol { get; set; }
        public Nullable<bool> TipsPaid { get; set; }
        public int OrderItemId { get; set; }
        public Nullable<int> ServiceDetailId { get; set; }
    }
}
