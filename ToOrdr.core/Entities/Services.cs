﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public  class ServicesMeradata
    {
        [Required]
        [Display(Name = "Name")]
        public string ServiceName { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string ServiceDescription { get; set; }
    }

     [MetadataType(typeof(ServicesMeradata))]
    public partial class Services : EntityBase
    {

    }
}
