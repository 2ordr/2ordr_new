﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{

    public class CuisineMetadata
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(CuisineMetadata))]
    public partial class Cuisine : EntityBase
    {
    }
}
