﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class ChainTableMetadata
    {
        [Required]
        [Display(Name = "Name")]
        public string ChainName;        
    }

     [MetadataType(typeof(ChainTableMetadata))]
    public partial class ChainTable : EntityBase
    {
    }
}
