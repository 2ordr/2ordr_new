﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(RestaurantTableMetadata))]
    public partial class RestaurantTable : EntityBase
    {
    }
    public class RestaurantTableMetadata
    {

        [Display(Name = "Table Number")]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_TableNumber", ErrorMessageResourceType = typeof(Resources))]
        public int TableNumber { get; set; }

        [Display(Name = "Total Seats")]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_TotalSeats", ErrorMessageResourceType = typeof(Resources))]
        public int TotalSeats { get; set; }

        public string Description { get; set; }
        [Display(Name = "QRCode Image")]
        public string QRCodeImage { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }
     

    }

    public class RestaurantTableGroupMetadata
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(RestaurantTableGroupMetadata))]
    public partial class RestaurantTableGroup:EntityBase
    {

    }

}
