﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{

    public enum AppServices
    {
        Restaurant,
        Spa,
        Laundry,
        Housekeeping,
        Excursion
    }
   
    public partial class Customer : EntityBase
    {

    }

    public class CustomerActiveOrdersAndBaskets
    {
        public ICollection<RestaurantOrder> ActiveOrders { get; set; }
        public ICollection<RestaurantCart> ActiveBaskets { get; set; }
    }

    public class CustomerActiveSpaOrdersAndBaskets
    {
        public ICollection<SpaOrder> ActiveSpaOrders { get; set; }
        public ICollection<SpaCart> ActiveSpaBaskets { get; set; }
    }

    public class CustomerActiveBookingsInHotel
    {
        public ICollection<CustomerHotelBooking> CustomerHotelBooking { get; set; }
        public ICollection<SpaOrder> SpaOrder { get; set; }

        //public ICollection<LaundryBooking> LaundryBooking { get; set; }
        //public ICollection<TaxiBooking> TaxiBooking { get; set; }
        //public ICollection<TripBooking> TripBooking { get; set; }
    }

    public partial class CustomerLog : EntityBase
    {

    }

    public class CustomerActiveLaundryOrdersAndBaskets
    {
        public ICollection<LaundryOrder> ActiveLaundryOrders { get; set; }
        public ICollection<LaundryCart> ActiveLaundryBaskets { get; set; }
    }

    public class CustomerActiveHousekeepingOrdersAndBaskets
    {
        public ICollection<HousekeepingOrder> ActiveHousekeepingOrders { get; set; }
        public ICollection<HousekeepingCart> ActiveHousekeepingBaskets { get; set; }
    }

    public class CustomerActiveExcursionOrdersAndBaskets
    {
        public ICollection<ExcursionOrder> ActiveExcursionOrders { get; set; }
        public ICollection<ExcursionCart> ActiveExcursionBaskets { get; set; }
    }
}
