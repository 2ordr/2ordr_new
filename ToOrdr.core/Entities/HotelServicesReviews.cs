﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
   public partial class HotelServicesReviews : EntityBase
    {
    }

    public class HotelAllServicesReviews
    {
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public string LogoImage { get; set; }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int HotelServiceId { get; set; }
        public int ServiceDetailId { get; set; }
        public string ServiceDetailName { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
    }
}
