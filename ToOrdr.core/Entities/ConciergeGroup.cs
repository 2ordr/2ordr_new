﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class ConciergeGroupMetaData
    {
        [Required(ErrorMessage = "Please enter valid ConciergeGroup name")]
        public string Name { get; set; }
    }

    [MetadataType(typeof(ConciergeGroupMetaData))]
    public partial class ConciergeGroup : EntityBase
    {

    }




    public class ConciergeMetaData
    {
        [Required(ErrorMessage = "Please enter valid Question")]
        public string Question { get; set; }

        [Required(ErrorMessage = "Please enter valid Answer")]
        public string Answer { get; set; }
    }

    [MetadataType(typeof(ConciergeMetaData))]

    public partial class Concierge : EntityBase
    {

    }
}
