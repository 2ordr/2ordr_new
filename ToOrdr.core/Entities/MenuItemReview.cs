﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public partial class MenuItemReview : EntityBase
    {
    }

    public class MenuItemReviewAndRestDetails
    {
        public int Id { get; set; }
        public int RestaurantMenuItemId { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
        public RestaurantMenuItem RestaurantMenuItem { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
