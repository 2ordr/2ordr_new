﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum TaxiBookingStatus
    {
        Pending = 0,
        Confirmed = 1,
        Delivered = 2,
        Canceled = 3
    }
    public partial class TaxiBooking : EntityBase
    {
    }

    public partial class TaxiBookingReview:EntityBase
    {

    }
}
