﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public enum WeekDays
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7

    }
    public enum ExcursionOrderStatus
    {
        Pending = 0,
        Confirmed = 1,
        Delivered = 2,
        Canceled = 3,
        Rejected = 4
    }
    public class ExcursionMetadata
    {
        [Required]
        public string Name { get; set; }
    }
    [MetadataType(typeof(ExcursionMetadata))]
    public partial class Excursion : EntityBase
    {
    }

    public partial class HotelExcursion : EntityBase
    {

    }

    public class ExcursionDetailMetadata
    {
        [Required]
        public string Name { get; set; }

        [Display(Name = "Duration In Days")]
        [Range(0, int.MaxValue, ErrorMessage = "Days shouldn't be Negative")]
        public int DurationInDays { get; set; }

        [Display(Name = "Duration Hours")]
        public Nullable<System.TimeSpan> DurationHours { get; set; }

        [Display(Name = "Week Day")]
        public int WeekDay { get; set; }

        [Display(Name = "Start Time")]
        public System.TimeSpan StartTime { get; set; }

        [Range(1, double.MaxValue, ErrorMessageResourceName = "Rqd_ExcursionDetPrice", ErrorMessageResourceType = typeof(Resources))]
        public double Price { get; set; }
    }

    [MetadataType(typeof(ExcursionDetailMetadata))]
    public partial class ExcursionDetail : EntityBase
    {

    }

    public class ExcursionPriceMinAndMax
    {
        public Nullable<double> Excursion_Price_Min { get; set; }
        public Nullable<double> Excursion_Price_Max { get; set; }
    }

    public partial class ExcursionDetailIngredient : EntityBase
    {

    }
    public class ExcursionIngredientCategoryMetaData
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(ExcursionIngredientCategoryMetaData))]
    public partial class ExcursionIngredientCategory : EntityBase
    {

    }
    public class ExcursionIngredientMetaData
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(ExcursionIngredientMetaData))]
    public partial class ExcursionIngredient : EntityBase
    {

    }

    public partial class ExcursionReview : EntityBase
    {

    }

    public class ExcursionOfferMetaData
    {
        [Required]
        public int ExcursionDetailId { get; set; }

        [Required]
        [Range(0.1, double.MaxValue, ErrorMessageResourceName = "Rqd_OfferPercentage", ErrorMessageResourceType = typeof(Resources))]
        public double Percentage { get; set; }

        [Display(Name = "Offer Image")]
        public string OfferImage { get; set; }
    }

    [MetadataType(typeof(ExcursionOfferMetaData))]
    public partial class ExcursionOffer : EntityBase
    {

    }

    public partial class ExcursionDetailImages : EntityBase
    {

    }

    public partial class ExcursionDetailGallery : EntityBase
    {

    }

    public partial class ExcursionSuggestion : EntityBase
    {

    }

    public partial class CustomerFavoriteExcursion : EntityBase
    {

    }
    public partial class CustomerExcursionPreferences : EntityBase
    {

    }

    public partial class ExcursionCart : EntityBase
    {

    }
    public partial class ExcursionCartItems : EntityBase
    {

    }

    public partial class ExcursionOrder : EntityBase
    {

    }
    public partial class ExcursionOrderItems : EntityBase
    {

    }

    public partial class ExcursionTips : EntityBase
    {

    }
    public partial class ExcursionOrderReview : EntityBase
    {

    }
}
