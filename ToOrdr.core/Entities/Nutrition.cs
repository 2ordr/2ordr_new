﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class NutritionMetaData
    {
        [Required(ErrorMessage = "Please enter valid Nutrition")]
        public string Name { get; set; }

        [Display(Name = "Measurement Unit")]
        [Required(ErrorMessage = "Please select proper Measurement Unit")]
        public int MeasurementUnitId { get; set; }
    }

    [MetadataType(typeof(NutritionMetaData))]
    public partial class Nutrition : EntityBase
    {
    }
}
