﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class HotelTripDetailsMetadata
    {
        [Required(ErrorMessage = "Please Enter Place Name")]
        public string Place { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
    [MetadataType(typeof(HotelTripDetailsMetadata))]
    public partial class TripDetail: EntityBase
    {
    }
}
