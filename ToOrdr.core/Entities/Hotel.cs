﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class HotelMetaData
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        [Display(Name = "Pin Code")]
        public string Pincode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        [Display(Name = "Cost For Two")]
        public Nullable<int> CostForTwo { get; set; }
        public bool Status { get; set; }
        [Display(Name = "Background Image")]
        public string BackgroundImage { get; set; }
        [Display(Name = "Logo Image")]
        public string LogoImage { get; set; }
        [Display(Name = "CheckIn Time")]
        public Nullable<System.TimeSpan> CheckInTime { get; set; }
        [Display(Name = "CheckOut Time")]
        public Nullable<System.TimeSpan> CheckOutTime { get; set; }
        [Display(Name = "Star Rating")]
        [Range(1, 7, ErrorMessageResourceName = "Valid_StarRating", ErrorMessageResourceType = typeof(Resources))]
        public int StarRating { get; set; }
        [Display(Name = "Price Range")]
        [Range(1, 5, ErrorMessageResourceName = "Valid_PriceRange", ErrorMessageResourceType = typeof(Resources))]
        public int PriceRange { get; set; }
        public Nullable<int> CountryId { get; set; }
    }
    [MetadataType(typeof(HotelMetaData))]
    public partial class Hotel : EntityBase
    {
    }

    public class HotelsGroupByStarRating
    {
        public int StarRating { get; set; }
        public ICollection<Hotel> Hotel { get; set; }
    }

    public class HotelsGroupByPriceRange
    {
        public int PriceRange { get; set; }
        public ICollection<Hotel> Hotel { get; set; }
    }

    public class HotelsGroupByRadius
    {
        public int Distance { get; set; }
        public ICollection<Hotel> Hotel { get; set; }
    }


    public class HotelNearByPlacesMetadata
    {
        [Required]
        public string Place { get; set; }
    }

    [MetadataType(typeof(HotelNearByPlacesMetadata))]
    public partial class HotelsNearByPlace : EntityBase
    {

    }

    public class HotelHouseKeepingInfoMetaData
    {
        [Required]
        public int HotelId { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [Display(Name = "House Keeping Contact")]
        public string HouseKeepingContact { get; set; }
    }

    [MetadataType(typeof(HotelHouseKeepingInfoMetaData))]
    public partial class HotelHouseKeepingInfo : EntityBase
    {

    }

    public class AllHotelServicesOffers : EntityBase
    {
        public string OfferType { get; set; }
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public int OfferId { get; set; }
        public int OfferDetailsId { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public string OfferImage { get; set; }
        public bool IsActive { get; set; }
    }
}
