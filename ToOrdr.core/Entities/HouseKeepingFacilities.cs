﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class RoomDetailsMetadata
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }


    [MetadataType(typeof(RoomDetailsMetadata))]
    public partial class RoomDetails : EntityBase
    {

    }

    public class HouseKeepingFacilitiesMetadata
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

    }


    [MetadataType(typeof(HouseKeepingFacilitiesMetadata))]
    public partial class HouseKeepingFacility : EntityBase
    {
    }

    public class HousekeepingFacilityDetailMetaData
    {
        [Required(ErrorMessage = "Please enter valid Name")]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Price shouldn't be zero or negative")]
        public Nullable<double> Price { get; set; }
    }

    [MetadataType(typeof(HousekeepingFacilityDetailMetaData))]
    public partial class HouseKeepingFacilityDetail : EntityBase
    {

    }

    public class HouseKeepingfacilitiesAdditionalMetadata
    {
        [Required]
        public int HouseKeepingAdditionalGroupId { get; set; }
    }

    [MetadataType(typeof(HouseKeepingfacilitiesAdditionalMetadata))]
    public partial class HouseKeepingfacilitiesAdditional : EntityBase
    {

    }

    public partial class HouseKeeping : EntityBase
    {

    }

    public class HousekeepingAdditionalGroupMetadata
    {
        [Required(ErrorMessage = "Please enter valid Name")]
        public string Name { get; set; }

        public string Description { get; set; }

        [Display(Name = "Min Selected")]
        [Range(0, 1, ErrorMessageResourceName = "Valid_MinValue", ErrorMessageResourceType = typeof(Resources))]
        public int MinSelected { get; set; }

        [Display(Name = "Max Selected")]
        public int MaxSelected { get; set; }
    }

    [MetadataType(typeof(HousekeepingAdditionalGroupMetadata))]
    public partial class HousekeepingAdditionalGroup : EntityBase
    {

    }

    public class HousekeepingAdditionalElementMetadata
    {
        [Required(ErrorMessage = "Please enter valid Name")]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please Select Proper Price")]
        public Nullable<double> Price { get; set; }
    }

    [MetadataType(typeof(HousekeepingAdditionalElementMetadata))]
    public partial class HousekeepingAdditionalElement : EntityBase
    {

    }

}
