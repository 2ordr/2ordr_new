﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class SPAServiceDetailsGroupMetaData
    {
        [Required(ErrorMessage = "Please Enter Service Details group Name")]
        [Display(Name = "Group Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Service Details group description")]
        [Display(Name = "Group Description")]
        public string Description { get; set; }
    }

    [MetadataType(typeof(SPAServiceDetailsGroupMetaData))]
    public partial class SPAServiceDetailsGroup : EntityBase
    {
    }
}
