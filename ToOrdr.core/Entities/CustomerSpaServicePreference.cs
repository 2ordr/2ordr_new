﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public partial class CustomerSpaServicePreference : EntityBase
    {
    }

    public class CustomerSPAPreferences
    {
        public IEnumerable<SpaServicePreferences> CustomerSpaServicePreference { get; set; }
    }

    public class SpaServicePreferences
    {
        public int SPAServiceDetailId { get; set; }
        public bool? IsPreference { get; set; }
    }
}
