﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(SpaAdditionalElementMetaData))]
    public partial class SpaAdditionalElement : EntityBase
    {
    }

    public class SpaAdditionalElementMetaData
    {
        [Display(Name = "Element Name")]
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessageResourceName = "Rqd_SpaAddElementPrice", ErrorMessageResourceType = typeof(Resources))]
        public int Price { get; set; }
    }
    [MetadataType(typeof(SpaAdditionalGroupMetaData))]
    public partial class SpaAdditionalGroup : EntityBase
    {
    }
    public class SpaAdditionalGroupMetaData
    {
        [Display(Name = "Group Name")]
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessageResourceName = "Valid_MinSelectedValue", ErrorMessageResourceType = typeof(Resources))]
        public int MinSelected { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessageResourceName = "Valid_MaxSelectedValue", ErrorMessageResourceType = typeof(Resources))]
        public int MaxSelected { get; set; }

    }
}
