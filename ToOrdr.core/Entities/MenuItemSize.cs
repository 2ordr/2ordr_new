﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class MenuItemSizeMetaData
    {
        [Required(ErrorMessageResourceName = "Rqd_ItemSize", ErrorMessageResourceType = typeof(Resources))]
        public string SizeName { get; set; }        
    }

    [MetadataType(typeof(MenuItemSizeMetaData))]
    public partial class MenuItemSize : EntityBase
    {
    }
}
