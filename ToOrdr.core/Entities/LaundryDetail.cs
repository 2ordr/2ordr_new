﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class LaundryDetailsMetadata
    {
        [Required(ErrorMessage="Please select proper Garment")]
        public string GarmentId { get; set; }
    }
    [MetadataType(typeof(LaundryDetailsMetadata))]
   public partial class LaundryDetail: EntityBase
    {

    }
    public partial class LaundryDetailAdditional: EntityBase
    {

    }
}
