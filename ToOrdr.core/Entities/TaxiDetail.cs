﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    [MetadataType(typeof(TaxiDetailsMetadata))]
    public partial class TaxiDetail : EntityBase
    {
    }
    public class TaxiDetailsMetadata
    {
         [Required]
        public string Name { get; set; }
        [Display(Name="Car Number")]
        [Required]
         public string Number { get; set; }
        [Display(Name="Rate Per Km")]
         public double RatePerKm { get; set; }

    }
    [MetadataType(typeof(TaxiDestinationMetaData))]
    public partial class TaxiDestination:EntityBase
    {

    }
    public class TaxiDestinationMetaData
    {
        [Display(Name="From Place")]
        [Required]
        public string FromPlace { get; set; }

        [Display(Name="To Place")]
        [Required]
        public string ToPlace { get; set; }

    }
}
