﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class MeasurementUnitMetadata
    {
        [Required]
        [Display(Name="Unit Name")]
        public string UnitName { get; set; }
    }
    [MetadataType(typeof(MeasurementUnitMetadata))]
   public partial class MeasurementUnit:EntityBase
    {
    }
}
