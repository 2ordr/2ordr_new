﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class SpaServiceMetadata
    {
        [Required(ErrorMessage = "Please Enter Service Name")]
        [Display(Name = "Service Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Service description")]
        [Display(Name = "Service Description")]
        public string Description { get; set; }
        //    [Range(1, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        //    [Display(Name = "Service Rate")]
        //    public double Service_Rate { get; set; }
        //    [Range(0.1, 24, ErrorMessage = "The value must be greater than 0")]
        //    [Display(Name = "Service Hours")]
        //    public double Service_Hours { get; set; }
        //    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        //    [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        //    [Display(Name = "Available From")]
        //    public System.TimeSpan Service_Available_From { get; set; }
        //    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        //    [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        //    [Display(Name = "Available To")]
        //    public System.TimeSpan Service_Available_To { get; set; }
        //    [Display(Name = "Service Photo")]
        //    public string Service_Photo { get; set; }
        //    public bool IsActive { get; set; }
    }

    [MetadataType(typeof(SpaServiceMetadata))]
    public partial class SpaService : EntityBase
    {
    }

    public partial class SpaServiceReview : EntityBase
    {

    }
    public partial class SpaServiceDetailImages:EntityBase
    {

    }
    [MetadataType(typeof(SpaRoomMetaData))]
    public partial class SpaRoom:EntityBase
    {

    }

    public class SpaRoomMetaData
    {
        [Display(Name="Room Number")]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Rqd_SpaRoomNumber", ErrorMessageResourceType = typeof(Resources))]
        public int Number { get; set; }
    }

    public partial class SpaRoomDetails: EntityBase
    {

    }
}
