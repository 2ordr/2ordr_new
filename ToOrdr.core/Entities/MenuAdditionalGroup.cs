﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Localization;

namespace ToOrdr.Core.Entities
{
    public class MenuAdditionalGroupMetadata
    {
        public int Id { get; set; }
        [Display(Name="Group Name")]
        [Required]
        public string GroupName { get; set; }
        [Display(Name = "Group Description")]
        public string GroupDescription { get; set; }

        [Display(Name = "Minimum Selected")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = "Rqd_MinSelectedValue",ErrorMessageResourceType=typeof(Resources))]
        public int MinimumSelected { get; set; }

        [Display(Name = "Maximum Selected")]
        [Range(0, int.MaxValue, ErrorMessageResourceName = "Rqd_MaxSelectedValue", ErrorMessageResourceType = typeof(Resources))]
        public int MaximumSelected { get; set; }
    }
    [MetadataType(typeof(MenuAdditionalGroupMetadata))]
    public partial class MenuAdditionalGroup : EntityBase
    {
    }
}
