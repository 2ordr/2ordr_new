﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class SPAServiceGroupMetaData
    {
        [Required(ErrorMessage = "Please Enter Service Name")]
        [Display(Name = "Service Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Service description")]
        [Display(Name = "Service Description")]
        public string Description { get; set; }
    }

    [MetadataType(typeof(SPAServiceGroupMetaData))]
    public partial class SPAServiceGroup : EntityBase
    {
    }
}
