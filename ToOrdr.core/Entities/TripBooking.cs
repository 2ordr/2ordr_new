﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class HotelTripBookingMetadata
    {
        [Display(Name = "Customer Name")]
        public int Id { get; set; }

        [Display(Name = "Booking Date & Time")]
        public System.DateTime Booking_Date_Time { get; set; }

        [Display(Name = "Number Of Seats")]
        public int Number_Of_Seats { get; set; }

        [Display(Name = "Booking Status")]
        public Nullable<bool> Booking_Status { get; set; }
    }
    //[MetadataType(typeof(HotelTripBookingMetadata))]
    public partial class TripBooking : EntityBase
    {
    }
}
