﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public enum LaundryOrderStatus
    {
        Pending = 0,
        Confirmed = 1,
        Delivered = 2,
        Canceled = 3,
        Rejected = 4
    }
    public partial class LaundryOrder : EntityBase
    {

    }

    public partial class LaundryOrderItems : EntityBase
    {

    }

    public partial class LaundryOrderItemsAdditional : EntityBase
    {

    }

    public partial class LaundryTip : EntityBase
    {

    }

    public partial class LaundryOrderItemImages : EntityBase
    {
        
    }
}
