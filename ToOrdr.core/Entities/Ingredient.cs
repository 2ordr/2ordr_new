﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{

    public enum DrinksType
    {
        Alcoholic = 0,
        NonAlcoholic = 1
    }


    public class IngredientMetadata
    {
        [Required]
        public string Name;

        [Display(Name = "Image")]
        public string IngredientImage { get; set; }

    }


    [MetadataType(typeof(IngredientMetadata))]
    public partial class Ingredient : EntityBase
    {
    }

    public class FoodProfileMetadata
    {
        [Required]
        public string Name { get; set; }
    }

    [MetadataType(typeof(FoodProfileMetadata))]
    public partial class FoodProfile : EntityBase
    {

    }

    public class IngredientCategoriesMetadata
    {
        [Required]
        public string Name { get; set; }

        [Display(Name = "Drink Type")]
        public Nullable<bool> DrinkType { get; set; }
    }

    [MetadataType(typeof(IngredientCategoriesMetadata))]
    public partial class IngredientCategory : EntityBase
    {

    }

    public partial class IngredientFoodProfile : EntityBase
    {

    }

    public class DrinkGroups
    {
        public Nullable<bool> DrinkType { get; set; }

        public virtual ICollection<IngredientCategory> IngredientCategory { get; set; }

    }


    public class IngredientNutritionValuesMetadata
    {
        [Display(Name = "Ingredient Weight (g)")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Ingredient weight shouldn't be zero or negative")]
        public double WeightInGrams { get; set; }

        [Display(Name = "Nutrition")]
        [Required(ErrorMessage = "Please select proper Nutrition")]
        public int NutritionId { get; set; }

        [Display(Name = "Value")]
        [Required(ErrorMessage = "Please enter proper value")]
        [RegularExpression("(^(^[0-9]+([.][0-9]{1,2})?$)|Tr)", ErrorMessage = "Please enter proper value")]
       
        public string NutritionValue { get; set; }
    }

    [MetadataType(typeof(IngredientNutritionValuesMetadata))]
    public partial class IngredientNutritionValues : EntityBase
    {

    }
}
