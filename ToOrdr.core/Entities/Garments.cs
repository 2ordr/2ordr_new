﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class GarmentsMetaData
    {
        [Required(ErrorMessage = "Please enter valid Garments name")]
        public string Name { get; set; }
    }

    [MetadataType(typeof(GarmentsMetaData))]
    public partial class Garments : EntityBase
    {
    }
}
