﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Entities
{
    public class AmenitiesMetaData
    {
        [Required(ErrorMessage="Please enter valid Amenity name")]
        public string Name { get; set; }
    }

    [MetadataType(typeof(AmenitiesMetaData))]
    public partial class Amenities : EntityBase
    {
    }
}
