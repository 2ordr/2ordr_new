﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Exceptions
{
    public class CartExistsException : InvalidOperationException
    {
        public CartExistsException(string message)
            : base(message)
        {

        }
    }
    public class CartEmptyException : InvalidOperationException
    {
        public CartEmptyException(string message)
            : base(message)
        {

        }
    }
    public class InvalidScheduleDateTimeException : ArgumentException
    {
        public InvalidScheduleDateTimeException(string message)
            : base(message)
        {

        }
    }
    public class OrderNotFoundException : ArgumentException
    {
        public OrderNotFoundException(string message)
            : base(message)
        {

        }
    }

    public class CardSpendingLimitException : ArgumentException
    {
        public CardSpendingLimitException(string message)
            : base(message)
        {

        }
    }
    public class ZeroItemQtyException : ArgumentException
    {
        public ZeroItemQtyException(string message)
            : base(message)
        {

        }
    }
    public class NegativeItemQtyException : ArgumentException
    {
        public NegativeItemQtyException(string message)
            : base(message)
        {

        }
    }

    public class BookingNotFoundException : ArgumentException
    {
        public BookingNotFoundException(string message)
            : base(message)
        {

        }
    }

    public class BadRequestException : ArgumentException
    {
        public BadRequestException(string message)
            : base(message)
        {

        }
    }
    public class ForbiddenException : Exception
    {
        public ForbiddenException(string message)
            : base(message)
        {

        }
    }
    public class NotFoundException : Exception
    {
        public NotFoundException(string message)
            : base(message)
        {

        }
    }

    public class NotCapturevoided : Exception
    {
        public NotCapturevoided(string message)
            : base(message)
        {

        }
    }

    public class NotFoundPaypalPaymentId : Exception
    {
        public NotFoundPaypalPaymentId(string message)
            : base(message)
        {

        }
    }

}
