﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Exceptions
{
    public class EmailInUseException : InvalidOperationException
    {
        public EmailInUseException(string message) : base(message) { }
    }
}
