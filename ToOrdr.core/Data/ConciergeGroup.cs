//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConciergeGroup
    {
        public ConciergeGroup()
        {
            this.Concierges = new HashSet<Concierge>();
        }
    
        public int Id { get; set; }
        public int HotelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual ICollection<Concierge> Concierges { get; set; }
        public virtual Hotel Hotel { get; set; }
    }
}
