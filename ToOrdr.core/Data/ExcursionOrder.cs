//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExcursionOrder
    {
        public ExcursionOrder()
        {
            this.ExcursionOrderItems = new HashSet<ExcursionOrderItems>();
            this.ExcursionTips = new HashSet<ExcursionTips>();
            this.ExcursionOrderReview = new HashSet<ExcursionOrderReview>();
        }
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int HotelId { get; set; }
        public int CardId { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public bool PaymentStatus { get; set; }
        public string QuickPayPaymentId { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public System.DateTime CreationDate { get; set; }
        public bool IsLate { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual CustomerCreditCard CustomerCreditCard { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual ICollection<ExcursionOrderItems> ExcursionOrderItems { get; set; }
        public virtual ICollection<ExcursionTips> ExcursionTips { get; set; }
        public virtual ICollection<ExcursionOrderReview> ExcursionOrderReview { get; set; }
    }
}
