//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExcursionIngredient
    {
        public ExcursionIngredient()
        {
            this.ExcursionDetailIngredients = new HashSet<ExcursionDetailIngredient>();
            this.CustomerExcursionPreferences = new HashSet<CustomerExcursionPreferences>();
        }
    
        public int Id { get; set; }
        public int ExcursionIngredientCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IngredientImage { get; set; }
    
        public virtual ICollection<ExcursionDetailIngredient> ExcursionDetailIngredients { get; set; }
        public virtual ExcursionIngredientCategory ExcursionIngredientCategory { get; set; }
        public virtual ICollection<CustomerExcursionPreferences> CustomerExcursionPreferences { get; set; }
    }
}
