//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpaService
    {
        public SpaService()
        {
            this.SpaServiceDetail = new HashSet<SpaServiceDetail>();
        }
    
        public int Id { get; set; }
        public int HotelServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual HotelServices HotelServices { get; set; }
        public virtual ICollection<SpaServiceDetail> SpaServiceDetail { get; set; }
    }
}
