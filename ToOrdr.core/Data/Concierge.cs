//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Concierge
    {
        public int Id { get; set; }
        public int ConciergeGroupId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual ConciergeGroup ConciergeGroup { get; set; }
    }
}
