//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RestaurantOrder
    {
        public RestaurantOrder()
        {
            this.RestaurantOrderItems = new HashSet<RestaurantOrderItem>();
            this.RestaurantOrderReviews = new HashSet<RestaurantOrderReview>();
            this.RestaurantTips = new HashSet<RestaurantTip>();
        }
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RestaurantId { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public System.DateTime ScheduledDate { get; set; }
        public Nullable<int> CustomerTableNumber { get; set; }
        public Nullable<int> CardId { get; set; }
        public Nullable<double> OrderTotal { get; set; }
        public bool PaymentStatus { get; set; }
        public string PaymentId { get; set; }
        public string Comment { get; set; }
        public bool IsActive { get; set; }
        public bool NextServingStatus { get; set; }
        public Nullable<int> RoomId { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual CustomerCreditCard CustomerCreditCard { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        public virtual ICollection<RestaurantOrderItem> RestaurantOrderItems { get; set; }
        public virtual RestaurantTable RestaurantTable { get; set; }
        public virtual ICollection<RestaurantOrderReview> RestaurantOrderReviews { get; set; }
        public virtual ICollection<RestaurantTip> RestaurantTips { get; set; }
        public virtual Room Room { get; set; }
    }
}
