//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaxiBooking
    {
        public TaxiBooking()
        {
            this.TaxiBookingReview = new HashSet<TaxiBookingReview>();
        }
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int TaxiDetailsId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreationDate { get; set; }
        public int OrderStatus { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual ICollection<TaxiBookingReview> TaxiBookingReview { get; set; }
        public virtual TaxiDetail TaxiDetails { get; set; }
    }
}
