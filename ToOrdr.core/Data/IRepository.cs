﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Data
{
    public interface IRepository<T> where T : EntityBase
    {
        T GetById(object id);

        T Insert(T entity);

        T InsertWithChild(T entity);


        void Insert(IEnumerable<T> entities);

        /// <summary>
        /// When whole entity to be replaced
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);


        void Update(IEnumerable<T> entities);





        /// <summary>
        /// Partial Update of entity, you can use source as any type but matching property names
        /// </summary>
        /// <param name="id">Id of the entity to be replaced</param>
        /// <param name="entity">Update from this</param>
        void Update(int id, object entity);


        /// <summary>
        /// Partial update of entity when source Type is same as entity type and to exclude few properties
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <param name="excludePropertyNames"></param>
        void Update(int id, T entity, string[] excludePropertyNames);


        void Delete(int id);

        void Delete(int[] ids);

        IQueryable<T> Table { get; }



        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        IList<T> ExecuteStoredProcedureList<T>(string commandText, params object[] parameters);
            

    }
}
