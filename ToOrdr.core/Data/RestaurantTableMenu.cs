//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RestaurantTableMenu
    {
        public int Id { get; set; }
        public int RestaurantTableId { get; set; }
        public int RestaurantMenuId { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual RestaurantMenu RestaurantMenu { get; set; }
        public virtual RestaurantTable RestaurantTable { get; set; }
    }
}
