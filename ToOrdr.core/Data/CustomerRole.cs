//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerRole
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RoleId { get; set; }
        public Nullable<int> HotelId { get; set; }
        public Nullable<int> RestaurantId { get; set; }
        public Nullable<int> ChainId { get; set; }
    
        public virtual Role Role { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ChainTable ChainTable { get; set; }
        public virtual Hotel Hotel { get; set; }
    }
}
