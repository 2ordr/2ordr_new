//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class LaundryCartItemImages
    {
        public int Id { get; set; }
        public int LaundryCartItemId { get; set; }
        public string Image { get; set; }
    
        public virtual LaundryCartItems LaundryCartItems { get; set; }
    }
}
