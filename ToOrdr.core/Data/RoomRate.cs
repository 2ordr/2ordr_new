//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RoomRate
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int RoomCapacityId { get; set; }
        public int RoomCategoryId { get; set; }
        public Nullable<double> Price { get; set; }
    
        public virtual RoomCapacity RoomCapacity { get; set; }
        public virtual RoomCategory RoomCategory { get; set; }
        public virtual Room Room { get; set; }
    }
}
