﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;


namespace ToOrdr.Core.Data
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private readonly ToOrdrDbContext _context = new ToOrdrDbContext();
        private IDbSet<TEntity> _entities;


        protected virtual IDbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();
                return _entities;
            }
        }


        public TEntity GetById(object id)
        {
            return this.Entities.Find(id);
        }

        public TEntity Insert(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");



                var newEntity = Entities.Create<TEntity>(); // we do this to populate navigational properties
                this.Entities.Add(newEntity);

                ((ToOrdrDbContext)_context).Entry(newEntity).CurrentValues.SetValues(entity);

                _context.SaveChanges();

                return newEntity;
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public TEntity InsertWithChild(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");



                this.Entities.Add(entity);


                _context.SaveChanges();

                return entity;
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }


        public void Insert(IEnumerable<TEntity> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entities");

                foreach (var entity in entities)
                {
                    Entities.Add(entity);
                }
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Update(TEntity entity)
        {
            try
            {

                _context.Entry<TEntity>(entity).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }

        }

        public void Update(IEnumerable<TEntity> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entities");
                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }


        public void Update(int id, Object entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                var retrievedEntity = GetById(id);

                if (retrievedEntity == null)
                    return;

                ((ToOrdrDbContext)_context).Entry(retrievedEntity).CurrentValues.SetValues(entity);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }


        public void Update(int id, TEntity entity, string[] excludePropertyNames)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                var retrievedEntity = GetById(id);

                if (retrievedEntity == null)
                    return;

                var properties = typeof(TEntity).GetProperties().Where(x => !excludePropertyNames.Contains(x.Name)).ToList();

                foreach (var property in properties)
                {
                    var propertyValue = property.GetValue(entity);
                    retrievedEntity.GetType().GetProperty(property.Name).SetValue(retrievedEntity, propertyValue);
                }


                this._context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }




        public void Delete(int id)
        {
            try
            {
                if (id == 0)
                    throw new ArgumentNullException("entity");


                var oldEntity = Entities.Find(id);
                this.Entities.Remove(oldEntity);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Delete(int[] ids)
        {
            try
            {
                if (ids == null)
                    throw new ArgumentNullException("entities");

                foreach (var id in ids)
                    this.Delete(id);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public IQueryable<TEntity> Table
        {
            get
            {
                return this.Entities;
            }
        }


        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters)
        {
            //add parameters to command
            if (parameters != null && parameters.Length > 0)
            {
                for (int i = 0; i <= parameters.Length - 1; i++)
                {
                    var p = parameters[i] as DbParameter;
                    if (p == null)
                        throw new Exception("Not support parameter type");

                    commandText += i == 0 ? " " : ", ";

                    commandText += "@" + p.ParameterName;
                    if (p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                    {
                        //output parameter
                        commandText += " output";
                    }
                }
            }

            var result = _context.Database.SqlQuery<TEntity>(commandText, parameters).ToList();
            return result;
        }



        protected string GetFullErrorText(DbEntityValidationException exc)
        {
            var msg = string.Empty;
            foreach (var validationErrors in exc.EntityValidationErrors)
                foreach (var error in validationErrors.ValidationErrors)
                    msg += string.Format("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage) + Environment.NewLine;
            return msg;
        }

    }
}
