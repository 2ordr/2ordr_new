//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToOrdr.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class LaundryDetailAdditional
    {
        public int Id { get; set; }
        public int LaundryDetailId { get; set; }
        public int LaundryAdditionalGroupId { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual LaundryDetail LaundryDetail { get; set; }
        public virtual LaundryAdditionalGroup LaundryAdditionalGroup { get; set; }
    }
}
