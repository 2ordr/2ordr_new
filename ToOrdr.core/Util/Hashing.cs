﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="Hashing.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;

namespace ToOrdr.Core.Util
{
    /// <summary>
    /// Class Hashing.
    /// </summary>
    public class Hashing
    {
        /// <summary>
        /// Gets the random salt.
        /// </summary>
        /// <returns>System.String.</returns>
        private static string GetRandomSalt()
        {
            return BCryptHelper.GenerateSalt(12);
        }

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns>System.String.</returns>
        public static string HashPassword(string password)
        {
            return BCryptHelper.HashPassword(password, GetRandomSalt());
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="correctHash">The correct hash.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ValidatePassword(string password, string correctHash)
        {
            return BCryptHelper.CheckPassword(password, correctHash);
        }

    }
}
