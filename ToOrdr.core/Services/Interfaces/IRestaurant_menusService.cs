﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="IRestaurant_menusService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface IRestaurantMenusService
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.RestaurantMenu}" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.RestaurantMenu}" />
    public interface IRestaurantMenusService : IServiceBase<RestaurantMenu>
    {
    }
}
