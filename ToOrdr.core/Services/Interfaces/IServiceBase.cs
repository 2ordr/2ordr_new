﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="IServiceBase.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface IServiceBase
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IServiceBase<T>
    {
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>T.</returns>
        T GetById(int id);

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>IEnumerable T.</returns>
        IEnumerable<T> GetAll(Func<T, bool> predicate);
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>IEnumerable T.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable T.</returns>
        IEnumerable<T> GetAll(string orderby, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable T.</returns>
        IEnumerable<T> GetAll(Func<T, bool> predicate, string orderby, int pageNumber, int pageSize, out int total);



        /// <summary>
        /// Adds the new.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void AddNew(T entity);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Update(T entity);

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="anyObect">Any obect.</param>
        void Update(int id, Object anyObect);

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="excludePropertyNames">The exclude property names.</param>
        void Update(int id, T entity, string[] excludePropertyNames);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(int id);
    }
}
