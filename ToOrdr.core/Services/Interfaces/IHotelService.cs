﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-11-2019
// ***********************************************************************
// <copyright file="IHotelService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Enum SearchTextTypes
    /// </summary>
    public enum SearchTextTypes
    {
        /// <summary>
        /// The everywhere
        /// </summary>
        Everywhere,
        /// <summary>
        /// The hotel name
        /// </summary>
        HotelName
    }
    /// <summary>
    /// Enum HotelGroupBy
    /// </summary>
    public enum HotelGroupBy
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The star rating
        /// </summary>
        StarRating,
        /// <summary>
        /// The price range
        /// </summary>
        PriceRange,
        /// <summary>
        /// The radius
        /// </summary>
        Radius
    }
    /// <summary>
    /// Interface IHotelService
    /// </summary>
    public interface IHotelService
    {

        #region Hotels


        /// <summary>
        /// Searches the specified total.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="checkInDate">The check in date.</param>
        /// <param name="checkOutDate">The check out date.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="radius_min">The radius minimum.</param>
        /// <param name="radius_max">The radius maximum.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="starRating_min">The star rating minimum.</param>
        /// <param name="starRating_max">The star rating maximum.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="userRating_min">The user rating minimum.</param>
        /// <param name="userRating_max">The user rating maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="searchTextTypes">The search text types.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selection">The selection.</param>
        /// <param name="starRating">The star rating.</param>
        /// <param name="priceRange">The price range.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> Search(out int total, string searchText, string checkInDate, string checkOutDate, double? lat, double? lng, double? radius_min, double? radius_max, int[] Ids, double? starRating_min, double? starRating_max, double? price_min = null, double? price_max = null, double? userRating_min = null, double? userRating_max = null, int page = 1, int limit = 20, SearchTextTypes searchTextTypes = SearchTextTypes.Everywhere, int? customerId = null, Selection selection = Selection.All, int? starRating = null, int? priceRange = null);

        /// <summary>
        /// Gets all hotels.
        /// </summary>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetAllHotels();
        /// <summary>
        /// Gets all user hotels.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetAllUserHotels(int customerId);

        /// <summary>
        /// Gets all hotels.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="range">The range.</param>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetAllHotels(double latitude, double longitude, double range, string orderby, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Gets the name of all hotels by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotel_name">Name of the hotel.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetAllHotelsByName(int customerId, string hotel_name);

        //IEnumerable<Hotel_Details> GetHotels(string orderby, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Gets the hotel by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Hotel.</returns>
        Hotel GetHotelById(int id);

        /// <summary>
        /// Creates the hotel.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Hotel.</returns>
        Hotel CreateHotel(Hotel details);

        /// <summary>
        /// Modifies the hotel.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Hotel.</returns>
        Hotel ModifyHotel(Hotel details);

        /// <summary>
        /// Deletes the hotel.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotel(int id);

        /// <summary>
        /// Hotels the authorized customers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> HotelAuthorizedCustomers(int id);
        /// <summary>
        /// Authorizeds the hotel customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> AuthorizedHotelCustomers(int customerId, int id);
        #endregion

        #region Hotel virtual Restaurants

        /// <summary>
        /// Gets all hotels restaurants.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelRestaurant.</returns>
        IEnumerable<HotelRestaurant> GetAllHotelsRestaurants(int skip, int pageSize, out int total);

        /// <summary>
        /// Gets the unassign hotel restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetUnassignHotelRestaurants();
        /// <summary>
        /// Creates the hotel restaurants.
        /// </summary>
        /// <param name="hotelRests">The hotel rests.</param>
        void CreateHotelRestaurants(List<HotelRestaurant> hotelRests);
        /// <summary>
        /// Deletes the hotel restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotelRestaurant(int id);
        /// <summary>
        /// Gets the hotel restaurant by rest identifier.
        /// </summary>
        /// <param name="restId">The rest identifier.</param>
        /// <returns>HotelRestaurant.</returns>
        HotelRestaurant GetHotelRestaurantByRestId(int restId);
        #endregion Hotel virtual Restaurants

        #region Hotel House Keeping Info
        /// <summary>
        /// Gets the hotel house keeping information by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>HotelHouseKeepingInfo.</returns>
        HotelHouseKeepingInfo GetHotelHouseKeepingInfoByHotelId(int hotelId);
        /// <summary>
        /// Adds the update house keeping information.
        /// </summary>
        /// <param name="hotelHouseKeepingInfo">The hotel house keeping information.</param>
        /// <returns>HotelHouseKeepingInfo.</returns>
        HotelHouseKeepingInfo AddUpdateHouseKeepingInfo(HotelHouseKeepingInfo hotelHouseKeepingInfo);
        #endregion Hotel House Keeping Info

        #region Hotel Near By Places
        //API
        /// <summary>
        /// Gets all hotel near by places.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelsNearByPlace.</returns>
        IEnumerable<HotelsNearByPlace> GetAllHotelNearByPlaces(int hotelId);

        //WEB
        /// <summary>
        /// Gets all hotel near by places.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelsNearByPlace.</returns>
        IEnumerable<HotelsNearByPlace> GetAllHotelNearByPlaces(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the hotel near by place.
        /// </summary>
        /// <param name="hotelNearByPlace">The hotel near by place.</param>
        /// <returns>HotelsNearByPlace.</returns>
        HotelsNearByPlace CreateHotelNearByPlace(HotelsNearByPlace hotelNearByPlace);
        /// <summary>
        /// Modifies the hotel near by place.
        /// </summary>
        /// <param name="hotelNearByPlace">The hotel near by place.</param>
        /// <returns>HotelsNearByPlace.</returns>
        HotelsNearByPlace ModifyHotelNearByPlace(HotelsNearByPlace hotelNearByPlace);
        /// <summary>
        /// Gets the hotel near by place by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HotelsNearByPlace.</returns>
        HotelsNearByPlace GetHotelNearByPlaceById(int id);
        /// <summary>
        /// Deletes the hotel near by place.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotelNearByPlace(int id);
        #endregion Hotel Near Places

        #region Hotel Reviews
        /// <summary>
        /// Gets the review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HotelReview.</returns>
        HotelReview GetReviewById(int id);
        /// <summary>
        /// Gets the hotel reviews.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetHotelReviews(int HotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all hotels reviews.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetAllHotelsReviews(string fromDate, string toDate, int? HotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all hotels reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetAllHotelsReviews(int customerId);
        /// <summary>
        /// Deletes the hotel review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotelReview(int id);
        /// <summary>
        /// Creates the hotel reviews.
        /// </summary>
        /// <param name="hotelReviews">The hotel reviews.</param>
        /// <returns>HotelReview.</returns>
        HotelReview CreateHotelReviews(HotelReview hotelReviews);
        /// <summary>
        /// Gets the customer all hotels reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetCustomerAllHotelsReviews(int customerId);
        /// <summary>
        /// Gets the hotel all reviews.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetHotelAllReviews(int? hotelId, int? customerId, int skip, int pageSize, out int total);

        #endregion

        #region hotel services
        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <returns>IEnumerable Entities.Services.</returns>
        IEnumerable<Entities.Services> GetAllServices();
        /// <summary>
        /// Gets all unassigned services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ToOrdr.Core.Entities.Services.</returns>
        IEnumerable<ToOrdr.Core.Entities.Services> GetAllUnassignedServices(int hotelId);
        /// <summary>
        /// Gets the name of all services by.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns>IEnumerable Entities.Services.</returns>
        IEnumerable<Entities.Services> GetAllServicesByName(string serviceName);
        /// <summary>
        /// Gets the hotel services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelServices.</returns>
        IEnumerable<HotelServices> GetHotelServices(int hotelId);
        /// <summary>
        /// Gets the hotel active service by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        HotelServices GetHotelActiveServiceByHotelId(int hotelId, int serviceId);
        /// <summary>
        /// Gets all hotel services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelServices.</returns>
        IEnumerable<HotelServices> GetAllHotelServices(int hotelId);
        /// <summary>
        /// Gets the hotel services by service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        HotelServices GetHotelServicesByServiceId(int serviceId);
        /// <summary>
        /// Gets the active hotel services by service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        HotelServices GetActiveHotelServicesByServiceId(int serviceId);
        /// <summary>
        /// Creates the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>Entities.Services.</returns>
        Entities.Services CreateService(Entities.Services services);
        /// <summary>
        /// Modifies the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>Entities.Services.</returns>
        Entities.Services ModifyService(Entities.Services services);
        /// <summary>
        /// Gets the service by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Entities.Services.</returns>
        Entities.Services GetServiceById(int Id);
        /// <summary>
        /// Deletes the service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        void DeleteService(int Id);

        /// <summary>
        /// Creates the hotel service.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <returns>HotelServices.</returns>
        HotelServices CreateHotelService(HotelServices hotelService);
        /// <summary>
        /// Modifies the hotel service.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <returns>HotelServices.</returns>
        HotelServices ModifyHotelService(HotelServices hotelService);
        /// <summary>
        /// Deletes the hotel service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        void DeleteHotelService(int Id);
        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole CreateCustomerRole(CustomerRole role);
        /// <summary>
        /// Gets all role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        IEnumerable<Role> GetAllRole();
        /// <summary>
        /// Gets all hotel admin access role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        IEnumerable<Role> GetAllHotelAdminAccessRole();
        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetCustomers(string searchText);
        /// <summary>
        /// Gets the customer role.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> GetCustomerRole(int customerId);
        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole UpdateCustomerRole(CustomerRole role);
        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomerRole(int id);
        /// <summary>
        /// Gets the customer by role identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole GetCustomerByRoleId(int Id);

        #endregion hotel services

        #region Hotel Services Reviews
        //HotelServicesReviews GetHotelServicesReviewsById(int reviewId);
        //IEnumerable<HotelServicesReviews> GetEachHotelServiceReviews(int hotelServiceId, int pageNumber, int pageSize, out int total);
        //IEnumerable<HotelServicesReviews> GetHotelAllServicesReviews(int hotelId, int pageNumber, int pageSize, out int total);
        //IEnumerable<HotelServicesReviews> GetHotelServicesReviews(int hotelId);
        //IEnumerable<HotelServicesReviews> GetHotelServicesReviewsByServiceId(int hotelId, int serviceId);
        //HotelServicesReviews CreateHotelServicesReviews(HotelServicesReviews hotelServicesReview);
        //void DeleteHotelServicesReview(int reviewId);
        ////IEnumerable<VW_ServicesReviews> GetAllHotelServicesReviews(int hotelId, int pageNumber, int pageSize, out int total);

        #endregion Hotel Services Reviews

        #region hotel spa_service

        //SPAServiceBooking GetHotelSPAServiceBooking(int id);
        //SPAServiceBooking CreateHotelSPAServiceBooking(SPAServiceBooking hotel_spa_service_booking);
        //IEnumerable<SPAServiceBooking> GetCustomerSPAServiceBooking(int customerId);
        //SPAServiceBooking ModifySPAServiceBooking(int customerId, SPAServiceBooking spaServiceBooking);
        // IEnumerable<SpaOrder> GetSpaOrder(int hotelId, int skip, int pageSize, out int total);
        //void DeleteSpaServiceBooking(int id);
        //IEnumerable<SPAServiceBookingDetail> GetSpaServiceBookingDetails(int id);


        /// <summary>
        /// Gets the hotel spa services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable&lt;SpaService&gt;.</returns>
        IEnumerable<SpaService> GetHotelSpaServices(int hotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the hotel spa service by service identifier.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable&lt;SpaService&gt;.</returns>
        IEnumerable<SpaService> GetHotelSPAServiceByServiceId(int hotelServiceId);
        /// <summary>
        /// Gets the hotel spa service by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaService.</returns>
        SpaService GetHotelSpaServiceById(int id);
        /// <summary>
        /// Gets the spa service details by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaServiceDetail.</returns>
        SpaServiceDetail GetSpaServiceDetailsById(int id);
        /// <summary>
        /// Creates the spa service.
        /// </summary>
        /// <param name="spaService">The spa service.</param>
        /// <returns>SpaService.</returns>
        SpaService CreateSpaService(SpaService spaService);
        /// <summary>
        /// Modifies the spa service.
        /// </summary>
        /// <param name="spaService">The spa service.</param>
        /// <returns>SpaService.</returns>
        SpaService ModifySpaService(SpaService spaService);
        /// <summary>
        /// Deletes the spa service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaService(int id);


        //IEnumerable<SPAServiceGroup> GetHotelSPAServiceGroups(int spaServiceId);
        //SPAServiceGroup CreateSPAServiceGroup(SPAServiceGroup spaServiceGroup);
        //SPAServiceGroup ModifySpaServiceGroup(SPAServiceGroup spaServiceGroup);
        //SPAServiceGroup GetHotelSPAServiceGroupById(int groupId);
        //void DeleteSpaServiceGroup(int id);


        //IEnumerable<SPAServiceDetailsGroup> GetHotelSPAServiceDetailsGroup(int spaServiceGroupId);
        //SPAServiceDetailsGroup CreateSPAServiceDetailsGroup(SPAServiceDetailsGroup spaServiceDetailGroup);
        //SPAServiceDetailsGroup ModifySPAServiceDetailsGroup(SPAServiceDetailsGroup spaServiceDetailGroup);
        //SPAServiceDetailsGroup GetHotelSPAServiceDetailsGroupById(int groupId);
        //void DeleteSPAServiceDetailsGroup(int id);


        /// <summary>
        /// Gets the spa service details.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <returns>IEnumerable&lt;SpaServiceDetail&gt;.</returns>
        IEnumerable<SpaServiceDetail> GetSpaServiceDetails(int spaServiceId);
        /// <summary>
        /// Gets the spa service details.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable&lt;SpaServiceDetail&gt;.</returns>
        IEnumerable<SpaServiceDetail> GetSpaServiceDetails(int spaServiceId, int customerId, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false);
        /// <summary>
        /// Searches the name of the spa service detail by.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable&lt;SpaServiceDetail&gt;.</returns>
        IEnumerable<SpaServiceDetail> SearchSpaServiceDetailByName(string searchText, int hotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Adds the spa service detail.
        /// </summary>
        /// <param name="spaServiceDetail">The spa service detail.</param>
        /// <returns>SpaServiceDetail.</returns>
        SpaServiceDetail AddSpaServiceDetail(SpaServiceDetail spaServiceDetail);
        /// <summary>
        /// Modifies the spa service detail.
        /// </summary>
        /// <param name="spaServiceDetail">The spa service detail.</param>
        /// <returns>SpaServiceDetail.</returns>
        SpaServiceDetail ModifySpaServiceDetail(SpaServiceDetail spaServiceDetail);
        /// <summary>
        /// Gets the spa service detail by identifier.
        /// </summary>
        /// <param name="detailId">The detail identifier.</param>
        /// <returns>SpaServiceDetail.</returns>
        SpaServiceDetail GetSpaServiceDetailById(int detailId);
        /// <summary>
        /// Deletes the spa service detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaServiceDetail(int id);

        /// <summary>
        /// Gets the spa detail image by identifier.
        /// </summary>
        /// <param name="spaDetImageId">The spa det image identifier.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        SpaServiceDetailImages GetSpaDetailImageById(int spaDetImageId);
        /// <summary>
        /// Gets the spa service detail images.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <returns>IEnumerable&lt;SpaServiceDetailImages&gt;.</returns>
        IEnumerable<SpaServiceDetailImages> GetSpaServiceDetailImages(int spaServiceDetailId);
        /// <summary>
        /// Updates the spa det image.
        /// </summary>
        /// <param name="spaDetImage">The spa det image.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        SpaServiceDetailImages UpdateSpaDetImage(SpaServiceDetailImages spaDetImage);
        /// <summary>
        /// Adds the spa detail image.
        /// </summary>
        /// <param name="spaDetImage">The spa det image.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        SpaServiceDetailImages AddSpaDetailImage(SpaServiceDetailImages spaDetImage);
        /// <summary>
        /// Deletes the spa service detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaServiceDetailImage(int id);

        //Spa service details Additional
        /// <summary>
        /// Gets all spa det additionals.
        /// </summary>
        /// <param name="spaDetailsId">The spa details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable&lt;SpaDetailAdditional&gt;.</returns>
        IEnumerable<SpaDetailAdditional> GetAllSpaDetAdditionals(int spaDetailsId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the spa det additional.
        /// </summary>
        /// <param name="spaDetailAdditional">The spa detail additional.</param>
        void CreateSpaDetAdditional(List<SpaDetailAdditional> spaDetailAdditional);
        /// <summary>
        /// Gets the spadet additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaDetailAdditional.</returns>
        SpaDetailAdditional GetSpadetAdditionalById(int id);
        /// <summary>
        /// Modifies the spa det additional.
        /// </summary>
        /// <param name="spaDetailAdditional">The spa detail additional.</param>
        /// <returns>SpaDetailAdditional.</returns>
        SpaDetailAdditional ModifySpaDetAdditional(SpaDetailAdditional spaDetailAdditional);
        /// <summary>
        /// Deletes the spadet additional.
        /// </summary>
        /// <param name="spaDetAdditionalId">The spa det additional identifier.</param>
        void DeleteSpadetAdditional(int spaDetAdditionalId);
        /// <summary>
        /// Gets the unassign spa add groups.
        /// </summary>
        /// <param name="spaDetailsId">The spa details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable&lt;SpaAdditionalGroup&gt;.</returns>
        IEnumerable<SpaAdditionalGroup> GetUnassignSpaAddGroups(int spaDetailsId, int hotelId);

        //Spa Additional Groups
        /// <summary>
        /// Gets the spa additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable&lt;SpaAdditionalGroup&gt;.</returns>
        IEnumerable<SpaAdditionalGroup> GetSpaAdditionalGroups(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa additional group by identifier.
        /// </summary>
        /// <param name="spaAddGroupId">The spa add group identifier.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        SpaAdditionalGroup GetSpaAdditionalGroupById(int spaAddGroupId);
        /// <summary>
        /// Creates the spa additional group.
        /// </summary>
        /// <param name="spaAddGroup">The spa add group.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        SpaAdditionalGroup CreateSpaAdditionalGroup(SpaAdditionalGroup spaAddGroup);
        /// <summary>
        /// Modifies the spa additional group.
        /// </summary>
        /// <param name="spaAddGroup">The spa add group.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        SpaAdditionalGroup ModifySpaAdditionalGroup(SpaAdditionalGroup spaAddGroup);
        /// <summary>
        /// Deletes the spa additional group.
        /// </summary>
        /// <param name="spaAddGrpId">The spa add GRP identifier.</param>
        void DeleteSpaAdditionalGroup(int spaAddGrpId);

        //Spa Additional Elements
        /// <summary>
        /// Gets the spa additional elements.
        /// </summary>
        /// <param name="spaAddGrpId">The spa add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable&lt;SpaAdditionalElement&gt;.</returns>
        IEnumerable<SpaAdditionalElement> GetSpaAdditionalElements(int spaAddGrpId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa additional element by identifier.
        /// </summary>
        /// <param name="spaAddElementId">The spa add element identifier.</param>
        /// <returns>SpaAdditionalElement.</returns>
        SpaAdditionalElement GetSpaAdditionalElementById(int spaAddElementId);
        /// <summary>
        /// Creates the spa additional element.
        /// </summary>
        /// <param name="spaAddElement">The spa add element.</param>
        /// <returns>SpaAdditionalElement.</returns>
        SpaAdditionalElement CreateSpaAdditionalElement(SpaAdditionalElement spaAddElement);
        /// <summary>
        /// Modifies the spa additional element.
        /// </summary>
        /// <param name="spaAddElement">The spa add element.</param>
        /// <returns>SpaAdditionalElement.</returns>
        SpaAdditionalElement ModifySpaAdditionalElement(SpaAdditionalElement spaAddElement);
        /// <summary>
        /// Deletes the spa additional element.
        /// </summary>
        /// <param name="spaAddElementId">The spa add element identifier.</param>
        void DeleteSpaAdditionalElement(int spaAddElementId);

        #endregion hotel spa_service

        #region Spa Suggestion
        /// <summary>
        /// Gets the spa service detail suggestions.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetSpaServiceDetailSuggestions(int spaServiceDetailId, int pageNumber, int pageSize, out int total);

        //WEB
        /// <summary>
        /// Gets the spa service det suggestions.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaSuggestion.</returns>
        IEnumerable<SpaSuggestion> GetSpaServiceDetSuggestions(int spaServiceDetId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the unassign spa service det suggetions.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetUnassignSpaServiceDetSuggetions(int spaServiceDetId, int hotelId);
        /// <summary>
        /// Creates the spa service detail suggestion.
        /// </summary>
        /// <param name="spaServiceDetSuggestion">The spa service det suggestion.</param>
        void CreateSpaServiceDetailSuggestion(List<SpaSuggestion> spaServiceDetSuggestion);
        /// <summary>
        /// Deletes the spa service det suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaServiceDetSuggestion(int id);

        #endregion Spa Suggestion

        #region Hotel Spa Service Detail Ingredients
        /// <summary>
        /// Gets the spa service detail ingredients.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetailIngredient.</returns>
        IEnumerable<SpaServiceDetailIngredient> GetSpaServiceDetailIngredients(int spaServiceDetailId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa service detail ingredient by identifier.
        /// </summary>
        /// <param name="spaServiceDetIngrId">The spa service det ingr identifier.</param>
        /// <returns>SpaServiceDetailIngredient.</returns>
        SpaServiceDetailIngredient GetSpaServiceDetailIngredientById(int spaServiceDetIngrId);
        /// <summary>
        /// Gets the unassign spa ingrdeients.
        /// </summary>
        /// <param name="spaServDetId">The spa serv det identifier.</param>
        /// <returns>IEnumerable SpaIngredient.</returns>
        IEnumerable<SpaIngredient> GetUnassignSpaIngrdeients(int spaServDetId);
        /// <summary>
        /// Creates the spa service detail ingredient.
        /// </summary>
        /// <param name="spaServiceDetailIngr">The spa service detail ingr.</param>
        void CreateSpaServiceDetailIngredient(List<SpaServiceDetailIngredient> spaServiceDetailIngr);
        /// <summary>
        /// Deletes the spa service detail ingredient.
        /// </summary>
        /// <param name="spaServiceDetIngrId">The spa service det ingr identifier.</param>
        void DeleteSpaServiceDetailIngredient(int spaServiceDetIngrId);
        #endregion Hotel Spa Service Detail Ingredients

        #region Spa Detail Extratime
        //App
        /// <summary>
        /// Gets the spa service dets extratimes.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraTime.</returns>
        IEnumerable<ExtraTime> GetSpaServiceDetsExtratimes(int spaServiceDetailId, int pageNumber, int pageSize, out int total);

        //Web
        /// <summary>
        /// Gets the spa service det extratimes.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraTime.</returns>
        IEnumerable<ExtraTime> GetSpaServiceDetExtratimes(int spaServiceDetId, int skip, int pageSize, out int total);

        /// <summary>
        /// Gets the spa detail extratime by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ExtraTime.</returns>
        ExtraTime GetSpaDetailExtratimeById(int Id);
        /// <summary>
        /// Creates the spa service detail extratime.
        /// </summary>
        /// <param name="extraTime">The extra time.</param>
        /// <returns>ExtraTime.</returns>
        ExtraTime CreateSpaServiceDetailExtratime(ExtraTime extraTime);
        /// <summary>
        /// Modifies the spa service detail extratime.
        /// </summary>
        /// <param name="extraTime">The extra time.</param>
        /// <returns>ExtraTime.</returns>
        ExtraTime ModifySpaServiceDetailExtratime(ExtraTime extraTime);
        /// <summary>
        /// Deletes the extra time.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExtraTime(int id);

        #endregion Spa Detail Extratime

        #region Spa Detail Extraprocedure

        //App
        /// <summary>
        /// Gets the spa service dets extraprocedures.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraProcedure.</returns>
        IEnumerable<ExtraProcedure> GetSpaServiceDetsExtraprocedures(int spaServiceDetailId, int pageNumber, int pageSize, out int total);

        //Web
        /// <summary>
        /// Gets the spa service det extraprocedure.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraProcedure.</returns>
        IEnumerable<ExtraProcedure> GetSpaServiceDetExtraprocedure(int spaServiceDetId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa detail extraprocedure by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ExtraProcedure.</returns>
        ExtraProcedure GetSpaDetailExtraprocedureById(int Id);
        /// <summary>
        /// Creates the spa service detail extraprocedure.
        /// </summary>
        /// <param name="extraProcedure">The extra procedure.</param>
        /// <returns>ExtraProcedure.</returns>
        ExtraProcedure CreateSpaServiceDetailExtraprocedure(ExtraProcedure extraProcedure);
        /// <summary>
        /// Modifies the spa service detail extraprocedure.
        /// </summary>
        /// <param name="extraProcedure">The extra procedure.</param>
        /// <returns>ExtraProcedure.</returns>
        ExtraProcedure ModifySpaServiceDetailExtraprocedure(ExtraProcedure extraProcedure);
        /// <summary>
        /// Deletes the extra procedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExtraProcedure(int id);

        #endregion Spa Detail Extraprocedure

        #region Spa Service Offer
        //API
        /// <summary>
        /// Gets the spa service detail offer by spa service identifier.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        IEnumerable<SpaServiceOffer> GetSpaServiceDetailOfferBySpaServiceId(int spaServiceId);
        /// <summary>
        /// Gets the hotel spa service detail offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        IEnumerable<SpaServiceOffer> GetHotelSpaServiceDetailOffer(int hotelId, int customerId, bool considerMyPreferences = false);
        //WEB
        /// <summary>
        /// Gets the spa service offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        IEnumerable<SpaServiceOffer> GetSpaServiceOffer(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the hotel unassigned offer spa service details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetHotelUnassignedOfferSpaServiceDetails(int hotelId);
        /// <summary>
        /// Gets the discounted spa service offer by identifier.
        /// </summary>
        /// <param name="spaServiceOfferId">The spa service offer identifier.</param>
        /// <returns>SpaServiceOffer.</returns>
        SpaServiceOffer GetDiscountedSpaServiceOfferById(int spaServiceOfferId);
        /// <summary>
        /// Creates the spa service offer.
        /// </summary>
        /// <param name="spaServiceOffer">The spa service offer.</param>
        /// <returns>SpaServiceOffer.</returns>
        SpaServiceOffer CreateSpaServiceOffer(SpaServiceOffer spaServiceOffer);
        /// <summary>
        /// Modifies the spa service offer.
        /// </summary>
        /// <param name="spaServiceOffer">The spa service offer.</param>
        /// <returns>SpaServiceOffer.</returns>
        SpaServiceOffer ModifySpaServiceOffer(SpaServiceOffer spaServiceOffer);
        /// <summary>
        /// Deletes the spa service offer.
        /// </summary>
        /// <param name="spaServiceOfferid">The spa service offerid.</param>
        void DeleteSpaServiceOffer(int spaServiceOfferid);

        #endregion Spa Service Offer

        #region Spa Ingredient Categories
        /// <summary>
        /// Gets all spa ingredient categories.
        /// </summary>
        /// <returns>IEnumerable SpaIngredientCategory.</returns>
        IEnumerable<SpaIngredientCategory> GetAllSpaIngredientCategories();
        /// <summary>
        /// Gets all spa ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaIngredientCategory.</returns>
        IEnumerable<SpaIngredientCategory> GetAllSpaIngredientCategories(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa ingredient category by identifier.
        /// </summary>
        /// <param name="spaIngredientCatId">The spa ingredient cat identifier.</param>
        /// <returns>SpaIngredientCategory.</returns>
        SpaIngredientCategory GetSpaIngredientCategoryById(int spaIngredientCatId);
        /// <summary>
        /// Creates the spa ingredient category.
        /// </summary>
        /// <param name="spaIngredientCategory">The spa ingredient category.</param>
        /// <returns>SpaIngredientCategory.</returns>
        SpaIngredientCategory CreateSpaIngredientCategory(SpaIngredientCategory spaIngredientCategory);
        /// <summary>
        /// Modifies the spa ingredient category.
        /// </summary>
        /// <param name="spaIngredientCategory">The spa ingredient category.</param>
        /// <returns>SpaIngredientCategory.</returns>
        SpaIngredientCategory ModifySpaIngredientCategory(SpaIngredientCategory spaIngredientCategory);
        /// <summary>
        /// Deletes the spa ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaIngredientCategory(int id);
        #endregion Spa Ingredient Categories

        #region Spa Ingredients
        /// <summary>
        /// Gets the spa ingredient by spa ing cat identifier.
        /// </summary>
        /// <param name="spaIngCatId">The spa ing cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaIngredient.</returns>
        IEnumerable<SpaIngredient> GetSpaIngredientBySpaIngCatId(int spaIngCatId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa ingredient by identifier.
        /// </summary>
        /// <param name="spaIngredientId">The spa ingredient identifier.</param>
        /// <returns>SpaIngredient.</returns>
        SpaIngredient GetSpaIngredientById(int spaIngredientId);
        /// <summary>
        /// Creates the spa ingredient.
        /// </summary>
        /// <param name="spaIngredient">The spa ingredient.</param>
        /// <returns>SpaIngredient.</returns>
        SpaIngredient CreateSpaIngredient(SpaIngredient spaIngredient);
        /// <summary>
        /// Modifies the spa ingredient.
        /// </summary>
        /// <param name="spaIngredient">The spa ingredient.</param>
        /// <returns>SpaIngredient.</returns>
        SpaIngredient ModifySpaIngredient(SpaIngredient spaIngredient);
        /// <summary>
        /// Deletes the spa ingredient.
        /// </summary>
        /// <param name="spaIngredientId">The spa ingredient identifier.</param>
        void DeleteSpaIngredient(int spaIngredientId);

        #endregion Spa Ingredients

        #region Spa Cart
        /// <summary>
        /// Gets the customer spa cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>SpaCart.</returns>
        SpaCart GetCustomerSpaCart(int customerId, int hotelId);
        /// <summary>
        /// Creates the spa cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>SpaCart.</returns>
        SpaCart CreateSpaCart(SpaCart cart, bool flush = false);
        /// <summary>
        /// Updates the spa cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>SpaCart.</returns>
        SpaCart UpdateSpaCart(int customerId, SpaCart cart);

        #endregion Spa Cart

        #region Spa Order

        /// <summary>
        /// Gets the spa order.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        IEnumerable<SpaOrder> GetSpaOrder(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa order.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        IEnumerable<SpaOrder> GetSpaOrder(int hotelId);
        /// <summary>
        /// Gets the spa orders details by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>SpaOrder.</returns>
        SpaOrder GetSpaOrdersDetailsById(int orderId);
        /// <summary>
        /// Creates the spa order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>SpaOrder.</returns>
        SpaOrder CreateSpaOrderFromCart(int customerId, int cardId, int hotelId);
        /// <summary>
        /// Gets the spa order is late status changed count.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>System.Int32.</returns>
        int GetSpaOrderIsLateStatusChangedCount(int hotelId);
        //SpaOrder ScheduleSpaOrder(int customerId, int orderId, DateTime scheduleDate);
        /// <summary>
        /// Updates the spa order status.
        /// </summary>
        /// <param name="spaOrder">The spa order.</param>
        /// <returns>SpaOrder.</returns>
        SpaOrder UpdateSpaOrderStatus(SpaOrder spaOrder);
        /// <summary>
        /// Creates the spa order review.
        /// </summary>
        /// <param name="spaOrdReview">The spa ord review.</param>
        /// <returns>SpaOrderReview.</returns>
        SpaOrderReview CreateSpaOrderReview(SpaOrderReview spaOrdReview);
        /// <summary>
        /// Gets the spa order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable SpaOrderReview.</returns>
        IEnumerable<SpaOrderReview> GetSpaOrderReviews(int id);
        /// <summary>
        /// Gets the spa order details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable SpaOrderItem.</returns>
        IEnumerable<SpaOrderItem> GetSpaOrderDetails(int id);
        /// <summary>
        /// Gets the customer active spa orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveSpaOrdersAndBaskets.</returns>
        CustomerActiveSpaOrdersAndBaskets GetCustomerActiveSpaOrders(int customerId);
        /// <summary>
        /// Updates the spa order item deliver status.
        /// </summary>
        /// <param name="spaOrderItemId">The spa order item identifier.</param>
        void UpdateSpaOrderItemDeliverStatus(int spaOrderItemId);
        /// <summary>
        /// Gets the spa order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        IEnumerable<SpaOrder> GetSpaOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total);

        /// <summary>
        /// Gets the customer Spa orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        IEnumerable<SpaOrder> GetCustomerSpaOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total);
        #endregion Spa Order

        #region Hotel Laundry
        /// <summary>
        /// Gets the hotel laundry service by identifier.
        /// </summary>
        /// <param name="Laundry_Service_ID">The laundry service identifier.</param>
        /// <returns>Laundry.</returns>
        Laundry GetHotelLaundryServiceByID(int Laundry_Service_ID);
        /// <summary>
        /// Gets the hotel laundry services.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Laundry.</returns>
        IEnumerable<Laundry> GetHotelLaundryServices(int HotelId);
        /// <summary>
        /// Gets all hotel laundry services.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Laundry.</returns>
        IEnumerable<Laundry> GetAllHotelLaundryServices(int HotelId, int hotelServiceId);
        /// <summary>
        /// Creates the hotel laundry service.
        /// </summary>
        /// <param name="laundryService">The laundry service.</param>
        /// <returns>Laundry.</returns>
        Laundry CreateHotelLaundryService(Laundry laundryService);
        /// <summary>
        /// Gets the laundry service details by identifier.
        /// </summary>
        /// <param name="ServiceId">The service identifier.</param>
        /// <returns>Laundry.</returns>
        Laundry GetLaundryServiceDetailsById(int ServiceId);
        /// <summary>
        /// Deletes the laundry service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteLaundryService(int id);
        /// <summary>
        /// Modifies the laundry service.
        /// </summary>
        /// <param name="Laundry_Service">The laundry service.</param>
        /// <returns>Laundry.</returns>
        Laundry ModifyLaundryService(Laundry Laundry_Service);

        /// <summary>
        /// Gets the hotel laundry details.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable LaundryDetail.</returns>
        IEnumerable<LaundryDetail> GetHotelLaundryDetails(int laundryId);
        /// <summary>
        /// Gets the hotel laundry details by laundry identifier.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable LaundryDetail.</returns>
        IEnumerable<LaundryDetail> GetHotelLaundryDetailsByLaundryId(int laundryId);
        /// <summary>
        /// Creates the hotel laundry details.
        /// </summary>
        /// <param name="Laundry_Details">The laundry details.</param>
        /// <returns>LaundryDetail.</returns>
        LaundryDetail CreateHotelLaundryDetails(LaundryDetail Laundry_Details);
        /// <summary>
        /// Modifies the hotel laundry details.
        /// </summary>
        /// <param name="Laundry_Details">The laundry details.</param>
        /// <returns>LaundryDetail.</returns>
        LaundryDetail ModifyHotelLaundryDetails(LaundryDetail Laundry_Details);
        /// <summary>
        /// Gets the hotel laundry details by identifier.
        /// </summary>
        /// <param name="Laundry_Details_ID">The laundry details identifier.</param>
        /// <returns>LaundryDetail.</returns>
        LaundryDetail GetHotelLaundryDetailsByID(int Laundry_Details_ID);
        /// <summary>
        /// Deletes the laundry details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteLaundryDetails(int id);

        //laundry reviews
        /// <summary>
        /// Gets the laundry reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryReview.</returns>
        IEnumerable<LaundryReview> GetLaundryReviews(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the laundry reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryReview.</returns>
        IEnumerable<LaundryReview> GetLaundryReviewsByHotel(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the laundry review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>LaundryReview.</returns>
        LaundryReview GetLaundryReviewById(int id);
        /// <summary>
        /// Deletes the laundry review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteLaundryReview(int id);
        /// <summary>
        /// Creates the laundry reviews.
        /// </summary>
        /// <param name="laundryReviews">The laundry reviews.</param>
        /// <returns>LaundryReview.</returns>
        LaundryReview CreateLaundryReviews(LaundryReview laundryReviews);
        //IEnumerable<Laundry> GetPendingLaundryServiceReviews(int customerId, int pageNumber, int pageSize, out int total);

        //Laundry Additional Groups
        /// <summary>
        /// Gets the laundry additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryAdditionalGroup.</returns>
        IEnumerable<LaundryAdditionalGroup> GetLaundryAdditionalGroups(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the laundry additional group by identifier.
        /// </summary>
        /// <param name="laundryAddGroupId">The laundry add group identifier.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        LaundryAdditionalGroup GetLaundryAdditionalGroupById(int laundryAddGroupId);
        /// <summary>
        /// Creates the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGroup">The laundry add group.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        LaundryAdditionalGroup CreateLaundryAdditionalGroup(LaundryAdditionalGroup laundryAddGroup);
        /// <summary>
        /// Modifies the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGroup">The laundry add group.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        LaundryAdditionalGroup ModifyLaundryAdditionalGroup(LaundryAdditionalGroup laundryAddGroup);
        /// <summary>
        /// Deletes the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGrpId">The laundry add GRP identifier.</param>
        void DeleteLaundryAdditionalGroup(int laundryAddGrpId);

        //Laundry Additional elements
        /// <summary>
        /// Gets the laundry additional elements.
        /// </summary>
        /// <param name="laundryAddGrpId">The laundry add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryAdditionalElement.</returns>
        IEnumerable<LaundryAdditionalElement> GetLaundryAdditionalElements(int laundryAddGrpId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the laundry additional element by identifier.
        /// </summary>
        /// <param name="laundryAddElementId">The laundry add element identifier.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        LaundryAdditionalElement GetLaundryAdditionalElementById(int laundryAddElementId);
        /// <summary>
        /// Creates the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElement">The laundry add element.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        LaundryAdditionalElement CreateLaundryAdditionalElement(LaundryAdditionalElement laundryAddElement);
        /// <summary>
        /// Modifies the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElement">The laundry add element.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        LaundryAdditionalElement ModifyLaundryAdditionalElement(LaundryAdditionalElement laundryAddElement);
        /// <summary>
        /// Deletes the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElementId">The laundry add element identifier.</param>
        void DeleteLaundryAdditionalElement(int laundryAddElementId);

        //Laundry Additional elements
        /// <summary>
        /// Gets all laundry det additionals.
        /// </summary>
        /// <param name="laundryDetailsId">The laundry details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryDetailAdditional.</returns>
        IEnumerable<LaundryDetailAdditional> GetAllLaundryDetAdditionals(int laundryDetailsId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the laundrydet additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>LaundryDetailAdditional.</returns>
        LaundryDetailAdditional GetLaundrydetAdditionalById(int id);
        /// <summary>
        /// Modifies the laundry det additional.
        /// </summary>
        /// <param name="laundryDetAdd">The laundry det add.</param>
        /// <returns>LaundryDetailAdditional.</returns>
        LaundryDetailAdditional ModifyLaundryDetAdditional(LaundryDetailAdditional laundryDetAdd);
        /// <summary>
        /// Deletes the laundrydet additional.
        /// </summary>
        /// <param name="laundryDetAdditionalId">The laundry det additional identifier.</param>
        void DeleteLaundrydetAdditional(int laundryDetAdditionalId);
        /// <summary>
        /// Gets the unassign laundry add groups.
        /// </summary>
        /// <param name="laundryDetailsId">The laundry details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable LaundryAdditionalGroup.</returns>
        IEnumerable<LaundryAdditionalGroup> GetUnassignLaundryAddGroups(int laundryDetailsId, int hotelId);
        /// <summary>
        /// Creates the laundry det additional.
        /// </summary>
        /// <param name="laundryDetAdditional">The laundry det additional.</param>
        void CreateLaundryDetAdditional(List<LaundryDetailAdditional> laundryDetAdditional);

        #endregion

        #region Laundry Cart

        /// <summary>
        /// Gets the customer laundry cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>LaundryCart.</returns>
        LaundryCart GetCustomerLaundryCart(int customerId, int hotelId);
        /// <summary>
        /// Creates the laundry cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>LaundryCart.</returns>
        LaundryCart CreateLaundryCart(LaundryCart cart, bool flush = false);
        /// <summary>
        /// Updates the laundry cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>LaundryCart.</returns>
        LaundryCart UpdateLaundryCart(int customerId, LaundryCart cart);

        #endregion Laundry Cart

        #region Laundry Order

        /// <summary>
        /// Gets the laundry orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryOrder.</returns>
        IEnumerable<LaundryOrder> GetLaundryOrdersQueue(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the laundry order item details.
        /// </summary>
        /// <param name="laundryOrderId">The laundry order identifier.</param>
        /// <returns>IEnumerable LaundryOrderItems.</returns>
        IEnumerable<LaundryOrderItems> GetLaundryOrderItemDetails(int laundryOrderId);
        /// <summary>
        /// Updates the laundry order item deliver status.
        /// </summary>
        /// <param name="laundryOrderItemId">The laundry order item identifier.</param>
        void UpdateLaundryOrderItemDeliverStatus(int laundryOrderItemId);
        /// <summary>
        /// Gets the laundry order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>LaundryOrder.</returns>
        LaundryOrder GetLaundryOrderById(int orderId);
        /// <summary>
        /// Creates the laundry order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>LaundryOrder.</returns>
        LaundryOrder CreateLaundryOrderFromCart(int customerId, int cardId, int roomId);
        /// <summary>
        /// Modifies the laundry order.
        /// </summary>
        /// <param name="laundryOrder">The laundry order.</param>
        /// <returns>LaundryOrder.</returns>
        LaundryOrder ModifyLaundryOrder(LaundryOrder laundryOrder);
        /// <summary>
        /// Creates the laundry order review.
        /// </summary>
        /// <param name="laundryOrderReview">The laundry order review.</param>
        /// <returns>LaundryOrderReview.</returns>
        LaundryOrderReview CreateLaundryOrderReview(LaundryOrderReview laundryOrderReview);
        /// <summary>
        /// Gets the laundry order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable LaundryOrderReview.</returns>
        IEnumerable<LaundryOrderReview> GetLaundryOrderReviews(int id);
        /// <summary>
        /// Gets all active laundry orders of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveLaundryOrdersAndBaskets.</returns>
        CustomerActiveLaundryOrdersAndBaskets GetAllActiveLaundryOrdersOfCustomer(int customerId);
        /// <summary>
        /// Gets the laundry order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryOrder.</returns>
        IEnumerable<LaundryOrder> GetLaundryOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total);

        /// <summary>
        /// Gets the customer laundry orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable laundryOrder.</returns>
        IEnumerable<LaundryOrder> GetCustomerLaundryOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total);
        #endregion Laundry Order

        #region Excursion Search
        /// <summary>
        /// Excursions the search.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="minUserRating">The minimum user rating.</param>
        /// <param name="maxUserRating">The maximum user rating.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="duration_min">The duration minimum.</param>
        /// <param name="duration_max">The duration maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> ExcursionSearch(out int total, int hotelId, string searchText, int[] Ids, double? minUserRating, double? maxUserRating, double? price_min, double? price_max, int? duration_min = null, int? duration_max = null, int page = 1, int limit = 20);
        #endregion Excursion Search

        #region Excursion
        /// <summary>
        /// Gets the excursions.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Excursion.</returns>
        IEnumerable<Excursion> GetExcursions(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion by identifier.
        /// </summary>
        /// <param name="excursionId">The excursion identifier.</param>
        /// <returns>Excursion.</returns>
        Excursion GetExcursionById(int excursionId);
        /// <summary>
        /// Creates the excursion.
        /// </summary>
        /// <param name="hotelExcursion">The hotel excursion.</param>
        /// <returns>Excursion.</returns>
        Excursion CreateExcursion(Excursion hotelExcursion);
        /// <summary>
        /// Modifies the excursion.
        /// </summary>
        /// <param name="hotelExcursion">The hotel excursion.</param>
        /// <returns>Excursion.</returns>
        Excursion ModifyExcursion(Excursion hotelExcursion);
        /// <summary>
        /// Deletes the excursion.
        /// </summary>
        /// <param name="excursionId">The excursion identifier.</param>
        void DeleteExcursion(int excursionId);

        //Hotel Excursion
        /// <summary>
        /// Gets the hotel excursion services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelExcursion.</returns>
        IEnumerable<HotelExcursion> GetHotelExcursionServices(int hotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all hotel excursions.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelExcursion.</returns>
        IEnumerable<HotelExcursion> GetAllHotelExcursions(int hotelServiceId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the hotel excursion by identifier.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <returns>HotelExcursion.</returns>
        HotelExcursion GetHotelExcursionById(int hotelExcursionId);
        /// <summary>
        /// Gets the unassign excursion.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Excursion.</returns>
        IEnumerable<Excursion> GetUnassignExcursion(int hotelServiceId);
        /// <summary>
        /// Creates the hotel excursion.
        /// </summary>
        /// <param name="listHotelExcursion">The list hotel excursion.</param>
        void CreateHotelExcursion(List<HotelExcursion> listHotelExcursion);
        /// <summary>
        /// Modifies the hotel excursion is active status.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        void ModifyHotelExcursionIsActiveStatus(int hotelExcursionId);
        /// <summary>
        /// Deletes the hotel excursion.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        void DeleteHotelExcursion(int hotelExcursionId);
        #endregion Excursion

        #region Excursion Detail
        /// <summary>
        /// Gets the excursion details by hotel exc identifier.
        /// </summary>
        /// <param name="HotelExcId">The hotel exc identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetExcursionDetailsByHotelExcId(int HotelExcId, int customerId, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false);

        /// <summary>
        /// Gets all excursion details.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetAllExcursionDetails(int hotelExcursionId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion detail by identifier.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>ExcursionDetail.</returns>
        ExcursionDetail GetExcursionDetailById(int excursionDetailId);
        /// <summary>
        /// Creates the excursion detail.
        /// </summary>
        /// <param name="hotelExcursionDet">The hotel excursion det.</param>
        /// <returns>ExcursionDetail.</returns>
        ExcursionDetail CreateExcursionDetail(ExcursionDetail hotelExcursionDet);
        /// <summary>
        /// Modifies the excursion detail.
        /// </summary>
        /// <param name="hotelExcursionDetails">The hotel excursion details.</param>
        /// <returns>ExcursionDetail.</returns>
        ExcursionDetail ModifyExcursionDetail(ExcursionDetail hotelExcursionDetails);
        /// <summary>
        /// Deletes the excursion detail.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        void DeleteExcursionDetail(int excursionDetailId);
        /// <summary>
        /// Gets the excursion price minimum and maximum of hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionPriceMinAndMax.</returns>
        ExcursionPriceMinAndMax GetExcursionPriceMinAndMaxOfHotel(int hotelId);
        #endregion Excursion Detail

        #region Excursion Detail Images
        /// <summary>
        /// Gets the excursion detail image by identifier.
        /// </summary>
        /// <param name="excDetImageId">The exc det image identifier.</param>
        /// <returns>ExcursionDetailImages.</returns>
        ExcursionDetailImages GetExcursionDetailImageById(int excDetImageId);
        /// <summary>
        /// Gets the excursion detail images.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>IEnumerable ExcursionDetailImages.</returns>
        IEnumerable<ExcursionDetailImages> GetExcursionDetailImages(int excursionDetailId);
        /// <summary>
        /// Updates the excursion det image.
        /// </summary>
        /// <param name="excDetImage">The exc det image.</param>
        /// <returns>ExcursionDetailImages.</returns>
        ExcursionDetailImages UpdateExcursionDetImage(ExcursionDetailImages excDetImage);
        /// <summary>
        /// Adds the excursion detail image.
        /// </summary>
        /// <param name="excDetImage">The exc det image.</param>
        /// <returns>ExcursionDetailImages.</returns>
        ExcursionDetailImages AddExcursionDetailImage(ExcursionDetailImages excDetImage);
        /// <summary>
        /// Deletes the excursion detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExcursionDetailImage(int id);
        #endregion Excursion Detail Images

        #region Excursion Detail Gallery
        /// <summary>
        /// Gets the excursion detail gallery by identifier.
        /// </summary>
        /// <param name="excDetGalleryId">The exc det gallery identifier.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        ExcursionDetailGallery GetExcursionDetailGalleryById(int excDetGalleryId);
        /// <summary>
        /// Gets the excursion detail gallery.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>IEnumerable ExcursionDetailGallery.</returns>
        IEnumerable<ExcursionDetailGallery> GetExcursionDetailGallery(int excursionDetailId);
        /// <summary>
        /// Updates the excursion det gallery.
        /// </summary>
        /// <param name="excDetGallery">The exc det gallery.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        ExcursionDetailGallery UpdateExcursionDetGallery(ExcursionDetailGallery excDetGallery);
        /// <summary>
        /// Adds the excursion detail gallery.
        /// </summary>
        /// <param name="excDetGallery">The exc det gallery.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        ExcursionDetailGallery AddExcursionDetailGallery(ExcursionDetailGallery excDetGallery);
        /// <summary>
        /// Deletes the excursion detail gallery.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExcursionDetailGallery(int id);

        #endregion Excursion Detail Gallery

        #region Excursion Ingredient Categories
        /// <summary>
        /// Gets all excursion ingredient categories.
        /// </summary>
        /// <returns>IEnumerable ExcursionIngredientCategory.</returns>
        IEnumerable<ExcursionIngredientCategory> GetAllExcursionIngredientCategories();
        /// <summary>
        /// Gets all excursion ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionIngredientCategory.</returns>
        IEnumerable<ExcursionIngredientCategory> GetAllExcursionIngredientCategories(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion ingredient category by identifier.
        /// </summary>
        /// <param name="excIngredientCatId">The exc ingredient cat identifier.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        ExcursionIngredientCategory GetExcursionIngredientCategoryById(int excIngredientCatId);
        /// <summary>
        /// Creates the excursion ingredient category.
        /// </summary>
        /// <param name="excurIngredientCategory">The excur ingredient category.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        ExcursionIngredientCategory CreateExcursionIngredientCategory(ExcursionIngredientCategory excurIngredientCategory);
        /// <summary>
        /// Modifies the excursion ingredient category.
        /// </summary>
        /// <param name="excIngredientCategory">The exc ingredient category.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        ExcursionIngredientCategory ModifyExcursionIngredientCategory(ExcursionIngredientCategory excIngredientCategory);
        /// <summary>
        /// Deletes the excursion ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExcursionIngredientCategory(int id);
        #endregion Excursion Ingredient Categories

        #region Excursion Ingredients
        /// <summary>
        /// Gets the excursion ingredient by exc ing cat identifier.
        /// </summary>
        /// <param name="excIngCatId">The exc ing cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionIngredient.</returns>
        IEnumerable<ExcursionIngredient> GetExcursionIngredientByExcIngCatId(int excIngCatId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion ingredient by identifier.
        /// </summary>
        /// <param name="excIngredientId">The exc ingredient identifier.</param>
        /// <returns>ExcursionIngredient.</returns>
        ExcursionIngredient GetExcursionIngredientById(int excIngredientId);
        /// <summary>
        /// Creates the excursion ingredient.
        /// </summary>
        /// <param name="excIngredient">The exc ingredient.</param>
        /// <returns>ExcursionIngredient.</returns>
        ExcursionIngredient CreateExcursionIngredient(ExcursionIngredient excIngredient);
        /// <summary>
        /// Modifies the excursion ingredient.
        /// </summary>
        /// <param name="excIngredient">The exc ingredient.</param>
        /// <returns>ExcursionIngredient.</returns>
        ExcursionIngredient ModifyExcursionIngredient(ExcursionIngredient excIngredient);
        /// <summary>
        /// Deletes the excursion ingredient.
        /// </summary>
        /// <param name="excIngredientId">The exc ingredient identifier.</param>
        void DeleteExcursionIngredient(int excIngredientId);
        #endregion Excursion Ingredients

        #region Excursion Reviews
        /// <summary>
        /// Creates the excursion review.
        /// </summary>
        /// <param name="excursionReview">The excursion review.</param>
        /// <returns>ExcursionReview.</returns>
        ExcursionReview CreateExcursionReview(ExcursionReview excursionReview);
        /// <summary>
        /// Gets all excursion reviews by exc det identifier.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionReview.</returns>
        IEnumerable<ExcursionReview> GetAllExcursionReviewsByExcDetId(int excursionDetailId, int pageNumber, int pageSize, out int total);
        #endregion Excursion Reviews

        #region Excursion Offer
        /// <summary>
        /// Gets the excursion offer by hotel excursion identifier.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        IEnumerable<ExcursionOffer> GetExcursionOfferByHotelExcursionId(int hotelExcursionId);
        /// <summary>
        /// Gets the excursion offer by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        IEnumerable<ExcursionOffer> GetExcursionOfferByHotelId(int hotelId, int customerId, bool considerMyPreferences = false);

        /// <summary>
        /// Gets the excursion offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        IEnumerable<ExcursionOffer> GetExcursionOffer(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the unassigned offer excursion details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetUnassignedOfferExcursionDetails(int hotelId);
        /// <summary>
        /// Gets the excursion offer by identifier.
        /// </summary>
        /// <param name="excursionOfferId">The excursion offer identifier.</param>
        /// <returns>ExcursionOffer.</returns>
        ExcursionOffer GetExcursionOfferById(int excursionOfferId);
        /// <summary>
        /// Creates the excursion offer.
        /// </summary>
        /// <param name="excursionOffer">The excursion offer.</param>
        /// <returns>ExcursionOffer.</returns>
        ExcursionOffer CreateExcursionOffer(ExcursionOffer excursionOffer);
        /// <summary>
        /// Modifies the excursion offer.
        /// </summary>
        /// <param name="excursionOffer">The excursion offer.</param>
        /// <returns>ExcursionOffer.</returns>
        ExcursionOffer ModifyExcursionOffer(ExcursionOffer excursionOffer);
        /// <summary>
        /// Deletes the excursion offer.
        /// </summary>
        /// <param name="excursionOfferid">The excursion offerid.</param>
        void DeleteExcursionOffer(int excursionOfferid);
        #endregion Excursion Offer

        #region Hotel Excursion Detail Ingredients
        /// <summary>
        /// Gets the excursion detail ingredients.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetailIngredient.</returns>
        IEnumerable<ExcursionDetailIngredient> GetExcursionDetailIngredients(int excursionDetailId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion detail ingredient by identifier.
        /// </summary>
        /// <param name="excursionDetIngrId">The excursion det ingr identifier.</param>
        /// <returns>ExcursionDetailIngredient.</returns>
        ExcursionDetailIngredient GetExcursionDetailIngredientById(int excursionDetIngrId);
        /// <summary>
        /// Gets the unassign excursion ingrdeients.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <returns>IEnumerable ExcursionIngredient.</returns>
        IEnumerable<ExcursionIngredient> GetUnassignExcursionIngrdeients(int excursionDetId);
        /// <summary>
        /// Creates the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetailIngr">The excursion detail ingr.</param>
        void CreateExcursionDetailIngredient(List<ExcursionDetailIngredient> excursionDetailIngr);
        /// <summary>
        /// Deletes the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetIngrId">The excursion det ingr identifier.</param>
        void DeleteExcursionDetailIngredient(int excursionDetIngrId);
        #endregion Hotel Excursion Detail Ingredients

        #region Excursion Suggestion
        /// <summary>
        /// Gets the excursion detail suggestions.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetExcursionDetailSuggestions(int excursionDetailId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion det suggestions.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionSuggestion.</returns>
        IEnumerable<ExcursionSuggestion> GetExcursionDetSuggestions(int excursionDetId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the unassign excursion det suggetions.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetUnassignExcursionDetSuggetions(int excursionDetId, int hotelId);
        /// <summary>
        /// Creates the excursion detail suggestion.
        /// </summary>
        /// <param name="excursionDetSuggestion">The excursion det suggestion.</param>
        void CreateExcursionDetailSuggestion(List<ExcursionSuggestion> excursionDetSuggestion);
        /// <summary>
        /// Deletes the excursion det suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteExcursionDetSuggestion(int id);

        #endregion Excursion Suggestion

        #region Excursion Cart
        /// <summary>
        /// Gets the customer excursion cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionCart.</returns>
        ExcursionCart GetCustomerExcursionCart(int customerId, int hotelId);
        /// <summary>
        /// Creates the excursion cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>ExcursionCart.</returns>
        ExcursionCart CreateExcursionCart(ExcursionCart cart, bool flush = false);
        /// <summary>
        /// Updates the excursion cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>ExcursionCart.</returns>
        ExcursionCart UpdateExcursionCart(int customerId, ExcursionCart cart);
        /// <summary>
        /// Modifies the excursion cart total.
        /// </summary>
        /// <param name="excCart">The exc cart.</param>
        /// <returns>ExcursionCart.</returns>
        ExcursionCart ModifyExcursionCartTotal(ExcursionCart excCart);
        /// <summary>
        /// Gets the excursion cart item by identifier.
        /// </summary>
        /// <param name="excursionCartItemId">The excursion cart item identifier.</param>
        /// <returns>ExcursionCartItems.</returns>
        ExcursionCartItems GetExcursionCartItemById(int excursionCartItemId);
        /// <summary>
        /// Deletes the excursion cart item.
        /// </summary>
        /// <param name="excCartItemId">The exc cart item identifier.</param>
        void DeleteExcursionCartItem(int excCartItemId);
        #endregion Excursion Cart

        #region Excursion Order
        //WEB
        /// <summary>
        /// Gets the excursion orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOrder.</returns>
        IEnumerable<ExcursionOrder> GetExcursionOrdersQueue(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion order item details.
        /// </summary>
        /// <param name="excursionOrderId">The excursion order identifier.</param>
        /// <returns>IEnumerable ExcursionOrderItems.</returns>
        IEnumerable<ExcursionOrderItems> GetExcursionOrderItemDetails(int excursionOrderId);
        /// <summary>
        /// Updates the excursion order item deliver status.
        /// </summary>
        /// <param name="excursionOrderItemId">The excursion order item identifier.</param>
        void UpdateExcursionOrderItemDeliverStatus(int excursionOrderItemId);
        /// <summary>
        /// Modifies the excursion order.
        /// </summary>
        /// <param name="excursionOrder">The excursion order.</param>
        /// <returns>ExcursionOrder.</returns>
        ExcursionOrder ModifyExcursionOrder(ExcursionOrder excursionOrder);
        /// <summary>
        /// Gets the excursion order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOrder.</returns>
        IEnumerable<ExcursionOrder> GetExcursionOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the excursion order is late status changed count.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>System.Int32.</returns>
        int GetExcursionOrderIsLateStatusChangedCount(int hotelId);

        //API
        /// <summary>
        /// Gets the excursion orders details by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>ExcursionOrder.</returns>
        ExcursionOrder GetExcursionOrdersDetailsById(int orderId);
        /// <summary>
        /// Creates the excursion order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionOrder.</returns>
        ExcursionOrder CreateExcursionOrderFromCart(int customerId, int cardId, int hotelId);
        /// <summary>
        /// Gets the customer active excursion orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveExcursionOrdersAndBaskets.</returns>
        CustomerActiveExcursionOrdersAndBaskets GetCustomerActiveExcursionOrders(int customerId);

        /// <summary>
        /// Creates the excursion order review.
        /// </summary>
        /// <param name="excursionOrderReview">The excursion order review.</param>
        /// <returns>ExcursionOrderReview.</returns>
        ExcursionOrderReview CreateExcursionOrderReview(ExcursionOrderReview excursionOrderReview);
        /// <summary>
        /// Gets the excursion order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable ExcursionOrderReview.</returns>
        IEnumerable<ExcursionOrderReview> GetExcursionOrderReviews(int id);

        /// <summary>
        /// Gets the customer excursion orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable excursionOrder.</returns>
        IEnumerable<ExcursionOrder> GetCustomerExcursionOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total);

        #endregion Excursion Order

        #region Customer HotelRoom
        /// <summary>
        /// Creates the customer hotel rooom.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>CustomerHotelRoom.</returns>
        CustomerHotelRoom CreateCustomerHotelRooom(int customerId, int roomId);
        /// <summary>
        /// Gets the customer rooom details by identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerHotelRoom.</returns>
        CustomerHotelRoom GetCustomerRooomDetailsById(int hotelId, int customerId);
        /// <summary>
        /// Modifies the customer hotel rooom.
        /// </summary>
        /// <param name="customerHotelRoomId">The customer hotel room identifier.</param>
        void ModifyCustomerHotelRooom(int customerHotelRoomId);

        #endregion Customer HotelRoom

        #region Housekeeping Cart

        /// <summary>
        /// Gets the customer housekeeping cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>HousekeepingCart.</returns>
        HousekeepingCart GetCustomerHousekeepingCart(int customerId, int hotelId);
        /// <summary>
        /// Creates the housekeeping cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>HousekeepingCart.</returns>
        HousekeepingCart CreateHousekeepingCart(HousekeepingCart cart, bool flush = false);
        /// <summary>
        /// Updates the housekeeping cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>HousekeepingCart.</returns>
        HousekeepingCart UpdateHousekeepingCart(int customerId, HousekeepingCart cart);

        #endregion Housekeeping Cart

        #region Housekeeping Order

        //Api
        /// <summary>
        /// Gets the housekeeping order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>HousekeepingOrder.</returns>
        HousekeepingOrder GetHousekeepingOrderById(int orderId);
        /// <summary>
        /// Creates the housekeeping order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>HousekeepingOrder.</returns>
        HousekeepingOrder CreateHousekeepingOrderFromCart(int customerId, int cardId, int roomId);
        /// <summary>
        /// Gets the customer active housekeeping orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveHousekeepingOrdersAndBaskets.</returns>
        CustomerActiveHousekeepingOrdersAndBaskets GetCustomerActiveHousekeepingOrders(int customerId);

        // Web
        /// <summary>
        /// Gets the housekeeping orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingOrder.</returns>
        IEnumerable<HousekeepingOrder> GetHousekeepingOrdersQueue(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Modifies the housekeeping order.
        /// </summary>
        /// <param name="housekeepingOrder">The housekeeping order.</param>
        /// <returns>HousekeepingOrder.</returns>
        HousekeepingOrder ModifyHousekeepingOrder(HousekeepingOrder housekeepingOrder);
        /// <summary>
        /// Gets the housekeeping order item details.
        /// </summary>
        /// <param name="housekeepingOrderId">The housekeeping order identifier.</param>
        /// <returns>IEnumerable HousekeepingOrderItem.</returns>
        IEnumerable<HousekeepingOrderItem> GetHousekeepingOrderItemDetails(int housekeepingOrderId);
        /// <summary>
        /// Updates the housekeeping order item deliver status.
        /// </summary>
        /// <param name="housekeepingOrderItemId">The housekeeping order item identifier.</param>
        void UpdateHousekeepingOrderItemDeliverStatus(int housekeepingOrderItemId);
        /// <summary>
        /// Gets the housekeeping order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingOrder.</returns>
        IEnumerable<HousekeepingOrder> GetHousekeepingOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total);

        /// <summary>
        /// Gets the customer housekeeping orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable housekeepingOrder.</returns>
        IEnumerable<HousekeepingOrder> GetCustomerHousekeepingOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total);
        #endregion Housekeeping Order

        #region Manage HotelRoom Customers

        /// <summary>
        /// Gets the rooms active customers.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelRoom.</returns>
        IEnumerable<CustomerHotelRoom> GetRoomsActiveCustomers(int hotelId, int skip, int pageSize, out int total);

        #endregion Manage HotelRoom Customers

        #region HotelRoom WakeUp Call

        /// <summary>
        /// Gets the hotel room wake up call.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable WakeUp.</returns>
        IEnumerable<WakeUp> GetHotelRoomWakeUpCall(int hotelId, int skip, int pageSize, out int total);

        /// <summary>
        /// Modifies the wake up call.
        /// </summary>
        /// <param name="WakeUpId">The wake up identifier.</param>
        void ModifyWakeUpCall(int WakeUpId);

        #endregion HotelRoom WakeUp Call

        #region Trip Service
        //Trip service
        /// <summary>
        /// Gets the hotel trips.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Trip.</returns>
        IEnumerable<Trip> GetHotelTrips(int HotelId);
        /// <summary>
        /// Gets all hotel trips.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="HotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Trip.</returns>
        IEnumerable<Trip> GetAllHotelTrips(int HotelId, int HotelServiceId);
        /// <summary>
        /// Creates the hotel trip.
        /// </summary>
        /// <param name="hotel_trip">The hotel trip.</param>
        /// <returns>Trip.</returns>
        Trip CreateHotelTrip(Trip hotel_trip);
        /// <summary>
        /// Modifies the hotel trip.
        /// </summary>
        /// <param name="Trip">The trip.</param>
        /// <returns>Trip.</returns>
        Trip ModifyHotelTrip(Trip Trip);
        /// <summary>
        /// Gets the hotel trip by identifier.
        /// </summary>
        /// <param name="TripId">The trip identifier.</param>
        /// <returns>Trip.</returns>
        Trip GetHotelTripByID(int TripId);
        /// <summary>
        /// Deletes the trip.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteTrip(int id);

        /// <summary>
        /// Gets the hotel trip details.
        /// </summary>
        /// <param name="Trip_ID">The trip identifier.</param>
        /// <returns>IEnumerable TripDetail.</returns>
        IEnumerable<TripDetail> GetHotelTripDetails(int Trip_ID);
        /// <summary>
        /// Creates the hotel trip details.
        /// </summary>
        /// <param name="hotel_trip_details">The hotel trip details.</param>
        /// <returns>TripDetail.</returns>
        TripDetail CreateHotelTripDetails(TripDetail hotel_trip_details);
        /// <summary>
        /// Modifies the hotel trip details.
        /// </summary>
        /// <param name="Trip_Details">The trip details.</param>
        /// <returns>TripDetail.</returns>
        TripDetail ModifyHotelTripDetails(TripDetail Trip_Details);
        /// <summary>
        /// Gets the hotel trip details by identifier.
        /// </summary>
        /// <param name="Trip_Details_ID">The trip details identifier.</param>
        /// <returns>TripDetail.</returns>
        TripDetail GetHotelTripDetailsByID(int Trip_Details_ID);
        /// <summary>
        /// Deletes the trip details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteTripDetails(int id);

        /// <summary>
        /// Gets the trip bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripBooking.</returns>
        IEnumerable<TripBooking> GetTripBookings(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Deletes the trip booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteTripBooking(int id);
        /// <summary>
        /// Gets the hotel trip booking by identifier.
        /// </summary>
        /// <param name="booking_id">The booking identifier.</param>
        /// <returns>TripBooking.</returns>
        TripBooking GetHotelTripBookingByID(int booking_id);
        /// <summary>
        /// Gets all trip booking of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable TripBooking.</returns>
        IEnumerable<TripBooking> GetAllTripBookingOfCustomer(int customerId);
        /// <summary>
        /// Modifies the hotel trip booking.
        /// </summary>
        /// <param name="tripBooking">The trip booking.</param>
        /// <returns>TripBooking.</returns>
        TripBooking ModifyHotelTripBooking(TripBooking tripBooking);
        /// <summary>
        /// Creates the hotel trip bookings.
        /// </summary>
        /// <param name="tripBooking">The trip booking.</param>
        /// <returns>TripBooking.</returns>
        TripBooking CreateHotelTripBookings(TripBooking tripBooking);

        /// <summary>
        /// Gets the trip reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripReview.</returns>
        IEnumerable<TripReview> GetTripReviews(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the trips reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripReview.</returns>
        IEnumerable<TripReview> GetTripsReviewsByHotel(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the trip reviews.
        /// </summary>
        /// <param name="tripReviews">The trip reviews.</param>
        /// <returns>TripReview.</returns>
        TripReview CreateTripReviews(TripReview tripReviews);
        /// <summary>
        /// Gets the trip review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TripReview.</returns>
        TripReview GetTripReviewById(int id);
        /// <summary>
        /// Deletes the trip review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteTripReview(int id);
        /// <summary>
        /// Gets the pending trip service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Trip.</returns>
        IEnumerable<Trip> GetPendingTripServiceReviews(int customerId, int pageNumber, int pageSize, out int total);
        #endregion Trip Service

        #region Taxi Services
        //TaxiServices
        /// <summary>
        /// Gets the hotel taxi details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        IEnumerable<TaxiDetail> GetHotelTaxiDetails(int hotelId);
        /// <summary>
        /// Gets the hotel taxi details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        IEnumerable<TaxiDetail> GetHotelTaxiDetails(int hotelId, int hotelServiceId);
        /// <summary>
        /// Gets the hotel taxi detail by identifier.
        /// </summary>
        /// <param name="taxiId">The taxi identifier.</param>
        /// <returns>TaxiDetail.</returns>
        TaxiDetail GetHotelTaxiDetailById(int taxiId);
        /// <summary>
        /// Creates the hotel taxi.
        /// </summary>
        /// <param name="taxi">The taxi.</param>
        /// <returns>TaxiDetail.</returns>
        TaxiDetail CreateHotelTaxi(TaxiDetail taxi);
        /// <summary>
        /// Modifies the hotel taxi details.
        /// </summary>
        /// <param name="taxiDetails">The taxi details.</param>
        /// <returns>TaxiDetail.</returns>
        TaxiDetail ModifyHotelTaxiDetails(TaxiDetail taxiDetails);
        /// <summary>
        /// Deletes the taxi details.
        /// </summary>
        /// <param name="taxiId">The taxi identifier.</param>
        void DeleteTaxiDetails(int taxiId);

        //Taxi Destinations
        /// <summary>
        /// Gets the hotel taxi destination.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable TaxiDestination.</returns>
        IEnumerable<TaxiDestination> GetHotelTaxiDestination(int hotelId);
        /// <summary>
        /// Gets all hotel taxi destination.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiDestination.</returns>
        IEnumerable<TaxiDestination> GetAllHotelTaxiDestination(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the taxi destination by identifier.
        /// </summary>
        /// <param name="taxiDestId">The taxi dest identifier.</param>
        /// <returns>TaxiDestination.</returns>
        TaxiDestination GetTaxiDestinationById(int taxiDestId);
        /// <summary>
        /// Creates the taxi destination.
        /// </summary>
        /// <param name="taxiDest">The taxi dest.</param>
        /// <returns>TaxiDestination.</returns>
        TaxiDestination CreateTaxiDestination(TaxiDestination taxiDest);
        /// <summary>
        /// Modifies the taxi destination.
        /// </summary>
        /// <param name="taxiDest">The taxi dest.</param>
        /// <returns>TaxiDestination.</returns>
        TaxiDestination ModifyTaxiDestination(TaxiDestination taxiDest);
        /// <summary>
        /// Deletes the taxi destination.
        /// </summary>
        /// <param name="taxiDestId">The taxi dest identifier.</param>
        void DeleteTaxiDestination(int taxiDestId);

        /// <summary>
        /// Creates the hotel taxi booking.
        /// </summary>
        /// <param name="taxiBooking">The taxi booking.</param>
        /// <returns>TaxiBooking.</returns>
        TaxiBooking CreateHotelTaxiBooking(TaxiBooking taxiBooking);
        /// <summary>
        /// Updates the taxi booking status.
        /// </summary>
        /// <param name="taxiBooking">The taxi booking.</param>
        /// <returns>TaxiBooking.</returns>
        TaxiBooking UpdateTaxiBookingStatus(TaxiBooking taxiBooking);
        /// <summary>
        /// Schedules the taxi booking.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="taxiBookingId">The taxi booking identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>TaxiBooking.</returns>
        TaxiBooking ScheduleTaxiBooking(int customerId, int taxiBookingId, DateTime scheduleDate);
        /// <summary>
        /// Gets the hotel taxi booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TaxiBooking.</returns>
        TaxiBooking GetHotelTaxiBooking(int id);
        /// <summary>
        /// Gets all taxi booking by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable TaxiBooking .</returns>
        IEnumerable<TaxiBooking> GetAllTaxiBookingByCustomer(int customerId);
        /// <summary>
        /// Gets all active taxi booking of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiBooking.</returns>
        IEnumerable<TaxiBooking> GetAllActiveTaxiBookingOfCustomer(int customerId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the taxi bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiBooking.</returns>
        IEnumerable<TaxiBooking> GetTaxiBookings(int hotelId, int skip, int pageSize, out int total);
        //void DeleteTaxiBooking(int id);
        //TaxiBooking ModifyTaxiBooking(TaxiBooking taxiBooking);

        //Taxi Booking Reviews
        /// <summary>
        /// Creates the taxi booking review.
        /// </summary>
        /// <param name="taxiBookingReview">The taxi booking review.</param>
        /// <returns>TaxiBookingReview.</returns>
        TaxiBookingReview CreateTaxiBookingReview(TaxiBookingReview taxiBookingReview);
        /// <summary>
        /// Gets the taxi booking review by booking identifier.
        /// </summary>
        /// <param name="taxiBookingId">The taxi booking identifier.</param>
        /// <returns>IEnumerable TaxiBookingReview.</returns>
        IEnumerable<TaxiBookingReview> GetTaxiBookingReviewByBookingId(int taxiBookingId);

        /// <summary>
        /// Gets the taxi reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiReview.</returns>
        IEnumerable<TaxiReview> GetTaxiReviews(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the taxi reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiReview.</returns>
        IEnumerable<TaxiReview> GetTaxiReviewsByHotel(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the taxi review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TaxiReview.</returns>
        TaxiReview GetTaxiReviewById(int id);
        /// <summary>
        /// Creates the taxi reviews.
        /// </summary>
        /// <param name="taxiReviews">The taxi reviews.</param>
        /// <returns>TaxiReview.</returns>
        TaxiReview CreateTaxiReviews(TaxiReview taxiReviews);

        /// <summary>
        /// Deletes the taxi review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteTaxiReview(int id);
        /// <summary>
        /// Gets the pending taxi service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        IEnumerable<TaxiDetail> GetPendingTaxiServiceReviews(int customerId, int pageNumber, int pageSize, out int total);
        #endregion Taxi Services

        #region Room types
        //Room types
        /// <summary>
        /// Gets all room types.
        /// </summary>
        /// <returns>IEnumerable RoomType.</returns>
        IEnumerable<RoomType> GetAllRoomTypes();
        /// <summary>
        /// Gets the room type by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RoomType.</returns>
        RoomType GetRoomTypeById(int id);
        /// <summary>
        /// Creates the type of the room.
        /// </summary>
        /// <param name="roomType">Type of the room.</param>
        /// <returns>RoomType.</returns>
        RoomType CreateRoomType(RoomType roomType);
        /// <summary>
        /// Modifies the type of the room.
        /// </summary>
        /// <param name="roomType">Type of the room.</param>
        /// <returns>RoomType.</returns>
        RoomType ModifyRoomType(RoomType roomType);
        /// <summary>
        /// Deletes the type of the room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRoomType(int id);
        #endregion Room types

        #region Hotel Rooms
        //Hotel Rooms
        /// <summary>
        /// Gets all hotel roooms by identifier.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns>Room.</returns>
        Room GetAllHotelRooomsById(int HotelId, string roomNumber);
        /// <summary>
        /// Gets the rooom details by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>Room.</returns>
        Room GetRooomDetailsByRoomId(int RoomId);
        /// <summary>
        /// Creates the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>Room.</returns>
        Room CreateHotelRoom(Room room);
        /// <summary>
        /// Gets the hotel room.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="RoomNumber">The room number.</param>
        /// <returns>IEnumerable Room.</returns>
        IEnumerable<Room> GetHotelRoom(int HotelId, string RoomNumber);
        /// <summary>
        /// Updates the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>Room.</returns>
        Room UpdateHotelRoom(Room room);
        /// <summary>
        /// Deletes the hotel room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotelRoom(int id);
        /// <summary>
        /// Gets the todays room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetTodaysRoomBookings(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all hotel roooms by hotel identifier.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Room.</returns>
        IEnumerable<Room> GetAllHotelRooomsByHotelId(int HotelId);
        /// <summary>
        /// Gets the hotel room by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>Room.</returns>
        Room GetHotelRoomByRoomId(int RoomId);
        #endregion Hotel Rooms

        #region Room Rate
        //Room Rate
        /// <summary>
        /// Creates the hotel room rate.
        /// </summary>
        /// <param name="roomRate">The room rate.</param>
        /// <returns>RoomRate.</returns>
        RoomRate CreateHotelRoomRate(RoomRate roomRate);
        /// <summary>
        /// Gets the room rates by room identifier.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>RoomRate.</returns>
        RoomRate GetRoomRatesByRoomId(int roomId);
        /// <summary>
        /// Modifies the room rate.
        /// </summary>
        /// <param name="roomRate">The room rate.</param>
        /// <returns>RoomRate.</returns>
        RoomRate ModifyRoomRate(RoomRate roomRate);
        #endregion Room Rate

        #region Room categories
        //Room categories
        /// <summary>
        /// Gets all room categories.
        /// </summary>
        /// <returns>IEnumerable RoomCategory.</returns>
        IEnumerable<RoomCategory> GetAllRoomCategories();
        /// <summary>
        /// Gets all room categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomCategory.</returns>
        IEnumerable<RoomCategory> GetAllRoomCategories(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the room category by identifier.
        /// </summary>
        /// <param name="roomCatId">The room cat identifier.</param>
        /// <returns>RoomCategory.</returns>
        RoomCategory GetRoomCategoryById(int roomCatId);
        /// <summary>
        /// Creates the room category.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <returns>RoomCategory.</returns>
        RoomCategory CreateRoomCategory(RoomCategory roomCategory);
        /// <summary>
        /// Modifies the room category.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <returns>RoomCategory.</returns>
        RoomCategory ModifyRoomCategory(RoomCategory roomCategory);
        /// <summary>
        /// Deletes the room category.
        /// </summary>
        /// <param name="roomCatId">The room cat identifier.</param>
        void DeleteRoomCategory(int roomCatId);
        #endregion Room categories

        #region Room Capacity
        //Room Capacity
        /// <summary>
        /// Gets all room capacities.
        /// </summary>
        /// <returns>IEnumerable RoomCapacity.</returns>
        IEnumerable<RoomCapacity> GetAllRoomCapacities();
        /// <summary>
        /// Gets all room capacities.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomCapacity.</returns>
        IEnumerable<RoomCapacity> GetAllRoomCapacities(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the room capacity by identifier.
        /// </summary>
        /// <param name="roomCapId">The room cap identifier.</param>
        /// <returns>RoomCapacity.</returns>
        RoomCapacity GetRoomCapacityById(int roomCapId);
        /// <summary>
        /// Creates the room capacity.
        /// </summary>
        /// <param name="roomCapacity">The room capacity.</param>
        /// <returns>RoomCapacity.</returns>
        RoomCapacity CreateRoomCapacity(RoomCapacity roomCapacity);
        /// <summary>
        /// Modifies the room capacity.
        /// </summary>
        /// <param name="roomCapacity">The room capacity.</param>
        /// <returns>RoomCapacity.</returns>
        RoomCapacity ModifyRoomCapacity(RoomCapacity roomCapacity);
        /// <summary>
        /// Deletes the room capacity.
        /// </summary>
        /// <param name="roomCapId">The room cap identifier.</param>
        void DeleteRoomCapacity(int roomCapId);
        #endregion Room Capacity

        #region Room Amenities
        //Room Amenities
        /// <summary>
        /// Gets all room amenities.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomAmenities.</returns>
        IEnumerable<RoomAmenities> GetAllRoomAmenities(int roomId, int skip, int pageSize, out int total);
        /// <summary>
        /// Deletes the room amenities.
        /// </summary>
        /// <param name="roomAmenitiesId">The room amenities identifier.</param>
        void DeleteRoomAmenities(int roomAmenitiesId);
        /// <summary>
        /// Gets the unassign amenities.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>IEnumerable Amenities.</returns>
        IEnumerable<Amenities> GetUnassignAmenities(int roomId);
        /// <summary>
        /// Creates the room amenities.
        /// </summary>
        /// <param name="roomAmenities">The room amenities.</param>
        void CreateRoomAmenities(List<RoomAmenities> roomAmenities);
        #endregion Room Amenities

        #region Room Images
        //Room Images
        /// <summary>
        /// Gets all room images.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>IEnumerable HotelRoomImages.</returns>
        IEnumerable<HotelRoomImages> GetAllRoomImages(int roomId);
        /// <summary>
        /// Gets the room image by image identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>HotelRoomImages.</returns>
        HotelRoomImages GetRoomImageByImageId(int Id);
        /// <summary>
        /// Adds the room image.
        /// </summary>
        /// <param name="roomImage">The room image.</param>
        /// <returns>HotelRoomImages.</returns>
        HotelRoomImages AddRoomImage(HotelRoomImages roomImage);
        /// <summary>
        /// Updates the room image.
        /// </summary>
        /// <param name="roomImage">The room image.</param>
        /// <returns>HotelRoomImages.</returns>
        HotelRoomImages UpdateRoomImage(HotelRoomImages roomImage);
        /// <summary>
        /// Deletes the room image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRoomImage(int id);
        #endregion Room Images

        //Room Reviews
        /// <summary>
        /// Gets the reviews by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>IEnumerable RoomReview.</returns>
        IEnumerable<RoomReview> GetReviewsByRoomId(int RoomId);
        /// <summary>
        /// Creates the room review.
        /// </summary>
        /// <param name="review">The review.</param>
        /// <returns>RoomReview.</returns>
        RoomReview CreateRoomReview(RoomReview review);

        //Hotel Booking
        /// <summary>
        /// Creates the hotel bookings.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>CustomerHotelBooking.</returns>
        CustomerHotelBooking CreateHotelBookings(CustomerHotelBooking hotelBooking);
        /// <summary>
        /// Gets the hotel room booking by identifier.
        /// </summary>
        /// <param name="bookingId">The booking identifier.</param>
        /// <returns>CustomerHotelBooking.</returns>
        CustomerHotelBooking GetHotelRoomBookingByID(int bookingId);
        /// <summary>
        /// Modifies the hotel bookings.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>CustomerHotelBooking.</returns>
        CustomerHotelBooking ModifyHotelBookings(CustomerHotelBooking hotelBooking);
        /// <summary>
        /// Gets the todays booking.
        /// </summary>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetTodaysBooking();
        /// <summary>
        /// Gets all hotel room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetAllHotelRoomBookings(int hotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all current room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetAllCurrentRoomBookings(int hotelId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Deletes the room booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRoomBooking(int id);
        /// <summary>
        /// Gets all room booking by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetAllRoomBookingByCustomer(int customerId);

        //PendingReviews
        /// <summary>
        /// Gets the pending reviews hotels.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetPendingReviewsHotels(int customerId, int pageNumber, int pageSize, out int total);

        //hotel spa_service Reviews
        /// <summary>
        /// Gets the spa service reviews by hotel identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        IEnumerable<SpaServiceReview> GetSpaServiceReviewsByHotelId(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the hotel spa service reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        IEnumerable<SpaServiceReview> GetHotelSpaServiceReviews(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all hotel spa service reviews.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        IEnumerable<SpaServiceReview> GetAllHotelSpaServiceReviews(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the customer spa service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        IEnumerable<SpaServiceReview> GetCustomerSpaServiceReviews(int customerId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Creates the hotel spa service review.
        /// </summary>
        /// <param name="hotelSpaServiceReviews">The hotel spa service reviews.</param>
        /// <returns>SpaServiceReview.</returns>
        SpaServiceReview CreateHotelSpaServiceReview(SpaServiceReview hotelSpaServiceReviews);
        /// <summary>
        /// Gets the hotel spa service review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaServiceReview.</returns>
        SpaServiceReview GetHotelSpaServiceReviewById(int id);
        /// <summary>
        /// Deletes the hotel spa service review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHotelSpaServiceReview(int id);
        //IEnumerable<SPAServiceDetail> GetPendingSPAServiceReviews(int customerId, int pageNumber, int pageSize, out int total);


        /// <summary>
        /// Gets the customer hotel reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetCustomerHotelReviews(int customerId, int pageNumber, int pageSize, out int total);

        //Customer active bookings in hotel
        /// <summary>
        /// Gets the customer active bookings in hotel.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveBookingsInHotel.</returns>
        CustomerActiveBookingsInHotel GetCustomerActiveBookingsInHotel(int customerId);

        /// <summary>
        /// Gets the un assign chain hotel.
        /// </summary>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetUnAssignChainHotel();

        /// <summary>
        /// Gets the assign chain hotel.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetAssignChainHotel(int id);
        /// <summary>
        /// Gets the chain recent hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> GetChainRecentHotels(int id);
        /// <summary>
        /// Gets the chain hotel recent orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        IEnumerable<CustomerHotelBooking> GetChainHotelRecentOrders(int customerId, int skip, int pageSize, out int total);

        #region Spa Employee
        /// <summary>
        /// Gets all spa employee.
        /// </summary>
        /// <returns>IEnumerable SpaEmployee.</returns>
        IEnumerable<SpaEmployee> GetAllSpaEmployee();
        /// <summary>
        /// Gets all spa employee.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployee.</returns>
        IEnumerable<SpaEmployee> GetAllSpaEmployee(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the spa employee.
        /// </summary>
        /// <param name="spaEmployee">The spa employee.</param>
        /// <returns>SpaEmployee.</returns>
        SpaEmployee CreateSpaEmployee(SpaEmployee spaEmployee);
        /// <summary>
        /// Modifies the spa employee.
        /// </summary>
        /// <param name="spaEmployee">The spa employee.</param>
        /// <returns>SpaEmployee.</returns>
        SpaEmployee ModifySpaEmployee(SpaEmployee spaEmployee);
        /// <summary>
        /// Gets the spa employee by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaEmployee.</returns>
        SpaEmployee GetSpaEmployeeById(int id);
        /// <summary>
        /// Deletes the spa employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaEmployee(int id);
        #endregion Spa Employee

        #region Spa Employee Details
        //spa employee details
        /// <summary>
        /// Gets the spa employee details by spa detail identifier.
        /// </summary>
        /// <param name="spaDetailId">The spa detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="gender">The gender.</param>
        /// <returns>IEnumerable SpaEmployeeDetails.</returns>
        IEnumerable<SpaEmployeeDetails> GetSpaEmployeeDetailsBySpaDetailId(int spaDetailId, int pageNumber, int pageSize, out int total, Gender gender = Gender.None);
        /// <summary>
        /// Gets the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployeeDetails.</returns>
        IEnumerable<SpaEmployeeDetails> GetSpaEmployeeDetails(int spaEmployeeId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa employee details time slot by identifier.
        /// </summary>
        /// <param name="spaEmployeeDetailsId">The spa employee details identifier.</param>
        /// <param name="date">The date.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="extratimeId">The extratime identifier.</param>
        /// <returns>IEnumerable SpaEmployeeAvailableTimeSlot.</returns>
        IEnumerable<SpaEmployeeAvailableTimeSlot> GetSpaEmployeeDetailsTimeSlotById(int spaEmployeeDetailsId, DateTime date, int[] Ids, int? extratimeId = null);
        /// <summary>
        /// Gets the spa employee details by identifier.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>SpaEmployeeDetails.</returns>
        SpaEmployeeDetails GetSpaEmployeeDetailsById(int spaEmployeeId);
        /// <summary>
        /// Deletes the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeDetId">The spa employee det identifier.</param>
        void DeleteSpaEmployeeDetails(int spaEmployeeDetId);
        /// <summary>
        /// Gets the unassign spa service details.
        /// </summary>
        /// <param name="spaEmpId">The spa emp identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetUnassignSpaServiceDetails(int spaEmpId, int hotelId);
        /// <summary>
        /// Creates the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeDetails">The spa employee details.</param>
        void CreateSpaEmployeeDetails(List<SpaEmployeeDetails> spaEmployeeDetails);

        /// <summary>
        /// Gets the spa employee time slots.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployeeTimeSlot.</returns>
        IEnumerable<SpaEmployeeTimeSlot> GetSpaEmployeeTimeSlots(int spaEmployeeId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa empl time slot calendar.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>IEnumerable SpaEmployeeTimeSlot.</returns>
        IEnumerable<SpaEmployeeTimeSlot> GetSpaEmplTimeSlotCalendar(int spaEmployeeId);
        /// <summary>
        /// Creates the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeTimeSlot">The spa employee time slot.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        SpaEmployeeTimeSlot CreateSpaEmployeeTimeSlot(SpaEmployeeTimeSlot spaEmployeeTimeSlot);
        /// <summary>
        /// Modifies the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeTimeSlot">The spa employee time slot.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        SpaEmployeeTimeSlot ModifySpaEmployeeTimeSlot(SpaEmployeeTimeSlot spaEmployeeTimeSlot);
        /// <summary>
        /// Gets the spa employee time slot by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        SpaEmployeeTimeSlot GetSpaEmployeeTimeSlotById(int id);
        /// <summary>
        /// Deletes the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteSpaEmployeeTimeSlot(int id);

        /// <summary>
        /// Gets the customer for spa.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetCustomerForSpa(int hotelId);
        /// <summary>
        /// Gets all spa addtioanl group.
        /// </summary>
        /// <returns>IEnumerable SpaAdditionalGroup.</returns>
        IEnumerable<SpaAdditionalGroup> GetAllSpaAddtioanlGroup();
        #endregion Spa Employee Details

        #region Spa Room
        //Spa Room
        /// <summary>
        /// Gets all spa rooms.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaRoom.</returns>
        IEnumerable<SpaRoom> GetAllSpaRooms(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the spa room by identifier.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <returns>SpaRoom.</returns>
        SpaRoom GetSpaRoomById(int spaRoomId);
        /// <summary>
        /// Gets the spa room.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns>SpaRoom.</returns>
        SpaRoom GetSpaRoom(int hotelId, int roomNumber);
        /// <summary>
        /// Creates the spa room.
        /// </summary>
        /// <param name="spaRoom">The spa room.</param>
        /// <returns>SpaRoom.</returns>
        SpaRoom CreateSpaRoom(SpaRoom spaRoom);
        /// <summary>
        /// Modifies the spa room.
        /// </summary>
        /// <param name="spaRoom">The spa room.</param>
        /// <returns>SpaRoom.</returns>
        SpaRoom ModifySpaRoom(SpaRoom spaRoom);
        /// <summary>
        /// Deletes the spa room.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        void DeleteSpaRoom(int spaRoomId);
        #endregion Spa Room

        #region House Keeping Facilities
        //House Keeping Facilities
        /// <summary>
        /// Gets all housekeeping facilities.
        /// </summary>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        IEnumerable<HouseKeepingFacility> GetAllHousekeepingFacilities();
        /// <summary>
        /// Gets the housekeeping by hotel service identifier.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        IEnumerable<HouseKeepingFacility> GetHousekeepingByHotelServiceId(int hotelServiceId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the housekeeping facilities.
        /// </summary>
        /// <param name="housekeepingFacilities">The housekeeping facilities.</param>
        /// <returns>HouseKeepingFacility.</returns>
        HouseKeepingFacility CreateHousekeepingFacilities(HouseKeepingFacility housekeepingFacilities);
        /// <summary>
        /// Modifies the housekeeping facilities.
        /// </summary>
        /// <param name="housekeepingFacilities">The housekeeping facilities.</param>
        /// <returns>HouseKeepingFacility.</returns>
        HouseKeepingFacility ModifyHousekeepingFacilities(HouseKeepingFacility housekeepingFacilities);
        /// <summary>
        /// Gets the housekeeping facilities by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingFacility.</returns>
        HouseKeepingFacility GetHousekeepingFacilitiesById(int id);
        /// <summary>
        /// Deletes the housekeeping facilitie.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHousekeepingFacilitie(int id);
        #endregion House Keeping Facilities

        #region House Keeping Facility Details
        //House Keeping Facility Details
        /// <summary>
        /// Gets the housekeeping fac details by hf identifier.
        /// </summary>
        /// <param name="housekeepingFacId">The housekeeping fac identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingFacilityDetail.</returns>
        IEnumerable<HouseKeepingFacilityDetail> GetHousekeepingFacDetailsByHFId(int housekeepingFacId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the housekeeping fac detail.
        /// </summary>
        /// <param name="hKeepingFacilityDetail">The h keeping facility detail.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        HouseKeepingFacilityDetail CreateHousekeepingFacDetail(HouseKeepingFacilityDetail hKeepingFacilityDetail);
        /// <summary>
        /// Modifies the housekeeping fac detail.
        /// </summary>
        /// <param name="hKeepingFacilityDetail">The h keeping facility detail.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        HouseKeepingFacilityDetail ModifyHousekeepingFacDetail(HouseKeepingFacilityDetail hKeepingFacilityDetail);
        /// <summary>
        /// Gets the housekeeping fac detail by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        HouseKeepingFacilityDetail GetHousekeepingFacDetailById(int id);
        /// <summary>
        /// Deletes the housekeeping fac detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHousekeepingFacDetail(int id);
        #endregion House Keeping Facility Details

        #region Housekeeping Additional groups
        //Housekeeping additionals groups
        /// <summary>
        /// Gets all un assign housekeeping additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>IEnumerable HousekeepingAdditionalGroup.</returns>
        IEnumerable<HousekeepingAdditionalGroup> GetAllUnAssignHousekeepingAdditionalGroups(int hotelId, int houseKeepingFacilityId);
        /// <summary>
        /// Gets the housekeeping additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingAdditionalGroup.</returns>
        IEnumerable<HousekeepingAdditionalGroup> GetHousekeepingAdditionalGroups(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the housekeeping additional group by identifier.
        /// </summary>
        /// <param name="housekeepingAddGroupId">The housekeeping add group identifier.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        HousekeepingAdditionalGroup GetHousekeepingAdditionalGroupById(int housekeepingAddGroupId);
        /// <summary>
        /// Creates the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGroup">The housekeeping add group.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        HousekeepingAdditionalGroup CreateHousekeepingAdditionalGroup(HousekeepingAdditionalGroup housekeepingAddGroup);
        /// <summary>
        /// Modifies the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGroup">The housekeeping add group.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        HousekeepingAdditionalGroup ModifyHousekeepingAdditionalGroup(HousekeepingAdditionalGroup housekeepingAddGroup);
        /// <summary>
        /// Deletes the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGrpId">The housekeeping add GRP identifier.</param>
        void DeleteHousekeepingAdditionalGroup(int housekeepingAddGrpId);
        #endregion Housekeeping Additional groups

        #region Housekeeping Additional elements
        //Housekeeping Additional elements
        /// <summary>
        /// Gets the housekeeping additional elements.
        /// </summary>
        /// <param name="housekeepingAddGrpId">The housekeeping add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingAdditionalElement.</returns>
        IEnumerable<HousekeepingAdditionalElement> GetHousekeepingAdditionalElements(int housekeepingAddGrpId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the housekeeping additional element by identifier.
        /// </summary>
        /// <param name="housekeepingAddElementId">The housekeeping add element identifier.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        HousekeepingAdditionalElement GetHousekeepingAdditionalElementById(int housekeepingAddElementId);
        /// <summary>
        /// Creates the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElement">The housekeeping add element.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        HousekeepingAdditionalElement CreateHousekeepingAdditionalElement(HousekeepingAdditionalElement housekeepingAddElement);
        /// <summary>
        /// Modifies the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElement">The housekeeping add element.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        HousekeepingAdditionalElement ModifyHousekeepingAdditionalElement(HousekeepingAdditionalElement housekeepingAddElement);
        /// <summary>
        /// Deletes the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElementId">The housekeeping add element identifier.</param>
        void DeleteHousekeepingAdditionalElement(int housekeepingAddElementId);
        #endregion Housekeeping Additional elements

        #region Housekeeping facilities Additionals
        //Housekeeping facilities Additionals
        /// <summary>
        /// Gets all housekeeping fac additionals.
        /// </summary>
        /// <returns>IEnumerable HouseKeepingfacilitiesAdditional.</returns>
        IEnumerable<HouseKeepingfacilitiesAdditional> GetAllHousekeepingFacAdditionals();
        /// <summary>
        /// Gets the housekeeping fac additional by HKF identifier.
        /// </summary>
        /// <param name="housekeepingFacilityId">The housekeeping facility identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingfacilitiesAdditional.</returns>
        IEnumerable<HouseKeepingfacilitiesAdditional> GetHousekeepingFacAdditionalByHKFId(int housekeepingFacilityId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the housekeeping fac additionals.
        /// </summary>
        /// <param name="housekeepingFacAdditional">The housekeeping fac additional.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        HouseKeepingfacilitiesAdditional CreateHousekeepingFacAdditionals(HouseKeepingfacilitiesAdditional housekeepingFacAdditional);
        /// <summary>
        /// Modifies the housekeeping fac additional.
        /// </summary>
        /// <param name="housekeepingFacAdditional">The housekeeping fac additional.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        HouseKeepingfacilitiesAdditional ModifyHousekeepingFacAdditional(HouseKeepingfacilitiesAdditional housekeepingFacAdditional);
        /// <summary>
        /// Gets the housekeeping fac additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        HouseKeepingfacilitiesAdditional GetHousekeepingFacAdditionalById(int id);
        /// <summary>
        /// Deletes the house keeping fac additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteHouseKeepingFacAdditional(int id);
        #endregion Housekeeping facilities Additionals

        #region Housekeeping
        //Housekeeping-Backend
        /// <summary>
        /// Gets all room house keeping.
        /// </summary>
        /// <param name="roomDetailsId">The room details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeeping.</returns>
        IEnumerable<HouseKeeping> GetAllRoomHouseKeeping(int roomDetailsId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the housekeeping by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeeping.</returns>
        HouseKeeping GetHousekeepingById(int id);
        /// <summary>
        /// Modifies the room house keeping facility.
        /// </summary>
        /// <param name="roomHousekeeping">The room housekeeping.</param>
        /// <returns>HouseKeeping.</returns>
        HouseKeeping ModifyRoomHouseKeepingFacility(HouseKeeping roomHousekeeping);
        /// <summary>
        /// Deletes the room house keeping.
        /// </summary>
        /// <param name="houseKeepingId">The house keeping identifier.</param>
        void DeleteRoomHouseKeeping(int houseKeepingId);
        /// <summary>
        /// Gets the unassign hk facilities.
        /// </summary>
        /// <param name="roomDetailsId">The room details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        IEnumerable<HouseKeepingFacility> GetUnassignHKFacilities(int roomDetailsId, int hotelId);
        /// <summary>
        /// Creates the room house keeping.
        /// </summary>
        /// <param name="roomHouseKeeping">The room house keeping.</param>
        void CreateRoomHouseKeeping(List<HouseKeeping> roomHouseKeeping);
        #endregion Housekeeping

        #region Room Details
        //RoomDetails
        /// <summary>
        /// Gets the room details.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomDetails.</returns>
        IEnumerable<RoomDetails> GetRoomDetails(int roomId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the room detail.
        /// </summary>
        /// <param name="roomDetails">The room details.</param>
        /// <returns>RoomDetails.</returns>
        RoomDetails CreateRoomDetail(RoomDetails roomDetails);
        /// <summary>
        /// Modifies the room detail.
        /// </summary>
        /// <param name="roomDetails">The room details.</param>
        /// <returns>RoomDetails.</returns>
        RoomDetails ModifyRoomDetail(RoomDetails roomDetails);
        /// <summary>
        /// Gets the room detail by identifier.
        /// </summary>
        /// <param name="roomDetailId">The room detail identifier.</param>
        /// <returns>RoomDetails.</returns>
        RoomDetails GetRoomDetailById(int roomDetailId);
        /// <summary>
        /// Deletes the room detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRoomDetail(int id);
        #endregion Room Details

        #region Spa Room Details
        //Spa Room Details
        /// <summary>
        /// Gets all spa room details.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaRoomDetails.</returns>
        IEnumerable<SpaRoomDetails> GetAllSpaRoomDetails(int spaRoomId, int skip, int pageSize, out int total);
        /// <summary>
        /// Deletes the spa room detail.
        /// </summary>
        /// <param name="spaRoomdetId">The spa roomdet identifier.</param>
        void DeleteSpaRoomDetail(int spaRoomdetId);
        /// <summary>
        /// Gets the unassign spa details.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetUnassignSpaDetails(int spaRoomId, int hotelId);
        /// <summary>
        /// Creates the spa room details.
        /// </summary>
        /// <param name="spaRoomDetails">The spa room details.</param>
        void CreateSpaRoomDetails(List<SpaRoomDetails> spaRoomDetails);
        #endregion Spa Room Details

        #region Amenities
        //Amenities
        /// <summary>
        /// Gets all amenities.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Amenities.</returns>
        IEnumerable<Amenities> GetAllAmenities(int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the amenities.
        /// </summary>
        /// <param name="amenities">The amenities.</param>
        /// <returns>Amenities.</returns>
        Amenities CreateAmenities(Amenities amenities);
        /// <summary>
        /// Modifies the amenities.
        /// </summary>
        /// <param name="amenities">The amenities.</param>
        /// <returns>Amenities.</returns>
        Amenities ModifyAmenities(Amenities amenities);
        /// <summary>
        /// Gets the amenity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Amenities.</returns>
        Amenities GetAmenityById(int id);
        /// <summary>
        /// Deletes the amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteAmenities(int id);
        #endregion Amenities

        #region Garments
        //Garments
        /// <summary>
        /// Gets all garments.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Garments.</returns>
        IEnumerable<Garments> GetAllGarments(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all garments.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable Garments.</returns>
        IEnumerable<Garments> GetAllGarments(int laundryId);
        /// <summary>
        /// Creates the garments.
        /// </summary>
        /// <param name="garments">The garments.</param>
        /// <returns>Garments.</returns>
        Garments CreateGarments(Garments garments);
        /// <summary>
        /// Modifies the garments.
        /// </summary>
        /// <param name="garments">The garments.</param>
        /// <returns>Garments.</returns>
        Garments ModifyGarments(Garments garments);
        /// <summary>
        /// Gets the garments by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Garments.</returns>
        Garments GetGarmentsById(int id);
        /// <summary>
        /// Deletes the garments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteGarments(int id);
        #endregion Garments

        #region Concierge Group
        //ConciergeGroup
        /// <summary>
        /// Gets all concierge group.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ConciergeGroup.</returns>
        IEnumerable<ConciergeGroup> GetAllConciergeGroup(int hotelId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all concierge groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ConciergeGroup.</returns>
        IEnumerable<ConciergeGroup> GetAllConciergeGroups(int hotelId);
        /// <summary>
        /// Creates the concierge group.
        /// </summary>
        /// <param name="conciergeGroup">The concierge group.</param>
        /// <returns>ConciergeGroup.</returns>
        ConciergeGroup CreateConciergeGroup(ConciergeGroup conciergeGroup);
        /// <summary>
        /// Modifies the concierge group.
        /// </summary>
        /// <param name="conciergeGroup">The concierge group.</param>
        /// <returns>ConciergeGroup.</returns>
        ConciergeGroup ModifyConciergeGroup(ConciergeGroup conciergeGroup);
        /// <summary>
        /// Gets the concierge group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ConciergeGroup.</returns>
        ConciergeGroup GetConciergeGroupById(int id);
        /// <summary>
        /// Deletes the concierge group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteConciergeGroup(int id);
        #endregion Concierge Group

        #region Concierge
        //Concierge
        /// <summary>
        /// Gets all concierge by group identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Concierge.</returns>
        IEnumerable<Concierge> GetAllConciergeByGroupId(int id, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the concierge.
        /// </summary>
        /// <param name="concierge">The concierge.</param>
        /// <returns>Concierge.</returns>
        Concierge CreateConcierge(Concierge concierge);
        /// <summary>
        /// Modifies the concierge.
        /// </summary>
        /// <param name="concierge">The concierge.</param>
        /// <returns>Concierge.</returns>
        Concierge ModifyConcierge(Concierge concierge);
        /// <summary>
        /// Gets the concierge by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Concierge.</returns>
        Concierge GetConciergeById(int id);
        /// <summary>
        /// Deletes the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteConcierge(int id);
        #endregion Concierge

        #region Hotels Services Offers

        /// <summary>
        /// Gets all hotel services offers.
        /// </summary>
        /// <returns>IEnumerable AllHotelServicesOffers.</returns>
        IEnumerable<AllHotelServicesOffers> GetAllHotelServicesOffers();

        #endregion Hotels Services Offers

    }
}
