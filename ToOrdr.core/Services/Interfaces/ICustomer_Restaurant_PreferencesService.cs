﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="ICustomer_Restaurant_PreferencesService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface ICustomer_Restaurant_PreferencesService
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Customer_Restaurant_Preference}" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Customer_Restaurant_Preference}" />
    public interface ICustomer_Restaurant_PreferencesService : IServiceBase<Customer_Restaurant_Preference>
    {
    }
}
