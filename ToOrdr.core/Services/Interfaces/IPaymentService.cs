﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 03-13-2019
// ***********************************************************************
// <copyright file="IPaymentService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface IPaymentService
    /// </summary>
    public interface IPaymentService
    {
        /// <summary>
        /// Checks the card spending limit.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerDetails">The customer details.</param>
        /// <param name="appService">The application service.</param>
        /// <param name="isTips">if set to <c>true</c> [is tips].</param>
        /// <param name="tipAmount">The tip amount.</param>
        /// <returns>System.String.</returns>
        string CheckCardSpendingLimit(int orderId, string quickPayToken, string quickPayUrl, int? cardId, Customer customerDetails, AppServices appService, bool isTips, double? tipAmount);
        /// <summary>
        /// Cards the payment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>System.String.</returns>
        string CardPayment(int orderId, string quickPayToken, string quickPayUrl, int? cardId, int customerId);
        /// <summary>
        /// Captures the card payment.
        /// </summary>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="QuickPayPaymentId">The quick pay payment identifier.</param>
        /// <param name="OrderAmount">The order amount.</param>
        /// <returns>Boolean.</returns>
        Boolean CaptureCardPayment(string quickPayToken, string quickPayUrl, string QuickPayPaymentId, double? OrderAmount);
        /// <summary>
        /// Cancels the order and release amount.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>System.String.</returns>
        string CancelOrderAndReleaseAmount(RestaurantOrder restOrder, string quickPayToken, string quickPayUrl);
        /// <summary>
        /// Tipses the payment using card.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="tipsAmount">The tips amount.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>System.String.</returns>
        string TipsPaymentUsingCard(int orderId, string quickPayToken, string quickPayUrl, double tipsAmount, string comment, int? cardId, int customerId);


        #region Hotel Services Payment

        /// <summary>
        /// Hotels the service order card payment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        string HotelServiceOrderCardPayment(int orderId, string currency, string quickPayToken, string quickPayUrl, int? cardId, int customerId, HotelServiceType hotelServiceType = HotelServiceType.Spa);
        /// <summary>
        /// Cancels the hotel service order and release amount.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        string CancelHotelServiceOrderAndReleaseAmount(int orderId, int customerId, string quickPayToken, string quickPayUrl, HotelServiceType hotelServiceType);
        /// <summary>
        /// Hotels the service tips payment using card.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="tipsAmount">The tips amount.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        string HotelServiceTipsPaymentUsingCard(int orderId, string quickPayToken, string quickPayUrl, double tipsAmount, string comment, int? cardId, int customerId, HotelServiceType hotelServiceType);
        /// <summary>
        /// Rejects the hotel service order and release amount.
        /// </summary>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="QuickPayPaymentId">The quick pay payment identifier.</param>
        /// <returns>Boolean.</returns>
        Boolean RejectHotelServiceOrderAndReleaseAmount(string quickPayToken, string quickPayUrl, string QuickPayPaymentId);
        #endregion Hotel Services Payment

        //bool PaypalPayment(int customerId, string email, int orderId, int cardId);

        /// <summary>
        /// Captures the paypal payment.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <param name="token">The token.</param>
        /// <returns>System.Object.</returns>
        object CapturePaypalPayment(RestaurantOrder restOrder, string baseAddress, string token);
        /// <summary>
        /// Cancels the order and release paypal amount.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="paypalBaseUrl">The paypal base URL.</param>
        /// <param name="token">The token.</param>
        /// <returns>System.String.</returns>
        string CancelOrderAndReleasePaypalAmount(RestaurantOrder restOrder, string paypalBaseUrl, string token);

        /// <summary>
        /// Paypals the token.
        /// </summary>
        /// <returns>System.Object.</returns>
        object PaypalToken();
        /// <summary>
        /// Payments the excecute.
        /// </summary>
        /// <param name="paymentID">The payment identifier.</param>
        /// <param name="payerID">The payer identifier.</param>
        /// <returns>System.Object.</returns>
        object PaymentExcecute(string paymentID, string payerID);

        /// <summary>
        /// Generates the full paypal token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <returns>System.Object.</returns>
        object GenerateFullPaypalToken(string token, string baseAddress);
        /// <summary>
        /// Generates the paypal token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <returns>System.Object.</returns>
        object GeneratePaypalToken(string token, string baseAddress);
    }
}
