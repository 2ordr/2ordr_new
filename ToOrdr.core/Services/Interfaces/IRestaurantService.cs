﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-14-2019
// ***********************************************************************
// <copyright file="IRestaurantService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Enum RestaurantTableStatus
    /// </summary>
    public enum RestaurantTableStatus
    {
        /// <summary>
        /// The clear table
        /// </summary>
        ClearTable = 1,
        /// <summary>
        /// The call waiter
        /// </summary>
        CallWaiter = 2
    }
    /// <summary>
    /// Enum RestaurantSort
    /// </summary>
    public enum RestaurantSort
    {
        /// <summary>
        /// The popularity
        /// </summary>
        Popularity,
        /// <summary>
        /// The rating
        /// </summary>
        Rating,
        /// <summary>
        /// The distance
        /// </summary>
        Distance,
        /// <summary>
        /// The cost
        /// </summary>
        Cost,
        //Price
    }

    /// <summary>
    /// Enum SortOrder
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// The asc
        /// </summary>
        Asc,
        /// <summary>
        /// The desc
        /// </summary>
        Desc
    }
    /// <summary>
    /// Enum Selection
    /// </summary>
    public enum Selection
    {
        /// <summary>
        /// All
        /// </summary>
        All,
        /// <summary>
        /// The popular
        /// </summary>
        Popular,
        /// <summary>
        /// The personal
        /// </summary>
        Personal
    }
    /// <summary>
    /// Enum SearchTextType
    /// </summary>
    public enum SearchTextType
    {
        /// <summary>
        /// The everywhere
        /// </summary>
        Everywhere,
        /// <summary>
        /// The restaurant name
        /// </summary>
        RestaurantName
    }

    /// <summary>
    /// Enum GroupBy
    /// </summary>
    public enum GroupBy
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The user rating
        /// </summary>
        UserRating,
        /// <summary>
        /// The price range
        /// </summary>
        PriceRange,
        /// <summary>
        /// The radius
        /// </summary>
        Radius
    }

    /// <summary>
    /// Enum ListBy
    /// </summary>
    public enum ListBy
    {
        /// <summary>
        /// The month
        /// </summary>
        Month,
        /// <summary>
        /// The year
        /// </summary>
        Year
    }

    /// <summary>
    /// Interface IRestaurantService
    /// </summary>
    public interface IRestaurantService
    {

        #region restaurant

        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="place">The place.</param>
        /// <param name="cuisineIds">The cuisine ids.</param>
        /// <returns>List Restaurant.</returns>
        List<Restaurant> SearchRestaurants(
           string keywords = null,
           string place = null,
           IList<int> cuisineIds = null
           );


        /// <summary>
        /// Searches the specified total.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="radius_min">The radius minimum.</param>
        /// <param name="radius_max">The radius maximum.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="rating_min">The rating minimum.</param>
        /// <param name="rating_max">The rating maximum.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="selection">The selection.</param>
        /// <param name="sortOn">The sort on.</param>
        /// <param name="order">The order.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <param name="searchTextType">Type of the search text.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="openingHour">The opening hour.</param>
        /// <param name="closingHour">The closing hour.</param>
        /// <param name="starRating">The star rating.</param>
        /// <param name="priceRange">The price range.</param>
        /// <param name="minUserRating">The minimum user rating.</param>
        /// <param name="maxUserRating">The maximum user rating.</param>
        /// <param name="groupBy">The group by.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> Search(out int total, string searchText, double? lat, double? lng, double? radius_min, double? radius_max, int[] Ids, double? rating_min, double? rating_max, double? price_min = null, double? price_max = null, int page = 1, int limit = 20, Selection selection = Selection.All, RestaurantSort sortOn = RestaurantSort.Cost, SortOrder order = SortOrder.Asc, bool considerMyPreferences = false, SearchTextType searchTextType = SearchTextType.Everywhere, int? customerId = null, TimeSpan? openingHour = null, TimeSpan? closingHour = null, int? starRating = null, int? priceRange = null, int? minUserRating = null, int? maxUserRating = null, GroupBy groupBy = GroupBy.None);


        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> SearchRestaurants(double lat, double lng);
        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="range">The range.</param>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> SearchRestaurants(double latitude, double longitude, double range, string orderby, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllRestaurants();
        /// <summary>
        /// Gets all non virtual restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllNonVirtualRestaurants();
        /// <summary>
        /// Gets all virtual restaurants.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllVirtualRestaurants(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all user restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllUserRestaurants(int customerId);

        /// <summary>
        /// Gets the restaurants.
        /// </summary>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetRestaurants(string orderby, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Gets the restaurant by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Restaurant.</returns>
        Restaurant GetRestaurantById(int id);

        /// <summary>
        /// Creates the restaurant.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Restaurant.</returns>
        Restaurant CreateRestaurant(Restaurant details);

        /// <summary>
        /// Modifies the restaurant.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Restaurant.</returns>
        Restaurant ModifyRestaurant(Restaurant details);

        /// <summary>
        /// Deletes the restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRestaurant(int id);

        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="range">The range.</param>
        /// <param name="keyword">The keyword.</param>
        /// <param name="cuisines">The cuisines.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="order">The order.</param>
        /// <returns>List&lt;Restaurant&gt;.</returns>
        List<Restaurant> SearchRestaurants(
         out int total,
         double? lat = null,
         double? lng = null,
         double? range = null,
         string keyword = "",
            string cuisines = "",
         int? page = null,
         int? limit = null,
         string sort = "",
         string order = ""
     );
        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="range">The range.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="order">The order.</param>
        /// <returns>List Restaurant.</returns>
        List<Restaurant> SearchRestaurants(
          out int total,
          double? lat = null,
          double? lng = null,
          double? range = null,
          int? page = null,
          int? limit = null,
          string sort = "",
          string order = " "
         );
        /// <summary>
        /// Gets the name of all restaurants by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllRestaurantsByName(int customerId, string restaurant_name);
        /// <summary>
        /// Gets all restaurants by name of owner.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllRestaurantsByNameOfOwner(int customerId, string restaurant_name);
        /// <summary>
        /// Gets all last week restaurants.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllLastWeekRestaurants(DateTime from, DateTime to);
        /// <summary>
        /// Gets the restaurant groups by star rating.
        /// </summary>
        /// <returns>IEnumerable RestaurantGroupByStarRating.</returns>
        IEnumerable<RestaurantGroupByStarRating> GetRestaurantGroupsByStarRating();
        /// <summary>
        /// Gets the restaurant by star rating.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetRestaurantByStarRating(int rating, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant groups by price range.
        /// </summary>
        /// <returns>IEnumerable RestaurantGroupByPriceRange.</returns>
        IEnumerable<RestaurantGroupByPriceRange> GetRestaurantGroupsByPriceRange();
        /// <summary>
        /// Gets the restaurants by price range.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetRestaurantsByPriceRange(int price, int pageNumber, int pageSize, out int total);

        #endregion

        #region Menu Item Offers
        /// <summary>
        /// Gets all rest offers menu items.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable MenuItemOffer.</returns>
        IEnumerable<MenuItemOffer> GetAllRestOffersMenuItems(int? hotelId);

        //web
        /// <summary>
        /// Gets the offers menu items of rest.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemOffer.</returns>
        IEnumerable<MenuItemOffer> GetOffersMenuItemsOfRest(int restaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the unassigned offer menu items of rest.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetUnassignedOfferMenuItemsOfRest(int restaurantId);
        /// <summary>
        /// Gets the discounted menu item by identifier.
        /// </summary>
        /// <param name="menuItemOfferId">The menu item offer identifier.</param>
        /// <returns>MenuItemOffer.</returns>
        MenuItemOffer GetDiscountedMenuItemById(int menuItemOfferId);
        /// <summary>
        /// Creates the menu item offer.
        /// </summary>
        /// <param name="menuItemOffer">The menu item offer.</param>
        /// <returns>MenuItemOffer.</returns>
        MenuItemOffer CreateMenuItemOffer(MenuItemOffer menuItemOffer);
        /// <summary>
        /// Modifies the menu item offer.
        /// </summary>
        /// <param name="menuItemOffer">The menu item offer.</param>
        /// <returns>MenuItemOffer.</returns>
        MenuItemOffer ModifyMenuItemOffer(MenuItemOffer menuItemOffer);
        /// <summary>
        /// Deletes the menu item offer.
        /// </summary>
        /// <param name="menuItemOfferid">The menu item offerid.</param>
        void DeleteMenuItemOffer(int menuItemOfferid);

        #endregion Menu Item Offers

        #region restaurant menu

        /// <summary>
        /// Gets the daily menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetDailyMenu(int RestaurantId, int customerId, int? languageId, Func<RestaurantMenu, Object> orderByPredicate, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false);
        /// <summary>
        /// Gets the customer preferred menus.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetCustomerPreferredMenus(int customerId, int RestaurantId);
        /// <summary>
        /// Gets all restaurant menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetAllRestaurantMenu(int RestaurantId, Func<RestaurantMenu, Object> orderByPredicate, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets all unassigned rest table menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="tableId">The table identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetAllUnassignedRestTableMenu(int RestaurantId, int tableId);
        /// <summary>
        /// Gets the daily menu language wise.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetDailyMenuLanguageWise(int RestaurantId, int? languageId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        RestaurantMenuItem GetMenuItem(int id, int? languageId);

        /// <summary>
        /// Gets the restaurant menu by table identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetRestaurantMenuByTableId(int id, int? languageId);

        /// <summary>
        /// Searches the menu items.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="restaurant_Id">The restaurant identifier.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> SearchMenuItems(out int total, string searchText, int? restaurant_Id, int page = 1, int limit = 10);

        /// <summary>
        /// Creates the menu item review.
        /// </summary>
        /// <param name="menuItemReview">The menu item review.</param>
        /// <returns>MenuItemReview.</returns>
        MenuItemReview CreateMenuItemReview(MenuItemReview menuItemReview);

        /// <summary>
        /// Gets the menu item reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReview.</returns>
        IEnumerable<MenuItemReview> GetMenuItemReviews(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant menu item reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReview.</returns>
        IEnumerable<MenuItemReview> GetRestaurantMenuItemReviews(string From, string To, int restaurantId, int skip, int pageSize, out int total);

        /// <summary>
        /// Creates the restaurant menu group.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        RestaurantMenuGroup CreateRestaurantMenuGroup(RestaurantMenuGroup group);
        /// <summary>
        /// Gets the restaurant menu group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        RestaurantMenuGroup GetRestaurantMenuGroupById(int id);
        /// <summary>
        /// Modifies the restaurant menu group.
        /// </summary>
        /// <param name="menuGroup">The menu group.</param>
        /// <param name="MenuGroupTranslation">The menu group translation.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        RestaurantMenuGroup ModifyRestaurantMenuGroup(RestaurantMenuGroup menuGroup, List<RestaurantMenuGroupTranslation> MenuGroupTranslation);
        /// <summary>
        /// Deletes the restaurant menu group translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        void DeleteRestaurantMenuGroupTranslation(int translationId);
        /// <summary>
        /// Creates the restaurant menu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        RestaurantMenuItem CreateRestaurantMenuItem(RestaurantMenuItem item, int restaurantId);

        /// <summary>
        /// Updates the menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        RestaurantMenuGroup UpdateMenuGroup(int id, int restaurantId);
        /// <summary>
        /// Updates the menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        RestaurantMenuItem UpdateMenuItem(int id, int restaurantId);
        /// <summary>
        /// Updates the menu itemfor sold out.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        void UpdateMenuItemforSoldOut(int itemId, int restaurantId);
        /// <summary>
        /// Modifies the menu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="ItemTranslation">The item translation.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        RestaurantMenuItem ModifyMenuItem(RestaurantMenuItem item, List<RestaurantMenuItemTranslation> ItemTranslation, int restaurantId);
        /// <summary>
        /// Deletes the restaurant menu item translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        void DeleteRestaurantMenuItemTranslation(int translationId);

        /// <summary>
        /// Deletes the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        void DeleteRestaurantMenuGroup(int id, int restaurantId);
        /// <summary>
        /// Deletes the restaurant menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        void DeleteRestaurantMenuItem(int id, int restaurantId);

        /// <summary>
        /// Creates the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <returns>RestaurantMenu.</returns>
        RestaurantMenu CreateRestaurantMenu(RestaurantMenu menu);
        /// <summary>
        /// Gets the restaurant menu by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenu.</returns>
        RestaurantMenu GetRestaurantMenuById(int id);
        /// <summary>
        /// Gets the restaurant menu by identifier language wise.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>RestaurantMenu.</returns>
        RestaurantMenu GetRestaurantMenuByIdLanguageWise(int id, int? languageId);
        /// <summary>
        /// Modifies the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <param name="MenuTranslation">The menu translation.</param>
        /// <returns>RestaurantMenu.</returns>
        RestaurantMenu ModifyRestaurantMenu(RestaurantMenu menu, List<RestaurantMenuTranslation> MenuTranslation);
        /// <summary>
        /// Deletes the restaurant menu translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        void DeleteRestaurantMenuTranslation(int translationId);
        /// <summary>
        /// Deletes the restaurant menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        void DeleteRestaurantMenu(int id, int restaurantId);

        /// <summary>
        /// Gets the menu item by group identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetMenuItemByGroupId(int id, Func<RestaurantMenuItem, Object> orderByPredicate, int pageNumber, int pageSize, out int total, int? languageId);
        /// <summary>
        /// Gets the menu items by group identifier and search text.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetMenuItemsByGroupIdAndSearchText(int id, int customerId, string searchText, int pageNumber, int pageSize, out int total, int? languageId, bool considerMyPreferences = false);

        /// <summary>
        /// Gets the menu item suggestions.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetMenuItemSuggestions(int ingredientId);

        /// <summary>
        /// Gets the menu item suggestions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetMenuItemSuggestions(int menuItemId, int pageNumber, int pageSize, out int total, int? languageId);

        /// <summary>
        /// Gets the menu groups by menu identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenuGroupsOfMenu.</returns>
        IEnumerable<RestaurantMenuGroupsOfMenu> GetMenuGroupsByMenuId(int id, int customerId, int pageNumber, int pageSize, out int total, int? languageId, bool considerMyPreferences = false);
        /// <summary>
        /// Gets the customer preferred menu groups.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selectedMenuId">The selected menu identifier.</param>
        /// <returns>IEnumerable RestaurantMenuGroup.</returns>
        IEnumerable<RestaurantMenuGroup> GetCustomerPreferredMenuGroups(int customerId, int selectedMenuId);
        #endregion

        #region restaurant reviews

        /// <summary>
        /// Gets the review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantReview.</returns>
        RestaurantReview GetReviewById(int id);

        /// <summary>
        /// Gets the restaurant reviews.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetRestaurantReviews(int RestaurantId, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Creates the restaurant review.
        /// </summary>
        /// <param name="restaurantReview">The restaurant review.</param>
        /// <returns>RestaurantReview.</returns>
        RestaurantReview CreateRestaurantReview(RestaurantReview restaurantReview);

        /// <summary>
        /// Deletes the restaurant review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRestaurantReview(int id);
        /// <summary>
        /// Gets the customer reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetCustomerReviews(int id);
        /// <summary>
        /// Gets all restaurants reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetAllRestaurantsReviews(int customerId);


        /// <summary>
        /// Gets all reviews of authentication restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetAllReviewsOfAuthRestaurants(int customerId, int pageNumber, int pageSize, int? restId, out int total);
        /// <summary>
        /// Gets the restaurant all reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetRestaurantAllReviews(string From, string To, int? restaurantId, int? customerId, int skip, int pageSize, out int total);
        #endregion

        #region Table Reservation

        /// <summary>
        /// Reserves the table.
        /// </summary>
        /// <param name="TotalSeats">The total seats.</param>
        /// <param name="Reservation_DateTime">The reservation date time.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        IEnumerable<RestaurantTable> ReserveTable(int TotalSeats, DateTime Reservation_DateTime);

        //Customer_Table_Reservations GetCustomerReservedTableById(int id);

        //Customer_Table_Reservations CreateReservation(Customer_Table_Reservations reservations, List<RestaurantTable> restaurantTables);

        #endregion

        #region Measurement Unit
        /// <summary>
        /// Gets all measurement units.
        /// </summary>
        /// <returns>IEnumerable MeasurementUnit.</returns>
        IEnumerable<MeasurementUnit> GetAllMeasurementUnits();
        /// <summary>
        /// Gets the measurement units.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MeasurementUnit.</returns>
        IEnumerable<MeasurementUnit> GetMeasurementUnits(int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the measurement units.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>MeasurementUnit.</returns>
        MeasurementUnit CreateMeasurementUnits(MeasurementUnit measurementUnit);
        /// <summary>
        /// Deletes the measurement unit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteMeasurementUnit(int id);
        #endregion Measurment Unit

        #region Cuisines
        /// <summary>
        /// Gets all cuisines.
        /// </summary>
        /// <returns>IEnumerable Cuisine.</returns>
        IEnumerable<Cuisine> GetAllCuisines();
        /// <summary>
        /// Gets all rest unassigned cuisines.
        /// </summary>
        /// <param name="restId">The rest identifier.</param>
        /// <returns>IEnumerable Cuisine.</returns>
        IEnumerable<Cuisine> GetAllRestUnassignedCuisines(int restId);
        /// <summary>
        /// Gets all cuisines.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Cuisine.</returns>
        IEnumerable<Cuisine> GetAllCuisines(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the cuisine by identifier.
        /// </summary>
        /// <param name="cuisineId">The cuisine identifier.</param>
        /// <returns>Cuisine.</returns>
        Cuisine GetCuisineById(int cuisineId);
        /// <summary>
        /// Creates the cuisine.
        /// </summary>
        /// <param name="cuisine">The cuisine.</param>
        /// <returns>Cuisine.</returns>
        Cuisine CreateCuisine(Cuisine cuisine);
        /// <summary>
        /// Modifies the cuisine.
        /// </summary>
        /// <param name="cuisine">The cuisine.</param>
        /// <returns>Cuisine.</returns>
        Cuisine ModifyCuisine(Cuisine cuisine);
        /// <summary>
        /// Deletes the cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCuisine(int id);

        /// <summary>
        /// Gets the restaurant cuisines.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantCuisine.</returns>
        IEnumerable<RestaurantCuisine> GetRestaurantCuisines(int id);
        /// <summary>
        /// Gets all restaurant cuisines.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantCuisine.</returns>
        IEnumerable<RestaurantCuisine> GetAllRestaurantCuisines(int restaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant cuisine by identifier.
        /// </summary>
        /// <param name="restCuisineId">The rest cuisine identifier.</param>
        /// <returns>RestaurantCuisine.</returns>
        RestaurantCuisine GetRestaurantCuisineById(int restCuisineId);
        /// <summary>
        /// Creates the restaurant cuisine.
        /// </summary>
        /// <param name="restCuisine">The rest cuisine.</param>
        /// <returns>RestaurantCuisine.</returns>
        RestaurantCuisine CreateRestaurantCuisine(RestaurantCuisine restCuisine);
        /// <summary>
        /// Modifies the rest cuisine.
        /// </summary>
        /// <param name="restCuisine">The rest cuisine.</param>
        /// <returns>RestaurantCuisine.</returns>
        RestaurantCuisine ModifyRestCuisine(RestaurantCuisine restCuisine);
        /// <summary>
        /// Deletes the restaurant cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRestaurantCuisine(int id);

        #endregion Cuisines

        #region Restaurant Cart

        /// <summary>
        /// Gets the customer cart.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantCart.</returns>
        RestaurantCart GetCustomerCart(int Id, int restaurantId);

        /// <summary>
        /// Creates the restaurant cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>RestaurantCart.</returns>
        RestaurantCart CreateRestaurantCart(RestaurantCart cart, OrderType orderType, int? table_room_Id, bool flush = false);

        /// <summary>
        /// Updates the restaurant cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>RestaurantCart.</returns>
        RestaurantCart UpdateRestaurantCart(int customerId, RestaurantCart cart);

        #endregion Restaurant Cart

        #region Restaurant Order

        /// <summary>
        /// Creates the restaurant order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="tableId">The table identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder CreateRestaurantOrderFromCart(int customerId, int? cardId, int restaurantId, DateTime scheduleDate);
        /// <summary>
        /// Schedules the restaurant order.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder ScheduleRestaurantOrder(int customerId, int orderId, DateTime scheduleDate);

        /// <summary>
        /// Gets the restaurant order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder GetRestaurantOrderById(int orderId);
        /// <summary>
        /// Gets the order next serving status changed count.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>System.Int32.</returns>
        int GetOrderNextServingStatusChangedCount(int restaurantId);
        /// <summary>
        /// Updates the rest order next serving status.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        void UpdateRestOrderNextServingStatus(int orderId);

        //Web
        /// <summary>
        /// Gets all recent orders of authentication restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetAllRecentOrdersOfAuthRestaurants(int customerId, int pageNumber, int pageSize, int? restId, out int total);
        /// <summary>
        /// Gets all restaurants orders.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetAllRestaurantsOrders(string From, string To, int RestaurantId, Double? minAmount, Double? maxAmount, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetRestaurantOrderHistory(string From, string To, int RestaurantId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the rest order item details.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>IEnumerable RestaurantOrderItem.</returns>
        IEnumerable<RestaurantOrderItem> GetRestOrderItemDetails(int orderId);
        /// <summary>
        /// Updates the rest order item served status.
        /// </summary>
        /// <param name="orderItemId">The order item identifier.</param>
        void UpdateRestOrderItemServedStatus(int orderItemId);
        /// <summary>
        /// Gets all orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetAllOrders(int RestaurantId);
        /// <summary>
        /// Gets the rest orders queue.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetRestOrdersQueue(int RestaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the rest recent orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetRestRecentOrders(int? RestaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the orders details by identifier.
        /// </summary>
        /// <param name="order_ID">The order identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder GetOrdersDetailsById(int order_ID);
        /// <summary>
        /// Modifies the orders.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder ModifyOrders(RestaurantOrder details);
        /// <summary>
        /// Creates the restaurant order review.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>RestaurantOrderReview.</returns>
        RestaurantOrderReview CreateRestaurantOrderReview(RestaurantOrderReview details);
        /// <summary>
        /// Gets the order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantOrderReview.</returns>
        IEnumerable<RestaurantOrderReview> GetOrderReviews(int id);
        /// <summary>
        /// Gets the restaurant order reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrderReview.</returns>
        IEnumerable<RestaurantOrderReview> GetRestaurantOrderReviews(string From, string To, int restaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteOrder(int id);

        /// <summary>
        /// Gets the todays orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetTodaysOrders(int RestaurantId);
        /// <summary>
        /// Gets the last twelve months orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable LastTwelveMonthsEarnings.</returns>
        IEnumerable<LastTwelveMonthsEarnings> GetLastTwelveMonthsOrders(int RestaurantId);
        /// <summary>
        /// Gets the restaurant wise monthly earnings.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantWiseMonthEarnings.</returns>
        IEnumerable<RestaurantWiseMonthEarnings> GetRestaurantWiseMonthlyEarnings(int customerId);
        /// <summary>
        /// Gets the todays orders.
        /// </summary>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetTodaysOrders();

        #endregion Restaurant Order

        #region Customer Restaurnt Orders

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId);

        /// <summary>
        /// Gets the customer active room service orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        CustomerActiveOrdersAndBaskets GetCustomerActiveRoomServiceOrders(int customerId);

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId, int year);

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Gets the customer active orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveOrdersAndBaskets.</returns>
        CustomerActiveOrdersAndBaskets GetCustomerActiveOrders(int customerId);

        #endregion Customer Restaurnt Orders

        #region Restaurant Table Group

        /// <summary>
        /// Gets the restaurant table groups.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantTableGroup.</returns>
        IEnumerable<RestaurantTableGroup> GetRestaurantTableGroups(int restaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant table group by identifier.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        /// <returns>RestaurantTableGroup.</returns>
        RestaurantTableGroup GetRestaurantTableGroupById(int restTableGroupId);
        /// <summary>
        /// Creates the restaurant table group.
        /// </summary>
        /// <param name="restTableGroup">The rest table group.</param>
        /// <returns>RestaurantTableGroup.</returns>
        RestaurantTableGroup CreateRestaurantTableGroup(RestaurantTableGroup restTableGroup);
        /// <summary>
        /// Modifies the restaurant table group.
        /// </summary>
        /// <param name="restTableGroup">The rest table group.</param>
        /// <returns>RestaurantTableGroup.</returns>
        RestaurantTableGroup modifyRestaurantTableGroup(RestaurantTableGroup restTableGroup);
        /// <summary>
        /// Deletes the restaurant table group.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        void DeleteRestaurantTableGroup(int restTableGroupId);

        #endregion Restaurant Table Group

        #region Restaurant Table

        /// <summary>
        /// Gets the restaurant table details.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        IEnumerable<RestaurantTable> GetRestaurantTableDetails(int restTableGroupId);
        /// <summary>
        /// Gets the restaurant table by identifier.
        /// </summary>
        /// <param name="table_id">The table identifier.</param>
        /// <returns>RestaurantTable.</returns>
        RestaurantTable GetRestaurantTableById(int table_id);
        /// <summary>
        /// Checks the exists table number.
        /// </summary>
        /// <param name="table_number">The table number.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        IEnumerable<RestaurantTable> CheckExistsTableNumber(int table_number, int restaurantId);
        /// <summary>
        /// Creates the restaurant table.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTable.</returns>
        RestaurantTable CreateRestaurantTable(RestaurantTable Table);
        /// <summary>
        /// Modifies the restaurant table.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTable.</returns>
        RestaurantTable ModifyRestaurantTable(RestaurantTable Table);
        /// <summary>
        /// Deletes the restaurant table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRestaurantTable(int id);
        /// <summary>
        /// Gets the rest status changed tables.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        IEnumerable<RestaurantTable> GetRestStatusChangedTables(int RestaurantId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the rest tables status changed count.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>System.Int32.</returns>
        int GetRestTablesStatusChangedCount(int restaurantId);

        #endregion Restaurant Table

        #region Restaurant Table Menu

        /// <summary>
        /// Gets the restaurant table menu by table identifier.
        /// </summary>
        /// <param name="tableId">The table identifier.</param>
        /// <returns>IEnumerable RestaurantTableMenu.</returns>
        IEnumerable<RestaurantTableMenu> GetRestaurantTableMenuByTableId(int tableId);
        /// <summary>
        /// Creates the restaurant table menu.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTableMenu.</returns>
        RestaurantTableMenu CreateRestaurantTableMenu(RestaurantTableMenu Table);
        /// <summary>
        /// Modifies the restaurant table menu.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTableMenu.</returns>
        RestaurantTableMenu ModifyRestaurantTableMenu(RestaurantTableMenu Table);
        /// <summary>
        /// Gets the restaurant table menu by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantTableMenu.</returns>
        RestaurantTableMenu GetRestaurantTableMenuById(int id);
        /// <summary>
        /// Deletes the restaurant table menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteRestaurantTableMenu(int id);

        #endregion Restaurant Table Menu

        #region Customer Role

        /// <summary>
        /// Restaurants the authorized customers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> RestaurantAuthorizedCustomers(int id);
        /// <summary>
        /// Authorizeds the restaurant customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> AuthorizedRestaurantCustomers(int customerId, int id);
        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetCustomers(string searchText);

        /// <summary>
        /// Gets all role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        IEnumerable<Role> GetAllRole();
        /// <summary>
        /// Gets the role for chain admin.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>IEnumerable Role.</returns>
        IEnumerable<Role> GetRoleForChainAdmin(int roleId);
        /// <summary>
        /// Manages the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>IEnumerable Role.</returns>
        IEnumerable<Role> ManageRole(int roleId);
        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole CreateCustomerRole(CustomerRole role);
        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole UpdateCustomerRole(CustomerRole role);
        /// <summary>
        /// Gets the customer role.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> GetCustomerRole(int customerId);
        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomerRole(int id);

        /// <summary>
        /// Tops the low selling menu item.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <returns>IEnumerable RestaurantOrderItem.</returns>
        IEnumerable<RestaurantOrderItem> TopLowSellingMenuItem(int restaurantId, string From, string To);

        /// <summary>
        /// Authorizeds the customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        IEnumerable<CustomerRole> AuthorizedCustomers(int customerId);
        /// <summary>
        /// Gets the un role customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetUnRoleCustomers(string searchText);

        /// <summary>
        /// Gets the customer by role identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>CustomerRole.</returns>
        CustomerRole GetCustomerByRoleId(int Id);

        #endregion Customer Role

        #region MenuItem Ingredient

        /// <summary>
        /// Gets the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemIngredient.</returns>
        IEnumerable<MenuItemIngredient> GetItemIngredient(int id);
        /// <summary>
        /// Gets the menu item ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemIngredient.</returns>
        IEnumerable<MenuItemIngredient> GetMenuItemIngredients(int id, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemSize.</returns>
        IEnumerable<MenuItemSize> GetItemSize(int id);
        /// <summary>
        /// Gets the item additionals.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemAdditional.</returns>
        IEnumerable<MenuItemAdditional> GetItemAdditionals(int id);
        /// <summary>
        /// Gets all ingredient.
        /// </summary>
        /// <returns>IEnumerable Ingredient.</returns>
        IEnumerable<Ingredient> GetAllIngredient();
        /// <summary>
        /// Gets all un assigned ingredient.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>IEnumerable Ingredient.</returns>
        IEnumerable<Ingredient> GetAllUnAssignedIngredient(int menuItemId);
        /// <summary>
        /// Gets all ingredient.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Ingredient.</returns>
        IEnumerable<Ingredient> GetAllIngredient(int ingredientCatId, int skip, int pageSize, out int total);
        /// <summary>
        /// Deletes the ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteIngredient(int id);
        /// <summary>
        /// Adds the item ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>MenuItemIngredient.</returns>
        MenuItemIngredient AddItemIngredient(MenuItemIngredient ingredient);
        /// <summary>
        /// Updates the item ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>MenuItemIngredient.</returns>
        MenuItemIngredient UpdateItemIngredient(MenuItemIngredient ingredient);
        /// <summary>
        /// Gets the ingredient by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemIngredient.</returns>
        MenuItemIngredient GetIngredientById(int id);
        /// <summary>
        /// Deletes the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteItemIngredient(int id);

        #endregion MenuItem Ingredient

        #region MenuItemIngredient Nutrition

        /// <summary>
        /// Gets the menu item ingredient nutrition.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemIngredientNutrition.</returns>
        IEnumerable<MenuItemIngredientNutrition> GetMenuItemIngredientNutrition(int id);
        /// <summary>
        /// Updates the menu item ingredient nutrition.
        /// </summary>
        /// <param name="menuItemIngredientNutrition">The menu item ingredient nutrition.</param>
        void UpdateMenuItemIngredientNutrition(MenuItemIngredientNutrition menuItemIngredientNutrition);
        /// <summary>
        /// Deletes the menu item ingredient nutrient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteMenuItemIngredientNutrient(int id);

        #endregion MenuItemIngredient Nutrition

        #region MenuItem Size

        /// <summary>
        /// Adds the size of the item.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        /// <returns>MenuItemSize.</returns>
        MenuItemSize AddItemSize(MenuItemSize itemSize);
        /// <summary>
        /// Gets the item size by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemSize.</returns>
        MenuItemSize GetItemSizeById(int id);
        /// <summary>
        /// Updates the size of the item.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        /// <returns>MenuItemSize.</returns>
        MenuItemSize UpdateItemSize(MenuItemSize itemSize);
        /// <summary>
        /// Deletes the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteItemSize(int id);

        #endregion MenuItem Size

        #region MenuItem Additional

        /// <summary>
        /// Adds the item additional.
        /// </summary>
        /// <param name="itemAdditional">The item additional.</param>
        /// <returns>MenuItemAdditional.</returns>
        MenuItemAdditional AddItemAdditional(MenuItemAdditional itemAdditional);
        /// <summary>
        /// Modifies the item additional.
        /// </summary>
        /// <param name="itemAdditional">The item additional.</param>
        /// <returns>MenuItemAdditional.</returns>
        MenuItemAdditional ModifyItemAdditional(MenuItemAdditional itemAdditional);
        /// <summary>
        /// Gets the item additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemAdditional.</returns>
        MenuItemAdditional GetItemAdditionalById(int id);
        /// <summary>
        /// Deletes the item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteItemAdditional(int id);

        #endregion MenuItem Additional

        #region Menu Additional Group

        /// <summary>
        /// Gets all menu additional group.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>IEnumerable MenuAdditionalGroup.</returns>
        IEnumerable<MenuAdditionalGroup> GetAllMenuAdditionalGroup(int restaurantId, int menuItemId);
        /// <summary>
        /// Gets the menu additional groups.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable MenuAdditionalGroup.</returns>
        IEnumerable<MenuAdditionalGroup> GetMenuAdditionalGroups(int restaurantId);
        /// <summary>
        /// Gets the menu additional group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        MenuAdditionalGroup GetMenuAdditionalGroupById(int id);
        /// <summary>
        /// Creates the menu additional group.
        /// </summary>
        /// <param name="menuAdditionalGrp">The menu additional GRP.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        MenuAdditionalGroup CreateMenuAdditionalGroup(MenuAdditionalGroup menuAdditionalGrp);
        /// <summary>
        /// Modifies the menu additional group.
        /// </summary>
        /// <param name="menuAdditionalGrp">The menu additional GRP.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        MenuAdditionalGroup ModifyMenuAdditionalGroup(MenuAdditionalGroup menuAdditionalGrp);
        /// <summary>
        /// Deletes the menu additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteMenuAdditionalGroup(int id);

        #endregion Menu Additional Group

        #region Menu Additional Element

        /// <summary>
        /// Gets the menu additional elements.
        /// </summary>
        /// <param name="grp_id">The GRP identifier.</param>
        /// <returns>IEnumerable MenuAdditionalElement.</returns>
        IEnumerable<MenuAdditionalElement> GetMenuAdditionalElements(int grp_id);
        /// <summary>
        /// Gets the menu additional element by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuAdditionalElement.</returns>
        MenuAdditionalElement GetMenuAdditionalElementById(int id);
        /// <summary>
        /// Creates the menu additional element.
        /// </summary>
        /// <param name="menuAdditionalElement">The menu additional element.</param>
        /// <returns>MenuAdditionalElement.</returns>
        MenuAdditionalElement CreateMenuAdditionalElement(MenuAdditionalElement menuAdditionalElement);
        /// <summary>
        /// Modifies the menu additional element.
        /// </summary>
        /// <param name="menuAdditionalElement">The menu additional element.</param>
        /// <returns>MenuAdditionalElement.</returns>
        MenuAdditionalElement ModifyMenuAdditionalElement(MenuAdditionalElement menuAdditionalElement);
        /// <summary>
        /// Deletes the menu additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteMenuAdditionalElement(int id);
        /// <summary>
        /// Updates the menu item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemAdditional.</returns>
        MenuItemAdditional UpdateMenuItemAdditional(int id);

        #endregion Menu Additional Element

        /// <summary>
        /// Gets the customer restaurant reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetCustomerRestaurantReviews(int customerId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the customer menu item reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReviewAndRestDetails.</returns>
        IEnumerable<MenuItemReviewAndRestDetails> GetCustomerMenuItemReviews(int customerId, int pageNumber, int pageSize, out int total);

        /// <summary>
        /// Gets the pending reviews restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetPendingReviewsRestaurants(int customerId, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the pending reviews menu items.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetPendingReviewsMenuItems(int customerId, int pageNumber, int pageSize, out int total);

        #region Restaurant MenuItem Image

        /// <summary>
        /// Gets the item images.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItemImage.</returns>
        IEnumerable<RestaurantMenuItemImage> GetItemImages(int id);
        /// <summary>
        /// Adds the item images.
        /// </summary>
        /// <param name="itemImages">The item images.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        RestaurantMenuItemImage AddItemImages(RestaurantMenuItemImage itemImages);
        /// <summary>
        /// Updates the item images.
        /// </summary>
        /// <param name="itemImages">The item images.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        RestaurantMenuItemImage UpdateItemImages(RestaurantMenuItemImage itemImages);
        /// <summary>
        /// Gets the item image by image identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        RestaurantMenuItemImage GetItemImageByImageId(int id);
        /// <summary>
        /// Deletes the item image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteItemImage(int id);
        /// <summary>
        /// Updates the item image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        RestaurantMenuItemImage UpdateItemImageActive(int id);

        #endregion Restaurant MenuItem Image

        #region Ingredient Categories
        /// <summary>
        /// Gets all ingredient categories.
        /// </summary>
        /// <returns>IEnumerable IngredientCategory.</returns>
        IEnumerable<IngredientCategory> GetAllIngredientCategories();
        /// <summary>
        /// Gets the type of all ingredient categories by.
        /// </summary>
        /// <param name="type">if set to <c>true</c> [type].</param>
        /// <returns>IEnumerable IngredientCategory.</returns>
        IEnumerable<IngredientCategory> GetAllIngredientCategoriesByType(bool type);
        /// <summary>
        /// Gets all ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable IngredientCategory.</returns>
        IEnumerable<IngredientCategory> GetAllIngredientCategories(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the ingredient category by identifier.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <returns>IngredientCategory.</returns>
        IngredientCategory GetIngredientCategoryById(int ingredientCatId);
        /// <summary>
        /// Creates the ingredient category.
        /// </summary>
        /// <param name="ingredientCategory">The ingredient category.</param>
        /// <returns>IngredientCategory.</returns>
        IngredientCategory CreateIngredientCategory(IngredientCategory ingredientCategory);
        /// <summary>
        /// Modifies the ingredient category.
        /// </summary>
        /// <param name="ingredientCategory">The ingredient category.</param>
        /// <returns>IngredientCategory.</returns>
        IngredientCategory ModifyIngredientCategory(IngredientCategory ingredientCategory);
        /// <summary>
        /// Deletes the ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteIngredientCategory(int id);
        #endregion Ingredient Categories

        #region Ingredient Food Profiles
        /// <summary>
        /// Creates the ingredient food profiles.
        /// </summary>
        /// <param name="ingredientFoodProfiles">The ingredient food profiles.</param>
        void CreateIngredientFoodProfiles(List<IngredientFoodProfile> ingredientFoodProfiles);
        /// <summary>
        /// Gets all ingredient food profiles.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable IngredientFoodProfile.</returns>
        IEnumerable<IngredientFoodProfile> GetAllIngredientFoodProfiles(int ingredientId);
        /// <summary>
        /// Deletes the ingredient food profile.
        /// </summary>
        /// <param name="ingredientFoodProfileId">The ingredient food profile identifier.</param>
        void DeleteIngredientFoodProfile(int ingredientFoodProfileId);

        #endregion Ingredient Food Profiles

        #region Food Profile
        /// <summary>
        /// Gets all unassigned food profiles.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable FoodProfile.</returns>
        IEnumerable<FoodProfile> GetAllUnassignedFoodProfiles(int? ingredientId);
        /// <summary>
        /// Gets all food profiles.
        /// </summary>
        /// <returns>IEnumerable FoodProfile.</returns>
        IEnumerable<FoodProfile> GetAllFoodProfiles();
        /// <summary>
        /// Gets all food profiles.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable FoodProfile.</returns>
        IEnumerable<FoodProfile> GetAllFoodProfiles(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the food profile by identifier.
        /// </summary>
        /// <param name="foodProfileId">The food profile identifier.</param>
        /// <returns>FoodProfile.</returns>
        FoodProfile GetFoodProfileById(int foodProfileId);
        /// <summary>
        /// Creates the food profile.
        /// </summary>
        /// <param name="foodProfile">The food profile.</param>
        /// <returns>FoodProfile.</returns>
        FoodProfile CreateFoodProfile(FoodProfile foodProfile);
        /// <summary>
        /// Modifies the food profile.
        /// </summary>
        /// <param name="foodProfile">The food profile.</param>
        /// <returns>FoodProfile.</returns>
        FoodProfile ModifyFoodProfile(FoodProfile foodProfile);
        /// <summary>
        /// Deletes the food profile.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteFoodProfile(int id);
        #endregion Food Profile

        #region Allergen
        /// <summary>
        /// Gets all allergen categories.
        /// </summary>
        /// <returns>IEnumerable AllergenCategory.</returns>
        IEnumerable<AllergenCategory> GetAllAllergenCategories();
        /// <summary>
        /// Gets all allergen categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable AllergenCategory.</returns>
        IEnumerable<AllergenCategory> GetAllAllergenCategories(int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the allergen category by identifier.
        /// </summary>
        /// <param name="allergenCatId">The allergen cat identifier.</param>
        /// <returns>AllergenCategory.</returns>
        AllergenCategory GetAllergenCategoryById(int allergenCatId);
        /// <summary>
        /// Creates the allergen category.
        /// </summary>
        /// <param name="allergenCat">The allergen cat.</param>
        /// <returns>AllergenCategory.</returns>
        AllergenCategory CreateAllergenCategory(AllergenCategory allergenCat);
        /// <summary>
        /// Modifies the allergen category.
        /// </summary>
        /// <param name="allergenCat">The allergen cat.</param>
        /// <returns>AllergenCategory.</returns>
        AllergenCategory ModifyAllergenCategory(AllergenCategory allergenCat);
        /// <summary>
        /// Deletes the allergen category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteAllergenCategory(int id);

        //Allergens
        /// <summary>
        /// Gets all allergen.
        /// </summary>
        /// <param name="allergenCatId">The allergen cat identifier.</param>
        /// <returns>IEnumerable Allergen.</returns>
        IEnumerable<Allergen> GetAllAllergen(int allergenCatId);
        /// <summary>
        /// Creates the allergen.
        /// </summary>
        /// <param name="allergen">The allergen.</param>
        /// <returns>Allergen.</returns>
        Allergen CreateAllergen(Allergen allergen);
        /// <summary>
        /// Modifies the allergen.
        /// </summary>
        /// <param name="allergen">The allergen.</param>
        /// <returns>Allergen.</returns>
        Allergen ModifyAllergen(Allergen allergen);
        /// <summary>
        /// Gets the allergen by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Allergen.</returns>
        Allergen GetAllergenById(int id);
        /// <summary>
        /// Deletes the allergen.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteAllergen(int id);
        #endregion Allergen

        #region Ingredient

        /// <summary>
        /// Creates the ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>Ingredient.</returns>
        Ingredient CreateIngredient(Ingredient ingredient);
        /// <summary>
        /// Modifies the ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>Ingredient.</returns>
        Ingredient ModifyIngredient(Ingredient ingredient);

        /// <summary>
        /// Gets the ingredient.
        /// </summary>
        /// <param name="ingredient_Id">The ingredient identifier.</param>
        /// <returns>Ingredient.</returns>
        Ingredient GetIngredient(int ingredient_Id);

        #endregion Ingredient

        #region Languages

        /// <summary>
        /// Gets all languages.
        /// </summary>
        /// <returns>IEnumerable Language.</returns>
        IEnumerable<Language> GetAllLanguages();

        /// <summary>
        /// Gets the language by culture.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>Language.</returns>
        Language GetLanguageByCulture(string culture);

        #endregion Languages

        #region ChainTable

        /// <summary>
        /// Gets all chains.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        IEnumerable<ChainTable> GetAllChains(int customerId);

        /// <summary>
        /// Gets the un assign chain restaurant.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetUnAssignChainRestaurant();

        /// <summary>
        /// Creates the chain.
        /// </summary>
        /// <param name="chainTable">The chain table.</param>
        /// <returns>ChainTable.</returns>
        ChainTable CreateChain(ChainTable chainTable);

        /// <summary>
        /// Gets the chain by identifier.
        /// </summary>
        /// <param name="chainId">The chain identifier.</param>
        /// <returns>ChainTable.</returns>
        ChainTable GetChainById(int chainId);
        /// <summary>
        /// Gets the chains by identifier.
        /// </summary>
        /// <param name="chainId">The chain identifier.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        IEnumerable<ChainTable> GetChainsById(int chainId);

        /// <summary>
        /// Gets the assign chain restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAssignChainRestaurant(int id);
        /// <summary>
        /// Gets the chain recent restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetChainRecentRestaurants(int id);

        /// <summary>
        /// Modifies the chain.
        /// </summary>
        /// <param name="chainTable">The chain table.</param>
        /// <returns>ChainTable.</returns>
        ChainTable ModifyChain(ChainTable chainTable);

        /// <summary>
        /// Deletes the chain.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteChain(int id);

        /// <summary>
        /// Gets all recent orders of chain admin.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="flag">The flag.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetAllRecentOrdersOfChainAdmin(int customerId, string fromDate, string toDate, int restId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total, int? flag);
        /// <summary>
        /// Gets all chain recent order.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetAllChainRecentOrder(int customerId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets all rest of chain admin by search text.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetAllRestOfChainAdminBySearchText(int customerId, string restaurant_name);

        /// <summary>
        /// Gets the name of all chains by.
        /// </summary>
        /// <param name="chain_name">Name of the chain.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        IEnumerable<ChainTable> GetAllChainsByName(string chain_name);

        /// <summary>
        /// Gets the name of all default chains by.
        /// </summary>
        /// <param name="chainName">Name of the chain.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        IEnumerable<ChainTable> GetAllDefaultChainsByName(string chainName);
        /// <summary>
        /// Gets all rest chain reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        IEnumerable<RestaurantReview> GetAllRestChainReviews(int customerId);
        /// <summary>
        /// Gets all hotel chain reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        IEnumerable<HotelReview> GetAllHotelChainReviews(int customerId);

        #endregion ChainTable

        #region MenuItem Suggestion

        /// <summary>
        /// Gets the restaurant menu item suggestions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemSuggestion.</returns>
        IEnumerable<MenuItemSuggestion> GetRestaurantMenuItemSuggestions(int menuItemId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the unassign menu item suggetions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetUnassignMenuItemSuggetions(int menuItemId, int restaurantId);
        /// <summary>
        /// Creates the menu item suggestion.
        /// </summary>
        /// <param name="menuItemSuggestion">The menu item suggestion.</param>
        void CreateMenuItemSuggestion(List<MenuItemSuggestion> menuItemSuggestion);
        /// <summary>
        /// Deletes the menu item suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteMenuItemSuggestion(int id);

        #endregion MenuItem Suggestion

        #region Nutrition

        /// <summary>
        /// Gets all nutritions.
        /// </summary>
        /// <returns>IEnumerable Nutrition.</returns>
        IEnumerable<Nutrition> GetAllNutritions();
        /// <summary>
        /// Gets the un assigned nutritions.
        /// </summary>
        /// <param name="IngredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable Nutrition.</returns>
        IEnumerable<Nutrition> GetUnAssignedNutritions(int IngredientId);
        /// <summary>
        /// Gets the nutritions.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Nutrition.</returns>
        IEnumerable<Nutrition> GetNutritions(int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the nutrition.
        /// </summary>
        /// <param name="nutrition">The nutrition.</param>
        /// <returns>Nutrition.</returns>
        Nutrition CreateNutrition(Nutrition nutrition);
        /// <summary>
        /// Deletes the nutrition.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteNutrition(int id);

        #endregion Nutrition

        #region Ingredient Nutrition Values

        /// <summary>
        /// Gets the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable IngredientNutritionValues.</returns>
        IEnumerable<IngredientNutritionValues> GetIngredientNutritionValues(int ingredientId);
        /// <summary>
        /// Gets the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable IngredientNutritionValues.</returns>
        IEnumerable<IngredientNutritionValues> GetIngredientNutritionValues(int ingredientId, int skip, int pageSize, out int total);
        /// <summary>
        /// Creates the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientNutritionValues">The ingredient nutrition values.</param>
        /// <returns>IngredientNutritionValues.</returns>
        IngredientNutritionValues CreateIngredientNutritionValues(IngredientNutritionValues ingredientNutritionValues);
        /// <summary>
        /// Gets the ingredient nutrition values by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IngredientNutritionValues.</returns>
        IngredientNutritionValues GetIngredientNutritionValuesById(int id);
        /// <summary>
        /// Modifies the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientNutritionValues">The ingredient nutrition values.</param>
        /// <returns>IngredientNutritionValues.</returns>
        IngredientNutritionValues ModifyIngredientNutritionValues(IngredientNutritionValues ingredientNutritionValues);
        /// <summary>
        /// Deletes the ingredient nutrition values.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteIngredientNutritionValues(int id);

        #endregion Ingredient Nutrition Values
    }
}
