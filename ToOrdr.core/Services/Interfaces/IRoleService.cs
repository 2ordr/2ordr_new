﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="IRoleService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface IRoleService
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Role}" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Role}" />
    public interface IRoleService : IServiceBase<Role>
    {
        /// <summary>
        /// Gets the roles for user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>IList Role.</returns>
        IList<Role> GetRolesForUser(string username);

        /// <summary>
        /// Determines whether [is user in role] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="role">The role.</param>
        /// <returns><c>true</c> if [is user in role] [the specified username]; otherwise, <c>false</c>.</returns>
        bool IsUserInRole(string username, string role);
        /// <summary>
        /// Gets the restaurant by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Restaurant.</returns>
        Restaurant GetRestaurantByCustomerId(int customerId);
        /// <summary>
        /// Gets the hotel by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Hotel.</returns>
        Hotel GetHotelByCustomerId(int customerId);
    }
}
