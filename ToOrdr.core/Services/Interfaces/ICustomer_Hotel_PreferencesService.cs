﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="ICustomer_Hotel_PreferencesService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface ICustomer_Hotel_PreferencesService
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Customer_Hotel_Preferences}" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IServiceBase{ToOrdr.Core.Entities.Customer_Hotel_Preferences}" />
    public interface ICustomer_Hotel_PreferencesService : IServiceBase<Customer_Hotel_Preferences>
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="func">The function.</param>
        /// <returns>IEnumerable Customer_Hotel_Preferences.</returns>
        IEnumerable<Customer_Hotel_Preferences> GetAll(Func<Customer_Hotel_Preferences, bool> func);
    }
}
