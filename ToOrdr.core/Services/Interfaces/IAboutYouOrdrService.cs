﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 12-27-2018
//
// Last Modified By : Manoj
// Last Modified On : 12-27-2018
// ***********************************************************************
// <copyright file="IAboutYouOrdrService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface IAboutYouOrdrService
    /// </summary>
    public interface IAboutYouOrdrService
    {
        /// <summary>
        /// Gets youordr description.
        /// </summary>
        /// <param name="langId">The language identifier.</param>
        /// <returns>AboutYouOrdr.</returns>
        AboutYouOrdr GetYouOrdrDescription(int langId);

        /// <summary>
        /// Gets all languages youordr description.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable AboutYouOrdr.</returns>
        IEnumerable<AboutYouOrdr> GetAllLanguagesYouOrdrDescription(int skip, int pageSize, out int total);

        /// <summary>
        /// Gets youordr description by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>AboutYouOrdr.</returns>
        AboutYouOrdr GetYouOrdrDescriptionById(int id);

        /// <summary>
        /// Creates the about youordr description.
        /// </summary>
        /// <param name="abtYouOrdr">The abt youordr.</param>
        /// <returns>AboutYouOrdr.</returns>
        AboutYouOrdr CreateAboutYouOrdrDescription(AboutYouOrdr abtYouOrdr);

        /// <summary>
        /// Modifies the about youordr descriotion.
        /// </summary>
        /// <param name="abtYouOrdr">The abt youordr.</param>
        /// <returns>AboutYouOrdr.</returns>
        AboutYouOrdr ModifyAboutYouOrdrDescriotion(AboutYouOrdr abtYouOrdr);

        /// <summary>
        /// Deletes the about youordr desciption.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteAboutYouOrdrDesciption(int id);

        /// <summary>
        /// Gets the unassigned languages.
        /// </summary>
        /// <returns>IEnumerable Language.</returns>
        IEnumerable<Language> GetUnassignedLanguages();

    }
}
