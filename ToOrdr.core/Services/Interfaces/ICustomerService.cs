﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-18-2019
// ***********************************************************************
// <copyright file="ICustomerService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Interfaces
{
    /// <summary>
    /// Interface ICustomerService
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Validates the credentials.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool ValidateCredentials(string email, string password);

        /// <summary>
        /// Validates the backend credentials.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool ValidateBackendCredentials(string email, string password);

        /// <summary>
        /// Validates the external login.
        /// </summary>
        /// <param name="providerKey">The provider key.</param>
        /// <param name="provider">The provider.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool ValidateExternalLogin(string providerKey, string provider);

        #region customer

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetAllCustomers();

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="Email">The email.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetAllCustomers(string name, string Email, int skip, int pageSize, out int total);

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetAllCustomers(Func<Customer, bool> predicate);

        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Customer.</returns>
        Customer GetCustomerById(int id);

        /// <summary>
        /// Gets the customer by telephone.
        /// </summary>
        /// <param name="telephone">The telephone.</param>
        /// <returns>Customer.</returns>
        Customer GetCustomerByTelephone(string telephone);
        /// <summary>
        /// Gets the customer by email identifier.
        /// </summary>
        /// <param name="emailId">The email identifier.</param>
        /// <returns>Customer.</returns>
        Customer GetCustomerByEmailId(string emailId);

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        Customer CreateCustomer(Customer details);

        /// <summary>
        /// Creates the external customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        Customer CreateExternalCustomer(Customer details);

        /// <summary>
        /// Creates the customer log.
        /// </summary>
        /// <param name="log">The log.</param>
        void CreateCustomerLog(CustomerLog log);

        /// <summary>
        /// Modifies the customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        Customer ModifyCustomer(Customer details);

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        Customer UpdatePassword(Customer details);

        /// <summary>
        /// Updates the email.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        Customer UpdateEmail(Customer details);

        /// <summary>
        /// Deletes the account.
        /// </summary>
        /// <param name="details">The details.</param>
        void DeleteAccount(Customer details);

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomer(int id);

        /// <summary>
        /// Gets all deleted customers.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Customer.</returns>
        IEnumerable<Customer> GetAllDeletedCustomers(int skip, int pageSize, out int total);

        #endregion

        #region Customer Preferred Food Profile
        /// <summary>
        /// Gets the customer preferred food profile.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        CustomerPreferredFoodProfile GetCustomerPreferredFoodProfile(int customerId);

        /// <summary>
        /// Creates the customer food profile.
        /// </summary>
        /// <param name="prefFoodProfile">The preference food profile.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        CustomerPreferredFoodProfile CreateCustomerFoodProfile(CustomerPreferredFoodProfile prefFoodProfile);

        /// <summary>
        /// Updates the customer preferred food profile.
        /// </summary>
        /// <param name="prefFoodProfile">The preference food profile.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        CustomerPreferredFoodProfile UpdateCustPreferredFoodProfile(CustomerPreferredFoodProfile prefFoodProfile);

        #endregion Customer Preffered Food Profile
        /// <summary>
        /// Customers the authorized restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> CustomerAuthorizedRestaurants(int id);

        /// <summary>
        /// Customers the authorized hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        IEnumerable<Hotel> CustomerAuthorizedHotels(int id);


        #region bookings

        /// <summary>
        /// Gets the bookings.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Hotel_Bookings.</returns>
        IEnumerable<Customer_Hotel_Bookings> GetBookings(int Id);

        /// <summary>
        /// Gets the booking by identifier.
        /// </summary>
        /// <param name="bookingId">The booking identifier.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        Customer_Hotel_Bookings GetBookingById(int bookingId);

        /// <summary>
        /// Creates the booking.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        Customer_Hotel_Bookings CreateBooking(Customer_Hotel_Bookings hotelBooking);

        /// <summary>
        /// Modifies the booking.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        Customer_Hotel_Bookings ModifyBooking(Customer_Hotel_Bookings hotelBooking);

        /// <summary>
        /// Deletes the booking.
        /// </summary>
        /// <param name="customer_hotel_booking_id">The customer hotel booking identifier.</param>
        void DeleteBooking(int customer_hotel_booking_id);

        #endregion

        #region restaurant preferenecs
        /// <summary>
        /// Modifies the ingredient preference.
        /// </summary>
        /// <param name="ingredientPreference">The ingredient preference.</param>
        /// <returns>System.String.</returns>
        string ModifyIngredientPreference(CustomerIngredientPreference ingredientPreference);
        /// <summary>
        /// Modifies the allergen preference.
        /// </summary>
        /// <param name="allergenPreference">The allergen preference.</param>
        /// <param name="include">if set to <c>true</c> [include].</param>
        /// <returns>System.String.</returns>
        string ModifyAllergenPreference(CustomerAllergenPreference allergenPreference, bool include);
        /// <summary>
        /// Gets the customer allergen prefernecs.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllergenPreference.</returns>
        IEnumerable<CustomerAllergenPreference> GetCustomerAllergenPrefernecs(int customerId);
        /// <summary>
        /// Gets the customer allergen by preference ingredient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable CustomerAllergenPreference.</returns>
        IEnumerable<CustomerAllergenPreference> GetCustAllergenByPrefIngredient(int customerId, int ingredientId);
        /// <summary>
        /// Gets the type of the customer ingredient preference by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="type">if set to <c>true</c> [type].</param>
        /// <returns>IEnumerable CustomerIngredientPreference.</returns>
        IEnumerable<CustomerIngredientPreference> GetCustomerIngredientPreferenceByType(int customerId, bool type);
        /// <summary>
        /// Gets the customer ingred preference by allergic ingredient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="allergicIngredients">The allergic ingredients.</param>
        /// <returns>IEnumerable CustomerIngredientPreference.</returns>
        IEnumerable<CustomerIngredientPreference> GetCustIngredPrefByAllergicIngredient(int customerId, List<int> allergicIngredients);
        /// <summary>
        /// Gets the customer restaurant preference.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        Customer_Restaurant_Preference GetCustomerRestaurantPreference(int Id);
        /// <summary>
        /// Gets the restaurant preferences.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Restaurant_Preference.</returns>
        IEnumerable<Customer_Restaurant_Preference> GetRestaurantPreferences(int Id);

        /// <summary>
        /// Gets the restaurant preferences by identifier.
        /// </summary>
        /// <param name="preferenceId">The preference identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        Customer_Restaurant_Preference GetRestaurantPreferencesById(int preferenceId);

        /// <summary>
        /// Creates the restaurant preference.
        /// </summary>
        /// <param name="restaurantPreference">The restaurant preference.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        Customer_Restaurant_Preference CreateRestaurantPreference(Customer_Restaurant_Preference restaurantPreference);

        /// <summary>
        /// Modifies the restaurant preference.
        /// </summary>
        /// <param name="customerPreference">The customer preference.</param>
        /// <param name="Id">The identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        Customer_Restaurant_Preference ModifyRestaurantPreference(CustomerPreferences customerPreference, int Id);

        /// <summary>
        /// Deletes the restaurant preference.
        /// </summary>
        /// <param name="customer_restaurant_preference_id">The customer restaurant preference identifier.</param>
        void DeleteRestaurantPreference(int customer_restaurant_preference_id);

        /// <summary>
        /// Gets the customer restaurant preference allergen.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_AllergenPreference.</returns>
        IEnumerable<Customer_AllergenPreference> GetCustomerRestaurantPreferenceAllergen(int Id);

        /// <summary>
        /// Gets the allergen preference by identifier.
        /// </summary>
        /// <param name="allergenId">The allergen identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerAllergenPreference.</returns>
        CustomerAllergenPreference GetAllergenPreferenceById(int allergenId, int customerId);
        /// <summary>
        /// Gets the customer preferred menus.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        IEnumerable<RestaurantMenu> GetCustomerPreferredMenus(int customerId);
        /// <summary>
        /// Gets the customer preferred menu items.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetCustomerPreferredMenuItems(int customerId);

        #endregion

        #region favorite restaurants

        /// <summary>
        /// Customers the favorite restaurant toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        void CustomerFavoriteRestaurantToggle(int Id, int RestaurantId);
        /// <summary>
        /// Gets the favorite restaurants.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetFavoriteRestaurants(int Id);
        /// <summary>
        /// Gets the favorite restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        IEnumerable<Restaurant> GetFavoriteRestaurants(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Deletes the favorite restaurant.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        void DeleteFavoriteRestaurant(int restaurantId, int customerId);
        #endregion favorite restaurants

        #region Favorite Hotel
        /// <summary>
        /// Customers the favorite hotel toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="HotelId">The hotel identifier.</param>
        void CustomerFavoriteHotelToggle(int Id, int HotelId);

        /// <summary>
        /// Gets the favorite hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel </returns>
        IEnumerable<Hotel> GetFavoriteHotels(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the favorite hotels.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable&lt;Hotel&gt;.</returns>
        IEnumerable<Hotel> GetFavoriteHotels(int Id);
        /// <summary>
        /// Deletes the favorite hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        void DeleteFavoriteHotel(int hotelId, int customerId);

        #endregion Favorite Hotel

        #region hotel preferences
        /// <summary>
        /// Gets the hotel preferences.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Hotel_Preferences.</returns>
        IEnumerable<Customer_Hotel_Preferences> GetHotelPreferences(int Id);

        /// <summary>
        /// Gets the hotel preferences by identifier.
        /// </summary>
        /// <param name="preferenceId">The preference identifier.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        Customer_Hotel_Preferences GetHotelPreferencesById(int preferenceId);

        /// <summary>
        /// Creates the hotel preference.
        /// </summary>
        /// <param name="hotelPreference">The hotel preference.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        Customer_Hotel_Preferences CreateHotelPreference(Customer_Hotel_Preferences hotelPreference);

        /// <summary>
        /// Modifies the hotel preference.
        /// </summary>
        /// <param name="hotelPreference">The hotel preference.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        Customer_Hotel_Preferences ModifyHotelPreference(Customer_Hotel_Preferences hotelPreference);

        /// <summary>
        /// Deletes the hotel preference.
        /// </summary>
        /// <param name="customer_hotel_preference_id">The customer hotel preference identifier.</param>
        void DeleteHotelPreference(int customer_hotel_preference_id);


        #endregion

        #region orders
        /// <summary>
        /// Gets the customer recent orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetCustomerRecentOrders(int customerId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the customer orders queue.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetCustomerOrdersQueue(int customerId, int skip, int pageSize, out int total);
        /// <summary>
        /// Gets the restaurant orders.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetRestaurantOrders(int Id);

        /// <summary>
        /// Gets the restaurant orders.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        IEnumerable<RestaurantOrder> GetRestaurantOrders(int Id, int RestaurantId);

        /// <summary>
        /// Gets the restaurant order by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder GetRestaurantOrderById(int id);

        /// <summary>
        /// Creates the restaurant order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder CreateRestaurantOrder(RestaurantOrder order);

        /// <summary>
        /// Modifies the restaurant order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>RestaurantOrder.</returns>
        RestaurantOrder ModifyRestaurantOrder(RestaurantOrder order);

        /// <summary>
        /// Deletes the restaurant order.
        /// </summary>
        /// <param name="customer_Id">The customer identifier.</param>
        void DeleteRestaurantOrder(int customer_Id);


        /// <summary>
        /// Adds the restaurant order item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>Customer_RestaurantOrder_Items.</returns>
        Customer_RestaurantOrder_Items AddRestaurantOrderItem(Customer_RestaurantOrder_Items item);
        /// <summary>
        /// Modifies the restaurant order item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>Customer_RestaurantOrder_Items.</returns>
        Customer_RestaurantOrder_Items ModifyRestaurantOrderItem(Customer_RestaurantOrder_Items item);

        /// <summary>
        /// Removes the restaurant order item.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        void RemoveRestaurantOrderItem(int itemId);


        /// <summary>
        /// Gets the restaurant item custamisations.
        /// </summary>
        /// <param name="customer_RestaurantOrder_item_id">The customer restaurant order item identifier.</param>
        /// <returns>IEnumerable Customer_RestaurantOrder_Item_customisation.</returns>
        IEnumerable<Customer_RestaurantOrder_Item_customisation> GetRestaurantItemCustamisations(int customer_RestaurantOrder_item_id);

        /// <summary>
        /// Gets the restaurant item custamisations by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        Customer_RestaurantOrder_Item_customisation GetRestaurantItemCustamisationsById(int id);

        /// <summary>
        /// Adds the restaurant item customisation.
        /// </summary>
        /// <param name="customisation">The customisation.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        Customer_RestaurantOrder_Item_customisation AddRestaurantItemCustomisation(Customer_RestaurantOrder_Item_customisation customisation);
        /// <summary>
        /// Modifies the restaurant item customisation.
        /// </summary>
        /// <param name="customisation">The customisation.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        Customer_RestaurantOrder_Item_customisation ModifyRestaurantItemCustomisation(Customer_RestaurantOrder_Item_customisation customisation);

        /// <summary>
        /// Removes the restaurant item customisation.
        /// </summary>
        /// <param name="customisationId">The customisation identifier.</param>
        void RemoveRestaurantItemCustomisation(int customisationId);



        #endregion

        #region Customer Hotel/Restaurant Orders
        /// <summary>
        /// Gets all hotel and rest orders of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllHotelAndRestActiveOrders.</returns>
        IEnumerable<CustomerAllHotelAndRestActiveOrders> GetAllHotelAndRestOrdersOfCustomer(int customerId);
        /// <summary>
        /// Gets the customer rest and hotels orders history.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllHotelAndRestOrderHistory.</returns>
        IEnumerable<CustomerAllHotelAndRestOrderHistory> GetCustomerRestAndHotelsOrdersHistory(int customerId);
        /// <summary>
        /// Deletes the customer orders history and active orders.
        /// </summary>
        /// <param name="orderType">Type of the order.</param>
        /// <param name="orderId">The order identifier.</param>
        void DeleteCustOrdersHistoryAndActiveOrders(string orderType, int orderId);
  
        #endregion Customer Hotel/Restaurant Orders

        #region credit cards
        /// <summary>
        /// Gets the customer card details by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>CustomerCreditCard.</returns>
        CustomerCreditCard GetCustomerCardDetailsById(int id);
        /// <summary>
        /// Gets the customer cards.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>IEnumerable CustomerPaymentCards.</returns>
        IEnumerable<CustomerPaymentCards> GetCustomerCards(int Id, string quickPayToken, string quickPayUrl);

        /// <summary>
        /// Gets the card by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        CustomerPaymentCards GetCardById(int id, string quickPayToken, string quickPayUrl);

        /// <summary>
        /// Creates the customer card.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        CustomerPaymentCards CreateCustomerCard(CustomerPaymentCards details, string quickPayToken, string quickPayUrl);

        /// <summary>
        /// Modifies the customer card.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="existingCard">The existing card.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        CustomerPaymentCards ModifyCustomerCard(CustomerPaymentCards details, CustomerCreditCard existingCard, string quickPayToken, string quickPayUrl);

        /// <summary>
        /// Deletes the customer card.
        /// </summary>
        /// <param name="cardDetails">The card details.</param>
        void DeleteCustomerCard(CustomerCreditCard cardDetails);

        #endregion credit cards

        #region paypal

        /// <summary>
        /// Updates the paypal details.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>PaypalDetail.</returns>
        PaypalDetail UpdatePaypalDetails(PaypalDetail details);

        /// <summary>
        /// Gets the paypal details.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>PaypalDetail.</returns>
        PaypalDetail GetPaypalDetails(int Id);

        /// <summary>
        /// Deletes the paypal details.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        void DeletePaypalDetails(int Id);
        #endregion paypal

        #region currency

        /// <summary>
        /// Gets all currencies.
        /// </summary>
        /// <returns>IEnumerable Currency.</returns>
        IEnumerable<Currency> GetAllCurrencies();

        /// <summary>
        /// Gets the currency by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Currency.</returns>
        Currency GetCurrencyById(int id);

        #endregion currency

        #region Favorite MenuItem

        /// <summary>
        /// Customers the favorite menu item toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        void CustomerFavoriteMenuItemToggle(int Id, int menuItemId);
        /// <summary>
        /// Gets the customer favorite menu items.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantFavoriteMenuItems.</returns>
        IEnumerable<RestaurantFavoriteMenuItems> GetCustomerFavoriteMenuItems(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the customer order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        CustomerOrderDetailsInfo GetCustomerOrderDetails(int customerId, int menuItemId);

        /// <summary>
        /// Gets the customer favorite menu items.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetCustomerFavoriteMenuItems(int id);

        #endregion Favorite MenuItem

        #region Country

        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <returns>IEnumerable Country.</returns>
        IEnumerable<Country> GetAllCountries();

        /// <summary>
        /// Gets the country by identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <returns>Country.</returns>
        Country GetCountryById(int countryId);

        #endregion Country

        /// <summary>
        /// Gets the menu items based on preferences.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        IEnumerable<RestaurantMenuItem> GetMenuItemsBasedOnPreferences(int customerId, int restaurantId, int pageNumber, int pageSize, out int total);

        #region Spa Ingredient Preference

        /// <summary>
        /// Gets the customer spa ingredient preference.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerSpaServicePreference.</returns>
        IEnumerable<CustomerSpaServicePreference> GetCustomerSpaIngredientPreference(int customerId);

        /// <summary>
        /// Modifies the spa ingredient preference.
        /// </summary>
        /// <param name="spaIngredientPref">The spa ingredient preference.</param>
        /// <returns>System.String.</returns>
        string ModifySpaIngredientPreference(CustomerSpaServicePreference spaIngredientPref);

        #endregion Spa Ingredient Preference

        #region Favorite Spa Service

        /// <summary>
        /// Customers the favorite spa service toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        void CustomerFavoriteSpaServiceToggle(int Id, int spaServiceDetailId);
        /// <summary>
        /// Gets the customer favorite spa service details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetCustomerFavoriteSpaServiceDetails(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the customer favorite spa service details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetCustomerFavoriteSpaServiceDetails(int customerId);
        /// <summary>
        /// Gets the customer spa order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        CustomerOrderDetailsInfo GetCustomerSpaOrderDetails(int customerId, int spaServiceDetailId);

        #endregion Favorite Spa Service

        #region Favorite Excursion Detail
        /// <summary>
        /// Customers the favorite excursion toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        void CustomerFavoriteExcursionToggle(int Id, int excursionDetailId);
        /// <summary>
        /// Gets the customer favorite excursion details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetCustomerFavoriteExcursionDetails(int id, int pageNumber, int pageSize, out int total);
        /// <summary>
        /// Gets the customer favorite excursion details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetCustomerFavoriteExcursionDetails(int customerId);
        /// <summary>
        /// Gets the customer excursion order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        CustomerOrderDetailsInfo GetCustomerExcursionOrderDetails(int customerId, int excursionDetailId);
        #endregion Favorite Excursion Detail

        #region Excursion Ingredient Preference
        /// <summary>
        /// Gets the customer excursion ingredient preference.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerExcursionPreferences.</returns>
        IEnumerable<CustomerExcursionPreferences> GetCustomerExcursionIngredientPreference(int customerId);
        /// <summary>
        /// Modifies the excursion ingredient preference.
        /// </summary>
        /// <param name="excursionIngredientPref">The excursion ingredient preference.</param>
        /// <returns>System.String.</returns>
        string ModifyExcursionIngredientPreference(CustomerExcursionPreferences excursionIngredientPref);
        #endregion Excursion Ingredient Preference

        #region language

        /// <summary>
        /// Gets all languages.
        /// </summary>
        /// <returns>IEnumerable Language.</returns>
        IEnumerable<Language> GetAllLanguages();

        #endregion language

        #region WakeUp

        /// <summary>
        /// Gets the customer wake up call.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable WakeUp.</returns>
        IEnumerable<WakeUp> GetCustomerWakeUpCall(int customerId);
        /// <summary>
        /// Gets the customer wake upcall.
        /// </summary>
        /// <param name="wakeupId">The wakeup identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>WakeUp.</returns>
        WakeUp GetCustomerWakeUpcall(int wakeupId, int customerId);
        /// <summary>
        /// Creates the customer wake upcall.
        /// </summary>
        /// <param name="wakeUp">The wake up.</param>
        /// <returns>WakeUp.</returns>
        WakeUp CreateCustomerWakeUpcall(WakeUp wakeUp);
        /// <summary>
        /// Deletes the customer wake up call.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        void DeleteCustomerWakeUpCall(int Id);

        #endregion WakeUp

        #region Spa Pending Reviews
        /// <summary>
        /// Gets the spa service detail pending reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        IEnumerable<SpaServiceDetail> GetSpaServiceDetailPendingReviews(int customerId, int pageNumber, int pageSize, out int total);
        #endregion Spa Pending Reviews

        #region Excursion Details Pending Reviews
        /// <summary>
        /// Gets the excursion detail pending reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        IEnumerable<ExcursionDetail> GetExcursionDetailPendingReviews(int customerId, int pageNumber, int pageSize, out int total);

        #endregion Excursion Details Pending Reviews

        #region Hotel Services Reviews
        /// <summary>
        /// Gets the hotel all services reviews of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelAllServicesReviews.</returns>
        IEnumerable<HotelAllServicesReviews> GetHotelAllServicesReviewsOfCust(int customerId, int pageNumber, int pageSize, out int total);
        #endregion Hotel Services Reviews

        #region Customer Payment History
        /// <summary>
        /// Gets the customer hotel and rest payment history.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="customerPrefCurrencyId">The customer preference currency identifier.</param>
        /// <param name="year">The year.</param>
        /// <returns>IEnumerable CustomerMonthlyPaymentHistory.</returns>
        IEnumerable<CustomerMonthlyPaymentHistory> GetCustomerHotelAndRestPaymentHistory(int customerId, int customerPrefCurrencyId, int year);
        #endregion Customer Payment History
    }
}
