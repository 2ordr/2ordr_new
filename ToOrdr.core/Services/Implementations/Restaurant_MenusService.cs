﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="Restaurant_MenusService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class RestaurantMenusService.
    /// Implements the <see cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.RestaurantMenu},ToOrdr.Core.Entities.RestaurantMenu}" />
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IRestaurantMenusService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.RestaurantMenu},ToOrdr.Core.Entities.RestaurantMenu}" />
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IRestaurantMenusService" />
    public class RestaurantMenusService : ImplementationBase<IRepository<RestaurantMenu>, RestaurantMenu>, IRestaurantMenusService
    {
        /// <summary>
        /// The restaurant menus repository
        /// </summary>
        IRepository<RestaurantMenu> _RestaurantMenusRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantMenusService"/> class.
        /// </summary>
        /// <param name="RestaurantMenusRepository">The restaurant menus repository.</param>
        public RestaurantMenusService(IRepository<RestaurantMenu> RestaurantMenusRepository)
            : base(RestaurantMenusRepository)
        {
            _RestaurantMenusRepository = RestaurantMenusRepository;
        }
    }
}
