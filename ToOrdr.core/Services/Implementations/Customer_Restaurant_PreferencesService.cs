﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="Customer_Restaurant_PreferencesService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class Customer_Restaurant_PreferencesService.
    /// Implements the <see cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Customer_Restaurant_Preference},ToOrdr.Core.Entities.Customer_Restaurant_Preference}" />
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.ICustomer_Restaurant_PreferencesService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Customer_Restaurant_Preference},ToOrdr.Core.Entities.Customer_Restaurant_Preference}" />
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.ICustomer_Restaurant_PreferencesService" />
    public class Customer_Restaurant_PreferencesService : ImplementationBase<IRepository<Customer_Restaurant_Preference>, Customer_Restaurant_Preference>, ICustomer_Restaurant_PreferencesService
    {
        /// <summary>
        /// The customer restaurant preferences repository
        /// </summary>
        IRepository<Customer_Restaurant_Preference> _customer_Restaurant_PreferencesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer_Restaurant_PreferencesService"/> class.
        /// </summary>
        /// <param name="customer_Restaurant_PreferencesRepository">The customer restaurant preferences repository.</param>
        public Customer_Restaurant_PreferencesService(IRepository<Customer_Restaurant_Preference> customer_Restaurant_PreferencesRepository)
            : base(customer_Restaurant_PreferencesRepository)
        {
            _customer_Restaurant_PreferencesRepository = customer_Restaurant_PreferencesRepository;
        }
    }
}
