﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-18-2019
// ***********************************************************************
// <copyright file="CustomerService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Exceptions;
using ToOrdr.Core.Services.Interfaces;
using ToOrdr.Localization;
using ToOrdr.Core.Extensions;
using System.Data.Entity;
using ToOrdr.Core.Util;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class CustomerService.
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.ICustomerService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.ICustomerService" />
    public class CustomerService : ICustomerService
    {
        #region initialization
        /// <summary>
        /// The context
        /// </summary>
        private readonly ToOrdrDbContext _context = new ToOrdrDbContext();

        //ImplementationBase<IRepository<Customer>, Customer>,

        /// <summary>
        /// The customer details repository
        /// </summary>
        IRepository<Customer> _customerDetailsRepository;
        /// <summary>
        /// The customer hotel bookings
        /// </summary>
        IRepository<Customer_Hotel_Bookings> _customerHotelBookings;
        /// <summary>
        /// The customer restaurant preferences
        /// </summary>
        IRepository<Customer_Restaurant_Preference> _customerRestaurantPreferences;
        /// <summary>
        /// The customer hotel preferences
        /// </summary>
        IRepository<Customer_Hotel_Preferences> _customerHotelPreferences;
        /// <summary>
        /// The customer restaurant orders
        /// </summary>
        IRepository<RestaurantOrder> _customerRestaurantOrders;
        /// <summary>
        /// The customer restaurant order items
        /// </summary>
        IRepository<Customer_RestaurantOrder_Items> _customerRestaurantOrderItems;
        /// <summary>
        /// The customer restaurant order item customisation
        /// </summary>
        IRepository<Customer_RestaurantOrder_Item_customisation> _customerRestaurantOrderItemCustomisation;
        /// <summary>
        /// The customer role repository
        /// </summary>
        IRepository<CustomerRole> _customerRoleRepository;
        /// <summary>
        /// The customer credit cards repository
        /// </summary>
        IRepository<CustomerCreditCard> _customerCreditCardsRepository;
        /// <summary>
        /// The customer allergen preference repository
        /// </summary>
        IRepository<CustomerAllergenPreference> _customerAllergenPreferenceRepository;
        /// <summary>
        /// The paypal details repository
        /// </summary>
        IRepository<PaypalDetail> _paypalDetailsRepository;
        /// <summary>
        /// The customer favorite restaurant repository
        /// </summary>
        IRepository<CustomerFavoriteRestaurant> _customerFavoriteRestaurantRepository;
        /// <summary>
        /// The customer favorite hotel repository
        /// </summary>
        IRepository<CustomerFavoriteHotel> _customerFavoriteHotelRepository;
        /// <summary>
        /// The currency details repository
        /// </summary>
        IRepository<Currency> _currencyDetailsRepository;
        /// <summary>
        /// The customer ingredient preferences repository
        /// </summary>
        IRepository<CustomerIngredientPreference> _customerIngredientPreferencesRepository;
        /// <summary>
        /// The customer favorite menu item repository
        /// </summary>
        IRepository<CustomerFavoriteMenuItem> _customerFavoriteMenuItemRepository;
        /// <summary>
        /// The country repository
        /// </summary>
        IRepository<Country> _countryRepository;
        /// <summary>
        /// The customer menu items repository
        /// </summary>
        IRepository<RestaurantMenuItem> _customerMenuItemsRepository;
        /// <summary>
        /// The customer spa service preference repository
        /// </summary>
        IRepository<CustomerSpaServicePreference> _customerSpaServicePreferenceRepository;
        /// <summary>
        /// The spa service repository
        /// </summary>
        IRepository<SpaService> _spaServiceRepository;
        /// <summary>
        /// The customer favorite spa service repository
        /// </summary>
        IRepository<CustomerFavoriteSpaService> _customerFavoriteSpaServiceRepository;
        /// <summary>
        /// The language repository
        /// </summary>
        IRepository<Language> _languageRepository;
        /// <summary>
        /// The customer log repository
        /// </summary>
        IRepository<CustomerLog> _customerLogRepository;
        /// <summary>
        /// The customer preferred food profile repository
        /// </summary>
        IRepository<CustomerPreferredFoodProfile> _customerPreferredFoodProfileRepository;
        /// <summary>
        /// The customer all orders repository
        /// </summary>
        IRepository<VW_YourPlansDashboard> _customerAllOrdersRepository;
        /// <summary>
        /// The customer orders history repository
        /// </summary>
        IRepository<VW_CustomerOrdersHistory> _customerOrdersHistoryRepository;
        /// <summary>
        /// The customer spa order repository
        /// </summary>
        IRepository<SpaOrder> _customerSpaOrderRepository;
        /// <summary>
        /// The customer spa order item repository
        /// </summary>
        IRepository<SpaOrderItem> _customerSpaOrderItemRepository;
        /// <summary>
        /// The customer laundry order items repository
        /// </summary>
        IRepository<LaundryOrderItems> _customerLaundryOrderItemsRepository;
        /// <summary>
        /// The customer housekeeping order item repository
        /// </summary>
        IRepository<HousekeepingOrderItem> _customerHousekeepingOrderItemRepository;
        /// <summary>
        /// The wake up repository
        /// </summary>
        IRepository<WakeUp> _wakeUpRepository;
        /// <summary>
        /// The hotel rooms repository
        /// </summary>
        IRepository<Room> _hotelRoomsRepository;
        /// <summary>
        /// The customer favorite excursion repository
        /// </summary>
        IRepository<CustomerFavoriteExcursion> _customerFavoriteExcursionRepository;
        /// <summary>
        /// The customer excursion preference repository
        /// </summary>
        IRepository<CustomerExcursionPreferences> _customerExcursionPreferenceRepository;
        /// <summary>
        /// The customer excursion order repository
        /// </summary>
        IRepository<ExcursionOrder> _customerExcursionOrderRepository;
        /// <summary>
        /// The customer excursion order items repository
        /// </summary>
        IRepository<ExcursionOrderItems> _customerExcursionOrderItemsRepository;

        /// <summary>
        /// The customer operations
        /// </summary>
        CRUDOperation<IRepository<Customer>, Customer> _customerOperations;
        /// <summary>
        /// The customer role operations
        /// </summary>
        CRUDOperation<IRepository<CustomerRole>, CustomerRole> _customerRoleOperations;
        /// <summary>
        /// The customer credit cards operation
        /// </summary>
        CRUDOperation<IRepository<CustomerCreditCard>, CustomerCreditCard> _customerCreditCardsOperation;
        /// <summary>
        /// The customer ingredient preferences operation
        /// </summary>
        CRUDOperation<IRepository<CustomerIngredientPreference>, CustomerIngredientPreference> _customerIngredientPreferencesOperation;
        /// <summary>
        /// The customer allergen preference operation
        /// </summary>
        CRUDOperation<IRepository<CustomerAllergenPreference>, CustomerAllergenPreference> _customerAllergenPreferenceOperation;
        /// <summary>
        /// The customer favourite restaurant operation
        /// </summary>
        CRUDOperation<IRepository<CustomerFavoriteRestaurant>, CustomerFavoriteRestaurant> _customerFavouriteRestaurantOperation;
        /// <summary>
        /// The customer favourite hotel operation
        /// </summary>
        CRUDOperation<IRepository<CustomerFavoriteHotel>, CustomerFavoriteHotel> _customerFavouriteHotelOperation;
        /// <summary>
        /// The currency operation
        /// </summary>
        CRUDOperation<IRepository<Currency>, Currency> _currencyOperation;
        /// <summary>
        /// The customer favorite menu item operation
        /// </summary>
        CRUDOperation<IRepository<CustomerFavoriteMenuItem>, CustomerFavoriteMenuItem> _customerFavoriteMenuItemOperation;
        /// <summary>
        /// The customer menu items operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuItem>, RestaurantMenuItem> _customerMenuItemsOperations;
        /// <summary>
        /// The customer spa service preference operation
        /// </summary>
        CRUDOperation<IRepository<CustomerSpaServicePreference>, CustomerSpaServicePreference> _customerSpaServicePreferenceOperation;
        /// <summary>
        /// The spa service operations
        /// </summary>
        CRUDOperation<IRepository<SpaService>, SpaService> _spaServiceOperations;
        /// <summary>
        /// The customer favorite spa service operations
        /// </summary>
        CRUDOperation<IRepository<CustomerFavoriteSpaService>, CustomerFavoriteSpaService> _customerFavoriteSpaServiceOperations;
        /// <summary>
        /// The language operation
        /// </summary>
        CRUDOperation<IRepository<Language>, Language> _languageOperation;
        /// <summary>
        /// The customer log operation
        /// </summary>
        CRUDOperation<IRepository<CustomerLog>, CustomerLog> _customerLogOperation;
        /// <summary>
        /// The customer restaurant order operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder> _customerRestaurantOrderOperations;
        /// <summary>
        /// The customer preferred food profile operations
        /// </summary>
        CRUDOperation<IRepository<CustomerPreferredFoodProfile>, CustomerPreferredFoodProfile> _customerPreferredFoodProfileOperations;
        /// <summary>
        /// The customer all orders operations
        /// </summary>
        CRUDOperation<IRepository<VW_YourPlansDashboard>, VW_YourPlansDashboard> _customerAllOrdersOperations;
        /// <summary>
        /// The customer orders history operations
        /// </summary>
        CRUDOperation<IRepository<VW_CustomerOrdersHistory>, VW_CustomerOrdersHistory> _customerOrdersHistoryOperations;
        /// <summary>
        /// The wake up operation
        /// </summary>
        CRUDOperation<IRepository<WakeUp>, WakeUp> _wakeUpOperation;
        /// <summary>
        /// The hotel rooms operation
        /// </summary>
        CRUDOperation<IRepository<Room>, Room> _hotelRoomsOperation;
        /// <summary>
        /// The customer favorite excursion operations
        /// </summary>
        CRUDOperation<IRepository<CustomerFavoriteExcursion>, CustomerFavoriteExcursion> _customerFavoriteExcursionOperations;
        /// <summary>
        /// The customer excursion preferences operations
        /// </summary>
        CRUDOperation<IRepository<CustomerExcursionPreferences>, CustomerExcursionPreferences> _customerExcursionPreferencesOperations;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        /// <param name="customerDetailsRepository">The customer details repository.</param>
        /// <param name="customerHotelBookings">The customer hotel bookings.</param>
        /// <param name="customerRestaurantPreferences">The customer restaurant preferences.</param>
        /// <param name="customerHotelPreferences">The customer hotel preferences.</param>
        /// <param name="customerRestaurantOrders">The customer restaurant orders.</param>
        /// <param name="customerRestaurantOrderItems">The customer restaurant order items.</param>
        /// <param name="customerRestaurantItemCustomisation">The customer restaurant item customisation.</param>
        /// <param name="customerRoleRepository">The customer role repository.</param>
        /// <param name="customerCreditCardsRepository">The customer credit cards repository.</param>
        /// <param name="customerIngredientPreferencesRepository">The customer ingredient preferences repository.</param>
        /// <param name="customerAllergenPreferenceRepository">The customer allergen preference repository.</param>
        /// <param name="paypalDetailsRepository">The paypal details repository.</param>
        /// <param name="customerFavoriteRestaurantRepository">The customer favorite restaurant repository.</param>
        /// <param name="customerFavoriteHotelRepository">The customer favorite hotel repository.</param>
        /// <param name="currencyDetailsRepository">The currency details repository.</param>
        /// <param name="customerFavoriteMenuItemRepository">The customer favorite menu item repository.</param>
        /// <param name="countryRepository">The country repository.</param>
        /// <param name="customerMenuItemsRepository">The customer menu items repository.</param>
        /// <param name="customerSpaServicePreferenceRepository">The customer spa service preference repository.</param>
        /// <param name="spaServiceRepository">The spa service repository.</param>
        /// <param name="customerFavoriteSpaServiceRepository">The customer favorite spa service repository.</param>
        /// <param name="languageRepository">The language repository.</param>
        /// <param name="customerLogRepository">The customer log repository.</param>
        /// <param name="customerPreferredFoodProfileRepository">The customer preferred food profile repository.</param>
        /// <param name="customerAllOrdersRepository">The customer all orders repository.</param>
        /// <param name="customerOrdersHistoryRepository">The customer orders history repository.</param>
        /// <param name="customerSpaOrderRepository">The customer spa order repository.</param>
        /// <param name="customerSpaOrderItemRepository">The customer spa order item repository.</param>
        /// <param name="customerLaundryOrderItemsRepository">The customer laundry order items repository.</param>
        /// <param name="customerHousekeepingOrderItemRepository">The customer housekeeping order item repository.</param>
        /// <param name="wakeUpRepository">The wake up repository.</param>
        /// <param name="hotelRoomsRepository">The hotel rooms repository.</param>
        /// <param name="customerFavoriteExcursionRepository">The customer favorite excursion repository.</param>
        /// <param name="customerExcursionPreferenceRepository">The customer excursion preference repository.</param>
        /// <param name="customerExcursionOrderRepository">The customer excursion order repository.</param>
        /// <param name="customerExcursionOrderItemsRepository">The customer excursion order items repository.</param>
        public CustomerService(
            IRepository<Customer> customerDetailsRepository,
            IRepository<Customer_Hotel_Bookings> customerHotelBookings,
            IRepository<Customer_Restaurant_Preference> customerRestaurantPreferences,
            IRepository<Customer_Hotel_Preferences> customerHotelPreferences,
            IRepository<RestaurantOrder> customerRestaurantOrders,
            IRepository<Customer_RestaurantOrder_Items> customerRestaurantOrderItems,
            IRepository<Customer_RestaurantOrder_Item_customisation> customerRestaurantItemCustomisation,
            IRepository<CustomerRole> customerRoleRepository,
            IRepository<CustomerCreditCard> customerCreditCardsRepository,
            IRepository<CustomerIngredientPreference> customerIngredientPreferencesRepository,
            IRepository<CustomerAllergenPreference> customerAllergenPreferenceRepository,
            IRepository<PaypalDetail> paypalDetailsRepository,
            IRepository<CustomerFavoriteRestaurant> customerFavoriteRestaurantRepository,
            IRepository<CustomerFavoriteHotel> customerFavoriteHotelRepository,
            IRepository<Currency> currencyDetailsRepository,
            IRepository<CustomerFavoriteMenuItem> customerFavoriteMenuItemRepository,
            IRepository<Country> countryRepository,
            IRepository<RestaurantMenuItem> customerMenuItemsRepository,
            IRepository<CustomerSpaServicePreference> customerSpaServicePreferenceRepository,
            IRepository<SpaService> spaServiceRepository,
            IRepository<CustomerFavoriteSpaService> customerFavoriteSpaServiceRepository,
            IRepository<Language> languageRepository,
            IRepository<CustomerLog> customerLogRepository,
            IRepository<CustomerPreferredFoodProfile> customerPreferredFoodProfileRepository,
            IRepository<VW_YourPlansDashboard> customerAllOrdersRepository,
            IRepository<VW_CustomerOrdersHistory> customerOrdersHistoryRepository,
            IRepository<SpaOrder> customerSpaOrderRepository,
            IRepository<SpaOrderItem> customerSpaOrderItemRepository,
            IRepository<LaundryOrderItems> customerLaundryOrderItemsRepository,
            IRepository<HousekeepingOrderItem> customerHousekeepingOrderItemRepository,
            IRepository<WakeUp> wakeUpRepository,
            IRepository<Room> hotelRoomsRepository,
            IRepository<CustomerFavoriteExcursion> customerFavoriteExcursionRepository,
            IRepository<CustomerExcursionPreferences> customerExcursionPreferenceRepository,
            IRepository<ExcursionOrder> customerExcursionOrderRepository,
            IRepository<ExcursionOrderItems> customerExcursionOrderItemsRepository
            )
        {
            _customerDetailsRepository = customerDetailsRepository;
            _customerHotelBookings = customerHotelBookings;
            _customerRestaurantPreferences = customerRestaurantPreferences;
            _customerHotelPreferences = customerHotelPreferences;
            _customerRestaurantOrders = customerRestaurantOrders;
            _customerRestaurantOrderItems = customerRestaurantOrderItems;
            _customerRestaurantOrderItemCustomisation = customerRestaurantItemCustomisation;
            _customerRoleRepository = customerRoleRepository;
            _customerCreditCardsRepository = customerCreditCardsRepository;
            _paypalDetailsRepository = paypalDetailsRepository;
            _customerFavoriteRestaurantRepository = customerFavoriteRestaurantRepository;
            _customerFavoriteHotelRepository = customerFavoriteHotelRepository;
            _customerIngredientPreferencesRepository = customerIngredientPreferencesRepository;
            _customerAllergenPreferenceRepository = customerAllergenPreferenceRepository;
            _currencyDetailsRepository = currencyDetailsRepository;
            _customerFavoriteMenuItemRepository = customerFavoriteMenuItemRepository;
            _countryRepository = countryRepository;
            _customerMenuItemsRepository = customerMenuItemsRepository;
            _customerSpaServicePreferenceRepository = customerSpaServicePreferenceRepository;
            _spaServiceRepository = spaServiceRepository;
            _customerFavoriteSpaServiceRepository = customerFavoriteSpaServiceRepository;
            _languageRepository = languageRepository;
            _customerLogRepository = customerLogRepository;
            _customerPreferredFoodProfileRepository = customerPreferredFoodProfileRepository;
            _customerAllOrdersRepository = customerAllOrdersRepository;
            _customerOrdersHistoryRepository = customerOrdersHistoryRepository;
            _customerSpaOrderRepository = customerSpaOrderRepository;
            _customerSpaOrderItemRepository = customerSpaOrderItemRepository;
            _customerLaundryOrderItemsRepository = customerLaundryOrderItemsRepository;
            _customerHousekeepingOrderItemRepository = customerHousekeepingOrderItemRepository;
            _wakeUpRepository = wakeUpRepository;
            _hotelRoomsRepository = hotelRoomsRepository;
            _customerFavoriteExcursionRepository = customerFavoriteExcursionRepository;
            _customerExcursionPreferenceRepository = customerExcursionPreferenceRepository;
            _customerExcursionOrderRepository = customerExcursionOrderRepository;
            _customerExcursionOrderItemsRepository = customerExcursionOrderItemsRepository;

            _customerOperations = new CRUDOperation<IRepository<Customer>, Customer>(_customerDetailsRepository);
            _customerRoleOperations = new CRUDOperation<IRepository<CustomerRole>, CustomerRole>(_customerRoleRepository);
            _customerCreditCardsOperation = new CRUDOperation<IRepository<CustomerCreditCard>, CustomerCreditCard>(_customerCreditCardsRepository);
            _customerIngredientPreferencesOperation = new CRUDOperation<IRepository<CustomerIngredientPreference>, CustomerIngredientPreference>(_customerIngredientPreferencesRepository);
            _customerAllergenPreferenceOperation = new CRUDOperation<IRepository<CustomerAllergenPreference>, CustomerAllergenPreference>(_customerAllergenPreferenceRepository);
            _customerFavouriteRestaurantOperation = new CRUDOperation<IRepository<CustomerFavoriteRestaurant>, CustomerFavoriteRestaurant>(_customerFavoriteRestaurantRepository);
            _customerFavouriteHotelOperation = new CRUDOperation<IRepository<CustomerFavoriteHotel>, CustomerFavoriteHotel>(_customerFavoriteHotelRepository);
            _currencyOperation = new CRUDOperation<IRepository<Currency>, Currency>(_currencyDetailsRepository);
            _customerFavoriteMenuItemOperation = new CRUDOperation<IRepository<CustomerFavoriteMenuItem>, CustomerFavoriteMenuItem>(_customerFavoriteMenuItemRepository);
            _customerMenuItemsOperations = new CRUDOperation<IRepository<RestaurantMenuItem>, RestaurantMenuItem>(_customerMenuItemsRepository);
            _customerSpaServicePreferenceOperation = new CRUDOperation<IRepository<CustomerSpaServicePreference>, CustomerSpaServicePreference>(_customerSpaServicePreferenceRepository);
            _spaServiceOperations = new CRUDOperation<IRepository<SpaService>, SpaService>(_spaServiceRepository);
            _customerFavoriteSpaServiceOperations = new CRUDOperation<IRepository<CustomerFavoriteSpaService>, CustomerFavoriteSpaService>(_customerFavoriteSpaServiceRepository);
            _languageOperation = new CRUDOperation<IRepository<Language>, Language>(_languageRepository);
            _customerLogOperation = new CRUDOperation<IRepository<CustomerLog>, CustomerLog>(_customerLogRepository);
            _customerRestaurantOrderOperations = new CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder>(_customerRestaurantOrders);
            _customerPreferredFoodProfileOperations = new CRUDOperation<IRepository<CustomerPreferredFoodProfile>, CustomerPreferredFoodProfile>(_customerPreferredFoodProfileRepository);
            _customerAllOrdersOperations = new CRUDOperation<IRepository<VW_YourPlansDashboard>, VW_YourPlansDashboard>(_customerAllOrdersRepository);
            _customerOrdersHistoryOperations = new CRUDOperation<IRepository<VW_CustomerOrdersHistory>, VW_CustomerOrdersHistory>(_customerOrdersHistoryRepository);
            _wakeUpOperation = new CRUDOperation<IRepository<WakeUp>, WakeUp>(_wakeUpRepository);
            _hotelRoomsOperation = new CRUDOperation<IRepository<Room>, Room>(_hotelRoomsRepository);
            _customerFavoriteExcursionOperations = new CRUDOperation<IRepository<CustomerFavoriteExcursion>, CustomerFavoriteExcursion>(_customerFavoriteExcursionRepository);
            _customerExcursionPreferencesOperations = new CRUDOperation<IRepository<CustomerExcursionPreferences>, CustomerExcursionPreferences>(_customerExcursionPreferenceRepository);
        }

        #endregion initialization

        /// <summary>
        /// Validates the credentials.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ValidateCredentials(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email)
                || string.IsNullOrWhiteSpace(password))
                return false;

            var userAccount = _customerDetailsRepository.Table.FirstOrDefault(c => c.Email == email && c.ActiveStatus == true && c.CustomerRoles.Count() == 0);
            if (userAccount != null)
                if (Hashing.ValidatePassword(password, userAccount.Password))
                    return true;
            return false;
        }

        /// <summary>
        /// Validates the backend credentials.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ValidateBackendCredentials(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email)
                || string.IsNullOrWhiteSpace(password))
                return false;

            var userAccount = _customerDetailsRepository.Table.FirstOrDefault(c => c.Email == email && c.ActiveStatus == true);
            if (userAccount != null)
                if (Hashing.ValidatePassword(password, userAccount.Password))
                    return true;
            return false;
        }

        /// <summary>
        /// Validates the external login.
        /// </summary>
        /// <param name="providerKey">The provider key.</param>
        /// <param name="provider">The provider.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ValidateExternalLogin(string providerKey, string provider)
        {
            if (string.IsNullOrWhiteSpace(providerKey)
                || string.IsNullOrWhiteSpace(provider))
                return false;


            return _customerDetailsRepository.Table.FirstOrDefault(c => c.ProviderKey == providerKey && c.ProviderName == provider) != null;
        }

        #region customer

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetAllCustomers()
        {
            return _customerOperations.GetAll();
        }

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="Email">The email.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetAllCustomers(string name, string Email, int skip, int pageSize, out int total)
        {
            var query = _customerDetailsRepository.Table;
            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(c => (c.FirstName + " " + c.LastName).Contains(name));
            }
            if (!string.IsNullOrWhiteSpace(Email))
            {
                query = query.Where(c => c.Email.Contains(Email));
            }
            var pages = query.OrderByDescending(o => o.CreationDate)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Customer>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetAllCustomers(Func<Customer, bool> predicate)
        {
            return _customerOperations.GetAll(predicate);
        }


        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Customer.</returns>
        public Customer GetCustomerById(int id)
        {
            return _customerOperations.GetById(id);
        }
        /// <summary>
        /// Gets the customer by telephone.
        /// </summary>
        /// <param name="telephone">The telephone.</param>
        /// <returns>Customer.</returns>
        public Customer GetCustomerByTelephone(string telephone)
        {
            return _customerOperations.GetById(c => c.Telephone == telephone && c.ActiveStatus != false);
        }

        /// <summary>
        /// Gets the customer by email identifier.
        /// </summary>
        /// <param name="emailId">The email identifier.</param>
        /// <returns>Customer.</returns>
        public Customer GetCustomerByEmailId(string emailId)
        {
            return _customerOperations.GetById(c => c.Email == emailId && c.ActiveStatus != false);
        }
        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public Customer CreateCustomer(Customer details)
        {
            var count = _customerOperations.GetAll(c => c.ActiveStatus != false && (c.Email == details.Email || c.Telephone == details.Telephone)).Count();

            if (count > 0)
                throw new InvalidOperationException(Resources.Exst_Email);
            else
                return _customerOperations.AddNew(details);
        }

        /// <summary>
        /// Creates the external customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public Customer CreateExternalCustomer(Customer details)
        {
            var count = _customerOperations.GetAll(c => c.ActiveStatus != false && c.Email == details.Email).Count();

            if (count > 0)
                throw new InvalidOperationException(Resources.Exst_Email);
            else
                return _customerOperations.AddNew(details);
        }

        /// <summary>
        /// Creates the customer log.
        /// </summary>
        /// <param name="log">The log.</param>
        public void CreateCustomerLog(CustomerLog log)
        {
            _customerLogOperation.AddNew(log);
        }

        /// <summary>
        /// Modifies the customer.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        /// <exception cref="EmailInUseException"></exception>
        public Customer ModifyCustomer(Customer details)
        {
            var otherUser = _customerOperations.GetAll(c => c.Id != details.Id && c.ActiveStatus == true && (c.Email == details.Email || c.Telephone == details.Telephone)).FirstOrDefault();

            if (otherUser != null)
            {
                throw new EmailInUseException(Resources.Exst_Email);
            }

            _customerOperations.Update(details.Id, details);
            return _customerOperations.GetById(details.Id);
        }

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        /// <exception cref="EmailInUseException">EMAIL_USED_BY_ANOTHER_USER</exception>
        public Customer UpdatePassword(Customer details)
        {
            var otherUser = _customerOperations.GetAll(c => c.Id != details.Id && c.Email == details.Email && c.ActiveStatus == true).FirstOrDefault();

            if (otherUser != null)
            {
                throw new EmailInUseException("EMAIL_USED_BY_ANOTHER_USER");
            }

            _customerOperations.Update(details.Id, details);
            return details;
        }
        /// <summary>
        /// Deletes the account.
        /// </summary>
        /// <param name="details">The details.</param>
        public void DeleteAccount(Customer details)
        {
            _customerOperations.Update(details.Id, details);
        }

        /// <summary>
        /// Updates the email.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Customer.</returns>
        /// <exception cref="EmailInUseException">EMAIL_USED_BY_ANOTHER_USER</exception>
        public Customer UpdateEmail(Customer details)
        {
            var otherUser = _customerOperations.GetAll(c => c.Id != details.Id && c.Email == details.Email && c.ActiveStatus == true).FirstOrDefault();

            if (otherUser != null)
            {
                throw new EmailInUseException("EMAIL_USED_BY_ANOTHER_USER");
            }
            _customerOperations.Update(details.Id, details);
            return details;
        }



        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCustomer(int id)
        {
            _customerOperations.Delete(id);
        }

        //public CustomerPreferedCurrency GetCustomerPreferedCurrency(int id)
        //{
        //    return _customerPreferedCurrencyOperations.GetById(c => c.CustomerId == id);
        //}
        //public CustomerPreferedCurrency AddCustomerPreferedCurrency(CustomerPreferedCurrency custPrefCurrency)
        //{
        //    return _customerPreferedCurrencyOperations.AddNew(custPrefCurrency);

        //}
        //public CustomerPreferedCurrency ModifyCustomerPreferedCurrency(CustomerPreferedCurrency custPrefCurrency)
        //{
        //    _customerPreferedCurrencyOperations.Update(custPrefCurrency.Id, custPrefCurrency);
        //    return custPrefCurrency;
        //}

        /// <summary>
        /// Gets all deleted customers.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetAllDeletedCustomers(int skip, int pageSize, out int total)
        {
            return _customerOperations.GetAllWithServerSidePagging(r => r.ActiveStatus == false, s => s.Id, skip, pageSize, out total);
        }

        #endregion

        #region Customer Preferred Food Profile
        /// <summary>
        /// Gets the customer preferred food profile.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        public CustomerPreferredFoodProfile GetCustomerPreferredFoodProfile(int customerId)
        {
            return _customerPreferredFoodProfileOperations.GetById(f => f.CustomerId == customerId);

        }
        /// <summary>
        /// Creates the customer food profile.
        /// </summary>
        /// <param name="prefFoodProfile">The preference food profile.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        public CustomerPreferredFoodProfile CreateCustomerFoodProfile(CustomerPreferredFoodProfile prefFoodProfile)
        {
            return _customerPreferredFoodProfileOperations.AddNew(prefFoodProfile);
        }
        /// <summary>
        /// Updates the customer preferred food profile.
        /// </summary>
        /// <param name="prefFoodProfile">The preference food profile.</param>
        /// <returns>CustomerPreferredFoodProfile.</returns>
        public CustomerPreferredFoodProfile UpdateCustPreferredFoodProfile(CustomerPreferredFoodProfile prefFoodProfile)
        {
            _customerPreferredFoodProfileOperations.Update(prefFoodProfile.Id, prefFoodProfile);
            return prefFoodProfile;
        }
        #endregion Customer Preffered Food Profile
        /// <summary>
        /// Customers the authorized restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> CustomerAuthorizedRestaurants(int id)
        {
            return _customerRoleOperations.GetAll(r => r.CustomerId == id && r.RestaurantId.HasValue).Select(r => r.Restaurant);

        }

        /// <summary>
        /// Customers the authorized hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> CustomerAuthorizedHotels(int id)
        {
            return _customerRoleOperations.GetAll(r => r.CustomerId == id && r.HotelId.HasValue).Select(r => r.Hotel);
        }


        #region bookings

        /// <summary>
        /// Gets the bookings.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Hotel_Bookings.</returns>
        public IEnumerable<Customer_Hotel_Bookings> GetBookings(int Id)
        {
            //return _customerHotelBookings.Table.Where(c => c.Customer_Hotel_Booking_ID == Id);
            return null;
        }


        /// <summary>
        /// Gets the booking by identifier.
        /// </summary>
        /// <param name="bookingId">The booking identifier.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        public Customer_Hotel_Bookings GetBookingById(int bookingId)
        {
            return _customerHotelBookings.GetById(bookingId);
        }

        /// <summary>
        /// Creates the booking.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        public Customer_Hotel_Bookings CreateBooking(Customer_Hotel_Bookings hotelBooking)
        {
            return _customerHotelBookings.Insert(hotelBooking);
        }

        /// <summary>
        /// Modifies the booking.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>Customer_Hotel_Bookings.</returns>
        public Customer_Hotel_Bookings ModifyBooking(Customer_Hotel_Bookings hotelBooking)
        {
            //_customerHotelBookings.Update(hotelBooking.Customer_Hotel_Booking_ID, hotelBooking); //to populate navigational properites

            //return _customerHotelBookings.GetById(hotelBooking.Customer_Hotel_Booking_ID);
            return null;
        }

        /// <summary>
        /// Deletes the booking.
        /// </summary>
        /// <param name="customer_hotel_booking_id">The customer hotel booking identifier.</param>
        public void DeleteBooking(int customer_hotel_booking_id)
        {
            _customerHotelBookings.Delete(customer_hotel_booking_id);
            return;
        }

        #endregion

        #region restaurant preferences

        /// <summary>
        /// Modifies the ingredient preference.
        /// </summary>
        /// <param name="ingredientPreference">The ingredient preference.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">INVALID_CUSTOMER_ID</exception>
        public string ModifyIngredientPreference(CustomerIngredientPreference ingredientPreference)
        {
            if (ingredientPreference.CustomerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");
            var oldIngPref = _customerIngredientPreferencesOperation.GetById(x => x.IngredientId == ingredientPreference.IngredientId && x.CustomerId == ingredientPreference.CustomerId);
            if (oldIngPref == null)
                _customerIngredientPreferencesRepository.Insert(ingredientPreference);
            else
            {
                oldIngPref.Include = ingredientPreference.Include;
                _customerIngredientPreferencesRepository.Update(oldIngPref);
            }
            return "Success";

        }

        /// <summary>
        /// Modifies the allergen preference.
        /// </summary>
        /// <param name="allergenPreference">The allergen preference.</param>
        /// <param name="include">if set to <c>true</c> [include].</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">INVALID_CUSTOMER_ID</exception>
        public string ModifyAllergenPreference(CustomerAllergenPreference allergenPreference, bool include)
        {
            if (allergenPreference.CustomerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            var oldPref = _customerAllergenPreferenceOperation.GetById(a => a.AllergenId == allergenPreference.AllergenId && a.CustomerId == allergenPreference.CustomerId);
            if (oldPref == null && include == true)
                _customerAllergenPreferenceRepository.Insert(allergenPreference);
            if (include == false && oldPref != null)
                _customerAllergenPreferenceRepository.Delete(oldPref.Id);
            return "Success";
        }

        /// <summary>
        /// Gets the customer allergen prefernecs.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllergenPreference.</returns>
        public IEnumerable<CustomerAllergenPreference> GetCustomerAllergenPrefernecs(int customerId)
        {
            return _customerAllergenPreferenceOperation.GetAll(a => a.CustomerId == customerId);
        }

        /// <summary>
        /// Gets the customer allergen by preference ingredient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable CustomerAllergenPreference.</returns>
        public IEnumerable<CustomerAllergenPreference> GetCustAllergenByPrefIngredient(int customerId, int ingredientId)
        {
            return _customerAllergenPreferenceOperation.GetAll(a => a.CustomerId == customerId && a.Allergen.AllergenIngredients.Any(ai => ai.IngredientID == ingredientId));
        }
        /// <summary>
        /// Gets the type of the customer ingredient preference by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="type">if set to <c>true</c> [type].</param>
        /// <returns>IEnumerable CustomerIngredientPreference.</returns>
        public IEnumerable<CustomerIngredientPreference> GetCustomerIngredientPreferenceByType(int customerId, bool type)
        {
            return _customerIngredientPreferencesOperation.GetAll(a => a.CustomerId == customerId && a.Ingredient.IngredientCategory.Type == type);
        }

        /// <summary>
        /// Gets the customer ingred preference by allergic ingredient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="allergicIngredients">The allergic ingredients.</param>
        /// <returns>IEnumerable CustomerIngredientPreference.</returns>
        public IEnumerable<CustomerIngredientPreference> GetCustIngredPrefByAllergicIngredient(int customerId, List<int> allergicIngredients)
        {
            return _customerIngredientPreferencesOperation.GetAll(a => a.CustomerId == customerId && allergicIngredients.Contains(a.IngredientId) && (a.Include == true || a.Include == null));
        }
        /// <summary>
        /// Gets the customer restaurant preference.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        public Customer_Restaurant_Preference GetCustomerRestaurantPreference(int Id)
        {
            return GetCustomerRestaurantPreference(Id, false);
        }

        /// <summary>
        /// Gets the customer restaurant preference.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="completeList">if set to <c>true</c> [complete list].</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        public Customer_Restaurant_Preference GetCustomerRestaurantPreference(int Id, bool completeList)
        {
            var ingredientPreferences = _context.uspCustomerIngredientPreference(Id).Select(p => new Customer_IngredientPreference
            {
                Ingredient_Id = p.Id.Value,
                Parent_Id = p.ParentId,
                Ingredient_Name = p.Name,
                Include = p.Include,
                Ingredient_Desc = p.Description,
                Ingredient_Image = p.IngredientImage,
                IsGroup = p.Level == 1
            });

            Customer_Restaurant_Preference result = new Customer_Restaurant_Preference();
            result.Ingredient_Preference = ingredientPreferences;

            var allergenPreferences = _context.uspCustomerAllergenPreference(Id).Select(p => new Customer_AllergenPreference
            {
                Allergen_ID = p.Id,
                Allergen_Name = p.Name,
                Allergen_Desc = p.Description,
                Allergen_Image = p.AllergenImage,
                Id = p.CustomerId

            });
            result.Allergen_Preference = allergenPreferences;
            return result;
        }


        /// <summary>
        /// Gets the restaurant preferences.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Restaurant_Preference.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<Customer_Restaurant_Preference> GetRestaurantPreferences(int Id)
        {
            throw new NotImplementedException();

        }


        /// <summary>
        /// Gets the restaurant preferences by identifier.
        /// </summary>
        /// <param name="preferenceId">The preference identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        public Customer_Restaurant_Preference GetRestaurantPreferencesById(int preferenceId)
        {
            return _customerRestaurantPreferences.GetById(preferenceId);
        }

        /// <summary>
        /// Creates the restaurant preference.
        /// </summary>
        /// <param name="restaurantPreference">The restaurant preference.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        public Customer_Restaurant_Preference CreateRestaurantPreference(Customer_Restaurant_Preference restaurantPreference)
        {
            return _customerRestaurantPreferences.Insert(restaurantPreference);
        }

        /// <summary>
        /// Modifies the restaurant preference.
        /// </summary>
        /// <param name="customerPreference">The customer preference.</param>
        /// <param name="Id">The identifier.</param>
        /// <returns>Customer_Restaurant_Preference.</returns>
        /// <exception cref="System.ArgumentException">
        /// INVALID_CUSTOMER_ID
        /// or
        /// </exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        public Customer_Restaurant_Preference ModifyRestaurantPreference(CustomerPreferences customerPreference, int Id)
        {
            if (Id <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            foreach (var preference in customerPreference.CustomerPreference)
            {
                if (string.IsNullOrWhiteSpace(preference.Id))
                    throw new ArgumentNullException(Resources.Id_Reqd);

                var idArr = preference.Id.Split(',');
                if (idArr.Length < 2)
                    throw new ArgumentException(Resources.Id_Reqd);

                var actualId = 0;
                int.TryParse(idArr[1], out actualId);

                switch (idArr[0])
                {
                    case "A":
                        var oldPref = _customerAllergenPreferenceOperation.GetById(a => a.AllergenId == actualId && a.CustomerId == Id);
                        if (oldPref == null && preference.ItemValue == true)
                            _customerAllergenPreferenceRepository.Insert(new CustomerAllergenPreference { AllergenId = actualId, CustomerId = Id });
                        if (preference.ItemValue.HasValue && preference.ItemValue == false && oldPref != null)
                            _customerAllergenPreferenceRepository.Delete(oldPref.Id);

                        break;

                    case "ING":
                    default:
                        var oldIngPref = _customerIngredientPreferencesOperation.GetById(x => x.IngredientId == actualId && x.CustomerId == Id);
                        if (oldIngPref == null)
                            _customerIngredientPreferencesRepository.Insert(new CustomerIngredientPreference { CustomerId = Id, Include = preference.ItemValue, IngredientId = actualId });
                        else
                        {
                            oldIngPref.Include = preference.ItemValue;
                            _customerIngredientPreferencesRepository.Update(oldIngPref);
                        }
                        break;
                }
            }
            return GetCustomerRestaurantPreference(Id);
        }

        /// <summary>
        /// Deletes the restaurant preference.
        /// </summary>
        /// <param name="customer_restaurant_preference_id">The customer restaurant preference identifier.</param>
        public void DeleteRestaurantPreference(int customer_restaurant_preference_id)
        {
            _customerRestaurantPreferences.Delete(customer_restaurant_preference_id);
            return;
        }

        /// <summary>
        /// Gets the customer restaurant preference allergen.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_AllergenPreference.</returns>
        public IEnumerable<Customer_AllergenPreference> GetCustomerRestaurantPreferenceAllergen(int Id)
        {
            var allergenPreferences = _context.uspCustomerAllergenPreference(Id).Select(p => new Customer_AllergenPreference
            {
                Allergen_ID = p.Id,
                Allergen_Name = p.Name,
                Allergen_Desc = p.Description,
                Id = p.CustomerId
            });
            return allergenPreferences;
        }

        /// <summary>
        /// Gets the allergen preference by identifier.
        /// </summary>
        /// <param name="allergenId">The allergen identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerAllergenPreference.</returns>
        public CustomerAllergenPreference GetAllergenPreferenceById(int allergenId, int customerId)
        {
            var oldPref = _customerAllergenPreferenceOperation.GetById(a => a.AllergenId == allergenId && a.CustomerId == customerId);
            if (oldPref == null)
                _customerAllergenPreferenceRepository.Insert(new CustomerAllergenPreference { AllergenId = allergenId, CustomerId = customerId });
            else
                _customerAllergenPreferenceRepository.Delete(oldPref.Id);
            return null;
        }

        /// <summary>
        /// Gets the customer preferred menus.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        public IEnumerable<RestaurantMenu> GetCustomerPreferredMenus(int customerId)
        {
            var preferredMenus = _context.uspGetCustomerPreferredMenus(customerId).Select(p => new RestaurantMenu
                {
                    Id = p.Id,
                    RestaurantId = p.RestaurantId,
                    MenuName = p.MenuName,
                    MenuDescription = p.MenuDescription,
                    IsActive = p.IsActive,
                    Image = p.Image,
                    LanguageId = p.LanguageId

                });
            return preferredMenus.ToList();
        }

        /// <summary>
        /// Gets the customer preferred menu items.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetCustomerPreferredMenuItems(int customerId)
        {
            var preferredMenuItems = _context.uspGetCustomerPreferredMenuItems(customerId).Select(i => new RestaurantMenuItem
                {
                    Id = i.Id,
                    RestaurantMenuGroupId = i.RestaurantMenuGroupId,
                    ItemName = i.ItemName,
                    ItemDescription = i.ItemDescription,
                    Price = i.Price,
                    TaxId = i.TaxId,
                    IsActive = i.IsActive,
                    LanguageId = i.LanguageId
                });
            return preferredMenuItems.ToList();
        }

        #endregion

        #region favorite restaurants

        /// <summary>
        /// Customers the favorite restaurant toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        public void CustomerFavoriteRestaurantToggle(int Id, int RestaurantId)
        {
            var exists = _customerFavoriteRestaurantRepository.Table.Where(f => f.CustomerId == Id && f.RestaurantId == RestaurantId).FirstOrDefault();

            if (exists != null)
            {
                _customerFavoriteRestaurantRepository.Delete(exists.Id);
            }
            else
            {
                _customerFavoriteRestaurantRepository.Insert(new CustomerFavoriteRestaurant { CustomerId = Id, RestaurantId = RestaurantId });
            }
        }

        /// <summary>
        /// Gets the favorite restaurants.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetFavoriteRestaurants(int Id)
        {
            return _customerFavoriteRestaurantRepository.Table.Where(f => f.CustomerId == Id)
                .Select(f => f.Restaurant);
        }

        /// <summary>
        /// Gets the favorite restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetFavoriteRestaurants(int id, int pageNumber, int pageSize, out int total)
        {
            var custFavRestaurants = _customerFavouriteRestaurantOperation.GetAllByDesc(r => r.CustomerId == id, f => f.Id, pageNumber, pageSize, out total).Select(h => h.Restaurant);
            return custFavRestaurants;
        }


        /// <summary>
        /// Deletes the favorite restaurant.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        public void DeleteFavoriteRestaurant(int restaurantId, int customerId)
        {
            var favoriteRestaurant = _customerFavouriteRestaurantOperation.GetAll(r => r.RestaurantId == restaurantId && r.CustomerId == customerId).FirstOrDefault();
            _customerFavoriteRestaurantRepository.Delete(favoriteRestaurant.Id);
        }

        #endregion favorite restaurants

        #region Favorite Hotel
        /// <summary>
        /// Customers the favorite hotel toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="HotelId">The hotel identifier.</param>
        public void CustomerFavoriteHotelToggle(int Id, int HotelId)
        {
            var exists = _customerFavoriteHotelRepository.Table.Where(f => f.CustomerId == Id && f.HotelId == HotelId).FirstOrDefault();

            if (exists != null)
            {
                _customerFavoriteHotelRepository.Delete(exists.Id);
            }
            else
            {
                _customerFavoriteHotelRepository.Insert(new CustomerFavoriteHotel { CustomerId = Id, HotelId = HotelId });
            }
        }

        /// <summary>
        /// Gets the favorite hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel</returns>
        public IEnumerable<Hotel> GetFavoriteHotels(int id, int pageNumber, int pageSize, out int total)
        {
            var custFavHotels = _customerFavouriteHotelOperation.GetAllByDesc(r => r.CustomerId == id, f => f.Id, pageNumber, pageSize, out total).Select(h => h.Hotel);
            return custFavHotels;
        }

        /// <summary>
        /// Gets the favorite hotels.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetFavoriteHotels(int Id)
        {
            return _customerFavoriteHotelRepository.Table.Where(f => f.CustomerId == Id)
                .Select(f => f.Hotel);
        }

        /// <summary>
        /// Deletes the favorite hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        public void DeleteFavoriteHotel(int hotelId, int customerId)
        {
            var favoriteHotel = _customerFavouriteHotelOperation.GetById(r => r.HotelId == hotelId && r.CustomerId == customerId);
            _customerFavouriteHotelOperation.Delete(favoriteHotel.Id);
        }
        #endregion Favorite Hotel

        #region hotel preferences

        /// <summary>
        /// Gets the hotel preferences.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable Customer_Hotel_Preferences.</returns>
        public IEnumerable<Customer_Hotel_Preferences> GetHotelPreferences(int Id)
        {
            // return _customerHotelPreferences.Table.Where(c => c.Customer_Hotel_Preference_ID == Id);
            return null;
        }

        /// <summary>
        /// Gets the hotel preferences by identifier.
        /// </summary>
        /// <param name="preferenceId">The preference identifier.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        public Customer_Hotel_Preferences GetHotelPreferencesById(int preferenceId)
        {
            return _customerHotelPreferences.GetById(preferenceId);
        }

        /// <summary>
        /// Creates the hotel preference.
        /// </summary>
        /// <param name="hotelPreference">The hotel preference.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        public Customer_Hotel_Preferences CreateHotelPreference(Customer_Hotel_Preferences hotelPreference)
        {
            return _customerHotelPreferences.Insert(hotelPreference);

        }

        /// <summary>
        /// Modifies the hotel preference.
        /// </summary>
        /// <param name="hotelPreference">The hotel preference.</param>
        /// <returns>Customer_Hotel_Preferences.</returns>
        public Customer_Hotel_Preferences ModifyHotelPreference(Customer_Hotel_Preferences hotelPreference)
        {
            //_customerHotelPreferences.Update(hotelPreference.Customer_Hotel_Preference_ID, hotelPreference);
            // return _customerHotelPreferences.GetById(hotelPreference.Customer_Hotel_Preference_ID);
            return null;
        }

        /// <summary>
        /// Deletes the hotel preference.
        /// </summary>
        /// <param name="customer_hotel_preference_id">The customer hotel preference identifier.</param>
        public void DeleteHotelPreference(int customer_hotel_preference_id)
        {
            _customerHotelPreferences.Delete(customer_hotel_preference_id);
            return;
        }

        #endregion

        #region orders
        /// <summary>
        /// Gets the customer recent orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetCustomerRecentOrders(int customerId, int skip, int pageSize, out int total)
        {
            var toDate = DateTime.Now;
            var fromDate = toDate.AddDays(-30);
            return _customerRestaurantOrderOperations.GetAllWithServerSidePagging(o => o.CustomerId == customerId && o.OrderStatus == 4 &&
                                                                                        Convert.ToDateTime(o.ScheduledDate).Date >= fromDate.Date && Convert.ToDateTime(o.ScheduledDate).Date <= toDate.Date,
                                                                                        o => o.ScheduledDate, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the customer orders queue.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetCustomerOrdersQueue(int customerId, int skip, int pageSize, out int total)
        {

            return _customerRestaurantOrderOperations.GetAllWithServerSidePagging(o => o.CustomerId == customerId && o.OrderStatus <= 3, o => o.ScheduledDate, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the restaurant orders.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetRestaurantOrders(int Id)
        {
            return _customerRestaurantOrders.Table.Where(o => o.CustomerId == Id);
        }

        /// <summary>
        /// Gets the restaurant orders.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetRestaurantOrders(int Id, int RestaurantId)
        {
            return _customerRestaurantOrders.Table.Where(o => o.CustomerId == Id && o.RestaurantId == RestaurantId);
        }

        /// <summary>
        /// Gets the restaurant order by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder GetRestaurantOrderById(int id)
        {
            return _customerRestaurantOrders.GetById(id);
        }

        /// <summary>
        /// Creates the restaurant order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder CreateRestaurantOrder(RestaurantOrder order)
        {
            return _customerRestaurantOrders.Insert(order);

        }

        /// <summary>
        /// Modifies the restaurant order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder ModifyRestaurantOrder(RestaurantOrder order)
        {
            _customerRestaurantOrders.Update(order);
            return order;
        }

        /// <summary>
        /// Deletes the restaurant order.
        /// </summary>
        /// <param name="customer_Id">The customer identifier.</param>
        public void DeleteRestaurantOrder(int customer_Id)
        {
            _customerRestaurantOrders.Delete(customer_Id);
            return;
        }

        #endregion

        #region Customer Hotel/Restaurant Orders
        /// <summary>
        /// Gets all hotel and rest orders of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllHotelAndRestActiveOrders.</returns>
        public IEnumerable<CustomerAllHotelAndRestActiveOrders> GetAllHotelAndRestOrdersOfCustomer(int customerId)
        {
            System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            var custOrder = _customerAllOrdersRepository.ExecuteStoredProcedureList<CustomerAllHotelAndRestActiveOrders>("exec uspGetCustomerAllHotelAndRestActiveOrders", customerIdParameter);
            //var custOrder = _customerAllOrdersRepository.ExecuteStoredProcedureList<CustomerAllHotelAndRestActiveOrders>("exec GetCustomerAllPlans", customerIdParameter);
            return custOrder;
        }

        /// <summary>
        /// Gets the customer rest and hotels orders history.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerAllHotelAndRestOrderHistory.</returns>
        public IEnumerable<CustomerAllHotelAndRestOrderHistory> GetCustomerRestAndHotelsOrdersHistory(int customerId)
        {
            System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            var custOrderHistory = _customerAllOrdersRepository.ExecuteStoredProcedureList<CustomerAllHotelAndRestOrderHistory>("exec uspGetCustomerAllHotelAndRestOrderHistory", customerIdParameter);
            //var custOrderHistory = _customerAllOrdersRepository.ExecuteStoredProcedureList<VW_CustomerOrdersHistory>("exec GetCustomerOrdersHistory", customerIdParameter);
            return custOrderHistory;
        }

        /// <summary>
        /// Deletes the customer orders history and active orders.
        /// </summary>
        /// <param name="orderType">Type of the order.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <exception cref="NotFoundException">
        /// Restaurant order with this Id not found
        /// or
        /// Spa order with this Id not found
        /// or
        /// laundry order with this Id not found
        /// or
        /// housekeeping order with this Id not found
        /// or
        /// excursion order with this Id not found
        /// or
        /// order type is not valid
        /// </exception>
        public void DeleteCustOrdersHistoryAndActiveOrders(string orderType, int orderId)
        {
            switch (orderType)
            {
                case "Restaurant":
                    var order = _customerRestaurantOrders.GetById(orderId);
                    if (order == null)
                        throw new NotFoundException("Restaurant order with this Id not found");
                    order.IsActive = false;
                    _customerRestaurantOrders.Update(order.Id, order);
                    break;

                case "Spa":
                    var spaorder = _customerSpaOrderItemRepository.GetById(orderId);
                    if (spaorder == null)
                        throw new NotFoundException("Spa order with this Id not found");
                    spaorder.IsActive = false;
                    _customerSpaOrderItemRepository.Update(spaorder.Id, spaorder);
                    break;

                case "Laundry":
                    var laundryorder = _customerLaundryOrderItemsRepository.GetById(orderId);
                    if (laundryorder == null)
                        throw new NotFoundException("laundry order with this Id not found");
                    laundryorder.IsActive = false;
                    _customerLaundryOrderItemsRepository.Update(laundryorder.Id, laundryorder);
                    break;

                case "Housekeeping":
                    var hkeepingorder = _customerHousekeepingOrderItemRepository.GetById(orderId);
                    if (hkeepingorder == null)
                        throw new NotFoundException("housekeeping order with this Id not found");
                    hkeepingorder.IsActive = false;
                    _customerHousekeepingOrderItemRepository.Update(hkeepingorder.Id, hkeepingorder);
                    break;

                case "Excursion":
                    var excursionorder = _customerExcursionOrderItemsRepository.GetById(orderId);
                    if (excursionorder == null)
                        throw new NotFoundException("excursion order with this Id not found");
                    excursionorder.IsActive = false;
                    _customerExcursionOrderItemsRepository.Update(excursionorder.Id, excursionorder);
                    break;

                default:
                    throw new NotFoundException("order type is not valid");
            }
        }

        #endregion Customer Hotel/Restaurant Orders

        /// <summary>
        /// Adds the restaurant order item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>Customer_RestaurantOrder_Items.</returns>
        public Customer_RestaurantOrder_Items AddRestaurantOrderItem(Customer_RestaurantOrder_Items item)
        {
            return _customerRestaurantOrderItems.Insert(item);

        }

        /// <summary>
        /// Modifies the restaurant order item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>Customer_RestaurantOrder_Items.</returns>
        public Customer_RestaurantOrder_Items ModifyRestaurantOrderItem(Customer_RestaurantOrder_Items item)
        {
            _customerRestaurantOrderItems.Update(item);
            return item;
        }

        /// <summary>
        /// Removes the restaurant order item.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        public void RemoveRestaurantOrderItem(int itemId)
        {
            _customerRestaurantOrderItems.Delete(itemId);
            return;
        }


        /// <summary>
        /// Gets the restaurant item custamisations.
        /// </summary>
        /// <param name="customer_RestaurantOrder_item_id">The customer restaurant order item identifier.</param>
        /// <returns>IEnumerable Customer_RestaurantOrder_Item_customisation.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<Customer_RestaurantOrder_Item_customisation> GetRestaurantItemCustamisations(int customer_RestaurantOrder_item_id)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// Gets the restaurant item custamisations by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        public Customer_RestaurantOrder_Item_customisation GetRestaurantItemCustamisationsById(int id)
        {
            return _customerRestaurantOrderItemCustomisation.GetById(id);
        }

        /// <summary>
        /// Adds the restaurant item customisation.
        /// </summary>
        /// <param name="customisation">The customisation.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        public Customer_RestaurantOrder_Item_customisation AddRestaurantItemCustomisation(Customer_RestaurantOrder_Item_customisation customisation)
        {
            return _customerRestaurantOrderItemCustomisation.Insert(customisation);
        }

        /// <summary>
        /// Modifies the restaurant item customisation.
        /// </summary>
        /// <param name="customisation">The customisation.</param>
        /// <returns>Customer_RestaurantOrder_Item_customisation.</returns>
        public Customer_RestaurantOrder_Item_customisation ModifyRestaurantItemCustomisation(Customer_RestaurantOrder_Item_customisation customisation)
        {
            _customerRestaurantOrderItemCustomisation.Update(customisation);
            return customisation;
        }

        /// <summary>
        /// Removes the restaurant item customisation.
        /// </summary>
        /// <param name="customisationId">The customisation identifier.</param>
        public void RemoveRestaurantItemCustomisation(int customisationId)
        {
            _customerRestaurantOrderItemCustomisation.Delete(customisationId);
            return;
        }


        #region credit cards
        // Customer Credit card Info

        /// <summary>
        /// Gets the customer card details by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>CustomerCreditCard.</returns>
        public CustomerCreditCard GetCustomerCardDetailsById(int id)
        {
            var cardInfo = _customerCreditCardsOperation.GetById(c => c.Id == id && c.QuickPayCardId != null && c.IsActive);
            return cardInfo;
        }
        /// <summary>
        /// Gets the customer cards.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>IEnumerable CustomerPaymentCards.</returns>
        public IEnumerable<CustomerPaymentCards> GetCustomerCards(int Id, string quickPayToken, string quickPayUrl)
        {
            var custCards = _customerCreditCardsOperation.GetAll(c => c.CustomerId == Id && c.QuickPayCardId != null && c.IsActive);
            if (custCards.Any())
            {
                List<CustomerPaymentCards> custAllPaymentCards = new List<CustomerPaymentCards>();

                foreach (var cardDetails in custCards)
                {
                    using (WebClient client = new WebClient())
                    {
                        client.Headers.Add("Accept-Version", "v10");
                        client.Headers.Add("Authorization", quickPayToken);//Claus Account

                        client.BaseAddress = quickPayUrl;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                        var cardDetailsResponse = client.DownloadData(string.Format("cards/{0}", cardDetails.QuickPayCardId));

                        string cardDetailsResponseStr = System.Text.Encoding.UTF8.GetString(cardDetailsResponse);
                        dynamic cardDetailsResponseJSON = JsonConvert.DeserializeObject(cardDetailsResponseStr);
                        if (cardDetailsResponseJSON != null)
                        {
                            var eachPaymentCard = GenerateCardDetailsModel(cardDetailsResponseJSON, cardDetails);
                            custAllPaymentCards.Add(eachPaymentCard);
                        }
                    }
                }
                return custAllPaymentCards;
            }
            return new List<CustomerPaymentCards>();
        }


        /// <summary>
        /// Gets the card by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        /// <exception cref="System.Exception"></exception>
        public CustomerPaymentCards GetCardById(int id, string quickPayToken, string quickPayUrl)
        {
            var cardInfo = _customerCreditCardsOperation.GetById(c => c.Id == id && c.QuickPayCardId != null && c.IsActive);
            if (cardInfo == null)
                throw new Exception(Resources.Not_Found_Card);
            if (cardInfo.QuickPayCardId != null)
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Accept-Version", "v10");
                    client.Headers.Add("Authorization", quickPayToken);//Claus Account

                    client.BaseAddress = quickPayUrl;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                    var cardDetailsResponse = client.DownloadData(string.Format("cards/{0}", cardInfo.QuickPayCardId));

                    string cardDetailsResponseStr = System.Text.Encoding.UTF8.GetString(cardDetailsResponse);
                    dynamic cardDetailsResponseJSON = JsonConvert.DeserializeObject(cardDetailsResponseStr);
                    if (cardDetailsResponseJSON != null)
                    {
                        var cardDetails = GenerateCardDetailsModel(cardDetailsResponseJSON, cardInfo);
                        return cardDetails;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Creates the customer card.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="BadRequestException">
        /// </exception>
        /// <exception cref="ForbiddenException">
        /// </exception>
        public CustomerPaymentCards CreateCustomerCard(CustomerPaymentCards details, string quickPayToken, string quickPayUrl)
        {
            var cardExpiryResult = ValidCardExpiryDate(details.CardExpiryDate);
            if (cardExpiryResult.Status == false)
                throw new InvalidOperationException(cardExpiryResult.ResponseMessage);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept-Version", "v10");
                client.DefaultRequestHeaders.Add("Authorization", quickPayToken);//Claus Account
                client.BaseAddress = new Uri(quickPayUrl);

                var content = GenerateRequestParamKeyValuePair(details, 1);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                var response = client.PostAsync("/cards", content).Result;
                string responseString = response.Content.ReadAsStringAsync().Result;
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);
                if (response.StatusCode == HttpStatusCode.Created)
                {
                    var cardId = responseJSON.id;
                    var authorizeContent = GenerateRequestParamKeyValuePair(details, 2);
                    var authorizeCardResponse = client.PostAsync(string.Format("cards/{0}/authorize", cardId), authorizeContent).Result;

                    string authorizeResponseStr = authorizeCardResponse.Content.ReadAsStringAsync().Result;
                    dynamic authorizeResponseJSON = JsonConvert.DeserializeObject(authorizeResponseStr);
                    if (authorizeCardResponse.StatusCode == HttpStatusCode.Accepted)
                    {
                        var defaultCard = _customerCreditCardsOperation.GetAll(c => c.CustomerId == details.CustomerId && c.IsPrimary && c.IsActive).FirstOrDefault();
                        if (defaultCard == null)
                            details.IsPrimary = true;   //we make this card as primary by default, since first card is primary by default
                        else if (details.IsPrimary)
                        {
                            defaultCard.IsPrimary = false;
                            _customerCreditCardsOperation.Update(defaultCard.Id, defaultCard);  // make old non-primary
                        }
                        //saving quickpay cardId in our db
                        CustomerCreditCard custCard = new CustomerCreditCard()
                        {
                            CustomerId = details.CustomerId,
                            QuickPayCardId = authorizeResponseJSON.id,
                            CreationDate = DateTime.UtcNow,
                            IsPrimary = details.IsPrimary,
                            IsActive = true
                        };

                        var result = _customerCreditCardsOperation.AddNew(custCard);
                        var cardDetails = GenerateCardDetailsModel(authorizeResponseJSON, result);
                        return cardDetails;
                    }
                    else
                    {
                        if (authorizeCardResponse.StatusCode == HttpStatusCode.BadRequest)
                        {
                            var ErrMsg = GenerateActualErroMsg(authorizeResponseJSON);
                            throw new BadRequestException(ErrMsg);
                        }
                        else
                        {
                            var ErrMsg = GenerateActualErroMsg(authorizeResponseJSON);
                            throw new ForbiddenException(ErrMsg);
                        }
                    }
                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var ErrMsg = GenerateActualErroMsg(responseJSON);
                        throw new BadRequestException(ErrMsg);
                    }

                    else
                    {
                        var ErrMsg = GenerateActualErroMsg(responseJSON);
                        throw new ForbiddenException(ErrMsg);
                    }

                }
            }
        }
        /// <summary>
        /// Generates the actual erro MSG.
        /// </summary>
        /// <param name="responseJSON">The response json.</param>
        /// <returns>System.String.</returns>
        public static string GenerateActualErroMsg(dynamic responseJSON)
        {
            var actualErrMsg = "";
            if (responseJSON.errors != null)
            {
                JObject obj = JObject.Parse(responseJSON.errors.ToString());
                foreach (var pair in obj)
                {
                    actualErrMsg = ": " + pair.Key + " " + pair.Value.FirstOrDefault().ToString();
                }
            }
            var fullActualErrMsg = responseJSON.message.ToString() + actualErrMsg;
            return fullActualErrMsg;
        }
        /// <summary>
        /// Generates the request parameter key value pair.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="flag">The flag.</param>
        /// <returns>FormUrlEncodedContent.</returns>
        public static FormUrlEncodedContent GenerateRequestParamKeyValuePair(CustomerPaymentCards details, int flag)
        {
            FormUrlEncodedContent content = null;
            switch (flag)
            {
                case 1:
                    var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("variables[customer_id]",details.CustomerId.ToString()),
                        new KeyValuePair<string, string>("variables[CardLabel]",details.CardLabel),
                        new KeyValuePair<string, string>("variables[CardLabelColor]",details.CardLabelColor),
                        new KeyValuePair<string, string>("variables[SpendingLimitEnabled]",details.SpendingLimitEnabled.ToString()),
                        new KeyValuePair<string, string>("variables[SpendingLimitAmount]",details.SpendingLimitAmount.ToString()),
                        new KeyValuePair<string, string>("variables[Currency]",details.Currency),
                        new KeyValuePair<string, string>("variables[Duration]",details.Duration.ToString())
                    };
                    content = new FormUrlEncodedContent(pairs);
                    break;
                case 2:
                    var cardExpirationDate = details.CardExpiryDate.ToString("yy") + details.CardExpiryDate.ToString("MM");
                    var authorizePairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("card[number]",details.CardNumber),
                        new KeyValuePair<string, string>("card[expiration]",cardExpirationDate),
                        new KeyValuePair<string, string>("card[cvd]",details.CVVNumber),
                        new KeyValuePair<string, string>("card[issued_to]",details.CardHolderName)
                    };
                    content = new FormUrlEncodedContent(authorizePairs);
                    break;
            }
            return content;

        }

        /// <summary>
        /// Modifies the customer card.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="existingCard">The existing card.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>CustomerPaymentCards.</returns>
        /// <exception cref="BadRequestException"></exception>
        /// <exception cref="ForbiddenException"></exception>
        /// <exception cref="NotFoundException"></exception>
        public CustomerPaymentCards ModifyCustomerCard(CustomerPaymentCards details, CustomerCreditCard existingCard, string quickPayToken, string quickPayUrl)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept-Version", "v10");
                client.DefaultRequestHeaders.Add("Authorization", quickPayToken);//Claus Account
                client.BaseAddress = new Uri(quickPayUrl);

                var content = GenerateRequestParamKeyValuePair(details, 1);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                var response = client.SendAsync(new HttpRequestMessage(new HttpMethod("PATCH"), string.Format("cards/{0}", existingCard.QuickPayCardId))
                {
                    Content = content
                }).Result;
                string responseString = response.Content.ReadAsStringAsync().Result;
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var defaultCard = _customerCreditCardsOperation.GetAll(c => c.CustomerId == details.CustomerId && c.Id != details.Id && c.IsPrimary && c.IsActive).FirstOrDefault();
                    if (defaultCard == null)
                        details.IsPrimary = true;
                    else if (details.IsPrimary)
                    {
                        defaultCard.IsPrimary = false;
                        _customerCreditCardsOperation.Update(defaultCard.Id, defaultCard);  // make old non-primary
                    }
                    existingCard.IsPrimary = details.IsPrimary;
                    _customerCreditCardsOperation.Update(existingCard.Id, existingCard);

                    var cardDetails = GenerateCardDetailsModel(responseJSON, existingCard);
                    return cardDetails;

                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var ErrMsg = GenerateActualErroMsg(responseJSON);
                        throw new BadRequestException(ErrMsg);
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        var ErrMsg = GenerateActualErroMsg(responseJSON);
                        throw new ForbiddenException(ErrMsg);
                    }
                    else
                    {
                        var ErrMsg = GenerateActualErroMsg(responseJSON);
                        throw new NotFoundException(ErrMsg);
                    }

                }
            }
        }

        /// <summary>
        /// Deletes the customer card.
        /// </summary>
        /// <param name="cardDetails">The card details.</param>
        public void DeleteCustomerCard(CustomerCreditCard cardDetails)
        {
            var customerId = cardDetails.CustomerId;
            cardDetails.IsActive = false;
            _customerCreditCardsOperation.Update(cardDetails.Id, cardDetails);

            if (cardDetails.IsPrimary) //if primary card is deleted
            {
                var existingCards = _customerCreditCardsOperation.GetAll(c => c.CustomerId == customerId && c.QuickPayCardId != null && c.IsActive);

                if (existingCards.Count() > 0)
                {
                    var futureDefault = existingCards.First();

                    futureDefault.IsPrimary = true;

                    _customerCreditCardsOperation.Update(futureDefault.Id, futureDefault);
                }
            }
        }

        /// <summary>
        /// Generates the card details model.
        /// </summary>
        /// <param name="cardDetailsResponseJSON">The card details response json.</param>
        /// <param name="custCardInfo">The customer card information.</param>
        /// <returns>CustomerPaymentCards.</returns>
        public static CustomerPaymentCards GenerateCardDetailsModel(dynamic cardDetailsResponseJSON, CustomerCreditCard custCardInfo)
        {
            CustomerPaymentCards cardDetails = new CustomerPaymentCards();
            cardDetails.Id = custCardInfo.Id;
            cardDetails.CustomerId = custCardInfo.CustomerId;
            cardDetails.CardHolderName = cardDetailsResponseJSON.metadata["issued_to"];
            cardDetails.CardNumber = cardDetailsResponseJSON.metadata["last4"];
            var expDate = Convert.ToDateTime(cardDetailsResponseJSON.metadata["exp_month"] + "/" + cardDetailsResponseJSON.metadata["exp_year"]);
            cardDetails.CardExpiryDate = expDate;
            cardDetails.CardLabel = cardDetailsResponseJSON.variables["CardLabel"];
            cardDetails.CardLabelColor = cardDetailsResponseJSON.variables["CardLabelColor"];
            var spendingEnable = cardDetailsResponseJSON.variables["SpendingLimitEnabled"];
            cardDetails.SpendingLimitEnabled = string.IsNullOrEmpty(spendingEnable.Value) ? null : Convert.ToBoolean(spendingEnable);
            cardDetails.SpendingLimitAmount = Convert.ToInt32(cardDetailsResponseJSON.variables["SpendingLimitAmount"]);
            cardDetails.Currency = cardDetailsResponseJSON.variables["Currency"];
            var durationStr = cardDetailsResponseJSON.variables["Duration"];
            cardDetails.Duration = string.IsNullOrEmpty(durationStr.Value) ? null : Convert.ToInt32(durationStr);
            cardDetails.IsPrimary = custCardInfo.IsPrimary;
            cardDetails.CardType = cardDetailsResponseJSON.metadata["brand"];
            return cardDetails;
        }

        /// <summary>
        /// Valids the card expiry date.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>CardExpiryResponse.</returns>
        CardExpiryResponse ValidCardExpiryDate(DateTime date)
        {

            if (date == null || date == DateTime.MinValue)
                return new CardExpiryResponse()
                {
                    Status = false,
                    ResponseMessage = Resources.Max_CardExpiryDate
                };

            if (date > DateTime.Now.AddYears(15))
                return new CardExpiryResponse()
               {
                   Status = false,
                   ResponseMessage = Resources.Invalid_CardExpiryDate
               };

            if (date < DateTime.Now)
                return new CardExpiryResponse()
                {
                    Status = false,
                    ResponseMessage = Resources.Invalid_CardExpiryDate
                };

            return new CardExpiryResponse()
            {
                Status = true,
                ResponseMessage = "valid"
            };

        }
        #endregion credit cards

        #region paypal

        /// <summary>
        /// Updates the paypal details.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>PaypalDetail.</returns>
        public PaypalDetail UpdatePaypalDetails(PaypalDetail details)
        {
            var existing = _paypalDetailsRepository.Table.Where(p => p.CustomerId == details.CustomerId).FirstOrDefault();

            if (existing == null)
            {
                return _paypalDetailsRepository.Insert(details);
            }
            else
            {
                existing.PaypalEmail = details.PaypalEmail;
                existing.Password = details.Password;
                _paypalDetailsRepository.Update(existing);
                return existing;
            }

        }


        /// <summary>
        /// Gets the paypal details.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>PaypalDetail.</returns>
        public PaypalDetail GetPaypalDetails(int Id)
        {
            return _paypalDetailsRepository.Table.Where(p => p.CustomerId == Id).FirstOrDefault();

        }

        /// <summary>
        /// Deletes the paypal details.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void DeletePaypalDetails(int Id)
        {
            var details = _paypalDetailsRepository.Table.Where(p => p.CustomerId == Id).FirstOrDefault();

            if (details != null)
                _paypalDetailsRepository.Delete(details.Id);
            return;

        }


        #endregion paypal

        #region currency


        /// <summary>
        /// Gets all currencies.
        /// </summary>
        /// <returns>IEnumerable Currency.</returns>
        public IEnumerable<Currency> GetAllCurrencies()
        {
            return _currencyOperation.GetAll();
        }

        /// <summary>
        /// Gets the currency by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Currency.</returns>
        public Currency GetCurrencyById(int id)
        {
            return _currencyOperation.GetById(id);
        }

        #endregion currency

        #region Favorite MenuItem

        /// <summary>
        /// Customers the favorite menu item toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        public void CustomerFavoriteMenuItemToggle(int Id, int menuItemId)
        {
            var exists = _customerFavoriteMenuItemRepository.Table.Where(f => f.CustomerId == Id && f.RestaurantMenuItemId == menuItemId).FirstOrDefault();

            if (exists != null)
            {
                _customerFavoriteMenuItemRepository.Delete(exists.Id);
            }
            else
            {
                _customerFavoriteMenuItemRepository.Insert(new CustomerFavoriteMenuItem { CustomerId = Id, RestaurantMenuItemId = menuItemId });
            }
        }

        /// <summary>
        /// Gets the customer favorite menu items.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetCustomerFavoriteMenuItems(int Id)
        {
            return _customerFavoriteMenuItemRepository.Table.Where(f => f.CustomerId == Id)
                .Select(f => f.RestaurantMenuItem);
        }

        /// <summary>
        /// Gets the customer favorite menu items.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantFavoriteMenuItems.</returns>
        public IEnumerable<RestaurantFavoriteMenuItems> GetCustomerFavoriteMenuItems(int id, int pageNumber, int pageSize, out int total)
        {
            var query = _customerFavoriteMenuItemRepository.Table;
            query = query.Where(r => r.CustomerId == id);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantFavoriteMenuItems>();
            total = pages.Key.Total;
            var entities = pages.Select(p => new RestaurantFavoriteMenuItems
            {
                Id = p.RestaurantMenuItem.Id,
                RestaurantMenuGroupId = p.RestaurantMenuItem.RestaurantMenuGroupId,
                ItemName = p.RestaurantMenuItem.ItemName,
                ItemDescription = p.RestaurantMenuItem.ItemDescription,
                Price = p.RestaurantMenuItem.Price,
                IsActive = p.RestaurantMenuItem.IsActive,
                RestaurantMenuItemImages = p.RestaurantMenuItem.RestaurantMenuItemImages,
                Restaurant = p.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant
            });
            return entities.ToList();
        }

        /// <summary>
        /// Gets the customer order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        public CustomerOrderDetailsInfo GetCustomerOrderDetails(int customerId, int menuItemId)
        {
            var orderDetails = _customerRestaurantOrders.Table.Where(o => o.CustomerId == customerId && o.RestaurantOrderItems.Any(i => i.RestaurantMenuItemId == menuItemId)).ToList();
            CustomerOrderDetailsInfo custOrderDetails;
            if (orderDetails.Count == 0)
            {
                custOrderDetails = new CustomerOrderDetailsInfo();
                return custOrderDetails;
            }
            else
            {
                custOrderDetails = new CustomerOrderDetailsInfo()
                {
                    TotalOrders = orderDetails.Count(),
                    LastOrder = orderDetails.OrderByDescending(o => o.Id).FirstOrDefault().CreationDate
                };
                return custOrderDetails;
            }
        }


        #endregion Favorite MenuItem

        #region Country

        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <returns>IEnumerable Country.</returns>
        public IEnumerable<Country> GetAllCountries()
        {
            return _countryRepository.Table.ToList();
        }

        /// <summary>
        /// Gets the country by identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <returns>Country.</returns>
        public Country GetCountryById(int countryId)
        {
            return _countryRepository.GetById(countryId);
        }
        #endregion Country

        #region Menu items Based on preferences
        /// <summary>
        /// Gets the menu items based on preferences.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetMenuItemsBasedOnPreferences(int customerId, int restaurantId, int pageNumber, int pageSize, out int total)
        {
            var ingredientPref = _customerIngredientPreferencesOperation.GetAll(c => c.CustomerId == customerId).Select(i => i.IngredientId).ToList();
            var allergenPref = _customerAllergenPreferenceOperation.GetAll(c => c.CustomerId == customerId).Select(i => i.AllergenId).ToList();

            if (restaurantId == 0)
            {
                var menuItems = _customerMenuItemsOperations.GetAll(i => i.MenuItemIngredient.Any(m => ingredientPref.Contains(m.IngredientId) && m.Ingredient.AllergenIngredients.Count(a => allergenPref.Contains(a.AllergenID)) <= 0), "Asc", pageNumber, pageSize, out total).ToList();
                return menuItems;
            }
            else
            {
                var menuItems = _customerMenuItemsOperations.GetAll(i => i.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId && i.MenuItemIngredient.Any(m => ingredientPref.Contains(m.IngredientId) && m.Ingredient.AllergenIngredients.Count(a => allergenPref.Contains(a.AllergenID)) <= 0), "Asc", pageNumber, pageSize, out total).ToList();
                return menuItems;
            }

        }
        #endregion

        #region Spa Ingredient Preference

        /// <summary>
        /// Gets the customer spa ingredient preference.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerSpaServicePreference.</returns>
        public IEnumerable<CustomerSpaServicePreference> GetCustomerSpaIngredientPreference(int customerId)
        {
            return _customerSpaServicePreferenceOperation.GetAll(p => p.CustomerId == customerId);
        }

        /// <summary>
        /// Modifies the spa ingredient preference.
        /// </summary>
        /// <param name="spaIngredientPref">The spa ingredient preference.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">INVALID_CUSTOMER_ID</exception>
        public string ModifySpaIngredientPreference(CustomerSpaServicePreference spaIngredientPref)
        {
            if (spaIngredientPref.CustomerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");
            var oldIngPref = _customerSpaServicePreferenceOperation.GetById(x => x.SpaIngredientId == spaIngredientPref.SpaIngredientId && x.CustomerId == spaIngredientPref.CustomerId);
            if (oldIngPref == null)
                _customerSpaServicePreferenceRepository.Insert(spaIngredientPref);
            else
            {
                oldIngPref.Include = spaIngredientPref.Include;
                _customerSpaServicePreferenceRepository.Update(oldIngPref);
            }
            return "Success";

        }

        #endregion Spa Ingredient Preference

        #region Favorite Spa Service

        /// <summary>
        /// Customers the favorite spa service toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        public void CustomerFavoriteSpaServiceToggle(int Id, int spaServiceDetailId)
        {
            var exists = _customerFavoriteSpaServiceRepository.Table.Where(s => s.CustomerId == Id && s.SpaServiceDetailId == spaServiceDetailId).FirstOrDefault();

            if (exists != null)
            {
                _customerFavoriteSpaServiceRepository.Delete(exists.Id);
            }
            else
            {
                _customerFavoriteSpaServiceRepository.Insert(new CustomerFavoriteSpaService { CustomerId = Id, SpaServiceDetailId = spaServiceDetailId });
            }
        }

        /// <summary>
        /// Gets the customer favorite spa service details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetCustomerFavoriteSpaServiceDetails(int id, int pageNumber, int pageSize, out int total)
        {
            return _customerFavoriteSpaServiceOperations.GetAllByDesc(s => s.CustomerId == id, s => s.Id, pageNumber, pageSize, out total).Select(sp => sp.SpaServiceDetail);
        }

        /// <summary>
        /// Gets the customer favorite spa service details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetCustomerFavoriteSpaServiceDetails(int customerId)
        {
            return _customerFavoriteSpaServiceOperations.GetAll(s => s.CustomerId == customerId).Select(sp => sp.SpaServiceDetail);
        }

        /// <summary>
        /// Gets the customer spa order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        public CustomerOrderDetailsInfo GetCustomerSpaOrderDetails(int customerId, int spaServiceDetailId)
        {
            var orderDetails = _customerSpaOrderRepository.Table.Where(o => o.CustomerId == customerId && o.SpaOrderItems.Any(e => e.SpaServiceDetailId == spaServiceDetailId)).ToList();
            CustomerOrderDetailsInfo custOrderDetails;
            if (orderDetails.Count == 0)
            {
                custOrderDetails = new CustomerOrderDetailsInfo();
                return custOrderDetails;
            }
            else
            {
                custOrderDetails = new CustomerOrderDetailsInfo()
                {
                    TotalOrders = orderDetails.Count(),
                    LastOrder = orderDetails.OrderByDescending(o => o.Id).FirstOrDefault().CreationDate
                };
                return custOrderDetails;
            }
        }
        #endregion Favorite Spa Service

        #region Favorite Excursion Detail

        /// <summary>
        /// Customers the favorite excursion toggle.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        public void CustomerFavoriteExcursionToggle(int Id, int excursionDetailId)
        {
            var exists = _customerFavoriteExcursionRepository.Table.Where(e => e.CustomerId == Id && e.ExcursionDetailId == excursionDetailId).FirstOrDefault();

            if (exists != null)
            {
                _customerFavoriteExcursionRepository.Delete(exists.Id);
            }
            else
            {
                _customerFavoriteExcursionRepository.Insert(new CustomerFavoriteExcursion { CustomerId = Id, ExcursionDetailId = excursionDetailId });
            }
        }

        /// <summary>
        /// Gets the customer favorite excursion details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetCustomerFavoriteExcursionDetails(int id, int pageNumber, int pageSize, out int total)
        {
            return _customerFavoriteExcursionOperations.GetAllByDesc(e => e.CustomerId == id, e => e.Id, pageNumber, pageSize, out total).Select(ep => ep.ExcursionDetails);
        }

        /// <summary>
        /// Gets the customer favorite excursion details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetCustomerFavoriteExcursionDetails(int customerId)
        {
            return _customerFavoriteExcursionOperations.GetAll(e => e.CustomerId == customerId).Select(ep => ep.ExcursionDetails);
        }

        /// <summary>
        /// Gets the customer excursion order details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>CustomerOrderDetailsInfo.</returns>
        public CustomerOrderDetailsInfo GetCustomerExcursionOrderDetails(int customerId, int excursionDetailId)
        {
            var orderDetails = _customerExcursionOrderRepository.Table.Where(o => o.CustomerId == customerId && o.ExcursionOrderItems.Any(e => e.ExcursionDetailsId == excursionDetailId)).ToList();
            CustomerOrderDetailsInfo custOrderDetails;
            if (orderDetails.Count == 0)
            {
                custOrderDetails = new CustomerOrderDetailsInfo();
                return custOrderDetails;
            }
            else
            {
                custOrderDetails = new CustomerOrderDetailsInfo()
                {
                    TotalOrders = orderDetails.Count(),
                    LastOrder = orderDetails.OrderByDescending(o => o.Id).FirstOrDefault().CreationDate
                };
                return custOrderDetails;
            }
        }
        #endregion Favorite Excursion Detail

        #region Excursion Ingredient Preference

        /// <summary>
        /// Gets the customer excursion ingredient preference.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerExcursionPreferences.</returns>
        public IEnumerable<CustomerExcursionPreferences> GetCustomerExcursionIngredientPreference(int customerId)
        {
            return _customerExcursionPreferencesOperations.GetAll(p => p.CustomerId == customerId);
        }

        /// <summary>
        /// Modifies the excursion ingredient preference.
        /// </summary>
        /// <param name="excursionIngredientPref">The excursion ingredient preference.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">INVALID_CUSTOMER_ID</exception>
        public string ModifyExcursionIngredientPreference(CustomerExcursionPreferences excursionIngredientPref)
        {
            if (excursionIngredientPref.CustomerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");
            var oldIngPref = _customerExcursionPreferencesOperations.GetById(x => x.ExcursionIngredientId == excursionIngredientPref.ExcursionIngredientId && x.CustomerId == excursionIngredientPref.CustomerId);
            if (oldIngPref == null)
                _customerExcursionPreferenceRepository.Insert(excursionIngredientPref);
            else
            {
                oldIngPref.Include = excursionIngredientPref.Include;
                _customerExcursionPreferenceRepository.Update(oldIngPref);
            }
            return "Success";

        }

        #endregion Excursion Ingredient Preference

        #region language

        /// <summary>
        /// Gets all languages.
        /// </summary>
        /// <returns>IEnumerable Language.</returns>
        public IEnumerable<Language> GetAllLanguages()
        {
            return _languageRepository.Table.ToList();
        }

        #endregion language

        #region WakeUp

        /// <summary>
        /// Gets the customer wake up call.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable WakeUp.</returns>
        public IEnumerable<WakeUp> GetCustomerWakeUpCall(int customerId)
        {
            return _wakeUpOperation.GetAll(c => c.CustomerId == customerId && c.IsCompleted == false);
        }

        /// <summary>
        /// Gets the customer wake upcall.
        /// </summary>
        /// <param name="wakeupId">The wakeup identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>WakeUp.</returns>
        public WakeUp GetCustomerWakeUpcall(int wakeupId, int customerId)
        {
            return _wakeUpOperation.GetById(c => c.Id == wakeupId && c.CustomerId == customerId);
        }
        /// <summary>
        /// Creates the customer wake upcall.
        /// </summary>
        /// <param name="wakeUp">The wake up.</param>
        /// <returns>WakeUp.</returns>
        /// <exception cref="System.ArgumentException">
        /// NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID
        /// or
        /// PLEASE_PROVIDE_VALID_TIME_INTERVAL
        /// </exception>
        public WakeUp CreateCustomerWakeUpcall(WakeUp wakeUp)
        {
            var room = _hotelRoomsOperation.GetById(r => r.Id == wakeUp.RoomId && r.IsActive);
            if (room == null)
                throw new ArgumentException("NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID");

            if (wakeUp.IsRepeated == true && wakeUp.Interval == null)
                throw new ArgumentException("PLEASE_PROVIDE_VALID_TIME_INTERVAL");

            wakeUp.CreationDate = DateTime.Now;
            return _wakeUpOperation.AddNew(wakeUp);
        }
        /// <summary>
        /// Deletes the customer wake up call.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void DeleteCustomerWakeUpCall(int Id)
        {
            _wakeUpOperation.Delete(Id);
        }

        #endregion WakeUp

        #region Spa Pending Reviews
        /// <summary>
        /// Gets the spa service detail pending reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetSpaServiceDetailPendingReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            total = 0;
            var spaServiceDet = _customerSpaOrderItemRepository.Table.Where(o => (o.SpaOrder.CustomerId == customerId && o.SpaOrder.OrderStatus == 2) && o.SpaServiceDetail.SpaServiceReview.Count(i => i.CustomerId == customerId) == 0).Select(o => o.SpaServiceDetail).Distinct();

            var page = spaServiceDet.OrderBy(i => i.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = spaServiceDet.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<SpaServiceDetail>();

            total = page.Key.Total;
            var spaSerDetEntities = page.Select(p => p);

            return spaSerDetEntities;
        }
        #endregion Spa Pending Reviews

        #region Excursion Details Pending Reviews
        /// <summary>
        /// Gets the excursion detail pending reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetExcursionDetailPendingReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            total = 0;
            var excursionDet = _customerExcursionOrderItemsRepository.Table.Where(o => (o.ExcursionOrder.CustomerId == customerId && o.ExcursionOrder.OrderStatus == 2) && o.ExcursionDetails.ExcursionReviews.Count(i => i.CustomerId == customerId) == 0).Select(o => o.ExcursionDetails).Distinct();

            var page = excursionDet.OrderBy(i => i.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = excursionDet.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<ExcursionDetail>();

            total = page.Key.Total;
            var excursionDetEntities = page.Select(p => p);

            return excursionDetEntities;
        }

        #endregion Excursion Details Pending Reviews

        #region Hotel Services Reviews
        /// <summary>
        /// Gets the hotel all services reviews of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelAllServicesReviews.</returns>
        public IEnumerable<HotelAllServicesReviews> GetHotelAllServicesReviewsOfCust(int customerId,int pageNumber, int pageSize, out int total)
        {
            System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            var custHotelServicesReviews = _customerAllOrdersRepository.ExecuteStoredProcedureList<HotelAllServicesReviews>("exec uspGetCustomerAllServicesReviews", customerIdParameter);
            var pages = custHotelServicesReviews
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .GroupBy(p => new { Total = custHotelServicesReviews.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HotelAllServicesReviews>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();

        }
        #endregion Hotel Services Reviews

        #region Customer Payment History
        /// <summary>
        /// Gets the customer hotel and rest payment history.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="customerPrefCurrencyId">The customer preference currency identifier.</param>
        /// <param name="year">The year.</param>
        /// <returns>IEnumerable CustomerMonthlyPaymentHistory.</returns>
        public IEnumerable<CustomerMonthlyPaymentHistory> GetCustomerHotelAndRestPaymentHistory(int customerId,int customerPrefCurrencyId,int year)
        {
            System.Data.SqlClient.SqlParameter customerIdParam = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            System.Data.SqlClient.SqlParameter custPrefCurrencyId = new System.Data.SqlClient.SqlParameter("custPrefCurrencyId", Convert.ToInt32(customerPrefCurrencyId));
            System.Data.SqlClient.SqlParameter yearParam = new System.Data.SqlClient.SqlParameter("year", Convert.ToInt32(year));
            var custPaymentHistory = _customerRestaurantOrders.ExecuteStoredProcedureList<CustomerMonthlyPaymentHistory>("exec uspGetCustomerAllHotelAndRestPaymentHistory", customerIdParam,custPrefCurrencyId, yearParam);
            return custPaymentHistory;
        }

        #endregion Customer Payment History
    }
}