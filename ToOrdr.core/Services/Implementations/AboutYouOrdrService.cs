﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 12-27-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-15-2019
// ***********************************************************************
// <copyright file="AboutYouOrdrService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class AboutYouOrdrService.
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IAboutYouOrdrService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IAboutYouOrdrService" />
   public class AboutYouOrdrService : IAboutYouOrdrService
    {
        /// <summary>
        /// The about youordr repository
        /// </summary>
       IRepository<AboutYouOrdr> _aboutYouOrdrRepository;
       /// <summary>
       /// The languages repository
       /// </summary>
       IRepository<Language> _languagesRepository;

       /// <summary>
       /// The about youordr operations
       /// </summary>
       CRUDOperation<IRepository<AboutYouOrdr>, AboutYouOrdr> _aboutYouOrdrOperations;

       /// <summary>
       /// Initializes a new instance of the <see cref="AboutYouOrdrService"/> class.
       /// </summary>
       /// <param name="aboutYouOrdrRepository">The about youordr repository.</param>
       /// <param name="languagesRepository">The languages repository.</param>
       public AboutYouOrdrService(IRepository<AboutYouOrdr> aboutYouOrdrRepository, IRepository<Language> languagesRepository)
       {
           _aboutYouOrdrRepository = aboutYouOrdrRepository;
           _languagesRepository = languagesRepository;

           _aboutYouOrdrOperations = new CRUDOperation<IRepository<AboutYouOrdr>, AboutYouOrdr>(_aboutYouOrdrRepository);
       }

       /// <summary>
       /// Gets youordr description.
       /// </summary>
       /// <param name="langId">The language identifier.</param>
       /// <returns>AboutYouOrdr.</returns>
       public AboutYouOrdr GetYouOrdrDescription(int langId)
       {
           return _aboutYouOrdrOperations.GetById(l => l.LanguageId == langId && l.IsActive);
       }
       /// <summary>
       /// Gets all languages youordr description.
       /// </summary>
       /// <param name="skip">The skip.</param>
       /// <param name="pageSize">Size of the page.</param>
       /// <param name="total">The total.</param>
       /// <returns>IEnumerable AboutYouOrdr.</returns>
       public IEnumerable<AboutYouOrdr> GetAllLanguagesYouOrdrDescription(int skip,int pageSize,out int total)
       {
           return _aboutYouOrdrOperations.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
       }

       /// <summary>
       /// Gets youordr description by identifier.
       /// </summary>
       /// <param name="id">The identifier.</param>
       /// <returns>AboutYouOrdr.</returns>
       public AboutYouOrdr GetYouOrdrDescriptionById(int id)
       {
           return _aboutYouOrdrOperations.GetById(id);
       }

       /// <summary>
       /// Creates the about youordr description.
       /// </summary>
       /// <param name="abtYouOrdr">The abt youordr.</param>
       /// <returns>AboutYouOrdr.</returns>
       public AboutYouOrdr CreateAboutYouOrdrDescription(AboutYouOrdr abtYouOrdr)
       {
           abtYouOrdr.CreationDate = DateTime.Now;
           return _aboutYouOrdrOperations.AddNew(abtYouOrdr);
       }

       /// <summary>
       /// Modifies the about youordr descriotion.
       /// </summary>
       /// <param name="abtYouOrdr">The abt youordr.</param>
       /// <returns>AboutYouOrdr.</returns>
       public AboutYouOrdr ModifyAboutYouOrdrDescriotion(AboutYouOrdr abtYouOrdr)
       {
           _aboutYouOrdrOperations.Update(abtYouOrdr.Id, abtYouOrdr);
           return abtYouOrdr;
       }

       /// <summary>
       /// Deletes the about youordr desciption.
       /// </summary>
       /// <param name="id">The identifier.</param>
       public void DeleteAboutYouOrdrDesciption(int id)
       {
           _aboutYouOrdrOperations.Delete(id);
       }

       /// <summary>
       /// Gets the unassigned languages.
       /// </summary>
       /// <returns>IEnumerable Language.</returns>
       public IEnumerable<Language> GetUnassignedLanguages()
       {
           var descAlreadyCreatedLangsId = _aboutYouOrdrOperations.GetAll().Select(l => l.LanguageId).Distinct();
           var unassignedLanguages = _languagesRepository.Table.Where(l => !descAlreadyCreatedLangsId.Contains(l.Id));
           return unassignedLanguages;
       }

    }
}
