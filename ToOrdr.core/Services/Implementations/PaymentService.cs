﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 03-14-2019
// ***********************************************************************
// <copyright file="PaymentService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Helpers;
using ToOrdr.Core.Services.Interfaces;
using PayPal.Api;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using ToOrdr.Core.Exceptions;
using System.Data.Entity;
using PayPal;
using System.IO;
using ToOrdr.Localization;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class PaymentService.
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IPaymentService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IPaymentService" />
    public class PaymentService : IPaymentService
    {

        /// <summary>
        /// The restaurant order repository
        /// </summary>
        IRepository<RestaurantOrder> _restaurantOrderRepository;
        /// <summary>
        /// The credit card repository
        /// </summary>
        IRepository<CustomerCreditCard> _creditCardRepository;
        /// <summary>
        /// The spa order repository
        /// </summary>
        IRepository<SpaOrder> _spaOrderRepository;
        /// <summary>
        /// The laundry order repository
        /// </summary>
        IRepository<LaundryOrder> _laundryOrderRepository;
        /// <summary>
        /// The restaurant tips repository
        /// </summary>
        IRepository<RestaurantTip> _restaurantTipsRepository;
        /// <summary>
        /// The housekeeping order repository
        /// </summary>
        IRepository<HousekeepingOrder> _housekeepingOrderRepository;
        /// <summary>
        /// The excursion order repository
        /// </summary>
        IRepository<ExcursionOrder> _excursionOrderRepository;
        /// <summary>
        /// The spa tip repository
        /// </summary>
        IRepository<SpaTip> _spaTipRepository;
        /// <summary>
        /// The laundry tip repository
        /// </summary>
        IRepository<LaundryTip> _laundryTipRepository;
        /// <summary>
        /// The housekeeping tip repository
        /// </summary>
        IRepository<HousekeepingTip> _housekeepingTipRepository;
        /// <summary>
        /// The excursion tips repository
        /// </summary>
        IRepository<ExcursionTips> _excursionTipsRepository;

        /// <summary>
        /// The restaurant orders operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder> _restaurantOrdersOperations;
        /// <summary>
        /// The spa orders operations
        /// </summary>
        CRUDOperation<IRepository<SpaOrder>, SpaOrder> _spaOrdersOperations;
        /// <summary>
        /// The restaurant tips operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantTip>, RestaurantTip> _restaurantTipsOperations;
        /// <summary>
        /// The housekeeping order oprations
        /// </summary>
        CRUDOperation<IRepository<HousekeepingOrder>, HousekeepingOrder> _housekeepingOrderOprations;
        /// <summary>
        /// The spa tip oprations
        /// </summary>
        CRUDOperation<IRepository<SpaTip>, SpaTip> _spaTipOprations;
        /// <summary>
        /// The laundry tip oprations
        /// </summary>
        CRUDOperation<IRepository<LaundryTip>, LaundryTip> _laundryTipOprations;
        /// <summary>
        /// The housekeeping tip oprations
        /// </summary>
        CRUDOperation<IRepository<HousekeepingTip>, HousekeepingTip> _housekeepingTipOprations;
        /// <summary>
        /// The excursion tips operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionTips>, ExcursionTips> _excursionTipsOperations;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentService"/> class.
        /// </summary>
        /// <param name="orderRepository">The order repository.</param>
        /// <param name="creditCardRepository">The credit card repository.</param>
        /// <param name="spaOrderRepository">The spa order repository.</param>
        /// <param name="laundryOrderRepository">The laundry order repository.</param>
        /// <param name="restaurantTipsRepository">The restaurant tips repository.</param>
        /// <param name="housekeepingOrderRepository">The housekeeping order repository.</param>
        /// <param name="excursionOrderRepository">The excursion order repository.</param>
        /// <param name="spaTipRepository">The spa tip repository.</param>
        /// <param name="laundryTipRepository">The laundry tip repository.</param>
        /// <param name="housekeepingTipRepository">The housekeeping tip repository.</param>
        /// <param name="excursionTipsRepository">The excursion tips repository.</param>
        public PaymentService(IRepository<RestaurantOrder> orderRepository,
            IRepository<CustomerCreditCard> creditCardRepository,
            IRepository<SpaOrder> spaOrderRepository,
            IRepository<LaundryOrder> laundryOrderRepository,
            IRepository<RestaurantTip> restaurantTipsRepository,
            IRepository<HousekeepingOrder> housekeepingOrderRepository,
            IRepository<ExcursionOrder> excursionOrderRepository,
            IRepository<SpaTip> spaTipRepository,
            IRepository<LaundryTip> laundryTipRepository,
            IRepository<HousekeepingTip> housekeepingTipRepository,
            IRepository<ExcursionTips> excursionTipsRepository)
        {
            _restaurantOrderRepository = orderRepository;
            _creditCardRepository = creditCardRepository;
            _spaOrderRepository = spaOrderRepository;
            _laundryOrderRepository = laundryOrderRepository;
            _restaurantTipsRepository = restaurantTipsRepository;
            _housekeepingOrderRepository = housekeepingOrderRepository;
            _excursionOrderRepository = excursionOrderRepository;
            _spaTipRepository = spaTipRepository;
            _laundryTipRepository = laundryTipRepository;
            _housekeepingTipRepository = housekeepingTipRepository;
            _excursionTipsRepository = excursionTipsRepository;

            _restaurantOrdersOperations = new CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder>(_restaurantOrderRepository);
            _spaOrdersOperations = new CRUDOperation<IRepository<SpaOrder>, SpaOrder>(_spaOrderRepository);
            _restaurantTipsOperations = new CRUDOperation<IRepository<RestaurantTip>, RestaurantTip>(_restaurantTipsRepository);
            _housekeepingOrderOprations = new CRUDOperation<IRepository<HousekeepingOrder>, HousekeepingOrder>(_housekeepingOrderRepository);
            _spaTipOprations = new CRUDOperation<IRepository<SpaTip>, SpaTip>(_spaTipRepository);
            _laundryTipOprations = new CRUDOperation<IRepository<LaundryTip>, LaundryTip>(_laundryTipRepository);
            _housekeepingTipOprations = new CRUDOperation<IRepository<HousekeepingTip>, HousekeepingTip>(_housekeepingTipRepository);
            _excursionTipsOperations = new CRUDOperation<IRepository<ExcursionTips>, ExcursionTips>(_excursionTipsRepository);
        }
        //public string CardPayment(int orderId, int? cardId, int customerId)
        //{
        //    var order = _restaurantOrderRepository.GetById(orderId);
        //    if (order == null)
        //        throw new ArgumentException("Order Id Not found");

        //    var custCardId = 0;
        //    if (cardId == null || cardId <= 0)
        //        custCardId = order.CardId;
        //    else
        //        custCardId = Convert.ToInt32(cardId);

        //    var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerId && c.IsActive).FirstOrDefault();
        //    if (card == null)
        //        throw new ArgumentException("Invalid card id");

        //    var total = order.OrderTotal;
        //    double? customerOrderTotal = 0;

        //    if (card.SpendingLimitEnabled == true)
        //    {
        //        if (card.Duration == 0)
        //            customerOrderTotal = _restaurantOrdersOperations.GetAll(c => c.CustomerId == customerId && c.CardId == custCardId && c.CreationDate.Date == DateTime.Now.Date && c.OrderStatus == 1)
        //                                                                      .Sum(c => c.OrderTotal);

        //        if (card.Duration == 1)
        //            customerOrderTotal = _restaurantOrdersOperations.GetAll(c => c.CustomerId == customerId && c.CardId == custCardId && c.CreationDate.Date >= c.CreationDate.Date.AddDays(-(int)c.CreationDate.DayOfWeek) && c.CreationDate.Date < c.CreationDate.Date.AddDays(-(int)c.CreationDate.DayOfWeek).AddDays(7) && c.OrderStatus == 1)
        //                                                                      .Sum(c => c.OrderTotal);

        //        if (card.Duration == 2)
        //            customerOrderTotal = _restaurantOrdersOperations.GetAll(c => c.CustomerId == customerId && c.CardId == custCardId && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.OrderStatus == 1)
        //                                                                      .Sum(c => c.OrderTotal);
        //        total = total + customerOrderTotal;
        //        if (total > card.SpendingLimitAmount)
        //            throw new CardSpendingLimitException("CARD_SPENDING_LIMIT_OVER");
        //    }

        //    double additional = order.RestaurantOrderItems.Sum(a => a.RestaurantOrderItemAdditionals.Sum(p => p.Qty * p.MenuAdditionalElement.AdditionalCost)).Value;
        //    double totalOrderItem = order.RestaurantOrderItems.Sum(o => o.RestaurantMenuItem.Price * o.Qty).Value;
        //    float amount = (float)(additional + totalOrderItem);

        //    using (WebClient client = new WebClient())
        //    {
        //        client.Headers.Add("Accept-Version", "v10");
        //        //client.Headers.Add("Authorization", "Basic OmVjNTQ2NjA1NWRmN2Q2Y2E3YmJjNmE5NDI1ZTRhZTNlZmY0YTMzNjlhODRlMGI1MGRkYTEyMjA1YzJmNmVhZTQ");//Rajesh Account
        //        //client.Headers.Add("Authorization", "Basic OjlkZjU0ODFkNjE2NzM0ZmE3OWZmODA3ZmNkNmVkMzM2Y2VlMjk4NWZhM2MzMDE5OTEzNTYwN2EwNGZjMjliMGM=");//Manoj Account
        //        client.Headers.Add("Authorization", "Basic OjEwOTY4MGM1YmJhMDNlYWI2MWQ4MTY4MDBkNmViMjA3N2ZjZDU0ZGM2ZmIxNzM3MzAzY2E1NjI2ODNkYmVlOWE=");//Claus Account

        //        client.BaseAddress = @"https://api.quickpay.net";

        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
        //        var response = client.UploadValues("/payments", new System.Collections.Specialized.NameValueCollection()
        //            {
        //                {"currency","dkk"},
        //                {"order_id", DateTime.Now.Ticks.ToString() } // todo: order.Id
        //            });

        //        string responseString = System.Text.Encoding.UTF8.GetString(response);
        //        dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

        //        string id = responseJSON.id;
        //        var linkResponse = client.UploadValues(string.Format("payments/{0}/link", id), "put", new System.Collections.Specialized.NameValueCollection()
        //        {
        //            {"amount",amount.ToString()},
        //            {"auto_capture","true"}
        //        });

        //        dynamic linqJson = JsonConvert.DeserializeObject(System.Text.Encoding.UTF8.GetString(linkResponse));

        //        string linq = linqJson.url;
        //        var page = client.DownloadString(linq);

        //        // use html agility pack to traverse page and retrieve session value
        //        // then post it to https://payment.quickpay.net/process_card
        //        // with 
        //        //session_id:cc2dc18d7052417c86798afb2451ee86
        //        //cardnumber:1000 0000 0000 0008
        //        //expiration[month]:10
        //        //expiration[year]:20
        //        //cvd:123

        //        var doc = new HtmlDocument();
        //        doc.LoadHtml(page);

        //        var sessionNodes = doc.DocumentNode.Descendants("input");
        //        string sessionValue = string.Empty;
        //        if (sessionNodes != null && sessionNodes.Count() > 0)
        //        {
        //            sessionValue = sessionNodes.First(el => el.Attributes["name"].Value == "session_id" && el.Attributes["type"].Value == "hidden").Attributes["value"].Value;
        //        }

        //        var res = client.UploadValues("https://payment.quickpay.net/process_card", new System.Collections.Specialized.NameValueCollection()
        //            {
        //                {"session_id",sessionValue},
        //                {"cardnumber",card.CardNumber},
        //                {"expiration[month]",card.CardExpiryDate.Month.ToString()},
        //                {"expiration[year]",card.CardExpiryDate.ToString("yy")},
        //                {"cvd",card.CVVNumber}
        //            }
        //            );

        //        var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(id)));
        //        string payResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
        //        dynamic payResponseJSON = JsonConvert.DeserializeObject(payResponseString);

        //        string status = payResponseJSON.state;

        //        if (status == "processed")
        //        {
        //            status = "Approved";
        //            order.PaymentStatus = true;
        //            _restaurantOrderRepository.Update(order);
        //        }
        //        return status;
        //    }
        //}

        /// <summary>
        /// Checks the card spending limit.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerDetails">The customer details.</param>
        /// <param name="appService">The application service.</param>
        /// <param name="isTips">if set to <c>true</c> [is tips].</param>
        /// <param name="tipAmount">The tip amount.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">
        /// Invalid card id
        /// </exception>
        public string CheckCardSpendingLimit(int orderId, string quickPayToken, string quickPayUrl, int? cardId, Customer customerDetails, AppServices appService, bool isTips, double? tipAmount)
        {
            var currentOrderTotal = 0.0;
            var custCardId = 0;
            if (isTips)
            {
                if (cardId == null || cardId <= 0)
                    throw new ArgumentException(Resources.Rqd_CreditCardId);
                else
                    custCardId = Convert.ToInt32(cardId);

                if (tipAmount == null || tipAmount <= 0)
                    throw new ArgumentException(Resources.Rqd_TipAmount);
                else
                    currentOrderTotal = Convert.ToDouble(tipAmount);
            }
            else
            {
                switch (appService)
                {
                    case AppServices.Restaurant:
                        var restOrder = _restaurantOrderRepository.GetById(orderId);
                        if (restOrder == null)
                            throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                        if (restOrder.PaymentStatus)
                            throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);
                        if (cardId == null || cardId <= 0)
                            custCardId = Convert.ToInt32(restOrder.CardId);
                        else
                            custCardId = Convert.ToInt32(cardId);
                        currentOrderTotal = restOrder.OrderTotal.Value;
                        break;
                    case AppServices.Spa:
                        var spaOrder = _spaOrderRepository.GetById(orderId);
                        if (spaOrder == null)
                            throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                        if (spaOrder.PaymentStatus)
                            throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                        if (cardId == null || cardId <= 0)
                            custCardId = spaOrder.CardId;
                        else
                            custCardId = Convert.ToInt32(cardId);
                        currentOrderTotal = spaOrder.OrderTotal.Value;
                        break;

                    case AppServices.Laundry:
                        var laundryOrder = _laundryOrderRepository.GetById(orderId);
                        if (laundryOrder == null)
                            throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                        if (laundryOrder.PaymentStatus)
                            throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                        if (cardId == null || cardId <= 0)
                            custCardId = laundryOrder.CardId;
                        else
                            custCardId = Convert.ToInt32(cardId);
                        currentOrderTotal = laundryOrder.OrderTotal.Value;
                        break;

                    case AppServices.Housekeeping:
                        var housekeepingOrder = _housekeepingOrderRepository.GetById(orderId);
                        if (housekeepingOrder == null)
                            throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                        if (housekeepingOrder.PaymentStatus)
                            throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                        if (cardId == null || cardId <= 0)
                            custCardId = housekeepingOrder.CardId;
                        else
                            custCardId = Convert.ToInt32(cardId);
                        currentOrderTotal = housekeepingOrder.OrderTotal.Value;
                        break;
                    case AppServices.Excursion:
                        var excursionOrder = _excursionOrderRepository.GetById(orderId);
                        if (excursionOrder == null)
                            throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                        if (excursionOrder.PaymentStatus)
                            throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                        if (cardId == null || cardId <= 0)
                            custCardId = excursionOrder.CardId;
                        else
                            custCardId = Convert.ToInt32(cardId);
                        currentOrderTotal = excursionOrder.OrderTotal.Value;
                        break;
                }
            }


            var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerDetails.Id && c.IsActive).FirstOrDefault();
            if (card == null)
                throw new ArgumentException("Invalid card id");

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                //Get customer card details
                var cardDetailsResponse = client.DownloadData(string.Format("cards/{0}", card.QuickPayCardId));

                string cardDetailsResponseStr = System.Text.Encoding.UTF8.GetString(cardDetailsResponse);
                dynamic cardDetailsResponseJSON = JsonConvert.DeserializeObject(cardDetailsResponseStr);
                if (cardDetailsResponseJSON != null)
                {
                    var cardDetails = CustomerService.GenerateCardDetailsModel(cardDetailsResponseJSON, card);

                    if (cardDetails.SpendingLimitEnabled == true)
                    {
                        var alreadySpentAmtTotal = GetAlreadySpentAmountFromCardOfCustomer(customerDetails, custCardId, cardDetails.Duration);
                        var totalAmoutToBeSpentFromCard = currentOrderTotal + alreadySpentAmtTotal;
                        if (totalAmoutToBeSpentFromCard > cardDetails.SpendingLimitAmount)
                            return "Your card spending limit is exceeded";
                    }
                    return "Please proceed to payment";
                }
                return "Card Not Found";
            }
        }

        /// <summary>
        /// Gets the already spent amount from card of customer.
        /// </summary>
        /// <param name="customerDetails">The customer details.</param>
        /// <param name="custCardId">The customer card identifier.</param>
        /// <param name="duration">The duration.</param>
        /// <returns>System.Nullable&lt;System.Double&gt;.</returns>
        public static double? GetAlreadySpentAmountFromCardOfCustomer(Customer customerDetails, int custCardId, int duration)
        {
            double? amountSpendFromCard = 0;
            switch (duration)
            {
                case 0:
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date == DateTime.Now.Date && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(r => r.RestaurantTips.Count > 0 && r.RestaurantTips.FirstOrDefault().CardId == custCardId
                                                                                        && r.RestaurantTips.FirstOrDefault().CreationDate.Date == DateTime.Now.Date)
                                                                                        .Sum(rot => rot.RestaurantTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.SpaOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date == DateTime.Now.Date && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.SpaOrder.Where(s => s.SpaTips.Count > 0 && s.SpaTips.FirstOrDefault().CardId == custCardId
                                                                                        && s.SpaTips.FirstOrDefault().CreationDate.Date == DateTime.Now.Date)
                                                                                        .Sum(sot => sot.SpaTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.LaundryOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date == DateTime.Now.Date && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.LaundryOrder.Where(l => l.LaundryTips.Count > 0 && l.LaundryTips.FirstOrDefault().CardId == custCardId
                                                                                        && l.LaundryTips.FirstOrDefault().CreationDate.Date == DateTime.Now.Date)
                                                                                        .Sum(lot => lot.LaundryTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date == DateTime.Now.Date && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(h => h.HousekeepingTips.Count > 0 && h.HousekeepingTips.FirstOrDefault().CardId == custCardId
                                                                                        && h.HousekeepingTips.FirstOrDefault().CreationDate.Date == DateTime.Now.Date)
                                                                                        .Sum(hot => hot.HousekeepingTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date == DateTime.Now.Date && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(e => e.ExcursionTips.Count > 0 && e.ExcursionTips.FirstOrDefault().CardId == custCardId
                                                                                        && e.ExcursionTips.FirstOrDefault().CreationDate.Date == DateTime.Now.Date)
                                                                                        .Sum(eot => eot.ExcursionTips.FirstOrDefault().TipAmount);
                    return amountSpendFromCard;
                case 1:
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7) && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(c => c.RestaurantTips.Count > 0 && c.RestaurantTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.RestaurantTips.FirstOrDefault().CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.RestaurantTips.FirstOrDefault().CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7))
                                                                                        .Sum(rot => rot.RestaurantTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.SpaOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7) && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.SpaOrder.Where(c => c.SpaTips.Count > 0 && c.SpaTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.SpaTips.FirstOrDefault().CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.SpaTips.FirstOrDefault().CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7))
                                                                                        .Sum(sot => sot.SpaTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.LaundryOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7) && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.LaundryOrder.Where(c => c.LaundryTips.Count > 0 && c.LaundryTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.LaundryTips.FirstOrDefault().CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.LaundryTips.FirstOrDefault().CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7))
                                                                                        .Sum(lot => lot.LaundryTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7) && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(c => c.HousekeepingTips.Count > 0 && c.HousekeepingTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.HousekeepingTips.FirstOrDefault().CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.HousekeepingTips.FirstOrDefault().CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7))
                                                                                        .Sum(hot => hot.HousekeepingTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7) && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(c => c.ExcursionTips.Count > 0 && c.ExcursionTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.ExcursionTips.FirstOrDefault().CreationDate.Date >= DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek)
                                                                                        && c.ExcursionTips.FirstOrDefault().CreationDate.Date < DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek).AddDays(7))
                                                                                        .Sum(eot => eot.ExcursionTips.FirstOrDefault().TipAmount);
                    return amountSpendFromCard;
                case 2:
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.RestaurantOrders.Where(c => c.RestaurantTips.Count > 0 && c.RestaurantTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.RestaurantTips.FirstOrDefault().CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.RestaurantTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.SpaOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.SpaOrder.Where(c => c.SpaTips.Count > 0 && c.SpaTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.SpaTips.FirstOrDefault().CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.SpaTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.LaundryOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.LaundryOrder.Where(c => c.LaundryTips.Count > 0 && c.LaundryTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.LaundryTips.FirstOrDefault().CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.LaundryTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.HousekeepingOrder.Where(c => c.HousekeepingTips.Count > 0 && c.HousekeepingTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.HousekeepingTips.FirstOrDefault().CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.HousekeepingTips.FirstOrDefault().TipAmount);

                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(c => c.CardId == custCardId
                                                                                        && c.CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.OrderTotal);
                    amountSpendFromCard += customerDetails.ExcursionOrder.Where(c => c.ExcursionTips.Count > 0 && c.ExcursionTips.FirstOrDefault().CardId == custCardId
                                                                                        && c.ExcursionTips.FirstOrDefault().CreationDate.Date.Month == DateTime.Now.Date.Month && c.PaymentStatus)
                                                                                        .Sum(c => c.ExcursionTips.FirstOrDefault().TipAmount);
                    return amountSpendFromCard;

                default:
                    return amountSpendFromCard;
            }
        }

        /// <summary>
        /// Cards the payment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">
        /// Order Id Not found
        /// or
        /// Payment for this order is already done
        /// or
        /// Please select card for the payment
        /// or
        /// Invalid card id
        /// </exception>
        public string CardPayment(int orderId, string quickPayToken, string quickPayUrl, int? cardId, int customerId)
        {
            var order = _restaurantOrderRepository.GetById(orderId);
            if (order == null)
                throw new ArgumentException("Order Id Not found");
            if (order.PaymentStatus)
                throw new ArgumentException("Payment for this order is already done");

            var custCardId = 0;
            if (cardId == null || cardId <= 0)
            {
                if (order.CardId != null)
                    custCardId = Convert.ToInt32(order.CardId);
                else
                    throw new ArgumentException("Please select card for the payment");
            }
            else
                custCardId = Convert.ToInt32(cardId);

            var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerId && c.IsActive).FirstOrDefault();
            if (card == null)
                throw new ArgumentException("Invalid card id");

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                //get the card token for payment
                var cardToken = client.UploadValues(string.Format("cards/{0}/tokens", card.QuickPayCardId), "post", new System.Collections.Specialized.NameValueCollection());
                string cardTokenResponseStr = System.Text.Encoding.UTF8.GetString(cardToken);
                dynamic cardTokenResponseJSON = JsonConvert.DeserializeObject(cardTokenResponseStr);

                //Generate transaction id for order
                var response = client.UploadValues("/payments", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"currency","dkk"},
                        {"order_id", DateTime.Now.Ticks.ToString() } // todo: order.Id
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                //Actual payment carried out using card token and transaction Id
                string transactionId = responseJSON.id;
                string token = cardTokenResponseJSON.token;
                var OrderAmount = order.OrderTotal * 100;
                var linkResponse = client.UploadValues(string.Format("payments/{0}/authorize", transactionId), "post", new System.Collections.Specialized.NameValueCollection()
                {
                    {"amount",OrderAmount.ToString()},
                    //{"auto_capture","true"}, //To capture the payment there it self
                    {"card[token]",token}

                });

                string paymentResponseStr = System.Text.Encoding.UTF8.GetString(linkResponse);
                dynamic PaymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseStr);
                System.Threading.Thread.Sleep(5000);
                //Get status of payment
                var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(transactionId)));
                string payResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                dynamic payResponseJSON = JsonConvert.DeserializeObject(payResponseString);

                var acceptedStatus = payResponseJSON.accepted;

                if (acceptedStatus == true)
                {
                    order.PaymentStatus = true;
                    order.CardId = custCardId;
                    order.PaymentId = transactionId;
                    _restaurantOrderRepository.Update(order);
                    return "Approved";
                }
                else
                    return "Decline";
            }
        }

        /// <summary>
        /// Captures the card payment.
        /// </summary>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="QuickPayPaymentId">The quick pay payment identifier.</param>
        /// <param name="OrderAmount">The order amount.</param>
        /// <returns>Boolean.</returns>
        public Boolean CaptureCardPayment(string quickPayToken, string quickPayUrl, string QuickPayPaymentId, double? OrderAmount)
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                OrderAmount = OrderAmount * 100;
                var linkResponse = client.UploadValues(string.Format("payments/{0}/capture", QuickPayPaymentId), "post", new System.Collections.Specialized.NameValueCollection()
                {
                    {"amount",OrderAmount.ToString()}
                });

                string paymentResponseStr = System.Text.Encoding.UTF8.GetString(linkResponse);
                dynamic PaymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseStr);

                var acceptedStatus = PaymentResponseJSON.accepted;

                if (acceptedStatus == true)
                {
                    var operationAuthorized = PaymentResponseJSON.operations[0];
                    var operationsCaptured = PaymentResponseJSON.operations[1];
                    if ((operationAuthorized.qp_status_code == "20000" && operationAuthorized.aq_status_code == "20000") &&
                        (operationsCaptured.qp_status_code == null && operationsCaptured.aq_status_code == null))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// Cancels the order and release amount.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <returns>System.String.</returns>
        public string CancelOrderAndReleaseAmount(RestaurantOrder restOrder, string quickPayToken, string quickPayUrl)
        {
            //var order = _restaurantOrderRepository.GetById(orderId);
            //if (order == null)
            //    throw new ArgumentException("Order Id Not found");
            //if (order.OrderStatus != 0)
            //    throw new ArgumentException("This order cannot be cancel, since it is already processing");

            if (restOrder.PaymentStatus)
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Accept-Version", "v10");
                    client.Headers.Add("Authorization", quickPayToken);//Claus Account

                    client.BaseAddress = quickPayUrl;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                    var linkResponse = client.UploadValues(string.Format("payments/{0}/cancel", restOrder.PaymentId), "post", new System.Collections.Specialized.NameValueCollection());

                    //check payment status
                    var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(restOrder.PaymentId)));
                    string paymentResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                    dynamic paymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseString);

                    //for handling multiple operations condition 
                    //var operations = paymentResponseJSON.operations;
                    //foreach(var op in operations)
                    //{
                    //    var t = op.type;
                    //}
                    var operationAuthorized = paymentResponseJSON.operations[0];
                    var operationsCancel = paymentResponseJSON.operations[1];
                    if ((operationAuthorized.qp_status_code == "20000" && operationAuthorized.aq_status_code == "20000") &&
                        (operationsCancel.qp_status_code == "20000" && operationsCancel.aq_status_code == "20000"))
                    {
                        restOrder.OrderStatus = 6;
                        _restaurantOrderRepository.Update(restOrder);
                        return "Succeeded";
                    }
                    else
                        return "Failed";
                }
            }
            else
            {
                restOrder.OrderStatus = 6;
                _restaurantOrderRepository.Update(restOrder);
                return "Succeeded";
            }
        }

        /// <summary>
        /// Tipses the payment using card.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="tipsAmount">The tips amount.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">
        /// Order Id Not found
        /// or
        /// Please select card for the payment
        /// or
        /// Invalid card id
        /// </exception>
        public string TipsPaymentUsingCard(int orderId, string quickPayToken, string quickPayUrl, double tipsAmount, string comment, int? cardId, int customerId)
        {
            var order = _restaurantOrderRepository.GetById(orderId);
            if (order == null)
                throw new ArgumentException("Order Id Not found");

            var custCardId = 0;
            if (cardId == null || cardId <= 0)
            {
                if (order.CardId != null)
                    custCardId = Convert.ToInt32(order.CardId);
                else
                    throw new ArgumentException("Please select card for the payment");
            }
            else
                custCardId = Convert.ToInt32(cardId);

            var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerId && c.IsActive).FirstOrDefault();
            if (card == null)
                throw new ArgumentException("Invalid card id");

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                //get the card token for payment
                var cardToken = client.UploadValues(string.Format("cards/{0}/tokens", card.QuickPayCardId), "post", new System.Collections.Specialized.NameValueCollection());
                string cardTokenResponseStr = System.Text.Encoding.UTF8.GetString(cardToken);
                dynamic cardTokenResponseJSON = JsonConvert.DeserializeObject(cardTokenResponseStr);

                //Generate transaction id for order
                var response = client.UploadValues("/payments", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"currency","dkk"},
                        {"order_id", DateTime.Now.Ticks.ToString() } // todo: order.Id
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                //Actual payment carried out using card token and transaction Id
                string transactionId = responseJSON.id;
                string token = cardTokenResponseJSON.token;
                var orderTipAmount = tipsAmount * 100;
                var linkResponse = client.UploadValues(string.Format("payments/{0}/authorize", transactionId), "post", new System.Collections.Specialized.NameValueCollection()
                {
                    {"amount",orderTipAmount.ToString()},
                    {"auto_capture","true"}, //To capture the payment there it self
                    {"card[token]",token}

                });

                string paymentResponseStr = System.Text.Encoding.UTF8.GetString(linkResponse);
                dynamic PaymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseStr);
                System.Threading.Thread.Sleep(5000);
                //Get status of payment
                var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(transactionId)));
                string payResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                dynamic payResponseJSON = JsonConvert.DeserializeObject(payResponseString);

                var acceptedStatus = payResponseJSON.accepted;

                if (acceptedStatus == true)
                {
                    RestaurantTip restTips = new RestaurantTip()
                    {
                        RestaurantOrderId = orderId,
                        CardId = custCardId,
                        TipAmount = tipsAmount,
                        PaymentStatus = true,
                        Comment = comment,
                        CreationDate = DateTime.Now
                    };
                    _restaurantTipsOperations.AddNew(restTips);
                    return "Approved";
                }
                else
                    return "Decline";
            }
        }



        #region Hotel Services Payment

        /// <summary>
        /// Hotels the service order card payment.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">
        /// Invalid card id
        /// </exception>
        public string HotelServiceOrderCardPayment(int orderId, string currency, string quickPayToken, string quickPayUrl, int? cardId, int customerId, HotelServiceType hotelServiceType = HotelServiceType.Spa)
        {
            var custCardId = 0;
            var hotelServiceOrderId = "";
            var hotelServiceOrderAmount = "";
            SpaOrder spaOrder = null;
            LaundryOrder laundryOrder = null;
            HousekeepingOrder housekeepingOrder = null;
            ExcursionOrder excursionOrder = null;

            switch (hotelServiceType)
            {
                case HotelServiceType.Spa:
                    spaOrder = _spaOrderRepository.GetById(orderId);
                    if (spaOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                    if (spaOrder.PaymentStatus)
                        throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                    if (cardId == null || cardId <= 0)
                        custCardId = spaOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);

                    hotelServiceOrderId = "SO_" + spaOrder.Id.ToString();
                    hotelServiceOrderAmount = (spaOrder.OrderTotal * 100).ToString();
                    break;

                case HotelServiceType.Laundry:
                    laundryOrder = _laundryOrderRepository.GetById(orderId);
                    if (laundryOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                    if (laundryOrder.PaymentStatus)
                        throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                    if (cardId == null || cardId <= 0)
                        custCardId = laundryOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);

                    hotelServiceOrderId = "LO_" + laundryOrder.Room.HotelId.ToString() + "_" + laundryOrder.Id.ToString();
                    hotelServiceOrderAmount = (laundryOrder.OrderTotal * 100).ToString();
                    break;

                case HotelServiceType.Housekeeping:
                    housekeepingOrder = _housekeepingOrderRepository.GetById(orderId);
                    if (housekeepingOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                    if (housekeepingOrder.PaymentStatus)
                        throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                    if (cardId == null || cardId <= 0)
                        custCardId = housekeepingOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);

                    hotelServiceOrderId = "HO_" + housekeepingOrder.Room.HotelId.ToString() + "_" + housekeepingOrder.Id.ToString();
                    hotelServiceOrderAmount = (housekeepingOrder.OrderTotal * 100).ToString();
                    break;

                case HotelServiceType.Excursion:
                    excursionOrder = _excursionOrderRepository.GetById(orderId);
                    if (excursionOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);
                    if (excursionOrder.PaymentStatus)
                        throw new ArgumentException(Resources.HotelServicesPayment_AlreadyDone);

                    if (cardId == null || cardId <= 0)
                        custCardId = excursionOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);

                    hotelServiceOrderId = "EO_" + excursionOrder.HotelId.ToString() + "_" + excursionOrder.Id.ToString();
                    hotelServiceOrderAmount = (excursionOrder.OrderTotal * 100).ToString();
                    break;
            }
            var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerId && c.IsActive).FirstOrDefault();
            if (card == null)
                throw new ArgumentException("Invalid card id");

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                //get the card token for payment
                var cardToken = client.UploadValues(string.Format("cards/{0}/tokens", card.QuickPayCardId), "post", new System.Collections.Specialized.NameValueCollection());
                string cardTokenResponseStr = System.Text.Encoding.UTF8.GetString(cardToken);
                dynamic cardTokenResponseJSON = JsonConvert.DeserializeObject(cardTokenResponseStr);

                //Generate transaction id for order
                var order_currency = (currency == null) ? "dkk" : currency;
                var response = client.UploadValues("/payments", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"currency", order_currency},
                        {"order_id", hotelServiceOrderId } //{"order_id", DateTime.Now.Ticks.ToString() }
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                //Actual payment carried out using card token and transaction Id
                string transactionId = responseJSON.id;
                string token = cardTokenResponseJSON.token;
                var linkResponse = client.UploadValues(string.Format("payments/{0}/authorize", transactionId), "post", new System.Collections.Specialized.NameValueCollection()
                {
                    {"amount",hotelServiceOrderAmount},
                    //{"auto_capture","true"},
                    {"card[token]",token}

                });

                System.Threading.Thread.Sleep(5000);
                //Get status of payment
                var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(transactionId)));
                string payResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                dynamic payResponseJSON = JsonConvert.DeserializeObject(payResponseString);

                var acceptedStatus = payResponseJSON.accepted;

                if (acceptedStatus == true)
                {
                    switch (hotelServiceType)
                    {
                        case HotelServiceType.Spa:
                            spaOrder.PaymentStatus = true;
                            spaOrder.CardId = custCardId;
                            spaOrder.QuickPayPaymentId = transactionId;
                            _spaOrderRepository.Update(spaOrder);
                            break;
                        case HotelServiceType.Laundry:
                            laundryOrder.PaymentStatus = true;
                            laundryOrder.CardId = custCardId;
                            laundryOrder.QuickPayPaymentId = transactionId;
                            _laundryOrderRepository.Update(laundryOrder);
                            break;
                        case HotelServiceType.Housekeeping:
                            housekeepingOrder.PaymentStatus = true;
                            housekeepingOrder.CardId = custCardId;
                            housekeepingOrder.PaymentId = Convert.ToInt32(transactionId);
                            _housekeepingOrderRepository.Update(housekeepingOrder);
                            break;
                        case HotelServiceType.Excursion:
                            excursionOrder.PaymentStatus = true;
                            excursionOrder.CardId = custCardId;
                            excursionOrder.QuickPayPaymentId = transactionId;
                            _excursionOrderRepository.Update(excursionOrder);
                            break;
                    }

                    return "Approved";
                }
                else
                    return "Decline";
            }
        }

        /// <summary>
        /// Cancels the hotel service order and release amount.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="NotFoundException">
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        public string CancelHotelServiceOrderAndReleaseAmount(int orderId, int customerId, string quickPayToken, string quickPayUrl, HotelServiceType hotelServiceType)
        {
            SpaOrder spaOrder = null;
            LaundryOrder laundryOrder = null;
            HousekeepingOrder housekeepingOrder = null;
            ExcursionOrder excursionOrder = null;

            bool payStatus = false;

            var QuickPayPaymentId = "";
            switch (hotelServiceType)
            {
                case HotelServiceType.Spa:
                    spaOrder = _spaOrderRepository.GetById(orderId);
                    if (spaOrder == null)
                        throw new NotFoundException(Resources.SpaOrder_NotFound);
                    if (spaOrder.OrderStatus == 3)
                        throw new ArgumentException(Resources.Order_AlreadyCanceled);
                    if (spaOrder.OrderStatus != 0)
                        throw new ArgumentException(Resources.Order_CannotBeCancel);
                    if (spaOrder.CustomerId != customerId)
                        throw new ArgumentException(Resources.Not_Authorized);

                    QuickPayPaymentId = spaOrder.QuickPayPaymentId;
                    payStatus = spaOrder.PaymentStatus;
                    break;

                case HotelServiceType.Laundry:
                    laundryOrder = _laundryOrderRepository.GetById(orderId);
                    if (laundryOrder == null)
                        throw new NotFoundException(Resources.HotelServicesOrder_NotFound);
                    if (laundryOrder.OrderStatus == 3)
                        throw new ArgumentException(Resources.Order_AlreadyCanceled);
                    if (laundryOrder.OrderStatus != 0)
                        throw new ArgumentException(Resources.Order_CannotBeCancel);
                    if (laundryOrder.CustomerId != customerId)
                        throw new ArgumentException(Resources.Not_Authorized);

                    QuickPayPaymentId = laundryOrder.QuickPayPaymentId;
                    payStatus = laundryOrder.PaymentStatus;
                    break;

                case HotelServiceType.Housekeeping:
                    housekeepingOrder = _housekeepingOrderRepository.GetById(orderId);
                    if (housekeepingOrder == null)
                        throw new NotFoundException(Resources.HotelServicesOrder_NotFound);
                    if (housekeepingOrder.OrderStatus == 3)
                        throw new ArgumentException(Resources.Order_AlreadyCanceled);
                    if (housekeepingOrder.OrderStatus != 0)
                        throw new ArgumentException(Resources.Order_CannotBeCancel);
                    if (housekeepingOrder.CustomerId != customerId)
                        throw new ArgumentException(Resources.Not_Authorized);

                    QuickPayPaymentId = housekeepingOrder.PaymentId.ToString();
                    payStatus = housekeepingOrder.PaymentStatus;
                    break;
                case HotelServiceType.Excursion:
                    excursionOrder = _excursionOrderRepository.GetById(orderId);
                    if (excursionOrder == null)
                        throw new NotFoundException(Resources.HotelServicesOrder_NotFound);
                    if (excursionOrder.OrderStatus == 3)
                        throw new ArgumentException(Resources.Order_AlreadyCanceled);
                    if (excursionOrder.OrderStatus != 0)
                        throw new ArgumentException(Resources.Order_CannotBeCancel);
                    if (excursionOrder.CustomerId != customerId)
                        throw new ArgumentException(Resources.Not_Authorized);

                    QuickPayPaymentId = excursionOrder.QuickPayPaymentId;
                    payStatus = excursionOrder.PaymentStatus;
                    break;
            }
            if (payStatus == true)
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Accept-Version", "v10");
                    client.Headers.Add("Authorization", quickPayToken);//Claus Account

                    client.BaseAddress = quickPayUrl;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                    var linkResponse = client.UploadValues(string.Format("payments/{0}/cancel", QuickPayPaymentId), "post", new System.Collections.Specialized.NameValueCollection());

                    //check payment status
                    var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(QuickPayPaymentId)));
                    string paymentResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                    dynamic paymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseString);

                    var operationAuthorized = paymentResponseJSON.operations[0];
                    var operationsCancel = paymentResponseJSON.operations[1];
                    if ((operationAuthorized.qp_status_code == "20000" && operationAuthorized.aq_status_code == "20000") &&
                        (operationsCancel.qp_status_code == "20000" && operationsCancel.aq_status_code == "20000"))
                    {
                        switch (hotelServiceType)
                        {
                            case HotelServiceType.Spa:
                                spaOrder.OrderStatus = 3;
                                _spaOrderRepository.Update(spaOrder);
                                break;
                            case HotelServiceType.Laundry:
                                laundryOrder.OrderStatus = 3;
                                _laundryOrderRepository.Update(laundryOrder);
                                break;
                            case HotelServiceType.Housekeeping:
                                housekeepingOrder.OrderStatus = 3;
                                _housekeepingOrderRepository.Update(housekeepingOrder);
                                break;
                            case HotelServiceType.Excursion:
                                excursionOrder.OrderStatus = 3;
                                _excursionOrderRepository.Update(excursionOrder);
                                break;
                        }
                        return "Succeeded";
                    }
                    else
                        return "Failed";
                }
            }
            else
            {
                switch (hotelServiceType)
                {
                    case HotelServiceType.Spa:
                        spaOrder.OrderStatus = 3;
                        _spaOrderRepository.Update(spaOrder);
                        break;
                    case HotelServiceType.Laundry:
                        laundryOrder.OrderStatus = 3;
                        _laundryOrderRepository.Update(laundryOrder);
                        break;
                    case HotelServiceType.Housekeeping:
                        housekeepingOrder.OrderStatus = 3;
                        _housekeepingOrderRepository.Update(housekeepingOrder);
                        break;
                    case HotelServiceType.Excursion:
                        excursionOrder.OrderStatus = 3;
                        _excursionOrderRepository.Update(excursionOrder);
                        break;
                }
                return "Succeeded";
            }
        }

        /// <summary>
        /// Hotels the service tips payment using card.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="tipsAmount">The tips amount.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelServiceType">Type of the hotel service.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">
        /// HotelService Type not valid
        /// or
        /// Invalid card id
        /// </exception>
        public string HotelServiceTipsPaymentUsingCard(int orderId, string quickPayToken, string quickPayUrl, double tipsAmount, string comment, int? cardId, int customerId, HotelServiceType hotelServiceType)
        {
            var custCardId = 0;
            SpaOrder spaOrder = null;
            LaundryOrder laundryOrder = null;
            HousekeepingOrder housekeepingOrder = null;
            ExcursionOrder excursionOrder = null;

            switch (hotelServiceType)
            {
                case HotelServiceType.Spa:
                    spaOrder = _spaOrderRepository.GetById(orderId);
                    if (spaOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);

                    if (spaOrder.SpaTips.Any(t => t.PaymentStatus == true))
                        throw new ArgumentException(Resources.Tips_AlreadyPaid);

                    if (cardId == null || cardId <= 0)
                        custCardId = spaOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);
                    break;

                case HotelServiceType.Laundry:
                    laundryOrder = _laundryOrderRepository.GetById(orderId);
                    if (laundryOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);

                    if (laundryOrder.LaundryTips.Any(t => t.PaymentStatus == true))
                        throw new ArgumentException(Resources.Tips_AlreadyPaid);

                    if (cardId == null || cardId <= 0)
                        custCardId = laundryOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);
                    break;

                case HotelServiceType.Housekeeping:
                    housekeepingOrder = _housekeepingOrderRepository.GetById(orderId);
                    if (housekeepingOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);

                    if (housekeepingOrder.HousekeepingTips.Any(t => t.PaymentStatus == true))
                        throw new ArgumentException(Resources.Tips_AlreadyPaid);

                    if (cardId == null || cardId <= 0)
                        custCardId = housekeepingOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);
                    break;

                case HotelServiceType.Excursion:
                    excursionOrder = _excursionOrderRepository.GetById(orderId);
                    if (excursionOrder == null)
                        throw new ArgumentException(Resources.HotelServicesOrder_NotFound);

                    if (excursionOrder.ExcursionTips.Any(t => t.PaymentStatus == true))
                        throw new ArgumentException(Resources.Tips_AlreadyPaid);

                    if (cardId == null || cardId <= 0)
                        custCardId = excursionOrder.CardId;
                    else
                        custCardId = Convert.ToInt32(cardId);
                    break;

                default:
                    throw new ArgumentException("HotelService Type not valid");
            }

            var card = _creditCardRepository.Table.Where(c => c.Id == custCardId && c.CustomerId == customerId && c.IsActive).FirstOrDefault();
            if (card == null)
                throw new ArgumentException("Invalid card id");

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                //get the card token for payment
                var cardToken = client.UploadValues(string.Format("cards/{0}/tokens", card.QuickPayCardId), "post", new System.Collections.Specialized.NameValueCollection());
                string cardTokenResponseStr = System.Text.Encoding.UTF8.GetString(cardToken);
                dynamic cardTokenResponseJSON = JsonConvert.DeserializeObject(cardTokenResponseStr);

                //Generate transaction id for order
                var response = client.UploadValues("/payments", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"currency","dkk"},
                        {"order_id", DateTime.Now.Ticks.ToString() } // todo: order.Id
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                //Actual payment carried out using card token and transaction Id
                string transactionId = responseJSON.id;
                string token = cardTokenResponseJSON.token;
                var orderTipAmount = tipsAmount * 100;
                var linkResponse = client.UploadValues(string.Format("payments/{0}/authorize", transactionId), "post", new System.Collections.Specialized.NameValueCollection()
                {
                    {"amount",orderTipAmount.ToString()},
                    {"auto_capture","true"}, //To capture the payment there it self
                    {"card[token]",token}

                });

                string paymentResponseStr = System.Text.Encoding.UTF8.GetString(linkResponse);
                dynamic PaymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseStr);
                System.Threading.Thread.Sleep(5000);
                //Get status of payment
                var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(transactionId)));
                string payResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                dynamic payResponseJSON = JsonConvert.DeserializeObject(payResponseString);

                var acceptedStatus = payResponseJSON.accepted;

                if (acceptedStatus == true)
                {
                    switch (hotelServiceType)
                    {
                        case HotelServiceType.Spa:
                            SpaTip spaTips = new SpaTip()
                            {
                                SpaOrderId = orderId,
                                CardId = custCardId,
                                TipAmount = tipsAmount,
                                PaymentStatus = true,
                                Comment = comment,
                                CreationDate = DateTime.Now
                            };
                            _spaTipOprations.AddNew(spaTips);
                            break;

                        case HotelServiceType.Laundry:
                            LaundryTip laundryTips = new LaundryTip()
                            {
                                LaundryOrderId = orderId,
                                CardId = custCardId,
                                TipAmount = tipsAmount,
                                PaymentStatus = true,
                                Comment = comment,
                                CreationDate = DateTime.Now
                            };
                            _laundryTipOprations.AddNew(laundryTips);
                            break;
                        case HotelServiceType.Housekeeping:
                            HousekeepingTip housekeepingTips = new HousekeepingTip()
                            {
                                HousekeepingOrderId = orderId,
                                CardId = custCardId,
                                TipAmount = tipsAmount,
                                PaymentStatus = true,
                                Comment = comment,
                                CreationDate = DateTime.Now
                            };
                            _housekeepingTipOprations.AddNew(housekeepingTips);
                            break;
                        case HotelServiceType.Excursion:
                            ExcursionTips excursionTips = new ExcursionTips()
                            {
                                ExcursionOrderId = orderId,
                                CardId = custCardId,
                                TipAmount = tipsAmount,
                                PaymentStatus = true,
                                Comment = comment,
                                CreationDate = DateTime.Now
                            };
                            _excursionTipsOperations.AddNew(excursionTips);
                            break;
                    }
                    return "Approved";

                }
                else
                    return "Decline";
            }
        }

        /// <summary>
        /// Rejects the hotel service order and release amount.
        /// </summary>
        /// <param name="quickPayToken">The quick pay token.</param>
        /// <param name="quickPayUrl">The quick pay URL.</param>
        /// <param name="QuickPayPaymentId">The quick pay payment identifier.</param>
        /// <returns>Boolean.</returns>
        public Boolean RejectHotelServiceOrderAndReleaseAmount(string quickPayToken, string quickPayUrl, string QuickPayPaymentId)
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept-Version", "v10");
                client.Headers.Add("Authorization", quickPayToken);//Claus Account

                client.BaseAddress = quickPayUrl;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                var linkResponse = client.UploadValues(string.Format("payments/{0}/cancel", QuickPayPaymentId), "post", new System.Collections.Specialized.NameValueCollection());

                //check payment status
                var paymentResponse = client.DownloadData(string.Format("payments/" + Convert.ToInt32(QuickPayPaymentId)));
                string paymentResponseString = System.Text.Encoding.UTF8.GetString(paymentResponse);
                dynamic paymentResponseJSON = JsonConvert.DeserializeObject(paymentResponseString);

                var operationAuthorized = paymentResponseJSON.operations[0];
                var operationsCancel = paymentResponseJSON.operations[1];
                if ((operationAuthorized.qp_status_code == "20000" && operationAuthorized.aq_status_code == "20000") &&
                    (operationsCancel.qp_status_code == "20000" && operationsCancel.aq_status_code == "20000"))
                {
                    return true;
                }
                else
                    return false;
            }
        }
        #endregion Hotel Services Payment

        //public bool PaypalPayment(int customerId, string email, int orderId, int cardId)
        //{
        //    var order = _restaurantOrderRepository.GetById(orderId);
        //    if (order == null)
        //        throw new ArgumentException("Order Id Not found");

        //    if (cardId == null || cardId <= 0)
        //        throw new ArgumentException("Invalid card id");

        //    var card = _creditCardRepository.Table.Where(c => c.Id == cardId && c.CustomerId == customerId).FirstOrDefault();

        //    double additional = order.RestaurantOrderItems.Sum(a => a.RestaurantOrderItemAdditionals.Sum(p => p.MenuAdditionalElement.AdditionalCost)).Value;
        //    double totalOrderItem = order.RestaurantOrderItems.Sum(o => o.RestaurantMenuItem.Price * o.Qty).Value;

        //    double TotalAmount = additional + totalOrderItem;

        //    var apiContext = PaypalConfiguration.GetAPIContext();
        //    var transaction = new Transaction()
        //    {
        //        amount = new Amount()
        //        {
        //            currency = card.Currency,
        //            total = TotalAmount.ToString()
        //        },
        //        invoice_number = Common.GetRandomInvoiceNumber()
        //    };

        //    // A resource representing a Payer that funds a payment.
        //    var payer = new PayPal.Api.Payer()
        //    {
        //        payment_method = "credit_card",
        //        funding_instruments = new List<FundingInstrument>()
        //        {
        //            new FundingInstrument()
        //            {
        //                credit_card = new CreditCard()
        //                {
        //                    cvv2 = card.CVVNumber,
        //                    expire_month =card.CardExpiryDate.Month,
        //                    expire_year = card.CardExpiryDate.Year,
        //                    first_name = card.CardHolderName,
        //                    number = card.CardNumber,
        //                    type= "visa"
        //                }
        //            }
        //        },
        //        payer_info = new PayerInfo
        //        {
        //            email = email
        //        }
        //    };

        //    var payment = new PayPal.Api.Payment()
        //    {
        //        intent = "sale",
        //        payer = payer,
        //        transactions = new List<Transaction>() { transaction }
        //    };

        //    // Create a payment using a valid APIContext
        //    var createdPayment = payment.Create(apiContext);
        //    return true;
        //}


        /// <summary>
        /// Class Common.
        /// </summary>
        public static class Common
        {
            /// <summary>
            /// Gets a random invoice number to be used with a sample request that requires an invoice number.
            /// </summary>
            /// <returns>A random invoice number in the range of 0 to 999999</returns>
            public static string GetRandomInvoiceNumber()
            {
                return new Random().Next(999999).ToString();
            }
        }


        /// <summary>
        /// Paypals the token.
        /// </summary>
        /// <returns>System.Object.</returns>
        public object PaypalToken()
        {
            var apiContext = PaypalConfiguration.GetAPIContext();
            return apiContext;
        }

        /// <summary>
        /// Generates the full paypal token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <returns>System.Object.</returns>
        public object GenerateFullPaypalToken(string token, string baseAddress)
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Authorization", "Basic " + token);

                client.BaseAddress = baseAddress;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                var response = client.UploadValues("/v1/oauth2/token", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"grant_type","client_credentials"}
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                return responseJSON;
            };
        }

        /// <summary>
        /// Generates the paypal token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <returns>System.Object.</returns>
        public object GeneratePaypalToken(string token, string baseAddress)
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Authorization", "Basic " + token);

                client.BaseAddress = baseAddress;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                var response = client.UploadValues("/v1/oauth2/token", new System.Collections.Specialized.NameValueCollection()
                    {
                        {"grant_type","client_credentials"}
                    });

                string responseString = System.Text.Encoding.UTF8.GetString(response);
                dynamic responseJSON = JsonConvert.DeserializeObject(responseString);

                return responseJSON.access_token;
            };
        }

        /// <summary>
        /// Payments the excecute.
        /// </summary>
        /// <param name="paymentID">The payment identifier.</param>
        /// <param name="payerID">The payer identifier.</param>
        /// <returns>System.Object.</returns>
        /// <exception cref="System.Exception"></exception>
        public object PaymentExcecute(string paymentID, string payerID)
        {
            try
            {
                var apiContext = PaypalConfiguration.GetAPIContext();
                var paymentExecution = new PaymentExecution() { payer_id = payerID };
                var payment = new Payment() { id = paymentID };

                var executedPayment = payment.Execute(apiContext, paymentExecution);
                return executedPayment;
            }
            catch (HttpException hp)
            {
                var messages = "";
                throw new Exception(messages = hp.Response);
            }
        }

        /// <summary>
        /// Captures the paypal payment.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="baseAddress">The base address.</param>
        /// <param name="token">The token.</param>
        /// <returns>System.Object.</returns>
        public object CapturePaypalPayment(RestaurantOrder restOrder, string baseAddress, string token)
        {
            dynamic responseKey = GeneratePaypalToken(token, baseAddress);
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/json");
                client.Headers.Add("Authorization", "Bearer " + responseKey);//manoj Account

                client.BaseAddress = baseAddress;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                var response = client.DownloadString("/v1/payments/payment/" + restOrder.PaymentId);
                dynamic result = JsonConvert.DeserializeObject(response);
                var state = result["state"];

                //want to Capture payment

                if (state == "approved")
                {
                    var authorizationId = result["transactions"][0]["related_resources"][0]["authorization"]["id"];
                    var authorizationState = result["transactions"][0]["related_resources"][0]["authorization"]["state"];
                    if (authorizationState == "authorized")
                    {
                        var orderAmount = result["transactions"][0]["related_resources"][0]["authorization"]["amount"]["total"];
                        var currency = result["transactions"][0]["related_resources"][0]["authorization"]["amount"]["currency"];

                        var amounts = new { total = orderAmount.ToString(), currency = currency };
                        var c = new { amount = amounts, is_final_capture = "true" };
                        var dataString = JsonConvert.SerializeObject(c);

                        using (WebClient client1 = new WebClient())
                        {
                            client1.Headers.Add("Content-Type", "application/json");
                            client1.Headers.Add("Authorization", "Bearer " + responseKey);//manoj Account

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                            response = client1.UploadString(new Uri(baseAddress + "/v1/payments/authorization/" + authorizationId + "/capture"), "POST", dataString);
                            result = JsonConvert.DeserializeObject(response);
                            state = result["state"];
                        }
                    }
                    else
                    {
                        return state;
                    }
                }
                else
                {
                    return state;
                }
                return state;
            }
        }

        /// <summary>
        /// Cancels the order and release paypal amount.
        /// </summary>
        /// <param name="restOrder">The rest order.</param>
        /// <param name="paypalBaseUrl">The paypal base URL.</param>
        /// <param name="token">The token.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="NotCapturevoided"></exception>
        /// <exception cref="System.ArgumentException">paypal payment in " + state + " state so it'll not cancled</exception>
        /// <exception cref="NotFoundPaypalPaymentId"></exception>
        public string CancelOrderAndReleasePaypalAmount(RestaurantOrder restOrder, string paypalBaseUrl, string token)
        {
            dynamic responseKey = GeneratePaypalToken(token, paypalBaseUrl);
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + responseKey);//manoj Account
                    client.BaseAddress = paypalBaseUrl;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                    var response = client.DownloadString("/v1/payments/payment/" + restOrder.PaymentId);
                    dynamic result = JsonConvert.DeserializeObject(response);
                    var state = result["state"];

                    if (state == "approved")
                    {
                        var authorizationId = result["transactions"][0]["related_resources"][0]["authorization"]["id"];
                        try
                        {
                            using (WebClient client1 = new WebClient())
                            {
                                client1.Headers.Add("Content-Type", "application/json");
                                client1.Headers.Add("Authorization", "Bearer " + responseKey);//manoj Account

                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                                response = client1.UploadString(new Uri(paypalBaseUrl + "/v1/payments/authorization/" + authorizationId + "/void"), "POST");
                                result = JsonConvert.DeserializeObject(response);
                                state = result["state"];

                                if (state == "voided")
                                {
                                    restOrder.OrderStatus = 6;
                                    _restaurantOrderRepository.Update(restOrder);
                                    return "Order cancled successfully";
                                }
                            }
                        }
                        catch (WebException webEx)
                        {
                            var statusCode = ((HttpWebResponse)webEx.Response).StatusCode;
                            var body = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                            dynamic responseJSON = JsonConvert.DeserializeObject(body);
                            string message = responseJSON["message"];
                            throw new NotCapturevoided(message);
                        }
                    }
                    else
                    {
                        throw new ArgumentException("paypal payment in " + state + " state so it'll not cancled");
                    }
                    return state;
                }
                catch (WebException webEx)
                {
                    var statusCode = ((HttpWebResponse)webEx.Response).StatusCode;
                    var body = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic responseJSON = JsonConvert.DeserializeObject(body);
                    string message = responseJSON["message"];
                    throw new NotFoundPaypalPaymentId(message);
                }
            }
        }
    }
}
