﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 04-22-2019
// ***********************************************************************
// <copyright file="HotelService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using System.Data.Entity;
using ToOrdr.Localization;
using System.Configuration;
using ToOrdr.Core.Exceptions;
using System.IO;
using System.Web;
using System.Net.Http;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class HotelService.
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IHotelService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IHotelService" />
    public class HotelService : IHotelService
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly ToOrdrDbContext _context = new ToOrdrDbContext();

        #region Initialization
        /// <summary>
        /// The hotel details repository
        /// </summary>
        IRepository<Hotel> _hotelDetailsRepository;
        /// <summary>
        /// The spa service repository
        /// </summary>
        IRepository<SpaService> _spaServiceRepository;
        /// <summary>
        /// The spa service booking repository
        /// </summary>
        IRepository<SPAServiceBooking> _spaServiceBookingRepository;
        /// <summary>
        /// The spa service booking detail repository
        /// </summary>
        IRepository<SPAServiceBookingDetail> _spaServiceBookingDetailRepository;
        /// <summary>
        /// The hotel laundry repository
        /// </summary>
        IRepository<Laundry> _hotelLaundryRepository;
        /// <summary>
        /// The hotel laundry details repository
        /// </summary>
        IRepository<LaundryDetail> _hotelLaundryDetailsRepository;
        /// <summary>
        /// The laundry detail additional repository
        /// </summary>
        IRepository<LaundryDetailAdditional> _laundryDetailAdditionalRepository;
        /// <summary>
        /// The laundry additional groups repository
        /// </summary>
        IRepository<LaundryAdditionalGroup> _laundryAdditionalGroupsRepository;
        /// <summary>
        /// The laundry additional element repository
        /// </summary>
        IRepository<LaundryAdditionalElement> _laundryAdditionalElementRepository;
        /// <summary>
        /// The hotel laundry booking repository
        /// </summary>
        IRepository<LaundryBooking> _hotelLaundryBookingRepository;
        /// <summary>
        /// The hotel laundry booking detail repository
        /// </summary>
        IRepository<LaundryBookingDetail> _hotelLaundryBookingDetailRepository;
        /// <summary>
        /// The hotel trips repository
        /// </summary>
        IRepository<Trip> _hotelTripsRepository;
        /// <summary>
        /// The hotel trip details repository
        /// </summary>
        IRepository<TripDetail> _hotelTripDetailsRepository;
        /// <summary>
        /// The hotel trip booking repository
        /// </summary>
        IRepository<TripBooking> _hotelTripBookingRepository;
        /// <summary>
        /// The hotel services repository
        /// </summary>
        IRepository<HotelServices> _hotelServicesRepository;
        /// <summary>
        /// The spa service details repository
        /// </summary>
        IRepository<SpaServiceDetail> _spaServiceDetailsRepository;
        /// <summary>
        /// The spa service detail ingredients repository
        /// </summary>
        IRepository<SpaServiceDetailIngredient> _spaServiceDetailIngredientsRepository;
        /// <summary>
        /// The spa service detail images repository
        /// </summary>
        IRepository<SpaServiceDetailImages> _spaServiceDetailImagesRepository;
        /// <summary>
        /// The hotel reviews repository
        /// </summary>
        IRepository<HotelReview> _hotelReviewsRepository;
        /// <summary>
        /// The taxi detail repository
        /// </summary>
        IRepository<TaxiDetail> _taxiDetailRepository;
        /// <summary>
        /// The taxi booking repository
        /// </summary>
        IRepository<TaxiBooking> _taxiBookingRepository;
        /// <summary>
        /// The taxi booking review repository
        /// </summary>
        IRepository<TaxiBookingReview> _taxiBookingReviewRepository;
        /// <summary>
        /// The customer role repository
        /// </summary>
        IRepository<CustomerRole> _customerRoleRepository;
        /// <summary>
        /// The hotel rooms repository
        /// </summary>
        IRepository<Room> _hotelRoomsRepository;
        /// <summary>
        /// The hotel room rate repository
        /// </summary>
        IRepository<RoomRate> _hotelRoomRateRepository;
        /// <summary>
        /// The hotel room category repository
        /// </summary>
        IRepository<RoomCategory> _hotelRoomCategoryRepository;
        /// <summary>
        /// The hotel room capacity repository
        /// </summary>
        IRepository<RoomCapacity> _hotelRoomCapacityRepository;
        /// <summary>
        /// The hotel room amenities repository
        /// </summary>
        IRepository<RoomAmenities> _hotelRoomAmenitiesRepository;
        /// <summary>
        /// The hotel room reviews repository
        /// </summary>
        IRepository<RoomReview> _hotelRoomReviewsRepository;
        /// <summary>
        /// The hotel booking repository
        /// </summary>
        IRepository<CustomerHotelBooking> _hotelBookingRepository;
        /// <summary>
        /// The customer credit card repository
        /// </summary>
        IRepository<CustomerCreditCard> _customerCreditCardRepository;
        /// <summary>
        /// The services repository
        /// </summary>
        IRepository<Entities.Services> _servicesRepository;
        /// <summary>
        /// The spa service review repository
        /// </summary>
        IRepository<SpaServiceReview> _spaServiceReviewRepository;
        /// <summary>
        /// The chain details table repository
        /// </summary>
        IRepository<ChainDetailsTable> _chainDetailsTableRepository;
        /// <summary>
        /// The chain table repository
        /// </summary>
        IRepository<ChainTable> _chainTableRepository;
        /// <summary>
        /// The trip review repository
        /// </summary>
        IRepository<TripReview> _tripReviewRepository;
        /// <summary>
        /// The laundry review repository
        /// </summary>
        IRepository<LaundryReview> _laundryReviewRepository;
        /// <summary>
        /// The taxi review repository
        /// </summary>
        IRepository<TaxiReview> _taxiReviewRepository;
        /// <summary>
        /// The spa service group repository
        /// </summary>
        IRepository<SPAServiceGroup> _spaServiceGroupRepository;
        /// <summary>
        /// The spa service details group repository
        /// </summary>
        IRepository<SPAServiceDetailsGroup> _spaServiceDetailsGroupRepository;
        /// <summary>
        /// The role repository
        /// </summary>
        IRepository<Role> _roleRepository;
        /// <summary>
        /// The customer repository
        /// </summary>
        IRepository<Customer> _customerRepository;
        /// <summary>
        /// The room type repository
        /// </summary>
        IRepository<RoomType> _roomTypeRepository;
        /// <summary>
        /// The room repository
        /// </summary>
        IRepository<Room> _roomRepository;
        /// <summary>
        /// The hotel room images repository
        /// </summary>
        IRepository<HotelRoomImages> _hotelRoomImagesRepository;
        /// <summary>
        /// The hotel restaurants repository
        /// </summary>
        IRepository<HotelRestaurant> _hotelRestaurantsRepository;
        /// <summary>
        /// The restaurant repository
        /// </summary>
        IRepository<Restaurant> _restaurantRepository;
        /// <summary>
        /// The hotel services reviews repository
        /// </summary>
        IRepository<HotelServicesReviews> _hotelServicesReviewsRepository;
        /// <summary>
        /// All hotel services reviews repository
        /// </summary>
        IRepository<VW_ServicesReviews> _allHotelServicesReviewsRepository;
        /// <summary>
        /// The spa cart repository
        /// </summary>
        IRepository<SpaCart> _spaCartRepository;
        /// <summary>
        /// The spa additional element repository
        /// </summary>
        IRepository<SpaAdditionalElement> _spaAdditionalElementRepository;
        /// <summary>
        /// The spa employee details repository
        /// </summary>
        IRepository<SpaEmployeeDetails> _spaEmployeeDetailsRepository;
        /// <summary>
        /// The spa cart items repository
        /// </summary>
        IRepository<SpaCartItem> _spaCartItemsRepository;
        /// <summary>
        /// The spa cart items additional repository
        /// </summary>
        IRepository<SpaCartItemsAdditional> _spaCartItemsAdditionalRepository;
        /// <summary>
        /// The spa cart extra procedure repository
        /// </summary>
        IRepository<SpaCartExtraProcedure> _spaCartExtraProcedureRepository;
        /// <summary>
        /// The spa additional group repository
        /// </summary>
        IRepository<SpaAdditionalGroup> _spaAdditionalGroupRepository;
        /// <summary>
        /// The spa order repository
        /// </summary>
        IRepository<SpaOrder> _spaOrderRepository;
        /// <summary>
        /// The spa order review repository
        /// </summary>
        IRepository<SpaOrderReview> _spaOrderReviewRepository;
        /// <summary>
        /// The spa order items repository
        /// </summary>
        IRepository<SpaOrderItem> _spaOrderItemsRepository;
        /// <summary>
        /// The spa employee repository
        /// </summary>
        IRepository<SpaEmployee> _spaEmployeeRepository;
        /// <summary>
        /// The spa detail additional repository
        /// </summary>
        IRepository<SpaDetailAdditional> _spaDetailAdditionalRepository;
        /// <summary>
        /// The spa employee time slot repository
        /// </summary>
        IRepository<SpaEmployeeTimeSlot> _SpaEmployeeTimeSlotRepository;
        /// <summary>
        /// The spa room repository
        /// </summary>
        IRepository<SpaRoom> _spaRoomRepository;
        /// <summary>
        /// The spa room details repository
        /// </summary>
        IRepository<SpaRoomDetails> _spaRoomDetailsRepository;
        /// <summary>
        /// The house keeping facilities repository
        /// </summary>
        IRepository<HouseKeepingFacility> _houseKeepingFacilitiesRepository;
        /// <summary>
        /// The house keeping facility details repository
        /// </summary>
        IRepository<HouseKeepingFacilityDetail> _houseKeepingFacilityDetailsRepository;
        /// <summary>
        /// The housekeeping additional groups repository
        /// </summary>
        IRepository<HousekeepingAdditionalGroup> _housekeepingAdditionalGroupsRepository;
        /// <summary>
        /// The housekeeping additional elements repository
        /// </summary>
        IRepository<HousekeepingAdditionalElement> _housekeepingAdditionalElementsRepository;
        /// <summary>
        /// The house keeping facilities additional repository
        /// </summary>
        IRepository<HouseKeepingfacilitiesAdditional> _houseKeepingfacilitiesAdditionalsRepository;
        /// <summary>
        /// The house keeping repository
        /// </summary>
        IRepository<HouseKeeping> _houseKeepingRepository;
        /// <summary>
        /// The room details repository
        /// </summary>
        IRepository<RoomDetails> _roomDetailsRepository;
        /// <summary>
        /// The taxi destination repository
        /// </summary>
        IRepository<TaxiDestination> _taxiDestinationRepository;
        /// <summary>
        /// The amenities repository
        /// </summary>
        IRepository<Amenities> _amenitiesRepository;
        /// <summary>
        /// The garments repository
        /// </summary>
        IRepository<Garments> _garmentsRepository;
        /// <summary>
        /// The laundry cart repository
        /// </summary>
        IRepository<LaundryCart> _laundryCartRepository;
        /// <summary>
        /// The laundry cart items repository
        /// </summary>
        IRepository<LaundryCartItems> _laundryCartItemsRepository;
        /// <summary>
        /// The laundry cart items additional repository
        /// </summary>
        IRepository<LaundryCartItemsAdditional> _laundryCartItemsAdditionalRepository;
        /// <summary>
        /// The laundry cart item images repository
        /// </summary>
        IRepository<LaundryCartItemImages> _laundryCartItemImagesRepository;
        /// <summary>
        /// The laundry order repository
        /// </summary>
        IRepository<LaundryOrder> _laundryOrderRepository;
        /// <summary>
        /// The laundry order item repository
        /// </summary>
        IRepository<LaundryOrderItems> _laundryOrderItemRepository;
        /// <summary>
        /// The laundry order review repository
        /// </summary>
        IRepository<LaundryOrderReview> _laundryOrderReviewRepository;
        /// <summary>
        /// The excursion repository
        /// </summary>
        IRepository<Excursion> _excursionRepository;
        /// <summary>
        /// The hotel excursion repository
        /// </summary>
        IRepository<HotelExcursion> _hotelExcursionRepository;
        /// <summary>
        /// The hotel excursion detail repository
        /// </summary>
        IRepository<ExcursionDetail> _hotelExcursionDetailRepository;
        /// <summary>
        /// The excursion detail images repository
        /// </summary>
        IRepository<ExcursionDetailImages> _excursionDetailImagesRepository;
        /// <summary>
        /// The excursion detail gallery repository
        /// </summary>
        IRepository<ExcursionDetailGallery> _excursionDetailGalleryRepository;
        /// <summary>
        /// The excursion ingredient category repository
        /// </summary>
        IRepository<ExcursionIngredientCategory> _excursionIngredientCategoryRepository;
        /// <summary>
        /// The excursion ingredient repository
        /// </summary>
        IRepository<ExcursionIngredient> _excursionIngredientRepository;
        /// <summary>
        /// The excursion review repository
        /// </summary>
        IRepository<ExcursionReview> _excursionReviewRepository;
        /// <summary>
        /// The excursion offer repository
        /// </summary>
        IRepository<ExcursionOffer> _excursionOfferRepository;
        /// <summary>
        /// The excursion detail ingredient repository
        /// </summary>
        IRepository<ExcursionDetailIngredient> _excursionDetailIngredientRepository;
        /// <summary>
        /// The excursion suggestion repository
        /// </summary>
        IRepository<ExcursionSuggestion> _excursionSuggestionRepository;
        /// <summary>
        /// The excursion cart repository
        /// </summary>
        IRepository<ExcursionCart> _excursionCartRepository;
        /// <summary>
        /// The excursion cart items repository
        /// </summary>
        IRepository<ExcursionCartItems> _excursionCartItemsRepository;
        /// <summary>
        /// The excursion order repository
        /// </summary>
        IRepository<ExcursionOrder> _excursionOrderRepository;
        /// <summary>
        /// The excursion order items repository
        /// </summary>
        IRepository<ExcursionOrderItems> _excursionOrderItemsRepository;
        /// <summary>
        /// The excursion order review repository
        /// </summary>
        IRepository<ExcursionOrderReview> _excursionOrderReviewRepository;
        /// <summary>
        /// The concierge group repository
        /// </summary>
        IRepository<ConciergeGroup> _conciergeGroupRepository;
        /// <summary>
        /// The concierge repository
        /// </summary>
        IRepository<Concierge> _conciergeRepository;
        /// <summary>
        /// The hotel near by places respository
        /// </summary>
        IRepository<HotelsNearByPlace> _hotelNearByPlacesRespository;
        /// <summary>
        /// The spa service offer repository
        /// </summary>
        IRepository<SpaServiceOffer> _spaServiceOfferRepository;
        /// <summary>
        /// The spa ingredient category repository
        /// </summary>
        IRepository<SpaIngredientCategory> _spaIngredientCategoryRepository;
        /// <summary>
        /// The spa ingredient repository
        /// </summary>
        IRepository<SpaIngredient> _spaIngredientRepository;
        /// <summary>
        /// The spa suggestion repository
        /// </summary>
        IRepository<SpaSuggestion> _spaSuggestionRepository;
        /// <summary>
        /// The extra time repository
        /// </summary>
        IRepository<ExtraTime> _extraTimeRepository;
        /// <summary>
        /// The extra procedure repository
        /// </summary>
        IRepository<ExtraProcedure> _extraProcedureRepository;
        /// <summary>
        /// The hotel house keeping information repository
        /// </summary>
        IRepository<HotelHouseKeepingInfo> _hotelHouseKeepingInfoRepository;
        /// <summary>
        /// The spa employee available time slot repository
        /// </summary>
        IRepository<SpaEmployeeAvailableTimeSlot> _spaEmployeeAvailableTimeSlotRepository;
        /// <summary>
        /// The customer hotel room repository
        /// </summary>
        IRepository<CustomerHotelRoom> _customerHotelRoomRepository;
        /// <summary>
        /// The housekeeping cart repository
        /// </summary>
        IRepository<HousekeepingCart> _housekeepingCartRepository;
        /// <summary>
        /// The housekeeping cart item repository
        /// </summary>
        IRepository<HousekeepingCartItem> _housekeepingCartItemRepository;
        /// <summary>
        /// The housekeeping cart item additional repository
        /// </summary>
        IRepository<HousekeepingCartItemAdditional> _housekeepingCartItemAdditionalRepository;
        /// <summary>
        /// The housekeeping order repository
        /// </summary>
        IRepository<HousekeepingOrder> _housekeepingOrderRepository;
        /// <summary>
        /// The housekeeping order item repository
        /// </summary>
        IRepository<HousekeepingOrderItem> _housekeepingOrderItemRepository;
        /// <summary>
        /// The wake up repository
        /// </summary>
        IRepository<WakeUp> _wakeUpRepository;
        /// <summary>
        /// All hotel services offers repository
        /// </summary>
        IRepository<AllHotelServicesOffers> _allHotelServicesOffersRepository;

        /// <summary>
        /// The hotel operations
        /// </summary>
        CRUDOperation<IRepository<Hotel>, Hotel> _hotelOperations;
        /// <summary>
        /// The spa service operations
        /// </summary>
        CRUDOperation<IRepository<SpaService>, SpaService> _spaServiceOperations;
        /// <summary>
        /// The spa service booking operations
        /// </summary>
        CRUDOperation<IRepository<SPAServiceBooking>, SPAServiceBooking> _spaServiceBookingOperations;
        /// <summary>
        /// The spa service booking detail operations
        /// </summary>
        CRUDOperation<IRepository<SPAServiceBookingDetail>, SPAServiceBookingDetail> _spaServiceBookingDetailOperations;
        /// <summary>
        /// The hotel laundry operations
        /// </summary>
        CRUDOperation<IRepository<Laundry>, Laundry> _hotelLaundryOperations;
        /// <summary>
        /// The hotel laundry details operations
        /// </summary>
        CRUDOperation<IRepository<LaundryDetail>, LaundryDetail> _hotelLaundryDetailsOperations;
        /// <summary>
        /// The laundry detail additional operations
        /// </summary>
        CRUDOperation<IRepository<LaundryDetailAdditional>, LaundryDetailAdditional> _laundryDetailAdditionalOperations;
        /// <summary>
        /// The laundry additional groups operations
        /// </summary>
        CRUDOperation<IRepository<LaundryAdditionalGroup>, LaundryAdditionalGroup> _laundryAdditionalGroupsOperations;
        /// <summary>
        /// The laundry additional element operations
        /// </summary>
        CRUDOperation<IRepository<LaundryAdditionalElement>, LaundryAdditionalElement> _laundryAdditionalElementOperations;
        /// <summary>
        /// The hotel laundry booking operations
        /// </summary>
        CRUDOperation<IRepository<LaundryBooking>, LaundryBooking> _hotelLaundryBookingOperations;
        /// <summary>
        /// The hotel laundry booking detail operations
        /// </summary>
        CRUDOperation<IRepository<LaundryBookingDetail>, LaundryBookingDetail> _hotelLaundryBookingDetailOperations;
        /// <summary>
        /// The hotel trip operations
        /// </summary>
        CRUDOperation<IRepository<Trip>, Trip> _hotelTripOperations;
        /// <summary>
        /// The hotel trip details operations
        /// </summary>
        CRUDOperation<IRepository<TripDetail>, TripDetail> _hotelTripDetailsOperations;
        /// <summary>
        /// The hotel trip booking operations
        /// </summary>
        CRUDOperation<IRepository<TripBooking>, TripBooking> _hotelTripBookingOperations;
        /// <summary>
        /// The hotel services operations
        /// </summary>
        CRUDOperation<IRepository<HotelServices>, HotelServices> _hotelServicesOperations;
        /// <summary>
        /// The spa service details operations
        /// </summary>
        CRUDOperation<IRepository<SpaServiceDetail>, SpaServiceDetail> _spaServiceDetailsOperations;
        /// <summary>
        /// The spa service detail ingredients operations
        /// </summary>
        CRUDOperation<IRepository<SpaServiceDetailIngredient>, SpaServiceDetailIngredient> _spaServiceDetailIngredientsOperations;
        /// <summary>
        /// The spa service detail images operations
        /// </summary>
        CRUDOperation<IRepository<SpaServiceDetailImages>, SpaServiceDetailImages> _spaServiceDetailImagesOperations;
        /// <summary>
        /// The hotel reviews operations
        /// </summary>
        CRUDOperation<IRepository<HotelReview>, HotelReview> _hotelReviewsOperations;
        /// <summary>
        /// The taxi detail operations
        /// </summary>
        CRUDOperation<IRepository<TaxiDetail>, TaxiDetail> _taxiDetailOperations;
        /// <summary>
        /// The taxi booking operations
        /// </summary>
        CRUDOperation<IRepository<TaxiBooking>, TaxiBooking> _taxiBookingOperations;
        /// <summary>
        /// The taxi booking review operations
        /// </summary>
        CRUDOperation<IRepository<TaxiBookingReview>, TaxiBookingReview> _taxiBookingReviewOperations;
        /// <summary>
        /// The hotel rooms operation
        /// </summary>
        CRUDOperation<IRepository<Room>, Room> _hotelRoomsOperation;
        /// <summary>
        /// The hotel room rate operations
        /// </summary>
        CRUDOperation<IRepository<RoomRate>, RoomRate> _hotelRoomRateOperations;
        /// <summary>
        /// The hotel room category operation
        /// </summary>
        CRUDOperation<IRepository<RoomCategory>, RoomCategory> _hotelRoomCategoryOperation;
        /// <summary>
        /// The hotel room capacity operations
        /// </summary>
        CRUDOperation<IRepository<RoomCapacity>, RoomCapacity> _hotelRoomCapacityOperations;
        /// <summary>
        /// The hotel room amenities operations
        /// </summary>
        CRUDOperation<IRepository<RoomAmenities>, RoomAmenities> _hotelRoomAmenitiesOperations;
        /// <summary>
        /// The hotel room reviews operation
        /// </summary>
        CRUDOperation<IRepository<RoomReview>, RoomReview> _hotelRoomReviewsOperation;
        /// <summary>
        /// The hotel booking operation
        /// </summary>
        CRUDOperation<IRepository<CustomerHotelBooking>, CustomerHotelBooking> _hotelBookingOperation;
        /// <summary>
        /// The customer credit card opration
        /// </summary>
        CRUDOperation<IRepository<CustomerCreditCard>, CustomerCreditCard> _customerCreditCardOpration;
        /// <summary>
        /// The spa service review operation
        /// </summary>
        CRUDOperation<IRepository<SpaServiceReview>, SpaServiceReview> _spaServiceReviewOperation;
        /// <summary>
        /// The customer role operations
        /// </summary>
        CRUDOperation<IRepository<CustomerRole>, CustomerRole> _customerRoleOperations;
        /// <summary>
        /// The chain table details operations
        /// </summary>
        CRUDOperation<IRepository<ChainDetailsTable>, ChainDetailsTable> _chainTableDetailsOperations;
        /// <summary>
        /// The chain table operation
        /// </summary>
        CRUDOperation<IRepository<ChainTable>, ChainTable> _chainTableOperation;
        /// <summary>
        /// The trip review operations
        /// </summary>
        CRUDOperation<IRepository<TripReview>, TripReview> _tripReviewOperations;
        /// <summary>
        /// The services operation
        /// </summary>
        CRUDOperation<IRepository<Entities.Services>, Entities.Services> _servicesOperation;
        /// <summary>
        /// The laundry review operations
        /// </summary>
        CRUDOperation<IRepository<LaundryReview>, LaundryReview> _laundryReviewOperations;
        /// <summary>
        /// The taxi review operations
        /// </summary>
        CRUDOperation<IRepository<TaxiReview>, TaxiReview> _taxiReviewOperations;
        /// <summary>
        /// The spa service group operations
        /// </summary>
        CRUDOperation<IRepository<SPAServiceGroup>, SPAServiceGroup> _spaServiceGroupOperations;
        /// <summary>
        /// The spa service details group operation
        /// </summary>
        CRUDOperation<IRepository<SPAServiceDetailsGroup>, SPAServiceDetailsGroup> _spaServiceDetailsGroupOperation;
        /// <summary>
        /// The customer operations
        /// </summary>
        CRUDOperation<IRepository<Customer>, Customer> _customerOperations;
        /// <summary>
        /// The room operations
        /// </summary>
        CRUDOperation<IRepository<Room>, Room> _roomOperations;
        /// <summary>
        /// The room type operations
        /// </summary>
        CRUDOperation<IRepository<RoomType>, RoomType> _roomTypeOperations;
        /// <summary>
        /// The hotel room images operations
        /// </summary>
        CRUDOperation<IRepository<HotelRoomImages>, HotelRoomImages> _hotelRoomImagesOperations;
        /// <summary>
        /// The hotel restaurants operations
        /// </summary>
        CRUDOperation<IRepository<HotelRestaurant>, HotelRestaurant> _hotelRestaurantsOperations;
        /// <summary>
        /// The hotel services reviews operations
        /// </summary>
        CRUDOperation<IRepository<HotelServicesReviews>, HotelServicesReviews> _hotelServicesReviewsOperations;
        /// <summary>
        /// All hotel services reviews operations
        /// </summary>
        CRUDOperation<IRepository<VW_ServicesReviews>, VW_ServicesReviews> _allHotelServicesReviewsOperations;
        /// <summary>
        /// The spa cart operations
        /// </summary>
        CRUDOperation<IRepository<SpaCart>, SpaCart> _spaCartOperations;
        /// <summary>
        /// The spa employee details operation
        /// </summary>
        CRUDOperation<IRepository<SpaEmployeeDetails>, SpaEmployeeDetails> _spaEmployeeDetailsOperation;
        /// <summary>
        /// The spa cart items operation
        /// </summary>
        CRUDOperation<IRepository<SpaCartItem>, SpaCartItem> _spaCartItemsOperation;
        /// <summary>
        /// The spa order operation
        /// </summary>
        CRUDOperation<IRepository<SpaOrder>, SpaOrder> _spaOrderOperation;
        /// <summary>
        /// The spa additional group operations
        /// </summary>
        CRUDOperation<IRepository<SpaAdditionalGroup>, SpaAdditionalGroup> _spaAdditionalGroupOperations;
        /// <summary>
        /// The spa additional element operations
        /// </summary>
        CRUDOperation<IRepository<SpaAdditionalElement>, SpaAdditionalElement> _spaAdditionalElementOperations;
        /// <summary>
        /// The spa order review operations
        /// </summary>
        CRUDOperation<IRepository<SpaOrderReview>, SpaOrderReview> _spaOrderReviewOperations;
        /// <summary>
        /// The spa order items operation
        /// </summary>
        CRUDOperation<IRepository<SpaOrderItem>, SpaOrderItem> _spaOrderItemsOperation;
        /// <summary>
        /// The spa employee operation
        /// </summary>
        CRUDOperation<IRepository<SpaEmployee>, SpaEmployee> _spaEmployeeOperation;
        /// <summary>
        /// The spa employee time slot operation
        /// </summary>
        CRUDOperation<IRepository<SpaEmployeeTimeSlot>, SpaEmployeeTimeSlot> _spaEmployeeTimeSlotOperation;
        /// <summary>
        /// The spa room operations
        /// </summary>
        CRUDOperation<IRepository<SpaRoom>, SpaRoom> _spaRoomOperations;
        /// <summary>
        /// The room details operations
        /// </summary>
        CRUDOperation<IRepository<RoomDetails>, RoomDetails> _roomDetailsOperations;
        /// <summary>
        /// The house keeping facilities operations
        /// </summary>
        CRUDOperation<IRepository<HouseKeepingFacility>, HouseKeepingFacility> _houseKeepingFacilitiesOperations;
        /// <summary>
        /// The house keeping facility details operations
        /// </summary>
        CRUDOperation<IRepository<HouseKeepingFacilityDetail>, HouseKeepingFacilityDetail> _houseKeepingFacilityDetailsOperations;
        /// <summary>
        /// The housekeeping additional groups operations
        /// </summary>
        CRUDOperation<IRepository<HousekeepingAdditionalGroup>, HousekeepingAdditionalGroup> _housekeepingAdditionalGroupsOperations;
        /// <summary>
        /// The housekeeping additional elements operations
        /// </summary>
        CRUDOperation<IRepository<HousekeepingAdditionalElement>, HousekeepingAdditionalElement> _housekeepingAdditionalElementsOperations;
        /// <summary>
        /// The house keepingfacilities additional operations
        /// </summary>
        CRUDOperation<IRepository<HouseKeepingfacilitiesAdditional>, HouseKeepingfacilitiesAdditional> _houseKeepingfacilitiesAdditionalOperations;
        /// <summary>
        /// The house keeping operations
        /// </summary>
        CRUDOperation<IRepository<HouseKeeping>, HouseKeeping> _houseKeepingOperations;
        /// <summary>
        /// The taxi destination operations
        /// </summary>
        CRUDOperation<IRepository<TaxiDestination>, TaxiDestination> _taxiDestinationOperations;
        /// <summary>
        /// The spa room details operations
        /// </summary>
        CRUDOperation<IRepository<SpaRoomDetails>, SpaRoomDetails> _spaRoomDetailsOperations;
        /// <summary>
        /// The amenities operation
        /// </summary>
        CRUDOperation<IRepository<Amenities>, Amenities> _amenitiesOperation;
        /// <summary>
        /// The garments operation
        /// </summary>
        CRUDOperation<IRepository<Garments>, Garments> _garmentsOperation;
        /// <summary>
        /// The laundry cart operations
        /// </summary>
        CRUDOperation<IRepository<LaundryCart>, LaundryCart> _laundryCartOperations;
        /// <summary>
        /// The laundry order operations
        /// </summary>
        CRUDOperation<IRepository<LaundryOrder>, LaundryOrder> _laundryOrderOperations;
        /// <summary>
        /// The laundry order item operations
        /// </summary>
        CRUDOperation<IRepository<LaundryOrderItems>, LaundryOrderItems> _laundryOrderItemOperations;
        /// <summary>
        /// The laundry order review operations
        /// </summary>
        CRUDOperation<IRepository<LaundryOrderReview>, LaundryOrderReview> _laundryOrderReviewOperations;
        /// <summary>
        /// The excursion operations
        /// </summary>
        CRUDOperation<IRepository<Excursion>, Excursion> _excursionOperations;
        /// <summary>
        /// The hotel excursion operations
        /// </summary>
        CRUDOperation<IRepository<HotelExcursion>, HotelExcursion> _hotelExcursionOperations;
        /// <summary>
        /// The hotel excursion details operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionDetail>, ExcursionDetail> _hotelExcursionDetailsOperations;
        /// <summary>
        /// The excursion detail images operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionDetailImages>, ExcursionDetailImages> _excursionDetailImagesOperations;
        /// <summary>
        /// The excursion detail gallery operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionDetailGallery>, ExcursionDetailGallery> _excursionDetailGalleryOperations;
        /// <summary>
        /// The excursion ingredient category operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionIngredientCategory>, ExcursionIngredientCategory> _excursionIngredientCategoryOperations;
        /// <summary>
        /// The excursion ingredient operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionIngredient>, ExcursionIngredient> _excursionIngredientOperations;
        /// <summary>
        /// The excursion reviews operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionReview>, ExcursionReview> _excursionReviewsOperations;
        /// <summary>
        /// The excursion offer operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionOffer>, ExcursionOffer> _excursionOfferOperations;
        /// <summary>
        /// The excursion detail ingredient operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionDetailIngredient>, ExcursionDetailIngredient> _excursionDetailIngredientOperations;
        /// <summary>
        /// The excursion suggestion operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionSuggestion>, ExcursionSuggestion> _excursionSuggestionOperations;
        /// <summary>
        /// The excursion cart operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionCart>, ExcursionCart> _excursionCartOperations;
        /// <summary>
        /// The excursion cart items opertions
        /// </summary>
        CRUDOperation<IRepository<ExcursionCartItems>, ExcursionCartItems> _excursionCartItemsOpertions;
        /// <summary>
        /// The excursion order operations
        /// </summary>
        CRUDOperation<IRepository<ExcursionOrder>, ExcursionOrder> _excursionOrderOperations;
        /// <summary>
        /// The excursion order items opersations
        /// </summary>
        CRUDOperation<IRepository<ExcursionOrderItems>, ExcursionOrderItems> _excursionOrderItemsOpersations;
        /// <summary>
        /// The excursion order review operation
        /// </summary>
        CRUDOperation<IRepository<ExcursionOrderReview>, ExcursionOrderReview> _excursionOrderReviewOperation;
        /// <summary>
        /// The spa detail additional operations
        /// </summary>
        CRUDOperation<IRepository<SpaDetailAdditional>, SpaDetailAdditional> _spaDetailAdditionalOperations;
        /// <summary>
        /// The concierge group operation
        /// </summary>
        CRUDOperation<IRepository<ConciergeGroup>, ConciergeGroup> _conciergeGroupOperation;
        /// <summary>
        /// The concierge operation
        /// </summary>
        CRUDOperation<IRepository<Concierge>, Concierge> _conciergeOperation;
        /// <summary>
        /// The hotel near by places operations
        /// </summary>
        CRUDOperation<IRepository<HotelsNearByPlace>, HotelsNearByPlace> _hotelNearByPlacesOperations;
        /// <summary>
        /// The spa service offer operations
        /// </summary>
        CRUDOperation<IRepository<SpaServiceOffer>, SpaServiceOffer> _spaServiceOfferOperations;
        /// <summary>
        /// The spa ingredient category operations
        /// </summary>
        CRUDOperation<IRepository<SpaIngredientCategory>, SpaIngredientCategory> _spaIngredientCategoryOperations;
        /// <summary>
        /// The spa ingredient operations
        /// </summary>
        CRUDOperation<IRepository<SpaIngredient>, SpaIngredient> _spaIngredientOperations;
        /// <summary>
        /// The spa suggestion operations
        /// </summary>
        CRUDOperation<IRepository<SpaSuggestion>, SpaSuggestion> _spaSuggestionOperations;
        /// <summary>
        /// The extra time operations
        /// </summary>
        CRUDOperation<IRepository<ExtraTime>, ExtraTime> _extraTimeOperations;
        /// <summary>
        /// The extra procedure operations
        /// </summary>
        CRUDOperation<IRepository<ExtraProcedure>, ExtraProcedure> _extraProcedureOperations;
        /// <summary>
        /// The hotel house keeping information operations
        /// </summary>
        CRUDOperation<IRepository<HotelHouseKeepingInfo>, HotelHouseKeepingInfo> _hotelHouseKeepingInfoOperations;
        /// <summary>
        /// The customer hotel room operation
        /// </summary>
        CRUDOperation<IRepository<CustomerHotelRoom>, CustomerHotelRoom> _customerHotelRoomOperation;
        /// <summary>
        /// The housekeeping cart operation
        /// </summary>
        CRUDOperation<IRepository<HousekeepingCart>, HousekeepingCart> _housekeepingCartOperation;
        /// <summary>
        /// The housekeeping order operation
        /// </summary>
        CRUDOperation<IRepository<HousekeepingOrder>, HousekeepingOrder> _housekeepingOrderOperation;
        /// <summary>
        /// The housekeeping order item operation
        /// </summary>
        CRUDOperation<IRepository<HousekeepingOrderItem>, HousekeepingOrderItem> _housekeepingOrderItemOperation;
        /// <summary>
        /// The wake up operation
        /// </summary>
        CRUDOperation<IRepository<WakeUp>, WakeUp> _wakeUpOperation;
        /// <summary>
        /// All hotel services offers operation
        /// </summary>
        CRUDOperation<IRepository<AllHotelServicesOffers>, AllHotelServicesOffers> _allHotelServicesOffersOperation;

        /// <summary>
        /// Initializes a new instance of the <see cref="HotelService"/> class.
        /// </summary>
        /// <param name="hotelDetailsRepository">The hotel details repository.</param>
        /// <param name="spaServiceRepository">The spa service repository.</param>
        /// <param name="spaServiceBookingRepository">The spa service booking repository.</param>
        /// <param name="spaServiceBookingDetailRepository">The spa service booking detail repository.</param>
        /// <param name="hotelLaundryRepository">The hotel laundry repository.</param>
        /// <param name="hotelLaundryDetailsRepository">The hotel laundry details repository.</param>
        /// <param name="laundryDetailAdditionalRepository">The laundry detail additional repository.</param>
        /// <param name="laundryAdditionalGroupsRepository">The laundry additional groups repository.</param>
        /// <param name="laundryAdditionalElementRepository">The laundry additional element repository.</param>
        /// <param name="hotelLaundryBookingRepository">The hotel laundry booking repository.</param>
        /// <param name="hotelLaundryBookingDetailRepository">The hotel laundry booking detail repository.</param>
        /// <param name="hotelTripsRepository">The hotel trips repository.</param>
        /// <param name="hotelTripDetailsRepository">The hotel trip details repository.</param>
        /// <param name="hotelTripBookingRepository">The hotel trip booking repository.</param>
        /// <param name="hotelServicesRepository">The hotel services repository.</param>
        /// <param name="spaServiceDetailsRepository">The spa service details repository.</param>
        /// <param name="spaServiceDetailIngredientsRepository">The spa service detail ingredients repository.</param>
        /// <param name="spaServiceDetailImagesRepository">The spa service detail images repository.</param>
        /// <param name="hotelReviewsRepository">The hotel reviews repository.</param>
        /// <param name="taxiDetailsRepository">The taxi details repository.</param>
        /// <param name="hotelRoomsRepository">The hotel rooms repository.</param>
        /// <param name="hotelRoomRateRepository">The hotel room rate repository.</param>
        /// <param name="taxiBookingRepository">The taxi booking repository.</param>
        /// <param name="taxiBookingReviewRepository">The taxi booking review repository.</param>
        /// <param name="hotelRoomReviewsRepository">The hotel room reviews repository.</param>
        /// <param name="hotelBookingRepository">The hotel booking repository.</param>
        /// <param name="customerCreditCardRepository">The customer credit card repository.</param>
        /// <param name="servicesRepository">The services repository.</param>
        /// <param name="spaServiceReviewRepository">The spa service review repository.</param>
        /// <param name="customerRoleRepository">The customer role repository.</param>
        /// <param name="chainDetailsTableRepository">The chain details table repository.</param>
        /// <param name="chainTableRepository">The chain table repository.</param>
        /// <param name="tripReviewRepository">The trip review repository.</param>
        /// <param name="laundryReviewRepository">The laundry review repository.</param>
        /// <param name="taxiReviewRepository">The taxi review repository.</param>
        /// <param name="spaServiceGroupRepository">The spa service group repository.</param>
        /// <param name="spaServiceDetailsGroupRepository">The spa service details group repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="roomTypeRepository">The room type repository.</param>
        /// <param name="roomRepository">The room repository.</param>
        /// <param name="hotelRoomCategoryRepository">The hotel room category repository.</param>
        /// <param name="hotelRoomCapacityRepository">The hotel room capacity repository.</param>
        /// <param name="hotelRoomAmenitiesRepository">The hotel room amenities repository.</param>
        /// <param name="hotelRoomImagesRepository">The hotel room images repository.</param>
        /// <param name="hotelRestaurantsRepository">The hotel restaurants repository.</param>
        /// <param name="restaurantRepository">The restaurant repository.</param>
        /// <param name="hotelServicesReviewsRepository">The hotel services reviews repository.</param>
        /// <param name="allHotelServicesReviewsRepository">All hotel services reviews repository.</param>
        /// <param name="spaCartRepository">The spa cart repository.</param>
        /// <param name="spaAdditionalElementRepository">The spa additional element repository.</param>
        /// <param name="spaEmployeeDetailsRepository">The spa employee details repository.</param>
        /// <param name="spaCartItemsRepository">The spa cart items repository.</param>
        /// <param name="spaCartItemsAdditionalRepository">The spa cart items additional repository.</param>
        /// <param name="spaCartExtraProcedureRepository">The spa cart extra procedure repository.</param>
        /// <param name="spaOrderRepository">The spa order repository.</param>
        /// <param name="spaAdditionalGroupRepository">The spa additional group repository.</param>
        /// <param name="spaOrderReviewRepository">The spa order review repository.</param>
        /// <param name="spaOrderItemsRepository">The spa order items repository.</param>
        /// <param name="spaEmployeeRepository">The spa employee repository.</param>
        /// <param name="spaDetailAdditionalRepository">The spa detail additional repository.</param>
        /// <param name="spaEmployeeTimeSlotRepository">The spa employee time slot repository.</param>
        /// <param name="spaRoomRepository">The spa room repository.</param>
        /// <param name="spaRoomDetailsRepository">The spa room details repository.</param>
        /// <param name="houseKeepingFacilitiesRepository">The house keeping facilities repository.</param>
        /// <param name="houseKeepingFacilityDetailsRepository">The house keeping facility details repository.</param>
        /// <param name="housekeepingAdditionalGroupsRepository">The housekeeping additional groups repository.</param>
        /// <param name="housekeepingAdditionalElementsRepository">The housekeeping additional elements repository.</param>
        /// <param name="houseKeepingfacilitiesAdditionalsRepository">The house keepingfacilities additionals repository.</param>
        /// <param name="houseKeepingRepository">The house keeping repository.</param>
        /// <param name="roomDetailsRepository">The room details repository.</param>
        /// <param name="taxiDestinationRepository">The taxi destination repository.</param>
        /// <param name="amenitiesRepository">The amenities repository.</param>
        /// <param name="garmentsRepository">The garments repository.</param>
        /// <param name="laundryCartRepository">The laundry cart repository.</param>
        /// <param name="laundryCartItemsRepository">The laundry cart items repository.</param>
        /// <param name="laundryCartItemsAdditionalRepository">The laundry cart items additional repository.</param>
        /// <param name="laundryCartItemImagesRepository">The laundry cart item images repository.</param>
        /// <param name="laundryOrderRepository">The laundry order repository.</param>
        /// <param name="laundryOrderItemRepository">The laundry order item repository.</param>
        /// <param name="laundryOrderReviewRepository">The laundry order review repository.</param>
        /// <param name="excursionRepository">The excursion repository.</param>
        /// <param name="hotelExcursionRepository">The hotel excursion repository.</param>
        /// <param name="hotelExcursionDetailRepository">The hotel excursion detail repository.</param>
        /// <param name="excursionDetailImagesRepository">The excursion detail images repository.</param>
        /// <param name="excursionDetailGalleryRepository">The excursion detail gallery repository.</param>
        /// <param name="excursionIngredientCategoryRepository">The excursion ingredient category repository.</param>
        /// <param name="excursionIngredientRepository">The excursion ingredient repository.</param>
        /// <param name="excursionReviewRepository">The excursion review repository.</param>
        /// <param name="excursionOfferRepository">The excursion offer repository.</param>
        /// <param name="excursionDetailIngredientRepository">The excursion detail ingredient repository.</param>
        /// <param name="excursionSuggestionRepository">The excursion suggestion repository.</param>
        /// <param name="excursionCartRepository">The excursion cart repository.</param>
        /// <param name="excursionCartItemsRepository">The excursion cart items repository.</param>
        /// <param name="excursionOrderRepository">The excursion order repository.</param>
        /// <param name="excursionOrderItemsRepository">The excursion order items repository.</param>
        /// <param name="excursionOrderReviewRepository">The excursion order review repository.</param>
        /// <param name="conciergeGroupRepository">The concierge group repository.</param>
        /// <param name="conciergeRepository">The concierge repository.</param>
        /// <param name="hotelNearByPlacesRespository">The hotel near by places respository.</param>
        /// <param name="spaServiceOfferRepository">The spa service offer repository.</param>
        /// <param name="spaSuggestionRepository">The spa suggestion repository.</param>
        /// <param name="spaIngredientCategoryRepository">The spa ingredient category repository.</param>
        /// <param name="spaIngredientRepository">The spa ingredient repository.</param>
        /// <param name="extratimeRepository">The extratime repository.</param>
        /// <param name="extraProcedureRepository">The extra procedure repository.</param>
        /// <param name="hotelHouseKeepingInfoRepository">The hotel house keeping information repository.</param>
        /// <param name="spaEmployeeAvailableTimeSlotRepository">The spa employee available time slot repository.</param>
        /// <param name="customerHotelRoomRepository">The customer hotel room repository.</param>
        /// <param name="housekeepingCartRepository">The housekeeping cart repository.</param>
        /// <param name="housekeepingCartItemRepository">The housekeeping cart item repository.</param>
        /// <param name="housekeepingCartItemAdditionalRepository">The housekeeping cart item additional repository.</param>
        /// <param name="housekeepingOrderRepository">The housekeeping order repository.</param>
        /// <param name="housekeepingOrderItemRepository">The housekeeping order item repository.</param>
        /// <param name="wakeUpRepository">The wake up repository.</param>
        /// <param name="allHotelServicesOffersRepository">All hotel services offers repository.</param>
        public HotelService(IRepository<Hotel> hotelDetailsRepository,
            IRepository<SpaService> spaServiceRepository,
            IRepository<SPAServiceBooking> spaServiceBookingRepository,
            IRepository<SPAServiceBookingDetail> spaServiceBookingDetailRepository,
            IRepository<Laundry> hotelLaundryRepository,
            IRepository<LaundryDetail> hotelLaundryDetailsRepository,
            IRepository<LaundryDetailAdditional> laundryDetailAdditionalRepository,
            IRepository<LaundryAdditionalGroup> laundryAdditionalGroupsRepository,
            IRepository<LaundryAdditionalElement> laundryAdditionalElementRepository,
            IRepository<LaundryBooking> hotelLaundryBookingRepository,
            IRepository<LaundryBookingDetail> hotelLaundryBookingDetailRepository,
            IRepository<Trip> hotelTripsRepository,
            IRepository<TripDetail> hotelTripDetailsRepository,
            IRepository<TripBooking> hotelTripBookingRepository,
            IRepository<HotelServices> hotelServicesRepository,
            IRepository<SpaServiceDetail> spaServiceDetailsRepository,
            IRepository<SpaServiceDetailIngredient> spaServiceDetailIngredientsRepository,
            IRepository<SpaServiceDetailImages> spaServiceDetailImagesRepository,
            IRepository<HotelReview> hotelReviewsRepository,
            IRepository<TaxiDetail> taxiDetailsRepository,
            IRepository<Room> hotelRoomsRepository,
            IRepository<RoomRate> hotelRoomRateRepository,
            IRepository<TaxiBooking> taxiBookingRepository,
            IRepository<TaxiBookingReview> taxiBookingReviewRepository,
            IRepository<RoomReview> hotelRoomReviewsRepository,
            IRepository<CustomerHotelBooking> hotelBookingRepository,
            IRepository<CustomerCreditCard> customerCreditCardRepository,
            IRepository<ToOrdr.Core.Entities.Services> servicesRepository,

            IRepository<SpaServiceReview> spaServiceReviewRepository,
            IRepository<CustomerRole> customerRoleRepository,
            IRepository<ChainDetailsTable> chainDetailsTableRepository,
            IRepository<ChainTable> chainTableRepository,
            IRepository<TripReview> tripReviewRepository,
            IRepository<LaundryReview> laundryReviewRepository,
            IRepository<TaxiReview> taxiReviewRepository,
            IRepository<SPAServiceGroup> spaServiceGroupRepository,
            IRepository<SPAServiceDetailsGroup> spaServiceDetailsGroupRepository,
            IRepository<Role> roleRepository,
            IRepository<Customer> customerRepository,
            IRepository<RoomType> roomTypeRepository,
            IRepository<Room> roomRepository,
            IRepository<RoomCategory> hotelRoomCategoryRepository,
            IRepository<RoomCapacity> hotelRoomCapacityRepository,
            IRepository<RoomAmenities> hotelRoomAmenitiesRepository,
            IRepository<HotelRoomImages> hotelRoomImagesRepository,
            IRepository<HotelRestaurant> hotelRestaurantsRepository,
            IRepository<Restaurant> restaurantRepository,
            IRepository<HotelServicesReviews> hotelServicesReviewsRepository,
            IRepository<VW_ServicesReviews> allHotelServicesReviewsRepository,
            IRepository<SpaCart> spaCartRepository,
            IRepository<SpaAdditionalElement> spaAdditionalElementRepository,
            IRepository<SpaEmployeeDetails> spaEmployeeDetailsRepository,
            IRepository<SpaCartItem> spaCartItemsRepository,
            IRepository<SpaCartItemsAdditional> spaCartItemsAdditionalRepository,
            IRepository<SpaCartExtraProcedure> spaCartExtraProcedureRepository,
            IRepository<SpaOrder> spaOrderRepository,
            IRepository<SpaAdditionalGroup> spaAdditionalGroupRepository,
            IRepository<SpaOrderReview> spaOrderReviewRepository,
            IRepository<SpaOrderItem> spaOrderItemsRepository,
            IRepository<SpaEmployee> spaEmployeeRepository,
            IRepository<SpaDetailAdditional> spaDetailAdditionalRepository,
            IRepository<SpaEmployeeTimeSlot> spaEmployeeTimeSlotRepository,
            IRepository<SpaRoom> spaRoomRepository,
            IRepository<SpaRoomDetails> spaRoomDetailsRepository,
            IRepository<HouseKeepingFacility> houseKeepingFacilitiesRepository,
            IRepository<HouseKeepingFacilityDetail> houseKeepingFacilityDetailsRepository,
            IRepository<HousekeepingAdditionalGroup> housekeepingAdditionalGroupsRepository,
            IRepository<HousekeepingAdditionalElement> housekeepingAdditionalElementsRepository,
            IRepository<HouseKeepingfacilitiesAdditional> houseKeepingfacilitiesAdditionalsRepository,
            IRepository<HouseKeeping> houseKeepingRepository,
            IRepository<RoomDetails> roomDetailsRepository,
            IRepository<TaxiDestination> taxiDestinationRepository,
            IRepository<Amenities> amenitiesRepository,
            IRepository<Garments> garmentsRepository,
            IRepository<LaundryCart> laundryCartRepository,
            IRepository<LaundryCartItems> laundryCartItemsRepository,
            IRepository<LaundryCartItemsAdditional> laundryCartItemsAdditionalRepository,
            IRepository<LaundryCartItemImages> laundryCartItemImagesRepository,
            IRepository<LaundryOrder> laundryOrderRepository,
            IRepository<LaundryOrderItems> laundryOrderItemRepository,
            IRepository<LaundryOrderReview> laundryOrderReviewRepository,
            IRepository<Excursion> excursionRepository,
            IRepository<HotelExcursion> hotelExcursionRepository,
            IRepository<ExcursionDetail> hotelExcursionDetailRepository,
            IRepository<ExcursionDetailImages> excursionDetailImagesRepository,
            IRepository<ExcursionDetailGallery> excursionDetailGalleryRepository,
            IRepository<ExcursionIngredientCategory> excursionIngredientCategoryRepository,
            IRepository<ExcursionIngredient> excursionIngredientRepository,
            IRepository<ExcursionReview> excursionReviewRepository,
            IRepository<ExcursionOffer> excursionOfferRepository,
            IRepository<ExcursionDetailIngredient> excursionDetailIngredientRepository,
            IRepository<ExcursionSuggestion> excursionSuggestionRepository,
            IRepository<ExcursionCart> excursionCartRepository,
            IRepository<ExcursionCartItems> excursionCartItemsRepository,
            IRepository<ExcursionOrder> excursionOrderRepository,
            IRepository<ExcursionOrderItems> excursionOrderItemsRepository,
            IRepository<ExcursionOrderReview> excursionOrderReviewRepository,
            IRepository<ConciergeGroup> conciergeGroupRepository,
            IRepository<Concierge> conciergeRepository,
            IRepository<HotelsNearByPlace> hotelNearByPlacesRespository,
            IRepository<SpaServiceOffer> spaServiceOfferRepository,
            IRepository<SpaSuggestion> spaSuggestionRepository,

            IRepository<SpaIngredientCategory> spaIngredientCategoryRepository,
            IRepository<SpaIngredient> spaIngredientRepository,
            IRepository<ExtraTime> extratimeRepository,
            IRepository<ExtraProcedure> extraProcedureRepository,
            IRepository<HotelHouseKeepingInfo> hotelHouseKeepingInfoRepository,
            IRepository<SpaEmployeeAvailableTimeSlot> spaEmployeeAvailableTimeSlotRepository,
            IRepository<CustomerHotelRoom> customerHotelRoomRepository,

            IRepository<HousekeepingCart> housekeepingCartRepository,
            IRepository<HousekeepingCartItem> housekeepingCartItemRepository,
            IRepository<HousekeepingCartItemAdditional> housekeepingCartItemAdditionalRepository,
            IRepository<HousekeepingOrder> housekeepingOrderRepository,
            IRepository<HousekeepingOrderItem> housekeepingOrderItemRepository,
            IRepository<WakeUp> wakeUpRepository,
            IRepository<AllHotelServicesOffers> allHotelServicesOffersRepository
            )
        {
            _hotelDetailsRepository = hotelDetailsRepository;
            _hotelReviewsRepository = hotelReviewsRepository;
            _spaServiceRepository = spaServiceRepository;
            _spaServiceBookingRepository = spaServiceBookingRepository;
            _spaServiceBookingDetailRepository = spaServiceBookingDetailRepository;
            _hotelLaundryRepository = hotelLaundryRepository;
            _hotelLaundryDetailsRepository = hotelLaundryDetailsRepository;
            _laundryDetailAdditionalRepository = laundryDetailAdditionalRepository;
            _laundryAdditionalGroupsRepository = laundryAdditionalGroupsRepository;
            _laundryAdditionalElementRepository = laundryAdditionalElementRepository;
            _hotelLaundryBookingRepository = hotelLaundryBookingRepository;
            _hotelLaundryBookingDetailRepository = hotelLaundryBookingDetailRepository;
            _hotelTripsRepository = hotelTripsRepository;
            _hotelTripDetailsRepository = hotelTripDetailsRepository;
            _hotelTripBookingRepository = hotelTripBookingRepository;
            _hotelServicesRepository = hotelServicesRepository;
            _spaServiceDetailsRepository = spaServiceDetailsRepository;
            _spaServiceDetailIngredientsRepository = spaServiceDetailIngredientsRepository;
            _spaServiceDetailImagesRepository = spaServiceDetailImagesRepository;
            _taxiDetailRepository = taxiDetailsRepository;
            _taxiBookingRepository = taxiBookingRepository;
            _taxiBookingReviewRepository = taxiBookingReviewRepository;
            _hotelRoomsRepository = hotelRoomsRepository;
            _hotelRoomRateRepository = hotelRoomRateRepository;
            _hotelRoomCategoryRepository = hotelRoomCategoryRepository;
            _hotelRoomCapacityRepository = hotelRoomCapacityRepository;
            _hotelRoomAmenitiesRepository = hotelRoomAmenitiesRepository;
            _hotelRoomReviewsRepository = hotelRoomReviewsRepository;
            _hotelBookingRepository = hotelBookingRepository;
            _customerCreditCardRepository = customerCreditCardRepository;
            _servicesRepository = servicesRepository;
            _spaServiceReviewRepository = spaServiceReviewRepository;
            _customerRoleRepository = customerRoleRepository;
            _chainDetailsTableRepository = chainDetailsTableRepository;
            _chainTableRepository = chainTableRepository;
            _tripReviewRepository = tripReviewRepository;
            _laundryReviewRepository = laundryReviewRepository;
            _taxiReviewRepository = taxiReviewRepository;
            _spaServiceGroupRepository = spaServiceGroupRepository;
            _spaServiceDetailsGroupRepository = spaServiceDetailsGroupRepository;
            _roleRepository = roleRepository;
            _customerRepository = customerRepository;
            _roomTypeRepository = roomTypeRepository;
            _roomRepository = roomRepository;
            _hotelRoomImagesRepository = hotelRoomImagesRepository;
            _hotelRestaurantsRepository = hotelRestaurantsRepository;
            _restaurantRepository = restaurantRepository;
            _hotelServicesReviewsRepository = hotelServicesReviewsRepository;
            _allHotelServicesReviewsRepository = allHotelServicesReviewsRepository;
            _spaCartRepository = spaCartRepository;
            _spaAdditionalElementRepository = spaAdditionalElementRepository;
            _spaCartExtraProcedureRepository = spaCartExtraProcedureRepository;
            _spaEmployeeDetailsRepository = spaEmployeeDetailsRepository;
            _spaCartItemsRepository = spaCartItemsRepository;
            _spaCartItemsAdditionalRepository = spaCartItemsAdditionalRepository;
            _spaOrderRepository = spaOrderRepository;
            _spaAdditionalGroupRepository = spaAdditionalGroupRepository;
            _spaOrderReviewRepository = spaOrderReviewRepository;
            _spaOrderItemsRepository = spaOrderItemsRepository;
            _spaEmployeeRepository = spaEmployeeRepository;
            _spaDetailAdditionalRepository = spaDetailAdditionalRepository;
            _SpaEmployeeTimeSlotRepository = spaEmployeeTimeSlotRepository;
            _spaRoomRepository = spaRoomRepository;
            _spaRoomDetailsRepository = spaRoomDetailsRepository;
            _houseKeepingFacilitiesRepository = houseKeepingFacilitiesRepository;
            _houseKeepingFacilityDetailsRepository = houseKeepingFacilityDetailsRepository;
            _housekeepingAdditionalGroupsRepository = housekeepingAdditionalGroupsRepository;
            _housekeepingAdditionalElementsRepository = housekeepingAdditionalElementsRepository;
            _houseKeepingfacilitiesAdditionalsRepository = houseKeepingfacilitiesAdditionalsRepository;
            _houseKeepingRepository = houseKeepingRepository;
            _roomDetailsRepository = roomDetailsRepository;
            _taxiDestinationRepository = taxiDestinationRepository;
            _amenitiesRepository = amenitiesRepository;
            _garmentsRepository = garmentsRepository;
            _laundryCartRepository = laundryCartRepository;
            _laundryCartItemsRepository = laundryCartItemsRepository;
            _laundryCartItemsAdditionalRepository = laundryCartItemsAdditionalRepository;
            _laundryCartItemImagesRepository = laundryCartItemImagesRepository;
            _laundryOrderRepository = laundryOrderRepository;
            _laundryOrderItemRepository = laundryOrderItemRepository;
            _laundryOrderReviewRepository = laundryOrderReviewRepository;
            _excursionRepository = excursionRepository;
            _hotelExcursionRepository = hotelExcursionRepository;
            _hotelExcursionDetailRepository = hotelExcursionDetailRepository;
            _excursionDetailImagesRepository = excursionDetailImagesRepository;
            _excursionDetailGalleryRepository = excursionDetailGalleryRepository;
            _excursionIngredientCategoryRepository = excursionIngredientCategoryRepository;
            _excursionIngredientRepository = excursionIngredientRepository;
            _excursionReviewRepository = excursionReviewRepository;
            _excursionOfferRepository = excursionOfferRepository;
            _excursionDetailIngredientRepository = excursionDetailIngredientRepository;
            _excursionSuggestionRepository = excursionSuggestionRepository;
            _excursionCartRepository = excursionCartRepository;
            _excursionCartItemsRepository = excursionCartItemsRepository;
            _excursionOrderRepository = excursionOrderRepository;
            _excursionOrderItemsRepository = excursionOrderItemsRepository;
            _excursionOrderReviewRepository = excursionOrderReviewRepository;
            _conciergeGroupRepository = conciergeGroupRepository;
            _conciergeRepository = conciergeRepository;
            _hotelNearByPlacesRespository = hotelNearByPlacesRespository;
            _spaServiceOfferRepository = spaServiceOfferRepository;
            _spaIngredientCategoryRepository = spaIngredientCategoryRepository;
            _spaIngredientRepository = spaIngredientRepository;
            _spaSuggestionRepository = spaSuggestionRepository;
            _extraTimeRepository = extratimeRepository;
            _extraProcedureRepository = extraProcedureRepository;
            _hotelHouseKeepingInfoRepository = hotelHouseKeepingInfoRepository;
            _spaEmployeeAvailableTimeSlotRepository = spaEmployeeAvailableTimeSlotRepository;
            _customerHotelRoomRepository = customerHotelRoomRepository;

            _housekeepingCartRepository = housekeepingCartRepository;
            _housekeepingCartItemRepository = housekeepingCartItemRepository;
            _housekeepingCartItemAdditionalRepository = housekeepingCartItemAdditionalRepository;
            _housekeepingOrderRepository = housekeepingOrderRepository;
            _housekeepingOrderItemRepository = housekeepingOrderItemRepository;
            _wakeUpRepository = wakeUpRepository;
            _allHotelServicesOffersRepository = allHotelServicesOffersRepository;

            _hotelOperations = new CRUDOperation<IRepository<Hotel>, Hotel>(_hotelDetailsRepository);
            _hotelReviewsOperations = new CRUDOperation<IRepository<HotelReview>, HotelReview>(_hotelReviewsRepository);
            _spaServiceOperations = new CRUDOperation<IRepository<SpaService>, SpaService>(_spaServiceRepository);
            _spaServiceBookingOperations = new CRUDOperation<IRepository<SPAServiceBooking>, SPAServiceBooking>(_spaServiceBookingRepository);
            _spaServiceBookingDetailOperations = new CRUDOperation<IRepository<SPAServiceBookingDetail>, SPAServiceBookingDetail>(_spaServiceBookingDetailRepository);
            _hotelLaundryOperations = new CRUDOperation<IRepository<Laundry>, Laundry>(_hotelLaundryRepository);
            _hotelLaundryDetailsOperations = new CRUDOperation<IRepository<LaundryDetail>, LaundryDetail>(_hotelLaundryDetailsRepository);
            _laundryDetailAdditionalOperations = new CRUDOperation<IRepository<LaundryDetailAdditional>, LaundryDetailAdditional>(_laundryDetailAdditionalRepository);
            _laundryAdditionalGroupsOperations = new CRUDOperation<IRepository<LaundryAdditionalGroup>, LaundryAdditionalGroup>(_laundryAdditionalGroupsRepository);
            _laundryAdditionalElementOperations = new CRUDOperation<IRepository<LaundryAdditionalElement>, LaundryAdditionalElement>(_laundryAdditionalElementRepository);
            _hotelLaundryBookingOperations = new CRUDOperation<IRepository<LaundryBooking>, LaundryBooking>(_hotelLaundryBookingRepository);
            _hotelLaundryBookingDetailOperations = new CRUDOperation<IRepository<LaundryBookingDetail>, LaundryBookingDetail>(_hotelLaundryBookingDetailRepository);
            _hotelTripOperations = new CRUDOperation<IRepository<Trip>, Trip>(_hotelTripsRepository);
            _hotelTripDetailsOperations = new CRUDOperation<IRepository<TripDetail>, TripDetail>(_hotelTripDetailsRepository);
            _hotelTripBookingOperations = new CRUDOperation<IRepository<TripBooking>, TripBooking>(_hotelTripBookingRepository);
            _hotelServicesOperations = new CRUDOperation<IRepository<HotelServices>, HotelServices>(_hotelServicesRepository);
            _spaServiceDetailsOperations = new CRUDOperation<IRepository<SpaServiceDetail>, SpaServiceDetail>(_spaServiceDetailsRepository);
            _spaServiceDetailIngredientsOperations = new CRUDOperation<IRepository<SpaServiceDetailIngredient>, SpaServiceDetailIngredient>(_spaServiceDetailIngredientsRepository);
            _spaServiceDetailImagesOperations = new CRUDOperation<IRepository<SpaServiceDetailImages>, SpaServiceDetailImages>(_spaServiceDetailImagesRepository);
            _taxiDetailOperations = new CRUDOperation<IRepository<TaxiDetail>, TaxiDetail>(_taxiDetailRepository);
            _taxiBookingOperations = new CRUDOperation<IRepository<TaxiBooking>, TaxiBooking>(_taxiBookingRepository);
            _taxiBookingReviewOperations = new CRUDOperation<IRepository<TaxiBookingReview>, TaxiBookingReview>(_taxiBookingReviewRepository);
            _hotelRoomsOperation = new CRUDOperation<IRepository<Room>, Room>(_hotelRoomsRepository);
            _hotelRoomRateOperations = new CRUDOperation<IRepository<RoomRate>, RoomRate>(_hotelRoomRateRepository);
            _hotelRoomCategoryOperation = new CRUDOperation<IRepository<RoomCategory>, RoomCategory>(_hotelRoomCategoryRepository);
            _hotelRoomCapacityOperations = new CRUDOperation<IRepository<RoomCapacity>, RoomCapacity>(_hotelRoomCapacityRepository);
            _hotelRoomAmenitiesOperations = new CRUDOperation<IRepository<RoomAmenities>, RoomAmenities>(_hotelRoomAmenitiesRepository);
            _hotelRoomReviewsOperation = new CRUDOperation<IRepository<RoomReview>, RoomReview>(_hotelRoomReviewsRepository);
            _hotelBookingOperation = new CRUDOperation<IRepository<CustomerHotelBooking>, CustomerHotelBooking>(_hotelBookingRepository);
            _customerCreditCardOpration = new CRUDOperation<IRepository<CustomerCreditCard>, CustomerCreditCard>(_customerCreditCardRepository);
            _spaServiceReviewOperation = new CRUDOperation<IRepository<SpaServiceReview>, SpaServiceReview>(_spaServiceReviewRepository);
            _customerRoleOperations = new CRUDOperation<IRepository<CustomerRole>, CustomerRole>(_customerRoleRepository);
            _chainTableDetailsOperations = new CRUDOperation<IRepository<ChainDetailsTable>, ChainDetailsTable>(_chainDetailsTableRepository);
            _chainTableOperation = new CRUDOperation<IRepository<ChainTable>, ChainTable>(_chainTableRepository);
            _tripReviewOperations = new CRUDOperation<IRepository<TripReview>, TripReview>(_tripReviewRepository);
            _servicesOperation = new CRUDOperation<IRepository<Entities.Services>, Entities.Services>(_servicesRepository);
            _laundryReviewOperations = new CRUDOperation<IRepository<LaundryReview>, LaundryReview>(_laundryReviewRepository);
            _taxiReviewOperations = new CRUDOperation<IRepository<TaxiReview>, TaxiReview>(_taxiReviewRepository);
            _spaServiceGroupOperations = new CRUDOperation<IRepository<SPAServiceGroup>, SPAServiceGroup>(_spaServiceGroupRepository);
            _spaServiceDetailsGroupOperation = new CRUDOperation<IRepository<SPAServiceDetailsGroup>, SPAServiceDetailsGroup>(_spaServiceDetailsGroupRepository);
            _customerOperations = new CRUDOperation<IRepository<Customer>, Customer>(_customerRepository);
            _roomOperations = new CRUDOperation<IRepository<Room>, Room>(_roomRepository);
            _roomTypeOperations = new CRUDOperation<IRepository<RoomType>, RoomType>(_roomTypeRepository);
            _hotelRoomImagesOperations = new CRUDOperation<IRepository<HotelRoomImages>, HotelRoomImages>(_hotelRoomImagesRepository);
            _hotelRestaurantsOperations = new CRUDOperation<IRepository<HotelRestaurant>, HotelRestaurant>(_hotelRestaurantsRepository);
            _hotelServicesReviewsOperations = new CRUDOperation<IRepository<HotelServicesReviews>, HotelServicesReviews>(_hotelServicesReviewsRepository);
            _allHotelServicesReviewsOperations = new CRUDOperation<IRepository<VW_ServicesReviews>, VW_ServicesReviews>(_allHotelServicesReviewsRepository);
            _spaCartOperations = new CRUDOperation<IRepository<SpaCart>, SpaCart>(_spaCartRepository);
            _spaEmployeeDetailsOperation = new CRUDOperation<IRepository<SpaEmployeeDetails>, SpaEmployeeDetails>(_spaEmployeeDetailsRepository);
            _spaCartItemsOperation = new CRUDOperation<IRepository<SpaCartItem>, SpaCartItem>(_spaCartItemsRepository);
            _spaOrderOperation = new CRUDOperation<IRepository<SpaOrder>, SpaOrder>(_spaOrderRepository);
            _spaAdditionalGroupOperations = new CRUDOperation<IRepository<SpaAdditionalGroup>, SpaAdditionalGroup>(_spaAdditionalGroupRepository);
            _spaAdditionalElementOperations = new CRUDOperation<IRepository<SpaAdditionalElement>, SpaAdditionalElement>(_spaAdditionalElementRepository);
            _spaOrderReviewOperations = new CRUDOperation<IRepository<SpaOrderReview>, SpaOrderReview>(_spaOrderReviewRepository);
            _spaOrderItemsOperation = new CRUDOperation<IRepository<SpaOrderItem>, SpaOrderItem>(_spaOrderItemsRepository);
            _spaEmployeeOperation = new CRUDOperation<IRepository<SpaEmployee>, SpaEmployee>(_spaEmployeeRepository);
            _spaEmployeeTimeSlotOperation = new CRUDOperation<IRepository<SpaEmployeeTimeSlot>, SpaEmployeeTimeSlot>(_SpaEmployeeTimeSlotRepository);
            _spaRoomOperations = new CRUDOperation<IRepository<SpaRoom>, SpaRoom>(_spaRoomRepository);
            _spaRoomDetailsOperations = new CRUDOperation<IRepository<SpaRoomDetails>, SpaRoomDetails>(_spaRoomDetailsRepository);
            _roomDetailsOperations = new CRUDOperation<IRepository<RoomDetails>, RoomDetails>(_roomDetailsRepository);
            _houseKeepingFacilitiesOperations = new CRUDOperation<IRepository<HouseKeepingFacility>, HouseKeepingFacility>(_houseKeepingFacilitiesRepository);
            _houseKeepingFacilityDetailsOperations = new CRUDOperation<IRepository<HouseKeepingFacilityDetail>, HouseKeepingFacilityDetail>(_houseKeepingFacilityDetailsRepository);
            _housekeepingAdditionalGroupsOperations = new CRUDOperation<IRepository<HousekeepingAdditionalGroup>, HousekeepingAdditionalGroup>(_housekeepingAdditionalGroupsRepository);
            _housekeepingAdditionalElementsOperations = new CRUDOperation<IRepository<HousekeepingAdditionalElement>, HousekeepingAdditionalElement>(_housekeepingAdditionalElementsRepository);
            _houseKeepingfacilitiesAdditionalOperations = new CRUDOperation<IRepository<HouseKeepingfacilitiesAdditional>, HouseKeepingfacilitiesAdditional>(_houseKeepingfacilitiesAdditionalsRepository);
            _houseKeepingOperations = new CRUDOperation<IRepository<HouseKeeping>, HouseKeeping>(_houseKeepingRepository);
            _taxiDestinationOperations = new CRUDOperation<IRepository<TaxiDestination>, TaxiDestination>(_taxiDestinationRepository);
            _amenitiesOperation = new CRUDOperation<IRepository<Amenities>, Amenities>(_amenitiesRepository);
            _garmentsOperation = new CRUDOperation<IRepository<Garments>, Garments>(_garmentsRepository);
            _laundryCartOperations = new CRUDOperation<IRepository<LaundryCart>, LaundryCart>(_laundryCartRepository);
            _laundryOrderOperations = new CRUDOperation<IRepository<LaundryOrder>, LaundryOrder>(_laundryOrderRepository);
            _laundryOrderItemOperations = new CRUDOperation<IRepository<LaundryOrderItems>, LaundryOrderItems>(_laundryOrderItemRepository);
            _laundryOrderReviewOperations = new CRUDOperation<IRepository<LaundryOrderReview>, LaundryOrderReview>(_laundryOrderReviewRepository);
            _excursionOperations = new CRUDOperation<IRepository<Excursion>, Excursion>(_excursionRepository);
            _hotelExcursionOperations = new CRUDOperation<IRepository<HotelExcursion>, HotelExcursion>(_hotelExcursionRepository);
            _hotelExcursionDetailsOperations = new CRUDOperation<IRepository<ExcursionDetail>, ExcursionDetail>(_hotelExcursionDetailRepository);
            _excursionDetailImagesOperations = new CRUDOperation<IRepository<ExcursionDetailImages>, ExcursionDetailImages>(_excursionDetailImagesRepository);
            _excursionDetailGalleryOperations = new CRUDOperation<IRepository<ExcursionDetailGallery>, ExcursionDetailGallery>(_excursionDetailGalleryRepository);
            _excursionIngredientCategoryOperations = new CRUDOperation<IRepository<ExcursionIngredientCategory>, ExcursionIngredientCategory>(_excursionIngredientCategoryRepository);
            _excursionIngredientOperations = new CRUDOperation<IRepository<ExcursionIngredient>, ExcursionIngredient>(_excursionIngredientRepository);
            _excursionReviewsOperations = new CRUDOperation<IRepository<ExcursionReview>, ExcursionReview>(_excursionReviewRepository);
            _excursionOfferOperations = new CRUDOperation<IRepository<ExcursionOffer>, ExcursionOffer>(_excursionOfferRepository);
            _excursionDetailIngredientOperations = new CRUDOperation<IRepository<ExcursionDetailIngredient>, ExcursionDetailIngredient>(_excursionDetailIngredientRepository);
            _excursionSuggestionOperations = new CRUDOperation<IRepository<ExcursionSuggestion>, ExcursionSuggestion>(_excursionSuggestionRepository);
            _excursionCartOperations = new CRUDOperation<IRepository<ExcursionCart>, ExcursionCart>(_excursionCartRepository);
            _excursionCartItemsOpertions = new CRUDOperation<IRepository<ExcursionCartItems>, ExcursionCartItems>(_excursionCartItemsRepository);
            _excursionOrderOperations = new CRUDOperation<IRepository<ExcursionOrder>, ExcursionOrder>(_excursionOrderRepository);
            _excursionOrderItemsOpersations = new CRUDOperation<IRepository<ExcursionOrderItems>, ExcursionOrderItems>(_excursionOrderItemsRepository);
            _excursionOrderReviewOperation = new CRUDOperation<IRepository<ExcursionOrderReview>, ExcursionOrderReview>(_excursionOrderReviewRepository);
            _spaDetailAdditionalOperations = new CRUDOperation<IRepository<SpaDetailAdditional>, SpaDetailAdditional>(_spaDetailAdditionalRepository);
            _conciergeGroupOperation = new CRUDOperation<IRepository<ConciergeGroup>, ConciergeGroup>(_conciergeGroupRepository);
            _conciergeOperation = new CRUDOperation<IRepository<Concierge>, Concierge>(_conciergeRepository);
            _hotelNearByPlacesOperations = new CRUDOperation<IRepository<HotelsNearByPlace>, HotelsNearByPlace>(_hotelNearByPlacesRespository);
            _spaServiceOfferOperations = new CRUDOperation<IRepository<SpaServiceOffer>, SpaServiceOffer>(_spaServiceOfferRepository);
            _spaIngredientCategoryOperations = new CRUDOperation<IRepository<SpaIngredientCategory>, SpaIngredientCategory>(_spaIngredientCategoryRepository);
            _spaIngredientOperations = new CRUDOperation<IRepository<SpaIngredient>, SpaIngredient>(_spaIngredientRepository);
            _spaSuggestionOperations = new CRUDOperation<IRepository<SpaSuggestion>, SpaSuggestion>(_spaSuggestionRepository);
            _extraTimeOperations = new CRUDOperation<IRepository<ExtraTime>, ExtraTime>(_extraTimeRepository);
            _extraProcedureOperations = new CRUDOperation<IRepository<ExtraProcedure>, ExtraProcedure>(_extraProcedureRepository);
            _hotelHouseKeepingInfoOperations = new CRUDOperation<IRepository<HotelHouseKeepingInfo>, HotelHouseKeepingInfo>(_hotelHouseKeepingInfoRepository);
            _customerHotelRoomOperation = new CRUDOperation<IRepository<CustomerHotelRoom>, CustomerHotelRoom>(_customerHotelRoomRepository);
            _housekeepingCartOperation = new CRUDOperation<IRepository<HousekeepingCart>, HousekeepingCart>(_housekeepingCartRepository);
            _housekeepingOrderOperation = new CRUDOperation<IRepository<HousekeepingOrder>, HousekeepingOrder>(_housekeepingOrderRepository);
            _housekeepingOrderItemOperation = new CRUDOperation<IRepository<HousekeepingOrderItem>, HousekeepingOrderItem>(_housekeepingOrderItemRepository);
            _wakeUpOperation = new CRUDOperation<IRepository<WakeUp>, WakeUp>(_wakeUpRepository);
            _allHotelServicesOffersOperation = new CRUDOperation<IRepository<AllHotelServicesOffers>, AllHotelServicesOffers>(_allHotelServicesOffersRepository);
        }
        #endregion Initialization

        #region Hotels
        /// <summary>
        /// Searches the specified total.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="checkInDate">The check in date.</param>
        /// <param name="checkOutDate">The check out date.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="radius_min">The radius minimum.</param>
        /// <param name="radius_max">The radius maximum.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="starRating_min">The star rating minimum.</param>
        /// <param name="starRating_max">The star rating maximum.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="userRating_min">The user rating minimum.</param>
        /// <param name="userRating_max">The user rating maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="searchTextTypes">The search text types.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selection">The selection.</param>
        /// <param name="starRating">The star rating.</param>
        /// <param name="priceRange">The price range.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> Search(out int total, string searchText, string checkInDate, string checkOutDate, double? lat, double? lng, double? radius_min, double? radius_max, int[] Ids, double? starRating_min, double? starRating_max, double? price_min, double? price_max, double? userRating_min = null, double? userRating_max = null, int page = 1, int limit = 20, SearchTextTypes searchTextTypes = SearchTextTypes.Everywhere, int? customerId = null, Selection selection = Selection.All, int? starRating = null, int? priceRange = null)
        {
            var query = _hotelDetailsRepository.Table.Where(h => h.Status == true && h.IsActive == true);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                switch (searchTextTypes)
                {
                    case SearchTextTypes.Everywhere:
                        query = query.Where(r => r.Name.Contains(searchText) ||
                    (r.Street + " " + r.City + " " + r.Region + " " + r.Country.Name).Contains(searchText));
                        break;
                    case SearchTextTypes.HotelName:
                        query = query.Where(r => r.Name.Contains(searchText));
                        break;
                }
            }

            if (lat.HasValue && lng.HasValue)
            {
                var sourcePoint = CreatePoint(lat.Value, lng.Value);
                query = query.Where(loc => loc.Location.Distance(sourcePoint) >= radius_min.Value && loc.Location.Distance(sourcePoint) < radius_max);
            }

            if (!string.IsNullOrWhiteSpace(checkInDate) && !string.IsNullOrWhiteSpace(checkOutDate))
            {
                var checkIn = Convert.ToDateTime(checkInDate);
                var checkOut = Convert.ToDateTime(checkOutDate);
                var bookedRooms = _hotelBookingOperation.GetAll(hb => hb.From <= checkIn && hb.To >= checkIn ||
                                                              (hb.From <= checkOut && hb.To >= checkOut) ||
                                                               (checkIn <= hb.From && checkOut >= hb.To)).Select(br => br.RoomId).ToList();

                query = query.Where(h => h.Room.Any(r => !bookedRooms.Contains(r.Id)));
            }
            if (Ids != null && Ids.Length > 0)
            {
                query = query.Where(h => h.HotelServices.Any(hs => Ids.Contains(hs.ServiceId) && hs.IsActive));
            }

            if (starRating_min.HasValue && !starRating_max.HasValue)
            {
                query = query.Where(h => h.StarRating >= starRating_min);
            }
            else if (starRating_max.HasValue && !starRating_min.HasValue)
            {
                query = query.Where(h => h.StarRating <= starRating_max);

            }
            else if (starRating_min.HasValue && starRating_max.HasValue)
            {
                query = query.Where(h => h.StarRating >= starRating_min && h.StarRating <= starRating_max);
            }

            if (price_min.HasValue && !price_max.HasValue)
            {
                query = query.Where(h => h.CostForTwo >= price_min);
            }

            else if (price_max.HasValue && !price_min.HasValue)
            {
                query = query.Where(h => h.CostForTwo <= price_max);

            }
            else if (price_min.HasValue && price_max.HasValue)
            {
                query = query.Where(h => h.CostForTwo >= price_min && h.CostForTwo <= price_max);
            }

            if (userRating_min.HasValue && !userRating_max.HasValue)
                query = query.Where(h => h.HotelReviews.Average(rw => rw.Score) >= userRating_min);

            else if (userRating_max.HasValue && !userRating_min.HasValue)
                query = query.Where(h => h.HotelReviews.Average(rw => rw.Score) <= userRating_max);

            else if (userRating_min.HasValue && userRating_max.HasValue)
                query = query.Where(h => h.HotelReviews.Average(rw => rw.Score) >= userRating_min && h.HotelReviews.Average(rw => rw.Score) <= userRating_max);

            if (starRating != null)
            {
                query = query.Where(r => r.StarRating == starRating);
            }
            if (priceRange != null)
            {
                query = query.Where(r => r.PriceRange == priceRange);
            }

            switch (selection)
            {
                case Selection.All:
                    break;
                case Selection.Popular:
                    var toDate = DateTime.Now;
                    var fromDate = toDate.AddDays(-30);
                    var booking = Convert.ToInt32(ConfigurationSettings.AppSettings["Orders"]);
                    query = query.Where(h => h.Room.Any(f => f.CustomerHotelBooking.Count(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDate) && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(toDate)) >= booking));
                    break;

                case Selection.Personal:

                    if (customerId.HasValue && customerId.Value > 0)
                    {
                        query = query.Where(h => h.CustomerFavoriteHotels.Any(f => f.CustomerId == customerId.Value));
                    }
                    break;
            }

            var pages = query.OrderBy(_ => true)
                        .Skip((page - 1) * limit)
                        .Take(limit)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Hotel>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets all hotels.
        /// </summary>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetAllHotels()
        {
            return _hotelOperations.GetAll();
        }

        /// <summary>
        /// Gets all user hotels.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetAllUserHotels(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            if (customerRole.RoleId == 1)
            {
                return _hotelOperations.GetAll(h => h.Status);
            }
            else
            {
                return _chainTableDetailsOperations.GetAll(c => c.ChainId == customerRole.ChainId && c.Hotel != null).Select(h => h.Hotel).Where(h => h.Status).ToList();
            }
        }

        /// <summary>
        /// Gets all hotels.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="range">The range.</param>
        /// <param name="orderby">The order by.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel .</returns>
        public IEnumerable<Hotel> GetAllHotels(double latitude, double longitude, double range, string orderby, int pageNumber, int pageSize, out int total)
        {
            //var text = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", longitude, latitude);
            var sourcePoint = CreatePoint(latitude, longitude);

            return _hotelOperations.GetAll(loc => loc.Location.Distance(sourcePoint) < range, orderby, pageNumber, pageSize, out total).OrderBy(loc => loc.Location.Distance(sourcePoint));
        }

        /// <summary>
        /// Creates the point.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <returns>DbGeography.</returns>
        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                     "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(text, 4326);
        }
        /// <summary>
        /// Gets the hotel by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Hotel.</returns>
        public Hotel GetHotelById(int id)
        {
            return _hotelOperations.GetById(id);
        }

        /// <summary>
        /// Creates the hotel.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Hotel.</returns>
        public Hotel CreateHotel(Hotel details)
        {
            var location = CreatePoint(Convert.ToDouble(details.Latitude), Convert.ToDouble(details.Longitude));
            details.Location = location;
            details.CreationDate = DateTime.UtcNow;
            return _hotelOperations.AddNew(details);
        }

        /// <summary>
        /// Modifies the hotel.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Hotel.</returns>
        public Hotel ModifyHotel(Hotel details)
        {
            var location = CreatePoint(Convert.ToDouble(details.Latitude), Convert.ToDouble(details.Longitude));
            details.Location = location;
            _hotelOperations.Update(details.Id, details);
            return details;
        }

        /// <summary>
        /// Deletes the hotel.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotel(int id)
        {
            _hotelOperations.Delete(id);
        }
        #endregion

        #region Hotel virtual Restaurants
        /// <summary>
        /// Gets all hotels restaurants.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelRestaurant.</returns>
        public IEnumerable<HotelRestaurant> GetAllHotelsRestaurants(int skip, int pageSize, out int total)
        {
            var query = _hotelRestaurantsRepository.Table;
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HotelRestaurant>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the unassign hotel restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetUnassignHotelRestaurants()
        {
            var hotRestId = _hotelRestaurantsOperations.GetAll().Select(hr => hr.RestaurantId).Distinct();
            var restaurants = _restaurantRepository.Table.Where(r => !hotRestId.Contains(r.Id));
            return restaurants;
        }

        /// <summary>
        /// Creates the hotel restaurants.
        /// </summary>
        /// <param name="hotelRests">The hotel rests.</param>
        public void CreateHotelRestaurants(List<HotelRestaurant> hotelRests)
        {
            foreach (var hotRest in hotelRests)
            {
                _hotelRestaurantsOperations.AddNew(hotRest);
            }
        }
        /// <summary>
        /// Deletes the hotel restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotelRestaurant(int id)
        {
            _hotelRestaurantsOperations.Delete(id);
        }
        /// <summary>
        /// Gets the hotel restaurant by rest identifier.
        /// </summary>
        /// <param name="restId">The rest identifier.</param>
        /// <returns>HotelRestaurant.</returns>
        public HotelRestaurant GetHotelRestaurantByRestId(int restId)
        {
            return _hotelRestaurantsOperations.GetById(r => r.RestaurantId == restId);
        }
        #endregion

        #region Hotel House Keeping Info
        /// <summary>
        /// Gets the hotel house keeping information by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>HotelHouseKeepingInfo.</returns>
        public HotelHouseKeepingInfo GetHotelHouseKeepingInfoByHotelId(int hotelId)
        {
            return _hotelHouseKeepingInfoOperations.GetById(h => h.HotelId == hotelId);
        }

        /// <summary>
        /// Adds the update house keeping information.
        /// </summary>
        /// <param name="hotelHouseKeepingInfo">The hotel house keeping information.</param>
        /// <returns>HotelHouseKeepingInfo.</returns>
        public HotelHouseKeepingInfo AddUpdateHouseKeepingInfo(HotelHouseKeepingInfo hotelHouseKeepingInfo)
        {
            if (hotelHouseKeepingInfo.Id == 0)
                return _hotelHouseKeepingInfoOperations.AddNew(hotelHouseKeepingInfo);
            else
            {
                _hotelHouseKeepingInfoOperations.Update(hotelHouseKeepingInfo.Id, hotelHouseKeepingInfo);
                return hotelHouseKeepingInfo;
            }

        }
        #endregion Hotel house Keeping Info

        #region Hotel Near By Places
        //API
        /// <summary>
        /// Gets all hotel near by places.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelsNearByPlace.</returns>
        public IEnumerable<HotelsNearByPlace> GetAllHotelNearByPlaces(int hotelId)
        {
            return _hotelNearByPlacesOperations.GetAll(h => h.HotelId == hotelId);
        }
        //WEb
        /// <summary>
        /// Gets all hotel near by places.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelsNearByPlace.</returns>
        public IEnumerable<HotelsNearByPlace> GetAllHotelNearByPlaces(int hotelId, int skip, int pageSize, out int total)
        {
            return _hotelNearByPlacesOperations.GetAllWithServerSidePagging(h => h.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the hotel near by place.
        /// </summary>
        /// <param name="hotelNearByPlace">The hotel near by place.</param>
        /// <returns>HotelsNearByPlace.</returns>
        public HotelsNearByPlace CreateHotelNearByPlace(HotelsNearByPlace hotelNearByPlace)
        {
            return _hotelNearByPlacesOperations.AddNew(hotelNearByPlace);
        }
        /// <summary>
        /// Modifies the hotel near by place.
        /// </summary>
        /// <param name="hotelNearByPlace">The hotel near by place.</param>
        /// <returns>HotelsNearByPlace.</returns>
        public HotelsNearByPlace ModifyHotelNearByPlace(HotelsNearByPlace hotelNearByPlace)
        {
            _hotelNearByPlacesOperations.Update(hotelNearByPlace.Id, hotelNearByPlace);
            return hotelNearByPlace;
        }

        /// <summary>
        /// Gets the hotel near by place by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HotelsNearByPlace.</returns>
        public HotelsNearByPlace GetHotelNearByPlaceById(int id)
        {
            return _hotelNearByPlacesOperations.GetById(id);
        }
        /// <summary>
        /// Deletes the hotel near by place.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotelNearByPlace(int id)
        {
            _hotelNearByPlacesOperations.Delete(id);
        }
        #endregion Hotel Near By Places

        #region hotel reviews

        /// <summary>
        /// Gets the hotel reviews.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetHotelReviews(int HotelId, int pageNumber, int pageSize, out int total)
        {
            var query = _hotelReviewsRepository.Table;
            query = query.Where(h => h.HotelId == HotelId);
            var pages = query.OrderByDescending(h => h.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HotelReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets all hotels reviews.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetAllHotelsReviews(string fromDate, string toDate, int? HotelId, int skip, int pageSize, out int total)
        {
            var query = _hotelReviewsRepository.Table;
            if (HotelId != null)
                query = query.Where(h => h.HotelId == HotelId);
            var pages = query.OrderByDescending(h => h.Id)
                        .Skip(skip)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HotelReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets all hotels reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetAllHotelsReviews(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId).RoleId;
            if (customerRole == 1)
            {
                return _hotelReviewsOperations.GetAll();
            }
            return _hotelReviewsOperations.GetAll();
            //else
            //{
            //    var chainID = _chainTableOperation.GetById(c => c.CustomerId == customerId).Id;
            //    return _chainTableDetailsOperations.GetAll(cd => cd.ChainId == chainID && cd.HotelId != null).SelectMany(h => h.Hotel.HotelReviews).ToList();
            //}
        }
        /// <summary>
        /// Gets the review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HotelReview.</returns>
        public HotelReview GetReviewById(int id)
        {
            return _hotelReviewsOperations.GetById(id);
        }
        /// <summary>
        /// Deletes the hotel review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotelReview(int id)
        {
            _hotelReviewsOperations.Delete(id);
        }
        /// <summary>
        /// Creates the hotel reviews.
        /// </summary>
        /// <param name="hotelReviews">The hotel reviews.</param>
        /// <returns>HotelReview.</returns>
        public HotelReview CreateHotelReviews(HotelReview hotelReviews)
        {
            hotelReviews.CreationDate = DateTime.UtcNow;
            return _hotelReviewsOperations.AddNew(hotelReviews);
        }


        /// <summary>
        /// Gets the customer all hotels reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetCustomerAllHotelsReviews(int customerId)
        {
            return _hotelReviewsOperations.GetAll(r => r.CustomerId == customerId);
        }

        /// <summary>
        /// Gets the hotel all reviews.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetHotelAllReviews(int? hotelId, int? customerId, int skip, int pageSize, out int total)
        {
            var query = _hotelReviewsRepository.Table;
            if (hotelId != null)
                query = query.Where(h => h.HotelId == hotelId);
            if (customerId != null)
                query = query.Where(h => h.CustomerId == customerId);
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HotelReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        #endregion

        #region hotel services
        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <returns>IEnumerable ToOrdr.Core.Entities.Services.</returns>
        public IEnumerable<ToOrdr.Core.Entities.Services> GetAllServices()
        {
            return _servicesOperation.GetAll().ToList();
        }
        /// <summary>
        /// Gets all unassigned services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ToOrdr.Core.Entities.Services.</returns>
        public IEnumerable<ToOrdr.Core.Entities.Services> GetAllUnassignedServices(int hotelId)
        {
            var assignedServices = _hotelServicesOperations.GetAll(s => s.HotelId == hotelId).Select(h => h.ServiceId).Distinct().ToList();
            return _servicesOperation.GetAll(s => !assignedServices.Contains(s.Id)).ToList();
        }
        /// <summary>
        /// Gets the name of all services by.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns>IEnumerable Entities.Services.</returns>
        public IEnumerable<Entities.Services> GetAllServicesByName(string serviceName)
        {
            return _servicesOperation.GetAll(hs => hs.ServiceName.ToLower().Contains(serviceName.ToLower())).ToList();
        }
        /// <summary>
        /// Gets the hotel services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelServices.</returns>
        public IEnumerable<HotelServices> GetHotelServices(int hotelId)
        {
            return _hotelServicesOperations.GetAll(s => s.HotelId == hotelId && s.IsActive);
        }
        /// <summary>
        /// Gets the hotel active service by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        public HotelServices GetHotelActiveServiceByHotelId(int hotelId, int serviceId)
        {
            return _hotelServicesOperations.GetById(s => s.HotelId == hotelId && s.Services.Id == serviceId && s.IsActive);
        }
        /// <summary>
        /// Gets all hotel services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HotelServices.</returns>
        public IEnumerable<HotelServices> GetAllHotelServices(int hotelId)
        {
            return _hotelServicesOperations.GetAll(s => s.HotelId == hotelId);
        }
        /// <summary>
        /// Gets the hotel services by service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        public HotelServices GetHotelServicesByServiceId(int serviceId)
        {
            return _hotelServicesOperations.GetById(s => s.Id == serviceId);
        }
        /// <summary>
        /// Gets the active hotel services by service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>HotelServices.</returns>
        public HotelServices GetActiveHotelServicesByServiceId(int serviceId)
        {
            return _hotelServicesOperations.GetById(s => s.Id == serviceId && s.IsActive);
        }
        /// <summary>
        /// Creates the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>Entities.Services.</returns>
        /// <exception cref="ArgumentException">Same Service already exist</exception>
        public Entities.Services CreateService(Entities.Services services)
        {
            var existing = _servicesOperation.GetAll(s => s.ServiceName.ToLower() == services.ServiceName.ToLower());
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Same Service already exist");

            else
                return _servicesOperation.AddNew(services);
        }
        /// <summary>
        /// Modifies the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>Entities.Services.</returns>
        /// <exception cref="ArgumentException">Same Service already exist</exception>
        public Entities.Services ModifyService(Entities.Services services)
        {
            var existing = _servicesOperation.GetAll(s => s.ServiceName.ToLower() == services.ServiceName.ToLower() && s.Id != services.Id);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Same Service already exist");
            else
                _servicesOperation.Update(services.Id, services);
            return services;
        }

        /// <summary>
        /// Gets the service by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Entities.Services.</returns>
        public Entities.Services GetServiceById(int Id)
        {
            return _servicesOperation.GetById(s => s.Id == Id);
        }

        /// <summary>
        /// Deletes the service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void DeleteService(int Id)
        {
            _servicesOperation.Delete(Id);
        }

        /// <summary>
        /// Creates the hotel service.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <returns>HotelServices.</returns>
        /// <exception cref="ArgumentException">Same Service already exist for hotel</exception>
        public HotelServices CreateHotelService(HotelServices hotelService)
        {
            hotelService.CreationDate = DateTime.UtcNow;

            var existing = _hotelServicesOperations.GetAll(s => s.HotelId == hotelService.HotelId && s.ServiceId == hotelService.ServiceId);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Same Service already exist for hotel");

            else
                return _hotelServicesOperations.AddNew(hotelService);
        }

        /// <summary>
        /// Modifies the hotel service.
        /// </summary>
        /// <param name="hotelService">The hotel service.</param>
        /// <returns>HotelServices.</returns>
        public HotelServices ModifyHotelService(HotelServices hotelService)
        {
            _hotelServicesOperations.Update(hotelService.Id, hotelService);
            return hotelService;
        }

        /// <summary>
        /// Deletes the hotel service.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void DeleteHotelService(int Id)
        {
            var hotelService = _hotelServicesOperations.GetById(s => s.Id == Id);
            _hotelServicesOperations.Delete(Id);
        }

        #endregion hotel services

        #region Hotel Services Reviews
        //public HotelServicesReviews GetHotelServicesReviewsById(int reviewId)
        //{
        //    return _hotelServicesReviewsOperations.GetById(reviewId);
        //}
        //public IEnumerable<HotelServicesReviews> GetEachHotelServiceReviews(int hotelServiceId, int pageNumber, int pageSize, out int total)
        //{
        //    return _hotelServicesReviewsOperations.GetAllByDesc(hs => hs.HotelServiceId == hotelServiceId, or => or.Id, pageNumber, pageSize, out total);
        //}
        //public IEnumerable<HotelServicesReviews> GetHotelAllServicesReviews(int hotelId, int pageNumber, int pageSize, out int total)
        //{
        //    return _hotelServicesReviewsOperations.GetAllByDesc(hs => hs.HotelServices.HotelId == hotelId, or => or.Id, pageNumber, pageSize, out total);
        //}
        //public IEnumerable<HotelServicesReviews> GetHotelServicesReviews(int hotelId)
        //{
        //    return _hotelServicesReviewsOperations.GetAll(hs => hs.HotelServices.HotelId == hotelId);
        //}
        //public IEnumerable<HotelServicesReviews> GetHotelServicesReviewsByServiceId(int hotelId, int serviceId)
        //{
        //    return _hotelServicesReviewsOperations.GetAll(hs => hs.HotelServices.HotelId == hotelId && hs.HotelServices.ServiceId == serviceId);
        //}
        //public HotelServicesReviews CreateHotelServicesReviews(HotelServicesReviews hotelServicesReview)
        //{
        //    var hotelService = _hotelServicesOperations.GetById(hotelServicesReview.HotelServiceId);
        //    if (hotelService == null)
        //        throw new ArgumentException("HotelServiceId doesn't exists");
        //    hotelServicesReview.CreationDate = DateTime.UtcNow;
        //    return _hotelServicesReviewsOperations.AddNew(hotelServicesReview);
        //}
        //public void DeleteHotelServicesReview(int reviewId)
        //{
        //    _hotelServicesReviewsOperations.Delete(reviewId);
        //}

        //public IEnumerable<VW_ServicesReviews> GetAllHotelServicesReviews(int hotelId, int pageNumber, int pageSize, out int total)
        //{
        //    var query = _allHotelServicesReviewsRepository.Table;
        //    query = query.Where(s => s.HotelId == hotelId);
        //    var page = query.OrderByDescending(o => o.CreationDate)
        //        .Skip((pageNumber - 1) * pageSize)
        //        .Take(pageSize)
        //        .GroupBy(p => new { Total = query.Count() })
        //        .FirstOrDefault();
        //    total = 0;
        //    if (page == null)
        //        return new List<VW_ServicesReviews>();

        //    total = page.Key.Total;
        //    var entities = page.Select(p => p);
        //    return entities;
        //}


        #endregion Hotel Services Reviews

        #region hotel spa service

        /// <summary>
        /// Gets the hotel spa services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaService.</returns>
        public IEnumerable<SpaService> GetHotelSpaServices(int hotelId, int pageNumber, int pageSize, out int total)
        {
            return _spaServiceOperations.GetAll(c => c.HotelServices.HotelId == hotelId && c.IsActive, s => s.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Gets the hotel spa service by service identifier.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable SpaService.</returns>
        public IEnumerable<SpaService> GetHotelSPAServiceByServiceId(int hotelServiceId)
        {
            return _spaServiceOperations.GetAll(c => c.HotelServiceId == hotelServiceId);
        }
        /// <summary>
        /// Gets the hotel spa service by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaService.</returns>
        public SpaService GetHotelSpaServiceById(int id)
        {
            return _spaServiceOperations.GetById(c => c.Id == id);
        }

        /// <summary>
        /// Gets the spa service details by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaServiceDetail.</returns>
        public SpaServiceDetail GetSpaServiceDetailsById(int id)
        {
            return _spaServiceDetailsOperations.GetById(c => c.Id == id);
        }
        /// <summary>
        /// Creates the spa service.
        /// </summary>
        /// <param name="spaService">The spa service.</param>
        /// <returns>SpaService.</returns>
        public SpaService CreateSpaService(SpaService spaService)
        {
            return _spaServiceOperations.AddNew(spaService);
        }
        /// <summary>
        /// Modifies the spa service.
        /// </summary>
        /// <param name="spaService">The spa service.</param>
        /// <returns>SpaService.</returns>
        public SpaService ModifySpaService(SpaService spaService)
        {
            _spaServiceOperations.Update(spaService.Id, spaService);
            return spaService;
        }
        /// <summary>
        /// Deletes the spa service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaService(int id)
        {
            _spaServiceOperations.Delete(id);
        }

        //Spa Service Details
        /// <summary>
        /// Gets the spa service details.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetSpaServiceDetails(int spaServiceId)
        {
            return _spaServiceDetailsOperations.GetAll(d => d.SpaServiceId == spaServiceId);
        }
        /// <summary>
        /// Gets the spa service details.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetSpaServiceDetails(int spaServiceId, int customerId, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false)
        {
            if (considerMyPreferences)
            {
                System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var custPrefSpaServiceDetailsIds = _spaServiceDetailsRepository.ExecuteStoredProcedureList<SpaServiceDetail>("exec uspGetCustomerPreferredSpaServiceDetails", customerIdParameter).Select(s => s.Id).ToList(); ;

                var spaServiceDetail = _spaServiceDetailsOperations.GetAll(s => s.SpaServiceId == spaServiceId && s.IsActive && custPrefSpaServiceDetailsIds.Contains(s.Id), o => o.Id, pageNumber, pageSize, out total);
                return spaServiceDetail;
            }
            else
            {
                var spaServiceDetail = _spaServiceDetailsOperations.GetAll(s => s.SpaServiceId == spaServiceId && s.IsActive, o => o.Id, pageNumber, pageSize, out total);
                return spaServiceDetail;
            }

        }

        /// <summary>
        /// Searches the name of the spa service detail by.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> SearchSpaServiceDetailByName(string searchText, int hotelId, int pageNumber, int pageSize, out int total)
        {
            var query = _spaServiceDetailsRepository.Table;

            query = query.Where(s => s.SpaService.HotelServices.HotelId == hotelId && s.IsActive == true && s.SpaService.IsActive == true);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(s => s.Name.Contains(searchText));
            }
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaServiceDetail>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Adds the spa service detail.
        /// </summary>
        /// <param name="spaServiceDetail">The spa service detail.</param>
        /// <returns>SpaServiceDetail.</returns>
        public SpaServiceDetail AddSpaServiceDetail(SpaServiceDetail spaServiceDetail)
        {
            spaServiceDetail.Creation_Date = DateTime.UtcNow;
            return _spaServiceDetailsRepository.InsertWithChild(spaServiceDetail);
        }
        /// <summary>
        /// Modifies the spa service detail.
        /// </summary>
        /// <param name="spaServiceDetail">The spa service detail.</param>
        /// <returns>SpaServiceDetail.</returns>
        public SpaServiceDetail ModifySpaServiceDetail(SpaServiceDetail spaServiceDetail)
        {
            _spaServiceDetailsOperations.Update(spaServiceDetail.Id, spaServiceDetail);
            return spaServiceDetail;
        }
        /// <summary>
        /// Gets the spa service detail by identifier.
        /// </summary>
        /// <param name="detailId">The detail identifier.</param>
        /// <returns>SpaServiceDetail.</returns>
        public SpaServiceDetail GetSpaServiceDetailById(int detailId)
        {
            return _spaServiceDetailsOperations.GetById(d => d.Id == detailId);
        }
        /// <summary>
        /// Deletes the spa service detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaServiceDetail(int id)
        {
            _spaServiceDetailsOperations.Delete(id);
        }


        //Spa Service Detail Images
        /// <summary>
        /// Gets the spa detail image by identifier.
        /// </summary>
        /// <param name="spaDetImageId">The spa det image identifier.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        public SpaServiceDetailImages GetSpaDetailImageById(int spaDetImageId)
        {
            return _spaServiceDetailImagesOperations.GetById(spaDetImageId);
        }
        /// <summary>
        /// Gets the spa service detail images.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <returns>IEnumerable SpaServiceDetailImages.</returns>
        public IEnumerable<SpaServiceDetailImages> GetSpaServiceDetailImages(int spaServiceDetailId)
        {
            return _spaServiceDetailImagesOperations.GetAll(d => d.SpaServiceDetailId == spaServiceDetailId);
        }
        /// <summary>
        /// Updates the spa det image.
        /// </summary>
        /// <param name="spaDetImage">The spa det image.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        public SpaServiceDetailImages UpdateSpaDetImage(SpaServiceDetailImages spaDetImage)
        {
            _spaServiceDetailImagesOperations.Update(spaDetImage.Id, spaDetImage);
            return spaDetImage;
        }
        /// <summary>
        /// Adds the spa detail image.
        /// </summary>
        /// <param name="spaDetImage">The spa det image.</param>
        /// <returns>SpaServiceDetailImages.</returns>
        public SpaServiceDetailImages AddSpaDetailImage(SpaServiceDetailImages spaDetImage)
        {
            spaDetImage.CreationDate = DateTime.UtcNow;
            return _spaServiceDetailImagesOperations.AddNew(spaDetImage);
        }
        /// <summary>
        /// Deletes the spa service detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaServiceDetailImage(int id)
        {
            _spaServiceDetailImagesOperations.Delete(id);
        }

        //Spa service details Additional
        /// <summary>
        /// Gets all spa det additionals.
        /// </summary>
        /// <param name="spaDetailsId">The spa details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaDetailAdditional.</returns>
        public IEnumerable<SpaDetailAdditional> GetAllSpaDetAdditionals(int spaDetailsId, int skip, int pageSize, out int total)
        {
            return _spaDetailAdditionalOperations.GetAllWithServerSidePagging(s => s.SpaDetailId == spaDetailsId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the unassign spa add groups.
        /// </summary>
        /// <param name="spaDetailsId">The spa details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaAdditionalGroup.</returns>
        public IEnumerable<SpaAdditionalGroup> GetUnassignSpaAddGroups(int spaDetailsId, int hotelId)
        {
            var assignSAGIds = _spaDetailAdditionalOperations.GetAll(s => s.SpaDetailId == spaDetailsId).Select(sd => sd.SpaAdditionalGroupId).Distinct();
            var unassignedSpaAddGroups = _spaAdditionalGroupRepository.Table.Where(r => !assignSAGIds.Contains(r.Id) && r.IsActive && r.HotelId == hotelId);
            return unassignedSpaAddGroups;
        }

        /// <summary>
        /// Creates the spa det additional.
        /// </summary>
        /// <param name="spaDetailAdditional">The spa detail additional.</param>
        public void CreateSpaDetAdditional(List<SpaDetailAdditional> spaDetailAdditional)
        {
            foreach (var eachSpaDetAdd in spaDetailAdditional)
            {
                eachSpaDetAdd.CreationDate = DateTime.UtcNow;
                _spaDetailAdditionalOperations.AddNew(eachSpaDetAdd);
            }
        }

        /// <summary>
        /// Gets the spadet additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaDetailAdditional.</returns>
        public SpaDetailAdditional GetSpadetAdditionalById(int id)
        {
            return _spaDetailAdditionalOperations.GetById(id);
        }

        /// <summary>
        /// Modifies the spa det additional.
        /// </summary>
        /// <param name="spaDetailAdditional">The spa detail additional.</param>
        /// <returns>SpaDetailAdditional.</returns>
        public SpaDetailAdditional ModifySpaDetAdditional(SpaDetailAdditional spaDetailAdditional)
        {
            _spaDetailAdditionalOperations.Update(spaDetailAdditional.Id, spaDetailAdditional);
            return spaDetailAdditional;
        }

        /// <summary>
        /// Deletes the spadet additional.
        /// </summary>
        /// <param name="spaDetAdditionalId">The spa det additional identifier.</param>
        public void DeleteSpadetAdditional(int spaDetAdditionalId)
        {
            _spaDetailAdditionalOperations.Delete(spaDetAdditionalId);
        }

        //Spa Additional Groups
        /// <summary>
        /// Gets the spa additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaAdditionalGroup.</returns>
        public IEnumerable<SpaAdditionalGroup> GetSpaAdditionalGroups(int hotelId, int skip, int pageSize, out int total)
        {
            return _spaAdditionalGroupOperations.GetAllWithServerSidePagging(sag => sag.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the spa additional group by identifier.
        /// </summary>
        /// <param name="spaAddGroupId">The spa add group identifier.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        public SpaAdditionalGroup GetSpaAdditionalGroupById(int spaAddGroupId)
        {
            return _spaAdditionalGroupOperations.GetById(spaAddGroupId);
        }
        /// <summary>
        /// Creates the spa additional group.
        /// </summary>
        /// <param name="spaAddGroup">The spa add group.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        public SpaAdditionalGroup CreateSpaAdditionalGroup(SpaAdditionalGroup spaAddGroup)
        {
            spaAddGroup.CreationDate = DateTime.UtcNow;
            return _spaAdditionalGroupOperations.AddNew(spaAddGroup);
        }
        /// <summary>
        /// Modifies the spa additional group.
        /// </summary>
        /// <param name="spaAddGroup">The spa add group.</param>
        /// <returns>SpaAdditionalGroup.</returns>
        public SpaAdditionalGroup ModifySpaAdditionalGroup(SpaAdditionalGroup spaAddGroup)
        {
            _spaAdditionalGroupOperations.Update(spaAddGroup.Id, spaAddGroup);
            return spaAddGroup;
        }


        /// <summary>
        /// Deletes the spa additional group.
        /// </summary>
        /// <param name="spaAddGrpId">The spa add GRP identifier.</param>
        public void DeleteSpaAdditionalGroup(int spaAddGrpId)
        {
            _spaAdditionalGroupOperations.Delete(spaAddGrpId);
        }

        //Spa Additional elements
        /// <summary>
        /// Gets the spa additional elements.
        /// </summary>
        /// <param name="spaAddGrpId">The spa add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaAdditionalElement.</returns>
        public IEnumerable<SpaAdditionalElement> GetSpaAdditionalElements(int spaAddGrpId, int skip, int pageSize, out int total)
        {
            return _spaAdditionalElementOperations.GetAllWithServerSidePagging(e => e.SpaAdditionalGroupId == spaAddGrpId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the spa additional element by identifier.
        /// </summary>
        /// <param name="spaAddElementId">The spa add element identifier.</param>
        /// <returns>SpaAdditionalElement.</returns>
        public SpaAdditionalElement GetSpaAdditionalElementById(int spaAddElementId)
        {
            return _spaAdditionalElementOperations.GetById(spaAddElementId);
        }
        /// <summary>
        /// Creates the spa additional element.
        /// </summary>
        /// <param name="spaAddElement">The spa add element.</param>
        /// <returns>SpaAdditionalElement.</returns>
        public SpaAdditionalElement CreateSpaAdditionalElement(SpaAdditionalElement spaAddElement)
        {
            spaAddElement.CreationDate = DateTime.UtcNow;
            return _spaAdditionalElementOperations.AddNew(spaAddElement);
        }
        /// <summary>
        /// Modifies the spa additional element.
        /// </summary>
        /// <param name="spaAddElement">The spa add element.</param>
        /// <returns>SpaAdditionalElement.</returns>
        public SpaAdditionalElement ModifySpaAdditionalElement(SpaAdditionalElement spaAddElement)
        {
            _spaAdditionalElementOperations.Update(spaAddElement.Id, spaAddElement);
            return spaAddElement;
        }


        /// <summary>
        /// Deletes the spa additional element.
        /// </summary>
        /// <param name="spaAddElementId">The spa add element identifier.</param>
        public void DeleteSpaAdditionalElement(int spaAddElementId)
        {
            _spaAdditionalElementOperations.Delete(spaAddElementId);
        }
        ////hotel spa service reviews

        /// <summary>
        /// Gets the spa service reviews by hotel identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        public IEnumerable<SpaServiceReview> GetSpaServiceReviewsByHotelId(int id, int pageNumber, int pageSize, out int total)
        {
            return _spaServiceReviewOperation.GetAllByDesc(s => s.SpaServiceDetail.SpaService.HotelServices.HotelId == id, o => o.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Gets the hotel spa service reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        public IEnumerable<SpaServiceReview> GetHotelSpaServiceReviews(int id, int pageNumber, int pageSize, out int total)
        {
            var query = _spaServiceReviewRepository.Table;
            query = query.Where(s => s.SpaServiceDetailId == id);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaServiceReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets all hotel spa service reviews.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        public IEnumerable<SpaServiceReview> GetAllHotelSpaServiceReviews(int hotelId, int skip, int pageSize, out int total)
        {
            return _spaServiceReviewOperation.GetAllWithServerSidePaggingDesc(r => r.SpaServiceDetail.SpaService.HotelServices.HotelId == hotelId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the customer spa service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceReview.</returns>
        public IEnumerable<SpaServiceReview> GetCustomerSpaServiceReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            var query = _spaServiceReviewRepository.Table;
            query = query.Where(s => s.CustomerId == customerId);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaServiceReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Creates the hotel spa service review.
        /// </summary>
        /// <param name="hotelSpaServiceReviews">The hotel spa service reviews.</param>
        /// <returns>SpaServiceReview.</returns>
        /// <exception cref="ArgumentException">SpaServiceDetailId doesn't exists</exception>
        public SpaServiceReview CreateHotelSpaServiceReview(SpaServiceReview hotelSpaServiceReviews)
        {
            var SPAServiceDetail = _spaServiceDetailsOperations.GetById(hotelSpaServiceReviews.SpaServiceDetailId);
            if (SPAServiceDetail == null)
                throw new ArgumentException("SpaServiceDetailId doesn't exists");
            hotelSpaServiceReviews.CreationDate = DateTime.UtcNow;

            return _spaServiceReviewOperation.AddNew(hotelSpaServiceReviews);
        }

        /// <summary>
        /// Gets the hotel spa service review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaServiceReview.</returns>
        public SpaServiceReview GetHotelSpaServiceReviewById(int id)
        {
            return _spaServiceReviewOperation.GetById(rw => rw.Id == id);
        }

        /// <summary>
        /// Deletes the hotel spa service review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotelSpaServiceReview(int id)
        {
            _spaServiceReviewOperation.Delete(id);
        }

        //public IEnumerable<SPAServiceDetail> GetPendingSPAServiceReviews(int customerId, int pageNumber, int pageSize, out int total)
        //{
        //    var custReviwed = (from item in _hotelSPAServiceReviewRepository.Table
        //                       where (item.CustomerId == customerId)
        //                       select item.SPAServiceDetailId).ToArray();

        //    var custSpaServiceBooked = _spaServiceBookingDetailOperations.GetAllByDesc(o => o.SPAServiceBooking.CustomerId == customerId && !custReviwed.Contains(o.SPAServiceDetailId), s => s.SPAServiceBookingId, pageNumber, pageSize, out total).Select(o => o.SPAServiceDetail).Distinct();
        //    return custSpaServiceBooked;
        //}
        #endregion

        #region Spa Suggestion
        /// <summary>
        /// Gets the spa service detail suggestions.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetSpaServiceDetailSuggestions(int spaServiceDetailId, int pageNumber, int pageSize, out int total)
        {
            var spaServiceSuggestion = _spaSuggestionOperations.GetAll(i => i.SpaServiceDetailId == spaServiceDetailId && i.SpaServiceDetail1.IsActive, i => i.Id, pageNumber, pageSize, out total).Select(i => i.SpaServiceDetail1);
            return spaServiceSuggestion;
        }

        //WEB
        /// <summary>
        /// Gets the spa service det suggestions.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaSuggestion.</returns>
        public IEnumerable<SpaSuggestion> GetSpaServiceDetSuggestions(int spaServiceDetId, int skip, int pageSize, out int total)
        {
            return _spaSuggestionOperations.GetAllWithServerSidePagging(i => i.SpaServiceDetailId == spaServiceDetId, i => i.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the unassign spa service det suggetions.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetUnassignSpaServiceDetSuggetions(int spaServiceDetId, int hotelId)
        {
            var assignSpaServiceDetIds = _spaSuggestionOperations.GetAll(s => s.SpaServiceDetailId == spaServiceDetId).Select(s => s.SpaServiceDetailSuggestionId).Distinct();
            var unassignedSpaServiceDet = _spaServiceDetailsRepository.Table.Where(r => r.Id != spaServiceDetId && !assignSpaServiceDetIds.Contains(r.Id) && r.SpaService.HotelServices.HotelId == hotelId && r.IsActive == true);
            return unassignedSpaServiceDet;
        }

        /// <summary>
        /// Creates the spa service detail suggestion.
        /// </summary>
        /// <param name="spaServiceDetSuggestion">The spa service det suggestion.</param>
        public void CreateSpaServiceDetailSuggestion(List<SpaSuggestion> spaServiceDetSuggestion)
        {
            foreach (var eachspaServDetSuggestion in spaServiceDetSuggestion)
            {
                _spaSuggestionOperations.AddNew(eachspaServDetSuggestion);
            }
        }

        /// <summary>
        /// Deletes the spa service det suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaServiceDetSuggestion(int id)
        {
            _spaSuggestionOperations.Delete(id);
        }
        #endregion Spa Suggestion

        #region Hotel Spa Service Detail Ingredients
        /// <summary>
        /// Gets the spa service detail ingredients.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceDetailIngredient.</returns>
        public IEnumerable<SpaServiceDetailIngredient> GetSpaServiceDetailIngredients(int spaServiceDetailId, int skip, int pageSize, out int total)
        {
            return _spaServiceDetailIngredientsOperations.GetAllWithServerSidePagging(sp => sp.SpaServiceDetailId == spaServiceDetailId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the spa service detail ingredient by identifier.
        /// </summary>
        /// <param name="spaServiceDetIngrId">The spa service det ingr identifier.</param>
        /// <returns>SpaServiceDetailIngredient.</returns>
        public SpaServiceDetailIngredient GetSpaServiceDetailIngredientById(int spaServiceDetIngrId)
        {
            return _spaServiceDetailIngredientsOperations.GetById(spaServiceDetIngrId);
        }

        /// <summary>
        /// Gets the unassign spa ingrdeients.
        /// </summary>
        /// <param name="spaServDetId">The spa serv det identifier.</param>
        /// <returns>IEnumerable SpaIngredient.</returns>
        public IEnumerable<SpaIngredient> GetUnassignSpaIngrdeients(int spaServDetId)
        {
            var assignSpaIngrIds = _spaServiceDetailIngredientsOperations.GetAll(s => s.SpaServiceDetailId == spaServDetId).Select(sd => sd.SpaIngredientsId).Distinct();
            var unassignedSpaIngredients = _spaIngredientRepository.Table.Where(s => !assignSpaIngrIds.Contains(s.Id));
            return unassignedSpaIngredients;
        }

        /// <summary>
        /// Creates the spa service detail ingredient.
        /// </summary>
        /// <param name="spaServiceDetailIngr">The spa service detail ingr.</param>
        public void CreateSpaServiceDetailIngredient(List<SpaServiceDetailIngredient> spaServiceDetailIngr)
        {
            foreach (var eachSpaSerDetIngr in spaServiceDetailIngr)
            {
                _spaServiceDetailIngredientsOperations.AddNew(eachSpaSerDetIngr);
            }
        }
        /// <summary>
        /// Deletes the spa service detail ingredient.
        /// </summary>
        /// <param name="spaServiceDetIngrId">The spa service det ingr identifier.</param>
        public void DeleteSpaServiceDetailIngredient(int spaServiceDetIngrId)
        {
            _spaServiceDetailIngredientsOperations.Delete(spaServiceDetIngrId);
        }
        #endregion Hotel Spa Service Detail Ingredients

        #region Spa Detail Extratime
        //App
        /// <summary>
        /// Gets the spa service dets extratimes.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraTime.</returns>
        public IEnumerable<ExtraTime> GetSpaServiceDetsExtratimes(int spaServiceDetailId, int pageNumber, int pageSize, out int total)
        {
            return _extraTimeOperations.GetAll(e => e.SpaServiceDetailId == spaServiceDetailId && e.IsActive, e => e.Id, pageNumber, pageSize, out total);
        }

        //Web
        /// <summary>
        /// Gets the spa service det extratimes.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraTime.</returns>
        public IEnumerable<ExtraTime> GetSpaServiceDetExtratimes(int spaServiceDetId, int skip, int pageSize, out int total)
        {
            return _extraTimeOperations.GetAllWithServerSidePagging(i => i.SpaServiceDetailId == spaServiceDetId, i => i.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the spa detail extratime by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ExtraTime.</returns>
        public ExtraTime GetSpaDetailExtratimeById(int Id)
        {
            return _extraTimeOperations.GetById(Id);
        }

        /// <summary>
        /// Creates the spa service detail extratime.
        /// </summary>
        /// <param name="extraTime">The extra time.</param>
        /// <returns>ExtraTime.</returns>
        public ExtraTime CreateSpaServiceDetailExtratime(ExtraTime extraTime)
        {
            extraTime.CreationDate = DateTime.Now;
            return _extraTimeOperations.AddNew(extraTime);
        }
        /// <summary>
        /// Modifies the spa service detail extratime.
        /// </summary>
        /// <param name="extraTime">The extra time.</param>
        /// <returns>ExtraTime.</returns>
        public ExtraTime ModifySpaServiceDetailExtratime(ExtraTime extraTime)
        {
            _extraTimeOperations.Update(extraTime.Id, extraTime);
            return extraTime;
        }
        /// <summary>
        /// Deletes the extra time.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExtraTime(int id)
        {
            _extraTimeOperations.Delete(id);
        }

        #endregion Spa Detail Extratime

        #region Spa Detail Extraprocedure
        //App
        /// <summary>
        /// Gets the spa service dets extraprocedures.
        /// </summary>
        /// <param name="spaServiceDetailId">The spa service detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraProcedure.</returns>
        public IEnumerable<ExtraProcedure> GetSpaServiceDetsExtraprocedures(int spaServiceDetailId, int pageNumber, int pageSize, out int total)
        {
            return _extraProcedureOperations.GetAll(i => i.SpaServiceDetailId == spaServiceDetailId && i.IsActive, i => i.Id, pageNumber, pageSize, out total);
        }
        //Web
        /// <summary>
        /// Gets the spa service det extraprocedure.
        /// </summary>
        /// <param name="spaServiceDetId">The spa service det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExtraProcedure.</returns>
        public IEnumerable<ExtraProcedure> GetSpaServiceDetExtraprocedure(int spaServiceDetId, int skip, int pageSize, out int total)
        {
            return _extraProcedureOperations.GetAllWithServerSidePagging(i => i.SpaServiceDetailId == spaServiceDetId, i => i.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the spa detail extraprocedure by identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>ExtraProcedure.</returns>
        public ExtraProcedure GetSpaDetailExtraprocedureById(int Id)
        {
            return _extraProcedureOperations.GetById(Id);
        }
        /// <summary>
        /// Creates the spa service detail extraprocedure.
        /// </summary>
        /// <param name="extraProcedure">The extra procedure.</param>
        /// <returns>ExtraProcedure.</returns>
        public ExtraProcedure CreateSpaServiceDetailExtraprocedure(ExtraProcedure extraProcedure)
        {
            extraProcedure.CreationDate = DateTime.Now;
            return _extraProcedureOperations.AddNew(extraProcedure);
        }
        /// <summary>
        /// Modifies the spa service detail extraprocedure.
        /// </summary>
        /// <param name="extraProcedure">The extra procedure.</param>
        /// <returns>ExtraProcedure.</returns>
        public ExtraProcedure ModifySpaServiceDetailExtraprocedure(ExtraProcedure extraProcedure)
        {
            _extraProcedureOperations.Update(extraProcedure.Id, extraProcedure);
            return extraProcedure;
        }
        /// <summary>
        /// Deletes the extra procedure.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExtraProcedure(int id)
        {
            _extraProcedureOperations.Delete(id);
        }

        #endregion Spa Detail Extraprocedure

        #region Spa Service Offer
        //API
        /// <summary>
        /// Gets the spa service detail offer by spa service identifier.
        /// </summary>
        /// <param name="spaServiceId">The spa service identifier.</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        public IEnumerable<SpaServiceOffer> GetSpaServiceDetailOfferBySpaServiceId(int spaServiceId)
        {
            return _spaServiceOfferOperations.GetAll(spa => spa.SpaServiceDetail.SpaServiceId == spaServiceId && spa.IsActive);
        }
        /// <summary>
        /// Gets the hotel spa service detail offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        public IEnumerable<SpaServiceOffer> GetHotelSpaServiceDetailOffer(int hotelId, int customerId, bool considerMyPreferences = false)
        {
            if (considerMyPreferences)
            {
                System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var custPrefSpaServiceDetailsIds = _spaServiceOfferRepository.ExecuteStoredProcedureList<SpaServiceDetail>("exec uspGetCustomerPreferredSpaServiceDetails", customerIdParameter).Select(s => s.Id).ToList(); ;

                return _spaServiceOfferOperations.GetAll(spa => spa.SpaServiceDetail.SpaService.HotelServices.HotelId == hotelId && spa.IsActive && custPrefSpaServiceDetailsIds.Contains(spa.SpaServiceDetailId));
            }
            else
            {
                return _spaServiceOfferOperations.GetAll(spa => spa.SpaServiceDetail.SpaService.HotelServices.HotelId == hotelId && spa.IsActive);
            }

        }
        //WEB
        /// <summary>
        /// Gets the spa service offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaServiceOffer.</returns>
        public IEnumerable<SpaServiceOffer> GetSpaServiceOffer(int hotelId, int skip, int pageSize, out int total)
        {
            return _spaServiceOfferOperations.GetAllWithServerSidePagging(o => o.SpaServiceDetail.SpaService.HotelServices.HotelId == hotelId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the hotel unassigned offer spa service details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetHotelUnassignedOfferSpaServiceDetails(int hotelId)
        {
            var assignedSpaServiceDetailOffer = _spaServiceOfferOperations.GetAll(o => o.SpaServiceDetail.SpaService.HotelServices.HotelId == hotelId).Select(o => o.SpaServiceDetailId).ToList();
            return _spaServiceDetailsOperations.GetAll(s => s.SpaService.HotelServices.HotelId == hotelId && s.IsActive && !assignedSpaServiceDetailOffer.Contains(s.Id));
        }

        /// <summary>
        /// Gets the discounted spa service offer by identifier.
        /// </summary>
        /// <param name="spaServiceOfferId">The spa service offer identifier.</param>
        /// <returns>SpaServiceOffer.</returns>
        public SpaServiceOffer GetDiscountedSpaServiceOfferById(int spaServiceOfferId)
        {
            return _spaServiceOfferOperations.GetById(spaServiceOfferId);
        }
        /// <summary>
        /// Creates the spa service offer.
        /// </summary>
        /// <param name="spaServiceOffer">The spa service offer.</param>
        /// <returns>SpaServiceOffer.</returns>
        public SpaServiceOffer CreateSpaServiceOffer(SpaServiceOffer spaServiceOffer)
        {
            return _spaServiceOfferOperations.AddNew(spaServiceOffer);
        }
        /// <summary>
        /// Modifies the spa service offer.
        /// </summary>
        /// <param name="spaServiceOffer">The spa service offer.</param>
        /// <returns>SpaServiceOffer.</returns>
        public SpaServiceOffer ModifySpaServiceOffer(SpaServiceOffer spaServiceOffer)
        {
            _spaServiceOfferOperations.Update(spaServiceOffer.Id, spaServiceOffer);
            return spaServiceOffer;
        }
        /// <summary>
        /// Deletes the spa service offer.
        /// </summary>
        /// <param name="spaServiceOfferid">The spa service offerid.</param>
        public void DeleteSpaServiceOffer(int spaServiceOfferid)
        {
            _spaServiceOfferOperations.Delete(spaServiceOfferid);
        }
        #endregion Spa Service Offer

        #region Spa Ingredient Categories
        /// <summary>
        /// Gets all spa ingredient categories.
        /// </summary>
        /// <returns>IEnumerable SpaIngredientCategory.</returns>
        public IEnumerable<SpaIngredientCategory> GetAllSpaIngredientCategories()
        {
            return _spaIngredientCategoryOperations.GetAll(i => i.IsActive);
        }
        /// <summary>
        /// Gets all spa ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaIngredientCategory.</returns>
        public IEnumerable<SpaIngredientCategory> GetAllSpaIngredientCategories(int skip, int pageSize, out int total)
        {
            return _spaIngredientCategoryOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the spa ingredient category by identifier.
        /// </summary>
        /// <param name="spaIngredientCatId">The spa ingredient cat identifier.</param>
        /// <returns>SpaIngredientCategory.</returns>
        public SpaIngredientCategory GetSpaIngredientCategoryById(int spaIngredientCatId)
        {
            return _spaIngredientCategoryOperations.GetById(spaIngredientCatId);
        }
        /// <summary>
        /// Creates the spa ingredient category.
        /// </summary>
        /// <param name="spaIngredientCategory">The spa ingredient category.</param>
        /// <returns>SpaIngredientCategory.</returns>
        public SpaIngredientCategory CreateSpaIngredientCategory(SpaIngredientCategory spaIngredientCategory)
        {
            spaIngredientCategory.CreationDate = DateTime.Now;
            return _spaIngredientCategoryOperations.AddNew(spaIngredientCategory);
        }
        /// <summary>
        /// Modifies the spa ingredient category.
        /// </summary>
        /// <param name="spaIngredientCategory">The spa ingredient category.</param>
        /// <returns>SpaIngredientCategory.</returns>
        public SpaIngredientCategory ModifySpaIngredientCategory(SpaIngredientCategory spaIngredientCategory)
        {
            _spaIngredientCategoryOperations.Update(spaIngredientCategory.Id, spaIngredientCategory);
            return spaIngredientCategory;
        }

        /// <summary>
        /// Deletes the spa ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaIngredientCategory(int id)
        {
            _spaIngredientCategoryOperations.Delete(id);
        }
        #endregion Spa Ingredient Categories

        #region Spa Ingredients
        /// <summary>
        /// Gets the spa ingredient by spa ing cat identifier.
        /// </summary>
        /// <param name="spaIngCatId">The spa ing cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaIngredient.</returns>
        public IEnumerable<SpaIngredient> GetSpaIngredientBySpaIngCatId(int spaIngCatId, int skip, int pageSize, out int total)
        {
            return _spaIngredientOperations.GetAllWithServerSidePagging(si => si.SpaIngredientCategoryId == spaIngCatId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the spa ingredient by identifier.
        /// </summary>
        /// <param name="spaIngredientId">The spa ingredient identifier.</param>
        /// <returns>SpaIngredient.</returns>
        public SpaIngredient GetSpaIngredientById(int spaIngredientId)
        {
            return _spaIngredientOperations.GetById(spaIngredientId);
        }
        /// <summary>
        /// Creates the spa ingredient.
        /// </summary>
        /// <param name="spaIngredient">The spa ingredient.</param>
        /// <returns>SpaIngredient.</returns>
        public SpaIngredient CreateSpaIngredient(SpaIngredient spaIngredient)
        {
            return _spaIngredientOperations.AddNew(spaIngredient);
        }
        /// <summary>
        /// Modifies the spa ingredient.
        /// </summary>
        /// <param name="spaIngredient">The spa ingredient.</param>
        /// <returns>SpaIngredient.</returns>
        public SpaIngredient ModifySpaIngredient(SpaIngredient spaIngredient)
        {
            _spaIngredientOperations.Update(spaIngredient.Id, spaIngredient);
            return spaIngredient;
        }
        /// <summary>
        /// Deletes the spa ingredient.
        /// </summary>
        /// <param name="spaIngredientId">The spa ingredient identifier.</param>
        public void DeleteSpaIngredient(int spaIngredientId)
        {
            _spaIngredientOperations.Delete(spaIngredientId);
        }
        #endregion Spa Ingredients

        #region Spa Cart

        /// <summary>
        /// Gets the customer spa cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>SpaCart.</returns>
        public SpaCart GetCustomerSpaCart(int customerId, int hotelId)
        {
            var cart = _spaCartOperations.GetAll(c => c.CustomerId == customerId && c.HotelId == hotelId).FirstOrDefault();
            return cart;
        }
        /// <summary>
        /// Creates the spa cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>SpaCart.</returns>
        /// <exception cref="ArgumentException">
        /// End date time shouldn't be less than start date time
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// INVALID_SPA_EMPLOYEE_DETAIL_ID
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// INVALID_EXTRA_TIME_ID
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId
        /// or
        /// NO_ROOM_AVAILABLE_FOR _THIS_TIME_SLOT
        /// or
        /// THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW
        /// </exception>
        /// <exception cref="CartExistsException">SPA_CART_ALREADY_EXISTS</exception>
        public SpaCart CreateSpaCart(SpaCart cart, bool flush = false)
        {
            int hotelId = 0;
            if (cart.SpaCartItems != null && cart.SpaCartItems.Any())
            {
                foreach (var item in cart.SpaCartItems)
                {
                    if (item.EndDateTime < item.StartDateTime)
                        throw new ArgumentException("End date time shouldn't be less than start date time");

                    var spaServiceDetail = _spaServiceDetailsOperations.GetById(i => i.Id == item.SpaServiceDetailId);
                    if (spaServiceDetail == null)
                        throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");

                    var SpaEmployeeDetails = _spaEmployeeDetailsOperation.GetById(i => i.Id == item.SpaEmployeeDetailsId);
                    if (SpaEmployeeDetails == null)
                        throw new ArgumentException("INVALID_SPA_EMPLOYEE_DETAIL_ID");

                    var hotelID = spaServiceDetail.SpaService.HotelServices.HotelId;
                    if (hotelId == 0)
                        hotelId = hotelID;

                    if (hotelId != hotelID)
                        throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");
                }
            }

            var exitingSpaCart = _spaCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.HotelId == hotelId).FirstOrDefault();
            if (exitingSpaCart != null)
            {
                if (flush)
                    _spaCartRepository.Delete(exitingSpaCart.Id);
                else
                    throw new CartExistsException("SPA_CART_ALREADY_EXISTS");
            }

            // verify additionals are correctly specified for each item
            if (cart.SpaCartItems != null && cart.SpaCartItems.Any())
            {
                double result = 0.00f;
                foreach (var item in cart.SpaCartItems)
                {
                    var spaServiceDetails = _spaServiceDetailsOperations.GetById(item.SpaServiceDetailId);
                    if (spaServiceDetails == null)
                        throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");

                    //Check for SpaService Details Offer if Exist
                    double? offerPrice = 0;
                    if (spaServiceDetails.SpaServiceOffers != null && spaServiceDetails.SpaServiceOffers.Count() >= 1)
                    {
                        double? percentage = spaServiceDetails.SpaServiceOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                        if (percentage > 0 && percentage != null)
                            offerPrice = ((spaServiceDetails.Price * percentage) / 100) * item.Qty;
                    }
                    item.OfferPrice = offerPrice;
                    item.Total = item.Qty * (spaServiceDetails.Price == null ? 0 : spaServiceDetails.Price) - offerPrice;


                    // Add Extra time price in Item total
                    if (item.ExtraTimeId != null)
                    {
                        var ExtraTime = _extraTimeOperations.GetById(i => i.Id == item.ExtraTimeId);
                        if (ExtraTime == null)
                            throw new ArgumentException("INVALID_EXTRA_TIME_ID");
                        else
                            item.Total += ExtraTime.Price;
                    }

                    // check for cart additional
                    var additionals = spaServiceDetails.SpaDetailAdditional;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.SpaAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if ((!addGroup.MinSelected.HasValue || addGroup.MinSelected == 0) && (!addGroup.MaxSelected.HasValue || addGroup.MaxSelected == 0))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;
                            if (item.SpaCartItemsAdditionals != null && item.SpaCartItemsAdditionals.Any())
                                foreach (var cItemAdditional in item.SpaCartItemsAdditionals)
                                {
                                    if (addGroup.SpaAdditionalElement.Any(i => i.Id == cItemAdditional.SpaAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > cartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < cartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId);
                        }
                    }

                    double additionalResult = 0.00f;
                    foreach (var additionalElement in item.SpaCartItemsAdditionals)
                    {
                        var details = _spaAdditionalElementRepository.GetById(additionalElement.SpaAdditionalElementId);
                        if (details != null)
                            additionalElement.Total = (details.Price == null ? 0 : details.Price) * additionalElement.Qty;
                        additionalResult += Convert.ToDouble(additionalElement.Total);
                    }
                    item.Total += additionalResult;


                    // Assign available Room to Customer
                    var spaRoomDetails = _spaRoomDetailsOperations.GetAll(r => r.SpaServiceDetailId == item.SpaServiceDetailId && r.SpaRoom.IsActive == true);
                    if (spaRoomDetails != null && spaRoomDetails.Count() >= 1)
                    {
                        foreach (var room in spaRoomDetails)
                        {
                            var roomAvailable = _spaOrderItemsOperation.GetAll(t => t.SpaRoomId == room.SpaRoomId &&
                                                                                ((item.StartDateTime >= t.StartDateTime && item.EndDateTime <= t.EndDateTime) ||
                                            (item.StartDateTime <= t.StartDateTime && item.EndDateTime >= t.StartDateTime && item.EndDateTime <= t.EndDateTime) ||
                                            (item.StartDateTime >= t.StartDateTime && item.StartDateTime <= t.EndDateTime && item.EndDateTime >= t.EndDateTime) ||
                                            (item.StartDateTime <= t.StartDateTime && item.EndDateTime >= t.EndDateTime)));
                            if (roomAvailable != null && roomAvailable.Count() >= 1)
                                continue;
                            else
                            {
                                item.SpaRoomId = room.SpaRoomId;
                                break;
                            }
                            throw new ArgumentException("NO_ROOM_AVAILABLE_FOR _THIS_TIME_SLOT");
                        }
                    }
                    else
                    {
                        throw new ArgumentException("THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW");
                    }

                    // Add Extra Procedure to SpaCartExtraProcedure
                    double extraProcedureTotal = 0.00f;
                    foreach (var extraProcedure in item.SpaCartExtraProcedures)
                    {
                        var details = _extraProcedureRepository.GetById(extraProcedure.ExtraprocedureId);
                        if (details != null)
                            extraProcedureTotal += Convert.ToDouble(details.Price);
                    }

                    item.Total += extraProcedureTotal;
                    result += Convert.ToDouble(item.Total);
                    cart.HotelId = spaServiceDetails.SpaService.HotelServices.HotelId;
                }
                cart.Total = result;
            }

            cart.CreationDate = DateTime.UtcNow;
            _spaCartRepository.InsertWithChild(cart); // unable to save child and populate navigation properties, so work around
            cart.Customer = _customerRepository.GetById(cart.CustomerId);

            foreach (var item in cart.SpaCartItems)
            {
                foreach (var additional in item.SpaCartItemsAdditionals)
                {
                    additional.SpaAdditionalElement = _spaAdditionalElementRepository.GetById(additional.SpaAdditionalElementId);
                }
                item.SpaEmployeeDetail = _spaEmployeeDetailsRepository.GetById(item.SpaEmployeeDetailsId);
                item.ExtraTime = _extraTimeRepository.GetById(item.ExtraTimeId);

                foreach (var exProcedure in item.SpaCartExtraProcedures)
                {
                    exProcedure.ExtraProcedure = _extraProcedureRepository.GetById(exProcedure.ExtraprocedureId);
                }

                item.SpaServiceDetail = _spaServiceDetailsRepository.Table
                    .Include(i => i.SpaDetailAdditional.Select(ad => ad.SpaAdditionalGroup))
                    .Include(i => i.SpaEmployeeDetails.Select(em => em.SpaEmployee))
                    .Where(i => i.Id == item.SpaServiceDetailId)
                    .FirstOrDefault();
            }
            cart.SpaCartItems = cart.SpaCartItems.OrderBy(c => c.ServeLevel).ToList();
            return cart;
        }

        /// <summary>
        /// Updates the spa cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>SpaCart.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_ID
        /// or
        /// End datetime shouldn't be less than start date time
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// INVALID_SPA_EMPLOYEE_DETAIL_ID
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId
        /// or
        /// INVALID_SPA_SERVICE_DETAIL_ID
        /// or
        /// Start datetime should be greater than current date time.
        /// or
        /// End datetime should be greater than current date time.
        /// or
        /// INVALID_EXTRA_TIME_ID
        /// or
        /// INVALID_SPA_EMPLOYEE_TIME_SLOT_OR_ALREADY_BOOKED
        /// or
        /// THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW
        /// or
        /// Start datetime should be greater than current date time.
        /// or
        /// End datetime should be greater than current date time.
        /// or
        /// INVALID_EXTRA_TIME_ID
        /// or
        /// INVALID_SPA_EMPLOYEE_TIME_SLOT_OR_ALREADY_BOOKED
        /// or
        /// THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW
        /// </exception>
        /// <exception cref="ArgumentNullException">INVALID_CART_DETAILS</exception>
        /// <exception cref="InvalidOperationException">CART_NOT_FOUND</exception>
        /// <exception cref="ZeroItemQtyException">ZERO_ITEM_QTY</exception>
        /// <exception cref="NegativeItemQtyException">NEGATIVE_ITEM_QTY</exception>
        public SpaCart UpdateSpaCart(int customerId, SpaCart cart)
        {
            if (customerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            if (cart == null)
                throw new ArgumentNullException("INVALID_CART_DETAILS");

            int hotelId = 0;
            if (cart.SpaCartItems != null && cart.SpaCartItems.Any())
            {
                foreach (var item in cart.SpaCartItems)
                {
                    if (item.EndDateTime < item.StartDateTime)
                        throw new ArgumentException("End datetime shouldn't be less than start date time");

                    var spaServiceDetail = _spaServiceDetailsOperations.GetById(i => i.Id == item.SpaServiceDetailId);
                    if (spaServiceDetail == null)
                        throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");

                    var SpaEmployeeDetails = _spaEmployeeDetailsOperation.GetById(i => i.Id == item.SpaEmployeeDetailsId);
                    if (SpaEmployeeDetails == null)
                        throw new ArgumentException("INVALID_SPA_EMPLOYEE_DETAIL_ID");

                    var hotelID = spaServiceDetail.SpaService.HotelServices.HotelId;
                    if (hotelId == 0)
                        hotelId = hotelID;

                    if (hotelId != hotelID)
                        throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");

                    var spaDetailAdditional = spaServiceDetail.SpaDetailAdditional;
                    if (spaDetailAdditional != null && spaDetailAdditional.Any())
                    {
                        var spaAdditional_groups = spaDetailAdditional.Select(ad => ad.SpaAdditionalGroup);
                        foreach (var addGroup in spaAdditional_groups)
                        {
                            if (!addGroup.MinSelected.HasValue && (!addGroup.MaxSelected.HasValue))
                                continue;  //no range to check, so ignore this group

                            int spaCartAdditionalsCount = 0;
                            if (item.SpaCartItemsAdditionals != null && item.SpaCartItemsAdditionals.Any())
                                foreach (var cItemAdditional in item.SpaCartItemsAdditionals)
                                {
                                    if (addGroup.SpaAdditionalElement.Any(i => i.Id == cItemAdditional.SpaAdditionalElementId))
                                    {
                                        spaCartAdditionalsCount++;
                                    }
                                }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > spaCartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < spaCartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR_SPASERVICE_DETAILS_ID " + item.SpaServiceDetailId);
                        }
                    }
                }
            }
            var exitingSpaCart = _spaCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.HotelId == hotelId).FirstOrDefault();
            if (exitingSpaCart == null)
                throw new InvalidOperationException("CART_NOT_FOUND");

            foreach (var spaCartItem in cart.SpaCartItems)
            {
                var spaServiceDetails = _spaServiceDetailsOperations.GetById(spaCartItem.SpaServiceDetailId);
                if (spaServiceDetails == null)
                    throw new ArgumentException("INVALID_SPA_SERVICE_DETAIL_ID");

                SpaCartItem existItem;
                existItem = exitingSpaCart.SpaCartItems.Where(i => i.Id == spaCartItem.Id && i.SpaServiceDetailId == spaCartItem.SpaServiceDetailId).FirstOrDefault();

                if (existItem == null)
                {
                    if (spaCartItem.Qty <= 0 || spaCartItem.Qty == null)
                    {
                        throw new ZeroItemQtyException("ZERO_ITEM_QTY");
                    }
                    else
                    {
                        if (spaCartItem.StartDateTime < DateTime.Now)
                            throw new ArgumentException("Start datetime should be greater than current date time.");

                        if (spaCartItem.EndDateTime < DateTime.Now)
                            throw new ArgumentException("End datetime should be greater than current date time.");

                        spaCartItem.ServeLevel = spaCartItem.ServeLevel != null ? spaCartItem.ServeLevel : 1;

                        //Check for Offer if SpaService Details Exist
                        double? offerPrice = 0;
                        if (spaServiceDetails.SpaServiceOffers != null && spaServiceDetails.SpaServiceOffers.Count() >= 1)
                        {
                            double? percentage = spaServiceDetails.SpaServiceOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                            if (percentage > 0 && percentage != null)
                                offerPrice = ((spaServiceDetails.Price * percentage) / 100) * spaCartItem.Qty;
                        }
                        spaCartItem.OfferPrice = offerPrice;
                        spaCartItem.Total = spaCartItem.Qty * (spaServiceDetails.Price == null ? 0 : spaServiceDetails.Price) - offerPrice;

                        // Add Extra time price in Item total
                        if (spaCartItem.ExtraTimeId != null)
                        {
                            var ExtraTime = _extraTimeOperations.GetById(i => i.Id == spaCartItem.ExtraTimeId);
                            if (ExtraTime == null)
                                throw new ArgumentException("INVALID_EXTRA_TIME_ID");
                            else
                                spaCartItem.Total += ExtraTime.Price;
                        }

                        // check for cart additional
                        double additionalResult = 0.00f;
                        foreach (var additionalElement in spaCartItem.SpaCartItemsAdditionals)
                        {
                            var details = _spaAdditionalElementRepository.GetById(additionalElement.SpaAdditionalElementId);
                            if (details != null)
                                additionalElement.Total = (details.Price == null ? 0 : details.Price) * additionalElement.Qty;
                            additionalResult += Convert.ToDouble(additionalElement.Total);
                        }
                        spaCartItem.Total += additionalResult;


                        // Assign available Room to Customer
                        var spaRoomDetails = _spaRoomDetailsOperations.GetAll(r => r.SpaServiceDetailId == spaCartItem.SpaServiceDetailId && r.SpaRoom.IsActive == true);
                        if (spaRoomDetails != null && spaRoomDetails.Count() >= 1)
                        {
                            foreach (var room in spaRoomDetails)
                            {
                                var roomAvailable = _spaOrderItemsOperation.GetAll(t => t.SpaRoomId == room.SpaRoomId &&
                                                                                    ((spaCartItem.StartDateTime >= t.StartDateTime && spaCartItem.EndDateTime <= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime <= t.StartDateTime && spaCartItem.EndDateTime >= t.StartDateTime && spaCartItem.EndDateTime <= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime >= t.StartDateTime && spaCartItem.StartDateTime <= t.EndDateTime && spaCartItem.EndDateTime >= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime <= t.StartDateTime && spaCartItem.EndDateTime >= t.EndDateTime)));
                                if (roomAvailable != null && roomAvailable.Count() >= 1)
                                    continue;
                                else
                                {
                                    spaCartItem.SpaRoomId = room.SpaRoomId;
                                    break;
                                }
                            }
                            if (spaCartItem.SpaRoomId == 0)
                            {
                                throw new ArgumentException("INVALID_SPA_EMPLOYEE_TIME_SLOT_OR_ALREADY_BOOKED");
                            }
                        }
                        else
                        {
                            throw new ArgumentException("THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW");
                        }

                        // Add Extra Procedure to SpaCartExtraProcedure
                        double extraProcedureTotal = 0.00f;
                        foreach (var extraProcedure in spaCartItem.SpaCartExtraProcedures)
                        {
                            var details = _extraProcedureRepository.GetById(extraProcedure.ExtraprocedureId);
                            if (details != null)
                                extraProcedureTotal += Convert.ToDouble(details.Price);
                        }

                        spaCartItem.Total += extraProcedureTotal;

                        exitingSpaCart.Total += spaCartItem.Total;
                        exitingSpaCart.SpaCartItems.Add(spaCartItem);
                    }
                }
                //existItem not NULL
                else
                {
                    if (spaCartItem.Qty <= 0) //If existItem Quantity set to 0 then delete
                    {
                        if (spaCartItem.Qty == 0 && spaCartItem.Id == existItem.Id)
                        {
                            if (exitingSpaCart.SpaCartItems.Count() == 1)
                                exitingSpaCart.Total = 0;
                            else
                                exitingSpaCart.Total -= existItem.Total;
                            _spaCartItemsRepository.Delete(existItem.Id);
                        }
                        else
                        {
                            throw new NegativeItemQtyException("NEGATIVE_ITEM_QTY");
                        }
                    }
                    else
                    {
                        if (existItem.StartDateTime != spaCartItem.StartDateTime && spaCartItem.StartDateTime < DateTime.Now)
                            throw new ArgumentException("Start datetime should be greater than current date time.");

                        if (existItem.EndDateTime != spaCartItem.EndDateTime && spaCartItem.EndDateTime < DateTime.Now)
                            throw new ArgumentException("End datetime should be greater than current date time.");

                        existItem.Qty = spaCartItem.Qty;
                        existItem.ServeLevel = spaCartItem.ServeLevel;
                        existItem.Comment = spaCartItem.Comment;
                        existItem.StartDateTime = spaCartItem.StartDateTime;
                        existItem.EndDateTime = spaCartItem.EndDateTime;
                        existItem.SpaEmployeeDetailsId = spaCartItem.SpaEmployeeDetailsId;

                        exitingSpaCart.Total -= existItem.Total;

                        //Check for Offer if Exist
                        double? offerPrice = 0;
                        if (spaServiceDetails.SpaServiceOffers != null && spaServiceDetails.SpaServiceOffers.Count() >= 1)
                        {
                            double? percentage = spaServiceDetails.SpaServiceOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                            if (percentage > 0 && percentage != null)
                                offerPrice = ((spaServiceDetails.Price * percentage) / 100) * spaCartItem.Qty;
                        }
                        spaCartItem.OfferPrice = offerPrice;
                        existItem.Total = spaCartItem.Qty * (spaServiceDetails.Price == null ? 0 : spaServiceDetails.Price) - offerPrice;

                        // Add Extra time price in Item total
                        if (spaCartItem.ExtraTimeId != null)
                        {
                            var ExtraTime = _extraTimeOperations.GetById(i => i.Id == spaCartItem.ExtraTimeId);
                            if (ExtraTime == null)
                                throw new ArgumentException("INVALID_EXTRA_TIME_ID");
                            else
                                existItem.Total += ExtraTime.Price;
                        }
                        existItem.ExtraTimeId = spaCartItem.ExtraTimeId;

                        // check for cart additional
                        double additionalResult = 0.00f;
                        foreach (var additionalItem in spaCartItem.SpaCartItemsAdditionals)
                        {
                            var details = _spaAdditionalElementRepository.GetById(additionalItem.SpaAdditionalElementId);
                            if (details != null)
                                additionalItem.Total = (details.Price == null ? 0 : details.Price) * additionalItem.Qty;
                            additionalResult += Convert.ToDouble(additionalItem.Total);

                            foreach (var exitAdditionalItem in existItem.SpaCartItemsAdditionals)
                            {
                                if (exitAdditionalItem.SpaAdditionalElementId == additionalItem.SpaAdditionalElementId)
                                {
                                    exitAdditionalItem.Qty = additionalItem.Qty;
                                    exitAdditionalItem.Total = additionalItem.Total;
                                }
                            }
                        }
                        existItem.Total += additionalResult;

                        var newAdd = spaCartItem.SpaCartItemsAdditionals
                        .Where(i => !existItem.SpaCartItemsAdditionals.Any(old => old.SpaAdditionalElementId == i.SpaAdditionalElementId));

                        var delAdd = existItem.SpaCartItemsAdditionals
                            .Where(old => !spaCartItem.SpaCartItemsAdditionals.Any(nw => nw.SpaAdditionalElementId == old.SpaAdditionalElementId));

                        foreach (var del in delAdd)
                        {
                            _spaCartItemsAdditionalRepository.Delete(del.Id);
                        }

                        foreach (var add in newAdd)
                        {
                            existItem.SpaCartItemsAdditionals.Add(new SpaCartItemsAdditional { SpaAdditionalElementId = add.SpaAdditionalElementId, Qty = add.Qty, Total = add.Total });
                        }


                        // Assign available Room to Customer
                        var spaRoomDetails = _spaRoomDetailsOperations.GetAll(r => r.SpaServiceDetailId == spaCartItem.SpaServiceDetailId && r.SpaRoom.IsActive == true);
                        if (spaRoomDetails != null && spaRoomDetails.Count() >= 1)
                        {
                            foreach (var room in spaRoomDetails)
                            {
                                var roomAvailable = _spaOrderItemsOperation.GetAll(t => t.SpaRoomId == room.SpaRoomId &&
                                                                                    ((spaCartItem.StartDateTime >= t.StartDateTime && spaCartItem.EndDateTime <= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime <= t.StartDateTime && spaCartItem.EndDateTime >= t.StartDateTime && spaCartItem.EndDateTime <= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime >= t.StartDateTime && spaCartItem.StartDateTime <= t.EndDateTime && spaCartItem.EndDateTime >= t.EndDateTime) ||
                                                (spaCartItem.StartDateTime <= t.StartDateTime && spaCartItem.EndDateTime >= t.EndDateTime)));
                                if (roomAvailable != null && roomAvailable.Count() >= 1)
                                    continue;
                                else
                                {
                                    existItem.SpaRoomId = room.SpaRoomId;
                                    break;
                                }
                            }
                            if (existItem.SpaRoomId == 0)
                            {
                                throw new ArgumentException("INVALID_SPA_EMPLOYEE_TIME_SLOT_OR_ALREADY_BOOKED");
                            }
                        }
                        else
                        {
                            throw new ArgumentException("THERE_IS_NO_ACTIVE_ROOM_AVAILABLE_FOR_NOW");
                        }

                        // Update Extra Procedure to SpaCartExtraProcedure
                        double extraProcedureTotal = 0.00f;
                        foreach (var extraProcedure in spaCartItem.SpaCartExtraProcedures)
                        {
                            var details = _extraProcedureRepository.GetById(extraProcedure.ExtraprocedureId);
                            if (details != null)
                                extraProcedureTotal += Convert.ToDouble(details.Price);
                        }

                        existItem.Total += extraProcedureTotal;
                        exitingSpaCart.Total += existItem.Total;

                        var newExtraPro = spaCartItem.SpaCartExtraProcedures
                       .Where(i => !existItem.SpaCartExtraProcedures.Any(old => old.ExtraprocedureId == i.ExtraprocedureId));

                        var delExtraPro = existItem.SpaCartExtraProcedures
                            .Where(old => !spaCartItem.SpaCartExtraProcedures.Any(nw => nw.ExtraprocedureId == old.ExtraprocedureId));

                        foreach (var del in delExtraPro)
                        {
                            _spaCartExtraProcedureRepository.Delete(del.Id);
                        }

                        foreach (var add in newExtraPro)
                        {
                            existItem.SpaCartExtraProcedures.Add(new SpaCartExtraProcedure { ExtraprocedureId = add.ExtraprocedureId });
                        }
                    }
                }
            }

            _spaCartOperations.Update(exitingSpaCart);

            var newRepo = new EfRepository<SpaCart>();
            var customerCart = newRepo.GetById(exitingSpaCart.Id);

            customerCart.SpaCartItems = customerCart.SpaCartItems.OrderBy(c => c.ServeLevel).ToList();
            return customerCart;
        }

        #endregion Spa Cart

        #region Spa Order

        /// <summary>
        /// Gets the spa order.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        public IEnumerable<SpaOrder> GetSpaOrder(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _spaOrderRepository.Table.Where(o => o.HotelId == hotelId && o.OrderStatus < 2);

            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the spa orders details by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>SpaOrder.</returns>
        public SpaOrder GetSpaOrdersDetailsById(int orderId)
        {
            return _spaOrderOperation.GetById(orderId);
        }
        /// <summary>
        /// Gets the spa order.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        public IEnumerable<SpaOrder> GetSpaOrder(int hotelId)
        {
            return _spaOrderRepository.Table.Where(o => o.HotelId == hotelId && o.OrderStatus < 2);
        }

        /// <summary>
        /// Creates the spa order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>SpaOrder.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_Id
        /// or
        /// INVALID_CARD_Id
        /// or
        /// Start datetime should be greater than current datetime for " + item.SpaServiceDetail.Name
        /// or
        /// End datetime should be greater than current datetime for " + item.SpaServiceDetail.Name
        /// </exception>
        /// <exception cref="CartEmptyException">CART_IS_EMPTY</exception>
        public SpaOrder CreateSpaOrderFromCart(int customerId, int cardId, int hotelId)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_Id");

            var spacart = _spaCartOperations.GetById(c => c.CustomerId == customerId && c.HotelId == hotelId);

            if (spacart == null || !spacart.SpaCartItems.Any())
                throw new CartEmptyException("CART_IS_EMPTY");

            var card = _customerCreditCardRepository.GetById(cardId);
            if (card == null || card.CustomerId != customerId)
                throw new ArgumentException("INVALID_CARD_Id");

            SpaOrder order = new SpaOrder
            {
                CustomerId = customerId,
                HotelId = spacart.HotelId,
                CardId = cardId,
                OrderStatus = 0,
                PaymentStatus = false,
                OrderTotal = spacart.Total,
                CreationDate = DateTime.UtcNow
            };

            foreach (var item in spacart.SpaCartItems)
            {
                if (item.StartDateTime < DateTime.Now)
                    throw new ArgumentException("Start datetime should be greater than current datetime for " + item.SpaServiceDetail.Name);

                if (item.EndDateTime < DateTime.Now)
                    throw new ArgumentException("End datetime should be greater than current datetime for " + item.SpaServiceDetail.Name);

                SpaOrderItem spaOrderItem = new SpaOrderItem
                {
                    SpaServiceDetailId = item.SpaServiceDetailId,
                    Qty = item.Qty,
                    ServeLevel = item.ServeLevel,
                    Comment = item.Comment,
                    Total = item.Total,
                    StartDateTime = Convert.ToDateTime(item.StartDateTime),
                    EndDateTime = Convert.ToDateTime(item.EndDateTime),
                    SpaEmployeeDetailsId = item.SpaEmployeeDetailsId,
                    SpaRoomId = item.SpaRoomId,
                    ExtraTimeId = item.ExtraTimeId,
                    OfferPrice = item.OfferPrice,
                    DeliveryStatus = false,
                    IsActive = true
                };

                if (item.SpaCartItemsAdditionals != null && item.SpaCartItemsAdditionals.Any())
                {
                    foreach (var add in item.SpaCartItemsAdditionals)
                    {
                        SpaOrderItemsAdditional additional = new SpaOrderItemsAdditional()
                        {
                            SpaAdditionalElementId = add.SpaAdditionalElementId,
                            Qty = add.Qty,
                            Total = add.Total
                        };
                        spaOrderItem.SpaOrderItemsAdditionals.Add(additional);
                    }
                }

                // check for SpaOrderExtraProcedure
                if (item.SpaCartExtraProcedures != null && item.SpaCartExtraProcedures.Any())
                {
                    foreach (var extraProcedure in item.SpaCartExtraProcedures)
                    {
                        SpaOrderExtraProcedure procedure = new SpaOrderExtraProcedure()
                        {
                            ExtraProcedureId = extraProcedure.ExtraprocedureId
                        };
                        spaOrderItem.SpaOrderExtraProcedures.Add(procedure);
                    }
                }
                order.SpaOrderItems.Add(spaOrderItem);
            }
            _spaOrderRepository.InsertWithChild(order);
            _spaCartRepository.Delete(spacart.Id);
            EfRepository<SpaOrder> newRepo = new EfRepository<SpaOrder>();

            var customerSpaOrder = newRepo.GetById(order.Id);
            customerSpaOrder.SpaOrderItems = customerSpaOrder.SpaOrderItems.OrderBy(c => c.ServeLevel).ToList();
            return customerSpaOrder;
        }
        /// <summary>
        /// Gets the spa order is late status changed count.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>System.Int32.</returns>
        public int GetSpaOrderIsLateStatusChangedCount(int hotelId)
        {
            var orderStatuses = new int[] { 0, 1 };
            return _spaOrderOperation.GetAll(o => o.HotelId == hotelId && o.IsLate == true && orderStatuses.Contains(o.OrderStatus.Value)).ToList().Count();
        }
        //public SpaOrder ScheduleSpaOrder(int customerId, int orderId, DateTime scheduleDate)
        //{
        //    if (customerId == 0)
        //        throw new ArgumentException("INVALID_CUSTOMER_Id");
        //    var spaOrder = _spaOrderOperation.GetAll(c => c.CustomerId == customerId && c.Id == orderId).FirstOrDefault();
        //    if (spaOrder == null)
        //    {
        //        throw new OrderNotFoundException("SPA_ORDER_NOT_FOUND");
        //    }

        //    int hotelServiceId = spaOrder.SpaOrderItems.Select(r => r.SpaServiceDetail.SpaService.HotelServiceId).FirstOrDefault();
        //    var hotelService = _hotelServicesOperations.GetById(s => s.Id == hotelServiceId);

        //    TimeSpan? openingTime = hotelService.OpeningHours;
        //    TimeSpan? closingTime = hotelService.ClosingHours;

        //    if (scheduleDate < DateTime.Now)
        //    {
        //        throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
        //    }
        //    else if (scheduleDate.TimeOfDay < openingTime && scheduleDate.TimeOfDay > closingTime)
        //    {
        //        throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
        //    }
        //    else
        //    {
        //        foreach (var spaItem in spaOrder.SpaOrderItems)
        //        {
        //            if (spaItem.SpaOrderItemsEmployee.Count() > 0)
        //            {
        //                var alreadyBooked = _spaOrderItemsEmployeeOperation.GetAll(e => spaItem.SpaOrderItemsEmployee.Any(i => i.SpaEmployeeDetailsId == e.SpaEmployeeDetailsId) && e.SpaOrderItems.SpaOrderId != spaItem.SpaOrderId);
        //                if (alreadyBooked != null && alreadyBooked.Count() > 0)
        //                    throw new InvalidScheduleDateTimeException("MASSEUR_IS_ALREADY_BOOKED_FOR_THAT_TIME_SLOT");
        //            }
        //        }
        //    }

        //    //spaOrder.ScheduledDate = scheduleDate;
        //    _spaOrderRepository.Update(spaOrder);
        //    EfRepository<SpaOrder> newRepo = new EfRepository<SpaOrder>();

        //    var customerSpaOrder = newRepo.GetById(spaOrder.Id);
        //    customerSpaOrder.SpaOrderItems = customerSpaOrder.SpaOrderItems.OrderBy(c => c.ServeLevel).ToList();
        //    return customerSpaOrder;
        //}
        /// <summary>
        /// Updates the spa order status.
        /// </summary>
        /// <param name="spaOrder">The spa order.</param>
        /// <returns>SpaOrder.</returns>
        public SpaOrder UpdateSpaOrderStatus(SpaOrder spaOrder)
        {
            _spaOrderOperation.Update(spaOrder.Id, spaOrder);
            return spaOrder;
        }

        /// <summary>
        /// Creates the spa order review.
        /// </summary>
        /// <param name="spaOrdReview">The spa ord review.</param>
        /// <returns>SpaOrderReview.</returns>
        public SpaOrderReview CreateSpaOrderReview(SpaOrderReview spaOrdReview)
        {
            spaOrdReview.CreationDate = DateTime.UtcNow;
            return _spaOrderReviewOperations.AddNew(spaOrdReview);
        }

        /// <summary>
        /// Gets the spa order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable SpaOrderReview.</returns>
        public IEnumerable<SpaOrderReview> GetSpaOrderReviews(int id)
        {
            return _spaOrderReviewOperations.GetAll(s => s.SpaOrderId == id).OrderByDescending(o => o.Id);
        }

        /// <summary>
        /// Gets the spa order details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable SpaOrderItem.</returns>
        public IEnumerable<SpaOrderItem> GetSpaOrderDetails(int id)
        {
            return _spaOrderItemsOperation.GetAll(s => s.SpaOrderId == id);
        }

        /// <summary>
        /// Gets the customer active spa orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveSpaOrdersAndBaskets.</returns>
        public CustomerActiveSpaOrdersAndBaskets GetCustomerActiveSpaOrders(int customerId)
        {
            var activeOrderStatuses = new int[] { 0, 1 };
            var activeSpaOrders = _spaOrderOperation.GetAll(o => o.CustomerId == customerId && activeOrderStatuses.Contains(o.OrderStatus.Value)).ToList();
            var activeSpaBaskets = _spaCartOperations.GetAll(c => c.CustomerId == customerId && c.Total > 0).ToList();
            CustomerActiveSpaOrdersAndBaskets activeSpaOrdersAndBaskets = new CustomerActiveSpaOrdersAndBaskets()
            {
                ActiveSpaOrders = activeSpaOrders,
                ActiveSpaBaskets = activeSpaBaskets
            };
            return activeSpaOrdersAndBaskets;
        }

        /// <summary>
        /// Updates the spa order item deliver status.
        /// </summary>
        /// <param name="spaOrderItemId">The spa order item identifier.</param>
        public void UpdateSpaOrderItemDeliverStatus(int spaOrderItemId)
        {
            var spaOrderItem = _spaOrderItemsOperation.GetById(spaOrderItemId);
            if (spaOrderItem.DeliveryStatus == true)
                spaOrderItem.DeliveryStatus = false;
            else
                spaOrderItem.DeliveryStatus = true;
            _spaOrderItemsOperation.Update(spaOrderItem);
        }

        /// <summary>
        /// Gets the spa order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        public IEnumerable<SpaOrder> GetSpaOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total)
        {
            var query = _spaOrderRepository.Table;
            if (hotelId > 0)
            {
                query = query.Where(o => o.HotelId == hotelId);
            }
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.OrderTotal >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.OrderTotal <= maxAmount);
            }
            var pages = query.OrderByDescending(o => o.Id)
          .Skip(skip)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }



        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaOrder.</returns>
        public IEnumerable<SpaOrder> GetCustomerSpaOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total)
        {
            var query = _spaOrderRepository.Table;
            if (month != 0)
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year && o.CreationDate.Month == month);
            else
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<SpaOrder>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        #endregion Spa Order

        #region Hotel Laundry

        /// <summary>
        /// Gets the hotel laundry service by identifier.
        /// </summary>
        /// <param name="Laundry_Service_ID">The laundry service identifier.</param>
        /// <returns>Laundry.</returns>
        public Laundry GetHotelLaundryServiceByID(int Laundry_Service_ID)
        {
            return _hotelLaundryOperations.GetById(Laundry_Service_ID);
        }

        /// <summary>
        /// Gets the hotel laundry services.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Laundry.</returns>
        public IEnumerable<Laundry> GetHotelLaundryServices(int HotelId)
        {
            return _hotelLaundryOperations.GetAll(l => l.HotelServices.HotelId == HotelId && l.IsActive);
        }

        /// <summary>
        /// Creates the hotel laundry service.
        /// </summary>
        /// <param name="laundryService">The laundry service.</param>
        /// <returns>Laundry.</returns>
        public Laundry CreateHotelLaundryService(Laundry laundryService)
        {
            laundryService.CreationDate = DateTime.UtcNow;
            return _hotelLaundryOperations.AddNew(laundryService);
        }
        /// <summary>
        /// Gets all hotel laundry services.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Laundry.</returns>
        public IEnumerable<Laundry> GetAllHotelLaundryServices(int HotelId, int hotelServiceId)
        {
            return _hotelLaundryOperations.GetAll(l => l.HotelServices.HotelId == HotelId && l.HotelServiceId == hotelServiceId);
        }
        /// <summary>
        /// Gets the laundry service details by identifier.
        /// </summary>
        /// <param name="ServiceId">The service identifier.</param>
        /// <returns>Laundry.</returns>
        public Laundry GetLaundryServiceDetailsById(int ServiceId)
        {
            return _hotelLaundryOperations.GetById(s => s.Id == ServiceId && s.IsActive);
        }

        /// <summary>
        /// Modifies the laundry service.
        /// </summary>
        /// <param name="Laundry_Service">The laundry service.</param>
        /// <returns>Laundry.</returns>
        public Laundry ModifyLaundryService(Laundry Laundry_Service)
        {
            _hotelLaundryOperations.Update(Laundry_Service.Id, Laundry_Service);
            return Laundry_Service;
        }
        /// <summary>
        /// Deletes the laundry service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteLaundryService(int id)
        {
            _hotelLaundryOperations.Delete(id);
        }


        /// <summary>
        /// Gets the hotel laundry details.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable LaundryDetail.</returns>
        public IEnumerable<LaundryDetail> GetHotelLaundryDetails(int laundryId)
        {
            return _hotelLaundryDetailsOperations.GetAll(h => h.LaundryId == laundryId);
        }

        /// <summary>
        /// Gets the hotel laundry details by laundry identifier.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable LaundryDetail.</returns>
        public IEnumerable<LaundryDetail> GetHotelLaundryDetailsByLaundryId(int laundryId)
        {
            return _hotelLaundryDetailsOperations.GetAll(h => h.LaundryId == laundryId && h.IsActive);
        }
        /// <summary>
        /// Creates the hotel laundry details.
        /// </summary>
        /// <param name="Laundry_Details">The laundry details.</param>
        /// <returns>LaundryDetail.</returns>
        public LaundryDetail CreateHotelLaundryDetails(LaundryDetail Laundry_Details)
        {
            return _hotelLaundryDetailsOperations.AddNew(Laundry_Details);
        }
        /// <summary>
        /// Modifies the hotel laundry details.
        /// </summary>
        /// <param name="Laundry_Details">The laundry details.</param>
        /// <returns>LaundryDetail.</returns>
        public LaundryDetail ModifyHotelLaundryDetails(LaundryDetail Laundry_Details)
        {
            _hotelLaundryDetailsOperations.Update(Laundry_Details.Id, Laundry_Details);
            return Laundry_Details;
        }
        /// <summary>
        /// Gets the hotel laundry details by identifier.
        /// </summary>
        /// <param name="Laundry_Details_ID">The laundry details identifier.</param>
        /// <returns>LaundryDetail.</returns>
        public LaundryDetail GetHotelLaundryDetailsByID(int Laundry_Details_ID)
        {
            return _hotelLaundryDetailsOperations.GetById(Laundry_Details_ID);
        }
        /// <summary>
        /// Deletes the laundry details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteLaundryDetails(int id)
        {
            _hotelLaundryDetailsOperations.Delete(id);
        }


        //Laundry reviews

        /// <summary>
        /// Gets the laundry reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryReview.</returns>
        public IEnumerable<LaundryReview> GetLaundryReviews(int id, int pageNumber, int pageSize, out int total)
        {
            var laundryReview = _laundryReviewOperations.GetAllByDesc(rw => rw.LaundryId == id, r => r.Id, pageNumber, pageSize, out total);
            return laundryReview;
        }

        /// <summary>
        /// Gets the laundry reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryReview.</returns>
        public IEnumerable<LaundryReview> GetLaundryReviewsByHotel(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _laundryReviewRepository.Table;
            query = query.Where(o => o.Laundry.HotelServices.HotelId == hotelId);
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<LaundryReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Deletes the laundry review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteLaundryReview(int id)
        {
            _laundryReviewOperations.Delete(id);
        }
        /// <summary>
        /// Creates the laundry reviews.
        /// </summary>
        /// <param name="laundryReviews">The laundry reviews.</param>
        /// <returns>LaundryReview.</returns>
        /// <exception cref="ArgumentException">Laundry details with this laundryId doesn't exists</exception>
        public LaundryReview CreateLaundryReviews(LaundryReview laundryReviews)
        {
            var laundry = _hotelLaundryOperations.GetById(laundryReviews.LaundryId);
            if (laundry == null)
                throw new ArgumentException("Laundry details with this laundryId doesn't exists");
            laundryReviews.CreationDate = DateTime.UtcNow;

            return _laundryReviewOperations.AddNew(laundryReviews);
        }

        /// <summary>
        /// Gets the laundry review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>LaundryReview.</returns>
        public LaundryReview GetLaundryReviewById(int id)
        {
            return _laundryReviewOperations.GetById(l => l.Id == id);
        }

        //public IEnumerable<Laundry> GetPendingLaundryServiceReviews(int customerId, int pageNumber, int pageSize, out int total)
        //{
        //    var customerLaundry = _hotelLaundryOperations.GetAll(r => r.LaundryBookings.Any(o => o.CustomerId == customerId) && r.LaundryReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
        //    return customerLaundry;
        //}


        //Laundry Additional Groups
        /// <summary>
        /// Gets the laundry additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryAdditionalGroup.</returns>
        public IEnumerable<LaundryAdditionalGroup> GetLaundryAdditionalGroups(int hotelId, int skip, int pageSize, out int total)
        {
            return _laundryAdditionalGroupsOperations.GetAllWithServerSidePagging(s => s.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the laundry additional group by identifier.
        /// </summary>
        /// <param name="laundryAddGroupId">The laundry add group identifier.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        public LaundryAdditionalGroup GetLaundryAdditionalGroupById(int laundryAddGroupId)
        {
            return _laundryAdditionalGroupsOperations.GetById(laundryAddGroupId);
        }
        /// <summary>
        /// Creates the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGroup">The laundry add group.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        public LaundryAdditionalGroup CreateLaundryAdditionalGroup(LaundryAdditionalGroup laundryAddGroup)
        {
            laundryAddGroup.CreationDate = DateTime.UtcNow;
            return _laundryAdditionalGroupsOperations.AddNew(laundryAddGroup);
        }
        /// <summary>
        /// Modifies the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGroup">The laundry add group.</param>
        /// <returns>LaundryAdditionalGroup.</returns>
        public LaundryAdditionalGroup ModifyLaundryAdditionalGroup(LaundryAdditionalGroup laundryAddGroup)
        {
            _laundryAdditionalGroupsOperations.Update(laundryAddGroup.Id, laundryAddGroup);
            return laundryAddGroup;
        }
        /// <summary>
        /// Deletes the laundry additional group.
        /// </summary>
        /// <param name="laundryAddGrpId">The laundry add GRP identifier.</param>
        public void DeleteLaundryAdditionalGroup(int laundryAddGrpId)
        {
            _laundryAdditionalGroupsOperations.Delete(laundryAddGrpId);
        }

        //Laundry Additional elements
        /// <summary>
        /// Gets the laundry additional elements.
        /// </summary>
        /// <param name="laundryAddGrpId">The laundry add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryAdditionalElement.</returns>
        public IEnumerable<LaundryAdditionalElement> GetLaundryAdditionalElements(int laundryAddGrpId, int skip, int pageSize, out int total)
        {
            return _laundryAdditionalElementOperations.GetAllWithServerSidePagging(e => e.LaundryAdditionalGroupId == laundryAddGrpId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the laundry additional element by identifier.
        /// </summary>
        /// <param name="laundryAddElementId">The laundry add element identifier.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        public LaundryAdditionalElement GetLaundryAdditionalElementById(int laundryAddElementId)
        {
            return _laundryAdditionalElementOperations.GetById(laundryAddElementId);
        }
        /// <summary>
        /// Creates the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElement">The laundry add element.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        public LaundryAdditionalElement CreateLaundryAdditionalElement(LaundryAdditionalElement laundryAddElement)
        {
            laundryAddElement.CreationDate = DateTime.UtcNow;
            return _laundryAdditionalElementOperations.AddNew(laundryAddElement);
        }
        /// <summary>
        /// Modifies the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElement">The laundry add element.</param>
        /// <returns>LaundryAdditionalElement.</returns>
        public LaundryAdditionalElement ModifyLaundryAdditionalElement(LaundryAdditionalElement laundryAddElement)
        {
            _laundryAdditionalElementOperations.Update(laundryAddElement.Id, laundryAddElement);
            return laundryAddElement;
        }
        /// <summary>
        /// Deletes the laundry additional element.
        /// </summary>
        /// <param name="laundryAddElementId">The laundry add element identifier.</param>
        public void DeleteLaundryAdditionalElement(int laundryAddElementId)
        {
            _laundryAdditionalElementOperations.Delete(laundryAddElementId);
        }

        //Laundry detail additionals
        /// <summary>
        /// Gets all laundry det additionals.
        /// </summary>
        /// <param name="laundryDetailsId">The laundry details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryDetailAdditional.</returns>
        public IEnumerable<LaundryDetailAdditional> GetAllLaundryDetAdditionals(int laundryDetailsId, int skip, int pageSize, out int total)
        {
            return _laundryDetailAdditionalOperations.GetAllWithServerSidePagging(r => r.LaundryDetailId == laundryDetailsId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the laundrydet additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>LaundryDetailAdditional.</returns>
        public LaundryDetailAdditional GetLaundrydetAdditionalById(int id)
        {
            return _laundryDetailAdditionalOperations.GetById(id);
        }
        /// <summary>
        /// Modifies the laundry det additional.
        /// </summary>
        /// <param name="laundryDetAdd">The laundry det add.</param>
        /// <returns>LaundryDetailAdditional.</returns>
        public LaundryDetailAdditional ModifyLaundryDetAdditional(LaundryDetailAdditional laundryDetAdd)
        {
            _laundryDetailAdditionalOperations.Update(laundryDetAdd.Id, laundryDetAdd);
            return laundryDetAdd;
        }

        /// <summary>
        /// Deletes the laundrydet additional.
        /// </summary>
        /// <param name="laundryDetAdditionalId">The laundry det additional identifier.</param>
        public void DeleteLaundrydetAdditional(int laundryDetAdditionalId)
        {
            _laundryDetailAdditionalOperations.Delete(laundryDetAdditionalId);
        }

        /// <summary>
        /// Gets the unassign laundry add groups.
        /// </summary>
        /// <param name="laundryDetailsId">The laundry details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable LaundryAdditionalGroup.</returns>
        public IEnumerable<LaundryAdditionalGroup> GetUnassignLaundryAddGroups(int laundryDetailsId, int hotelId)
        {
            var assignLAGIds = _laundryDetailAdditionalOperations.GetAll(s => s.LaundryDetailId == laundryDetailsId).Select(sd => sd.LaundryAdditionalGroupId).Distinct();
            var unassignedLaundryAddGroups = _laundryAdditionalGroupsRepository.Table.Where(r => !assignLAGIds.Contains(r.Id) && r.HotelId == hotelId && r.IsActive);
            return unassignedLaundryAddGroups;
        }

        /// <summary>
        /// Creates the laundry det additional.
        /// </summary>
        /// <param name="laundryDetAdditional">The laundry det additional.</param>
        public void CreateLaundryDetAdditional(List<LaundryDetailAdditional> laundryDetAdditional)
        {
            foreach (var eachLaundryDetAdd in laundryDetAdditional)
            {
                eachLaundryDetAdd.CreationDate = DateTime.UtcNow;
                _laundryDetailAdditionalOperations.AddNew(eachLaundryDetAdd);
            }
        }
        #endregion

        #region Laundry Cart

        /// <summary>
        /// Gets the customer laundry cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>LaundryCart.</returns>
        public LaundryCart GetCustomerLaundryCart(int customerId, int hotelId)
        {
            var cart = _laundryCartOperations.GetAll(c => c.CustomerId == customerId && c.Room.HotelId == hotelId).FirstOrDefault();
            return cart;
        }

        /// <summary>
        /// Creates the laundry cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>LaundryCart.</returns>
        /// <exception cref="ArgumentException">
        /// NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID_OR_NOT_ACTIVE
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + laundryCartItem.LaundryDetailId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + laundryCartItem.LaundryDetailId
        /// </exception>
        /// <exception cref="CartExistsException">LAUNDRY_CART_ALREADY_EXISTS</exception>
        public LaundryCart CreateLaundryCart(LaundryCart cart, bool flush = false)
        {
            int hotelId = 0;

            var room = _hotelRoomsOperation.GetById(r => r.Id == cart.RoomId && r.IsActive);
            if (room == null)
                throw new ArgumentException("NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID");
            else
                hotelId = room.HotelId;

            if (cart.LaundryCartItems != null && cart.LaundryCartItems.Any())
            {
                foreach (var laundryItem in cart.LaundryCartItems)
                {
                    if (laundryItem.LaundryDetailId == 0)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    if (laundryItem.ScheduledDate != null && laundryItem.ScheduledDate <= DateTime.Now)
                        throw new ArgumentException(Resources.Invalid_ScheduleDate);

                    var laundryDetail = _hotelLaundryDetailsOperations.GetById(i => i.Id == laundryItem.LaundryDetailId);
                    if (laundryDetail == null)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    if (hotelId != laundryDetail.Laundry.HotelServices.HotelId)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    if (laundryItem.ScheduledDate < DateTime.Now.AddHours(2))
                        throw new ArgumentException(Resources.Min_ScheduleDateGap + laundryDetail.Garments.Name);
                }
            }

            var exitingLaundryCart = _laundryCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.RoomId == room.Id).FirstOrDefault();
            if (exitingLaundryCart != null)
            {
                if (flush)
                {

                    //Deleting images from Folder Path
                    if (exitingLaundryCart.LaundryCartItems.Any(i => i.LaundryCartItemImages.Count() > 0))
                    {
                        foreach (var laundryCartItem in exitingLaundryCart.LaundryCartItems)
                        {
                            foreach (var itemImages in laundryCartItem.LaundryCartItemImages)
                            {
                                var imgPath = "~/Images/Customers/" + exitingLaundryCart.CustomerId + "/Room/" + exitingLaundryCart.RoomId.ToString() + "/Laundry/" + itemImages.LaundryCartItems.LaundryDetailId.ToString() + "/" + itemImages.Image;
                                string imagePath = HttpContext.Current.Server.MapPath(imgPath);
                                if (File.Exists(imagePath))
                                    File.Delete(imagePath);
                            }
                        }
                    }

                    _laundryCartRepository.Delete(exitingLaundryCart.Id);
                }
                else
                    throw new CartExistsException("LAUNDRY_CART_ALREADY_EXISTS");
            }

            // verify additionals are correctly specified for each item
            if (cart.LaundryCartItems != null && cart.LaundryCartItems.Any())
            {
                double result = 0.00f;
                foreach (var laundryCartItem in cart.LaundryCartItems)
                {
                    if (laundryCartItem.Quantity == 0)
                        laundryCartItem.Quantity = 1;

                    var laundryDetail = _hotelLaundryDetailsOperations.GetAll(i => i.Id == laundryCartItem.LaundryDetailId && i.IsActive == true).FirstOrDefault();
                    if (laundryDetail == null)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID_OR_NOT_ACTIVE");

                    laundryCartItem.Total = laundryCartItem.Quantity * (laundryDetail.Price == null ? 0 : laundryDetail.Price);

                    double additionalResult = 0.00f;
                    foreach (var additionalCartItem in laundryCartItem.LaundryCartItemsAdditional)
                    {
                        if (additionalCartItem.Quantity == null || additionalCartItem.Quantity == 0)
                            additionalCartItem.Quantity = 1;

                        var additionalDetails = _laundryAdditionalElementRepository.GetById(additionalCartItem.LaundryAdditionalElementId);
                        if (additionalDetails != null)
                        {
                            additionalCartItem.Total = (additionalDetails.Price == null ? 0 : additionalDetails.Price) * additionalCartItem.Quantity;
                            additionalResult += Convert.ToDouble(additionalCartItem.Total);
                        }
                    }

                    var additionals = laundryDetail.LaundryDetailAdditional;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.LaundryAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if (!addGroup.MinSelected.HasValue && (!addGroup.MaxSelected.HasValue))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (laundryCartItem.LaundryCartItemsAdditional != null && laundryCartItem.LaundryCartItemsAdditional.Any())
                            {
                                foreach (var cItemAdditional in laundryCartItem.LaundryCartItemsAdditional)
                                {
                                    if (addGroup.LaundryAdditionalElements.Any(i => i.Id == cItemAdditional.LaundryAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > cartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + laundryCartItem.LaundryDetailId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < cartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + laundryCartItem.LaundryDetailId);
                        }
                    }
                    laundryCartItem.Total += additionalResult;
                    result += Convert.ToDouble(laundryCartItem.Total);
                    //need to add customer Id
                    var imgPath = "~/Images/Customers/" + cart.CustomerId + "/Room/" + cart.RoomId.ToString() + "/Laundry/" + laundryCartItem.LaundryDetailId.ToString();
                    string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
                    try
                    {
                        string[] files = Directory.GetFiles(FolderPathOnServer, "*.*", SearchOption.AllDirectories);

                        List<LaundryCartItemImages> imageFiles = new List<LaundryCartItemImages>();
                        foreach (string filename in files)
                        {
                            FileInfo info = new FileInfo(filename);
                            var i = new LaundryCartItemImages()
                            {
                                Image = info.Name
                            };
                            imageFiles.Add(i);
                        }

                        laundryCartItem.LaundryCartItemImages = imageFiles;
                    }
                    catch
                    {
                        continue;
                    }
                }
                cart.Total = result;
            }

            cart.CreationDate = DateTime.UtcNow;
            _laundryCartRepository.InsertWithChild(cart); // unable to save child and populate navigation properties, so work around
            cart.Room = _hotelRoomsRepository.GetById(cart.RoomId);

            foreach (var item in cart.LaundryCartItems)
            {
                foreach (var additional in item.LaundryCartItemsAdditional)
                {
                    additional.LaundryAdditionalElement = _laundryAdditionalElementRepository.GetById(additional.LaundryAdditionalElementId);
                }

                item.LaundryDetail = _hotelLaundryDetailsRepository.Table
                    .Include(i => i.LaundryDetailAdditional.Select(ad => ad.LaundryAdditionalGroup))
                    .Where(i => i.Id == item.LaundryDetailId)
                    .FirstOrDefault();
            }
            cart.LaundryCartItems = cart.LaundryCartItems.OrderBy(c => c.ScheduledDate).ToList();
            return cart;

        }

        /// <summary>
        /// Updates the laundry cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>LaundryCart.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_ID
        /// or
        /// NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + item.LaundryDetailId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + item.LaundryDetailId
        /// or
        /// INVALID_LAUNDRY_DETAIL_ID
        /// or
        /// or
        /// or
        /// or
        /// </exception>
        /// <exception cref="ArgumentNullException">INVALID_CART_DETAILS</exception>
        /// <exception cref="InvalidOperationException">CART_NOT_FOUND</exception>
        /// <exception cref="NegativeItemQtyException">NEGATIVE_ITEM_QTY</exception>
        public LaundryCart UpdateLaundryCart(int customerId, LaundryCart cart)
        {
            if (customerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            if (cart == null)
                throw new ArgumentNullException("INVALID_CART_DETAILS");

            int hotelId = 0;

            var room = _hotelRoomsOperation.GetById(r => r.Id == cart.RoomId && r.IsActive);
            if (room == null)
                throw new ArgumentException("NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID");
            else
                hotelId = room.HotelId;

            if (cart.LaundryCartItems != null && cart.LaundryCartItems.Any())
            {
                foreach (var item in cart.LaundryCartItems)
                {
                    if (item.LaundryDetailId == 0)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    var laundryDetail = _hotelLaundryDetailsOperations.GetById(i => i.Id == item.LaundryDetailId);
                    if (laundryDetail == null)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    if (hotelId != laundryDetail.Laundry.HotelServices.HotelId)
                        throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                    //if (item.ScheduledDate != null && item.ScheduledDate < DateTime.Now)
                    //    throw new ArgumentException(Resources.Invalid_ScheduleDate);

                    //if (item.ScheduledDate < DateTime.Now.AddHours(2))
                    //    throw new ArgumentException(Resources.Min_ScheduleDateGap + laundryDetail.Garments.Name);

                    var additionals = laundryDetail.LaundryDetailAdditional;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.LaundryAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if (!addGroup.MinSelected.HasValue && (!addGroup.MaxSelected.HasValue))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (item.LaundryCartItemsAdditional != null && item.LaundryCartItemsAdditional.Any())
                            {
                                foreach (var cItemAdditional in item.LaundryCartItemsAdditional)
                                {
                                    if (addGroup.LaundryAdditionalElements.Any(i => i.Id == cItemAdditional.LaundryAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > cartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + item.LaundryDetailId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < cartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_LAUNDRY_DETAILS_ID " + item.LaundryDetailId);

                        }
                    }
                }
            }
            var exitingLaundryCart = _laundryCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.RoomId == cart.RoomId).LastOrDefault();
            if (exitingLaundryCart == null)
                throw new InvalidOperationException("CART_NOT_FOUND");

            foreach (var laundryCartItem in cart.LaundryCartItems)
            {
                var laundryDetails = _hotelLaundryDetailsOperations.GetById(laundryCartItem.LaundryDetailId);
                if (laundryDetails == null)
                    throw new ArgumentException("INVALID_LAUNDRY_DETAIL_ID");

                LaundryCartItems existItem;
                existItem = exitingLaundryCart.LaundryCartItems.Where(i => i.Id == laundryCartItem.Id && i.LaundryDetailId == laundryCartItem.LaundryDetailId).FirstOrDefault();

                if (existItem == null)
                {
                    if (laundryCartItem.ScheduledDate != null && laundryCartItem.ScheduledDate <= DateTime.Now)
                        throw new ArgumentException(Resources.Invalid_ScheduleDate);

                    if (laundryCartItem.ScheduledDate < DateTime.Now.AddHours(2))
                        throw new ArgumentException(Resources.Min_ScheduleDateGap + laundryDetails.Garments.Name);

                    if (laundryCartItem.Quantity <= 0 || laundryCartItem.Quantity == null)
                    {
                        laundryCartItem.Quantity = 1;
                        if (laundryCartItem.LaundryCartItemsAdditional != null)
                            foreach (var additional in laundryCartItem.LaundryCartItemsAdditional)
                            {
                                if (additional.Quantity == null)
                                    additional.Quantity = 1;
                            }
                    }
                    laundryCartItem.Total = laundryCartItem.Quantity * (laundryDetails.Price == null ? 0 : laundryDetails.Price);
                    // check for cart additional
                    double additionalResult = 0.00f;
                    foreach (var additionalElement in laundryCartItem.LaundryCartItemsAdditional)
                    {
                        if (additionalElement.Quantity == null || additionalElement.Quantity == 0)
                            additionalElement.Quantity = 1;

                        var details = _laundryAdditionalElementRepository.GetById(additionalElement.LaundryAdditionalElementId);
                        if (details != null)
                            additionalElement.Total = (details.Price == null ? 0 : details.Price) * additionalElement.Quantity;
                        additionalResult += Convert.ToDouble(additionalElement.Total);
                    }

                    laundryCartItem.Total += additionalResult;

                    exitingLaundryCart.Total += laundryCartItem.Total;
                    exitingLaundryCart.LaundryCartItems.Add(laundryCartItem);

                    //Add item images from folder
                    var imgPath = "~/Images/Customers/" + cart.CustomerId + "/Room/" + cart.RoomId.ToString() + "/Laundry/" + laundryCartItem.LaundryDetailId.ToString();
                    string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
                    try
                    {
                        string[] files = Directory.GetFiles(FolderPathOnServer, "*.*", SearchOption.AllDirectories);

                        List<LaundryCartItemImages> imageFiles = new List<LaundryCartItemImages>();
                        foreach (string filename in files)
                        {
                            FileInfo info = new FileInfo(filename);
                            var i = new LaundryCartItemImages()
                            {
                                Image = info.Name
                            };
                            imageFiles.Add(i);
                        }
                        laundryCartItem.LaundryCartItemImages = imageFiles;
                    }
                    catch
                    {
                        continue;
                    }
                }
                //existItem not NULL
                else
                {
                    if (laundryCartItem.Quantity <= 0) //If existItem Quantity set to 0 then delete
                    {
                        if (laundryCartItem.Quantity == 0 && laundryCartItem.Id == existItem.Id)
                        {
                            if (exitingLaundryCart.LaundryCartItems.Count() == 1)
                                exitingLaundryCart.Total = 0;
                            else
                                exitingLaundryCart.Total -= existItem.Total;


                            //Deleting images from Folder Path
                            if (existItem.LaundryCartItemImages.Count() > 0)
                            {
                                foreach (var itemImages in existItem.LaundryCartItemImages)
                                {
                                    var imgPath = "~/Images/Customers/" + exitingLaundryCart.CustomerId + "/Room/" + exitingLaundryCart.RoomId.ToString() + "/Laundry/" + itemImages.LaundryCartItems.LaundryDetailId.ToString() + "/" + itemImages.Image;
                                    string imagePath = HttpContext.Current.Server.MapPath(imgPath);
                                    if (File.Exists(imagePath))
                                        File.Delete(imagePath);
                                }
                            }
                            _laundryCartItemsRepository.Delete(existItem.Id);
                        }
                        else
                        {
                            throw new NegativeItemQtyException("NEGATIVE_ITEM_QTY");
                        }
                    }
                    else
                    {
                        if (laundryCartItem.ScheduledDate != null && laundryCartItem.ScheduledDate <= DateTime.Now)
                            throw new ArgumentException(Resources.Invalid_ScheduleDate);

                        if (laundryCartItem.ScheduledDate < DateTime.Now.AddHours(2))
                            throw new ArgumentException(Resources.Min_ScheduleDateGap + laundryDetails.Garments.Name);

                        existItem.Quantity = laundryCartItem.Quantity;
                        existItem.Comment = laundryCartItem.Comment;
                        existItem.ScheduledDate = laundryCartItem.ScheduledDate;

                        exitingLaundryCart.Total -= existItem.Total;
                        existItem.Total = laundryCartItem.Quantity * (laundryDetails.Price == null ? 0 : laundryDetails.Price);

                        // check for cart additional
                        double additionalResult = 0.00f;
                        foreach (var additionalItem in laundryCartItem.LaundryCartItemsAdditional)
                        {
                            if (additionalItem.Quantity == null || additionalItem.Quantity == 0)
                                additionalItem.Quantity = 1;

                            var details = _laundryAdditionalElementRepository.GetById(additionalItem.LaundryAdditionalElementId);
                            if (details != null)
                                additionalItem.Total = (details.Price == null ? 0 : details.Price) * additionalItem.Quantity;
                            additionalResult += Convert.ToDouble(additionalItem.Total);

                            foreach (var exitAdditionalItem in existItem.LaundryCartItemsAdditional)
                            {
                                if (exitAdditionalItem.LaundryAdditionalElementId == additionalItem.LaundryAdditionalElementId)
                                {
                                    exitAdditionalItem.Quantity = additionalItem.Quantity;
                                    exitAdditionalItem.Total = additionalItem.Total;
                                }
                            }
                        }
                        existItem.Total += additionalResult;
                        exitingLaundryCart.Total += existItem.Total;

                        var newAdd = laundryCartItem.LaundryCartItemsAdditional
                        .Where(i => !existItem.LaundryCartItemsAdditional.Any(old => old.LaundryAdditionalElementId == i.LaundryAdditionalElementId));

                        var delAdd = existItem.LaundryCartItemsAdditional
                            .Where(old => !laundryCartItem.LaundryCartItemsAdditional.Any(nw => nw.LaundryAdditionalElementId == old.LaundryAdditionalElementId));

                        foreach (var del in delAdd)
                        {
                            _laundryCartItemsAdditionalRepository.Delete(del.Id);
                        }

                        foreach (var add in newAdd)
                        {
                            existItem.LaundryCartItemsAdditional.Add(new LaundryCartItemsAdditional { LaundryAdditionalElementId = add.LaundryAdditionalElementId, Quantity = add.Quantity, Total = add.Total });
                        }

                        //Add item images from folder
                        var imgPath = "~/Images/Customers/" + cart.CustomerId + "/Room/" + cart.RoomId.ToString() + "/Laundry/" + laundryCartItem.LaundryDetailId.ToString();
                        string FolderPathOnServer = HttpContext.Current.Server.MapPath(imgPath);
                        try
                        {
                            string[] files = Directory.GetFiles(FolderPathOnServer, "*.*", SearchOption.AllDirectories);

                            foreach (string filename in files)
                            {
                                FileInfo info = new FileInfo(filename);
                                var i = new LaundryCartItemImages()
                                {
                                    LaundryCartItemId = existItem.Id,
                                    Image = info.Name
                                };
                                if (!existItem.LaundryCartItemImages.Any(ig => ig.Image == info.Name))
                                    existItem.LaundryCartItemImages.Add(i);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }

            _laundryCartOperations.Update(exitingLaundryCart);

            var newRepo = new EfRepository<LaundryCart>();
            var customerCart = newRepo.GetById(exitingLaundryCart.Id);

            customerCart.LaundryCartItems = customerCart.LaundryCartItems.OrderBy(c => c.ScheduledDate).ToList();
            return customerCart;
        }


        #endregion Laundry Cart

        #region Laundry Order
        /// <summary>
        /// Gets the laundry orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryOrder.</returns>
        public IEnumerable<LaundryOrder> GetLaundryOrdersQueue(int hotelId, int skip, int pageSize, out int total)
        {
            var orderStauses = new int[] { 0, 1 };
            return _laundryOrderOperations.GetAllWithServerSidePaggingDesc(l => l.Room.HotelId == hotelId && orderStauses.Contains(l.OrderStatus.Value), o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the laundry order item details.
        /// </summary>
        /// <param name="laundryOrderId">The laundry order identifier.</param>
        /// <returns>IEnumerable LaundryOrderItems.</returns>
        public IEnumerable<LaundryOrderItems> GetLaundryOrderItemDetails(int laundryOrderId)
        {
            return _laundryOrderItemOperations.GetAll(o => o.LaundryOrderId == laundryOrderId);
        }

        /// <summary>
        /// Updates the laundry order item deliver status.
        /// </summary>
        /// <param name="laundryOrderItemId">The laundry order item identifier.</param>
        public void UpdateLaundryOrderItemDeliverStatus(int laundryOrderItemId)
        {
            var laundryOrderItem = _laundryOrderItemOperations.GetById(laundryOrderItemId);
            if (laundryOrderItem.LaundryDetailOrderStatus == true)
                laundryOrderItem.LaundryDetailOrderStatus = false;
            else
                laundryOrderItem.LaundryDetailOrderStatus = true;
            _laundryOrderItemOperations.Update(laundryOrderItem);
        }
        /// <summary>
        /// Gets the laundry order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>LaundryOrder.</returns>
        public LaundryOrder GetLaundryOrderById(int orderId)
        {
            return _laundryOrderOperations.GetById(l => l.Id == orderId);
        }

        /// <summary>
        /// Creates the laundry order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>LaundryOrder.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_Id
        /// or
        /// INVALID_CARD_Id
        /// or
        /// or
        /// </exception>
        /// <exception cref="CartEmptyException">CART_IS_EMPTY</exception>
        public LaundryOrder CreateLaundryOrderFromCart(int customerId, int cardId, int roomId)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_Id");

            var laundryCart = _laundryCartOperations.GetById(c => c.CustomerId == customerId && c.RoomId == roomId);

            if (laundryCart == null || !laundryCart.LaundryCartItems.Any())
                throw new CartEmptyException("CART_IS_EMPTY");

            var card = _customerCreditCardRepository.GetById(cardId);
            if (card == null || card.CustomerId != customerId)
                throw new ArgumentException("INVALID_CARD_Id");

            LaundryOrder order = new LaundryOrder
            {
                CustomerId = customerId,
                RoomId = laundryCart.RoomId,
                CardId = cardId,
                OrderStatus = 0,
                PaymentStatus = false,
                OrderTotal = laundryCart.Total,
                CreationDate = DateTime.Now
            };

            foreach (var item in laundryCart.LaundryCartItems)
            {
                if (item.ScheduledDate != null && item.ScheduledDate < DateTime.Now)
                    throw new ArgumentException(Resources.Invalid_ScheduleDate);

                if (item.ScheduledDate < DateTime.Now.AddHours(2))
                    throw new ArgumentException(Resources.Min_ScheduleDateGap + item.LaundryDetail.Garments.Name);

                LaundryOrderItems laundryOrderItem = new LaundryOrderItems
                {
                    LaundryDetailId = item.LaundryDetailId,
                    Quantity = item.Quantity,
                    Comment = item.Comment,
                    Total = item.Total,
                    ScheduledDate = item.ScheduledDate == null ? DateTime.Now.AddHours(2) : item.ScheduledDate,
                    LaundryDetailOrderStatus = false,
                    IsActive = true
                };

                if (item.LaundryCartItemsAdditional != null && item.LaundryCartItemsAdditional.Any())
                {
                    foreach (var add in item.LaundryCartItemsAdditional)
                    {
                        LaundryOrderItemsAdditional additional = new LaundryOrderItemsAdditional()
                        {
                            LaundryAdditionalElementId = add.LaundryAdditionalElementId,
                            Qty = add.Quantity,
                            Total = add.Total
                        };
                        laundryOrderItem.LaundryOrderItemsAdditional.Add(additional);
                    }
                }

                //Adding LaundryCartItemImages
                if (item.LaundryCartItemImages != null && item.LaundryCartItemImages.Any())
                {
                    var path = "~/Images/Customers/" + laundryCart.CustomerId + "/Room/" + laundryCart.RoomId.ToString() + "/Laundry/" + item.LaundryDetailId.ToString() + "/";
                    string FolderPathOnServer = HttpContext.Current.Server.MapPath(path);

                    var newPath = "~/Images/Customers/" + laundryCart.CustomerId + "/Room/" + laundryCart.RoomId.ToString() + "/LaundryOrder/" + item.LaundryDetailId.ToString() + "/";
                    string NewPathOnServer = HttpContext.Current.Server.MapPath(newPath);

                    if (!Directory.Exists(NewPathOnServer))
                        Directory.CreateDirectory(NewPathOnServer);

                    foreach (var add in item.LaundryCartItemImages)
                    {
                        if (File.Exists(FolderPathOnServer + add.Image.ToString()))
                        {
                            string newFileName = NewPathOnServer + laundryCart.Id + "_" + add.Image.ToString();
                            File.Move(FolderPathOnServer + add.Image.ToString(), newFileName);
                            LaundryOrderItemImages images = new LaundryOrderItemImages()
                                {
                                    Image = laundryCart.Id + "_" + add.Image.ToString()
                                };
                            laundryOrderItem.LaundryOrderItemImages.Add(images);
                        }
                    }
                }
                order.LaundryOrderItems.Add(laundryOrderItem);
            }
            _laundryOrderRepository.InsertWithChild(order);
            _laundryCartRepository.Delete(laundryCart.Id);

            EfRepository<LaundryOrder> newRepo = new EfRepository<LaundryOrder>();

            var customerLaundryOrder = newRepo.GetById(order.Id);
            customerLaundryOrder.LaundryOrderItems = customerLaundryOrder.LaundryOrderItems.OrderBy(c => c.ScheduledDate).ToList();
            return customerLaundryOrder;
        }

        /// <summary>
        /// Modifies the laundry order.
        /// </summary>
        /// <param name="laundryOrder">The laundry order.</param>
        /// <returns>LaundryOrder.</returns>
        public LaundryOrder ModifyLaundryOrder(LaundryOrder laundryOrder)
        {
            _laundryOrderOperations.Update(laundryOrder.Id, laundryOrder);
            return laundryOrder;
        }
        /// <summary>
        /// Creates the laundry order review.
        /// </summary>
        /// <param name="laundryOrderReview">The laundry order review.</param>
        /// <returns>LaundryOrderReview.</returns>
        public LaundryOrderReview CreateLaundryOrderReview(LaundryOrderReview laundryOrderReview)
        {
            laundryOrderReview.CreationDate = DateTime.UtcNow;
            return _laundryOrderReviewOperations.AddNew(laundryOrderReview);
        }

        /// <summary>
        /// Gets the laundry order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable LaundryOrderReview.</returns>
        public IEnumerable<LaundryOrderReview> GetLaundryOrderReviews(int id)
        {
            return _laundryOrderReviewOperations.GetAll(l => l.LaundryOrderId == id).OrderByDescending(o => o.Id);
        }

        /// <summary>
        /// Gets all active laundry orders of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveLaundryOrdersAndBaskets.</returns>
        public CustomerActiveLaundryOrdersAndBaskets GetAllActiveLaundryOrdersOfCustomer(int customerId)
        {
            var activeOrderStatuses = new int[] { 0, 1 };
            var activeLaundryOrders = _laundryOrderOperations.GetAll(t => t.CustomerId == customerId && activeOrderStatuses.Contains(t.OrderStatus.Value)).ToList();
            var activeLaundryBaskets = _laundryCartOperations.GetAll(c => c.CustomerId == customerId && c.Total > 0).ToList();
            CustomerActiveLaundryOrdersAndBaskets activeLaundryOrdersAndBaskets = new CustomerActiveLaundryOrdersAndBaskets()
            {
                ActiveLaundryOrders = activeLaundryOrders,
                ActiveLaundryBaskets = activeLaundryBaskets
            };
            return activeLaundryOrdersAndBaskets;
        }

        /// <summary>
        /// Gets the laundry order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable LaundryOrder.</returns>
        public IEnumerable<LaundryOrder> GetLaundryOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total)
        {
            var query = _laundryOrderRepository.Table;
            if (hotelId > 0)
            {
                query = query.Where(o => o.Room.HotelId == hotelId);
            }
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.OrderTotal >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.OrderTotal <= maxAmount);
            }
            var pages = query.OrderByDescending(o => o.Id)
          .Skip(skip)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<LaundryOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the customer laundry orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable laundryOrder.</returns>
        public IEnumerable<LaundryOrder> GetCustomerLaundryOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total)
        {
            var query = _laundryOrderRepository.Table;
            if (month != 0)
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year && o.CreationDate.Month == month);
            else
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<LaundryOrder>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        #endregion Laundry Order

        #region Hotel Trip

        /// <summary>
        /// Gets the hotel trips.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Trip.</returns>
        public IEnumerable<Trip> GetHotelTrips(int HotelId)
        {
            var query = _hotelTripsRepository.Table;
            return query.Where(h => h.HotelServices.HotelId == HotelId &&
                                    DbFunctions.TruncateTime(h.TripDate) >= DbFunctions.TruncateTime(DateTime.Now) && h.IsActive).ToList();
        }

        /// <summary>
        /// Gets all hotel trips.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="HotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Trip.</returns>
        public IEnumerable<Trip> GetAllHotelTrips(int HotelId, int HotelServiceId)
        {
            return _hotelTripOperations.GetAll(t => t.HotelServices.HotelId == HotelId && t.HotelServiceId == HotelServiceId);
        }
        /// <summary>
        /// Gets the hotel trip by identifier.
        /// </summary>
        /// <param name="TripId">The trip identifier.</param>
        /// <returns>Trip.</returns>
        public Trip GetHotelTripByID(int TripId)
        {
            return _hotelTripOperations.GetById(t => t.Id == TripId && t.IsActive);
        }

        /// <summary>
        /// Creates the hotel trip.
        /// </summary>
        /// <param name="hotelTrip">The hotel trip.</param>
        /// <returns>Trip.</returns>
        public Trip CreateHotelTrip(Trip hotelTrip)
        {
            hotelTrip.CreationDate = DateTime.UtcNow;
            return _hotelTripOperations.AddNew(hotelTrip);
        }
        /// <summary>
        /// Modifies the hotel trip.
        /// </summary>
        /// <param name="Trip">The trip.</param>
        /// <returns>Trip.</returns>
        public Trip ModifyHotelTrip(Trip Trip)
        {
            _hotelTripOperations.Update(Trip.Id, Trip);
            return Trip;
        }


        /// <summary>
        /// Deletes the trip.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTrip(int id)
        {
            _hotelTripOperations.Delete(id);
        }

        /// <summary>
        /// Gets the hotel trip details.
        /// </summary>
        /// <param name="Trip_ID">The trip identifier.</param>
        /// <returns>IEnumerable TripDetail.</returns>
        public IEnumerable<TripDetail> GetHotelTripDetails(int Trip_ID)
        {
            return _hotelTripDetailsOperations.GetAll(h => h.TripID == Trip_ID);

        }

        /// <summary>
        /// Creates the hotel trip details.
        /// </summary>
        /// <param name="hotel_trip_details">The hotel trip details.</param>
        /// <returns>TripDetail.</returns>
        public TripDetail CreateHotelTripDetails(TripDetail hotel_trip_details)
        {
            return _hotelTripDetailsOperations.AddNew(hotel_trip_details);
        }
        /// <summary>
        /// Modifies the hotel trip details.
        /// </summary>
        /// <param name="Trip_Details">The trip details.</param>
        /// <returns>TripDetail.</returns>
        public TripDetail ModifyHotelTripDetails(TripDetail Trip_Details)
        {
            _hotelTripDetailsOperations.Update(Trip_Details.Id, Trip_Details);
            return Trip_Details;
        }
        /// <summary>
        /// Gets the hotel trip details by identifier.
        /// </summary>
        /// <param name="Trip_Details_ID">The trip details identifier.</param>
        /// <returns>TripDetail.</returns>
        public TripDetail GetHotelTripDetailsByID(int Trip_Details_ID)
        {
            return _hotelTripDetailsOperations.GetById(Trip_Details_ID);
        }
        /// <summary>
        /// Deletes the trip details.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTripDetails(int id)
        {
            _hotelTripDetailsOperations.Delete(id);
        }

        /// <summary>
        /// Gets the trip bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripBooking.</returns>
        public IEnumerable<TripBooking> GetTripBookings(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _hotelTripBookingRepository.Table;
            query = query.Where(o => o.Trip.HotelServices.HotelId == hotelId);
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<TripBooking>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Deletes the trip booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTripBooking(int id)
        {
            _hotelTripBookingOperations.Delete(id);
        }

        /// <summary>
        /// Gets the hotel trip booking by identifier.
        /// </summary>
        /// <param name="booking_id">The booking identifier.</param>
        /// <returns>TripBooking.</returns>
        public TripBooking GetHotelTripBookingByID(int booking_id)
        {
            return _hotelTripBookingOperations.GetById(booking_id);
        }

        /// <summary>
        /// Gets all trip booking of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable TripBooking.</returns>
        public IEnumerable<TripBooking> GetAllTripBookingOfCustomer(int customerId)
        {
            return _hotelTripBookingOperations.GetAll(t => t.CustomerId == customerId);
        }

        /// <summary>
        /// Modifies the hotel trip booking.
        /// </summary>
        /// <param name="tripBooking">The trip booking.</param>
        /// <returns>TripBooking.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_TRIP_ID
        /// or
        /// INVALID_TRIP_BOOKING_ID
        /// or
        /// or
        /// INVALID_TRIP_ID
        /// </exception>
        public TripBooking ModifyHotelTripBooking(TripBooking tripBooking)
        {
            double amount = 0;
            if (tripBooking.TripId <= 0)
                throw new ArgumentException("INVALID_TRIP_ID");

            if (tripBooking.Id <= 0)
                throw new ArgumentException("INVALID_TRIP_BOOKING_ID");

            var existingBooking = _hotelTripBookingOperations.GetById(tb => tb.Id == tripBooking.Id && tb.CustomerId == tripBooking.CustomerId);
            if (existingBooking == null)
                throw new ArgumentException(Resources.TripBooking_NotFound);

            var trip = _hotelTripOperations.GetById(s => s.Id == tripBooking.TripId);
            if (trip == null)
                throw new ArgumentException("INVALID_TRIP_ID");

            amount = trip.Rate * tripBooking.Seats;
            existingBooking.TripId = tripBooking.TripId;
            existingBooking.Seats = tripBooking.Seats;
            existingBooking.Amount = amount;
            _hotelTripBookingOperations.Update(existingBooking);
            return existingBooking;
        }
        /// <summary>
        /// Creates the hotel trip bookings.
        /// </summary>
        /// <param name="tripBooking">The trip booking.</param>
        /// <returns>TripBooking.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_TRIP_ID
        /// or
        /// INVALID_TRIP_ID
        /// </exception>
        public TripBooking CreateHotelTripBookings(TripBooking tripBooking)
        {
            double amount = 0;
            if (tripBooking.TripId <= 0)
                throw new ArgumentException("INVALID_TRIP_ID");

            var trip = _hotelTripOperations.GetById(s => s.Id == tripBooking.TripId);
            if (trip == null)
                throw new ArgumentException("INVALID_TRIP_ID");

            amount = trip.Rate * tripBooking.Seats;
            tripBooking.StatusId = 1;
            tripBooking.CreationDate = DateTime.UtcNow;
            tripBooking.Amount = amount;
            return _hotelTripBookingOperations.AddNew(tripBooking);
        }

        /// <summary>
        /// Gets the trip reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripReview.</returns>
        public IEnumerable<TripReview> GetTripReviews(int id, int pageNumber, int pageSize, out int total)
        {
            var tripReview = _tripReviewOperations.GetAllByDesc(rw => rw.TripId == id, r => r.Id, pageNumber, pageSize, out total);
            return tripReview;
        }

        /// <summary>
        /// Gets the trips reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TripReview.</returns>
        public IEnumerable<TripReview> GetTripsReviewsByHotel(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _tripReviewRepository.Table;
            query = query.Where(o => o.Trip.HotelServices.HotelId == hotelId);
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<TripReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Creates the trip reviews.
        /// </summary>
        /// <param name="tripReviews">The trip reviews.</param>
        /// <returns>TripReview.</returns>
        /// <exception cref="ArgumentException">Trip with this tripId doesn't exists</exception>
        public TripReview CreateTripReviews(TripReview tripReviews)
        {
            var trip = _hotelTripOperations.GetById(tripReviews.TripId);
            if (trip == null)
                throw new ArgumentException("Trip with this tripId doesn't exists");
            tripReviews.CreationDate = DateTime.UtcNow;

            return _tripReviewOperations.AddNew(tripReviews);
        }

        /// <summary>
        /// Gets the trip review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TripReview.</returns>
        public TripReview GetTripReviewById(int id)
        {
            return _tripReviewOperations.GetById(id);
        }
        /// <summary>
        /// Deletes the trip review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTripReview(int id)
        {
            _tripReviewOperations.Delete(id);
        }
        /// <summary>
        /// Gets the pending trip service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Trip.</returns>
        public IEnumerable<Trip> GetPendingTripServiceReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            var customerTrips = _hotelTripOperations.GetAll(r => r.TripBookings.Any(o => o.CustomerId == customerId) && r.TripReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
            return customerTrips;
        }
        #endregion Hotel Trip

        #region Hotel Taxi
        //TaxiService
        /// <summary>
        /// Gets the hotel taxi details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        public IEnumerable<TaxiDetail> GetHotelTaxiDetails(int hotelId)
        {
            return _taxiDetailOperations.GetAll(t => t.HotelServices.HotelId == hotelId && t.IsActive == true);
        }

        /// <summary>
        /// Gets the hotel taxi details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        public IEnumerable<TaxiDetail> GetHotelTaxiDetails(int hotelId, int hotelServiceId)
        {
            return _taxiDetailOperations.GetAll(t => t.HotelServices.HotelId == hotelId && t.HotelServiceId == hotelServiceId);
        }
        /// <summary>
        /// Gets the hotel taxi detail by identifier.
        /// </summary>
        /// <param name="taxiId">The taxi identifier.</param>
        /// <returns>TaxiDetail.</returns>
        public TaxiDetail GetHotelTaxiDetailById(int taxiId)
        {
            return _taxiDetailOperations.GetById(t => t.Id == taxiId && t.IsActive == true);
        }
        /// <summary>
        /// Creates the hotel taxi.
        /// </summary>
        /// <param name="taxi">The taxi.</param>
        /// <returns>TaxiDetail.</returns>
        public TaxiDetail CreateHotelTaxi(TaxiDetail taxi)
        {
            taxi.CreationDate = DateTime.UtcNow;
            return _taxiDetailOperations.AddNew(taxi);
        }
        /// <summary>
        /// Modifies the hotel taxi details.
        /// </summary>
        /// <param name="taxiDetails">The taxi details.</param>
        /// <returns>TaxiDetail.</returns>
        public TaxiDetail ModifyHotelTaxiDetails(TaxiDetail taxiDetails)
        {
            _taxiDetailOperations.Update(taxiDetails.Id, taxiDetails);
            return taxiDetails;
        }

        /// <summary>
        /// Deletes the taxi details.
        /// </summary>
        /// <param name="taxiId">The taxi identifier.</param>
        public void DeleteTaxiDetails(int taxiId)
        {
            _taxiDetailOperations.Delete(taxiId);
        }
        //Taxi Destinations
        /// <summary>
        /// Gets the hotel taxi destination.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable TaxiDestination.</returns>
        public IEnumerable<TaxiDestination> GetHotelTaxiDestination(int hotelId)
        {
            return _taxiDestinationOperations.GetAll(t => t.HotelId == hotelId && t.IsActive == true);
        }

        /// <summary>
        /// Gets all hotel taxi destination.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiDestination.</returns>
        public IEnumerable<TaxiDestination> GetAllHotelTaxiDestination(int hotelId, int skip, int pageSize, out int total)
        {
            return _taxiDestinationOperations.GetAllWithServerSidePagging(t => t.HotelId == hotelId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the taxi destination by identifier.
        /// </summary>
        /// <param name="taxiDestId">The taxi dest identifier.</param>
        /// <returns>TaxiDestination.</returns>
        public TaxiDestination GetTaxiDestinationById(int taxiDestId)
        {
            return _taxiDestinationOperations.GetById(taxiDestId);
        }
        /// <summary>
        /// Creates the taxi destination.
        /// </summary>
        /// <param name="taxiDest">The taxi dest.</param>
        /// <returns>TaxiDestination.</returns>
        public TaxiDestination CreateTaxiDestination(TaxiDestination taxiDest)
        {
            taxiDest.CreationDate = DateTime.UtcNow;
            return _taxiDestinationOperations.AddNew(taxiDest);
        }
        /// <summary>
        /// Modifies the taxi destination.
        /// </summary>
        /// <param name="taxiDest">The taxi dest.</param>
        /// <returns>TaxiDestination.</returns>
        public TaxiDestination ModifyTaxiDestination(TaxiDestination taxiDest)
        {
            _taxiDestinationOperations.Update(taxiDest.Id, taxiDest);
            return taxiDest;
        }
        /// <summary>
        /// Deletes the taxi destination.
        /// </summary>
        /// <param name="taxiDestId">The taxi dest identifier.</param>
        public void DeleteTaxiDestination(int taxiDestId)
        {
            _taxiDestinationOperations.Delete(taxiDestId);
        }


        /// <summary>
        /// Creates the hotel taxi booking.
        /// </summary>
        /// <param name="taxiBooking">The taxi booking.</param>
        /// <returns>TaxiBooking.</returns>
        /// <exception cref="ArgumentException">
        /// INVAID_TAXI_DETAILS_ID
        /// or
        /// INVAID_TAXI_DETAILS_ID
        /// or
        /// Taxi not available for this schedule datatime
        /// </exception>
        public TaxiBooking CreateHotelTaxiBooking(TaxiBooking taxiBooking)
        {
            if (taxiBooking.TaxiDetailsId <= 0)
                throw new ArgumentException("INVAID_TAXI_DETAILS_ID");

            var taxiDetail = _taxiDetailOperations.GetById(t => t.Id == taxiBooking.TaxiDetailsId && t.IsActive);
            if (taxiDetail == null)
                throw new ArgumentException("INVAID_TAXI_DETAILS_ID");
            if (taxiBooking.ScheduleDate != null)
            {
                var available = _taxiBookingOperations.GetAll(t => t.TaxiDetailsId == taxiBooking.TaxiDetailsId && t.ScheduleDate == taxiBooking.ScheduleDate).FirstOrDefault();
                if (available != null)
                    throw new ArgumentException("Taxi not available for this schedule datatime");
            }
            taxiBooking.OrderStatus = 0;
            taxiBooking.CreationDate = DateTime.UtcNow;

            return _taxiBookingOperations.AddNew(taxiBooking);
        }

        /// <summary>
        /// Updates the taxi booking status.
        /// </summary>
        /// <param name="taxiBooking">The taxi booking.</param>
        /// <returns>TaxiBooking.</returns>
        public TaxiBooking UpdateTaxiBookingStatus(TaxiBooking taxiBooking)
        {
            _taxiBookingOperations.Update(taxiBooking.Id, taxiBooking);
            return taxiBooking;
        }

        /// <summary>
        /// Schedules the taxi booking.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="taxiBookingId">The taxi booking identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>TaxiBooking.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_Id
        /// or
        /// Taxi not available for this schedule datatime
        /// </exception>
        /// <exception cref="BookingNotFoundException">TAXI_BOOKING_NOT_FOUND</exception>
        public TaxiBooking ScheduleTaxiBooking(int customerId, int taxiBookingId, DateTime scheduleDate)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_CUSTOMER_Id");

            var taxiBooking = _taxiBookingOperations.GetById(t => t.Id == taxiBookingId);
            if (taxiBooking == null)
                throw new BookingNotFoundException("TAXI_BOOKING_NOT_FOUND");

            var available = _taxiBookingOperations.GetAll(t => t.ScheduleDate == scheduleDate && t.Id != taxiBookingId && t.TaxiDetailsId == taxiBooking.TaxiDetailsId && (t.OrderStatus == 0 || t.OrderStatus == 1));
            if (available != null && available.Count() > 0)
                throw new ArgumentException("Taxi not available for this schedule datatime");

            taxiBooking.ScheduleDate = scheduleDate;
            _taxiBookingRepository.Update(taxiBooking);
            EfRepository<TaxiBooking> newRepo = new EfRepository<TaxiBooking>();

            var booking = newRepo.GetById(taxiBooking.Id);
            return booking;
        }

        /// <summary>
        /// Gets the hotel taxi booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TaxiBooking.</returns>
        public TaxiBooking GetHotelTaxiBooking(int id)
        {
            return _taxiBookingOperations.GetById(t => t.Id == id);
        }

        /// <summary>
        /// Gets all taxi booking by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable TaxiBooking.</returns>
        public IEnumerable<TaxiBooking> GetAllTaxiBookingByCustomer(int customerId)
        {
            return _taxiBookingOperations.GetAll(t => t.CustomerId == customerId);
        }
        /// <summary>
        /// Gets all active taxi booking of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiBooking.</returns>
        public IEnumerable<TaxiBooking> GetAllActiveTaxiBookingOfCustomer(int customerId, int pageNumber, int pageSize, out int total)
        {
            return _taxiBookingOperations.GetAll(t => t.CustomerId == customerId && t.OrderStatus != 2 && t.OrderStatus != 3, b => b.Id, pageNumber, pageSize, out total);
        }
        /// <summary>
        /// Gets the taxi bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiBooking.</returns>
        public IEnumerable<TaxiBooking> GetTaxiBookings(int hotelId, int skip, int pageSize, out int total)
        {
            return _taxiBookingOperations.GetAllWithServerSidePaggingDesc(t => t.TaxiDetails.HotelServices.HotelId == hotelId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Deletes the taxi booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTaxiBooking(int id)
        {
            _taxiBookingOperations.Delete(id);
        }

        //Taxi Booking Review
        /// <summary>
        /// Creates the taxi booking review.
        /// </summary>
        /// <param name="taxiBookingReview">The taxi booking review.</param>
        /// <returns>TaxiBookingReview.</returns>
        public TaxiBookingReview CreateTaxiBookingReview(TaxiBookingReview taxiBookingReview)
        {
            taxiBookingReview.CreationDate = DateTime.UtcNow;
            return _taxiBookingReviewOperations.AddNew(taxiBookingReview);

        }
        /// <summary>
        /// Gets the taxi booking review by booking identifier.
        /// </summary>
        /// <param name="taxiBookingId">The taxi booking identifier.</param>
        /// <returns>IEnumerable TaxiBookingReview.</returns>
        public IEnumerable<TaxiBookingReview> GetTaxiBookingReviewByBookingId(int taxiBookingId)
        {
            return _taxiBookingReviewOperations.GetAll(r => r.TaxiBookingId == taxiBookingId).OrderByDescending(o => o.Id);
        }

        //public TaxiBooking ModifyTaxiBooking(TaxiBooking taxiBooking)
        //{
        //    if (taxiBooking.Id <= 0)
        //        throw new ArgumentException("INVAID_TAXI_BOOKING_ID");

        //    if (taxiBooking.TaxiId <= 0)
        //        throw new ArgumentException("INVAID_TAXI_ID");

        //    if (taxiBooking.From > taxiBooking.To)
        //        throw new ArgumentException("FROM_DATE_MUST_BE_LESS_THAN_TO_DATE");

        //    var taxiDetail = _taxiDetailOperations.GetById(t => t.Id == taxiBooking.TaxiId);
        //    if (taxiDetail == null)
        //        throw new ArgumentException("INVAID_TAXI_ID");

        //    var existingBooking = _taxiBookingOperations.GetById(tb => tb.Id == taxiBooking.Id && tb.CustomerId == taxiBooking.CustomerId);
        //    if (existingBooking == null)
        //        throw new ArgumentException(Resources.TaxiBooking_NotFound);

        //    var available = _taxiBookingOperations.GetAll(t => t.TaxiId == taxiBooking.TaxiId && ((t.From <= taxiBooking.From && t.To >= taxiBooking.From) ||
        //                                                                                          (t.From <= taxiBooking.To && t.To >= taxiBooking.To) ||
        //                                                                                          (taxiBooking.From <= t.From && taxiBooking.To >= t.To))).FirstOrDefault();
        //    if (available != null)
        //        throw new ArgumentException("Taxi not available for this time slot");

        //    existingBooking.From = taxiBooking.From;
        //    existingBooking.To = taxiBooking.To;
        //    existingBooking.CreationDate = DateTime.UtcNow;

        //    _taxiBookingOperations.Update(existingBooking);
        //    return existingBooking;
        //}

        /// <summary>
        /// Gets the taxi reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiReview.</returns>
        public IEnumerable<TaxiReview> GetTaxiReviews(int id, int pageNumber, int pageSize, out int total)
        {
            var taxiReview = _taxiReviewOperations.GetAllByDesc(rw => rw.TaxiId == id, r => r.Id, pageNumber, pageSize, out total);
            return taxiReview;
        }
        /// <summary>
        /// Gets the taxi reviews by hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiReview.</returns>
        public IEnumerable<TaxiReview> GetTaxiReviewsByHotel(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _taxiReviewRepository.Table;
            query = query.Where(o => o.TaxiDetail.HotelServices.HotelId == hotelId);
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<TaxiReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets the taxi review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TaxiReview.</returns>
        public TaxiReview GetTaxiReviewById(int id)
        {
            return _taxiReviewOperations.GetById(id);
        }
        /// <summary>
        /// Creates the taxi reviews.
        /// </summary>
        /// <param name="taxiReviews">The taxi reviews.</param>
        /// <returns>TaxiReview.</returns>
        /// <exception cref="ArgumentException">Taxi details with this taxiId doesn't exists</exception>
        public TaxiReview CreateTaxiReviews(TaxiReview taxiReviews)
        {
            var taxi = _taxiDetailOperations.GetById(taxiReviews.TaxiId);
            if (taxi == null)
                throw new ArgumentException("Taxi details with this taxiId doesn't exists");
            taxiReviews.CreationDate = DateTime.UtcNow;

            return _taxiReviewOperations.AddNew(taxiReviews);
        }

        /// <summary>
        /// Deletes the taxi review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteTaxiReview(int id)
        {
            _taxiReviewOperations.Delete(id);
        }
        /// <summary>
        /// Gets the pending taxi service reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TaxiDetail.</returns>
        public IEnumerable<TaxiDetail> GetPendingTaxiServiceReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            var customerTaxiReviews = _taxiDetailOperations.GetAll(r => r.TaxiBooking.Any(o => o.CustomerId == customerId) && r.TaxiReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
            return customerTaxiReviews;
        }
        #endregion Hotel Taxi

        #region Excursion Search
        /// <summary>
        /// Excursions the search.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="minUserRating">The minimum user rating.</param>
        /// <param name="maxUserRating">The maximum user rating.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="duration_min">The duration minimum.</param>
        /// <param name="duration_max">The duration maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> ExcursionSearch(out int total, int hotelId, string searchText, int[] Ids, double? minUserRating, double? maxUserRating, double? price_min, double? price_max, int? duration_min = null, int? duration_max = null, int page = 1, int limit = 20)
        {
            var query = _hotelExcursionDetailRepository.Table.Where(e => e.IsActive == true && e.HotelExcursion.HotelServices.HotelId == hotelId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(e => e.Name.Contains(searchText));
            }
            if (Ids != null && Ids.Length > 0)
            {
                query = query.Where(e => Ids.Contains(e.HotelExcursionId) && e.HotelExcursion.IsActive);
            }
            if (price_min.HasValue)
            {
                query = query.Where(e => e.Price >= price_min);
            }

            if (price_max.HasValue)
            {
                query = query.Where(e => e.Price <= price_max);
            }

            if (minUserRating.HasValue)
                query = query.Where(e => e.ExcursionReviews.Average(rw => rw.Score) >= minUserRating);

            if (maxUserRating.HasValue)
                query = query.Where(e => e.ExcursionReviews.Average(rw => rw.Score) <= maxUserRating);

            if (duration_min.HasValue)
                query = query.Where(e => e.DurationInDays >= duration_min);

            if (duration_max.HasValue)
                query = query.Where(e => e.DurationInDays <= duration_max);

            var pages = query.OrderBy(_ => true)
                        .Skip((page - 1) * limit)
                        .Take(limit)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<ExcursionDetail>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        #endregion Excursion Search

        #region Excursion
        /// <summary>
        /// Gets the excursions.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Excursion.</returns>
        public IEnumerable<Excursion> GetExcursions(int skip, int pageSize, out int total)
        {
            return _excursionOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the excursion by identifier.
        /// </summary>
        /// <param name="excursionId">The excursion identifier.</param>
        /// <returns>Excursion.</returns>
        public Excursion GetExcursionById(int excursionId)
        {
            return _excursionOperations.GetById(excursionId);
        }
        /// <summary>
        /// Creates the excursion.
        /// </summary>
        /// <param name="hotelExcursion">The hotel excursion.</param>
        /// <returns>Excursion.</returns>
        public Excursion CreateExcursion(Excursion hotelExcursion)
        {
            hotelExcursion.CreationDate = DateTime.UtcNow;
            return _excursionOperations.AddNew(hotelExcursion);
        }
        /// <summary>
        /// Modifies the excursion.
        /// </summary>
        /// <param name="hotelExcursion">The hotel excursion.</param>
        /// <returns>Excursion.</returns>
        public Excursion ModifyExcursion(Excursion hotelExcursion)
        {
            _excursionOperations.Update(hotelExcursion.Id, hotelExcursion);
            return hotelExcursion;
        }
        /// <summary>
        /// Deletes the excursion.
        /// </summary>
        /// <param name="excursionId">The excursion identifier.</param>
        public void DeleteExcursion(int excursionId)
        {
            _excursionOperations.Delete(excursionId);
        }

        //hotel excursion

        /// <summary>
        /// Gets the hotel excursion services.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelExcursion.</returns>
        public IEnumerable<HotelExcursion> GetHotelExcursionServices(int hotelId, int pageNumber, int pageSize, out int total)
        {
            return _hotelExcursionOperations.GetAll(c => c.HotelServices.HotelId == hotelId && c.IsActive, s => s.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Gets all hotel excursions.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelExcursion.</returns>
        public IEnumerable<HotelExcursion> GetAllHotelExcursions(int hotelServiceId, int skip, int pageSize, out int total)
        {
            return _hotelExcursionOperations.GetAllWithServerSidePagging(e => e.HotelServiceId == hotelServiceId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the hotel excursion by identifier.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <returns>HotelExcursion.</returns>
        public HotelExcursion GetHotelExcursionById(int hotelExcursionId)
        {
            return _hotelExcursionOperations.GetById(hotelExcursionId);
        }

        /// <summary>
        /// Deletes the hotel excursion.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        public void DeleteHotelExcursion(int hotelExcursionId)
        {
            _hotelExcursionOperations.Delete(hotelExcursionId);
        }

        /// <summary>
        /// Gets the unassign excursion.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <returns>IEnumerable Excursion.</returns>
        public IEnumerable<Excursion> GetUnassignExcursion(int hotelServiceId)
        {
            var assignExcursionIds = _hotelExcursionOperations.GetAll(s => s.HotelServiceId == hotelServiceId).Select(sd => sd.ExcursionId).Distinct();
            var unassignedExcursion = _excursionRepository.Table.Where(r => !assignExcursionIds.Contains(r.Id) && r.IsActive);
            return unassignedExcursion;
        }

        /// <summary>
        /// Creates the hotel excursion.
        /// </summary>
        /// <param name="listHotelExcursion">The list hotel excursion.</param>
        public void CreateHotelExcursion(List<HotelExcursion> listHotelExcursion)
        {
            foreach (var eachHotelExcursion in listHotelExcursion)
            {
                eachHotelExcursion.CreationDate = DateTime.Now;
                _hotelExcursionOperations.AddNew(eachHotelExcursion);
            }
        }
        /// <summary>
        /// Modifies the hotel excursion is active status.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        public void ModifyHotelExcursionIsActiveStatus(int hotelExcursionId)
        {
            var hotelExcursion = _hotelExcursionOperations.GetById(hotelExcursionId);
            if (hotelExcursion.IsActive == true)
                hotelExcursion.IsActive = false;
            else
                hotelExcursion.IsActive = true;
            _hotelExcursionOperations.Update(hotelExcursion);
        }
        #endregion Excursion

        #region Excursion Details
        //API
        /// <summary>
        /// Gets the excursion details by hotel exc identifier.
        /// </summary>
        /// <param name="HotelExcId">The hotel exc identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetExcursionDetailsByHotelExcId(int HotelExcId, int customerId, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false)
        {
            if (considerMyPreferences)
            {
                System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var custPrefExcursionDetailsIds = _hotelExcursionDetailRepository.ExecuteStoredProcedureList<ExcursionDetail>("exec uspGetCustomerPreferredExcursionDetails", customerIdParameter).Select(s => s.Id).ToList(); ;

                var excursionDetail = _hotelExcursionDetailsOperations.GetAll(s => s.HotelExcursionId == HotelExcId && custPrefExcursionDetailsIds.Contains(s.Id) && s.IsActive, o => o.Id, pageNumber, pageSize, out total);
                return excursionDetail;
            }
            else
            {
                var excursionDetail = _hotelExcursionDetailsOperations.GetAll(e => e.HotelExcursionId == HotelExcId && e.IsActive, o => o.Id, pageNumber, pageSize, out total);
                return excursionDetail;
            }

        }

        //WEB
        /// <summary>
        /// Gets all excursion details.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetAllExcursionDetails(int hotelExcursionId, int skip, int pageSize, out int total)
        {
            return _hotelExcursionDetailsOperations.GetAllWithServerSidePagging(ed => ed.HotelExcursionId == hotelExcursionId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the excursion detail by identifier.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>ExcursionDetail.</returns>
        public ExcursionDetail GetExcursionDetailById(int excursionDetailId)
        {
            return _hotelExcursionDetailsOperations.GetById(excursionDetailId);
        }
        /// <summary>
        /// Creates the excursion detail.
        /// </summary>
        /// <param name="hotelExcursionDet">The hotel excursion det.</param>
        /// <returns>ExcursionDetail.</returns>
        public ExcursionDetail CreateExcursionDetail(ExcursionDetail hotelExcursionDet)
        {
            hotelExcursionDet.CreationDate = DateTime.Now;
            return _hotelExcursionDetailRepository.Insert(hotelExcursionDet);
        }
        /// <summary>
        /// Modifies the excursion detail.
        /// </summary>
        /// <param name="hotelExcursionDetails">The hotel excursion details.</param>
        /// <returns>ExcursionDetail.</returns>
        public ExcursionDetail ModifyExcursionDetail(ExcursionDetail hotelExcursionDetails)
        {
            _hotelExcursionDetailsOperations.Update(hotelExcursionDetails.Id, hotelExcursionDetails);
            return hotelExcursionDetails;
        }
        /// <summary>
        /// Deletes the excursion detail.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        public void DeleteExcursionDetail(int excursionDetailId)
        {
            _hotelExcursionDetailsOperations.Delete(excursionDetailId);
        }

        /// <summary>
        /// Gets the excursion price minimum and maximum of hotel.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionPriceMinAndMax.</returns>
        public ExcursionPriceMinAndMax GetExcursionPriceMinAndMaxOfHotel(int hotelId)
        {
            System.Data.SqlClient.SqlParameter hotelIdParameter = new System.Data.SqlClient.SqlParameter("hotelId", Convert.ToInt32(hotelId));
            var excPriceMinAndMax = _hotelExcursionDetailRepository.ExecuteStoredProcedureList<ExcursionPriceMinAndMax>("exec upsGetExcursionPricMinAndMaxOfHotel", hotelIdParameter).ToList(); ;
            return excPriceMinAndMax.FirstOrDefault();
        }
        #endregion Excursion Details

        #region Excursion Detail Images
        /// <summary>
        /// Gets the excursion detail image by identifier.
        /// </summary>
        /// <param name="excDetImageId">The exc det image identifier.</param>
        /// <returns>ExcursionDetailImages.</returns>
        public ExcursionDetailImages GetExcursionDetailImageById(int excDetImageId)
        {
            return _excursionDetailImagesOperations.GetById(excDetImageId);
        }
        /// <summary>
        /// Gets the excursion detail images.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>IEnumerable ExcursionDetailImages.</returns>
        public IEnumerable<ExcursionDetailImages> GetExcursionDetailImages(int excursionDetailId)
        {
            return _excursionDetailImagesOperations.GetAll(d => d.ExcursionDetailId == excursionDetailId);
        }
        /// <summary>
        /// Updates the excursion det image.
        /// </summary>
        /// <param name="excDetImage">The exc det image.</param>
        /// <returns>ExcursionDetailImages.</returns>
        public ExcursionDetailImages UpdateExcursionDetImage(ExcursionDetailImages excDetImage)
        {
            _excursionDetailImagesOperations.Update(excDetImage.Id, excDetImage);
            return excDetImage;
        }
        /// <summary>
        /// Adds the excursion detail image.
        /// </summary>
        /// <param name="excDetImage">The exc det image.</param>
        /// <returns>ExcursionDetailImages.</returns>
        public ExcursionDetailImages AddExcursionDetailImage(ExcursionDetailImages excDetImage)
        {
            return _excursionDetailImagesOperations.AddNew(excDetImage);
        }
        /// <summary>
        /// Deletes the excursion detail image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExcursionDetailImage(int id)
        {
            _excursionDetailImagesOperations.Delete(id);
        }
        #endregion Excursion Detail Images

        #region Excursion Detail Gallery
        /// <summary>
        /// Gets the excursion detail gallery by identifier.
        /// </summary>
        /// <param name="excDetGalleryId">The exc det gallery identifier.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        public ExcursionDetailGallery GetExcursionDetailGalleryById(int excDetGalleryId)
        {
            return _excursionDetailGalleryOperations.GetById(excDetGalleryId);
        }
        /// <summary>
        /// Gets the excursion detail gallery.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <returns>IEnumerable ExcursionDetailGallery.</returns>
        public IEnumerable<ExcursionDetailGallery> GetExcursionDetailGallery(int excursionDetailId)
        {
            return _excursionDetailGalleryOperations.GetAll(d => d.ExcursionDetailId == excursionDetailId);
        }
        /// <summary>
        /// Updates the excursion det gallery.
        /// </summary>
        /// <param name="excDetGallery">The exc det gallery.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        public ExcursionDetailGallery UpdateExcursionDetGallery(ExcursionDetailGallery excDetGallery)
        {
            _excursionDetailGalleryOperations.Update(excDetGallery.Id, excDetGallery);
            return excDetGallery;
        }
        /// <summary>
        /// Adds the excursion detail gallery.
        /// </summary>
        /// <param name="excDetGallery">The exc det gallery.</param>
        /// <returns>ExcursionDetailGallery.</returns>
        public ExcursionDetailGallery AddExcursionDetailGallery(ExcursionDetailGallery excDetGallery)
        {
            return _excursionDetailGalleryOperations.AddNew(excDetGallery);
        }
        /// <summary>
        /// Deletes the excursion detail gallery.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExcursionDetailGallery(int id)
        {
            _excursionDetailGalleryOperations.Delete(id);
        }

        #endregion Excursion Detail Gallery

        #region Excursion Ingredient Categories
        /// <summary>
        /// Gets all excursion ingredient categories.
        /// </summary>
        /// <returns>IEnumerable ExcursionIngredientCategory.</returns>
        public IEnumerable<ExcursionIngredientCategory> GetAllExcursionIngredientCategories()
        {
            return _excursionIngredientCategoryOperations.GetAll(i => i.IsActive);
        }
        /// <summary>
        /// Gets all excursion ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionIngredientCategory.</returns>
        public IEnumerable<ExcursionIngredientCategory> GetAllExcursionIngredientCategories(int skip, int pageSize, out int total)
        {
            return _excursionIngredientCategoryOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the excursion ingredient category by identifier.
        /// </summary>
        /// <param name="excIngredientCatId">The exc ingredient cat identifier.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        public ExcursionIngredientCategory GetExcursionIngredientCategoryById(int excIngredientCatId)
        {
            return _excursionIngredientCategoryOperations.GetById(excIngredientCatId);
        }
        /// <summary>
        /// Creates the excursion ingredient category.
        /// </summary>
        /// <param name="excurIngredientCategory">The excur ingredient category.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        public ExcursionIngredientCategory CreateExcursionIngredientCategory(ExcursionIngredientCategory excurIngredientCategory)
        {
            excurIngredientCategory.CreationDate = DateTime.Now;
            return _excursionIngredientCategoryOperations.AddNew(excurIngredientCategory);
        }
        /// <summary>
        /// Modifies the excursion ingredient category.
        /// </summary>
        /// <param name="excIngredientCategory">The exc ingredient category.</param>
        /// <returns>ExcursionIngredientCategory.</returns>
        public ExcursionIngredientCategory ModifyExcursionIngredientCategory(ExcursionIngredientCategory excIngredientCategory)
        {
            _excursionIngredientCategoryOperations.Update(excIngredientCategory.Id, excIngredientCategory);
            return excIngredientCategory;
        }

        /// <summary>
        /// Deletes the excursion ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExcursionIngredientCategory(int id)
        {
            _excursionIngredientCategoryOperations.Delete(id);
        }
        #endregion Excursion Ingredient Categories

        #region Excursion Ingredients
        /// <summary>
        /// Gets the excursion ingredient by exc ing cat identifier.
        /// </summary>
        /// <param name="excIngCatId">The exc ing cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionIngredient.</returns>
        public IEnumerable<ExcursionIngredient> GetExcursionIngredientByExcIngCatId(int excIngCatId, int skip, int pageSize, out int total)
        {
            return _excursionIngredientOperations.GetAllWithServerSidePagging(ei => ei.ExcursionIngredientCategoryId == excIngCatId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the excursion ingredient by identifier.
        /// </summary>
        /// <param name="excIngredientId">The exc ingredient identifier.</param>
        /// <returns>ExcursionIngredient.</returns>
        public ExcursionIngredient GetExcursionIngredientById(int excIngredientId)
        {
            return _excursionIngredientOperations.GetById(excIngredientId);
        }
        /// <summary>
        /// Creates the excursion ingredient.
        /// </summary>
        /// <param name="excIngredient">The exc ingredient.</param>
        /// <returns>ExcursionIngredient.</returns>
        public ExcursionIngredient CreateExcursionIngredient(ExcursionIngredient excIngredient)
        {
            return _excursionIngredientOperations.AddNew(excIngredient);
        }
        /// <summary>
        /// Modifies the excursion ingredient.
        /// </summary>
        /// <param name="excIngredient">The exc ingredient.</param>
        /// <returns>ExcursionIngredient.</returns>
        public ExcursionIngredient ModifyExcursionIngredient(ExcursionIngredient excIngredient)
        {
            _excursionIngredientOperations.Update(excIngredient.Id, excIngredient);
            return excIngredient;
        }
        /// <summary>
        /// Deletes the excursion ingredient.
        /// </summary>
        /// <param name="excIngredientId">The exc ingredient identifier.</param>
        public void DeleteExcursionIngredient(int excIngredientId)
        {
            _excursionIngredientOperations.Delete(excIngredientId);
        }
        #endregion Excursion Ingredients

        #region Excursion Reviews
        /// <summary>
        /// Creates the excursion review.
        /// </summary>
        /// <param name="excursionReview">The excursion review.</param>
        /// <returns>ExcursionReview.</returns>
        /// <exception cref="ArgumentException">Excursion detail with this Id not found</exception>
        public ExcursionReview CreateExcursionReview(ExcursionReview excursionReview)
        {
            var excursionDetail = _hotelExcursionDetailsOperations.GetById(excursionReview.ExcursionDetailsId);
            if (excursionDetail == null)
                throw new ArgumentException("Excursion detail with this Id not found");
            excursionReview.CreationDate = DateTime.Now;

            return _excursionReviewsOperations.AddNew(excursionReview);
        }
        /// <summary>
        /// Gets all excursion reviews by exc det identifier.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionReview.</returns>
        public IEnumerable<ExcursionReview> GetAllExcursionReviewsByExcDetId(int excursionDetailId, int pageNumber, int pageSize, out int total)
        {
            return _excursionReviewsOperations.GetAllWithServerSidePaggingDesc(er => er.ExcursionDetailsId == excursionDetailId, o => o.Id, (pageNumber - 1) * pageSize, pageSize, out total);
        }
        #endregion Excursion Reviews

        #region Excursion Offer
        //API
        /// <summary>
        /// Gets the excursion offer by hotel excursion identifier.
        /// </summary>
        /// <param name="hotelExcursionId">The hotel excursion identifier.</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        public IEnumerable<ExcursionOffer> GetExcursionOfferByHotelExcursionId(int hotelExcursionId)
        {
            return _excursionOfferOperations.GetAll(ex => ex.ExcursionDetail.HotelExcursionId == hotelExcursionId && ex.IsActive);
        }
        /// <summary>
        /// Gets the excursion offer by hotel identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        public IEnumerable<ExcursionOffer> GetExcursionOfferByHotelId(int hotelId, int customerId, bool considerMyPreferences = false)
        {
            if (considerMyPreferences)
            {
                System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var custPrefExcursionDetailsIds = _hotelExcursionDetailRepository.ExecuteStoredProcedureList<ExcursionDetail>("exec uspGetCustomerPreferredExcursionDetails", customerIdParameter).Select(s => s.Id).ToList(); ;

                return _excursionOfferOperations.GetAll(exc => exc.ExcursionDetail.HotelExcursion.HotelServices.HotelId == hotelId && exc.IsActive && custPrefExcursionDetailsIds.Contains(exc.ExcursionDetailId));
            }
            else
            {
                return _excursionOfferOperations.GetAll(ex => ex.ExcursionDetail.HotelExcursion.HotelServices.HotelId == hotelId && ex.IsActive);
            }
        }
        //WEB
        /// <summary>
        /// Gets the excursion offer.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOffer.</returns>
        public IEnumerable<ExcursionOffer> GetExcursionOffer(int hotelId, int skip, int pageSize, out int total)
        {
            return _excursionOfferOperations.GetAllWithServerSidePagging(o => o.ExcursionDetail.HotelExcursion.HotelServices.HotelId == hotelId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the unassigned offer excursion details.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetUnassignedOfferExcursionDetails(int hotelId)
        {
            var assignedExcursionDetailOffer = _excursionOfferOperations.GetAll(o => o.ExcursionDetail.HotelExcursion.HotelServices.HotelId == hotelId).Select(o => o.ExcursionDetailId).ToList();
            return _hotelExcursionDetailsOperations.GetAll(s => s.HotelExcursion.HotelServices.HotelId == hotelId && s.IsActive && !assignedExcursionDetailOffer.Contains(s.Id));
        }

        /// <summary>
        /// Gets the excursion offer by identifier.
        /// </summary>
        /// <param name="excursionOfferId">The excursion offer identifier.</param>
        /// <returns>ExcursionOffer.</returns>
        public ExcursionOffer GetExcursionOfferById(int excursionOfferId)
        {
            return _excursionOfferOperations.GetById(excursionOfferId);
        }
        /// <summary>
        /// Creates the excursion offer.
        /// </summary>
        /// <param name="excursionOffer">The excursion offer.</param>
        /// <returns>ExcursionOffer.</returns>
        public ExcursionOffer CreateExcursionOffer(ExcursionOffer excursionOffer)
        {
            return _excursionOfferOperations.AddNew(excursionOffer);
        }
        /// <summary>
        /// Modifies the excursion offer.
        /// </summary>
        /// <param name="excursionOffer">The excursion offer.</param>
        /// <returns>ExcursionOffer.</returns>
        public ExcursionOffer ModifyExcursionOffer(ExcursionOffer excursionOffer)
        {
            _excursionOfferOperations.Update(excursionOffer.Id, excursionOffer);
            return excursionOffer;
        }
        /// <summary>
        /// Deletes the excursion offer.
        /// </summary>
        /// <param name="excursionOfferid">The excursion offerid.</param>
        public void DeleteExcursionOffer(int excursionOfferid)
        {
            _excursionOfferOperations.Delete(excursionOfferid);
        }
        #endregion Excursion Offer

        #region Hotel Excursion Detail Ingredients
        /// <summary>
        /// Gets the excursion detail ingredients.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetailIngredient.</returns>
        public IEnumerable<ExcursionDetailIngredient> GetExcursionDetailIngredients(int excursionDetailId, int skip, int pageSize, out int total)
        {
            return _excursionDetailIngredientOperations.GetAllWithServerSidePagging(ei => ei.ExcursionDetailsId == excursionDetailId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the excursion detail ingredient by identifier.
        /// </summary>
        /// <param name="excursionDetIngrId">The excursion det ingr identifier.</param>
        /// <returns>ExcursionDetailIngredient.</returns>
        public ExcursionDetailIngredient GetExcursionDetailIngredientById(int excursionDetIngrId)
        {
            return _excursionDetailIngredientOperations.GetById(excursionDetIngrId);
        }

        /// <summary>
        /// Gets the unassign excursion ingrdeients.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <returns>IEnumerable ExcursionIngredient.</returns>
        public IEnumerable<ExcursionIngredient> GetUnassignExcursionIngrdeients(int excursionDetId)
        {
            var assignExcursionIngrIds = _excursionDetailIngredientOperations.GetAll(e => e.ExcursionDetailsId == excursionDetId).Select(sd => sd.ExcursionIngredientsId).Distinct();
            var unassignedExcursionIngredients = _excursionIngredientRepository.Table.Where(e => !assignExcursionIngrIds.Contains(e.Id));
            return unassignedExcursionIngredients;
        }

        /// <summary>
        /// Creates the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetailIngr">The excursion detail ingr.</param>
        public void CreateExcursionDetailIngredient(List<ExcursionDetailIngredient> excursionDetailIngr)
        {
            foreach (var eachExcDetIngr in excursionDetailIngr)
            {
                _excursionDetailIngredientOperations.AddNew(eachExcDetIngr);
            }
        }
        /// <summary>
        /// Deletes the excursion detail ingredient.
        /// </summary>
        /// <param name="excursionDetIngrId">The excursion det ingr identifier.</param>
        public void DeleteExcursionDetailIngredient(int excursionDetIngrId)
        {
            _excursionDetailIngredientOperations.Delete(excursionDetIngrId);
        }
        #endregion Hotel Excursion Detail Ingredients

        #region Excursion Suggestion
        /// <summary>
        /// Gets the excursion detail suggestions.
        /// </summary>
        /// <param name="excursionDetailId">The excursion detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetExcursionDetailSuggestions(int excursionDetailId, int pageNumber, int pageSize, out int total)
        {
            var excursionSuggestion = _excursionSuggestionOperations.GetAll(i => i.ExcursionDetailId == excursionDetailId && i.ExcursionDetail.IsActive, i => i.Id, pageNumber, pageSize, out total).Select(i => i.ExcursionDetail1);
            return excursionSuggestion;
        }

        //WEB
        /// <summary>
        /// Gets the excursion det suggestions.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionSuggestion.</returns>
        public IEnumerable<ExcursionSuggestion> GetExcursionDetSuggestions(int excursionDetId, int skip, int pageSize, out int total)
        {
            return _excursionSuggestionOperations.GetAllWithServerSidePagging(i => i.ExcursionDetailId == excursionDetId, i => i.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the unassign excursion det suggetions.
        /// </summary>
        /// <param name="excursionDetId">The excursion det identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ExcursionDetail.</returns>
        public IEnumerable<ExcursionDetail> GetUnassignExcursionDetSuggetions(int excursionDetId, int hotelId)
        {
            var assignExcursionDetIds = _excursionSuggestionOperations.GetAll(s => s.ExcursionDetailId == excursionDetId).Select(s => s.ExcursionDetailSuggestionId).Distinct();
            var unassignedExcursioneDet = _hotelExcursionDetailRepository.Table.Where(r => r.Id != excursionDetId && !assignExcursionDetIds.Contains(r.Id) && r.HotelExcursion.HotelServices.HotelId == hotelId && r.IsActive == true);
            return unassignedExcursioneDet;
        }

        /// <summary>
        /// Creates the excursion detail suggestion.
        /// </summary>
        /// <param name="excursionDetSuggestion">The excursion det suggestion.</param>
        public void CreateExcursionDetailSuggestion(List<ExcursionSuggestion> excursionDetSuggestion)
        {
            foreach (var eachExcDetSuggestion in excursionDetSuggestion)
            {
                _excursionSuggestionOperations.AddNew(eachExcDetSuggestion);
            }
        }

        /// <summary>
        /// Deletes the excursion det suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteExcursionDetSuggestion(int id)
        {
            _excursionSuggestionOperations.Delete(id);
        }
        #endregion Excursion Suggestion

        #region Excursion Cart
        /// <summary>
        /// Gets the customer excursion cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionCart.</returns>
        public ExcursionCart GetCustomerExcursionCart(int customerId, int hotelId)
        {
            var cart = _excursionCartOperations.GetById(c => c.CustomerId == customerId && c.HotelId == hotelId);
            return cart;
        }
        /// <summary>
        /// Creates the excursion cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>ExcursionCart.</returns>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ArgumentException">
        /// INVALID_EXCURSION_DETAIL_ID
        /// or
        /// INVALID_EXCURSION_DETAIL_ID
        /// </exception>
        /// <exception cref="CartExistsException">EXCURSION_CART_ALREADY_EXISTS</exception>
        public ExcursionCart CreateExcursionCart(ExcursionCart cart, bool flush = false)
        {
            int hotelId = 0;
            if (cart.ExcursionCartItems != null && cart.ExcursionCartItems.Any())
            {
                foreach (var item in cart.ExcursionCartItems)
                {
                    var excursionDetail = _hotelExcursionDetailsOperations.GetById(i => i.Id == item.ExcursionDetailsId);
                    if (excursionDetail == null)
                        throw new NotFoundException(Resources.ExcursionDetail_NotFound);

                    var hotelID = excursionDetail.HotelExcursion.HotelServices.HotelId;
                    if (hotelId == 0)
                        hotelId = hotelID;

                    if (hotelId != hotelID)
                        throw new ArgumentException("INVALID_EXCURSION_DETAIL_ID");
                }
            }

            var exitingExcursionCart = _excursionCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.HotelId == hotelId).FirstOrDefault();
            if (exitingExcursionCart != null)
            {
                if (flush)
                    _excursionCartRepository.Delete(exitingExcursionCart.Id);
                else
                    throw new CartExistsException("EXCURSION_CART_ALREADY_EXISTS");
            }

            if (cart.ExcursionCartItems != null && cart.ExcursionCartItems.Any())
            {
                double result = 0.00f;
                foreach (var item in cart.ExcursionCartItems)
                {
                    var excursionDetails = _hotelExcursionDetailsOperations.GetById(item.ExcursionDetailsId);
                    if (excursionDetails == null)
                        throw new ArgumentException("INVALID_EXCURSION_DETAIL_ID");

                    //Chek If NoOfPerson = 0
                    if (item.NoOfPerson == 0)
                        item.NoOfPerson = 1;

                    //Check for excursion Details Offer if Exist
                    double? offerPrice = 0;
                    if (excursionDetails.ExcursionOffers != null && excursionDetails.ExcursionOffers.Count() >= 1)
                    {
                        double? percentage = excursionDetails.ExcursionOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                        if (percentage > 0 && percentage != null)
                            offerPrice = ((excursionDetails.Price * percentage) / 100) * item.NoOfPerson;
                    }
                    item.OfferPrice = offerPrice;
                    item.Total = (item.NoOfPerson * excursionDetails.Price) - Convert.ToDouble(offerPrice);

                    result += Convert.ToDouble(item.Total);
                    cart.HotelId = hotelId;
                }
                cart.Total = result;
            }

            cart.CreationDate = DateTime.Now;
            _excursionCartRepository.InsertWithChild(cart); // unable to save child and populate navigation properties, so work around
            cart.Customer = _customerRepository.GetById(cart.CustomerId);

            foreach (var item in cart.ExcursionCartItems)
            {
                item.ExcursionDetails = _hotelExcursionDetailRepository.Table
                    .Where(i => i.Id == item.ExcursionDetailsId)
                    .FirstOrDefault();
            }
            cart.ExcursionCartItems = cart.ExcursionCartItems.OrderBy(c => c.Id).ToList();
            return cart;
        }

        /// <summary>
        /// Updates the excursion cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>ExcursionCart.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_ID
        /// or
        /// INVALID_EXCURSION_DETAIL_ID
        /// or
        /// INVALID_EXCURSION_DETAIL_ID
        /// </exception>
        /// <exception cref="ArgumentNullException">INVALID_CART_DETAILS</exception>
        /// <exception cref="NotFoundException">Existing excursion cart not found</exception>
        public ExcursionCart UpdateExcursionCart(int customerId, ExcursionCart cart)
        {
            if (customerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            if (cart == null)
                throw new ArgumentNullException("INVALID_CART_DETAILS");

            int hotelId = 0;
            if (cart.ExcursionCartItems != null && cart.ExcursionCartItems.Any())
            {
                foreach (var item in cart.ExcursionCartItems)
                {
                    var excursionDetail = _hotelExcursionDetailsOperations.GetById(i => i.Id == item.ExcursionDetailsId);
                    if (excursionDetail == null)
                        throw new ArgumentException("INVALID_EXCURSION_DETAIL_ID");

                    var hotelID = excursionDetail.HotelExcursion.HotelServices.HotelId;
                    if (hotelId == 0)
                        hotelId = hotelID;

                    if (hotelId != hotelID)
                        throw new ArgumentException("INVALID_EXCURSION_DETAIL_ID");
                }
            }
            var exitingExcursionCart = _excursionCartOperations.GetById(c => c.CustomerId == cart.CustomerId && c.HotelId == hotelId);
            if (exitingExcursionCart == null)
                throw new NotFoundException("Existing excursion cart not found");

            foreach (var excCartItem in cart.ExcursionCartItems)
            {
                var excursionDetails = _hotelExcursionDetailsOperations.GetById(excCartItem.ExcursionDetailsId);

                ExcursionCartItems existItem;
                existItem = exitingExcursionCart.ExcursionCartItems.Where(i => i.Id == excCartItem.Id && i.ExcursionDetailsId == excCartItem.ExcursionDetailsId).FirstOrDefault();

                // check NoOfPerson=0
                if (excCartItem.NoOfPerson == 0)
                    excCartItem.NoOfPerson = 1;

                if (existItem == null)
                {
                    //Check for excursion detail offer if Exists
                    double? offerPrice = 0;
                    if (excursionDetails.ExcursionOffers != null && excursionDetails.ExcursionOffers.Count() >= 1)
                    {
                        double? percentage = excursionDetails.ExcursionOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                        if (percentage > 0 && percentage != null)
                            offerPrice = ((excursionDetails.Price * percentage) / 100) * excCartItem.NoOfPerson;
                    }
                    excCartItem.OfferPrice = offerPrice;
                    excCartItem.Total = (excCartItem.NoOfPerson * excursionDetails.Price) - Convert.ToDouble(offerPrice);

                    exitingExcursionCart.Total += excCartItem.Total;
                    exitingExcursionCart.ExcursionCartItems.Add(excCartItem);
                }
                //existItem not NULL
                else
                {
                    existItem.Comment = excCartItem.Comment;
                    existItem.ScheduleDate = excCartItem.ScheduleDate;
                    existItem.NoOfPerson = excCartItem.NoOfPerson;

                    exitingExcursionCart.Total -= existItem.Total;

                    //Check for Offer if Exist
                    double? offerPrice = 0;
                    if (excursionDetails.ExcursionOffers != null && excursionDetails.ExcursionOffers.Count() >= 1)
                    {
                        double? percentage = excursionDetails.ExcursionOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                        if (percentage > 0 && percentage != null)
                            offerPrice = ((excursionDetails.Price * percentage) / 100) * excCartItem.NoOfPerson;
                    }
                    excCartItem.OfferPrice = offerPrice;
                    existItem.Total = (excCartItem.NoOfPerson * excursionDetails.Price) - Convert.ToDouble(offerPrice);

                    exitingExcursionCart.Total += existItem.Total;
                }
            }

            _excursionCartOperations.Update(exitingExcursionCart);

            var newRepo = new EfRepository<ExcursionCart>();
            var customerCart = newRepo.GetById(exitingExcursionCart.Id);

            customerCart.ExcursionCartItems = customerCart.ExcursionCartItems.OrderBy(c => c.Id).ToList();
            return customerCart;
        }

        /// <summary>
        /// Modifies the excursion cart total.
        /// </summary>
        /// <param name="excCart">The exc cart.</param>
        /// <returns>ExcursionCart.</returns>
        public ExcursionCart ModifyExcursionCartTotal(ExcursionCart excCart)
        {
            _excursionCartOperations.Update(excCart.Id, excCart);
            return excCart;
        }

        /// <summary>
        /// Gets the excursion cart item by identifier.
        /// </summary>
        /// <param name="excursionCartItemId">The excursion cart item identifier.</param>
        /// <returns>ExcursionCartItems.</returns>
        public ExcursionCartItems GetExcursionCartItemById(int excursionCartItemId)
        {
            return _excursionCartItemsOpertions.GetById(excursionCartItemId);
        }

        /// <summary>
        /// Deletes the excursion cart item.
        /// </summary>
        /// <param name="excCartItemId">The exc cart item identifier.</param>
        public void DeleteExcursionCartItem(int excCartItemId)
        {
            _excursionCartItemsOpertions.Delete(excCartItemId);
        }
        #endregion Excursion Cart

        #region Excursion Order
        //WEB
        /// <summary>
        /// Gets the excursion orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOrder.</returns>
        public IEnumerable<ExcursionOrder> GetExcursionOrdersQueue(int hotelId, int skip, int pageSize, out int total)
        {
            var orderStatuses = new int[] { 0, 1 };
            return _excursionOrderOperations.GetAllWithServerSidePaggingDesc(e => e.HotelId == hotelId && orderStatuses.Contains(e.OrderStatus.Value), o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the excursion order item details.
        /// </summary>
        /// <param name="excursionOrderId">The excursion order identifier.</param>
        /// <returns>IEnumerable ExcursionOrderItems.</returns>
        public IEnumerable<ExcursionOrderItems> GetExcursionOrderItemDetails(int excursionOrderId)
        {
            return _excursionOrderItemsOpersations.GetAll(o => o.ExcursionOrderId == excursionOrderId).OrderBy(o => o.ScheduleDate);
        }

        /// <summary>
        /// Updates the excursion order item deliver status.
        /// </summary>
        /// <param name="excursionOrderItemId">The excursion order item identifier.</param>
        public void UpdateExcursionOrderItemDeliverStatus(int excursionOrderItemId)
        {
            var excursionOrderItem = _excursionOrderItemsOpersations.GetById(excursionOrderItemId);
            if (excursionOrderItem.DeliveryStatus == true)
                excursionOrderItem.DeliveryStatus = false;
            else
                excursionOrderItem.DeliveryStatus = true;
            _excursionOrderItemsOpersations.Update(excursionOrderItem);
        }
        /// <summary>
        /// Modifies the excursion order.
        /// </summary>
        /// <param name="excursionOrder">The excursion order.</param>
        /// <returns>ExcursionOrder.</returns>
        public ExcursionOrder ModifyExcursionOrder(ExcursionOrder excursionOrder)
        {
            _excursionOrderOperations.Update(excursionOrder.Id, excursionOrder);
            return excursionOrder;
        }

        /// <summary>
        /// Gets the excursion order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ExcursionOrder.</returns>
        public IEnumerable<ExcursionOrder> GetExcursionOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total)
        {
            var query = _excursionOrderRepository.Table;
            if (hotelId > 0)
            {
                query = query.Where(o => o.HotelId == hotelId);
            }
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.OrderTotal >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.OrderTotal <= maxAmount);
            }
            var pages = query.OrderByDescending(o => o.Id)
                        .Skip(skip)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<ExcursionOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the excursion order is late status changed count.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>System.Int32.</returns>
        public int GetExcursionOrderIsLateStatusChangedCount(int hotelId)
        {
            return _excursionOrderOperations.GetAll(o => o.HotelId == hotelId && o.IsLate == true && o.OrderStatus < 2).ToList().Count();
        }


        //API
        /// <summary>
        /// Gets the excursion orders details by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>ExcursionOrder.</returns>
        public ExcursionOrder GetExcursionOrdersDetailsById(int orderId)
        {
            return _excursionOrderOperations.GetById(orderId);
        }

        /// <summary>
        /// Creates the excursion order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>ExcursionOrder.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_Id
        /// or
        /// INVALID_CARD_Id
        /// or
        /// Schedule date time should be greater than current date time for " + item.ExcursionDetails.Name
        /// </exception>
        /// <exception cref="CartEmptyException">CART_IS_EMPTY</exception>
        public ExcursionOrder CreateExcursionOrderFromCart(int customerId, int cardId, int hotelId)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_Id");

            var excCart = _excursionCartOperations.GetById(c => c.CustomerId == customerId && c.HotelId == hotelId);

            if (excCart == null || !excCart.ExcursionCartItems.Any())
                throw new CartEmptyException("CART_IS_EMPTY");

            var card = _customerCreditCardRepository.GetById(cardId);
            if (card == null || card.CustomerId != customerId)
                throw new ArgumentException("INVALID_CARD_Id");

            ExcursionOrder order = new ExcursionOrder
            {
                CustomerId = customerId,
                HotelId = excCart.HotelId,
                CardId = cardId,
                OrderStatus = 0,
                PaymentStatus = false,
                OrderTotal = excCart.Total,
                CreationDate = DateTime.Now
            };

            foreach (var item in excCart.ExcursionCartItems)
            {
                if (item.ScheduleDate < DateTime.Now)
                    throw new ArgumentException("Schedule date time should be greater than current date time for " + item.ExcursionDetails.Name);


                ExcursionOrderItems excOrderItem = new ExcursionOrderItems
                {
                    ExcursionDetailsId = item.ExcursionDetailsId,
                    Comment = item.Comment,
                    NoOfPerson = item.NoOfPerson,
                    Total = item.Total,
                    ScheduleDate = Convert.ToDateTime(item.ScheduleDate),
                    OfferPrice = item.OfferPrice,
                    DeliveryStatus = false,
                    IsActive = true
                };

                order.ExcursionOrderItems.Add(excOrderItem);
            }
            _excursionOrderRepository.InsertWithChild(order);
            _excursionCartRepository.Delete(excCart.Id);
            EfRepository<ExcursionOrder> newRepo = new EfRepository<ExcursionOrder>();

            var customerExcursionOrder = newRepo.GetById(order.Id);
            customerExcursionOrder.ExcursionOrderItems = customerExcursionOrder.ExcursionOrderItems.OrderBy(c => c.ScheduleDate).ToList();
            return customerExcursionOrder;
        }
        /// <summary>
        /// Gets the customer active excursion orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveExcursionOrdersAndBaskets.</returns>
        public CustomerActiveExcursionOrdersAndBaskets GetCustomerActiveExcursionOrders(int customerId)
        {
            var activeOrderStatuses = new int[] { 0, 1 };
            var activeExcursionOrders = _excursionOrderOperations.GetAll(o => o.CustomerId == customerId && activeOrderStatuses.Contains(o.OrderStatus.Value)).ToList();
            var activeExcursionBaskets = _excursionCartOperations.GetAll(c => c.CustomerId == customerId && c.Total > 0).ToList();
            CustomerActiveExcursionOrdersAndBaskets activeExcursionOrdersAndBaskets = new CustomerActiveExcursionOrdersAndBaskets()
            {
                ActiveExcursionOrders = activeExcursionOrders,
                ActiveExcursionBaskets = activeExcursionBaskets
            };
            return activeExcursionOrdersAndBaskets;
        }

        /// <summary>
        /// Creates the excursion order review.
        /// </summary>
        /// <param name="excursionOrderReview">The excursion order review.</param>
        /// <returns>ExcursionOrderReview.</returns>
        public ExcursionOrderReview CreateExcursionOrderReview(ExcursionOrderReview excursionOrderReview)
        {
            excursionOrderReview.CreationDate = DateTime.UtcNow;
            return _excursionOrderReviewOperation.AddNew(excursionOrderReview);
        }
        /// <summary>
        /// Gets the excursion order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable&lt;ExcursionOrderReview&gt;.</returns>
        public IEnumerable<ExcursionOrderReview> GetExcursionOrderReviews(int id)
        {
            return _excursionOrderReviewOperation.GetAll(l => l.ExcursionOrderId == id).OrderByDescending(o => o.Id);
        }

        /// <summary>
        /// Gets the customer excursion orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable excursionOrder.</returns>
        public IEnumerable<ExcursionOrder> GetCustomerExcursionOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total)
        {
            var query = _excursionOrderRepository.Table;
            if (month != 0)
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year && o.CreationDate.Month == month);
            else
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<ExcursionOrder>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        #endregion Excursion Order

        #region Customer HotelRoom
        /// <summary>
        /// Gets the customer rooom details by identifier.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerHotelRoom.</returns>
        public CustomerHotelRoom GetCustomerRooomDetailsById(int hotelId, int customerId)
        {
            return _customerHotelRoomOperation.GetById(c => c.Room.HotelId == hotelId && c.CustomerId == customerId && c.IsActive);
        }
        /// <summary>
        /// Creates the customer hotel rooom.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>CustomerHotelRoom.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_Id
        /// or
        /// HOTEL_ROOM_NOT_AVAILABLE_FOR_THIS_ID
        /// or
        /// HOTEL_ROOM_ALREADY_ACTIVE
        /// </exception>
        public CustomerHotelRoom CreateCustomerHotelRooom(int customerId, int roomId)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_CUSTOMER_Id");

            var hotelRoom = _roomOperations.GetById(roomId);
            if (hotelRoom == null)
                throw new ArgumentException("HOTEL_ROOM_NOT_AVAILABLE_FOR_THIS_ID");

            var alreadyAtive = hotelRoom.CustomerHotelRoom.Where(r => r.IsActive == true).FirstOrDefault();
            if (alreadyAtive != null)
                throw new ArgumentException("HOTEL_ROOM_ALREADY_ACTIVE");

            CustomerHotelRoom customerHotelRoom = new CustomerHotelRoom
            {
                RoomId = roomId,
                CustomerId = customerId,
                IsActive = true
            };
            return _customerHotelRoomOperation.AddNew(customerHotelRoom);
        }

        /// <summary>
        /// Modifies the customer hotel rooom.
        /// </summary>
        /// <param name="customerHotelRoomId">The customer hotel room identifier.</param>
        public void ModifyCustomerHotelRooom(int customerHotelRoomId)
        {
            var hotelRoomCustomers = _customerHotelRoomOperation.GetById(customerHotelRoomId);
            if (hotelRoomCustomers.IsActive == true)
                hotelRoomCustomers.IsActive = false;
            else
                hotelRoomCustomers.IsActive = true;
            _customerHotelRoomOperation.Update(hotelRoomCustomers);
        }

        #endregion Customer HotelRoom

        #region Housekeeping Cart

        /// <summary>
        /// Gets the customer housekeeping cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>HousekeepingCart.</returns>
        public HousekeepingCart GetCustomerHousekeepingCart(int customerId, int hotelId)
        {
            var cart = _housekeepingCartOperation.GetAll(c => c.CustomerId == customerId && c.Room.HotelId == hotelId).FirstOrDefault();
            return cart;
        }

        /// <summary>
        /// Creates the housekeeping cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>HousekeepingCart.</returns>
        /// <exception cref="ArgumentException">
        /// NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID_OR_NOT_ACTIVE
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + hKeepingCartItem.HouseKeepingFacilityDetailsId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + hKeepingCartItem.HouseKeepingFacilityDetailsId
        /// </exception>
        /// <exception cref="CartExistsException">HOUSEKEEPING_CART_ALREADY_EXISTS</exception>
        public HousekeepingCart CreateHousekeepingCart(HousekeepingCart cart, bool flush = false)
        {
            int hotelId = 0;

            var room = _hotelRoomsOperation.GetById(r => r.Id == cart.RoomId && r.IsActive);
            if (room == null)
                throw new ArgumentException("NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID");
            else
                hotelId = room.HotelId;

            if (cart.HousekeepingCartItems != null && cart.HousekeepingCartItems.Any())
            {
                foreach (var hkeepingItem in cart.HousekeepingCartItems)
                {
                    if (hkeepingItem.HouseKeepingFacilityDetailsId == 0)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                    if (hkeepingItem.ScheduleDate != null && hkeepingItem.ScheduleDate <= DateTime.Now)
                        throw new ArgumentException(Resources.Invalid_ScheduleDate);

                    var housekeepingFacilityDetail = _houseKeepingFacilityDetailsOperations.GetById(i => i.Id == hkeepingItem.HouseKeepingFacilityDetailsId);
                    if (housekeepingFacilityDetail == null)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                    if (hotelId != housekeepingFacilityDetail.HouseKeepingFacility.HotelService.HotelId)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");
                }
            }

            var exitingHousekeepingCart = _housekeepingCartOperation.GetAll(c => c.CustomerId == cart.CustomerId && c.RoomId == room.Id).FirstOrDefault();
            if (exitingHousekeepingCart != null)
            {
                if (flush)
                    _housekeepingCartRepository.Delete(exitingHousekeepingCart.Id);
                else
                    throw new CartExistsException("HOUSEKEEPING_CART_ALREADY_EXISTS");
            }

            // verify additionals are correctly specified for each item
            if (cart.HousekeepingCartItems != null && cart.HousekeepingCartItems.Any())
            {
                double result = 0.00f;
                foreach (var hKeepingCartItem in cart.HousekeepingCartItems)
                {
                    if (hKeepingCartItem.Quantity == 0)
                        hKeepingCartItem.Quantity = 1;

                    var housekeepingFacilityDetail = _houseKeepingFacilityDetailsOperations.GetAll(i => i.Id == hKeepingCartItem.HouseKeepingFacilityDetailsId && i.IsActive == true).FirstOrDefault();
                    if (housekeepingFacilityDetail == null)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID_OR_NOT_ACTIVE");

                    hKeepingCartItem.Total = hKeepingCartItem.Quantity * (housekeepingFacilityDetail.Price == null ? 0 : housekeepingFacilityDetail.Price);

                    double additionalResult = 0.00f;
                    foreach (var additionalCartItem in hKeepingCartItem.HousekeepingCartItemAdditionals)
                    {
                        if (additionalCartItem.Qty == null || additionalCartItem.Qty == 0)
                            additionalCartItem.Qty = 1;

                        var additionalDetails = _housekeepingAdditionalElementsRepository.GetById(additionalCartItem.HousekeepingAdditionalElementId);
                        if (additionalDetails != null)
                        {
                            additionalCartItem.Total = (additionalDetails.Price == null ? 0 : additionalDetails.Price) * additionalCartItem.Qty;
                            additionalResult += Convert.ToDouble(additionalCartItem.Total);
                        }
                    }

                    var additionals = housekeepingFacilityDetail.HouseKeepingFacility.HouseKeepingfacilitiesAdditionals;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.HousekeepingAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if (!addGroup.MinSelected.HasValue && (!addGroup.MaxSelected.HasValue))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (hKeepingCartItem.HousekeepingCartItemAdditionals != null && hKeepingCartItem.HousekeepingCartItemAdditionals.Any())
                            {
                                foreach (var cItemAdditional in hKeepingCartItem.HousekeepingCartItemAdditionals)
                                {
                                    if (addGroup.HousekeepingAdditionalElements.Any(i => i.Id == cItemAdditional.HousekeepingAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > cartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + hKeepingCartItem.HouseKeepingFacilityDetailsId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < cartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + hKeepingCartItem.HouseKeepingFacilityDetailsId);
                        }
                    }
                    hKeepingCartItem.Total += additionalResult;
                    result += Convert.ToDouble(hKeepingCartItem.Total);
                }
                cart.Total = result;
            }

            cart.CreationDate = DateTime.Now;
            _housekeepingCartRepository.InsertWithChild(cart); // unable to save child and populate navigation properties, so work around
            cart.Room = _hotelRoomsRepository.GetById(cart.RoomId);

            foreach (var item in cart.HousekeepingCartItems)
            {
                foreach (var additional in item.HousekeepingCartItemAdditionals)
                {
                    additional.HousekeepingAdditionalElement = _housekeepingAdditionalElementsRepository.GetById(additional.HousekeepingAdditionalElementId);
                }

                item.HouseKeepingFacilityDetail = _houseKeepingFacilityDetailsRepository.Table
                    .Include(i => i.HouseKeepingFacility.HouseKeepingfacilitiesAdditionals.Select(ad => ad.HousekeepingAdditionalGroup))
                    .Where(i => i.Id == item.HouseKeepingFacilityDetailsId)
                    .FirstOrDefault();
            }
            cart.HousekeepingCartItems = cart.HousekeepingCartItems.OrderBy(c => c.ScheduleDate).ToList();
            return cart;
        }

        /// <summary>
        /// Updates the housekeeping cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>HousekeepingCart.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_ID
        /// or
        /// NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + item.HouseKeepingFacilityDetailsId
        /// or
        /// MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + item.HouseKeepingFacilityDetailsId
        /// or
        /// INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID
        /// or
        /// or
        /// </exception>
        /// <exception cref="ArgumentNullException">INVALID_CART_DETAILS</exception>
        /// <exception cref="InvalidOperationException">CART_NOT_FOUND</exception>
        /// <exception cref="NegativeItemQtyException">NEGATIVE_ITEM_QTY</exception>
        public HousekeepingCart UpdateHousekeepingCart(int customerId, HousekeepingCart cart)
        {
            if (customerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");
            if (cart == null)
                throw new ArgumentNullException("INVALID_CART_DETAILS");

            int hotelId = 0;
            var room = _hotelRoomsOperation.GetById(r => r.Id == cart.RoomId && r.IsActive);
            if (room == null)
                throw new ArgumentException("NO_ACTIVE_ROOM_AVAILABLE_FOR_THIS_ID");
            else
                hotelId = room.HotelId;

            if (cart.HousekeepingCartItems != null && cart.HousekeepingCartItems.Any())
            {
                foreach (var item in cart.HousekeepingCartItems)
                {
                    if (item.HouseKeepingFacilityDetailsId == 0)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                    var housekeepingFacilityDetail = _houseKeepingFacilityDetailsOperations.GetById(i => i.Id == item.HouseKeepingFacilityDetailsId);
                    if (housekeepingFacilityDetail == null)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                    if (hotelId != housekeepingFacilityDetail.HouseKeepingFacility.HotelService.HotelId)
                        throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                    var additionals = housekeepingFacilityDetail.HouseKeepingFacility.HouseKeepingfacilitiesAdditionals;
                    if (additionals != null && additionals.Any())
                    {
                        var additional_groups = additionals.Select(ad => ad.HousekeepingAdditionalGroup);
                        foreach (var addGroup in additional_groups)
                        {
                            if (!addGroup.MinSelected.HasValue && (!addGroup.MaxSelected.HasValue))
                                continue;  //no range to check, so ignore this group

                            int cartAdditionalsCount = 0;

                            if (item.HousekeepingCartItemAdditionals != null && item.HousekeepingCartItemAdditionals.Any())
                            {
                                foreach (var cItemAdditional in item.HousekeepingCartItemAdditionals)
                                {
                                    if (addGroup.HousekeepingAdditionalElements.Any(i => i.Id == cItemAdditional.HousekeepingAdditionalElementId))
                                    {
                                        cartAdditionalsCount++;
                                    }
                                }
                            }
                            if (addGroup.MinSelected.HasValue && addGroup.MinSelected.Value > cartAdditionalsCount)
                                throw new ArgumentException("MINIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_LESS_THAN " + addGroup.MinSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + item.HouseKeepingFacilityDetailsId);

                            if (addGroup.MaxSelected.HasValue && addGroup.MaxSelected.Value < cartAdditionalsCount)
                                throw new ArgumentException("MAXIMUM_ADDITIONALS_TO_BE_SELECTED_SHOULDN'T_BE_GREATER_THAN " + addGroup.MaxSelected.Value.ToString() + " FOR " + addGroup.Name.ToUpper() + " GROUP_OF_HOUSEKEEPING_FACILITY_DETAILS_ID " + item.HouseKeepingFacilityDetailsId);
                        }
                    }
                }
            }
            var exitingHousekeepingCart = _housekeepingCartOperation.GetAll(c => c.CustomerId == cart.CustomerId && c.RoomId == cart.RoomId).LastOrDefault();
            if (exitingHousekeepingCart == null)
                throw new InvalidOperationException("CART_NOT_FOUND");

            foreach (var hKeepingCartItem in cart.HousekeepingCartItems)
            {
                var housekeepingFacilityDetail = _houseKeepingFacilityDetailsOperations.GetById(hKeepingCartItem.HouseKeepingFacilityDetailsId);
                if (housekeepingFacilityDetail == null)
                    throw new ArgumentException("INVALID_HOUSEKEEPING_FACILITY_DETAIL_ID");

                HousekeepingCartItem existItem;
                existItem = exitingHousekeepingCart.HousekeepingCartItems.Where(i => i.Id == hKeepingCartItem.Id && i.HouseKeepingFacilityDetailsId == hKeepingCartItem.HouseKeepingFacilityDetailsId).FirstOrDefault();

                if (existItem == null)
                {
                    if (hKeepingCartItem.ScheduleDate != null && hKeepingCartItem.ScheduleDate <= DateTime.Now)
                        throw new ArgumentException(Resources.Invalid_ScheduleDate);

                    if (hKeepingCartItem.Quantity <= 0 || hKeepingCartItem.Quantity == null)
                    {
                        hKeepingCartItem.Quantity = 1;
                    }
                    hKeepingCartItem.Total = hKeepingCartItem.Quantity * (housekeepingFacilityDetail.Price == null ? 0 : housekeepingFacilityDetail.Price);

                    // check for cart additional

                    double additionalResult = 0.00f;
                    foreach (var additionalElement in hKeepingCartItem.HousekeepingCartItemAdditionals)
                    {
                        if (additionalElement.Qty == null || additionalElement.Qty == 0)
                            additionalElement.Qty = 1;

                        var details = _housekeepingAdditionalElementsRepository.GetById(additionalElement.HousekeepingAdditionalElementId);
                        if (details != null)
                            additionalElement.Total = (details.Price == null ? 0 : details.Price) * additionalElement.Qty;
                        additionalResult += Convert.ToDouble(additionalElement.Total);
                    }
                    hKeepingCartItem.Total += additionalResult;

                    exitingHousekeepingCart.Total += hKeepingCartItem.Total;
                    exitingHousekeepingCart.HousekeepingCartItems.Add(hKeepingCartItem);
                }

                //existItem not NULL
                else
                {
                    if (hKeepingCartItem.Quantity <= 0) //If existItem Quantity set to 0 then delete
                    {
                        if (hKeepingCartItem.Quantity == 0 && hKeepingCartItem.Id == existItem.Id)
                        {
                            if (exitingHousekeepingCart.HousekeepingCartItems.Count() == 1)
                                exitingHousekeepingCart.Total = 0;
                            else
                                exitingHousekeepingCart.Total -= existItem.Total;
                            _housekeepingCartItemRepository.Delete(existItem.Id);
                        }
                        else
                        {
                            throw new NegativeItemQtyException("NEGATIVE_ITEM_QTY");
                        }
                    }
                    else
                    {
                        if (hKeepingCartItem.ScheduleDate != null && existItem.ScheduleDate != hKeepingCartItem.ScheduleDate && hKeepingCartItem.ScheduleDate <= DateTime.Now)
                            throw new ArgumentException(Resources.Invalid_ScheduleDate);

                        existItem.Quantity = hKeepingCartItem.Quantity;
                        existItem.Comment = hKeepingCartItem.Comment;
                        existItem.ScheduleDate = hKeepingCartItem.ScheduleDate;

                        exitingHousekeepingCart.Total -= existItem.Total;
                        existItem.Total = hKeepingCartItem.Quantity * (housekeepingFacilityDetail.Price == null ? 0 : housekeepingFacilityDetail.Price);

                        // check for cart additional
                        double additionalResult = 0.00f;
                        foreach (var additionalItem in hKeepingCartItem.HousekeepingCartItemAdditionals)
                        {
                            var details = _housekeepingAdditionalElementsRepository.GetById(additionalItem.HousekeepingAdditionalElementId);
                            if (details != null)
                                additionalItem.Total = (details.Price == null ? 0 : details.Price) * additionalItem.Qty;
                            additionalResult += Convert.ToDouble(additionalItem.Total);

                            foreach (var exitAdditionalItem in existItem.HousekeepingCartItemAdditionals)
                            {
                                if (exitAdditionalItem.HousekeepingAdditionalElementId == additionalItem.HousekeepingAdditionalElementId)
                                {
                                    exitAdditionalItem.Qty = additionalItem.Qty;
                                    exitAdditionalItem.Total = additionalItem.Total;
                                }
                            }
                        }
                        existItem.Total += additionalResult;

                        var newAdd = hKeepingCartItem.HousekeepingCartItemAdditionals
                        .Where(i => !existItem.HousekeepingCartItemAdditionals.Any(old => old.HousekeepingAdditionalElementId == i.HousekeepingAdditionalElementId));

                        var delAdd = existItem.HousekeepingCartItemAdditionals
                            .Where(old => !hKeepingCartItem.HousekeepingCartItemAdditionals.Any(nw => nw.HousekeepingAdditionalElementId == old.HousekeepingAdditionalElementId));

                        foreach (var del in delAdd)
                        {
                            _housekeepingCartItemAdditionalRepository.Delete(del.Id);
                        }

                        foreach (var add in newAdd)
                        {
                            existItem.HousekeepingCartItemAdditionals.Add(new HousekeepingCartItemAdditional { HousekeepingAdditionalElementId = add.HousekeepingAdditionalElementId, Qty = add.Qty, Total = add.Total });
                        }

                        exitingHousekeepingCart.Total += existItem.Total;
                    }
                }
            }

            _housekeepingCartOperation.Update(exitingHousekeepingCart);

            var newRepo = new EfRepository<HousekeepingCart>();
            var customerCart = newRepo.GetById(exitingHousekeepingCart.Id);

            customerCart.HousekeepingCartItems = customerCart.HousekeepingCartItems.OrderBy(c => c.ScheduleDate).ToList();
            return customerCart;
        }

        #endregion Housekeeping Cart

        #region Housekeeping Order

        // Api
        /// <summary>
        /// Gets the housekeeping order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>HousekeepingOrder.</returns>
        public HousekeepingOrder GetHousekeepingOrderById(int orderId)
        {
            return _housekeepingOrderOperation.GetById(l => l.Id == orderId);
        }

        /// <summary>
        /// Creates the housekeeping order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>HousekeepingOrder.</returns>
        /// <exception cref="ArgumentException">
        /// INVALID_CUSTOMER_Id
        /// or
        /// INVALID_CARD_Id
        /// or
        /// </exception>
        /// <exception cref="CartEmptyException">CART_IS_EMPTY</exception>
        public HousekeepingOrder CreateHousekeepingOrderFromCart(int customerId, int cardId, int roomId)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_CUSTOMER_Id");

            var housekeepingCart = _housekeepingCartOperation.GetById(c => c.CustomerId == customerId && c.RoomId == roomId);

            if (housekeepingCart == null || !housekeepingCart.HousekeepingCartItems.Any())
                throw new CartEmptyException("CART_IS_EMPTY");

            var card = _customerCreditCardRepository.GetById(cardId);
            if (card == null || card.CustomerId != customerId)
                throw new ArgumentException("INVALID_CARD_Id");

            HousekeepingOrder order = new HousekeepingOrder
            {
                CustomerId = customerId,
                RoomId = housekeepingCart.RoomId,
                CardId = cardId,
                OrderStatus = 0,
                PaymentStatus = false,
                OrderTotal = housekeepingCart.Total,
                CreationDate = DateTime.Now
            };

            foreach (var item in housekeepingCart.HousekeepingCartItems)
            {
                if (item.ScheduleDate != null && item.ScheduleDate < DateTime.Now)
                    throw new ArgumentException(Resources.Invalid_ScheduleDate);

                HousekeepingOrderItem housekeepingOrderItem = new HousekeepingOrderItem
                {
                    HouseKeepingFacilityDetailsId = item.HouseKeepingFacilityDetailsId,
                    Quantity = item.Quantity,
                    Comment = item.Comment,
                    Total = item.Total,
                    ScheduleDate = Convert.ToDateTime(item.ScheduleDate == null ? DateTime.Now.AddHours(1) : item.ScheduleDate),
                    HousekeepingOrderStatus = false,
                    IsActive = true
                };

                if (item.HousekeepingCartItemAdditionals != null && item.HousekeepingCartItemAdditionals.Any())
                {
                    foreach (var add in item.HousekeepingCartItemAdditionals)
                    {
                        HousekeepingOrderItemAdditional additional = new HousekeepingOrderItemAdditional()
                        {
                            HousekeepingAdditionalElementId = add.HousekeepingAdditionalElementId,
                            Qty = add.Qty,
                            Total = add.Total
                        };
                        housekeepingOrderItem.HousekeepingOrderItemAdditionals.Add(additional);
                    }
                }
                order.HousekeepingOrderItems.Add(housekeepingOrderItem);
            }
            _housekeepingOrderRepository.InsertWithChild(order);
            _housekeepingCartRepository.Delete(housekeepingCart.Id);

            EfRepository<HousekeepingOrder> newRepo = new EfRepository<HousekeepingOrder>();

            var customerHousekeepingOrder = newRepo.GetById(order.Id);
            customerHousekeepingOrder.HousekeepingOrderItems = customerHousekeepingOrder.HousekeepingOrderItems.OrderBy(c => c.ScheduleDate).ToList();
            return customerHousekeepingOrder;
        }

        /// <summary>
        /// Gets the customer active housekeeping orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveHousekeepingOrdersAndBaskets.</returns>
        public CustomerActiveHousekeepingOrdersAndBaskets GetCustomerActiveHousekeepingOrders(int customerId)
        {
            var activeOrderStatuses = new int[] { 0, 1 };
            var activeHousekeepingOrders = _housekeepingOrderOperation.GetAll(o => o.CustomerId == customerId && activeOrderStatuses.Contains(o.OrderStatus.Value)).ToList();
            var activeHousekeepingBaskets = _housekeepingCartOperation.GetAll(c => c.CustomerId == customerId && c.Total > 0).ToList();
            CustomerActiveHousekeepingOrdersAndBaskets activeHousekeepingOrdersAndBaskets = new CustomerActiveHousekeepingOrdersAndBaskets()
            {
                ActiveHousekeepingOrders = activeHousekeepingOrders,
                ActiveHousekeepingBaskets = activeHousekeepingBaskets
            };
            return activeHousekeepingOrdersAndBaskets;
        }

        // Web
        /// <summary>
        /// Gets the housekeeping orders queue.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingOrder.</returns>
        public IEnumerable<HousekeepingOrder> GetHousekeepingOrdersQueue(int hotelId, int skip, int pageSize, out int total)
        {
            var orderStauses = new int[] { 0, 1 };
            return _housekeepingOrderOperation.GetAllWithServerSidePaggingDesc(l => l.Room.HotelId == hotelId && orderStauses.Contains(l.OrderStatus.Value), o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Modifies the housekeeping order.
        /// </summary>
        /// <param name="housekeepingOrder">The housekeeping order.</param>
        /// <returns>HousekeepingOrder.</returns>
        public HousekeepingOrder ModifyHousekeepingOrder(HousekeepingOrder housekeepingOrder)
        {
            _housekeepingOrderOperation.Update(housekeepingOrder.Id, housekeepingOrder);
            return housekeepingOrder;
        }

        /// <summary>
        /// Gets the housekeeping order item details.
        /// </summary>
        /// <param name="housekeepingOrderId">The housekeeping order identifier.</param>
        /// <returns>IEnumerable&lt;HousekeepingOrderItem&gt;.</returns>
        public IEnumerable<HousekeepingOrderItem> GetHousekeepingOrderItemDetails(int housekeepingOrderId)
        {
            return _housekeepingOrderItemOperation.GetAll(o => o.HousekeepingOrderId == housekeepingOrderId);
        }

        /// <summary>
        /// Updates the housekeeping order item deliver status.
        /// </summary>
        /// <param name="housekeepingOrderItemId">The housekeeping order item identifier.</param>
        public void UpdateHousekeepingOrderItemDeliverStatus(int housekeepingOrderItemId)
        {
            var housekeepingOrderItem = _housekeepingOrderItemOperation.GetById(housekeepingOrderItemId);
            if (housekeepingOrderItem.HousekeepingOrderStatus == true)
                housekeepingOrderItem.HousekeepingOrderStatus = false;
            else
                housekeepingOrderItem.HousekeepingOrderStatus = true;
            _housekeepingOrderItemOperation.Update(housekeepingOrderItem);
        }

        /// <summary>
        /// Gets the housekeeping order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingOrder.</returns>
        public IEnumerable<HousekeepingOrder> GetHousekeepingOrderHistory(string From, string To, int hotelId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total)
        {
            var query = _housekeepingOrderRepository.Table;
            if (hotelId > 0)
            {
                query = query.Where(o => o.Room.HotelId == hotelId);
            }
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.OrderTotal >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.OrderTotal <= maxAmount);
            }
            var pages = query.OrderByDescending(o => o.Id)
                      .Skip(skip)
                      .Take(pageSize)
                      .GroupBy(p => new { Total = query.Count() })
                      .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HousekeepingOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets the customer housekeeping orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable housekeepingOrder.</returns>
        public IEnumerable<HousekeepingOrder> GetCustomerHousekeepingOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total)
        {
            var query = _housekeepingOrderRepository.Table;
            if (month != 0)
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year && o.CreationDate.Month == month);
            else
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<HousekeepingOrder>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        #endregion Housekeeping Order

        #region Manage HotelRoom Customers

        /// <summary>
        /// Gets the rooms active customers.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelRoom.</returns>
        public IEnumerable<CustomerHotelRoom> GetRoomsActiveCustomers(int hotelId, int skip, int pageSize, out int total)
        {
            //Web
            return _customerHotelRoomOperation.GetAllWithServerSidePagging(s => s.Room.HotelId == hotelId && s.IsActive, s => s.Id, skip, pageSize, out total);
        }

        #endregion Manage HotelRoom Customers

        #region HotelRoom WakeUp Call

        /// <summary>
        /// Gets the hotel room wake up call.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable WakeUp.</returns>
        public IEnumerable<WakeUp> GetHotelRoomWakeUpCall(int hotelId, int skip, int pageSize, out int total)
        {
            //web 
            return _wakeUpOperation.GetAllWithServerSidePagging(s => s.Room.HotelId == hotelId && s.IsCompleted == false, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Modifies the wake up call.
        /// </summary>
        /// <param name="WakeUpId">The wake up identifier.</param>
        public void ModifyWakeUpCall(int WakeUpId)
        {
            var wakeUp = _wakeUpOperation.GetById(WakeUpId);
            wakeUp.IsCompleted = true;

            _wakeUpOperation.Update(wakeUp);
        }

        #endregion HotelRoom WakeUp Call

        //Room Reviews
        /// <summary>
        /// Gets the reviews by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>IEnumerable RoomReview.</returns>
        public IEnumerable<RoomReview> GetReviewsByRoomId(int RoomId)
        {
            return _hotelRoomReviewsOperation.GetAll(r => r.RoomId == RoomId);
        }

        /// <summary>
        /// Creates the room review.
        /// </summary>
        /// <param name="review">The review.</param>
        /// <returns>RoomReview.</returns>
        /// <exception cref="ArgumentException">RoomId doesn't exists</exception>
        public RoomReview CreateRoomReview(RoomReview review)
        {
            var roomDetails = _hotelRoomsOperation.GetById(review.RoomId);
            if (roomDetails == null)
                throw new ArgumentException("RoomId doesn't exists");
            review.CreationDate = DateTime.UtcNow;
            return _hotelRoomReviewsOperation.AddNew(review);
        }

        /// <summary>
        /// Creates the hotel bookings.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>CustomerHotelBooking.</returns>
        /// <exception cref="ArgumentException">
        /// NO_ROOM_FOUND_FOR_THIS_ID
        /// or
        /// NO_CUSTOMER_CREDIT_CARD-FOUND-FOR-THIS-ID
        /// or
        /// Room not available for this date
        /// </exception>
        public CustomerHotelBooking CreateHotelBookings(CustomerHotelBooking hotelBooking)
        {
            var hotelRoom = _hotelRoomsOperation.GetById(hr => hr.Id == hotelBooking.RoomId);
            if (hotelRoom == null)
                throw new ArgumentException("NO_ROOM_FOUND_FOR_THIS_ID");

            var creditcardDetails = _customerCreditCardOpration.GetById(c => c.Id == hotelBooking.CreditCardId && c.CustomerId == hotelBooking.CustomerId);
            if (creditcardDetails == null)
                throw new ArgumentException("NO_CUSTOMER_CREDIT_CARD-FOUND-FOR-THIS-ID");

            var available = _hotelBookingOperation.GetAll(hb => hb.RoomId == hotelBooking.RoomId && ((hb.From <= hotelBooking.From && hb.To >= hotelBooking.From) ||
                                                                                                   (hb.From <= hotelBooking.To && hb.To >= hotelBooking.To) ||
                                                                                                   (hotelBooking.From <= hb.From && hotelBooking.To >= hb.To))).FirstOrDefault();

            if (available != null)
                throw new ArgumentException("Room not available for this date");

            //var rate = hotelRoom.Rate == null ? 0 : hotelRoom.Rate;
            //TimeSpan diff = hotelBooking.To - hotelBooking.From;
            //int days = (int)Math.Abs(Math.Round(diff.TotalDays));
            //if (diff.Hours >= 1 && diff.Minutes > 0)
            //    days++;

            //hotelBooking.Total = (rate * days);
            hotelBooking.StatusId = 1;
            hotelBooking.CreationDate = DateTime.UtcNow;
            return _hotelBookingOperation.AddNew(hotelBooking);
        }

        /// <summary>
        /// Gets the hotel room booking by identifier.
        /// </summary>
        /// <param name="bookingId">The booking identifier.</param>
        /// <returns>CustomerHotelBooking.</returns>
        public CustomerHotelBooking GetHotelRoomBookingByID(int bookingId)
        {
            return _hotelBookingOperation.GetById(bookingId);
        }

        /// <summary>
        /// Modifies the hotel bookings.
        /// </summary>
        /// <param name="hotelBooking">The hotel booking.</param>
        /// <returns>CustomerHotelBooking.</returns>
        /// <exception cref="ArgumentException">
        /// HOTEL_BOOKING_NOT_FOUND
        /// or
        /// NO_ROOM_FOUND_FOR_THIS_ID
        /// or
        /// NO_CUSTOMER_CREDIT_CARD-FOUND-FOR-THIS-ID
        /// or
        /// Room not available for this date
        /// </exception>
        public CustomerHotelBooking ModifyHotelBookings(CustomerHotelBooking hotelBooking)
        {
            var existingBooking = _hotelBookingOperation.GetById(b => b.Id == hotelBooking.Id);
            if (existingBooking == null)
                throw new ArgumentException("HOTEL_BOOKING_NOT_FOUND");

            var hotelRoom = _hotelRoomsOperation.GetById(hr => hr.Id == hotelBooking.RoomId);
            if (hotelRoom == null)
                throw new ArgumentException("NO_ROOM_FOUND_FOR_THIS_ID");

            var creditcardDetails = _customerCreditCardOpration.GetById(c => c.Id == hotelBooking.CreditCardId && c.CustomerId == hotelBooking.CustomerId);
            if (creditcardDetails == null)
                throw new ArgumentException("NO_CUSTOMER_CREDIT_CARD-FOUND-FOR-THIS-ID");

            var available = _hotelBookingOperation.GetAll(hb => hb.RoomId == hotelBooking.RoomId && ((hb.From <= hotelBooking.From && hb.To >= hotelBooking.From) ||
                                                                                                     (hb.From <= hotelBooking.To && hb.To >= hotelBooking.To) ||
                                                                                                     (hotelBooking.From <= hb.From && hotelBooking.To >= hb.To))).FirstOrDefault();

            if (available != null)
                throw new ArgumentException("Room not available for this date");

            existingBooking.RoomId = hotelBooking.RoomId;
            existingBooking.CreditCardId = hotelBooking.CreditCardId;
            existingBooking.From = hotelBooking.From;
            existingBooking.To = hotelBooking.To;
            existingBooking.CreationDate = DateTime.UtcNow;

            _hotelBookingOperation.Update(existingBooking);
            return existingBooking;
        }

        /// <summary>
        /// Gets all room booking by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetAllRoomBookingByCustomer(int customerId)
        {
            return _hotelBookingOperation.GetAll(hb => hb.CustomerId == customerId);
        }

        /// <summary>
        /// Gets the todays booking.
        /// </summary>
        /// <returns>IEnumerable ustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetTodaysBooking()
        {
            var query = _hotelBookingRepository.Table;
            var todayDate = DateTime.Now;
            return query.Where(o => DbFunctions.TruncateTime(o.CreationDate) == DbFunctions.TruncateTime(todayDate)).ToList();
        }

        /// <summary>
        /// Gets all hotel room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetAllHotelRoomBookings(int hotelId, int pageNumber, int pageSize, out int total)
        {
            return _hotelBookingOperation.GetAllByDesc(hb => hb.Room.HotelId == hotelId, hb => hb.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Gets all current room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetAllCurrentRoomBookings(int hotelId, int pageNumber, int pageSize, out int total)
        {
            var currentDate = DateTime.Now;
            return _hotelBookingOperation.GetAllByDesc(hb => hb.Room.HotelId == hotelId && hb.From.Date >= currentDate.Date, hb => hb.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Deletes the room booking.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRoomBooking(int id)
        {
            _hotelBookingOperation.Delete(id);
        }

        /// <summary>
        /// Gets the todays room bookings.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetTodaysRoomBookings(int hotelId, int skip, int pageSize, out int total)
        {
            var query = _hotelBookingRepository.Table;
            query = query.Where(h => h.Room.HotelId == hotelId && DbFunctions.TruncateTime(h.From) == DbFunctions.TruncateTime(DateTime.Now));
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<CustomerHotelBooking>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the pending reviews hotels.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetPendingReviewsHotels(int customerId, int pageNumber, int pageSize, out int total)
        {
            //var customerHotels = _hotelOperations.GetAll(h => h.Room.Any(r => r.CustomerHotelRoom.Any(b => b.CustomerId == customerId)) && h.HotelReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
            var customerHotels = _hotelOperations.GetAll(h => (h.Room.Any(r => r.HousekeepingOrder.Any(b => b.CustomerId == customerId) || r.LaundryOrder.Any(b => b.CustomerId == customerId) || r.WakeUp.Any(b => b.CustomerId == customerId)) || h.SpaOrder.Any(b => b.CustomerId == customerId) || h.ExcursionOrder.Any(b => b.CustomerId == customerId)) && h.HotelReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
            return customerHotels;
        }



        /// <summary>
        /// Gets the customer hotel reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetCustomerHotelReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            return _hotelReviewsOperations.GetAllByDesc(rw => rw.CustomerId == customerId, h => h.Id, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Gets the customer active bookings in hotel.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveBookingsInHotel.</returns>
        public CustomerActiveBookingsInHotel GetCustomerActiveBookingsInHotel(int customerId)
        {
            var RoomBooked = _hotelBookingOperation.GetAll(hb => hb.CustomerId == customerId).ToList();
            var spaOrder = _spaOrderOperation.GetAll(o => o.CustomerId == customerId && (o.OrderStatus != 2 || o.OrderStatus != 3)).ToList();
            //var LaundryBooking = _hotelLaundryBookingOperations.GetAll(c => c.CustomerId == customerId).ToList();
            //var TaxiBooking = _taxiBookingOperations.GetAll(c => c.CustomerId == customerId).ToList();
            //var TripBooking = _hotelTripBookingOperations.GetAll(c => c.CustomerId == customerId).ToList();
            CustomerActiveBookingsInHotel activeBookingsInHotel = new CustomerActiveBookingsInHotel()
            {
                CustomerHotelBooking = RoomBooked,
                SpaOrder = spaOrder
                //LaundryBooking = LaundryBooking,
                //TaxiBooking = TaxiBooking,
                //TripBooking = TripBooking

            };
            return activeBookingsInHotel;
        }

        /// <summary>
        /// Gets the name of all hotels by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="hotel_name">Name of the hotel.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetAllHotelsByName(int customerId, string hotel_name)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            var query = _hotelDetailsRepository.Table.Where(h => h.Status == true && h.IsActive == true);
            if (customerRole != null)
            {
                if (customerRole.RoleId == 1)
                {
                    return query.Where(h => h.Name.Contains(hotel_name)).ToList();
                }
                else
                {
                    var hotelIds = _chainDetailsTableRepository.Table.Where(c => c.ChainId == customerRole.ChainId && c.HotelId != null).Select(c => c.HotelId).ToList();
                    return query.Where(h => h.Name.Contains(hotel_name) && hotelIds.Contains(h.Id)).ToList();
                }
            }
            else
            {
                return query.ToList();
            }
        }


        /// <summary>
        /// Hotels the authorized customers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> HotelAuthorizedCustomers(int id)
        {
            return _customerRoleOperations.GetAll(r => r.HotelId == id);
        }

        /// <summary>
        /// Authorizeds the hotel customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> AuthorizedHotelCustomers(int customerId, int id)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            if (customerRole.RoleId == 1)
            {
                return _customerRoleOperations.GetAll(h => h.HotelId == id || h.HotelId == null);
            }
            else
            {
                var chainRestaurantId = (from chainDetails in _chainDetailsTableRepository.Table
                                         where chainDetails.RestaurantId != null && chainDetails.ChainId == customerRole.ChainId
                                         select chainDetails.RestaurantId).ToArray();

                var chainHotelId = (from chainDetails in _chainDetailsTableRepository.Table
                                    where chainDetails.HotelId != null && chainDetails.ChainId == customerRole.ChainId && chainDetails.HotelId == id
                                    select chainDetails.HotelId).ToArray();

                return _customerRoleOperations.GetAll(c => c.CustomerId == customerId || (chainRestaurantId.Contains(c.RestaurantId)) || (chainHotelId.Contains(c.HotelId)));
            }
        }
        /// <summary>
        /// Gets the un assign chain hotel.
        /// </summary>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetUnAssignChainHotel()
        {
            var chainDetailsTable = _chainDetailsTableRepository.Table.Select(c => c.HotelId).ToArray();

            var hotel = (from hot in _hotelDetailsRepository.Table.Where(h => h.Status && h.IsActive)
                         where (!chainDetailsTable.Contains(hot.Id))
                         select (hot));

            return hotel;
        }

        /// <summary>
        /// Gets the assign chain hotel.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetAssignChainHotel(int id)
        {
            var chainDetailsTable = _chainDetailsTableRepository.Table.Where(c => c.ChainId != id).Select(c => c.HotelId).ToArray();

            var hotel = (from hot in _hotelDetailsRepository.Table
                         where (!chainDetailsTable.Contains(hot.Id))
                         select (hot));

            return hotel;
        }

        /// <summary>
        /// Gets the chain recent hotels.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Hotel.</returns>
        public IEnumerable<Hotel> GetChainRecentHotels(int id)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == id).ChainId;
            var chainDetailsTable = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.HotelId != null).Select(c => c.HotelId).ToList();

            var query = _hotelBookingRepository.Table;
            var todayDate = DateTime.Now;
            return query.Where(o => DbFunctions.TruncateTime(o.From) <= DbFunctions.TruncateTime(todayDate)
                                    && DbFunctions.TruncateTime(o.To) >= DbFunctions.TruncateTime(todayDate)
                                    && chainDetailsTable.Contains(o.Room.HotelId)).Select(h => h.Room.Hotel).ToList();
        }

        /// <summary>
        /// Gets the chain hotel recent orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable CustomerHotelBooking.</returns>
        public IEnumerable<CustomerHotelBooking> GetChainHotelRecentOrders(int customerId, int skip, int pageSize, out int total)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            var Hotels = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.HotelId != null).Select(c => c.HotelId).ToList();

            var query = _hotelBookingRepository.Table.Where(hb => Hotels.Contains(hb.Room.HotelId));

            var toDt = DateTime.Now;
            var fromDt = toDt.AddDays(-30);

            query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDt)
                                                                && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(toDt));

            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<CustomerHotelBooking>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole CreateCustomerRole(CustomerRole role)
        {
            return _customerRoleOperations.AddNew(role);
        }
        /// <summary>
        /// Gets all role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        public IEnumerable<Role> GetAllRole()
        {
            return _roleRepository.Table;
        }
        /// <summary>
        /// Gets all hotel admin access role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        public IEnumerable<Role> GetAllHotelAdminAccessRole()
        {
            List<int> notAuthorizedRole = new List<int>() { 1, 2, 3, 4, 5, 8 };
            return _roleRepository.Table.Where(r => !notAuthorizedRole.Contains(r.Id));
        }
        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetCustomers(string searchText)
        {
            var customerRole = (from role in _customerRoleRepository.Table
                                select role.CustomerId).ToArray();

            var customer = _customerRepository.Table
                       .Where(c => c.FirstName.Contains(searchText) && !customerRole.Contains(c.Id) && c.ActiveStatus == true);

            return customer;
        }
        /// <summary>
        /// Gets the customer role.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> GetCustomerRole(int customerId)
        {
            return _customerRoleOperations.GetAll(r => r.CustomerId == customerId);
        }
        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole UpdateCustomerRole(CustomerRole role)
        {
            _customerRoleOperations.Update(role.Id, role);
            return role;
        }
        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCustomerRole(int id)
        {
            _customerRoleOperations.Delete(id);
        }
        /// <summary>
        /// Gets the customer by role identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole GetCustomerByRoleId(int Id)
        {
            return _customerRoleOperations.GetById(Id);
        }

        #region Room types
        //Room types
        /// <summary>
        /// Gets all room types.
        /// </summary>
        /// <returns>IEnumerable RoomType.</returns>
        public IEnumerable<RoomType> GetAllRoomTypes()
        {
            return _roomTypeRepository.Table;
        }
        /// <summary>
        /// Gets the room type by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RoomType.</returns>
        public RoomType GetRoomTypeById(int id)
        {
            return _roomTypeOperations.GetById(id);
        }
        /// <summary>
        /// Creates the type of the room.
        /// </summary>
        /// <param name="roomType">Type of the room.</param>
        /// <returns>RoomType.</returns>
        public RoomType CreateRoomType(RoomType roomType)
        {
            roomType.CreationDate = DateTime.UtcNow;
            return _roomTypeOperations.AddNew(roomType);
        }
        /// <summary>
        /// Modifies the type of the room.
        /// </summary>
        /// <param name="roomType">Type of the room.</param>
        /// <returns>RoomType.</returns>
        public RoomType ModifyRoomType(RoomType roomType)
        {
            _roomTypeOperations.Update(roomType.Id, roomType);
            return roomType;
        }
        /// <summary>
        /// Deletes the type of the room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRoomType(int id)
        {
            _roomTypeOperations.Delete(id);
        }
        #endregion Room types

        #region Hotel Rooms
        //Hotel Rooms
        /// <summary>
        /// Creates the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>Room.</returns>
        public Room CreateHotelRoom(Room room)
        {
            return _roomOperations.AddNew(room);
        }
        /// <summary>
        /// Gets the hotel room.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="RoomNumber">The room number.</param>
        /// <returns>IEnumerable Room.</returns>
        public IEnumerable<Room> GetHotelRoom(int HotelId, string RoomNumber)
        {
            return _hotelRoomsOperation.GetAll(r => r.HotelId == HotelId && r.Number == RoomNumber);
        }

        /// <summary>
        /// Updates the hotel room.
        /// </summary>
        /// <param name="room">The room.</param>
        /// <returns>Room.</returns>
        /// <exception cref="ArgumentException">Room Number already exist</exception>
        public Room UpdateHotelRoom(Room room)
        {
            var existing = _roomOperations.GetAll(r => r.HotelId == room.HotelId && r.Number == room.Number && r.Id != room.Id);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Room Number already exist");

            _roomOperations.Update(room.Id, room);
            return room;
        }
        /// <summary>
        /// Gets all hotel roooms by hotel identifier.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Room.</returns>
        public IEnumerable<Room> GetAllHotelRooomsByHotelId(int HotelId)
        {
            return _hotelRoomsOperation.GetAll(r => r.HotelId == HotelId);
        }
        /// <summary>
        /// Deletes the hotel room.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHotelRoom(int id)
        {
            _roomRepository.Delete(id);
        }
        /// <summary>
        /// Gets all hotel roooms by identifier.
        /// </summary>
        /// <param name="HotelId">The hotel identifier.</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns>Room.</returns>
        public Room GetAllHotelRooomsById(int HotelId, string roomNumber)
        {
            return _hotelRoomsOperation.GetAll(r => r.HotelId == HotelId && r.Number == roomNumber && r.IsActive).FirstOrDefault();
        }

        /// <summary>
        /// Gets the rooom details by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>Room.</returns>
        public Room GetRooomDetailsByRoomId(int RoomId)
        {
            return _hotelRoomsOperation.GetById(RoomId);
        }

        /// <summary>
        /// Gets the hotel room by room identifier.
        /// </summary>
        /// <param name="RoomId">The room identifier.</param>
        /// <returns>Room.</returns>
        public Room GetHotelRoomByRoomId(int RoomId)
        {
            return _hotelRoomsOperation.GetById(r => r.Id == RoomId && r.IsActive);
        }
        #endregion Hotel Rooms

        #region Room Rate
        //Room Rate
        /// <summary>
        /// Creates the hotel room rate.
        /// </summary>
        /// <param name="roomRate">The room rate.</param>
        /// <returns>RoomRate.</returns>
        public RoomRate CreateHotelRoomRate(RoomRate roomRate)
        {
            return _hotelRoomRateOperations.AddNew(roomRate);
        }
        /// <summary>
        /// Gets the room rates by room identifier.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>RoomRate.</returns>
        public RoomRate GetRoomRatesByRoomId(int roomId)
        {
            return _hotelRoomRateOperations.GetById(r => r.RoomId == roomId);
        }
        /// <summary>
        /// Modifies the room rate.
        /// </summary>
        /// <param name="roomRate">The room rate.</param>
        /// <returns>RoomRate.</returns>
        public RoomRate ModifyRoomRate(RoomRate roomRate)
        {
            _hotelRoomRateOperations.Update(roomRate.Id, roomRate);
            return roomRate;
        }
        #endregion Room Rate

        #region Room categories
        //Room categories
        /// <summary>
        /// Gets all room categories.
        /// </summary>
        /// <returns>IEnumerable RoomCategory.</returns>
        public IEnumerable<RoomCategory> GetAllRoomCategories()
        {
            return _hotelRoomCategoryOperation.GetAll();
        }
        /// <summary>
        /// Gets all room categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomCategory.</returns>
        public IEnumerable<RoomCategory> GetAllRoomCategories(int skip, int pageSize, out int total)
        {
            return _hotelRoomCategoryOperation.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the room category by identifier.
        /// </summary>
        /// <param name="roomCatId">The room cat identifier.</param>
        /// <returns>RoomCategory.</returns>
        public RoomCategory GetRoomCategoryById(int roomCatId)
        {
            return _hotelRoomCategoryOperation.GetById(roomCatId);
        }

        /// <summary>
        /// Creates the room category.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <returns>RoomCategory.</returns>
        public RoomCategory CreateRoomCategory(RoomCategory roomCategory)
        {
            roomCategory.CreationDate = DateTime.UtcNow;
            return _hotelRoomCategoryOperation.AddNew(roomCategory);
        }
        /// <summary>
        /// Modifies the room category.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <returns>RoomCategory.</returns>
        public RoomCategory ModifyRoomCategory(RoomCategory roomCategory)
        {
            _hotelRoomCategoryOperation.Update(roomCategory.Id, roomCategory);
            return roomCategory;
        }
        /// <summary>
        /// Deletes the room category.
        /// </summary>
        /// <param name="roomCatId">The room cat identifier.</param>
        public void DeleteRoomCategory(int roomCatId)
        {
            _hotelRoomCategoryOperation.Delete(roomCatId);
        }
        #endregion Room categories

        #region Room Capacity
        //Room Capacity
        /// <summary>
        /// Gets all room capacities.
        /// </summary>
        /// <returns>IEnumerable RoomCapacity.</returns>
        public IEnumerable<RoomCapacity> GetAllRoomCapacities()
        {
            return _hotelRoomCapacityOperations.GetAll();
        }
        /// <summary>
        /// Gets all room capacities.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomCapacity.</returns>
        public IEnumerable<RoomCapacity> GetAllRoomCapacities(int skip, int pageSize, out int total)
        {
            return _hotelRoomCapacityOperations.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the room capacity by identifier.
        /// </summary>
        /// <param name="roomCapId">The room cap identifier.</param>
        /// <returns>RoomCapacity.</returns>
        public RoomCapacity GetRoomCapacityById(int roomCapId)
        {
            return _hotelRoomCapacityOperations.GetById(roomCapId);
        }

        /// <summary>
        /// Creates the room capacity.
        /// </summary>
        /// <param name="roomCapacity">The room capacity.</param>
        /// <returns>RoomCapacity.</returns>
        public RoomCapacity CreateRoomCapacity(RoomCapacity roomCapacity)
        {
            roomCapacity.CreationDate = DateTime.UtcNow;
            return _hotelRoomCapacityOperations.AddNew(roomCapacity);
        }
        /// <summary>
        /// Modifies the room capacity.
        /// </summary>
        /// <param name="roomCapacity">The room capacity.</param>
        /// <returns>RoomCapacity.</returns>
        public RoomCapacity ModifyRoomCapacity(RoomCapacity roomCapacity)
        {
            _hotelRoomCapacityOperations.Update(roomCapacity.Id, roomCapacity);
            return roomCapacity;
        }
        /// <summary>
        /// Deletes the room capacity.
        /// </summary>
        /// <param name="roomCapId">The room cap identifier.</param>
        public void DeleteRoomCapacity(int roomCapId)
        {
            _hotelRoomCapacityOperations.Delete(roomCapId);
        }
        #endregion Room Capacity

        #region Room Amenities
        //Room Amenities
        /// <summary>
        /// Gets all room amenities.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomAmenities.</returns>
        public IEnumerable<RoomAmenities> GetAllRoomAmenities(int roomId, int skip, int pageSize, out int total)
        {
            return _hotelRoomAmenitiesOperations.GetAllWithServerSidePagging(s => s.RoomId == roomId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Deletes the room amenities.
        /// </summary>
        /// <param name="roomAmenitiesId">The room amenities identifier.</param>
        public void DeleteRoomAmenities(int roomAmenitiesId)
        {
            _hotelRoomAmenitiesOperations.Delete(roomAmenitiesId);
        }

        /// <summary>
        /// Gets the unassign amenities.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>IEnumerable Amenities.</returns>
        public IEnumerable<Amenities> GetUnassignAmenities(int roomId)
        {
            var assignAmenitiesIds = _hotelRoomAmenitiesOperations.GetAll(s => s.RoomId == roomId).Select(sd => sd.AmenitiesId).Distinct();
            var unassignAmenities = _amenitiesRepository.Table.Where(r => !assignAmenitiesIds.Contains(r.Id));
            return unassignAmenities;
        }

        /// <summary>
        /// Creates the room amenities.
        /// </summary>
        /// <param name="roomAmenities">The room amenities.</param>
        public void CreateRoomAmenities(List<RoomAmenities> roomAmenities)
        {
            foreach (var eachRoomAmenities in roomAmenities)
            {
                eachRoomAmenities.CreationDate = DateTime.UtcNow;
                _hotelRoomAmenitiesOperations.AddNew(eachRoomAmenities);
            }
        }
        #endregion Room Amenities

        #region Room Images
        //Hotel Room Images
        /// <summary>
        /// Gets all room images.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <returns>IEnumerable HotelRoomImages.</returns>
        public IEnumerable<HotelRoomImages> GetAllRoomImages(int roomId)
        {
            return _hotelRoomImagesOperations.GetAll(r => r.RoomId == roomId);
        }
        /// <summary>
        /// Gets the room image by image identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>HotelRoomImages.</returns>
        public HotelRoomImages GetRoomImageByImageId(int Id)
        {
            return _hotelRoomImagesOperations.GetById(Id);
        }
        /// <summary>
        /// Adds the room image.
        /// </summary>
        /// <param name="roomImage">The room image.</param>
        /// <returns>HotelRoomImages.</returns>
        public HotelRoomImages AddRoomImage(HotelRoomImages roomImage)
        {
            roomImage.CreationDate = DateTime.UtcNow;
            return _hotelRoomImagesOperations.AddNew(roomImage);
        }
        /// <summary>
        /// Updates the room image.
        /// </summary>
        /// <param name="roomImage">The room image.</param>
        /// <returns>HotelRoomImages.</returns>
        public HotelRoomImages UpdateRoomImage(HotelRoomImages roomImage)
        {
            _hotelRoomImagesOperations.Update(roomImage.Id, roomImage);
            return roomImage;
        }

        /// <summary>
        /// Deletes the room image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRoomImage(int id)
        {
            _hotelRoomImagesOperations.Delete(id);
        }
        #endregion Room Images

        #region Spa Employee
        /// <summary>
        /// Gets all spa employee.
        /// </summary>
        /// <returns>IEnumerable SpaEmployee.</returns>
        public IEnumerable<SpaEmployee> GetAllSpaEmployee()
        {
            return _spaEmployeeRepository.Table;
        }
        /// <summary>
        /// Gets all spa employee.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployee.</returns>
        public IEnumerable<SpaEmployee> GetAllSpaEmployee(int hotelId, int skip, int pageSize, out int total)
        {
            return _spaEmployeeOperation.GetAllWithServerSidePagging(se => se.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Creates the spa employee.
        /// </summary>
        /// <param name="spaEmployee">The spa employee.</param>
        /// <returns>SpaEmployee.</returns>
        public SpaEmployee CreateSpaEmployee(SpaEmployee spaEmployee)
        {
            spaEmployee.CreationDate = DateTime.UtcNow;
            return _spaEmployeeOperation.AddNew(spaEmployee);
        }
        /// <summary>
        /// Modifies the spa employee.
        /// </summary>
        /// <param name="spaEmployee">The spa employee.</param>
        /// <returns>SpaEmployee.</returns>
        public SpaEmployee ModifySpaEmployee(SpaEmployee spaEmployee)
        {
            _spaEmployeeOperation.Update(spaEmployee.Id, spaEmployee);
            return spaEmployee;
        }
        /// <summary>
        /// Gets the spa employee by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaEmployee.</returns>
        public SpaEmployee GetSpaEmployeeById(int id)
        {
            return _spaEmployeeOperation.GetById(id);
        }
        /// <summary>
        /// Deletes the spa employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaEmployee(int id)
        {
            _spaEmployeeOperation.Delete(id);
        }
        #endregion Spa Employee

        #region Spa Employee Details
        //Spa Employee Details
        /// <summary>
        /// Gets the spa employee details by spa detail identifier.
        /// </summary>
        /// <param name="spaDetailId">The spa detail identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="gender">The gender.</param>
        /// <returns>IEnumerable SpaEmployeeDetails.</returns>
        public IEnumerable<SpaEmployeeDetails> GetSpaEmployeeDetailsBySpaDetailId(int spaDetailId, int pageNumber, int pageSize, out int total, Gender gender = Gender.None)
        {
            switch (gender)
            {
                case Gender.Male:
                    return _spaEmployeeDetailsOperation.GetAll(se => se.SpaDetailId == spaDetailId && se.SpaEmployee.IsActive && se.SpaEmployee.Customer.Gender == gender.ToString(), se => se.Id, pageNumber, pageSize, out total);

                case Gender.Female:
                    return _spaEmployeeDetailsOperation.GetAll(se => se.SpaDetailId == spaDetailId && se.SpaEmployee.IsActive && se.SpaEmployee.Customer.Gender == gender.ToString(), se => se.Id, pageNumber, pageSize, out total);

                default:
                    return _spaEmployeeDetailsOperation.GetAll(se => se.SpaDetailId == spaDetailId && se.SpaEmployee.IsActive, se => se.Id, pageNumber, pageSize, out total);
            }

        }
        /// <summary>
        /// Gets the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployeeDetails.</returns>
        public IEnumerable<SpaEmployeeDetails> GetSpaEmployeeDetails(int spaEmployeeId, int skip, int pageSize, out int total)
        {
            return _spaEmployeeDetailsOperation.GetAllWithServerSidePagging(se => se.SpaEmployeeId == spaEmployeeId, se => se.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the spa employee details time slot by identifier.
        /// </summary>
        /// <param name="spaEmployeeDetailsId">The spa employee details identifier.</param>
        /// <param name="date">The date.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="extratimeId">The extratime identifier.</param>
        /// <returns>IEnumerable SpaEmployeeAvailableTimeSlot.</returns>
        public IEnumerable<SpaEmployeeAvailableTimeSlot> GetSpaEmployeeDetailsTimeSlotById(int spaEmployeeDetailsId, DateTime date, int[] Ids, int? extratimeId = null)
        {
            var result = string.Join(",", Ids);

            System.Data.SqlClient.SqlParameter employeeDetailsId = new System.Data.SqlClient.SqlParameter("spaEmployeeDetailsId", Convert.ToInt32(spaEmployeeDetailsId));
            System.Data.SqlClient.SqlParameter currentDate = new System.Data.SqlClient.SqlParameter("date", date);
            System.Data.SqlClient.SqlParameter spaExtratimeId = new System.Data.SqlClient.SqlParameter("extratimeId", extratimeId);
            System.Data.SqlClient.SqlParameter spaExtraProcedureIds = new System.Data.SqlClient.SqlParameter("extraProcedureIds", Convert.ToString(result));
            var custOrderHistory = _spaEmployeeAvailableTimeSlotRepository.ExecuteStoredProcedureList<SpaEmployeeAvailableTimeSlot>("exec SpaEmployeeAvailableTimeSlot", employeeDetailsId, currentDate, spaExtratimeId, spaExtraProcedureIds);
            return custOrderHistory;
        }

        /// <summary>
        /// Gets the spa employee details by identifier.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>SpaEmployeeDetails.</returns>
        public SpaEmployeeDetails GetSpaEmployeeDetailsById(int spaEmployeeId)
        {
            return _spaEmployeeDetailsOperation.GetById(spaEmployeeId);
        }
        /// <summary>
        /// Deletes the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeDetId">The spa employee det identifier.</param>
        public void DeleteSpaEmployeeDetails(int spaEmployeeDetId)
        {
            _spaEmployeeDetailsOperation.Delete(spaEmployeeDetId);
        }
        /// <summary>
        /// Gets the unassign spa service details.
        /// </summary>
        /// <param name="spaEmpId">The spa emp identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetUnassignSpaServiceDetails(int spaEmpId, int hotelId)
        {
            var assignSpaDetIds = _spaEmployeeDetailsOperation.GetAll(s => s.SpaEmployeeId == spaEmpId).Select(sd => sd.SpaDetailId).Distinct();
            var spaServiceDetails = _spaServiceDetailsRepository.Table.Where(r => !assignSpaDetIds.Contains(r.Id) && r.SpaService.HotelServices.HotelId == hotelId);
            return spaServiceDetails;
        }

        /// <summary>
        /// Creates the spa employee details.
        /// </summary>
        /// <param name="spaEmployeeDetails">The spa employee details.</param>
        public void CreateSpaEmployeeDetails(List<SpaEmployeeDetails> spaEmployeeDetails)
        {
            foreach (var eachSpaEmpDet in spaEmployeeDetails)
            {
                _spaEmployeeDetailsOperation.AddNew(eachSpaEmpDet);
            }
        }



        /// <summary>
        /// Gets the spa employee time slots.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaEmployeeTimeSlot.</returns>
        public IEnumerable<SpaEmployeeTimeSlot> GetSpaEmployeeTimeSlots(int spaEmployeeId, int skip, int pageSize, out int total)
        {
            return _spaEmployeeTimeSlotOperation.GetAllWithServerSidePagging(e => e.SpaEmployeeId == spaEmployeeId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the spa empl time slot calendar.
        /// </summary>
        /// <param name="spaEmployeeId">The spa employee identifier.</param>
        /// <returns>IEnumerable SpaEmployeeTimeSlot.</returns>
        public IEnumerable<SpaEmployeeTimeSlot> GetSpaEmplTimeSlotCalendar(int spaEmployeeId)
        {
            return _spaEmployeeTimeSlotOperation.GetAll(e => e.SpaEmployeeId == spaEmployeeId);
        }
        /// <summary>
        /// Creates the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeTimeSlot">The spa employee time slot.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        /// <exception cref="ArgumentException">Employee timeslot already created for " + spaEmployeeTimeSlot.StartDateTime.ToShortDateString()</exception>
        public SpaEmployeeTimeSlot CreateSpaEmployeeTimeSlot(SpaEmployeeTimeSlot spaEmployeeTimeSlot)
        {
            spaEmployeeTimeSlot.CreationDate = DateTime.Now;

            var availableTimeSlot = _spaEmployeeTimeSlotOperation.GetAll(t => t.SpaEmployeeId == spaEmployeeTimeSlot.SpaEmployeeId &&
                                                    (spaEmployeeTimeSlot.StartDateTime.Date == t.StartDateTime.Date));

            if (availableTimeSlot != null && availableTimeSlot.Count() > 0)
                throw new ArgumentException("Employee timeslot already created for " + spaEmployeeTimeSlot.StartDateTime.ToShortDateString());

            return _spaEmployeeTimeSlotOperation.AddNew(spaEmployeeTimeSlot);
        }
        /// <summary>
        /// Modifies the spa employee time slot.
        /// </summary>
        /// <param name="spaEmployeeTimeSlot">The spa employee time slot.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        /// <exception cref="ArgumentException">Employee timeslot already created for " + spaEmployeeTimeSlot.StartDateTime.ToShortDateString()</exception>
        public SpaEmployeeTimeSlot ModifySpaEmployeeTimeSlot(SpaEmployeeTimeSlot spaEmployeeTimeSlot)
        {
            var availableTimeSlot = _spaEmployeeTimeSlotOperation.GetAll(t => t.SpaEmployeeId == spaEmployeeTimeSlot.SpaEmployeeId && t.Id != spaEmployeeTimeSlot.Id &&
                                                    (spaEmployeeTimeSlot.StartDateTime.Date == t.StartDateTime.Date));
            if (availableTimeSlot != null && availableTimeSlot.Count() > 0)
                throw new ArgumentException("Employee timeslot already created for " + spaEmployeeTimeSlot.StartDateTime.ToShortDateString());

            _spaEmployeeTimeSlotOperation.Update(spaEmployeeTimeSlot.Id, spaEmployeeTimeSlot);
            return spaEmployeeTimeSlot;
        }

        /// <summary>
        /// Gets the spa employee time slot by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>SpaEmployeeTimeSlot.</returns>
        public SpaEmployeeTimeSlot GetSpaEmployeeTimeSlotById(int id)
        {
            return _spaEmployeeTimeSlotOperation.GetById(id);
        }

        /// <summary>
        /// Deletes the spa employee time slot.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteSpaEmployeeTimeSlot(int id)
        {
            _spaEmployeeTimeSlotOperation.Delete(id);
        }

        /// <summary>
        /// Gets the customer for spa.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetCustomerForSpa(int hotelId)
        {
            var spaEmployee = (from spaEmp in _spaEmployeeRepository.Table
                               select spaEmp.CustomerId).ToArray();

            var customer = _customerRepository.Table
                       .Where(c => c.CustomerRoles.FirstOrDefault().RoleId == 10 && c.CustomerRoles.FirstOrDefault().HotelId == hotelId && !spaEmployee.Contains(c.Id));

            return customer;
        }

        /// <summary>
        /// Gets all spa addtioanl group.
        /// </summary>
        /// <returns>IEnumerable SpaAdditionalGroup.</returns>
        public IEnumerable<SpaAdditionalGroup> GetAllSpaAddtioanlGroup()
        {
            return _spaAdditionalGroupRepository.Table;
        }

        #endregion Spa Employee Details

        #region Spa Room
        //Spa Room
        /// <summary>
        /// Gets all spa rooms.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaRoom.</returns>
        public IEnumerable<SpaRoom> GetAllSpaRooms(int hotelId, int skip, int pageSize, out int total)
        {
            return _spaRoomOperations.GetAllWithServerSidePagging(sr => sr.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the spa room by identifier.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <returns>SpaRoom.</returns>
        public SpaRoom GetSpaRoomById(int spaRoomId)
        {
            return _spaRoomOperations.GetById(spaRoomId);
        }
        /// <summary>
        /// Gets the spa room.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns>SpaRoom.</returns>
        public SpaRoom GetSpaRoom(int hotelId, int roomNumber)
        {
            return _spaRoomOperations.GetById(r => r.HotelId == hotelId && r.Number == roomNumber);
        }
        /// <summary>
        /// Creates the spa room.
        /// </summary>
        /// <param name="spaRoom">The spa room.</param>
        /// <returns>SpaRoom.</returns>
        public SpaRoom CreateSpaRoom(SpaRoom spaRoom)
        {
            spaRoom.CreationDate = DateTime.UtcNow;
            return _spaRoomOperations.AddNew(spaRoom);
        }
        /// <summary>
        /// Modifies the spa room.
        /// </summary>
        /// <param name="spaRoom">The spa room.</param>
        /// <returns>SpaRoom.</returns>
        public SpaRoom ModifySpaRoom(SpaRoom spaRoom)
        {
            _spaRoomOperations.Update(spaRoom.Id, spaRoom);
            return spaRoom;
        }
        /// <summary>
        /// Deletes the spa room.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        public void DeleteSpaRoom(int spaRoomId)
        {
            _spaRoomOperations.Delete(spaRoomId);
        }
        #endregion Spa Room

        #region House Keeping Facilities
        //House Keeping Facilities

        /// <summary>
        /// Gets all housekeeping facilities.
        /// </summary>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        public IEnumerable<HouseKeepingFacility> GetAllHousekeepingFacilities()
        {
            return _houseKeepingFacilitiesRepository.Table.Where(h => h.IsActive);
        }

        /// <summary>
        /// Gets the housekeeping by hotel service identifier.
        /// </summary>
        /// <param name="hotelServiceId">The hotel service identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        public IEnumerable<HouseKeepingFacility> GetHousekeepingByHotelServiceId(int hotelServiceId, int skip, int pageSize, out int total)
        {
            return _houseKeepingFacilitiesOperations.GetAllWithServerSidePagging(hk => hk.HotelServiceId == hotelServiceId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the housekeeping facilities.
        /// </summary>
        /// <param name="housekeepingFacilities">The housekeeping facilities.</param>
        /// <returns>HouseKeepingFacility.</returns>
        public HouseKeepingFacility CreateHousekeepingFacilities(HouseKeepingFacility housekeepingFacilities)
        {
            housekeepingFacilities.CreationDate = DateTime.Now;
            return _houseKeepingFacilitiesOperations.AddNew(housekeepingFacilities);
        }
        /// <summary>
        /// Modifies the housekeeping facilities.
        /// </summary>
        /// <param name="housekeepingFacilities">The housekeeping facilities.</param>
        /// <returns>HouseKeepingFacility.</returns>
        public HouseKeepingFacility ModifyHousekeepingFacilities(HouseKeepingFacility housekeepingFacilities)
        {
            _houseKeepingFacilitiesOperations.Update(housekeepingFacilities.Id, housekeepingFacilities);
            return housekeepingFacilities;
        }
        /// <summary>
        /// Gets the housekeeping facilities by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingFacility.</returns>
        public HouseKeepingFacility GetHousekeepingFacilitiesById(int id)
        {
            return _houseKeepingFacilitiesOperations.GetById(hk => hk.Id == id);
        }
        /// <summary>
        /// Deletes the housekeeping facilitie.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHousekeepingFacilitie(int id)
        {
            _houseKeepingFacilitiesOperations.Delete(id);
        }

        #endregion House Keeping Facilities

        #region House Keeping Facility Details
        //House Keeping Facility Details
        /// <summary>
        /// Gets the housekeeping fac details by hf identifier.
        /// </summary>
        /// <param name="housekeepingFacId">The housekeeping fac identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingFacilityDetail.</returns>
        public IEnumerable<HouseKeepingFacilityDetail> GetHousekeepingFacDetailsByHFId(int housekeepingFacId, int skip, int pageSize, out int total)
        {
            return _houseKeepingFacilityDetailsOperations.GetAllWithServerSidePagging(h => h.HouseKeepingFacilityId == housekeepingFacId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the housekeeping fac detail.
        /// </summary>
        /// <param name="hKeepingFacilityDetail">The h keeping facility detail.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        public HouseKeepingFacilityDetail CreateHousekeepingFacDetail(HouseKeepingFacilityDetail hKeepingFacilityDetail)
        {
            hKeepingFacilityDetail.CreationDate = DateTime.Now;
            return _houseKeepingFacilityDetailsOperations.AddNew(hKeepingFacilityDetail);
        }
        /// <summary>
        /// Modifies the housekeeping fac detail.
        /// </summary>
        /// <param name="hKeepingFacilityDetail">The h keeping facility detail.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        public HouseKeepingFacilityDetail ModifyHousekeepingFacDetail(HouseKeepingFacilityDetail hKeepingFacilityDetail)
        {
            _houseKeepingFacilityDetailsOperations.Update(hKeepingFacilityDetail.Id, hKeepingFacilityDetail);
            return hKeepingFacilityDetail;
        }
        /// <summary>
        /// Gets the housekeeping fac detail by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingFacilityDetail.</returns>
        public HouseKeepingFacilityDetail GetHousekeepingFacDetailById(int id)
        {
            return _houseKeepingFacilityDetailsOperations.GetById(hkf => hkf.Id == id);
        }
        /// <summary>
        /// Deletes the housekeeping fac detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHousekeepingFacDetail(int id)
        {
            _houseKeepingFacilityDetailsOperations.Delete(id);
        }
        #endregion House Keeping Facility Details

        #region Housekeeping Additional groups
        //Housekeeping additionals groups
        /// <summary>
        /// Gets all un assign housekeeping additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="houseKeepingFacilityId">The house keeping facility identifier.</param>
        /// <returns>IEnumerable HousekeepingAdditionalGroup.</returns>
        public IEnumerable<HousekeepingAdditionalGroup> GetAllUnAssignHousekeepingAdditionalGroups(int hotelId, int houseKeepingFacilityId)
        {
            var assignedAddGrp = _houseKeepingfacilitiesAdditionalOperations.GetAll(a => a.HouseKeepingFacilityId == houseKeepingFacilityId).Select(m => m.HouseKeepingAdditionalGroupId).ToList();
            return _housekeepingAdditionalGroupsOperations.GetAll(g => g.HotelId == hotelId && !assignedAddGrp.Contains(g.Id));
        }
        /// <summary>
        /// Gets the housekeeping additional groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingAdditionalGroup.</returns>
        public IEnumerable<HousekeepingAdditionalGroup> GetHousekeepingAdditionalGroups(int hotelId, int skip, int pageSize, out int total)
        {
            return _housekeepingAdditionalGroupsOperations.GetAllWithServerSidePagging(h => h.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the housekeeping additional group by identifier.
        /// </summary>
        /// <param name="housekeepingAddGroupId">The housekeeping add group identifier.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        public HousekeepingAdditionalGroup GetHousekeepingAdditionalGroupById(int housekeepingAddGroupId)
        {
            return _housekeepingAdditionalGroupsOperations.GetById(housekeepingAddGroupId);
        }
        /// <summary>
        /// Creates the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGroup">The housekeeping add group.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        public HousekeepingAdditionalGroup CreateHousekeepingAdditionalGroup(HousekeepingAdditionalGroup housekeepingAddGroup)
        {
            housekeepingAddGroup.CreationDate = DateTime.Now;
            return _housekeepingAdditionalGroupsOperations.AddNew(housekeepingAddGroup);
        }
        /// <summary>
        /// Modifies the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGroup">The housekeeping add group.</param>
        /// <returns>HousekeepingAdditionalGroup.</returns>
        public HousekeepingAdditionalGroup ModifyHousekeepingAdditionalGroup(HousekeepingAdditionalGroup housekeepingAddGroup)
        {
            _housekeepingAdditionalGroupsOperations.Update(housekeepingAddGroup.Id, housekeepingAddGroup);
            return housekeepingAddGroup;
        }
        /// <summary>
        /// Deletes the housekeeping additional group.
        /// </summary>
        /// <param name="housekeepingAddGrpId">The housekeeping add GRP identifier.</param>
        public void DeleteHousekeepingAdditionalGroup(int housekeepingAddGrpId)
        {
            _housekeepingAdditionalGroupsOperations.Delete(housekeepingAddGrpId);
        }
        #endregion Housekeeping Additional groups

        #region Housekeeping Additional elements
        //Housekeeping Additional elements
        /// <summary>
        /// Gets the housekeeping additional elements.
        /// </summary>
        /// <param name="housekeepingAddGrpId">The housekeeping add GRP identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HousekeepingAdditionalElement.</returns>
        public IEnumerable<HousekeepingAdditionalElement> GetHousekeepingAdditionalElements(int housekeepingAddGrpId, int skip, int pageSize, out int total)
        {
            return _housekeepingAdditionalElementsOperations.GetAllWithServerSidePagging(s => s.HousekeepingAdditionalGroupId == housekeepingAddGrpId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the housekeeping additional element by identifier.
        /// </summary>
        /// <param name="housekeepingAddElementId">The housekeeping add element identifier.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        public HousekeepingAdditionalElement GetHousekeepingAdditionalElementById(int housekeepingAddElementId)
        {
            return _housekeepingAdditionalElementsOperations.GetById(housekeepingAddElementId);
        }
        /// <summary>
        /// Creates the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElement">The housekeeping add element.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        public HousekeepingAdditionalElement CreateHousekeepingAdditionalElement(HousekeepingAdditionalElement housekeepingAddElement)
        {
            housekeepingAddElement.CreationDate = DateTime.Now;
            return _housekeepingAdditionalElementsOperations.AddNew(housekeepingAddElement);
        }
        /// <summary>
        /// Modifies the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElement">The housekeeping add element.</param>
        /// <returns>HousekeepingAdditionalElement.</returns>
        public HousekeepingAdditionalElement ModifyHousekeepingAdditionalElement(HousekeepingAdditionalElement housekeepingAddElement)
        {
            _housekeepingAdditionalElementsOperations.Update(housekeepingAddElement.Id, housekeepingAddElement);
            return housekeepingAddElement;
        }
        /// <summary>
        /// Deletes the housekeeping additional element.
        /// </summary>
        /// <param name="housekeepingAddElementId">The housekeeping add element identifier.</param>
        public void DeleteHousekeepingAdditionalElement(int housekeepingAddElementId)
        {
            _housekeepingAdditionalElementsOperations.Delete(housekeepingAddElementId);
        }
        #endregion Housekeeping Additional elements

        #region Housekeeping facilities Additionals
        //Housekeeping facilities Additionals

        /// <summary>
        /// Gets all housekeeping fac additionals.
        /// </summary>
        /// <returns>IEnumerable HouseKeepingfacilitiesAdditional.</returns>
        public IEnumerable<HouseKeepingfacilitiesAdditional> GetAllHousekeepingFacAdditionals()
        {
            return _houseKeepingfacilitiesAdditionalsRepository.Table.Where(h => h.IsActive);
        }

        /// <summary>
        /// Gets the housekeeping fac additional by HKF identifier.
        /// </summary>
        /// <param name="housekeepingFacilityId">The housekeeping facility identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeepingfacilitiesAdditional.</returns>
        public IEnumerable<HouseKeepingfacilitiesAdditional> GetHousekeepingFacAdditionalByHKFId(int housekeepingFacilityId, int skip, int pageSize, out int total)
        {
            return _houseKeepingfacilitiesAdditionalOperations.GetAllWithServerSidePagging(hk => hk.HouseKeepingFacilityId == housekeepingFacilityId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the housekeeping fac additionals.
        /// </summary>
        /// <param name="housekeepingFacAdditional">The housekeeping fac additional.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        public HouseKeepingfacilitiesAdditional CreateHousekeepingFacAdditionals(HouseKeepingfacilitiesAdditional housekeepingFacAdditional)
        {
            housekeepingFacAdditional.CreationDate = DateTime.Now;
            return _houseKeepingfacilitiesAdditionalOperations.AddNew(housekeepingFacAdditional);
        }
        /// <summary>
        /// Modifies the housekeeping fac additional.
        /// </summary>
        /// <param name="housekeepingFacAdditional">The housekeeping fac additional.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        public HouseKeepingfacilitiesAdditional ModifyHousekeepingFacAdditional(HouseKeepingfacilitiesAdditional housekeepingFacAdditional)
        {
            _houseKeepingfacilitiesAdditionalOperations.Update(housekeepingFacAdditional.Id, housekeepingFacAdditional);
            return housekeepingFacAdditional;
        }
        /// <summary>
        /// Gets the housekeeping fac additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeepingfacilitiesAdditional.</returns>
        public HouseKeepingfacilitiesAdditional GetHousekeepingFacAdditionalById(int id)
        {
            return _houseKeepingfacilitiesAdditionalOperations.GetById(hk => hk.Id == id);
        }
        /// <summary>
        /// Deletes the house keeping fac additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteHouseKeepingFacAdditional(int id)
        {
            _houseKeepingfacilitiesAdditionalOperations.Delete(id);
        }
        #endregion Housekeeping facilities Additionals

        #region Housekeeping
        //House Keeping
        /// <summary>
        /// Gets all room house keeping.
        /// </summary>
        /// <param name="roomDetailsId">The room details identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable HouseKeeping.</returns>
        public IEnumerable<HouseKeeping> GetAllRoomHouseKeeping(int roomDetailsId, int skip, int pageSize, out int total)
        {
            return _houseKeepingOperations.GetAllWithServerSidePagging(r => r.RoomDetailsId == roomDetailsId, s => s.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the housekeeping by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>HouseKeeping.</returns>
        public HouseKeeping GetHousekeepingById(int id)
        {
            return _houseKeepingOperations.GetById(id);
        }
        /// <summary>
        /// Modifies the room house keeping facility.
        /// </summary>
        /// <param name="roomHousekeeping">The room housekeeping.</param>
        /// <returns>HouseKeeping.</returns>
        public HouseKeeping ModifyRoomHouseKeepingFacility(HouseKeeping roomHousekeeping)
        {
            _houseKeepingOperations.Update(roomHousekeeping.Id, roomHousekeeping);
            return roomHousekeeping;
        }

        /// <summary>
        /// Deletes the room house keeping.
        /// </summary>
        /// <param name="houseKeepingId">The house keeping identifier.</param>
        public void DeleteRoomHouseKeeping(int houseKeepingId)
        {
            _houseKeepingOperations.Delete(houseKeepingId);
        }

        /// <summary>
        /// Gets the unassign hk facilities.
        /// </summary>
        /// <param name="roomDetailsId">The room details identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable HouseKeepingFacility.</returns>
        public IEnumerable<HouseKeepingFacility> GetUnassignHKFacilities(int roomDetailsId, int hotelId)
        {
            var assignHKFIds = _houseKeepingOperations.GetAll(s => s.RoomDetailsId == roomDetailsId).Select(sd => sd.HouseKeepingFacilityId).Distinct();
            var houseKeepingFacilities = _houseKeepingFacilitiesRepository.Table.Where(r => r.HotelService.HotelId == hotelId && !assignHKFIds.Contains(r.Id) && r.IsActive);
            return houseKeepingFacilities;
        }

        /// <summary>
        /// Creates the room house keeping.
        /// </summary>
        /// <param name="roomHouseKeeping">The room house keeping.</param>
        public void CreateRoomHouseKeeping(List<HouseKeeping> roomHouseKeeping)
        {
            foreach (var eachHouseKeeping in roomHouseKeeping)
            {
                _houseKeepingOperations.AddNew(eachHouseKeeping);
            }
        }
        #endregion Housekeeping

        #region Room Details
        //RoomDetails

        /// <summary>
        /// Gets the room details.
        /// </summary>
        /// <param name="roomId">The room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RoomDetails.</returns>
        public IEnumerable<RoomDetails> GetRoomDetails(int roomId, int skip, int pageSize, out int total)
        {
            return _roomDetailsOperations.GetAllWithServerSidePagging(r => r.RoomId == roomId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the room detail.
        /// </summary>
        /// <param name="roomDetails">The room details.</param>
        /// <returns>RoomDetails.</returns>
        public RoomDetails CreateRoomDetail(RoomDetails roomDetails)
        {
            roomDetails.CreationDate = DateTime.Now;
            return _roomDetailsOperations.AddNew(roomDetails);
        }
        /// <summary>
        /// Modifies the room detail.
        /// </summary>
        /// <param name="roomDetails">The room details.</param>
        /// <returns>RoomDetails.</returns>
        public RoomDetails ModifyRoomDetail(RoomDetails roomDetails)
        {
            _roomDetailsOperations.Update(roomDetails.Id, roomDetails);
            return roomDetails;
        }
        /// <summary>
        /// Gets the room detail by identifier.
        /// </summary>
        /// <param name="roomDetailId">The room detail identifier.</param>
        /// <returns>RoomDetails.</returns>
        public RoomDetails GetRoomDetailById(int roomDetailId)
        {
            return _roomDetailsOperations.GetById(roomDetailId);
        }
        /// <summary>
        /// Deletes the room detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRoomDetail(int id)
        {
            _roomDetailsOperations.Delete(id);
        }
        #endregion Room Details

        #region Spa Room Details
        //Spa Room Details
        /// <summary>
        /// Gets all spa room details.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable SpaRoomDetails.</returns>
        public IEnumerable<SpaRoomDetails> GetAllSpaRoomDetails(int spaRoomId, int skip, int pageSize, out int total)
        {
            return _spaRoomDetailsOperations.GetAllWithServerSidePagging(s => s.SpaRoomId == spaRoomId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Deletes the spa room detail.
        /// </summary>
        /// <param name="spaRoomdetId">The spa roomdet identifier.</param>
        public void DeleteSpaRoomDetail(int spaRoomdetId)
        {
            _spaRoomDetailsOperations.Delete(spaRoomdetId);
        }

        /// <summary>
        /// Gets the unassign spa details.
        /// </summary>
        /// <param name="spaRoomId">The spa room identifier.</param>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable SpaServiceDetail.</returns>
        public IEnumerable<SpaServiceDetail> GetUnassignSpaDetails(int spaRoomId, int hotelId)
        {
            var assignSpaDetIds = _spaRoomDetailsOperations.GetAll(s => s.SpaRoomId == spaRoomId).Select(sd => sd.SpaServiceDetailId).Distinct();
            var spaServiceDetails = _spaServiceDetailsRepository.Table.Where(r => !assignSpaDetIds.Contains(r.Id) && r.SpaService.HotelServices.HotelId == hotelId);
            return spaServiceDetails;
        }

        /// <summary>
        /// Creates the spa room details.
        /// </summary>
        /// <param name="spaRoomDetails">The spa room details.</param>
        public void CreateSpaRoomDetails(List<SpaRoomDetails> spaRoomDetails)
        {
            foreach (var eachSpaRoomDet in spaRoomDetails)
            {
                _spaRoomDetailsOperations.AddNew(eachSpaRoomDet);
            }
        }
        #endregion Spa Room Details

        #region Amenities
        //Amenities
        /// <summary>
        /// Gets all amenities.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Amenities.</returns>
        public IEnumerable<Amenities> GetAllAmenities(int skip, int pageSize, out int total)
        {
            return _amenitiesOperation.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the amenities.
        /// </summary>
        /// <param name="amenities">The amenities.</param>
        /// <returns>Amenities.</returns>
        public Amenities CreateAmenities(Amenities amenities)
        {
            amenities.CreationDate = DateTime.UtcNow;
            return _amenitiesOperation.AddNew(amenities);
        }
        /// <summary>
        /// Modifies the amenities.
        /// </summary>
        /// <param name="amenities">The amenities.</param>
        /// <returns>Amenities.</returns>
        public Amenities ModifyAmenities(Amenities amenities)
        {
            _amenitiesOperation.Update(amenities.Id, amenities);
            return amenities;
        }

        /// <summary>
        /// Gets the amenity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Amenities.</returns>
        public Amenities GetAmenityById(int id)
        {
            return _amenitiesOperation.GetById(id);
        }
        /// <summary>
        /// Deletes the amenities.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteAmenities(int id)
        {
            _amenitiesOperation.Delete(id);
        }
        #endregion Amenities

        #region Garments
        //Garments
        /// <summary>
        /// Gets all garments.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Garments.</returns>
        public IEnumerable<Garments> GetAllGarments(int skip, int pageSize, out int total)
        {
            return _garmentsOperation.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets all garments.
        /// </summary>
        /// <param name="laundryId">The laundry identifier.</param>
        /// <returns>IEnumerable Garments.</returns>
        public IEnumerable<Garments> GetAllGarments(int laundryId)
        {

            var existGarment = _hotelLaundryDetailsOperations.GetAll(l => l.LaundryId == laundryId).Select(r => r.GarmentId).ToArray();

            return _garmentsRepository.Table.Where(g => !existGarment.Contains(g.Id));
        }
        /// <summary>
        /// Creates the garments.
        /// </summary>
        /// <param name="garments">The garments.</param>
        /// <returns>Garments.</returns>
        public Garments CreateGarments(Garments garments)
        {
            garments.CreationDate = DateTime.UtcNow;
            return _garmentsOperation.AddNew(garments);
        }
        /// <summary>
        /// Modifies the garments.
        /// </summary>
        /// <param name="garments">The garments.</param>
        /// <returns>Garments.</returns>
        public Garments ModifyGarments(Garments garments)
        {
            _garmentsOperation.Update(garments.Id, garments);
            return garments;
        }
        /// <summary>
        /// Gets the garments by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Garments.</returns>
        public Garments GetGarmentsById(int id)
        {
            return _garmentsOperation.GetById(id);
        }
        /// <summary>
        /// Deletes the garments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteGarments(int id)
        {
            _garmentsOperation.Delete(id);
        }
        #endregion Garments

        #region Concierge Group
        //ConciergeGroup
        /// <summary>
        /// Gets all concierge group.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable ConciergeGroup.</returns>
        public IEnumerable<ConciergeGroup> GetAllConciergeGroup(int hotelId, int skip, int pageSize, out int total)
        {
            return _conciergeGroupOperation.GetAllWithServerSidePagging(c => c.HotelId == hotelId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets all concierge groups.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable ConciergeGroup.</returns>
        public IEnumerable<ConciergeGroup> GetAllConciergeGroups(int hotelId)
        {
            return _conciergeGroupOperation.GetAll(c => c.HotelId == hotelId && c.IsActive);
        }

        /// <summary>
        /// Creates the concierge group.
        /// </summary>
        /// <param name="conciergeGroup">The concierge group.</param>
        /// <returns>ConciergeGroup.</returns>
        public ConciergeGroup CreateConciergeGroup(ConciergeGroup conciergeGroup)
        {
            conciergeGroup.CreationDate = DateTime.UtcNow;
            return _conciergeGroupOperation.AddNew(conciergeGroup);
        }

        /// <summary>
        /// Modifies the concierge group.
        /// </summary>
        /// <param name="conciergeGroup">The concierge group.</param>
        /// <returns>ConciergeGroup.</returns>
        public ConciergeGroup ModifyConciergeGroup(ConciergeGroup conciergeGroup)
        {
            _conciergeGroupOperation.Update(conciergeGroup.Id, conciergeGroup);
            return conciergeGroup;
        }

        /// <summary>
        /// Gets the concierge group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ConciergeGroup.</returns>
        public ConciergeGroup GetConciergeGroupById(int id)
        {
            return _conciergeGroupOperation.GetById(id);
        }

        /// <summary>
        /// Deletes the concierge group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteConciergeGroup(int id)
        {
            _conciergeGroupOperation.Delete(id);
        }
        #endregion Concierge Group

        #region Concierge
        //Concierge
        /// <summary>
        /// Gets all concierge by group identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Concierge.</returns>
        public IEnumerable<Concierge> GetAllConciergeByGroupId(int id, int skip, int pageSize, out int total)
        {
            return _conciergeOperation.GetAllWithServerSidePagging(c => c.ConciergeGroupId == id, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the concierge.
        /// </summary>
        /// <param name="concierge">The concierge.</param>
        /// <returns>Concierge.</returns>
        public Concierge CreateConcierge(Concierge concierge)
        {
            concierge.CreationDate = DateTime.UtcNow;
            return _conciergeOperation.AddNew(concierge);
        }
        /// <summary>
        /// Modifies the concierge.
        /// </summary>
        /// <param name="concierge">The concierge.</param>
        /// <returns>Concierge.</returns>
        public Concierge ModifyConcierge(Concierge concierge)
        {
            _conciergeOperation.Update(concierge.Id, concierge);
            return concierge;
        }
        /// <summary>
        /// Gets the concierge by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Concierge.</returns>
        public Concierge GetConciergeById(int id)
        {
            return _conciergeOperation.GetById(id);
        }
        /// <summary>
        /// Deletes the concierge.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteConcierge(int id)
        {
            _conciergeOperation.Delete(id);
        }
        #endregion Concierge

        #region Hotels Services Offers

        /// <summary>
        /// Gets all hotel services offers.
        /// </summary>
        /// <returns>IEnumerable AllHotelServicesOffers.</returns>
        public IEnumerable<AllHotelServicesOffers> GetAllHotelServicesOffers()
        {
            var allOffers = _allHotelServicesOffersRepository.ExecuteStoredProcedureList<AllHotelServicesOffers>("exec uspGetAllHotelServicesOffers");
            return allOffers;
        }

        #endregion Hotels Services Offers

    }
}