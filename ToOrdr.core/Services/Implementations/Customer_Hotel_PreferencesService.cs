﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="Customer_Hotel_PreferencesService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class Customer_Hotel_PreferencesService.
    /// Implements the <see cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Customer_Hotel_Preferences},ToOrdr.Core.Entities.Customer_Hotel_Preferences}" />
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.ICustomer_Hotel_PreferencesService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Customer_Hotel_Preferences},ToOrdr.Core.Entities.Customer_Hotel_Preferences}" />
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.ICustomer_Hotel_PreferencesService" />
    public class Customer_Hotel_PreferencesService : ImplementationBase<IRepository<Customer_Hotel_Preferences>, Customer_Hotel_Preferences>, ICustomer_Hotel_PreferencesService
    {
        /// <summary>
        /// The customer hotel preferences repository
        /// </summary>
        IRepository<Customer_Hotel_Preferences> _customer_Hotel_PreferencesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer_Hotel_PreferencesService"/> class.
        /// </summary>
        /// <param name="customer_Hotel_PreferencesRepository">The customer hotel preferences repository.</param>
        public Customer_Hotel_PreferencesService(IRepository<Customer_Hotel_Preferences> customer_Hotel_PreferencesRepository)
            : base(customer_Hotel_PreferencesRepository)
        {
            _customer_Hotel_PreferencesRepository = customer_Hotel_PreferencesRepository;
        }

    }
}
