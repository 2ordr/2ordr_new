﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 05-03-2019
// ***********************************************************************
// <copyright file="RestaurantService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;
using System.Data.Entity;
using ToOrdr.Core.Exceptions;
using System.Configuration;


namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class RestaurantService.
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IRestaurantService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IRestaurantService" />
    public class RestaurantService : IRestaurantService
    {

        #region initialization
        /// <summary>
        /// The customer repository
        /// </summary>
        IRepository<Customer> _customerRepository;
        /// <summary>
        /// The restaurant details repository
        /// </summary>
        IRepository<Restaurant> _restaurantDetailsRepository;
        /// <summary>
        /// The restaurant menus repository
        /// </summary>
        IRepository<RestaurantMenu> _restaurantMenusRepository;
        /// <summary>
        /// The restaurant menu items repository
        /// </summary>
        IRepository<RestaurantMenuItem> _restaurantMenuItemsRepository;
        /// <summary>
        /// The menu additional element repository
        /// </summary>
        IRepository<MenuAdditionalElement> _menuAdditionalElementRepository;
        /// <summary>
        /// The restaurant cart repository
        /// </summary>
        IRepository<RestaurantCart> _restaurantCartRepository;
        /// <summary>
        /// The restaurant cart item repository
        /// </summary>
        IRepository<RestaurantCartItem> _restaurantCartItemRepository;
        /// <summary>
        /// The restaurant cart item additional
        /// </summary>
        IRepository<RestaurantCartItemAdditional> _restaurantCartItemAdditional;
        /// <summary>
        /// The restaurant order repository
        /// </summary>
        IRepository<RestaurantOrder> _restaurantOrderRepository;
        /// <summary>
        /// The restaurant order item repository
        /// </summary>
        IRepository<RestaurantOrderItem> _restaurantOrderItemRepository;
        /// <summary>
        /// The restaurant review repository
        /// </summary>
        IRepository<RestaurantReview> _restaurantReviewRepository;
        /// <summary>
        /// The restaurant customer reviews repository
        /// </summary>
        IRepository<RestaurantOrderReview> _restaurantCustomerReviewsRepository;
        /// <summary>
        /// The restaurant table groups repository
        /// </summary>
        IRepository<RestaurantTableGroup> _restaurantTableGroupsRepository;
        /// <summary>
        /// The restaurant tables repository
        /// </summary>
        IRepository<RestaurantTable> _restaurantTablesRepository;
        /// <summary>
        /// The customer restaurant orders repository
        /// </summary>
        IRepository<RestaurantOrder> _customerRestaurantOrdersRepository;
        /// <summary>
        /// The customer restaurant order items repository
        /// </summary>
        IRepository<Customer_RestaurantOrder_Items> _customerRestaurantOrderItemsRepository;
        /// <summary>
        /// The cuisines repository
        /// </summary>
        IRepository<Cuisine> _cuisinesRepository;
        /// <summary>
        /// The restaurant cuisines repository
        /// </summary>
        IRepository<RestaurantCuisine> _restaurantCuisinesRepository;
        /// <summary>
        /// The menu item review repository
        /// </summary>
        IRepository<MenuItemReview> _menuItemReviewRepository;
        /// <summary>
        /// The customer credit card repository
        /// </summary>
        IRepository<CustomerCreditCard> _customerCreditCardRepository;
        /// <summary>
        /// The restaurant menu group repository
        /// </summary>
        IRepository<RestaurantMenuGroup> _restaurantMenuGroupRepository;
        /// <summary>
        /// The customer role repository
        /// </summary>
        IRepository<CustomerRole> _customerRoleRepository;
        /// <summary>
        /// The role repository
        /// </summary>
        IRepository<Role> _roleRepository;
        /// <summary>
        /// The menu item ingredient repository
        /// </summary>
        IRepository<MenuItemIngredient> _menuItemIngredientRepository;
        /// <summary>
        /// The menu item size repository
        /// </summary>
        IRepository<MenuItemSize> _menuItemSizeRepository;
        /// <summary>
        /// The menu item additional repository
        /// </summary>
        IRepository<MenuItemAdditional> _menuItemAdditionalRepository;
        /// <summary>
        /// The ingredient categories repository
        /// </summary>
        IRepository<IngredientCategory> _ingredientCategoriesRepository;
        /// <summary>
        /// The ingredient repository
        /// </summary>
        IRepository<Ingredient> _ingredientRepository;
        /// <summary>
        /// The ingredient nutrition values repository
        /// </summary>
        IRepository<IngredientNutritionValues> _ingredientNutritionValuesRepository;
        /// <summary>
        /// The menu additional group repository
        /// </summary>
        IRepository<MenuAdditionalGroup> _menuAdditionalGroupRepository;
        /// <summary>
        /// The restaurant table menu repository
        /// </summary>
        IRepository<RestaurantTableMenu> _restaurantTableMenuRepository;
        /// <summary>
        /// The restaurant menu item image repository
        /// </summary>
        IRepository<RestaurantMenuItemImage> _restaurantMenuItemImageRepository;
        /// <summary>
        /// The allergen categories repository
        /// </summary>
        IRepository<AllergenCategory> _allergenCategoriesRepository;
        /// <summary>
        /// The allergen repository
        /// </summary>
        IRepository<Allergen> _allergenRepository;
        /// <summary>
        /// The language repository
        /// </summary>
        IRepository<Language> _languageRepository;
        /// <summary>
        /// The restaurant menu translation repository
        /// </summary>
        IRepository<RestaurantMenuTranslation> _restaurantMenuTranslationRepository;
        /// <summary>
        /// The restaurant menu group translation repository
        /// </summary>
        IRepository<RestaurantMenuGroupTranslation> _restaurantMenuGroupTranslationRepository;
        /// <summary>
        /// The restaurant menu item translation repository
        /// </summary>
        IRepository<RestaurantMenuItemTranslation> _restaurantMenuItemTranslationRepository;
        /// <summary>
        /// The chain table repository
        /// </summary>
        IRepository<ChainTable> _chainTableRepository;
        /// <summary>
        /// The chain details table repository
        /// </summary>
        IRepository<ChainDetailsTable> _chainDetailsTableRepository;
        /// <summary>
        /// The menu item suggestion repository
        /// </summary>
        IRepository<MenuItemSuggestion> _menuItemSuggestionRepository;
        /// <summary>
        /// The measurement unit repository
        /// </summary>
        IRepository<MeasurementUnit> _measurementUnitRepository;
        /// <summary>
        /// The food profile repository
        /// </summary>
        IRepository<FoodProfile> _foodProfileRepository;
        /// <summary>
        /// The ingredient food profile repository
        /// </summary>
        IRepository<IngredientFoodProfile> _ingredientFoodProfileRepository;
        /// <summary>
        /// The menu item offers repository
        /// </summary>
        IRepository<MenuItemOffer> _menuItemOffersRepository;
        /// <summary>
        /// The menu item ingredient nutrition repository
        /// </summary>
        IRepository<MenuItemIngredientNutrition> _menuItemIngredientNutritionRepository;
        /// <summary>
        /// The nutrition repository
        /// </summary>
        IRepository<Nutrition> _nutritionRepository;
        /// <summary>
        /// The room repository
        /// </summary>
        IRepository<Room> _roomRepository;

        /// <summary>
        /// The customer operations
        /// </summary>
        CRUDOperation<IRepository<Customer>, Customer> _customerOperations;
        /// <summary>
        /// The restaurant operations
        /// </summary>
        CRUDOperation<IRepository<Restaurant>, Restaurant> _restaurantOperations;
        /// <summary>
        /// The restaurant menus operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenu>, RestaurantMenu> _restaurantMenusOperations;
        /// <summary>
        /// The restaurant menu items operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuItem>, RestaurantMenuItem> _restaurantMenuItemsOperations;
        /// <summary>
        /// The restaurant customer reviews operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantOrderReview>, RestaurantOrderReview> _restaurantCustomerReviewsOperations;
        /// <summary>
        /// The restaurant review operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantReview>, RestaurantReview> _restaurantReviewOperations;
        /// <summary>
        /// The restaurant cart operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantCart>, RestaurantCart> _restaurantCartOperations;
        /// <summary>
        /// The restaurant cart item operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantCartItem>, RestaurantCartItem> _restaurantCartItemOperations;
        /// <summary>
        /// The restaurant table group operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantTableGroup>, RestaurantTableGroup> _restaurantTableGroupOperations;
        /// <summary>
        /// The restaurant tables operation
        /// </summary>
        CRUDOperation<IRepository<RestaurantTable>, RestaurantTable> _restaurantTablesOperation;
        /// <summary>
        /// The customer restaurant orders operation
        /// </summary>
        CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder> _customerRestaurantOrdersOperation;
        /// <summary>
        /// The restaurant order item operation
        /// </summary>
        CRUDOperation<IRepository<RestaurantOrderItem>, RestaurantOrderItem> _restaurantOrderItemOperation;
        /// <summary>
        /// The customer restaurant order items operation
        /// </summary>
        CRUDOperation<IRepository<Customer_RestaurantOrder_Items>, Customer_RestaurantOrder_Items> _customerRestaurantOrderItemsOperation;
        /// <summary>
        /// The cuisines operations
        /// </summary>
        CRUDOperation<IRepository<Cuisine>, Cuisine> _cuisinesOperations;
        /// <summary>
        /// The restaurant cuisines operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantCuisine>, RestaurantCuisine> _restaurantCuisinesOperations;
        /// <summary>
        /// The menu item review operation
        /// </summary>
        CRUDOperation<IRepository<MenuItemReview>, MenuItemReview> _menuItemReviewOperation;
        /// <summary>
        /// The restaurant menu group operation
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuGroup>, RestaurantMenuGroup> _restaurantMenuGroupOperation;
        /// <summary>
        /// The customer role operations
        /// </summary>
        CRUDOperation<IRepository<CustomerRole>, CustomerRole> _customerRoleOperations;
        /// <summary>
        /// The menu item ingredient operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemIngredient>, MenuItemIngredient> _menuItemIngredientOperations;
        /// <summary>
        /// The menu item size operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemSize>, MenuItemSize> _menuItemSizeOperations;
        /// <summary>
        /// The menu item additional operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemAdditional>, MenuItemAdditional> _menuItemAdditionalOperations;
        /// <summary>
        /// The restaurant table menu operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantTableMenu>, RestaurantTableMenu> _restaurantTableMenuOperations;
        /// <summary>
        /// The restaurant menu item image operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuItemImage>, RestaurantMenuItemImage> _restaurantMenuItemImageOperations;
        /// <summary>
        /// The menu additional group operations
        /// </summary>
        CRUDOperation<IRepository<MenuAdditionalGroup>, MenuAdditionalGroup> _menuAdditionalGroupOperations;
        /// <summary>
        /// The menu additional element operations
        /// </summary>
        CRUDOperation<IRepository<MenuAdditionalElement>, MenuAdditionalElement> _menuAdditionalElementOperations;
        /// <summary>
        /// The allergen categories operations
        /// </summary>
        CRUDOperation<IRepository<AllergenCategory>, AllergenCategory> _allergenCategoriesOperations;
        /// <summary>
        /// The allergen operation
        /// </summary>
        CRUDOperation<IRepository<Allergen>, Allergen> _allergenOperation;
        /// <summary>
        /// The ingredient categories operations
        /// </summary>
        CRUDOperation<IRepository<IngredientCategory>, IngredientCategory> _ingredientCategoriesOperations;
        /// <summary>
        /// The ingredient operation
        /// </summary>
        CRUDOperation<IRepository<Ingredient>, Ingredient> _ingredientOperation;
        /// <summary>
        /// The ingredient nutrition values operation
        /// </summary>
        CRUDOperation<IRepository<IngredientNutritionValues>, IngredientNutritionValues> _ingredientNutritionValuesOperation;
        /// <summary>
        /// The language operation
        /// </summary>
        CRUDOperation<IRepository<Language>, Language> _languageOperation;
        /// <summary>
        /// The restaurant menu translation operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuTranslation>, RestaurantMenuTranslation> _restaurantMenuTranslationOperations;
        /// <summary>
        /// The restaurant menu group translation operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuGroupTranslation>, RestaurantMenuGroupTranslation> _restaurantMenuGroupTranslationOperations;
        /// <summary>
        /// The restaurant menu item translation operations
        /// </summary>
        CRUDOperation<IRepository<RestaurantMenuItemTranslation>, RestaurantMenuItemTranslation> _restaurantMenuItemTranslationOperations;
        /// <summary>
        /// The chain table operation
        /// </summary>
        CRUDOperation<IRepository<ChainTable>, ChainTable> _chainTableOperation;
        /// <summary>
        /// The chain table details operations
        /// </summary>
        CRUDOperation<IRepository<ChainDetailsTable>, ChainDetailsTable> _chainTableDetailsOperations;
        /// <summary>
        /// The menu item suggestion operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemSuggestion>, MenuItemSuggestion> _menuItemSuggestionOperations;
        /// <summary>
        /// The measurement unit operations
        /// </summary>
        CRUDOperation<IRepository<MeasurementUnit>, MeasurementUnit> _measurementUnitOperations;
        /// <summary>
        /// The food profile operations
        /// </summary>
        CRUDOperation<IRepository<FoodProfile>, FoodProfile> _foodProfileOperations;
        /// <summary>
        /// The ingredient food profile operations
        /// </summary>
        CRUDOperation<IRepository<IngredientFoodProfile>, IngredientFoodProfile> _ingredientFoodProfileOperations;
        /// <summary>
        /// The menu item offers operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemOffer>, MenuItemOffer> _menuItemOffersOperations;
        /// <summary>
        /// The menu item ingredient nutrition operations
        /// </summary>
        CRUDOperation<IRepository<MenuItemIngredientNutrition>, MenuItemIngredientNutrition> _menuItemIngredientNutritionOperations;
        /// <summary>
        /// The nutrition operations
        /// </summary>
        CRUDOperation<IRepository<Nutrition>, Nutrition> _nutritionOperations;
        /// The customer hotel room repository
        /// </summary>
        IRepository<CustomerHotelRoom> _customerHotelRoomRepository;
        /// <summary>

        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantService"/> class.
        /// </summary>
        /// <param name="restaurantDetailsRepository">The restaurant details repository.</param>
        /// <param name="restaurantMenusRepository">The restaurant menus repository.</param>
        /// <param name="restaurantMenuItemsRepository">The restaurant menu items repository.</param>
        /// <param name="restaurantCustomerReviewsRepository">The restaurant customer reviews repository.</param>
        /// <param name="restaurantTableGroupsRepository">The restaurant table groups repository.</param>
        /// <param name="restaurantTablesRepository">The restaurant tables repository.</param>
        /// <param name="customerRestaurantOrdersRepository">The customer restaurant orders repository.</param>
        /// <param name="customerRestaurantOrderItemsRepository">The customer restaurant order items repository.</param>
        /// <param name="cuisinesRepository">The cuisines repository.</param>
        /// <param name="restaurantCuisinesRepository">The restaurant cuisines repository.</param>
        /// <param name="restaurantReviewRepository">The restaurant review repository.</param>
        /// <param name="restaurantCartRepository">The restaurant cart repository.</param>
        /// <param name="restaurantCartItemRepository">The restaurant cart item repository.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="menuAdditionalElementRepository">The menu additional element repository.</param>
        /// <param name="restaurantCartItemAdditional">The restaurant cart item additional.</param>
        /// <param name="restaurantOrderRepository">The restaurant order repository.</param>
        /// <param name="restaurantOrderItemRepository">The restaurant order item repository.</param>
        /// <param name="menuItemReviewRepository">The menu item review repository.</param>
        /// <param name="customerCreditCardRepository">The customer credit card repository.</param>
        /// <param name="restaurantMenuGroupRepository">The restaurant menu group repository.</param>
        /// <param name="customerRoleRepository">The customer role repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="menuItemIngredientRepository">The menu item ingredient repository.</param>
        /// <param name="menuItemSizeRepository">The menu item size repository.</param>
        /// <param name="menuItemAdditionalRepository">The menu item additional repository.</param>
        /// <param name="ingredientCategoriesRepository">The ingredient categories repository.</param>
        /// <param name="ingredientRepository">The ingredient repository.</param>
        /// <param name="ingredientNutritionValuesRepository">The ingredient nutrition values repository.</param>
        /// <param name="menuAdditionalGroupRepository">The menu additional group repository.</param>
        /// <param name="restaurantTableMenuRepository">The restaurant table menu repository.</param>
        /// <param name="restaurantMenuItemImageRepository">The restaurant menu item image repository.</param>
        /// <param name="allergenCategoriesRepository">The allergen categories repository.</param>
        /// <param name="allergenRepository">The allergen repository.</param>
        /// <param name="languageRepository">The language repository.</param>
        /// <param name="restaurantMenuTranslationRepository">The restaurant menu translation repository.</param>
        /// <param name="restaurantMenuGroupTranslationRepository">The restaurant menu group translation repository.</param>
        /// <param name="restaurantMenuItemTranslationRepository">The restaurant menu item translation repository.</param>
        /// <param name="chainTableRepository">The chain table repository.</param>
        /// <param name="chainDetailsTableRepository">The chain details table repository.</param>
        /// <param name="menuItemSuggestionRepository">The menu item suggestion repository.</param>
        /// <param name="measurementUnitRepository">The measurement unit repository.</param>
        /// <param name="foodProfileRepository">The food profile repository.</param>
        /// <param name="ingredientFoodProfileRepository">The ingredient food profile repository.</param>
        /// <param name="menuItemOffersRepository">The menu item offers repository.</param>
        /// <param name="menuItemIngredientNutritionRepository">The menu item ingredient nutrition repository.</param>
        /// <param name="nutritionRepository">The nutrition repository.</param>
        /// <param name="customerHotelRoomRepository">The customer hotel room repository.</param>
        public RestaurantService(
            IRepository<Restaurant> restaurantDetailsRepository,
            IRepository<RestaurantMenu> restaurantMenusRepository,
            IRepository<RestaurantMenuItem> restaurantMenuItemsRepository,
            IRepository<RestaurantOrderReview> restaurantCustomerReviewsRepository,
            IRepository<RestaurantTableGroup> restaurantTableGroupsRepository,
            IRepository<RestaurantTable> restaurantTablesRepository,
            IRepository<RestaurantOrder> customerRestaurantOrdersRepository,
            IRepository<Customer_RestaurantOrder_Items> customerRestaurantOrderItemsRepository,
            IRepository<Cuisine> cuisinesRepository,
            IRepository<RestaurantCuisine> restaurantCuisinesRepository,
            IRepository<RestaurantReview> restaurantReviewRepository,
            IRepository<RestaurantCart> restaurantCartRepository,
            IRepository<RestaurantCartItem> restaurantCartItemRepository,
            IRepository<Customer> customerRepository,
            IRepository<MenuAdditionalElement> menuAdditionalElementRepository,
            IRepository<RestaurantCartItemAdditional> restaurantCartItemAdditional,
            IRepository<RestaurantOrder> restaurantOrderRepository,
            IRepository<RestaurantOrderItem> restaurantOrderItemRepository,
            IRepository<MenuItemReview> menuItemReviewRepository,
            IRepository<CustomerCreditCard> customerCreditCardRepository,
            IRepository<RestaurantMenuGroup> restaurantMenuGroupRepository,
            IRepository<CustomerRole> customerRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<MenuItemIngredient> menuItemIngredientRepository,
            IRepository<MenuItemSize> menuItemSizeRepository,
            IRepository<MenuItemAdditional> menuItemAdditionalRepository,
            IRepository<IngredientCategory> ingredientCategoriesRepository,
            IRepository<Ingredient> ingredientRepository,
            IRepository<IngredientNutritionValues> ingredientNutritionValuesRepository,
            IRepository<MenuAdditionalGroup> menuAdditionalGroupRepository,
            IRepository<RestaurantTableMenu> restaurantTableMenuRepository,
            IRepository<RestaurantMenuItemImage> restaurantMenuItemImageRepository,
            IRepository<AllergenCategory> allergenCategoriesRepository,
            IRepository<Allergen> allergenRepository,
            IRepository<Language> languageRepository,
            IRepository<RestaurantMenuTranslation> restaurantMenuTranslationRepository,
            IRepository<RestaurantMenuGroupTranslation> restaurantMenuGroupTranslationRepository,
            IRepository<RestaurantMenuItemTranslation> restaurantMenuItemTranslationRepository,
            IRepository<ChainTable> chainTableRepository,
            IRepository<ChainDetailsTable> chainDetailsTableRepository,
            IRepository<MenuItemSuggestion> menuItemSuggestionRepository,
            IRepository<MeasurementUnit> measurementUnitRepository,
            IRepository<FoodProfile> foodProfileRepository,
            IRepository<IngredientFoodProfile> ingredientFoodProfileRepository,
            IRepository<MenuItemOffer> menuItemOffersRepository,
            IRepository<MenuItemIngredientNutrition> menuItemIngredientNutritionRepository,
            IRepository<Nutrition> nutritionRepository,
            IRepository<Room> roomRepository,
            IRepository<CustomerHotelRoom> customerHotelRoomRepository
            )
        {
            _restaurantDetailsRepository = restaurantDetailsRepository;
            _restaurantMenusRepository = restaurantMenusRepository;
            _restaurantMenuItemsRepository = restaurantMenuItemsRepository;
            _menuAdditionalElementRepository = menuAdditionalElementRepository;
            _restaurantCustomerReviewsRepository = restaurantCustomerReviewsRepository;
            _restaurantReviewRepository = restaurantReviewRepository;
            _restaurantCartRepository = restaurantCartRepository;
            _restaurantCartItemRepository = restaurantCartItemRepository;
            _restaurantCartItemAdditional = restaurantCartItemAdditional;
            _restaurantOrderRepository = restaurantOrderRepository;
            _restaurantOrderItemRepository = restaurantOrderItemRepository;
            _restaurantTableGroupsRepository = restaurantTableGroupsRepository;
            _restaurantTablesRepository = restaurantTablesRepository;
            _customerRestaurantOrdersRepository = customerRestaurantOrdersRepository;
            _customerRestaurantOrderItemsRepository = customerRestaurantOrderItemsRepository;
            _cuisinesRepository = cuisinesRepository;
            _restaurantCuisinesRepository = restaurantCuisinesRepository;
            _menuItemReviewRepository = menuItemReviewRepository;
            _customerCreditCardRepository = customerCreditCardRepository;
            _restaurantMenuGroupRepository = restaurantMenuGroupRepository;
            _customerRoleRepository = customerRoleRepository;
            _roleRepository = roleRepository;
            _menuItemIngredientRepository = menuItemIngredientRepository;
            _menuItemSizeRepository = menuItemSizeRepository;
            _menuItemAdditionalRepository = menuItemAdditionalRepository;
            _ingredientCategoriesRepository = ingredientCategoriesRepository;
            _ingredientRepository = ingredientRepository;
            _ingredientNutritionValuesRepository = ingredientNutritionValuesRepository;
            _menuAdditionalGroupRepository = menuAdditionalGroupRepository;
            _restaurantTableMenuRepository = restaurantTableMenuRepository;
            _customerRepository = customerRepository;
            _restaurantMenuItemImageRepository = restaurantMenuItemImageRepository;
            _allergenCategoriesRepository = allergenCategoriesRepository;
            _allergenRepository = allergenRepository;
            _languageRepository = languageRepository;
            _restaurantMenuTranslationRepository = restaurantMenuTranslationRepository;
            _restaurantMenuGroupTranslationRepository = restaurantMenuGroupTranslationRepository;
            _restaurantMenuItemTranslationRepository = restaurantMenuItemTranslationRepository;
            _chainTableRepository = chainTableRepository;
            _chainDetailsTableRepository = chainDetailsTableRepository;
            _menuItemSuggestionRepository = menuItemSuggestionRepository;
            _measurementUnitRepository = measurementUnitRepository;
            _foodProfileRepository = foodProfileRepository;
            _ingredientFoodProfileRepository = ingredientFoodProfileRepository;
            _menuItemOffersRepository = menuItemOffersRepository;
            _menuItemIngredientNutritionRepository = menuItemIngredientNutritionRepository;
            _nutritionRepository = nutritionRepository;
            _roomRepository = roomRepository;
            _customerHotelRoomRepository = customerHotelRoomRepository;

            _customerOperations = new CRUDOperation<IRepository<Customer>, Customer>(_customerRepository);
            _restaurantOperations = new CRUDOperation<IRepository<Restaurant>, Restaurant>(_restaurantDetailsRepository);
            _restaurantMenusOperations = new CRUDOperation<IRepository<RestaurantMenu>, RestaurantMenu>(_restaurantMenusRepository);
            _restaurantMenuItemsOperations = new CRUDOperation<IRepository<RestaurantMenuItem>, RestaurantMenuItem>(_restaurantMenuItemsRepository);
            _restaurantCartOperations = new CRUDOperation<IRepository<RestaurantCart>, RestaurantCart>(_restaurantCartRepository);
            _restaurantCustomerReviewsOperations = new CRUDOperation<IRepository<RestaurantOrderReview>, RestaurantOrderReview>(_restaurantCustomerReviewsRepository);
            _restaurantReviewOperations = new CRUDOperation<IRepository<RestaurantReview>, RestaurantReview>(_restaurantReviewRepository);
            _restaurantTableGroupOperations = new CRUDOperation<IRepository<RestaurantTableGroup>, RestaurantTableGroup>(_restaurantTableGroupsRepository);
            _restaurantTablesOperation = new CRUDOperation<IRepository<RestaurantTable>, RestaurantTable>(_restaurantTablesRepository);
            _customerRestaurantOrdersOperation = new CRUDOperation<IRepository<RestaurantOrder>, RestaurantOrder>(_customerRestaurantOrdersRepository);
            _restaurantOrderItemOperation = new CRUDOperation<IRepository<RestaurantOrderItem>, RestaurantOrderItem>(_restaurantOrderItemRepository);
            _cuisinesOperations = new CRUDOperation<IRepository<Cuisine>, Cuisine>(_cuisinesRepository);
            _restaurantCuisinesOperations = new CRUDOperation<IRepository<RestaurantCuisine>, RestaurantCuisine>(_restaurantCuisinesRepository);
            _menuItemReviewOperation = new CRUDOperation<IRepository<MenuItemReview>, MenuItemReview>(_menuItemReviewRepository);
            _restaurantMenuGroupOperation = new CRUDOperation<IRepository<RestaurantMenuGroup>, RestaurantMenuGroup>(_restaurantMenuGroupRepository);
            _customerRoleOperations = new CRUDOperation<IRepository<CustomerRole>, CustomerRole>(_customerRoleRepository);
            _menuItemIngredientOperations = new CRUDOperation<IRepository<MenuItemIngredient>, MenuItemIngredient>(_menuItemIngredientRepository);
            _menuItemSizeOperations = new CRUDOperation<IRepository<MenuItemSize>, MenuItemSize>(_menuItemSizeRepository);
            _menuItemAdditionalOperations = new CRUDOperation<IRepository<MenuItemAdditional>, MenuItemAdditional>(_menuItemAdditionalRepository);
            _restaurantTableMenuOperations = new CRUDOperation<IRepository<RestaurantTableMenu>, RestaurantTableMenu>(_restaurantTableMenuRepository);
            _restaurantMenuItemImageOperations = new CRUDOperation<IRepository<RestaurantMenuItemImage>, RestaurantMenuItemImage>(_restaurantMenuItemImageRepository);
            _menuAdditionalGroupOperations = new CRUDOperation<IRepository<MenuAdditionalGroup>, MenuAdditionalGroup>(_menuAdditionalGroupRepository);
            _menuAdditionalElementOperations = new CRUDOperation<IRepository<MenuAdditionalElement>, MenuAdditionalElement>(_menuAdditionalElementRepository);
            _allergenCategoriesOperations = new CRUDOperation<IRepository<AllergenCategory>, AllergenCategory>(_allergenCategoriesRepository);
            _allergenOperation = new CRUDOperation<IRepository<Allergen>, Allergen>(_allergenRepository);
            _ingredientCategoriesOperations = new CRUDOperation<IRepository<IngredientCategory>, IngredientCategory>(_ingredientCategoriesRepository);
            _ingredientOperation = new CRUDOperation<IRepository<Ingredient>, Ingredient>(_ingredientRepository);
            _ingredientNutritionValuesOperation = new CRUDOperation<IRepository<IngredientNutritionValues>, IngredientNutritionValues>(_ingredientNutritionValuesRepository);
            _languageOperation = new CRUDOperation<IRepository<Language>, Language>(_languageRepository);
            _restaurantMenuTranslationOperations = new CRUDOperation<IRepository<RestaurantMenuTranslation>, RestaurantMenuTranslation>(_restaurantMenuTranslationRepository);
            _restaurantMenuGroupTranslationOperations = new CRUDOperation<IRepository<RestaurantMenuGroupTranslation>, RestaurantMenuGroupTranslation>(_restaurantMenuGroupTranslationRepository);
            _restaurantMenuItemTranslationOperations = new CRUDOperation<IRepository<RestaurantMenuItemTranslation>, RestaurantMenuItemTranslation>(_restaurantMenuItemTranslationRepository);
            _chainTableOperation = new CRUDOperation<IRepository<ChainTable>, ChainTable>(_chainTableRepository);
            _chainTableDetailsOperations = new CRUDOperation<IRepository<ChainDetailsTable>, ChainDetailsTable>(_chainDetailsTableRepository);
            _menuItemSuggestionOperations = new CRUDOperation<IRepository<MenuItemSuggestion>, MenuItemSuggestion>(_menuItemSuggestionRepository);
            _measurementUnitOperations = new CRUDOperation<IRepository<MeasurementUnit>, MeasurementUnit>(_measurementUnitRepository);
            _foodProfileOperations = new CRUDOperation<IRepository<FoodProfile>, FoodProfile>(_foodProfileRepository);
            _ingredientFoodProfileOperations = new CRUDOperation<IRepository<IngredientFoodProfile>, IngredientFoodProfile>(_ingredientFoodProfileRepository);
            _menuItemOffersOperations = new CRUDOperation<IRepository<MenuItemOffer>, MenuItemOffer>(_menuItemOffersRepository);
            _menuItemIngredientNutritionOperations = new CRUDOperation<IRepository<MenuItemIngredientNutrition>, MenuItemIngredientNutrition>(_menuItemIngredientNutritionRepository);
            _nutritionOperations = new CRUDOperation<IRepository<Nutrition>, Nutrition>(_nutritionRepository);
            //_customerHotelRoomOperation = new CRUDOperation<IRepository<CustomerHotelRoom>, CustomerHotelRoom>(_customerHotelRoomRepository);
        }


        #endregion initialization

        #region restaurant


        /// <summary>
        /// Searches the specified total.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="radius_min">The radius minimum.</param>
        /// <param name="radius_max">The radius maximum.</param>
        /// <param name="Ids">The ids.</param>
        /// <param name="rating_min">The rating minimum.</param>
        /// <param name="rating_max">The rating maximum.</param>
        /// <param name="price_min">The price minimum.</param>
        /// <param name="price_max">The price maximum.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="selection">The selection.</param>
        /// <param name="sortOn">The sort on.</param>
        /// <param name="order">The order.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <param name="searchTextType">Type of the search text.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="openingHour">The opening hour.</param>
        /// <param name="closingHour">The closing hour.</param>
        /// <param name="starRating">The star rating.</param>
        /// <param name="priceRange">The price range.</param>
        /// <param name="minUserRating">The minimum user rating.</param>
        /// <param name="maxUserRating">The maximum user rating.</param>
        /// <param name="groupBy">The group by.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> Search(out int total, string searchText, double? lat, double? lng, double? radius_min, double? radius_max, int[] Ids, double? rating_min, double? rating_max, double? price_min, double? price_max, int page = 1, int limit = 20, Selection selection = Selection.All, RestaurantSort sortOn = RestaurantSort.Cost, SortOrder order = SortOrder.Asc, bool considerMyPreferences = false, SearchTextType searchTextType = SearchTextType.Everywhere, int? customerId = null, TimeSpan? openingHour = null, TimeSpan? closingHour = null, int? starRating = null, int? priceRange = null, int? minUserRating = null, int? maxUserRating = null, GroupBy groupBy = GroupBy.None)
        {
            var query = _restaurantDetailsRepository.Table.Where(r => r.ActiveStatus == true && r.IsActive == true && r.HotelRestaurants.Count() < 1);


            if (!string.IsNullOrWhiteSpace(searchText))
            {
                switch (searchTextType)
                {
                    case SearchTextType.Everywhere:
                        query = query.Where(r => r.Name.Contains(searchText) ||
                    (r.Street + " " + r.City + " " + r.Region).Contains(searchText));
                        break;
                    case SearchTextType.RestaurantName:
                        query = query.Where(r => r.Name.Contains(searchText));
                        break;
                }
            }

            if (lat.HasValue && lng.HasValue)
            {
                var sourcePoint = CreatePoint(lat.Value, lng.Value);
                query = query.Where(loc => loc.Location.Distance(sourcePoint) >= radius_min.Value && loc.Location.Distance(sourcePoint) < radius_max.Value);
            }

            if (Ids != null && Ids.Length > 0)
            {
                query = query.Where(r => r.RestaurantCuisines.Any(rc => Ids.Contains(rc.Cuisine.Id)));
            }
            if (starRating != null)
            {
                query = query.Where(r => r.StarRating == starRating);
            }
            if (priceRange != null)
            {
                query = query.Where(r => r.PriceRange == priceRange);
            }
            if (minUserRating != null)
            {
                query = query.Where(r => r.RestaurantReviews.Average(rv => rv.Score) > minUserRating);
            }
            if (maxUserRating != null)
            {
                query = query.Where(r => r.RestaurantReviews.Average(rv => rv.Score) <= maxUserRating);
            }
            if (rating_min.HasValue)
            {
                query = query.Where(r => r.RestaurantReviews.Average(rv => rv.Score) >= rating_min);
            }
            if (rating_max.HasValue)
            {
                query = query.Where(r => r.RestaurantReviews.Average(rv => rv.Score) <= rating_max);
            }

            if (price_min.HasValue)
            {
                query = query.Where(r => r.CostForTwo >= price_min);
            }
            if (price_max.HasValue)
            {
                query = query.Where(r => r.CostForTwo <= price_max);

            }
            if (openingHour.HasValue && closingHour == null)
            {
                query = query.Where(r => r.OpeningHours >= openingHour.Value);
            }

            if (closingHour.HasValue && openingHour == null)
            {
                query = query.Where(r => r.ClosingHours <= closingHour.Value);
            }
            if (openingHour.HasValue && closingHour.HasValue)
            {
                if (openingHour < closingHour)
                    query = query.Where(r => (r.OpeningHours >= openingHour.Value && (r.ClosingHours <= closingHour.Value && r.ClosingHours >= openingHour.Value)));
                else
                    query = query.Where(r => ((r.OpeningHours >= openingHour.Value && (r.ClosingHours <= closingHour.Value || r.ClosingHours >= openingHour.Value)) ||
                                              (r.OpeningHours >= openingHour.Value && r.ClosingHours >= closingHour.Value && !(r.ClosingHours <= openingHour.Value))));
            }

            if (considerMyPreferences)
            {
                // restaurants which serve my preferred items
                // get customer preferred ingredients, match that with restaurants menus
                System.Data.SqlClient.SqlParameter customerParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var preferredRestaurants = _restaurantDetailsRepository.ExecuteStoredProcedureList<int>("exec CustomerPreferredRestaurant", customerParameter);

                if (preferredRestaurants.Count > 0)
                    query = query.Where(r => preferredRestaurants.Contains(r.Id));

            }

            switch (selection)
            {
                case Selection.All:
                    break;
                case Selection.Popular:
                    var toDate = DateTime.Now;
                    var fromDate = toDate.AddDays(-30);
                    var orders = Convert.ToInt32(ConfigurationSettings.AppSettings["Orders"]);
                    query = query.Where(r => r.RestaurantOrders.Count(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDate)
                                                                        && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(toDate)) >= orders);
                    break;
                case Selection.Personal:

                    if (customerId.HasValue && customerId.Value > 0)
                    {
                        query = query.Where(r => r.CustomerFavoriteRestaurants.Any(f => f.CustomerId == customerId.Value));
                    }
                    break;
            }

            switch (sortOn)
            {
                case RestaurantSort.Popularity:

                    query = (order == SortOrder.Asc) ? query.OrderBy(r => r.RestaurantOrders.Count()) : query.OrderByDescending(r => r.RestaurantOrders.Count());
                    break;
                case RestaurantSort.Rating:

                    query = (order == SortOrder.Asc)
                        ? query.OrderBy(r => r.RestaurantOrders.SelectMany(o => o.RestaurantOrderReviews).Average(rv => rv.Score))
                        : query.OrderByDescending(r => r.RestaurantOrders.SelectMany(o => o.RestaurantOrderReviews).Average(rv => rv.Score));

                    break;
                case RestaurantSort.Distance:
                    if (lat.HasValue && lng.HasValue)
                    {
                        var sourcePoint = CreatePoint(lat.Value, lng.Value);
                        query = (order == SortOrder.Asc) ? query.OrderBy(r => r.Location.Distance(sourcePoint)) : query.OrderByDescending(r => r.Location.Distance(sourcePoint));
                    }
                    else
                        query = query.OrderBy(r => r.Id);
                    break;
                case RestaurantSort.Cost:
                    query = (order == SortOrder.Asc) ? query.OrderBy(r => r.CostForTwo) : query.OrderByDescending(r => r.CostForTwo);
                    break;
                //case RestaurantSort.Price:
                //    query = (order == SortOrder.Asc) ? query.OrderBy(r => r.Night_Price) : query.OrderByDescending(r => r.Night_Price);
                //    break;
                default:
                    query = query.OrderBy(r => r.CostForTwo);
                    break;
            }

            var pages = query.OrderBy(_ => true)
            .Skip((page - 1) * limit)
            .Take(limit)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Restaurant>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }



        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="range">The range.</param>
        /// <param name="keyword">The keyword.</param>
        /// <param name="cuisines">The cuisines.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="order">The order.</param>
        /// <returns>List&lt;Restaurant&gt;.</returns>
        public List<Restaurant> SearchRestaurants(
            out int total,
            double? lat = null,
            double? lng = null,
            double? range = null,
            string keyword = "",
            string cuisines = "",
            int? page = null,
            int? limit = null,
            string sort = "",
            string order = " "
        )
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = limit != null ? limit.Value : 50;
            string orderBy = string.IsNullOrEmpty(order) ? "asc" : order;
            double radius = range != null ? range.Value : 1000;
            var query = _restaurantDetailsRepository.Table;


            //keyword
            if (!String.IsNullOrWhiteSpace(keyword))
            {
                query = from r in query.OrderBy(r => r.Name)
                        where (r.Name.Contains(keyword)) ||
                        (r.Description.Contains(keyword)) ||
                        ((r.City + " " + r.Region + " " + r.Country).Contains(keyword))
                        select r;
            }
            if (!String.IsNullOrWhiteSpace(cuisines))
            {
                query = from r in query
                        from rc in r.RestaurantCuisines.Where(rc => rc.Cuisine.Name.Contains(cuisines))
                        select r;

            }
            if (lat != null && lng != null)
            {
                var latitude = Convert.ToDouble(lat);
                var longitude = Convert.ToDouble(lng);
                var sourcePoint = CreatePoint(latitude, longitude);
                if (orderBy == "asc")
                    query = query.Where(loc => loc.Location.Distance(sourcePoint) < radius).OrderBy(loc => loc.Location.Distance(sourcePoint));
                else
                    query = query.Where(loc => loc.Location.Distance(sourcePoint) < radius).OrderByDescending(loc => loc.Location.Distance(sourcePoint));
            }

            if (String.IsNullOrWhiteSpace(keyword) && (lat == null && lng == null) && String.IsNullOrWhiteSpace(cuisines))
            {
                if (orderBy == "asc")
                    query = query.Where(x => true).OrderBy(r => r.CostForTwo);
                else
                    query = query.Where(x => true).OrderByDescending(r => r.CostForTwo);
            }

            if (!String.IsNullOrEmpty(sort))
            {
                if (sort == "Popularity")
                {

                    //query = from r in query
                    //        from cro in r.Customer_RestaurantOrders
                    //        where r.RestaurantId == cro.RestaurantId
                    //        group r by cro.RestaurantId into restOrders
                    //        orderby restOrders.Count()
                    //        select r;
                    //{
                    //    r = restOrders.Key,
                    //    numberoforder = restOrders.Count()
                    //};


                    //query = from r in query
                    //        from cro in r.Customer_RestaurantOrders.Where(ro=>ro.RestaurantId==r.RestaurantId).GroupBy(cro => cro.RestaurantId).Maxb(o=>o.Count())
                    //        select r;

                }
                else if (sort == "Rating")
                {
                    //query = from r in query
                    //        from cro in r.Customer_RestaurantOrders.GroupBy(cro => cro.Restaurant_Customer_Reviews.OrderByDescending(rw=>rw.Id)).OrderByDescending(o => o.Count())
                    //        select r;
                }
                else if (sort == "Distance")
                {
                    query = query;
                }
                else if (sort == "Cost")
                {

                }
            }
            var pages = query.OrderBy(_ => true)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Restaurant>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <param name="range">The range.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="order">The order.</param>
        /// <returns>List Restaurant.</returns>
        public List<Restaurant> SearchRestaurants
         (
            out int total,
            double? lat = null,
            double? lng = null,
            double? range = null,
            int? page = null,
            int? limit = null,
            string sort = "",
            string order = " "
        )
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = limit != null ? limit.Value : 50;
            string orderBy = string.IsNullOrEmpty(order) ? "desc" : order;
            double radius = range != null ? range.Value : 1000;
            var query = _restaurantDetailsRepository.Table;
            if (lat != null && lng != null)
            {
                var latitude = Convert.ToDouble(lat);
                var longitude = Convert.ToDouble(lng);
                var sourcePoint = CreatePoint(latitude, longitude);
                if (orderBy == "asc")
                    query = query.Where(loc => loc.Location.Distance(sourcePoint) < radius).OrderBy(loc => loc.Location.Distance(sourcePoint));
                else
                    query = query.Where(loc => loc.Location.Distance(sourcePoint) < radius).OrderByDescending(loc => loc.Location.Distance(sourcePoint));
            }
            else
            {
                if (orderBy == "asc")
                    query = query.OrderBy(r => r.CostForTwo);
                else
                    query = query.OrderByDescending(r => r.CostForTwo);
            }

            var default_pages = query.Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .GroupBy(p => new { Total = query.Count() })
             .FirstOrDefault();
            total = default_pages.Key.Total;
            var default_entities = default_pages.Select(p => p);
            return default_entities.ToList();
        }



        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="keywords">The keywords.</param>
        /// <param name="place">The place.</param>
        /// <param name="cuisineIds">The cuisine ids.</param>
        /// <returns>List Restaurant.</returns>
        public List<Restaurant> SearchRestaurants(
           string keywords = null,
           string place = null,
           IList<int> cuisineIds = null
           )
        {
            var restaurants = new List<Restaurant>();
            var query = _restaurantDetailsRepository.Table;

            //keyword
            if (!String.IsNullOrWhiteSpace(keywords))
            {
                query = from r in query
                        where (r.Name.Contains(keywords)) ||
                        (r.Description.Contains(keywords)) ||
                        ((r.Street + " " + r.Street2 + " " + r.City + " " + r.Region).Contains(keywords))
                        select r;
            }

            if (!String.IsNullOrWhiteSpace(place))
            {
                query = query.Where(r => (r.Street + " " + r.Street2 + " " + r.City + " " + r.Region).Contains(place));
            }

            // cuisines

            if (cuisineIds != null && cuisineIds.Count > 0)
            {
                query = from r in query
                        from rc in r.RestaurantCuisines.Where(rc => cuisineIds.Contains(rc.CuisineId))
                        select r;
            }

            restaurants = query.ToList();

            return restaurants;
        }


        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="lat">The lat.</param>
        /// <param name="lng">The LNG.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> SearchRestaurants(double lat, double lng)
        {
            var sourcePoint = CreatePoint(lat, lng);
            return _restaurantOperations.GetAll(loc => loc.Location.Distance(sourcePoint) < 5).OrderBy(loc => loc.Location.Distance(sourcePoint)); ;
        }

        /// <summary>
        /// Searches the restaurants.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="range">The range.</param>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> SearchRestaurants(double latitude, double longitude, double range, string orderby, int pageNumber, int pageSize, out int total)
        {
            //var text = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", longitude, latitude);
            var sourcePoint = CreatePoint(latitude, longitude);

            return _restaurantOperations.GetAll(loc => loc.Location.Distance(sourcePoint) < range, orderby, pageNumber, pageSize, out total).OrderBy(loc => loc.Location.Distance(sourcePoint));

        }

        /// <summary>
        /// Creates the point.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <returns>DbGeography.</returns>
        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                     "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(text, 4326);
        }

        /// <summary>
        /// Gets all restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllRestaurants()
        {
            return _restaurantOperations.GetAll();
        }

        /// <summary>
        /// Gets all non virtual restaurants.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllNonVirtualRestaurants()
        {
            return _restaurantOperations.GetAll(r => r.ActiveStatus && r.HotelRestaurants.Count() < 1);
        }
        /// <summary>
        /// Gets all virtual restaurants.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllVirtualRestaurants(int skip, int pageSize, out int total)
        {
            return _restaurantOperations.GetAllWithServerSidePagging(r => r.ActiveStatus && r.HotelRestaurants.Count() > 0, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets all user restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllUserRestaurants(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            if (customerRole.RoleId == 1)
            {
                return _restaurantOperations.GetAll(r => r.ActiveStatus);
            }
            else
            {
                return _chainTableDetailsOperations.GetAll(c => c.ChainId == customerRole.ChainId && c.RestaurantId != null).Select(r => r.Restaurant).ToList();
            }
        }

        /// <summary>
        /// Gets the restaurants.
        /// </summary>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetRestaurants(string orderby, int pageNumber, int pageSize, out int total)
        {
            return _restaurantOperations.GetAll(orderby, pageNumber, pageSize, out total);
        }


        /// <summary>
        /// Gets the restaurant by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Restaurant.</returns>
        public Restaurant GetRestaurantById(int id)
        {
            return _restaurantOperations.GetById(id);
        }

        /// <summary>
        /// Creates the restaurant.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Restaurant.</returns>
        public Restaurant CreateRestaurant(Restaurant details)
        {
            var location = CreatePoint(Convert.ToDouble(details.Latitude), Convert.ToDouble(details.Longitude));
            details.Location = location;
            details.CreationDate = DateTime.UtcNow;
            return _restaurantOperations.AddNew(details);
        }

        /// <summary>
        /// Modifies the restaurant.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>Restaurant.</returns>
        public Restaurant ModifyRestaurant(Restaurant details)
        {
            var location = CreatePoint(Convert.ToDouble(details.Latitude), Convert.ToDouble(details.Longitude));
            details.Location = location;
            _restaurantOperations.Update(details.Id, details);
            return details;
        }

        /// <summary>
        /// Deletes the restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRestaurant(int id)
        {
            _restaurantOperations.Delete(id);
        }

        /// <summary>
        /// Gets the name of all restaurants by.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllRestaurantsByName(int customerId, string restaurant_name)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            var query = _restaurantDetailsRepository.Table.Where(r => r.ActiveStatus == true && r.IsActive == true);
            if (customerRole != null)
            {
                if (customerRole.RoleId == 1)
                {
                    return query.Where(r => r.Name.Contains(restaurant_name)).ToList();
                }
                else
                {
                    var restaurantIds = _chainDetailsTableRepository.Table.Where(c => c.ChainId == customerRole.ChainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();
                    return query.Where(r => r.Name.Contains(restaurant_name) && restaurantIds.Contains(r.Id)).ToList();
                }
            }
            else
            {
                return query.ToList();
            }


        }

        /// <summary>
        /// Gets all restaurants by name of owner.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllRestaurantsByNameOfOwner(int customerId, string restaurant_name)
        {
            var restaurants = _customerRoleOperations.GetAll(c => c.CustomerId == customerId).Select(r => r.RestaurantId).ToList();
            var query = _restaurantDetailsRepository.Table;
            return query.Where(r => r.Name.Contains(restaurant_name) && restaurants.Contains(r.Id)).ToList();
        }
        /// <summary>
        /// Gets all last week restaurants.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllLastWeekRestaurants(DateTime from, DateTime to)
        {
            var query = _restaurantDetailsRepository.Table;
            query = query.Where(r => DbFunctions.TruncateTime(r.CreationDate) >= DbFunctions.TruncateTime(from)
                                    && DbFunctions.TruncateTime(r.CreationDate) <= DbFunctions.TruncateTime(to));
            return query.ToList();
        }
        /// <summary>
        /// Gets the restaurant groups by star rating.
        /// </summary>
        /// <returns>IEnumerable RestaurantGroupByStarRating.</returns>
        public IEnumerable<RestaurantGroupByStarRating> GetRestaurantGroupsByStarRating()
        {
            var query = _restaurantDetailsRepository.Table;
            var result = query.GroupBy(r => r.StarRating).OrderByDescending(rg => rg.Key).Select(gr => new RestaurantGroupByStarRating { StarRating = gr.Key, Restaurant = gr.Take(5).ToList() });
            return result;
        }
        /// <summary>
        /// Gets the restaurant by star rating.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetRestaurantByStarRating(int rating, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantDetailsRepository.Table;
            query = query.Where(r => r.StarRating == rating);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Restaurant>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets the restaurant groups by price range.
        /// </summary>
        /// <returns>IEnumerable RestaurantGroupByPriceRange.</returns>
        public IEnumerable<RestaurantGroupByPriceRange> GetRestaurantGroupsByPriceRange()
        {
            var query = _restaurantDetailsRepository.Table;
            var result = query.GroupBy(r => r.PriceRange).OrderByDescending(rg => rg.Key).Select(gr => new RestaurantGroupByPriceRange { PriceRange = gr.Key, Restaurant = gr.Take(5).ToList() });
            return result;
        }

        /// <summary>
        /// Gets the restaurants by price range.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetRestaurantsByPriceRange(int price, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantDetailsRepository.Table;
            query = query.Where(r => r.PriceRange == price);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<Restaurant>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        #endregion

        #region Menu Item Offers
        //API
        /// <summary>
        /// Gets all rest offers menu items.
        /// </summary>
        /// <param name="hotelId">The hotel identifier.</param>
        /// <returns>IEnumerable MenuItemOffer.</returns>
        public IEnumerable<MenuItemOffer> GetAllRestOffersMenuItems(int? hotelId)
        {
            if (hotelId == null)
                return _menuItemOffersOperations.GetAll(o => o.IsActive && o.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant.HotelRestaurants.Count() < 1);
            else
                return _menuItemOffersOperations.GetAll(o => o.IsActive && o.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant.HotelRestaurants.Any(hr => hr.HotelId == hotelId));

        }

        //Web

        /// <summary>
        /// Gets the offers menu items of rest.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemOffer.</returns>
        public IEnumerable<MenuItemOffer> GetOffersMenuItemsOfRest(int restaurantId, int skip, int pageSize, out int total)
        {
            return _menuItemOffersOperations.GetAllWithServerSidePagging(o => o.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the unassigned offer menu items of rest.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetUnassignedOfferMenuItemsOfRest(int restaurantId)
        {
            var assignedOfferMenuItemsIds = _menuItemOffersOperations.GetAll(o => o.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId).Select(o => o.MenuItemId).ToList();
            return _restaurantMenuItemsOperations.GetAll(i => i.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId && i.IsActive && !assignedOfferMenuItemsIds.Contains(i.Id));
        }

        /// <summary>
        /// Gets the discounted menu item by identifier.
        /// </summary>
        /// <param name="menuItemOfferId">The menu item offer identifier.</param>
        /// <returns>MenuItemOffer.</returns>
        public MenuItemOffer GetDiscountedMenuItemById(int menuItemOfferId)
        {
            return _menuItemOffersOperations.GetById(menuItemOfferId);
        }
        /// <summary>
        /// Creates the menu item offer.
        /// </summary>
        /// <param name="menuItemOffer">The menu item offer.</param>
        /// <returns>MenuItemOffer.</returns>
        public MenuItemOffer CreateMenuItemOffer(MenuItemOffer menuItemOffer)
        {
            return _menuItemOffersOperations.AddNew(menuItemOffer);
        }
        /// <summary>
        /// Modifies the menu item offer.
        /// </summary>
        /// <param name="menuItemOffer">The menu item offer.</param>
        /// <returns>MenuItemOffer.</returns>
        public MenuItemOffer ModifyMenuItemOffer(MenuItemOffer menuItemOffer)
        {
            _menuItemOffersOperations.Update(menuItemOffer.Id, menuItemOffer);
            return menuItemOffer;
        }
        /// <summary>
        /// Deletes the menu item offer.
        /// </summary>
        /// <param name="menuItemOfferid">The menu item offerid.</param>
        public void DeleteMenuItemOffer(int menuItemOfferid)
        {
            _menuItemOffersOperations.Delete(menuItemOfferid);
        }

        #endregion Menu Item Offers

        #region restaurant menu

        /// <summary>
        /// Gets the daily menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        public IEnumerable<RestaurantMenu> GetDailyMenu(int RestaurantId, int customerId, int? languageId, Func<RestaurantMenu, Object> orderByPredicate, int pageNumber, int pageSize, out int total, bool considerMyPreferences = false)
        {

            if (considerMyPreferences)
            {
                var preferredMenuIds = GetCustomerPreferredMenus(customerId, RestaurantId).Select(m => m.Id).ToList();

                if (preferredMenuIds.Count > 0)
                {
                    var restaurantMenus = _restaurantMenusOperations.GetAll(r => r.RestaurantId == RestaurantId && r.IsActive == true && preferredMenuIds.Contains(r.Id), orderByPredicate, pageNumber, pageSize, out  total);
                    if (restaurantMenus.Any() && languageId != null)
                    {
                        foreach (var menu in restaurantMenus)
                        {
                            if (languageId != menu.LanguageId && menu.RestaurantMenuTranslations.Count(l => l.LanguageId == languageId) != 0)
                            {
                                foreach (var menuTrans in menu.RestaurantMenuTranslations)
                                {
                                    if (menuTrans.LanguageId == languageId)
                                    {
                                        menu.MenuName = menuTrans.TranslatedMenuName;
                                        menu.MenuDescription = menuTrans.TranslatedMenuDescription;
                                    }
                                }
                            }
                        }
                    }
                    return restaurantMenus;
                }
                else
                {
                    total = 0;
                    return new List<RestaurantMenu>();
                }

            }
            var restMenus = _restaurantMenusOperations.GetAll(r => r.RestaurantId == RestaurantId && r.IsActive == true, orderByPredicate, pageNumber, pageSize, out  total);
            if (restMenus.Any() && languageId != null)
            {
                foreach (var menu in restMenus)
                {
                    if (languageId != menu.LanguageId && menu.RestaurantMenuTranslations.Count(l => l.LanguageId == languageId) != 0)
                    {
                        foreach (var menuTrans in menu.RestaurantMenuTranslations)
                        {
                            if (menuTrans.LanguageId == languageId)
                            {
                                menu.MenuName = menuTrans.TranslatedMenuName;
                                menu.MenuDescription = menuTrans.TranslatedMenuDescription;
                            }
                        }
                    }
                }
            }
            return restMenus;
        }

        /// <summary>
        /// Gets the customer preferred menus.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        public IEnumerable<RestaurantMenu> GetCustomerPreferredMenus(int customerId, int RestaurantId)
        {
            // menu which serve my preferred items
            // get customer preferred ingredients, match that with restaurants menus 
            System.Data.SqlClient.SqlParameter customerIdParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            System.Data.SqlClient.SqlParameter restaurantIdParameter = new System.Data.SqlClient.SqlParameter("restaurantId", Convert.ToInt32(RestaurantId));
            var custPrefMenus = _restaurantDetailsRepository.ExecuteStoredProcedureList<RestaurantMenu>("exec CustomerPreferredMenus", customerIdParameter, restaurantIdParameter);

            //allergic menus based on CPA
            System.Data.SqlClient.SqlParameter custId = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            System.Data.SqlClient.SqlParameter restId = new System.Data.SqlClient.SqlParameter("restaurantId", Convert.ToInt32(RestaurantId));
            var allergicMenuIds = _restaurantDetailsRepository.ExecuteStoredProcedureList<int>("exec CustomerAllergicMenus", custId, restId);

            var custPreferredMenus = custPrefMenus.Where(pm => !allergicMenuIds.Contains(pm.Id));
            return custPreferredMenus;
        }

        /// <summary>
        /// Gets all restaurant menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        public IEnumerable<RestaurantMenu> GetAllRestaurantMenu(int RestaurantId, Func<RestaurantMenu, Object> orderByPredicate, int pageNumber, int pageSize, out int total)
        {
            return _restaurantMenusOperations.GetAll(r => r.RestaurantId == RestaurantId, orderByPredicate, pageNumber, pageSize, out  total);
        }
        /// <summary>
        /// Gets all unassigned rest table menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="tableId">The table identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        public IEnumerable<RestaurantMenu> GetAllUnassignedRestTableMenu(int RestaurantId, int tableId)
        {
            var assignedTableMenuIds = _restaurantTableMenuOperations.GetAll(t => t.RestaurantTableId == tableId).Select(t => t.RestaurantMenuId).ToList();
            return _restaurantMenusOperations.GetAll(r => r.RestaurantId == RestaurantId && !assignedTableMenuIds.Contains(r.Id));
        }
        /// <summary>
        /// Gets the daily menu language wise.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public IEnumerable<RestaurantMenu> GetDailyMenuLanguageWise(int RestaurantId, int? languageId, int pageNumber, int pageSize, out int total)
        {
            if (languageId == null)
            {
                return AllRestaurantMenu(RestaurantId, pageNumber, pageSize, out total);
            }
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);
                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                var restaurantMenu = AllRestaurantMenu(RestaurantId, pageNumber, pageSize, out total);

                if (restaurantMenu.Any() && languageId != null)
                {
                    return MenuTransalation(restaurantMenu, Convert.ToInt32(languageId));
                }
                return restaurantMenu;
            }
        }

        /// <summary>
        /// Alls the restaurant menu.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenu&gt;.</returns>
        private IEnumerable<RestaurantMenu> AllRestaurantMenu(int RestaurantId, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantMenusRepository.Table;
            query = query.Where(r => r.RestaurantId == RestaurantId && r.IsActive == true);

            foreach (var rm in query)
            {
                var l = from mGroup in rm.RestaurantMenuGroups
                        where mGroup.IsActive == true
                        select mGroup;
                rm.RestaurantMenuGroups = l.ToList();
                foreach (var rmg in rm.RestaurantMenuGroups)
                {
                    var k = from mItem in rmg.RestaurantMenuItems
                            where mItem.IsActive == true
                            select mItem;
                    rmg.RestaurantMenuItems = k.ToList();
                }
            }

            var pages = query.OrderBy(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantMenu>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public RestaurantMenuItem GetMenuItem(int id, int? languageId)
        {
            if (languageId == null)
            {
                return _restaurantMenuItemsOperations.GetById(id);
            }
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                var menuItem = _restaurantMenuItemsOperations.GetById(id);

                if (languageId != menuItem.LanguageId && menuItem.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0)
                {
                    foreach (var menuItemTrans in menuItem.RestaurantMenuItemTranslations)
                    {
                        if (menuItemTrans.LanguageId == languageId)
                        {
                            menuItem.ItemName = menuItemTrans.TranslatedItemName;
                            menuItem.ItemDescription = menuItemTrans.TranslatedItemDescription;
                        }
                    }
                }
                return menuItem;
            }
        }

        /// <summary>
        /// Gets the restaurant menu by table identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        /// <exception cref="System.ArgumentException">
        /// INVALID_TABLE_ID
        /// or
        /// INVALID_LANGUAGE_ID
        /// </exception>
        public IEnumerable<RestaurantMenu> GetRestaurantMenuByTableId(int id, int? languageId)
        {
            var table = _restaurantTablesOperation.GetById(id);
            if (table == null)
                throw new ArgumentException("INVALID_TABLE_ID");
            else
            {
                var restaurantMenu = ActiveRestaurantTableMenu(table.Id);
                if (restaurantMenu.Any() && languageId != null)
                {
                    var language = _languageOperation.GetById(l => l.Id == languageId);

                    if (language == null)
                        throw new ArgumentException("INVALID_LANGUAGE_ID");

                    return MenuTransalation(restaurantMenu, Convert.ToInt32(languageId));
                }
                return restaurantMenu;
            }
        }

        /// <summary>
        /// Actives the restaurant table menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        private IEnumerable<RestaurantMenu> ActiveRestaurantTableMenu(int id)
        {

            var query = _restaurantTableMenuRepository.Table.Where(tm => tm.RestaurantTableId == id && tm.IsActive == true && tm.RestaurantMenu.IsActive == true).Select(r => r.RestaurantMenu);
            foreach (var rm in query)
            {
                var l = from mGroup in rm.RestaurantMenuGroups
                        where mGroup.IsActive == true
                        select mGroup;
                rm.RestaurantMenuGroups = l.ToList();

                foreach (var rmg in rm.RestaurantMenuGroups)
                {

                    var k = from mItem in rmg.RestaurantMenuItems
                            where mItem.IsActive == true
                            select mItem;
                    rmg.RestaurantMenuItems = k.ToList();
                }
            }
            return query.ToList();
        }

        /// <summary>
        /// Menus the transalation.
        /// </summary>
        /// <param name="restaurantMenu">The restaurant menu.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenu.</returns>
        private static IEnumerable<RestaurantMenu> MenuTransalation(IEnumerable<RestaurantMenu> restaurantMenu, int languageId)
        {
            foreach (var menu in restaurantMenu)
            {
                if (languageId != menu.LanguageId && (menu.RestaurantMenuTranslations.Count() != 0 && menu.RestaurantMenuTranslations.Count(l => l.LanguageId == languageId) != 0))
                {
                    foreach (var menuTrans in menu.RestaurantMenuTranslations)
                    {
                        if (menuTrans.LanguageId == languageId)
                        {
                            menu.MenuName = menuTrans.TranslatedMenuName;
                            menu.MenuDescription = menuTrans.TranslatedMenuDescription;
                        }
                    }
                }

                foreach (var MenuGroup in menu.RestaurantMenuGroups)
                {
                    if (languageId != MenuGroup.LanguageId && (MenuGroup.RestaurantMenuGroupTranslations.Count() != 0 && MenuGroup.RestaurantMenuGroupTranslations.Count(l => l.LanguageId == languageId) != 0))
                    {
                        foreach (var menuGrpTrans in MenuGroup.RestaurantMenuGroupTranslations)
                        {
                            if (menuGrpTrans.LanguageId == languageId)
                            {
                                MenuGroup.GroupName = menuGrpTrans.TranslatedGroupName;
                                MenuGroup.GroupDescription = menuGrpTrans.TranslatedGroupDescription;
                            }
                        }
                    }
                    foreach (var item in MenuGroup.RestaurantMenuItems)
                    {
                        if (languageId != item.LanguageId && (item.RestaurantMenuItemTranslations.Count() != 0 && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0))
                        {
                            foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                            {
                                if (menuItemTrans.LanguageId == languageId)
                                {
                                    item.ItemName = menuItemTrans.TranslatedItemName;
                                    item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                                }
                            }
                        }
                    }
                }
            }
            return restaurantMenu;
        }

        /// <summary>
        /// Searches the menu items.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="restaurant_Id">The restaurant identifier.</param>
        /// <param name="page">The page.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> SearchMenuItems(out int total, string searchText, int? restaurant_Id, int page = 1, int limit = 10)
        {
            var query = _restaurantMenuItemsRepository.Table;

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(m => m.ItemName.Contains(searchText) && m.IsActive == true && m.RestaurantMenuGroup.IsActive == true && m.RestaurantMenuGroup.RestaurantMenu.IsActive == true);
            }
            if (restaurant_Id.HasValue)
            {
                query = query.Where(m => m.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurant_Id && m.IsActive == true && m.RestaurantMenuGroup.IsActive == true && m.RestaurantMenuGroup.RestaurantMenu.IsActive == true);
            }
            var pages = query.OrderBy(_ => true)
                        .Skip((page - 1) * limit)
                        .Take(limit)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantMenuItem>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Creates the menu item review.
        /// </summary>
        /// <param name="menuItemReview">The menu item review.</param>
        /// <returns>MenuItemReview.</returns>
        public MenuItemReview CreateMenuItemReview(MenuItemReview menuItemReview)
        {
            menuItemReview.CreationDate = DateTime.Now;
            return _menuItemReviewOperation.AddNew(menuItemReview);
        }

        /// <summary>
        /// Gets the menu item reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReview.</returns>
        public IEnumerable<MenuItemReview> GetMenuItemReviews(int id, int pageNumber, int pageSize, out int total)
        {
            var query = _menuItemReviewRepository.Table;
            query = query.Where(m => m.RestaurantMenuItemId == id);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<MenuItemReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Creates the restaurant menu group.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        public RestaurantMenuGroup CreateRestaurantMenuGroup(RestaurantMenuGroup group)
        {
            group.CreationDate = DateTime.UtcNow;
            return _restaurantMenuGroupRepository.InsertWithChild(group);
        }

        /// <summary>
        /// Gets the restaurant menu group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        public RestaurantMenuGroup GetRestaurantMenuGroupById(int id)
        {
            return _restaurantMenuGroupOperation.GetById(r => r.Id == id);
        }

        /// <summary>
        /// Modifies the restaurant menu group.
        /// </summary>
        /// <param name="menuGroup">The menu group.</param>
        /// <param name="MenuGroupTranslation">The menu group translation.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        public RestaurantMenuGroup ModifyRestaurantMenuGroup(RestaurantMenuGroup menuGroup, List<RestaurantMenuGroupTranslation> MenuGroupTranslation)
        {
            var resMenuGroup = _restaurantMenuGroupOperation.GetById(menuGroup.Id);

            resMenuGroup.GroupName = menuGroup.GroupName;
            resMenuGroup.GroupDescription = menuGroup.GroupDescription;
            resMenuGroup.ServeLevel = menuGroup.ServeLevel;
            resMenuGroup.IsActive = menuGroup.IsActive;
            resMenuGroup.LanguageId = menuGroup.LanguageId;
            _restaurantMenuGroupOperation.Update(resMenuGroup);

            if (MenuGroupTranslation.Count() > 0)
            {
                foreach (var eachTranslation in MenuGroupTranslation)
                {
                    if (eachTranslation.Id == 0)
                    {
                        _restaurantMenuGroupTranslationOperations.AddNew(eachTranslation);
                    }
                    else
                    {
                        _restaurantMenuGroupTranslationOperations.Update(eachTranslation);
                    }
                }
            }

            return menuGroup;
        }
        /// <summary>
        /// Deletes the restaurant menu group translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        public void DeleteRestaurantMenuGroupTranslation(int translationId)
        {
            _restaurantMenuGroupTranslationOperations.Delete(translationId);
        }

        /// <summary>
        /// Gets the restaurant menu item reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReview.</returns>
        public IEnumerable<MenuItemReview> GetRestaurantMenuItemReviews(string From, string To, int restaurantId, int skip, int pageSize, out int total)
        {
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                return _menuItemReviewOperation.GetAllWithServerSidePaggingDesc(m => m.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId
                                                                                                 && m.CreationDate.Date >= from_date.Date
                                                                                                 && m.CreationDate.Date <= to_date.Date
                                                                                                 , o => o.Id, skip, pageSize, out total);
            }
            else
            {
                return _menuItemReviewOperation.GetAllWithServerSidePaggingDesc(m => m.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId, o => o.Id, skip, pageSize, out total);

            }
        }


        /// <summary>
        /// Creates the restaurant menu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">Menu item with this name already exists</exception>
        public RestaurantMenuItem CreateRestaurantMenuItem(RestaurantMenuItem item, int restaurantId)
        {
            var existing = _restaurantMenuItemsOperations.GetAll(a => a.ItemName.ToLower() == item.ItemName.ToLower() && a.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Menu item with this name already exists");

            item.CreationDate = DateTime.UtcNow;
            return _restaurantMenuItemsRepository.InsertWithChild(item);
        }

        /// <summary>
        /// Updates the menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuGroup.</returns>
        public RestaurantMenuGroup UpdateMenuGroup(int id, int restaurantId)
        {
            var menuGroup = _restaurantMenuGroupOperation.GetAll(m => m.Id == id && m.RestaurantMenu.RestaurantId == restaurantId).FirstOrDefault();
            if (menuGroup.IsActive == true)
                menuGroup.IsActive = false;
            else
                menuGroup.IsActive = true;

            _restaurantMenuGroupOperation.Update(menuGroup);
            return menuGroup;
        }

        /// <summary>
        /// Updates the menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        public RestaurantMenuItem UpdateMenuItem(int id, int restaurantId)
        {
            var menuItem = _restaurantMenuItemsOperations.GetAll(m => m.Id == id && m.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId).FirstOrDefault();

            if (menuItem.IsActive == true)
                menuItem.IsActive = false;
            else
                menuItem.IsActive = true;

            _restaurantMenuItemsOperations.Update(menuItem);
            return menuItem;

        }

        /// <summary>
        /// Updates the menu itemfor sold out.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        public void UpdateMenuItemforSoldOut(int itemId, int restaurantId)
        {
            var menuItem = _restaurantMenuItemsOperations.GetAll(m => m.Id == itemId && m.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId).FirstOrDefault();
            if (menuItem.SoldOut == true)
                menuItem.SoldOut = false;
            else
                menuItem.SoldOut = true;
            _restaurantMenuItemsOperations.Update(menuItem);
        }

        /// <summary>
        /// Modifies the menu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="ItemTranslation">The item translation.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">Menu item with this name already exists</exception>
        public RestaurantMenuItem ModifyMenuItem(RestaurantMenuItem item, List<RestaurantMenuItemTranslation> ItemTranslation, int restaurantId)
        {
            var existing = _restaurantMenuItemsOperations.GetAll(a => a.ItemName.ToLower() == item.ItemName.ToLower() && a.Id != item.Id && a.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Menu item with this name already exists");
            _restaurantMenuItemsOperations.Update(item.Id, item);

            if (ItemTranslation.Count() > 0)
            {
                foreach (var eachTranslation in ItemTranslation)
                {
                    if (eachTranslation.Id == 0)
                    {
                        _restaurantMenuItemTranslationOperations.AddNew(eachTranslation);
                    }
                    else
                    {
                        _restaurantMenuItemTranslationOperations.Update(eachTranslation);
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// Deletes the restaurant menu item translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        public void DeleteRestaurantMenuItemTranslation(int translationId)
        {
            _restaurantMenuItemTranslationOperations.Delete(translationId);
        }

        /// <summary>
        /// Deletes the restaurant menu group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        public void DeleteRestaurantMenuGroup(int id, int restaurantId)
        {
            var menuGroup = _restaurantMenuGroupOperation.GetAll(m => m.Id == id && m.RestaurantMenu.RestaurantId == restaurantId).FirstOrDefault();
            _restaurantMenuGroupOperation.Delete(menuGroup.Id);
        }

        /// <summary>
        /// Deletes the restaurant menu item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        public void DeleteRestaurantMenuItem(int id, int restaurantId)
        {
            var menuItem = _restaurantMenuItemsOperations.GetAll(m => m.Id == id && m.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId).FirstOrDefault();
            _restaurantMenuItemsOperations.Delete(menuItem.Id);

        }

        /// <summary>
        /// Creates the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <returns>RestaurantMenu.</returns>
        public RestaurantMenu CreateRestaurantMenu(RestaurantMenu menu)
        {
            menu.CreationDate = DateTime.UtcNow;
            return _restaurantMenusRepository.InsertWithChild(menu);
        }

        /// <summary>
        /// Gets the restaurant menu by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenu.</returns>
        public RestaurantMenu GetRestaurantMenuById(int id)
        {
            return _restaurantMenusOperations.GetById(r => r.Id == id);
        }

        /// <summary>
        /// Gets the restaurant menu by identifier language wise.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>RestaurantMenu.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public RestaurantMenu GetRestaurantMenuByIdLanguageWise(int id, int? languageId)
        {
            if (languageId == null)
            {
                return AllRestaurantMenu(id);
            }
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                var restaurantMenu = AllRestaurantMenu(id);
                if (languageId != restaurantMenu.LanguageId && (restaurantMenu.RestaurantMenuTranslations.Count() != 0 && restaurantMenu.RestaurantMenuTranslations.Count(l => l.LanguageId == languageId) != 0))
                {
                    foreach (var menuTrans in restaurantMenu.RestaurantMenuTranslations)
                    {
                        if (menuTrans.LanguageId == languageId)
                        {
                            restaurantMenu.MenuName = menuTrans.TranslatedMenuName;
                            restaurantMenu.MenuDescription = menuTrans.TranslatedMenuDescription;
                        }
                    }
                }

                foreach (var MenuGroup in restaurantMenu.RestaurantMenuGroups)
                {
                    if (languageId != MenuGroup.LanguageId && (MenuGroup.RestaurantMenuGroupTranslations.Count() != 0 && MenuGroup.RestaurantMenuGroupTranslations.Count(l => l.LanguageId == languageId) != 0))
                    {
                        foreach (var menuGrpTrans in MenuGroup.RestaurantMenuGroupTranslations)
                        {
                            if (menuGrpTrans.LanguageId == languageId)
                            {
                                MenuGroup.GroupName = menuGrpTrans.TranslatedGroupName;
                                MenuGroup.GroupDescription = menuGrpTrans.TranslatedGroupDescription;
                            }
                        }
                    }
                    foreach (var item in MenuGroup.RestaurantMenuItems)
                    {
                        if (languageId != item.LanguageId && (item.RestaurantMenuItemTranslations.Count() != 0 && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0))
                        {
                            foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                            {
                                if (menuItemTrans.LanguageId == languageId)
                                {
                                    item.ItemName = menuItemTrans.TranslatedItemName;
                                    item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                                }
                            }
                        }
                    }
                }
                return restaurantMenu;
            }
        }

        /// <summary>
        /// Alls the restaurant menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenu.</returns>
        /// <exception cref="System.ArgumentException">INVALID_MENU_ID</exception>
        private RestaurantMenu AllRestaurantMenu(int id)
        {
            var query = _restaurantMenusRepository.Table.Where(rm => rm.Id == id && rm.IsActive == true).FirstOrDefault();
            if (query != null)
            {
                var l = from mGroup in query.RestaurantMenuGroups
                        where mGroup.IsActive == true
                        select mGroup;
                query.RestaurantMenuGroups = l.ToList();

                foreach (var rmg in query.RestaurantMenuGroups)
                {
                    var k = from mItem in rmg.RestaurantMenuItems
                            where mItem.IsActive == true
                            select mItem;
                    rmg.RestaurantMenuItems = k.ToList();
                }
                return query;
            }
            else
            {
                throw new ArgumentException("INVALID_MENU_ID");
            }
        }
        /// <summary>
        /// Modifies the restaurant menu.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <param name="MenuTranslation">The menu translation.</param>
        /// <returns>RestaurantMenu.</returns>
        public RestaurantMenu ModifyRestaurantMenu(RestaurantMenu menu, List<RestaurantMenuTranslation> MenuTranslation)
        {
            var resMenu = _restaurantMenusOperations.GetById(menu.Id);
            resMenu.LanguageId = menu.LanguageId;
            resMenu.MenuName = menu.MenuName;
            resMenu.MenuDescription = menu.MenuDescription;
            resMenu.IsActive = menu.IsActive;
            resMenu.Image = menu.Image != null ? menu.Image : resMenu.Image;
            _restaurantMenusOperations.Update(resMenu);

            if (MenuTranslation.Count() > 0)
            {
                foreach (var eachTranslation in MenuTranslation)
                {
                    if (eachTranslation.Id == 0)
                    {
                        _restaurantMenuTranslationOperations.AddNew(eachTranslation);
                    }
                    else
                    {
                        _restaurantMenuTranslationOperations.Update(eachTranslation);
                    }
                }
            }

            return menu;
        }
        /// <summary>
        /// Deletes the restaurant menu translation.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        public void DeleteRestaurantMenuTranslation(int translationId)
        {
            _restaurantMenuTranslationOperations.Delete(translationId);
        }
        /// <summary>
        /// Deletes the restaurant menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        public void DeleteRestaurantMenu(int id, int restaurantId)
        {
            var menu = _restaurantMenusOperations.GetAll(m => m.Id == id && m.RestaurantId == restaurantId).FirstOrDefault();
            _restaurantMenusOperations.Delete(menu.Id);
        }

        /// <summary>
        /// Gets the menu item by group identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public IEnumerable<RestaurantMenuItem> GetMenuItemByGroupId(int id, Func<RestaurantMenuItem, Object> orderByPredicate, int pageNumber, int pageSize, out int total, int? languageId)
        {
            if (languageId == null)
            {
                return _restaurantMenuItemsOperations.GetAll(r => r.RestaurantMenuGroupId == id && r.IsActive == true, orderByPredicate, pageNumber, pageSize, out total);
            }
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                var menuItems = _restaurantMenuItemsOperations.GetAll(r => r.RestaurantMenuGroupId == id && r.IsActive == true, orderByPredicate, pageNumber, pageSize, out total);

                if (menuItems.Any())
                {
                    var menuGroup = menuItems.Select(i => i.RestaurantMenuGroup).Distinct().FirstOrDefault();

                    if (languageId != menuGroup.LanguageId && (menuGroup.RestaurantMenuGroupTranslations.Count() != 0 && menuGroup.RestaurantMenuGroupTranslations.Count(l => l.LanguageId == languageId) != 0))
                    {
                        foreach (var menuGrpTrans in menuGroup.RestaurantMenuGroupTranslations)
                        {
                            if (menuGrpTrans.LanguageId == languageId)
                            {
                                menuGroup.GroupName = menuGrpTrans.TranslatedGroupName;
                                menuGroup.GroupDescription = menuGrpTrans.TranslatedGroupDescription;
                            }
                        }
                    }

                    foreach (var item in menuItems)
                    {
                        if (languageId != item.LanguageId && (item.RestaurantMenuItemTranslations.Count() != 0 && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0))
                        {
                            foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                            {
                                if (menuItemTrans.LanguageId == languageId)
                                {
                                    item.ItemName = menuItemTrans.TranslatedItemName;
                                    item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                                }
                            }
                        }
                    }
                    return menuItems;
                }
                return menuItems;
            }

        }

        /// <summary>
        /// Gets the menu items by group identifier and search text.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public IEnumerable<RestaurantMenuItem> GetMenuItemsByGroupIdAndSearchText(int id, int customerId, string searchText, int pageNumber, int pageSize, out int total, int? languageId, bool considerMyPreferences = false)
        {
            var query = _restaurantMenuItemsRepository.Table;
            query = query.Where(m => m.RestaurantMenuGroupId == id && m.IsActive == true);
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(i => i.ItemName.Contains(searchText));
            }
            if (considerMyPreferences)
            {
                // menu items which serve my preferred items
                // get customer preferred ingredients, match that with restaurants menus items
                System.Data.SqlClient.SqlParameter customerParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
                var preferredMenuItems = _restaurantMenuItemsRepository.ExecuteStoredProcedureList<RestaurantMenuItem>("exec uspGetCustomerPreferredMenuItems", customerParameter);

                var preferredMenuItemsIds = preferredMenuItems.Where(i => i.RestaurantMenuGroupId == id).Select(i => i.Id).Distinct().ToList();
                if (preferredMenuItemsIds.Count > 0)
                    query = query.Where(i => preferredMenuItemsIds.Contains(i.Id));
                else
                {
                    total = 0;
                    return new List<RestaurantMenuItem>();
                }
            }

            var pages = query.OrderBy(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantMenuItem>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);

            if (entities.Count() >= 1 && languageId != null)
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                foreach (var item in entities)
                {
                    if (languageId != item.LanguageId && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0)
                    {
                        foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                        {
                            if (menuItemTrans.LanguageId == languageId)
                            {
                                item.ItemName = menuItemTrans.TranslatedItemName;
                                item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                            }
                        }
                    }
                }
            }

            return entities.ToList();
        }

        /// <summary>
        /// Gets the menu item suggestions.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetMenuItemSuggestions(int ingredientId)
        {
            return _menuItemIngredientOperations.GetAll(i => i.IngredientId == ingredientId).Select(m => m.RestaurantMenuItem).ToList();
        }

        /// <summary>
        /// Gets the menu item suggestions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public IEnumerable<RestaurantMenuItem> GetMenuItemSuggestions(int menuItemId, int pageNumber, int pageSize, out int total, int? languageId)
        {
            //var query = _menuItemIngredientRepository.Table;
            //var result = query.Where(r => r.RestaurantMenuItemId == menuItemId).Select(i => i.IngredientId).Distinct().ToList();
            //int restaurantId = _restaurantMenuItemsRepository.Table.Where(i => i.Id == menuItemId).Select(i => i.RestaurantMenuGroup.RestaurantMenu.RestaurantId).FirstOrDefault();

            if (languageId == null)
            {
                // var menuItems = _restaurantMenuItemsOperations.GetAll(i => i.Id != menuItemId && i.IsActive.HasValue && i.IsActive.Value == true && i.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId && i.MenuItemIngredients.Any(m => result.Contains(m.IngredientId)), "Asc", pageNumber, pageSize, out total).ToList();
                var menuItems = _menuItemSuggestionOperations.GetAll(i => i.RestaurantMenuItemId == menuItemId && i.RestaurantMenuItem.IsActive == true, i => i.Id, pageNumber, pageSize, out total).Select(i => i.RestaurantMenuItem);
                return menuItems;
            }
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");

                var menuItems = _menuItemSuggestionOperations.GetAll(i => i.RestaurantMenuItemId == menuItemId && i.RestaurantMenuItem.IsActive == true, i => i.Id, pageNumber, pageSize, out total).Select(i => i.RestaurantMenuItem);//&& i.MenuItemIngredients.Any(m => result.Contains(m.IngredientId))
                if (menuItems.Any())
                {
                    foreach (var item in menuItems)
                    {
                        if (languageId != item.LanguageId && (item.RestaurantMenuItemTranslations.Count() != 0 && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0))
                        {
                            foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                            {
                                if (menuItemTrans.LanguageId == languageId)
                                {
                                    item.ItemName = menuItemTrans.TranslatedItemName;
                                    item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                                }
                            }
                        }
                    }
                }
                return menuItems;
            }
        }


        /// <summary>
        /// Gets the menu groups by menu identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="considerMyPreferences">if set to <c>true</c> [consider my preferences].</param>
        /// <returns>IEnumerable RestaurantMenuGroupsOfMenu.</returns>
        /// <exception cref="System.ArgumentException">INVALID_LANGUAGE_ID</exception>
        public IEnumerable<RestaurantMenuGroupsOfMenu> GetMenuGroupsByMenuId(int id, int customerId, int pageNumber, int pageSize, out int total, int? languageId, bool considerMyPreferences = false)
        {
            IEnumerable<RestaurantMenuGroup> restMenuGroups;
            if (considerMyPreferences)
            {
                var preferredMenuGroupIds = GetCustomerPreferredMenuGroups(customerId, id).Select(pmg => pmg.Id).ToList();

                if (preferredMenuGroupIds.Count > 0)
                    restMenuGroups = AllRestaurantMenuGroups(id, preferredMenuGroupIds.ToArray(), pageNumber, pageSize, out total);
                else
                {
                    total = 0;
                    return new List<RestaurantMenuGroupsOfMenu>();
                }
            }
            else
            {
                int[] prefered = { 0 };
                restMenuGroups = AllRestaurantMenuGroups(id, prefered, pageNumber, pageSize, out  total);
            }
            if (languageId == null)
                return restMenuGroups.Select(mg => new RestaurantMenuGroupsOfMenu { Id = mg.Id, GroupName = mg.GroupName, GroupDescription = mg.GroupDescription, RestaurantMenuItems = mg.RestaurantMenuItems.Take(5).ToList() });
            else
            {
                var language = _languageOperation.GetById(l => l.Id == languageId);

                if (language == null)
                    throw new ArgumentException("INVALID_LANGUAGE_ID");
                foreach (var MenuGroup in restMenuGroups)
                {
                    if (languageId != MenuGroup.LanguageId && (MenuGroup.RestaurantMenuGroupTranslations.Count() != 0 && MenuGroup.RestaurantMenuGroupTranslations.Count(l => l.LanguageId == languageId) != 0))
                    {
                        foreach (var menuGrpTrans in MenuGroup.RestaurantMenuGroupTranslations)
                        {
                            if (menuGrpTrans.LanguageId == languageId)
                            {
                                MenuGroup.GroupName = menuGrpTrans.TranslatedGroupName;
                                MenuGroup.GroupDescription = menuGrpTrans.TranslatedGroupDescription;
                            }
                        }
                    }
                    foreach (var item in MenuGroup.RestaurantMenuItems)
                    {
                        if (languageId != item.LanguageId && (item.RestaurantMenuItemTranslations.Count() != 0 && item.RestaurantMenuItemTranslations.Count(l => l.LanguageId == languageId) != 0))
                        {
                            foreach (var menuItemTrans in item.RestaurantMenuItemTranslations)
                            {
                                if (menuItemTrans.LanguageId == languageId)
                                {
                                    item.ItemName = menuItemTrans.TranslatedItemName;
                                    item.ItemDescription = menuItemTrans.TranslatedItemDescription;
                                }
                            }
                        }
                    }
                }
                return restMenuGroups.Select(mg => new RestaurantMenuGroupsOfMenu { Id = mg.Id, GroupName = mg.GroupName, GroupDescription = mg.GroupDescription, RestaurantMenuItems = mg.RestaurantMenuItems.Take(5).ToList() });
            }
        }
        /// <summary>
        /// Gets the customer preferred menu groups.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selectedMenuId">The selected menu identifier.</param>
        /// <returns>IEnumerable RestaurantMenuGroup.</returns>
        public IEnumerable<RestaurantMenuGroup> GetCustomerPreferredMenuGroups(int customerId, int selectedMenuId)
        {
            // menu groups which serve my preferred items
            // get customer preferred ingredients, match that with restaurants menus and menu groups
            System.Data.SqlClient.SqlParameter customerParameter = new System.Data.SqlClient.SqlParameter("customerId", Convert.ToInt32(customerId));
            System.Data.SqlClient.SqlParameter menuId = new System.Data.SqlClient.SqlParameter("restMenuId", Convert.ToInt32(selectedMenuId));
            var preferredMenuGroups = _restaurantDetailsRepository.ExecuteStoredProcedureList<RestaurantMenuGroup>("exec CustomerPreferredMenuGroups", customerParameter, menuId);
            return preferredMenuGroups;
        }

        /// <summary>
        /// Alls the restaurant menu groups.
        /// </summary>
        /// <param name="RestaurantMenuId">The restaurant menu identifier.</param>
        /// <param name="preferredMenuGroups">The preferred menu groups.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenuGroup.</returns>
        private IEnumerable<RestaurantMenuGroup> AllRestaurantMenuGroups(int RestaurantMenuId, int[] preferredMenuGroups, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantMenuGroupRepository.Table;
            if (preferredMenuGroups.Count() >= 1 && !preferredMenuGroups.Contains(0))
                query = query.Where(rm => rm.RestaurantMenuId == RestaurantMenuId && rm.IsActive == true && preferredMenuGroups.Contains(rm.Id));
            else
                query = query.Where(rm => rm.RestaurantMenuId == RestaurantMenuId && rm.IsActive == true);

            foreach (var rm in query)
                foreach (var rmg in query)
                {
                    var l = from mItem in rmg.RestaurantMenuItems
                            where mItem.IsActive == true
                            select mItem;
                    rmg.RestaurantMenuItems = l.ToList();
                }

            var pages = query.OrderBy(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantMenuGroup>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        #endregion

        #region restaurant reviews

        /// <summary>
        /// Gets the review by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantReview.</returns>
        public RestaurantReview GetReviewById(int id)
        {
            return _restaurantReviewOperations.GetById(id);
        }

        /// <summary>
        /// Gets the restaurant reviews.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetRestaurantReviews(int RestaurantId, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantReviewRepository.Table;
            query = query.Where(r => r.RestaurantId == RestaurantId);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Creates the restaurant review.
        /// </summary>
        /// <param name="review">The review.</param>
        /// <returns>RestaurantReview.</returns>
        public RestaurantReview CreateRestaurantReview(RestaurantReview review)
        {
            review.CreationDate = DateTime.UtcNow;
            return _restaurantReviewOperations.AddNew(review);
        }

        /// <summary>
        /// Deletes the restaurant review.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRestaurantReview(int id)
        {
            _restaurantReviewOperations.Delete(id);
        }
        /// <summary>
        /// Gets the customer reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetCustomerReviews(int id)
        {
            return _restaurantReviewOperations.GetAll(r => r.CustomerId == id);
        }
        /// <summary>
        /// Gets all restaurants reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetAllRestaurantsReviews(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId).RoleId;
            if (customerRole == 1)
            {
                return _restaurantReviewOperations.GetAll();
            }
            return _restaurantReviewOperations.GetAll();
            //else
            //{
            //    var chainID = _chainTableOperation.GetById(c => c.CustomerId == customerId).Id;
            //    return _chainTableDetailsOperations.GetAll(cd => cd.ChainId == chainID && cd.RestaurantId != null).SelectMany(r => r.Restaurant.RestaurantReviews).ToList();
            //}

        }
        /// <summary>
        /// Gets all reviews of authentication restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetAllReviewsOfAuthRestaurants(int customerId, int pageNumber, int pageSize, int? restId, out int total)
        {
            var restaurants = _customerRoleOperations.GetAll(c => c.CustomerId == customerId).Select(r => r.RestaurantId).ToList();
            var query = _restaurantReviewRepository.Table;

            if (restId != null)
            {
                query = query.Where(o => o.RestaurantId == restId);

            }
            else
            {
                query = query.Where(o => restaurants.Contains(o.RestaurantId));
            }

            var pages = query.OrderByDescending(o => o.Id)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .GroupBy(p => new { Total = query.Count() })
             .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the restaurant all reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetRestaurantAllReviews(string From, string To, int? restaurantId, int? customerId, int skip, int pageSize, out int total)
        {
            var query = _restaurantReviewRepository.Table;
            if (restaurantId != null)
                query = query.Where(h => h.RestaurantId == restaurantId);
            if (customerId != null)
                query = query.Where(c => c.CustomerId == customerId);
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            var pages = query.OrderByDescending(o => o.Id)
            .Skip(skip)
            .Take(pageSize)
            .GroupBy(p => new { Total = query.Count() })
            .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantReview>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        #endregion

        #region Table Reservation

        /// <summary>
        /// Reserves the table.
        /// </summary>
        /// <param name="TotalSeats">The total seats.</param>
        /// <param name="Reservation_DateTime">The reservation date time.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<RestaurantTable> ReserveTable(int TotalSeats, DateTime Reservation_DateTime)
        {
            throw new NotImplementedException();
            //return _restaurantTablesOperation.GetAll(r => r.ActiveStatus == true &&
            //                                               r.TotalSeats >= TotalSeats &&
            //                                               r.Customer_Reserved_Table.All(c => c.Table_ID == r.Table_ID &&
            //                                                                                  (c.Customer_Table_Reservations.Reservation_DateTime >= Reservation_DateTime.AddHours(2) ||
            //                                                                                  c.Customer_Table_Reservations.Reservation_DateTime.AddHours(2) <= Reservation_DateTime)
            //                                                                             )
            //                                          ).Take(1);
        }

        //public Customer_Table_Reservations CreateReservation(Customer_Table_Reservations reservations, List<RestaurantTable> restaurantTables)
        //{
        //    if (restaurantTables.Count() != 0 && reservations.No_Of_Seats <= restaurantTables.FirstOrDefault().TotalSeats)
        //    {
        //        var reservation = _customerTableReservationsOperation.AddNew(reservations);

        //        Customer_Reserved_Table customerReservedTable = new Customer_Reserved_Table
        //        {
        //            Customer_Table_Reservation_ID = reservation.Customer_Table_Reservation_ID,
        //            Table_ID = restaurantTables.First().Id,
        //            Seat_Reserved = reservation.No_Of_Seats
        //        };

        //        _customerReservedTableOperation.AddNew(customerReservedTable);

        //        return reservation;
        //    }
        //    else
        //    {
        //        return reservations;
        //    }


        //}

        //public Customer_Table_Reservations GetCustomerReservedTableById(int id)
        //{
        //    return _customerTableReservationsOperation.GetById(id);
        //}



        #endregion

        #region Measurement Unit
        /// <summary>
        /// Gets all measurement units.
        /// </summary>
        /// <returns>IEnumerable MeasurementUnit.</returns>
        public IEnumerable<MeasurementUnit> GetAllMeasurementUnits()
        {
            return _measurementUnitOperations.GetAll();
        }
        /// <summary>
        /// Gets the measurement units.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MeasurementUnit.</returns>
        public IEnumerable<MeasurementUnit> GetMeasurementUnits(int skip, int pageSize, out int total)
        {
            return _measurementUnitOperations.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Creates the measurement units.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>MeasurementUnit.</returns>
        public MeasurementUnit CreateMeasurementUnits(MeasurementUnit measurementUnit)
        {
            measurementUnit.CreationDate = DateTime.UtcNow;
            return _measurementUnitOperations.AddNew(measurementUnit);
        }
        /// <summary>
        /// Deletes the measurement unit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteMeasurementUnit(int id)
        {
            _measurementUnitOperations.Delete(id);
        }
        #endregion Measurement Unit

        #region cuisines

        /// <summary>
        /// Gets all cuisines.
        /// </summary>
        /// <returns>IEnumerable Cuisine.</returns>
        public IEnumerable<Cuisine> GetAllCuisines()
        {
            return _cuisinesOperations.GetAll();
        }

        /// <summary>
        /// Gets all rest unassigned cuisines.
        /// </summary>
        /// <param name="restId">The rest identifier.</param>
        /// <returns>IEnumerable Cuisine.</returns>
        public IEnumerable<Cuisine> GetAllRestUnassignedCuisines(int restId)
        {
            var assignedCuisines = _restaurantCuisinesOperations.GetAll(c => c.RestaurantId == restId).Select(r => r.CuisineId).ToList();
            var unassignedRestCuisines = _cuisinesOperations.GetAll(c => c.IsActive && !assignedCuisines.Contains(c.Id));
            return unassignedRestCuisines;
        }

        /// <summary>
        /// Gets all cuisines.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Cuisine.</returns>
        public IEnumerable<Cuisine> GetAllCuisines(int skip, int pageSize, out int total)
        {
            return _cuisinesOperations.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the cuisine by identifier.
        /// </summary>
        /// <param name="cuisineId">The cuisine identifier.</param>
        /// <returns>Cuisine.</returns>
        public Cuisine GetCuisineById(int cuisineId)
        {
            return _cuisinesOperations.GetById(cuisineId);
        }

        /// <summary>
        /// Creates the cuisine.
        /// </summary>
        /// <param name="cuisine">The cuisine.</param>
        /// <returns>Cuisine.</returns>
        public Cuisine CreateCuisine(Cuisine cuisine)
        {
            cuisine.CreationDate = DateTime.UtcNow;
            return _cuisinesOperations.AddNew(cuisine);
        }
        /// <summary>
        /// Modifies the cuisine.
        /// </summary>
        /// <param name="cuisine">The cuisine.</param>
        /// <returns>Cuisine.</returns>
        public Cuisine ModifyCuisine(Cuisine cuisine)
        {
            _cuisinesOperations.Update(cuisine.Id, cuisine);
            return cuisine;
        }
        /// <summary>
        /// Deletes the cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCuisine(int id)
        {
            _cuisinesOperations.Delete(id);
        }

        /// <summary>
        /// Gets the restaurant cuisines.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantCuisine.</returns>
        public IEnumerable<RestaurantCuisine> GetRestaurantCuisines(int id)
        {
            return _restaurantCuisinesOperations.GetAll(r => r.RestaurantId == id);
        }

        /// <summary>
        /// Gets all restaurant cuisines.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantCuisine.</returns>
        public IEnumerable<RestaurantCuisine> GetAllRestaurantCuisines(int restaurantId, int skip, int pageSize, out int total)
        {
            return _restaurantCuisinesOperations.GetAllWithServerSidePagging(r => r.RestaurantId == restaurantId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the restaurant cuisine by identifier.
        /// </summary>
        /// <param name="restCuisineId">The rest cuisine identifier.</param>
        /// <returns>RestaurantCuisine.</returns>
        public RestaurantCuisine GetRestaurantCuisineById(int restCuisineId)
        {
            return _restaurantCuisinesOperations.GetById(restCuisineId);
        }

        /// <summary>
        /// Creates the restaurant cuisine.
        /// </summary>
        /// <param name="restCuisine">The rest cuisine.</param>
        /// <returns>RestaurantCuisine.</returns>
        public RestaurantCuisine CreateRestaurantCuisine(RestaurantCuisine restCuisine)
        {
            restCuisine.CreationDate = DateTime.UtcNow;
            return _restaurantCuisinesOperations.AddNew(restCuisine);
        }

        /// <summary>
        /// Modifies the rest cuisine.
        /// </summary>
        /// <param name="restCuisine">The rest cuisine.</param>
        /// <returns>RestaurantCuisine.</returns>
        public RestaurantCuisine ModifyRestCuisine(RestaurantCuisine restCuisine)
        {
            _restaurantCuisinesOperations.Update(restCuisine.Id, restCuisine);
            return restCuisine;
        }

        /// <summary>
        /// Deletes the restaurant cuisine.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRestaurantCuisine(int id)
        {
            _restaurantCuisinesOperations.Delete(id);
        }
        #endregion cuisines

        #region Restaurant Cart
        /// <summary>
        /// Gets the customer cart.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>RestaurantCart.</returns>
        public RestaurantCart GetCustomerCart(int Id, int restaurantId)
        {
            var cart = _restaurantCartOperations.GetAll(c => c.CustomerId == Id && c.RestaurantId == restaurantId).FirstOrDefault();
            return cart;
        }


        /// <summary>
        /// Creates the restaurant cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <returns>RestaurantCart.</returns>
        /// <exception cref="CartExistsException">CART_ALREADY_EXISTS</exception>
        public RestaurantCart CreateRestaurantCart(RestaurantCart cart, OrderType orderType, int? table_room_Id, bool flush = false)
        {
            var exitingCart = _restaurantCartOperations.GetAll(c => c.CustomerId == cart.CustomerId && c.RestaurantId == cart.RestaurantId).FirstOrDefault();

            if (exitingCart != null)
            {
                if (flush)
                    _restaurantCartRepository.Delete(exitingCart.Id);
                else
                    throw new CartExistsException("CART_ALREADY_EXISTS");
            }
            if (table_room_Id != null)
            {
                if (OrderType.Table == orderType)
                {
                    var tableDetails = _restaurantTablesOperation.GetById(Convert.ToInt32(table_room_Id));
                    if (tableDetails == null || tableDetails.RestaurantTableGroup.RestaurantId != cart.RestaurantId)
                        throw new ArgumentException("INVALID_TABLE_Id");
                }
                if (OrderType.Room == orderType)
                {
                    var room = _roomRepository.Table.Where(r => r.Id == table_room_Id).FirstOrDefault();
                    if (room == null)
                        throw new ArgumentException("INVALID_ROOM_ID");
                    var customerRoom = _customerHotelRoomRepository.Table.Where(cr => cr.CustomerId == cart.CustomerId && cr.RoomId == table_room_Id && cr.IsActive ).FirstOrDefault();
                    if(customerRoom == null)
                        throw new ArgumentException("HOTEL_ROOM_NOT_ACTIVE_FOR_THIS_CUSTOMER_ID");
                }

            }
            // verify additionals are correctly specified for each item
            if (cart.RestaurantCartItems != null && cart.RestaurantCartItems.Any())
            {
                double result = 0.00f;
                foreach (var item in cart.RestaurantCartItems)
                {
                    var menu_item_id = item.RestaurantMenuItemId;
                    var menu_item = _restaurantMenuItemsOperations.GetAll(i => i.Id == menu_item_id).FirstOrDefault();
                    if (item.ServeLevel == 0)
                        item.ServeLevel = menu_item.RestaurantMenuGroup.ServeLevel;

                    double? offerPrice = 0;
                    if (menu_item.MenuItemOffers != null && menu_item.MenuItemOffers.Count() >= 1)
                    {
                        double? percentage = menu_item.MenuItemOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                        if (percentage > 0 && percentage != null)
                            offerPrice = ((menu_item.Price * percentage) / 100) * item.Qty;
                    }

                    item.Total = item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;

                    double additionalResult = 0.00f;
                    foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                    {
                        var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                        if (details.MenuAdditionalGroup.HasQuantity)
                            additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                        else
                            additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                        additionalResult += Convert.ToDouble(additionalItem.Total);
                    }
                    item.OfferPrice = offerPrice;
                    item.Total += additionalResult;
                    result += Convert.ToDouble(item.Total);
                    cart.RestaurantId = menu_item.RestaurantMenuGroup.RestaurantMenu.RestaurantId;
                }
                cart.Amount = result;
            }

            cart.CreationDate = DateTime.Now;

            if (OrderType.Table == orderType && table_room_Id != null)
                cart.TableId = table_room_Id;
            if (OrderType.Room == orderType && table_room_Id != null)
                cart.RoomId = table_room_Id;

            _restaurantCartRepository.InsertWithChild(cart); // unable to save child and populate navigation properties, so work around
            cart.Customer = _customerRepository.GetById(cart.CustomerId);

            foreach (var item in cart.RestaurantCartItems)
            {
                foreach (var additional in item.RestaurantCartItemAdditionals)
                {
                    additional.MenuAdditionalElement = _menuAdditionalElementRepository.GetById(additional.MenuAdditionalElementId);
                }

                item.RestaurantMenuItem = _restaurantMenuItemsRepository.Table
                    .Include(i => i.MenuItemAdditionals.Select(ad => ad.MenuAdditionalGroup))
                    .Where(i => i.Id == item.RestaurantMenuItemId)
                    .FirstOrDefault();
            }

            cart.RestaurantCartItems = cart.RestaurantCartItems.OrderBy(c => c.ServeLevel).ToList();
            return cart;
        }


        /// <summary>
        /// Updates the restaurant cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cart">The cart.</param>
        /// <returns>RestaurantCart.</returns>
        /// <exception cref="System.ArgumentException">INVALID_CUSTOMER_ID</exception>
        /// <exception cref="System.ArgumentNullException">INVALID_CART_DETAILS</exception>
        /// <exception cref="System.InvalidOperationException">CART_NOT_FOUND</exception>
        /// <exception cref="ZeroItemQtyException">MENU_ITEM_QTY_SHOULD_NOT_ZERO_NEGATIVE_OR_NULL</exception>
        /// <exception cref="NegativeItemQtyException">NEGATIVE_ITEM_QTY</exception>
        public RestaurantCart UpdateRestaurantCart(int customerId, RestaurantCart cart)
        {
            if (customerId <= 0)
                throw new ArgumentException("INVALID_CUSTOMER_ID");

            if (cart == null)
                throw new ArgumentNullException("INVALID_CART_DETAILS");

            var existingCart = _restaurantCartOperations.GetAll(ct => ct.CustomerId == customerId && ct.RestaurantId == cart.RestaurantId).FirstOrDefault();

            if (existingCart == null)
                throw new InvalidOperationException("CART_NOT_FOUND");

            foreach (var item in cart.RestaurantCartItems)
            {
                var menu_item = _restaurantMenuItemsOperations.GetById(item.RestaurantMenuItemId);

                RestaurantCartItem existItem = null;
                if (item.Id == 0)
                {
                    //existItem = existingCart.RestaurantCartItems.Where(i => i.RestaurantMenuItemId == item.RestaurantMenuItemId && i.Comment == item.Comment && i.ServeLevel == item.ServeLevel).FirstOrDefault();
                    var existItems = existingCart.RestaurantCartItems.Where(i => i.RestaurantMenuItemId == item.RestaurantMenuItemId && i.Comment == item.Comment && i.ServeLevel == item.ServeLevel).ToList();
                    foreach (var existingCartItem in existItems)
                    {
                        int IsValue = 0;
                        var countadd = item.RestaurantCartItemAdditionals.Count();
                        foreach (var itemAdditional in item.RestaurantCartItemAdditionals)
                        {
                            if (existingCartItem.RestaurantCartItemAdditionals.Any(a => a.MenuAdditionalElementId == itemAdditional.MenuAdditionalElementId))
                                IsValue++;
                        }
                        if (countadd == IsValue)
                            existItem = existingCartItem;
                    }

                }
                else
                    existItem = existingCart.RestaurantCartItems.Where(i => i.Id == item.Id && i.RestaurantMenuItemId == item.RestaurantMenuItemId).FirstOrDefault();

                if (existItem == null)
                {
                    if (item.Qty <= 0 || item.Qty == null)
                    {
                        throw new ZeroItemQtyException("MENU_ITEM_QTY_SHOULD_NOT_ZERO_NEGATIVE_OR_NULL");
                    }
                    else
                    {
                        double? offerPrice = 0;
                        if (menu_item.MenuItemOffers != null && menu_item.MenuItemOffers.Count() >= 1)
                        {
                            double? percentage = menu_item.MenuItemOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                            if (percentage > 0 && percentage != null)
                                offerPrice = ((menu_item.Price * percentage) / 100) * item.Qty;
                        }

                        item.Total = item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;

                        double additionalResult = 0.00f;
                        foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                        {
                            var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                            if (details.MenuAdditionalGroup.HasQuantity)
                                additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                            else
                                additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                            additionalResult += Convert.ToDouble(additionalItem.Total);
                        }
                        item.OfferPrice = offerPrice;
                        item.Total += additionalResult;
                        existingCart.Amount += item.Total;
                        existingCart.RestaurantCartItems.Add(item);
                    }
                }
                else
                {
                    if (item.Qty <= 0)
                    {
                        if (item.Qty == 0 && item.Id == existItem.Id)
                        {
                            if (existingCart.RestaurantCartItems.Count() == 1)
                                existingCart.Amount = 0;
                            else
                                existingCart.Amount -= existItem.Total;
                            _restaurantCartItemRepository.Delete(existItem.Id);
                        }
                        else
                        {
                            throw new NegativeItemQtyException("NEGATIVE_ITEM_QTY");
                        }
                    }
                    else
                    {
                        double? offerPrice = 0;
                        if (menu_item.MenuItemOffers != null && menu_item.MenuItemOffers.Count() >= 1)
                        {
                            double? percentage = menu_item.MenuItemOffers.Where(o => o.IsActive).Select(o => o.Percentage).FirstOrDefault();
                            if (percentage > 0 && percentage != null)
                                offerPrice = ((menu_item.Price * percentage) / 100) * item.Qty;
                        }

                        if (existItem.Id != item.Id)
                        {
                            int IsValue = 0;
                            int IsAdditional = 0;
                            foreach (var itemAdditional in item.RestaurantCartItemAdditionals)
                            {
                                if (existItem.RestaurantCartItemAdditionals.Any(a => a.MenuAdditionalElementId == itemAdditional.MenuAdditionalElementId))
                                    IsValue++;
                                else IsAdditional++;
                            }
                            if (IsAdditional == 0 && IsValue.Equals(existItem.RestaurantCartItemAdditionals.Count()) && (existItem.Comment == item.Comment) && existItem.ServeLevel == item.ServeLevel)
                            {
                                existItem.Qty += item.Qty;
                                existItem.ServeLevel = item.ServeLevel;
                                if (existItem.Comment == null)
                                    existItem.Comment = item.Comment;
                                existingCart.Amount -= existItem.Total;
                                existItem.Total += item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;
                                existItem.OfferPrice += offerPrice;

                                double additionalResult = 0.00f;
                                foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                                {
                                    var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                                    if (details.MenuAdditionalGroup.HasQuantity)
                                        additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                                    else
                                        additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                                    additionalResult += Convert.ToDouble(additionalItem.Total);

                                    foreach (var exitAdditionalItem in existItem.RestaurantCartItemAdditionals)
                                    {
                                        if (exitAdditionalItem.MenuAdditionalElementId == additionalItem.MenuAdditionalElementId)
                                        {
                                            exitAdditionalItem.Qty += additionalItem.Qty;
                                            exitAdditionalItem.Total += additionalItem.Total;
                                        }
                                    }
                                }
                                existItem.Total += additionalResult;
                                existingCart.Amount += existItem.Total;
                            }
                            else
                            {
                                item.Total = item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;

                                double additionalResult = 0.00f;
                                foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                                {
                                    var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                                    if (details.MenuAdditionalGroup.HasQuantity)
                                        additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                                    else
                                        additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                                    additionalResult += Convert.ToDouble(additionalItem.Total);
                                }

                                item.OfferPrice = offerPrice;
                                item.Total += additionalResult;
                                existingCart.Amount += item.Total;
                                existingCart.RestaurantCartItems.Add(item);
                            }
                        }
                        else
                        {
                            var existAnotherItem = existingCart.RestaurantCartItems.Where(i => i.RestaurantMenuItemId == item.RestaurantMenuItemId && i.Id != item.Id && i.ServeLevel == item.ServeLevel && i.Comment == item.Comment).FirstOrDefault();

                            if (existAnotherItem != null)
                            {
                                int IsValue = 0;
                                foreach (var itemAdditional in item.RestaurantCartItemAdditionals)
                                {
                                    if (existAnotherItem.RestaurantCartItemAdditionals.Any(a => a.MenuAdditionalElementId == itemAdditional.MenuAdditionalElementId))
                                        IsValue++;
                                }

                                if (IsValue.Equals(existAnotherItem.RestaurantCartItemAdditionals.Count()) && existItem.ServeLevel != item.ServeLevel)
                                {
                                    existAnotherItem.Qty += item.Qty;
                                    existingCart.Amount = existingCart.Amount - (existItem.Total + existAnotherItem.Total);
                                    existAnotherItem.Total += item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;
                                    existAnotherItem.OfferPrice += offerPrice;

                                    double additionalResult = 0.00f;
                                    foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                                    {
                                        var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                                        if (details.MenuAdditionalGroup.HasQuantity)
                                            additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                                        else
                                            additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                                        additionalResult += Convert.ToDouble(additionalItem.Total);

                                        foreach (var exitAdditionalItem in existAnotherItem.RestaurantCartItemAdditionals)
                                        {
                                            if (exitAdditionalItem.MenuAdditionalElementId == additionalItem.MenuAdditionalElementId)
                                            {
                                                additionalResult = additionalResult - Convert.ToDouble(exitAdditionalItem.Total);
                                                exitAdditionalItem.Qty += additionalItem.Qty;
                                                exitAdditionalItem.Total += additionalItem.Total;

                                            }
                                        }
                                    }
                                    existAnotherItem.Total += additionalResult;
                                    existingCart.Amount += existAnotherItem.Total;
                                    _restaurantCartItemRepository.Delete(existItem.Id);
                                }
                                else
                                {
                                    existItem.Qty = item.Qty;
                                    existItem.ServeLevel = item.ServeLevel;
                                    existingCart.Amount -= existItem.Total;
                                    existItem.Total = item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;
                                    existItem.OfferPrice = offerPrice;

                                    double additionalResult = 0.00f;
                                    foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                                    {
                                        var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                                        if (details.MenuAdditionalGroup.HasQuantity)
                                            additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                                        else
                                            additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                                        additionalResult += Convert.ToDouble(additionalItem.Total);

                                        foreach (var exitAdditionalItem in existItem.RestaurantCartItemAdditionals)
                                        {
                                            if (exitAdditionalItem.MenuAdditionalElementId == additionalItem.MenuAdditionalElementId)
                                            {
                                                //additionalResult = additionalResult - Convert.ToDouble(exitAdditionalItem.Total);
                                                exitAdditionalItem.Qty = additionalItem.Qty;
                                                exitAdditionalItem.Total = additionalItem.Total;
                                            }
                                        }
                                    }
                                    existItem.Total += additionalResult;
                                    existingCart.Amount += existItem.Total;

                                    var newAdd = item.RestaurantCartItemAdditionals
                                    .Where(i => !existItem.RestaurantCartItemAdditionals.Any(old => old.MenuAdditionalElementId == i.MenuAdditionalElementId));

                                    var delAdd = existItem.RestaurantCartItemAdditionals
                                        .Where(old => !item.RestaurantCartItemAdditionals.Any(nw => nw.MenuAdditionalElementId == old.MenuAdditionalElementId));

                                    foreach (var del in delAdd)
                                    {
                                        _restaurantCartItemAdditional.Delete(del.Id);
                                    }

                                    foreach (var add in newAdd)
                                    {
                                        existItem.RestaurantCartItemAdditionals.Add(new RestaurantCartItemAdditional { MenuAdditionalElementId = add.MenuAdditionalElementId, Qty = add.Qty, Total = add.Total });
                                    }
                                }
                            }
                            else
                            {
                                existItem.Qty = item.Qty;
                                existItem.ServeLevel = item.ServeLevel;
                                existingCart.Amount -= existItem.Total;
                                existItem.Total = item.Qty * (menu_item.Price == null ? 0 : menu_item.Price) - offerPrice;
                                existItem.OfferPrice = offerPrice;

                                double additionalResult = 0.00f;
                                foreach (var additionalItem in item.RestaurantCartItemAdditionals)
                                {
                                    var details = _menuAdditionalElementRepository.GetById(additionalItem.MenuAdditionalElementId);
                                    if (details.MenuAdditionalGroup.HasQuantity)
                                        additionalItem.Total = (details.AdditionalCost == null ? 0 : details.AdditionalCost) * additionalItem.Qty;
                                    else
                                        additionalItem.Total = details.AdditionalCost == null ? 0 : details.AdditionalCost * item.Qty;
                                    additionalResult += Convert.ToDouble(additionalItem.Total);

                                    foreach (var exitAdditionalItem in existItem.RestaurantCartItemAdditionals)
                                    {
                                        if (exitAdditionalItem.MenuAdditionalElementId == additionalItem.MenuAdditionalElementId)
                                        {
                                            exitAdditionalItem.Qty = additionalItem.Qty;
                                            exitAdditionalItem.Total = additionalItem.Total;
                                        }
                                    }
                                }
                                existItem.Total += additionalResult;
                                existingCart.Amount += existItem.Total;

                                var newAdd = item.RestaurantCartItemAdditionals
                                .Where(i => !existItem.RestaurantCartItemAdditionals.Any(old => old.MenuAdditionalElementId == i.MenuAdditionalElementId));

                                var delAdd = existItem.RestaurantCartItemAdditionals
                                    .Where(old => !item.RestaurantCartItemAdditionals.Any(nw => nw.MenuAdditionalElementId == old.MenuAdditionalElementId));

                                foreach (var del in delAdd)
                                {
                                    _restaurantCartItemAdditional.Delete(del.Id);
                                }

                                foreach (var add in newAdd)
                                {
                                    existItem.RestaurantCartItemAdditionals.Add(new RestaurantCartItemAdditional { MenuAdditionalElementId = add.MenuAdditionalElementId, Qty = add.Qty, Total = add.Total });
                                }
                            }

                        }
                    }
                }
            }

            _restaurantCartOperations.Update(existingCart);

            var newRepo = new EfRepository<RestaurantCart>();
            var customerCart = newRepo.GetById(existingCart.Id);

            customerCart.RestaurantCartItems = customerCart.RestaurantCartItems.OrderBy(c => c.ServeLevel).ToList();
            return customerCart;
        }

        #endregion Restaurant Cart

        #region Restaurant Order

        /// <summary>
        /// Creates the restaurant order from cart.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="tableId">The table identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>RestaurantOrder.</returns>
        /// <exception cref="System.ArgumentException">
        /// INVALID_Id
        /// or
        /// INVALID_TABLE_Id
        /// or
        /// INVALID_CARD_Id
        /// </exception>
        /// <exception cref="CartEmptyException">CART_IS_EMPTY</exception>
        /// <exception cref="InvalidScheduleDateTimeException">
        /// INVALID_SCHEDULE_DATE_TIME
        /// or
        /// INVALID_SCHEDULE_DATE_TIME
        /// or
        /// INVALID_SCHEDULE_DATE_TIME
        /// </exception>
        public RestaurantOrder CreateRestaurantOrderFromCart(int customerId, int? cardId, int restaurantId, DateTime scheduleDate)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_Id");

            var cart = _restaurantCartOperations.GetById(c => c.CustomerId == customerId && c.RestaurantId == restaurantId);

            if (cart == null || !cart.RestaurantCartItems.Any())
                throw new CartEmptyException("CART_IS_EMPTY");
            //if (table_room_Id != null)
            //{
            //    if (OrderType.Table == orderType)
            //    {
            //        var tableDetails = _restaurantTablesOperation.GetById(Convert.ToInt32(table_room_Id));
            //        if (tableDetails == null || tableDetails.RestaurantTableGroup.RestaurantId != cart.RestaurantId)
            //            throw new ArgumentException("INVALID_TABLE_Id");
            //    }
            //    if (OrderType.Room == orderType)
            //    {
            //        var room = _roomRepository.Table.Where(r => r.Id == table_room_Id).FirstOrDefault();
            //        if (room == null)
            //            throw new ArgumentException("INVALID_ROOM_ID");
            //    }

            //}
            if (scheduleDate != null)
            {
                DateTime startOfDay = new DateTime(scheduleDate.Year, scheduleDate.Month, scheduleDate.Day, 00, 00, 00);
                DateTime endOfDay = new DateTime(scheduleDate.Year, scheduleDate.Month, scheduleDate.Day, 23, 59, 59);
                //if (scheduleDate < DateTime.Now || (scheduleDate.Value.TimeOfDay > cart.Restaurant.ClosingHours))
                if (scheduleDate < DateTime.Now)
                {
                    throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
                }
                else if ((cart.Restaurant.ClosingHours >= startOfDay.TimeOfDay) && (cart.Restaurant.ClosingHours <= cart.Restaurant.OpeningHours))
                {
                    if (scheduleDate.TimeOfDay < cart.Restaurant.OpeningHours && scheduleDate.TimeOfDay > cart.Restaurant.ClosingHours)
                        throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
                }
                else if ((cart.Restaurant.ClosingHours <= endOfDay.TimeOfDay) && (cart.Restaurant.ClosingHours >= cart.Restaurant.OpeningHours))
                {
                    if ((scheduleDate.TimeOfDay > startOfDay.TimeOfDay && scheduleDate.TimeOfDay < cart.Restaurant.OpeningHours)
                        || (scheduleDate.TimeOfDay > cart.Restaurant.OpeningHours && scheduleDate.TimeOfDay > cart.Restaurant.ClosingHours))
                        throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
                }
            }

            if (cardId != null)
            {
                var card = _customerCreditCardRepository.GetById(cardId);
                if (card == null || card.CustomerId != customerId)
                {
                    throw new ArgumentException("INVALID_CARD_Id");
                }
            }

            RestaurantOrder order = new RestaurantOrder
            {
                CreationDate = DateTime.Now,
                CustomerId = customerId,
                RoomId = cart.RoomId,
                RestaurantId = cart.RestaurantId,
                CustomerTableNumber = cart.TableId,
                ScheduledDate = scheduleDate,
                CardId = cardId,
                OrderStatus = 0,
                OrderTotal = cart.Amount,
                IsActive = true
            };
            //if (OrderType.Table == orderType && table_room_Id != null)
            //    order.CustomerTableNumber = table_room_Id;
            //if (OrderType.Room == orderType && table_room_Id != null)
            //    order.RoomId = table_room_Id;


            foreach (var item in cart.RestaurantCartItems)
            {
                RestaurantOrderItem orderItem = new RestaurantOrderItem
                {
                    Qty = item.Qty,
                    RestaurantMenuItemId = item.RestaurantMenuItemId,
                    ServeLevel = item.ServeLevel,
                    OrderItemTotal = item.Total,
                    Comment = item.Comment,
                    OfferPrice = item.OfferPrice
                };

                if (item.RestaurantCartItemAdditionals != null && item.RestaurantCartItemAdditionals.Any())
                {
                    foreach (var add in item.RestaurantCartItemAdditionals)
                    {
                        RestaurantOrderItemAdditional additional = new RestaurantOrderItemAdditional()
                        {
                            MenuAdditionalElementId = add.MenuAdditionalElementId,
                            Qty = add.Qty,
                            Total = add.Total
                        };
                        orderItem.RestaurantOrderItemAdditionals.Add(additional);
                    }
                }
                order.RestaurantOrderItems.Add(orderItem);
            }
            _restaurantOrderRepository.InsertWithChild(order);
            _restaurantCartRepository.Delete(cart.Id);
            EfRepository<RestaurantOrder> newRepo = new EfRepository<RestaurantOrder>();

            var customerOrder = newRepo.GetById(order.Id);
            customerOrder.RestaurantOrderItems = customerOrder.RestaurantOrderItems.OrderBy(c => c.ServeLevel).ToList();
            return customerOrder;

        }
        /// <summary>
        /// Schedules the restaurant order.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <returns>RestaurantOrder.</returns>
        /// <exception cref="System.ArgumentException">INVALID_Id</exception>
        /// <exception cref="OrderNotFoundException">ORDER_NOT_FOUND</exception>
        /// <exception cref="InvalidScheduleDateTimeException">
        /// INVALID_SCHEDULE_DATE_TIME
        /// or
        /// INVALID_SCHEDULE_DATE_TIME
        /// or
        /// INVALID_SCHEDULE_DATE_TIME
        /// </exception>
        public RestaurantOrder ScheduleRestaurantOrder(int customerId, int orderId, DateTime scheduleDate)
        {
            if (customerId == 0)
                throw new ArgumentException("INVALID_Id");
            var restaurantOrder = _customerRestaurantOrdersOperation.GetAll(c => c.CustomerId == customerId && c.Id == orderId).FirstOrDefault();
            if (restaurantOrder == null)
            {
                throw new OrderNotFoundException("ORDER_NOT_FOUND");
            }

            DateTime startOfDay = new DateTime(scheduleDate.Year, scheduleDate.Month, scheduleDate.Day, 00, 00, 00);
            DateTime endOfDay = new DateTime(scheduleDate.Year, scheduleDate.Month, scheduleDate.Day, 23, 59, 59);
            if (scheduleDate < DateTime.Now)
            {
                throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
            }
            else if ((restaurantOrder.Restaurant.ClosingHours >= startOfDay.TimeOfDay) && (restaurantOrder.Restaurant.ClosingHours <= restaurantOrder.Restaurant.OpeningHours))
            {
                if (scheduleDate.TimeOfDay < restaurantOrder.Restaurant.OpeningHours && scheduleDate.TimeOfDay > restaurantOrder.Restaurant.ClosingHours)
                    throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
            }
            else if ((restaurantOrder.Restaurant.ClosingHours <= endOfDay.TimeOfDay) && (restaurantOrder.Restaurant.ClosingHours >= restaurantOrder.Restaurant.OpeningHours))
            {
                if ((scheduleDate.TimeOfDay > startOfDay.TimeOfDay && scheduleDate.TimeOfDay < restaurantOrder.Restaurant.OpeningHours)
                    || (scheduleDate.TimeOfDay > restaurantOrder.Restaurant.OpeningHours && scheduleDate.TimeOfDay > restaurantOrder.Restaurant.ClosingHours))
                    throw new InvalidScheduleDateTimeException("INVALID_SCHEDULE_DATE_TIME");
            }
            restaurantOrder.ScheduledDate = scheduleDate;
            _restaurantOrderRepository.Update(restaurantOrder);
            EfRepository<RestaurantOrder> newRepo = new EfRepository<RestaurantOrder>();

            var order = newRepo.GetById(restaurantOrder.Id);
            order.RestaurantOrderItems = order.RestaurantOrderItems.OrderBy(c => c.ServeLevel).ToList();
            return order;
        }

        /// <summary>
        /// Gets the restaurant order by identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder GetRestaurantOrderById(int orderId)
        {
            return _customerRestaurantOrdersOperation.GetById(orderId);
        }

        /// <summary>
        /// Gets the order next serving status changed count.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>System.Int32.</returns>
        public int GetOrderNextServingStatusChangedCount(int restaurantId)
        {
            return _customerRestaurantOrdersOperation.GetAll(o => o.RestaurantId == restaurantId && o.NextServingStatus == true).ToList().Count();
        }

        /// <summary>
        /// Updates the rest order next serving status.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        public void UpdateRestOrderNextServingStatus(int orderId)
        {
            var order = _customerRestaurantOrdersOperation.GetById(orderId);
            order.NextServingStatus = false;
            _customerRestaurantOrdersOperation.Update(order);
        }

        //Web
        /// <summary>
        /// Gets all recent orders of authentication restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetAllRecentOrdersOfAuthRestaurants(int customerId, int pageNumber, int pageSize, int? restId, out int total)
        {
            var restaurants = _customerRoleOperations.GetAll(c => c.CustomerId == customerId).Select(r => r.RestaurantId).ToList();

            var query = _restaurantOrderRepository.Table;
            var toDate = DateTime.Now;
            var fromDate = toDate.AddDays(-30);
            if (restId != null)
            {
                query = query.Where(o => o.RestaurantId == restId);
            }
            else
            {
                query = query.Where(o => restaurants.Contains(o.RestaurantId));
            }

            query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDate)
                                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(toDate));

            var pages = query.OrderByDescending(o => o.Id)
          .Skip((pageNumber - 1) * pageSize)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }


        /// <summary>
        /// Gets all restaurants orders.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetAllRestaurantsOrders(string From, string To, int RestaurantId, Double? minAmount, Double? maxAmount, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantOrderRepository.Table;
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (RestaurantId > 0)
            {
                query = query.Where(o => o.RestaurantId == RestaurantId);
            }

            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.RestaurantOrderItems.Sum(i => i.RestaurantMenuItem.Price * i.Qty) >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.RestaurantOrderItems.Sum(i => i.RestaurantMenuItem.Price * i.Qty) <= maxAmount);

            }
            var pages = query.OrderBy(_ => true)
          .Skip((pageNumber - 1) * pageSize)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the restaurant order history.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetRestaurantOrderHistory(string From, string To, int RestaurantId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total)
        {
            var query = _restaurantOrderRepository.Table;
            if (RestaurantId > 0)
            {
                query = query.Where(o => o.RestaurantId == RestaurantId);
            }
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.ScheduledDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.ScheduledDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.OrderTotal >= minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.OrderTotal <= maxAmount);
            }
            var pages = query.OrderByDescending(o => o.Id)
          .Skip(skip)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the rest order item details.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>IEnumerable RestaurantOrderItem.</returns>
        public IEnumerable<RestaurantOrderItem> GetRestOrderItemDetails(int orderId)
        {
            return _restaurantOrderItemOperation.GetAll(o => o.RestaurantOrderId == orderId);
        }

        /// <summary>
        /// Updates the rest order item served status.
        /// </summary>
        /// <param name="orderItemId">The order item identifier.</param>
        public void UpdateRestOrderItemServedStatus(int orderItemId)
        {
            var orderItem = _restaurantOrderItemOperation.GetById(orderItemId);
            if (orderItem.ServedStatus == true)
                orderItem.ServedStatus = false;
            else
                orderItem.ServedStatus = true;
            _restaurantOrderItemOperation.Update(orderItem);
        }
        /// <summary>
        /// Gets all orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetAllOrders(int RestaurantId)
        {
            return _customerRestaurantOrdersRepository.Table.Where(o => o.RestaurantId == RestaurantId && !(o.OrderStatus == 3)).OrderBy(or => or.ScheduledDate);
        }

        /// <summary>
        /// Gets the rest orders queue.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetRestOrdersQueue(int RestaurantId, int skip, int pageSize, out int total)
        {
            return _customerRestaurantOrdersOperation.GetAllWithServerSidePagging(o => o.RestaurantId == RestaurantId && o.OrderStatus <= 3, o => o.ScheduledDate, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the rest recent orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetRestRecentOrders(int? RestaurantId, int skip, int pageSize, out int total)
        {
            var toDate = DateTime.Now;
            var fromDate = toDate.AddDays(-30);
            if (RestaurantId != null)
            {
                return _customerRestaurantOrdersOperation.GetAllWithServerSidePagging(o => o.RestaurantId == RestaurantId && o.OrderStatus == 4 &&
                                                                            o.ScheduledDate.Date >= fromDate.Date && o.ScheduledDate.Date <= toDate.Date,
                                                                            o => o.ScheduledDate, skip, pageSize, out total);
            }
            else
            {
                return _customerRestaurantOrdersOperation.GetAllWithServerSidePagging(o => o.OrderStatus == 4 &&
                                                                            o.ScheduledDate.Date >= fromDate.Date && o.ScheduledDate.Date <= toDate.Date,
                                                                            o => o.ScheduledDate, skip, pageSize, out total);
            }

        }

        /// <summary>
        /// Gets the orders details by identifier.
        /// </summary>
        /// <param name="order_ID">The order identifier.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder GetOrdersDetailsById(int order_ID)
        {

            return _customerRestaurantOrdersOperation.GetById(order_ID);
        }

        /// <summary>
        /// Modifies the orders.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>RestaurantOrder.</returns>
        public RestaurantOrder ModifyOrders(RestaurantOrder details)
        {
            _customerRestaurantOrdersOperation.Update(details.Id, details);
            return details;
        }

        /// <summary>
        /// Creates the restaurant order review.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>RestaurantOrderReview.</returns>
        public RestaurantOrderReview CreateRestaurantOrderReview(RestaurantOrderReview details)
        {
            details.CreationDate = DateTime.UtcNow;
            return _restaurantCustomerReviewsOperations.AddNew(details);
        }

        /// <summary>
        /// Gets the order reviews.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantOrderReview.</returns>
        public IEnumerable<RestaurantOrderReview> GetOrderReviews(int id)
        {
            return _restaurantCustomerReviewsOperations.GetAll(m => m.RestaurantOrderId == id);
        }
        /// <summary>
        /// Gets the restaurant order reviews.
        /// </summary>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrderReview.</returns>
        public IEnumerable<RestaurantOrderReview> GetRestaurantOrderReviews(string From, string To, int restaurantId, int skip, int pageSize, out int total)
        {
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                DateTime from_date = DateTime.ParseExact(From, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                DateTime to_date = DateTime.ParseExact(To, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                return _restaurantCustomerReviewsOperations.GetAllWithServerSidePaggingDesc(m => m.RestaurantOrder.RestaurantId == restaurantId
                                                                                                 && m.CreationDate.Date >= from_date.Date
                                                                                                 && m.CreationDate.Date <= to_date.Date
                                                                                                 , o => o.Id, skip, pageSize, out total);
            }
            else
            {
                return _restaurantCustomerReviewsOperations.GetAllWithServerSidePaggingDesc(m => m.RestaurantOrder.RestaurantId == restaurantId, o => o.Id, skip, pageSize, out total);
            }

        }

        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteOrder(int id)
        {
            _customerRestaurantOrdersOperation.Delete(id);
        }


        /// <summary>
        /// Gets the todays orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetTodaysOrders(int RestaurantId)
        {
            var query = _restaurantOrderRepository.Table;
            var todayDate = DateTime.Now;
            return query.Where(o => DbFunctions.TruncateTime(o.CreationDate) == DbFunctions.TruncateTime(todayDate) && o.RestaurantId == RestaurantId).ToList();
        }
        /// <summary>
        /// Gets the last twelve months orders.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable LastTwelveMonthsEarnings.</returns>
        public IEnumerable<LastTwelveMonthsEarnings> GetLastTwelveMonthsOrders(int RestaurantId)
        {
            var query = _restaurantOrderRepository.Table;
            var todayDate = DateTime.Now;
            var fromDate = todayDate.AddMonths(-11);
            query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDate) &&
                                    DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(todayDate)
                                    && o.RestaurantId == RestaurantId);

            return query.GroupBy(o => o.CreationDate.Month)
                                    .Select(gr => new LastTwelveMonthsEarnings { Month = gr.Key, OrderDate = gr.FirstOrDefault().CreationDate, TotalAmount = gr.Sum(o => o.RestaurantOrderItems.Sum(i => i.RestaurantMenuItem.Price * i.Qty)) })
                                    .OrderBy(o => o.OrderDate).ToList();
        }

        /// <summary>
        /// Gets the restaurant wise monthly earnings.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantWiseMonthEarnings.</returns>
        public IEnumerable<RestaurantWiseMonthEarnings> GetRestaurantWiseMonthlyEarnings(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            var query = _restaurantOrderRepository.Table;
            if (customerRole.RoleId == 8)
            {
                var restaurantIds = _chainDetailsTableRepository.Table.Where(c => c.ChainId == customerRole.ChainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();
                query.Where(odr => restaurantIds.Contains(odr.RestaurantId)).ToList();
            }

            var todayDate = DateTime.Now;
            var fromDate = todayDate.AddDays(-30);
            query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDate) &&
                                    DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(todayDate));

            return query.GroupBy(o => o.RestaurantId)
                                    .Select(gr => new RestaurantWiseMonthEarnings { RestaurantName = gr.FirstOrDefault().Restaurant.Name, TotalAmount = gr.Sum(o => o.RestaurantOrderItems.Sum(i => i.RestaurantMenuItem.Price * i.Qty)) })
                                    .OrderByDescending(r => r.TotalAmount).Take(20)
                                    .ToList();
        }
        /// <summary>
        /// Gets the todays orders.
        /// </summary>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetTodaysOrders()
        {
            var query = _restaurantOrderRepository.Table;
            var todayDate = DateTime.Now;
            return query.Where(o => DbFunctions.TruncateTime(o.ScheduledDate) == DbFunctions.TruncateTime(todayDate)).ToList();
        }


        #endregion Restaurant Order

        #region Customer Restaurnt Orders

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId)
        {
            return _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId && o.OrderStatus < 4 && o.RoomId == null);
        }

        /// <summary>
        /// Gets the customer active room service orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public CustomerActiveOrdersAndBaskets GetCustomerActiveRoomServiceOrders(int customerId)
        {
            //return _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId && o.OrderStatus < 4 && o.RoomId != null);
            var activeOrders = _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId && o.OrderStatus < 4 && o.RoomId != null).ToList();
            var activeBaskets = _restaurantCartOperations.GetAll(c => c.CustomerId == customerId && c.Amount > 0 && c.RoomId != null).ToList();
            CustomerActiveOrdersAndBaskets activeOrdersAndBaskets = new CustomerActiveOrdersAndBaskets()
            {
                ActiveOrders = activeOrders,
                ActiveBaskets = activeBaskets
            };
            return activeOrdersAndBaskets;
        }

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId, int year)
        {
            if (year != 0)
                return _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId && o.CreationDate.Year == year && o.PaymentStatus == true);
            else
            {
                var currentDate = DateTime.UtcNow;
                var previousDate = currentDate.AddMonths(-12);
                previousDate = previousDate.AddMonths(1);
                var prevStartDate = new DateTime(previousDate.Year, previousDate.Month, 1);
                return _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId
                                                                      && (o.CreationDate.Date >= prevStartDate.Date && o.CreationDate.Date <= currentDate.Date)
                                                                      && o.PaymentStatus == true);
            }

        }

        /// <summary>
        /// Gets the customer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetCustomerOrders(int customerId, int year, int month, int pageNumber, int pageSize, out int total)
        {
            var query = _customerRestaurantOrdersRepository.Table;
            if (month != 0)
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year && o.CreationDate.Month == month);
            else
                query = query.Where(o => o.CustomerId == customerId && o.PaymentStatus == true && o.CreationDate.Year == year);
            var pages = query.OrderBy(_ => true)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the customer active orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>CustomerActiveOrdersAndBaskets.</returns>
        public CustomerActiveOrdersAndBaskets GetCustomerActiveOrders(int customerId)
        {
            var activeOrders = _customerRestaurantOrdersOperation.GetAll(o => o.CustomerId == customerId && o.OrderStatus <= 3 && o.CustomerTableNumber != null).ToList();
            var activeBaskets = _restaurantCartOperations.GetAll(c => c.CustomerId == customerId && c.Amount > 0 && c.TableId != null).ToList();
            CustomerActiveOrdersAndBaskets activeOrdersAndBaskets = new CustomerActiveOrdersAndBaskets()
            {
                ActiveOrders = activeOrders,
                ActiveBaskets = activeBaskets
            };
            return activeOrdersAndBaskets;
        }

        #endregion Customer Restaurnt Orders

        #region Restaurant Table Group

        /// <summary>
        /// Gets the restaurant table groups.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantTableGroup.</returns>
        public IEnumerable<RestaurantTableGroup> GetRestaurantTableGroups(int restaurantId, int skip, int pageSize, out int total)
        {
            return _restaurantTableGroupOperations.GetAllWithServerSidePagging(t => t.RestaurantId == restaurantId, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Gets the restaurant table group by identifier.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        /// <returns>RestaurantTableGroup.</returns>
        public RestaurantTableGroup GetRestaurantTableGroupById(int restTableGroupId)
        {
            return _restaurantTableGroupOperations.GetById(restTableGroupId);
        }
        /// <summary>
        /// Creates the restaurant table group.
        /// </summary>
        /// <param name="restTableGroup">The rest table group.</param>
        /// <returns>RestaurantTableGroup.</returns>
        public RestaurantTableGroup CreateRestaurantTableGroup(RestaurantTableGroup restTableGroup)
        {
            restTableGroup.CreationDate = DateTime.UtcNow;
            return _restaurantTableGroupOperations.AddNew(restTableGroup);
        }
        /// <summary>
        /// Modifies the restaurant table group.
        /// </summary>
        /// <param name="restTableGroup">The rest table group.</param>
        /// <returns>RestaurantTableGroup.</returns>
        public RestaurantTableGroup modifyRestaurantTableGroup(RestaurantTableGroup restTableGroup)
        {
            _restaurantTableGroupOperations.Update(restTableGroup.Id, restTableGroup);
            return restTableGroup;
        }
        /// <summary>
        /// Deletes the restaurant table group.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        public void DeleteRestaurantTableGroup(int restTableGroupId)
        {
            _restaurantTableGroupOperations.Delete(restTableGroupId);
        }

        #endregion Restaurant Table Group

        #region Restaurant Table

        /// <summary>
        /// Gets the restaurant table details.
        /// </summary>
        /// <param name="restTableGroupId">The rest table group identifier.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        public IEnumerable<RestaurantTable> GetRestaurantTableDetails(int restTableGroupId)
        {
            return _restaurantTablesOperation.GetAll(t => t.RestaurantTableGroupId == restTableGroupId);
        }
        /// <summary>
        /// Gets the restaurant table by identifier.
        /// </summary>
        /// <param name="table_id">The table identifier.</param>
        /// <returns>RestaurantTable.</returns>
        public RestaurantTable GetRestaurantTableById(int table_id)
        {
            return _restaurantTablesOperation.GetById(table_id);
        }
        /// <summary>
        /// Checks the exists table number.
        /// </summary>
        /// <param name="table_number">The table number.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        public IEnumerable<RestaurantTable> CheckExistsTableNumber(int table_number, int restaurantId)
        {
            return _restaurantTablesOperation.GetAll(t => t.TableNumber == table_number && t.RestaurantTableGroup.RestaurantId == restaurantId);
        }
        /// <summary>
        /// Creates the restaurant table.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTable.</returns>
        public RestaurantTable CreateRestaurantTable(RestaurantTable Table)
        {
            Table.CreationDate = DateTime.UtcNow;
            return _restaurantTablesOperation.AddNew(Table);
        }
        /// <summary>
        /// Modifies the restaurant table.
        /// </summary>
        /// <param name="Table">The table.</param>
        /// <returns>RestaurantTable.</returns>
        public RestaurantTable ModifyRestaurantTable(RestaurantTable Table)
        {
            _restaurantTablesOperation.Update(Table.Id, Table);
            return Table;
        }
        /// <summary>
        /// Deletes the restaurant table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRestaurantTable(int id)
        {
            _restaurantTablesOperation.Delete(id);
        }
        /// <summary>
        /// Gets the rest status changed tables.
        /// </summary>
        /// <param name="RestaurantId">The restaurant identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantTable.</returns>
        public IEnumerable<RestaurantTable> GetRestStatusChangedTables(int RestaurantId, int skip, int pageSize, out int total)
        {
            return _restaurantTablesOperation.GetAllWithServerSidePagging(t => t.RestaurantTableGroup.RestaurantId == RestaurantId && t.TableStatus != null, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the rest tables status changed count.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>System.Int32.</returns>
        public int GetRestTablesStatusChangedCount(int restaurantId)
        {
            return _restaurantTablesOperation.GetAll(t => t.RestaurantTableGroup.RestaurantId == restaurantId && t.TableStatus != null).ToList().Count();
        }

        #endregion Restaurant Table

        #region Restaurant Table Menu

        /// <summary>
        /// Gets the restaurant table menu by table identifier.
        /// </summary>
        /// <param name="tableId">The table identifier.</param>
        /// <returns>IEnumerable RestaurantTableMenu.</returns>
        public IEnumerable<RestaurantTableMenu> GetRestaurantTableMenuByTableId(int tableId)
        {
            return _restaurantTableMenuOperations.GetAll(tm => tm.RestaurantTableId == tableId);
        }

        /// <summary>
        /// Creates the restaurant table menu.
        /// </summary>
        /// <param name="tableMenu">The table menu.</param>
        /// <returns>RestaurantTableMenu.</returns>
        public RestaurantTableMenu CreateRestaurantTableMenu(RestaurantTableMenu tableMenu)
        {
            tableMenu.CreationDate = DateTime.UtcNow;
            return _restaurantTableMenuOperations.AddNew(tableMenu);
        }
        /// <summary>
        /// Modifies the restaurant table menu.
        /// </summary>
        /// <param name="tableMenu">The table menu.</param>
        /// <returns>RestaurantTableMenu.</returns>
        public RestaurantTableMenu ModifyRestaurantTableMenu(RestaurantTableMenu tableMenu)
        {
            _restaurantTableMenuOperations.Update(tableMenu.Id, tableMenu);
            return tableMenu;
        }
        /// <summary>
        /// Gets the restaurant table menu by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantTableMenu.</returns>
        public RestaurantTableMenu GetRestaurantTableMenuById(int id)
        {
            return _restaurantTableMenuOperations.GetById(id);
        }
        /// <summary>
        /// Deletes the restaurant table menu.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteRestaurantTableMenu(int id)
        {
            _restaurantTableMenuOperations.Delete(id);
        }

        #endregion Restaurant Table Menu

        #region Customer Role

        /// <summary>
        /// Restaurants the authorized customers.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> RestaurantAuthorizedCustomers(int id)
        {
            return _customerRoleOperations.GetAll(r => r.RestaurantId == id);
        }

        /// <summary>
        /// Authorizeds the restaurant customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> AuthorizedRestaurantCustomers(int customerId, int id)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            if (customerRole.RoleId == 1)
            {
                return _customerRoleOperations.GetAll(r => r.RestaurantId == id || r.RestaurantId == null);
            }
            else
            {
                var chainRestaurantId = (from chainDetails in _chainDetailsTableRepository.Table
                                         where chainDetails.RestaurantId != null && chainDetails.ChainId == customerRole.ChainId && chainDetails.RestaurantId == id
                                         select chainDetails.RestaurantId).ToArray();

                var chainHotelId = (from chainDetails in _chainDetailsTableRepository.Table
                                    where chainDetails.HotelId != null && chainDetails.ChainId == customerRole.ChainId
                                    select chainDetails.HotelId).ToArray();

                return _customerRoleOperations.GetAll(c => c.CustomerId == customerId || (chainRestaurantId.Contains(c.RestaurantId)) || (chainHotelId.Contains(c.HotelId)));
            }
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetCustomers(string searchText)
        {
            var customerRole = (from role in _customerRoleRepository.Table
                                select role.CustomerId).ToArray();

            var customer = _customerRepository.Table
                       .Where(c => c.FirstName.Contains(searchText) && !customerRole.Contains(c.Id));

            return customer;
        }

        /// <summary>
        /// Gets all role.
        /// </summary>
        /// <returns>IEnumerable Role.</returns>
        public IEnumerable<Role> GetAllRole()
        {
            return _roleRepository.Table;
        }

        /// <summary>
        /// Gets the role for chain admin.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>IEnumerable Role.</returns>
        public IEnumerable<Role> GetRoleForChainAdmin(int roleId)
        {
            if (roleId == 8)
                return _roleRepository.Table.Where(r => r.Id != 1);
            else
                return _roleRepository.Table;
        }

        /// <summary>
        /// Manages the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>IEnumerable Role.</returns>
        public IEnumerable<Role> ManageRole(int roleId)
        {
            if (roleId == 1)
            {
                int[] roleList = new int[] { 1, 2, 3, 4 };
                return _roleRepository.Table.Where(r => roleList.Contains(r.Id));
            }
            else if (roleId == 2 || roleId == 3)
            {
                return _roleRepository.Table.Where(r => r.Id == 3 || r.Id == 4);
            }
            else if (roleId == 8)
            {
                int[] roleList = new int[] { 2, 3, 4 };
                return _roleRepository.Table.Where(r => roleList.Contains(r.Id));
            }
            else
                return _roleRepository.Table;
        }

        /// <summary>
        /// Creates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole CreateCustomerRole(CustomerRole role)
        {
            return _customerRoleOperations.AddNew(role);
        }

        /// <summary>
        /// Updates the customer role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole UpdateCustomerRole(CustomerRole role)
        {
            _customerRoleOperations.Update(role.Id, role);
            return role;
        }

        /// <summary>
        /// Gets the customer role.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> GetCustomerRole(int customerId)
        {
            return _customerRoleOperations.GetAll(r => r.CustomerId == customerId);
        }
        /// <summary>
        /// Deletes the customer role.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCustomerRole(int id)
        {
            _customerRoleOperations.Delete(id);
        }

        /// <summary>
        /// Tops the low selling menu item.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="From">From.</param>
        /// <param name="To">To.</param>
        /// <returns>IEnumerable RestaurantOrderItem.</returns>
        public IEnumerable<RestaurantOrderItem> TopLowSellingMenuItem(int restaurantId, string From, string To)
        {
            var query = _restaurantOrderItemRepository.Table;
            query = query.Where(ro => ro.RestaurantOrder.RestaurantId == restaurantId);
            if (!string.IsNullOrWhiteSpace(From) && !string.IsNullOrWhiteSpace(To))
            {
                var from_date = Convert.ToDateTime(From);
                var to_date = Convert.ToDateTime(To);
                query = query.Where(o => DbFunctions.TruncateTime(o.RestaurantOrder.CreationDate) >= DbFunctions.TruncateTime(from_date)
                                    && DbFunctions.TruncateTime(o.RestaurantOrder.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            return query.ToList();
        }

        /// <summary>
        /// Authorizeds the customers.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable CustomerRole.</returns>
        public IEnumerable<CustomerRole> AuthorizedCustomers(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId);
            if (customerRole.RoleId == 1)
            {
                return _customerRoleOperations.GetAll();
            }
            else
            {
                var chainRestaurantId = (from chainDetails in _chainDetailsTableRepository.Table
                                         where chainDetails.RestaurantId != null && chainDetails.ChainId == customerRole.ChainId
                                         select chainDetails.RestaurantId).ToArray();

                var chainHotelId = (from chainDetails in _chainDetailsTableRepository.Table
                                    where chainDetails.HotelId != null && chainDetails.ChainId == customerRole.ChainId
                                    select chainDetails.HotelId).ToArray();

                return _customerRoleOperations.GetAll(c => c.CustomerId == customerId || (chainRestaurantId.Contains(c.RestaurantId)) || (chainHotelId.Contains(c.HotelId)));
            }
        }

        /// <summary>
        /// Gets the un role customers.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable Customer.</returns>
        public IEnumerable<Customer> GetUnRoleCustomers(string searchText)
        {
            var customerRole = (from role in _customerRoleRepository.Table
                                where (role.RoleId == 1 || role.RoleId == 3)
                                select role.CustomerId).ToArray();

            var customer = _customerRepository.Table
                       .Where(c => c.FirstName.Contains(searchText) && !customerRole.Contains(c.Id));

            return customer;
        }

        /// <summary>
        /// Gets the customer by role identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>CustomerRole.</returns>
        public CustomerRole GetCustomerByRoleId(int Id)
        {
            return _customerRoleOperations.GetById(Id);
        }

        #endregion Customer Role

        #region MenuItem Ingredient

        /// <summary>
        /// Gets the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemIngredient.</returns>
        public IEnumerable<MenuItemIngredient> GetItemIngredient(int id)
        {
            return _menuItemIngredientOperations.GetAll(i => i.RestaurantMenuItemId == id);
        }

        /// <summary>
        /// Gets the menu item ingredients.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemIngredient.</returns>
        public IEnumerable<MenuItemIngredient> GetMenuItemIngredients(int id, int skip, int pageSize, out int total)
        {
            return _menuItemIngredientOperations.GetAllWithServerSidePagging(i => i.RestaurantMenuItemId == id, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemSize.</returns>
        public IEnumerable<MenuItemSize> GetItemSize(int id)
        {
            return _menuItemSizeOperations.GetAll(i => i.RestaurantMenuItemId == id);
        }
        /// <summary>
        /// Gets the item additionals.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemAdditional.</returns>
        public IEnumerable<MenuItemAdditional> GetItemAdditionals(int id)
        {
            return _menuItemAdditionalOperations.GetAll(i => i.RestaurantMenuItemId == id);
        }
        /// <summary>
        /// Gets all ingredient.
        /// </summary>
        /// <returns>IEnumerable Ingredient.</returns>
        public IEnumerable<Ingredient> GetAllIngredient()
        {
            return _ingredientRepository.Table;
        }


        /// <summary>
        /// Gets all un assigned ingredient.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>IEnumerable Ingredient.</returns>
        public IEnumerable<Ingredient> GetAllUnAssignedIngredient(int menuItemId)
        {
            var itemIngredientId = _menuItemIngredientRepository.Table.Where(i => i.RestaurantMenuItemId == menuItemId).Select(i => i.IngredientId).ToArray();

            var ingredient = from ingre in _ingredientRepository.Table
                             where (!itemIngredientId.Contains(ingre.Id))
                             select (ingre);

            return ingredient;
        }

        /// <summary>
        /// Gets all ingredient.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Ingredient.</returns>
        public IEnumerable<Ingredient> GetAllIngredient(int ingredientCatId, int skip, int pageSize, out int total)
        {
            return _ingredientOperation.GetAllWithServerSidePagging(i => i.IngredientCategoryId == ingredientCatId, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Deletes the ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteIngredient(int id)
        {
            _ingredientOperation.Delete(id);
        }

        /// <summary>
        /// Adds the item ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>MenuItemIngredient.</returns>
        public MenuItemIngredient AddItemIngredient(MenuItemIngredient ingredient)
        {
            return _menuItemIngredientRepository.InsertWithChild(ingredient);
        }
        /// <summary>
        /// Updates the item ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>MenuItemIngredient.</returns>
        public MenuItemIngredient UpdateItemIngredient(MenuItemIngredient ingredient)
        {
            _menuItemIngredientOperations.Update(ingredient.Id, ingredient);
            return ingredient;
        }
        /// <summary>
        /// Gets the ingredient by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemIngredient.</returns>
        public MenuItemIngredient GetIngredientById(int id)
        {
            return _menuItemIngredientOperations.GetById(id);
        }

        /// <summary>
        /// Deletes the item ingredient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteItemIngredient(int id)
        {
            _menuItemIngredientOperations.Delete(id);
        }

        #endregion MenuItem Ingredient

        #region MenuItemIngredient Nutrition

        /// <summary>
        /// Gets the menu item ingredient nutrition.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable MenuItemIngredientNutrition.</returns>
        public IEnumerable<MenuItemIngredientNutrition> GetMenuItemIngredientNutrition(int id)
        {
            return _menuItemIngredientNutritionOperations.GetAll(i => i.MenuItemIngredientId == id);
        }

        /// <summary>
        /// Updates the menu item ingredient nutrition.
        /// </summary>
        /// <param name="menuItemIngredientNutrition">The menu item ingredient nutrition.</param>
        public void UpdateMenuItemIngredientNutrition(MenuItemIngredientNutrition menuItemIngredientNutrition)
        {
            _menuItemIngredientNutritionRepository.Update(menuItemIngredientNutrition.Id, menuItemIngredientNutrition);
        }
        /// <summary>
        /// Deletes the menu item ingredient nutrient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteMenuItemIngredientNutrient(int id)
        {
            _menuItemIngredientNutritionOperations.Delete(id);
        }

        #endregion MenuItemIngredient Nutrition

        #region MenuItem Size

        /// <summary>
        /// Adds the size of the item.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        /// <returns>MenuItemSize.</returns>
        public MenuItemSize AddItemSize(MenuItemSize itemSize)
        {
            return _menuItemSizeOperations.AddNew(itemSize);
        }
        /// <summary>
        /// Gets the item size by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemSize.</returns>
        public MenuItemSize GetItemSizeById(int id)
        {
            return _menuItemSizeOperations.GetById(id);
        }
        /// <summary>
        /// Updates the size of the item.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        /// <returns>MenuItemSize.</returns>
        public MenuItemSize UpdateItemSize(MenuItemSize itemSize)
        {
            _menuItemSizeOperations.Update(itemSize.Id, itemSize);
            return itemSize;
        }
        /// <summary>
        /// Deletes the size of the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteItemSize(int id)
        {
            _menuItemSizeOperations.Delete(id);
        }

        #endregion MenuItem Size

        #region MenuItem Additional

        /// <summary>
        /// Adds the item additional.
        /// </summary>
        /// <param name="itemAdditional">The item additional.</param>
        /// <returns>MenuItemAdditional.</returns>
        public MenuItemAdditional AddItemAdditional(MenuItemAdditional itemAdditional)
        {
            return _menuItemAdditionalOperations.AddNew(itemAdditional);
        }
        /// <summary>
        /// Modifies the item additional.
        /// </summary>
        /// <param name="itemAdditional">The item additional.</param>
        /// <returns>MenuItemAdditional.</returns>
        public MenuItemAdditional ModifyItemAdditional(MenuItemAdditional itemAdditional)
        {
            _menuItemAdditionalOperations.Update(itemAdditional.Id, itemAdditional);
            return itemAdditional;
        }
        /// <summary>
        /// Gets the item additional by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemAdditional.</returns>
        public MenuItemAdditional GetItemAdditionalById(int id)
        {
            return _menuItemAdditionalOperations.GetById(id);
        }
        /// <summary>
        /// Deletes the item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteItemAdditional(int id)
        {
            _menuItemAdditionalOperations.Delete(id);
        }

        #endregion MenuItem Additional

        #region Menu Additional Group

        /// <summary>
        /// Gets all menu additional group.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <returns>IEnumerable MenuAdditionalGroup.</returns>
        public IEnumerable<MenuAdditionalGroup> GetAllMenuAdditionalGroup(int restaurantId, int menuItemId)
        {
            var assignedMenuAddGrp = _menuItemAdditionalOperations.GetAll(a => a.RestaurantMenuItemId == menuItemId).Select(m => m.MenuAdditionalGroupId).ToList();
            return _menuAdditionalGroupOperations.GetAll(g => g.RestaurantId == restaurantId && !assignedMenuAddGrp.Contains(g.Id));
        }
        /// <summary>
        /// Gets the menu additional groups.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable MenuAdditionalGroup.</returns>
        public IEnumerable<MenuAdditionalGroup> GetMenuAdditionalGroups(int restaurantId)
        {
            return _menuAdditionalGroupOperations.GetAll(m => m.RestaurantId == restaurantId);
        }
        /// <summary>
        /// Gets the menu additional group by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        public MenuAdditionalGroup GetMenuAdditionalGroupById(int id)
        {
            return _menuAdditionalGroupOperations.GetById(id);
        }

        /// <summary>
        /// Creates the menu additional group.
        /// </summary>
        /// <param name="menuAdditionalGrp">The menu additional GRP.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        public MenuAdditionalGroup CreateMenuAdditionalGroup(MenuAdditionalGroup menuAdditionalGrp)
        {
            menuAdditionalGrp.CreationDate = DateTime.UtcNow;
            return _menuAdditionalGroupOperations.AddNew(menuAdditionalGrp);
        }
        /// <summary>
        /// Modifies the menu additional group.
        /// </summary>
        /// <param name="menuAdditionalGrp">The menu additional GRP.</param>
        /// <returns>MenuAdditionalGroup.</returns>
        public MenuAdditionalGroup ModifyMenuAdditionalGroup(MenuAdditionalGroup menuAdditionalGrp)
        {
            _menuAdditionalGroupOperations.Update(menuAdditionalGrp);
            return menuAdditionalGrp;
        }
        /// <summary>
        /// Deletes the menu additional group.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteMenuAdditionalGroup(int id)
        {
            _menuAdditionalGroupOperations.Delete(id);
        }

        #endregion Menu Additional Group

        #region Menu Additional Element

        /// <summary>
        /// Gets the menu additional elements.
        /// </summary>
        /// <param name="grp_id">The GRP identifier.</param>
        /// <returns>IEnumerable MenuAdditionalElement.</returns>
        public IEnumerable<MenuAdditionalElement> GetMenuAdditionalElements(int grp_id)
        {
            return _menuAdditionalElementOperations.GetAll(e => e.MenuAdditionalGroupId == grp_id);
        }
        /// <summary>
        /// Gets the menu additional element by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuAdditionalElement.</returns>
        public MenuAdditionalElement GetMenuAdditionalElementById(int id)
        {
            return _menuAdditionalElementOperations.GetById(id);
        }
        /// <summary>
        /// Creates the menu additional element.
        /// </summary>
        /// <param name="menuAdditionalElement">The menu additional element.</param>
        /// <returns>MenuAdditionalElement.</returns>
        public MenuAdditionalElement CreateMenuAdditionalElement(MenuAdditionalElement menuAdditionalElement)
        {
            menuAdditionalElement.CreationDate = DateTime.UtcNow;
            return _menuAdditionalElementOperations.AddNew(menuAdditionalElement);
        }
        /// <summary>
        /// Modifies the menu additional element.
        /// </summary>
        /// <param name="menuAdditionalElement">The menu additional element.</param>
        /// <returns>MenuAdditionalElement.</returns>
        public MenuAdditionalElement ModifyMenuAdditionalElement(MenuAdditionalElement menuAdditionalElement)
        {
            _menuAdditionalElementOperations.Update(menuAdditionalElement);
            return menuAdditionalElement;
        }
        /// <summary>
        /// Deletes the menu additional element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteMenuAdditionalElement(int id)
        {
            _menuAdditionalElementOperations.Delete(id);
        }

        /// <summary>
        /// Updates the menu item additional.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>MenuItemAdditional.</returns>
        public MenuItemAdditional UpdateMenuItemAdditional(int id)
        {
            var menuItemAdd = _menuItemAdditionalOperations.GetAll(m => m.Id == id).FirstOrDefault();
            if (menuItemAdd.IsActive == true)
            {
                menuItemAdd.IsActive = false;
            }
            else
            {
                menuItemAdd.IsActive = true;
            }
            _menuItemAdditionalOperations.Update(menuItemAdd);
            return menuItemAdd;
        }

        #endregion Menu Additional Element

        /// <summary>
        /// Gets the customer restaurant reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetCustomerRestaurantReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            var query = _restaurantReviewRepository.Table;
            query = query.Where(r => r.CustomerId == customerId);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantReview>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets the customer menu item reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemReviewAndRestDetails.</returns>
        public IEnumerable<MenuItemReviewAndRestDetails> GetCustomerMenuItemReviews(int customerId, int pageNumber, int pageSize, out int total)
        {
            var query = _menuItemReviewRepository.Table;
            query = query.Where(r => r.CustomerId == customerId);
            var pages = query.OrderByDescending(r => r.Id)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize)
                        .GroupBy(p => new { Total = query.Count() })
                        .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<MenuItemReviewAndRestDetails>();
            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            var result = entities.Select(r => new MenuItemReviewAndRestDetails
            {
                Id = r.Id,
                RestaurantMenuItemId = r.RestaurantMenuItemId,
                Score = r.Score,
                Comment = r.Comment,
                RestaurantMenuItem = r.RestaurantMenuItem,
                Restaurant = r.RestaurantMenuItem.RestaurantMenuGroup.RestaurantMenu.Restaurant
            }).ToList();
            return result;
        }

        /// <summary>
        /// Gets the pending reviews restaurants.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetPendingReviewsRestaurants(int customerId, int pageNumber, int pageSize, out int total)
        {
            var customerRestaurants = _restaurantOperations.GetAll(r => r.RestaurantOrders.Any(o => o.CustomerId == customerId && o.OrderStatus != 0) && r.RestaurantReviews.Count(i => i.CustomerId == customerId) == 0, "Desc", pageNumber, pageSize, out total);
            return customerRestaurants;
        }

        /// <summary>
        /// Gets the pending reviews menu items.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetPendingReviewsMenuItems(int customerId, int pageNumber, int pageSize, out int total)
        {
            total = 0;
            var menuItem = _restaurantOrderItemRepository.Table.Where(o => (o.RestaurantOrder.CustomerId == customerId && o.RestaurantOrder.OrderStatus != 0) && o.RestaurantMenuItem.MenuItemReviews.Count(i => i.CustomerId == customerId) == 0).Select(o => o.RestaurantMenuItem).Distinct();

            var page = menuItem.OrderBy(i => i.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = menuItem.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<RestaurantMenuItem>();

            total = page.Key.Total;
            var menuItemEntities = page.Select(p => p);

            return menuItemEntities;
        }

        #region Restaurant MenuItem Image

        /// <summary>
        /// Gets the item images.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItemImage.</returns>
        public IEnumerable<RestaurantMenuItemImage> GetItemImages(int id)
        {
            return _restaurantMenuItemImageOperations.GetAll(i => i.RestaurantMenuItemId == id);
        }
        /// <summary>
        /// Adds the item images.
        /// </summary>
        /// <param name="itemImages">The item images.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        public RestaurantMenuItemImage AddItemImages(RestaurantMenuItemImage itemImages)
        {
            itemImages.CreationDate = DateTime.UtcNow;
            return _restaurantMenuItemImageOperations.AddNew(itemImages);
        }
        /// <summary>
        /// Updates the item images.
        /// </summary>
        /// <param name="itemImages">The item images.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        public RestaurantMenuItemImage UpdateItemImages(RestaurantMenuItemImage itemImages)
        {
            _restaurantMenuItemImageOperations.Update(itemImages.Id, itemImages);
            return itemImages;
        }

        /// <summary>
        /// Gets the item image by image identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        public RestaurantMenuItemImage GetItemImageByImageId(int id)
        {
            return _restaurantMenuItemImageOperations.GetById(id);
        }

        /// <summary>
        /// Deletes the item image.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteItemImage(int id)
        {
            _restaurantMenuItemImageOperations.Delete(id);
        }

        /// <summary>
        /// Updates the item image active.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>RestaurantMenuItemImage.</returns>
        public RestaurantMenuItemImage UpdateItemImageActive(int id)
        {
            var itemImage = _restaurantMenuItemImageOperations.GetById(i => i.Id == id);

            if (itemImage.IsActive == true)
                itemImage.IsActive = false;
            else itemImage.IsActive = true;

            _restaurantMenuItemImageOperations.Update(itemImage);
            return itemImage;
        }

        #endregion Restaurant MenuItem Image

        #region Ingredient Categories
        /// <summary>
        /// Gets all ingredient categories.
        /// </summary>
        /// <returns>IEnumerable IngredientCategory.</returns>
        public IEnumerable<IngredientCategory> GetAllIngredientCategories()
        {
            return _ingredientCategoriesOperations.GetAll(i => i.IsActive);
        }

        /// <summary>
        /// Gets the type of all ingredient categories by.
        /// </summary>
        /// <param name="type">if set to <c>true</c> [type].</param>
        /// <returns>IEnumerable IngredientCategory.</returns>
        public IEnumerable<IngredientCategory> GetAllIngredientCategoriesByType(bool type)
        {
            return _ingredientCategoriesOperations.GetAll(i => i.IsActive && i.Type == type);
        }

        /// <summary>
        /// Gets all ingredient categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable IngredientCategory.</returns>
        public IEnumerable<IngredientCategory> GetAllIngredientCategories(int skip, int pageSize, out int total)
        {
            return _ingredientCategoriesOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the ingredient category by identifier.
        /// </summary>
        /// <param name="ingredientCatId">The ingredient cat identifier.</param>
        /// <returns>IngredientCategory.</returns>
        public IngredientCategory GetIngredientCategoryById(int ingredientCatId)
        {
            return _ingredientCategoriesOperations.GetById(ingredientCatId);
        }
        /// <summary>
        /// Creates the ingredient category.
        /// </summary>
        /// <param name="ingredientCategory">The ingredient category.</param>
        /// <returns>IngredientCategory.</returns>
        public IngredientCategory CreateIngredientCategory(IngredientCategory ingredientCategory)
        {
            ingredientCategory.CreationDate = DateTime.UtcNow;
            return _ingredientCategoriesOperations.AddNew(ingredientCategory);
        }
        /// <summary>
        /// Modifies the ingredient category.
        /// </summary>
        /// <param name="ingredientCategory">The ingredient category.</param>
        /// <returns>IngredientCategory.</returns>
        public IngredientCategory ModifyIngredientCategory(IngredientCategory ingredientCategory)
        {
            _ingredientCategoriesOperations.Update(ingredientCategory.Id, ingredientCategory);
            return ingredientCategory;
        }

        /// <summary>
        /// Deletes the ingredient category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteIngredientCategory(int id)
        {
            _ingredientCategoriesOperations.Delete(id);
        }
        #endregion Ingredient Categories

        #region Ingredient Food Profiles
        /// <summary>
        /// Creates the ingredient food profiles.
        /// </summary>
        /// <param name="ingredientFoodProfiles">The ingredient food profiles.</param>
        public void CreateIngredientFoodProfiles(List<IngredientFoodProfile> ingredientFoodProfiles)
        {
            foreach (var eachIngredientFoodProfile in ingredientFoodProfiles)
            {
                _ingredientFoodProfileOperations.AddNew(eachIngredientFoodProfile);
            }
        }

        /// <summary>
        /// Gets all ingredient food profiles.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable IngredientFoodProfile.</returns>
        public IEnumerable<IngredientFoodProfile> GetAllIngredientFoodProfiles(int ingredientId)
        {
            return _ingredientFoodProfileOperations.GetAll(i => i.IngredientId == ingredientId);
        }

        /// <summary>
        /// Deletes the ingredient food profile.
        /// </summary>
        /// <param name="ingredientFoodProfileId">The ingredient food profile identifier.</param>
        public void DeleteIngredientFoodProfile(int ingredientFoodProfileId)
        {
            _ingredientFoodProfileOperations.Delete(ingredientFoodProfileId);
        }

        #endregion Ingredient Food Profiles

        #region Food Profile
        /// <summary>
        /// Gets all unassigned food profiles.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable FoodProfile.</returns>
        public IEnumerable<FoodProfile> GetAllUnassignedFoodProfiles(int? ingredientId)
        {
            if (ingredientId == null)
                return _foodProfileOperations.GetAll(f => f.IsActive);
            else
            {
                var assignedFoodProfileIds = _ingredientFoodProfileOperations.GetAll(i => i.IngredientId == ingredientId).Select(f => f.FoodProfileId).ToList();
                var unassignedFoodProfiles = _foodProfileOperations.GetAll(f => f.IsActive && !assignedFoodProfileIds.Contains(f.Id));
                return unassignedFoodProfiles;

            }
        }
        /// <summary>
        /// Gets all food profiles.
        /// </summary>
        /// <returns>IEnumerable FoodProfile.</returns>
        public IEnumerable<FoodProfile> GetAllFoodProfiles()
        {
            return _foodProfileOperations.GetAll(f => f.IsActive);
        }
        /// <summary>
        /// Gets all food profiles.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable FoodProfile.</returns>
        public IEnumerable<FoodProfile> GetAllFoodProfiles(int skip, int pageSize, out int total)
        {
            return _foodProfileOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the food profile by identifier.
        /// </summary>
        /// <param name="foodProfileId">The food profile identifier.</param>
        /// <returns>FoodProfile.</returns>
        public FoodProfile GetFoodProfileById(int foodProfileId)
        {
            return _foodProfileOperations.GetById(foodProfileId);
        }
        /// <summary>
        /// Creates the food profile.
        /// </summary>
        /// <param name="foodProfile">The food profile.</param>
        /// <returns>FoodProfile.</returns>
        public FoodProfile CreateFoodProfile(FoodProfile foodProfile)
        {
            foodProfile.CreationDate = DateTime.UtcNow;
            return _foodProfileOperations.AddNew(foodProfile);
        }
        /// <summary>
        /// Modifies the food profile.
        /// </summary>
        /// <param name="foodProfile">The food profile.</param>
        /// <returns>FoodProfile.</returns>
        public FoodProfile ModifyFoodProfile(FoodProfile foodProfile)
        {
            _foodProfileOperations.Update(foodProfile.Id, foodProfile);
            return foodProfile;
        }

        /// <summary>
        /// Deletes the food profile.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteFoodProfile(int id)
        {
            _foodProfileOperations.Delete(id);
        }
        #endregion Food Profile

        #region Allergen
        /// <summary>
        /// Gets all allergen categories.
        /// </summary>
        /// <returns>IEnumerable AllergenCategory.</returns>
        public IEnumerable<AllergenCategory> GetAllAllergenCategories()
        {
            return _allergenCategoriesOperations.GetAll(a => a.IsActive);
        }
        /// <summary>
        /// Gets all allergen categories.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable AllergenCategory.</returns>
        public IEnumerable<AllergenCategory> GetAllAllergenCategories(int skip, int pageSize, out int total)
        {
            return _allergenCategoriesOperations.GetAllWithServerSidePagging(_ => true, s => s.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the allergen category by identifier.
        /// </summary>
        /// <param name="allergenCatId">The allergen cat identifier.</param>
        /// <returns>AllergenCategory.</returns>
        public AllergenCategory GetAllergenCategoryById(int allergenCatId)
        {
            return _allergenCategoriesOperations.GetById(allergenCatId);
        }
        /// <summary>
        /// Creates the allergen category.
        /// </summary>
        /// <param name="allergenCat">The allergen cat.</param>
        /// <returns>AllergenCategory.</returns>
        public AllergenCategory CreateAllergenCategory(AllergenCategory allergenCat)
        {
            allergenCat.CreationDate = DateTime.UtcNow;
            return _allergenCategoriesOperations.AddNew(allergenCat);
        }
        /// <summary>
        /// Modifies the allergen category.
        /// </summary>
        /// <param name="allergenCat">The allergen cat.</param>
        /// <returns>AllergenCategory.</returns>
        public AllergenCategory ModifyAllergenCategory(AllergenCategory allergenCat)
        {
            _allergenCategoriesOperations.Update(allergenCat.Id, allergenCat);
            return allergenCat;
        }

        /// <summary>
        /// Deletes the allergen category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteAllergenCategory(int id)
        {
            _allergenCategoriesOperations.Delete(id);
        }

        //Allergen

        /// <summary>
        /// Gets all allergen.
        /// </summary>
        /// <param name="allergenCatId">The allergen cat identifier.</param>
        /// <returns>IEnumerable Allergen.</returns>
        public IEnumerable<Allergen> GetAllAllergen(int allergenCatId)
        {
            return _allergenOperation.GetAll(a => a.AllergenCategoryId == allergenCatId);
        }

        /// <summary>
        /// Creates the allergen.
        /// </summary>
        /// <param name="allergen">The allergen.</param>
        /// <returns>Allergen.</returns>
        /// <exception cref="System.ArgumentException">Same Allergen already exist</exception>
        public Allergen CreateAllergen(Allergen allergen)
        {
            var existing = _allergenOperation.GetAll(a => a.Name.ToLower() == allergen.Name.ToLower());
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Same Allergen already exist");

            return _allergenOperation.AddNew(allergen);
        }
        /// <summary>
        /// Modifies the allergen.
        /// </summary>
        /// <param name="allergen">The allergen.</param>
        /// <returns>Allergen.</returns>
        /// <exception cref="System.ArgumentException">Same Allergen already exist</exception>
        public Allergen ModifyAllergen(Allergen allergen)
        {
            var existing = _allergenOperation.GetAll(a => a.Name.ToLower() == allergen.Name.ToLower() && a.Id != allergen.Id);
            if (existing != null && existing.Count() > 0)
                throw new ArgumentException("Same Allergen already exist");

            _allergenOperation.Update(allergen.Id, allergen);
            return allergen;
        }

        /// <summary>
        /// Gets the allergen by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Allergen.</returns>
        public Allergen GetAllergenById(int id)
        {
            return _allergenOperation.GetById(id);
        }

        /// <summary>
        /// Deletes the allergen.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteAllergen(int id)
        {
            _allergenOperation.Delete(id);
        }
        #endregion Allergen

        #region Ingredient
        /// <summary>
        /// Creates the ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>Ingredient.</returns>
        /// <exception cref="System.ArgumentException">Same Ingredient already exist</exception>
        public Ingredient CreateIngredient(Ingredient ingredient)
        {
            var existingIngredient = _ingredientOperation.GetAll(i => i.Name.ToLower() == ingredient.Name.ToLower());
            if (existingIngredient != null && existingIngredient.Count() > 0)
                throw new ArgumentException("Same Ingredient already exist");

            return _ingredientOperation.AddNew(ingredient);
        }

        /// <summary>
        /// Modifies the ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <returns>Ingredient.</returns>
        /// <exception cref="System.ArgumentException">Same Ingredient already exist</exception>
        public Ingredient ModifyIngredient(Ingredient ingredient)
        {
            var existingIngredient = _ingredientOperation.GetAll(i => i.Name.ToLower() == ingredient.Name.ToLower() && i.Id != ingredient.Id);
            if (existingIngredient != null && existingIngredient.Count() > 0)
                throw new ArgumentException("Same Ingredient already exist");

            _ingredientOperation.Update(ingredient.Id, ingredient);
            return ingredient;
        }

        /// <summary>
        /// Gets the ingredient.
        /// </summary>
        /// <param name="ingredient_Id">The ingredient identifier.</param>
        /// <returns>Ingredient.</returns>
        public Ingredient GetIngredient(int ingredient_Id)
        {
            return _ingredientOperation.GetById(ingredient_Id);
        }

        #endregion Ingredient

        #region Languages

        /// <summary>
        /// Gets all languages.
        /// </summary>
        /// <returns>IEnumerable Language.</returns>
        public IEnumerable<Language> GetAllLanguages()
        {
            return _languageRepository.Table.ToList();
        }

        /// <summary>
        /// Gets the language by culture.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>Language.</returns>
        public Language GetLanguageByCulture(string culture)
        {
            return _languageOperation.GetAll(l => l.LanguageCulture == culture).FirstOrDefault();
        }

        #endregion Languages

        #region ChainTable

        /// <summary>
        /// Gets all chains.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        public IEnumerable<ChainTable> GetAllChains(int customerId)
        {
            var customerRole = _customerRoleOperations.GetById(c => c.CustomerId == customerId).RoleId;
            if (customerRole == 1)
            {
                return _chainTableOperation.GetAll();
            }
            else
            {
                return _customerRoleOperations.GetAll(c => c.CustomerId == customerId).Select(c => c.ChainTable).Where(ct => ct.IsActive == true);
            }

        }

        /// <summary>
        /// Gets the un assign chain restaurant.
        /// </summary>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetUnAssignChainRestaurant()
        {
            var chainDetailsTable = _chainDetailsTableRepository.Table.Select(c => c.RestaurantId).ToArray();

            var restaurant = (from rest in _restaurantDetailsRepository.Table.Where(r => r.ActiveStatus && r.IsActive)
                              where (!chainDetailsTable.Contains(rest.Id))
                              select (rest));

            return restaurant;
        }

        /// <summary>
        /// Creates the chain.
        /// </summary>
        /// <param name="chainTable">The chain table.</param>
        /// <returns>ChainTable.</returns>
        public ChainTable CreateChain(ChainTable chainTable)
        {
            chainTable.CreationDate = DateTime.UtcNow;
            return _chainTableRepository.InsertWithChild(chainTable);
        }

        /// <summary>
        /// Gets the chain by identifier.
        /// </summary>
        /// <param name="chainId">The chain identifier.</param>
        /// <returns>ChainTable.</returns>
        public ChainTable GetChainById(int chainId)
        {
            return _chainTableOperation.GetById(c => c.Id == chainId);
        }
        /// <summary>
        /// Gets the chains by identifier.
        /// </summary>
        /// <param name="chainId">The chain identifier.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        public IEnumerable<ChainTable> GetChainsById(int chainId)
        {
            return _chainTableOperation.GetAll(c => c.Id == chainId);
        }
        /// <summary>
        /// Gets the assign chain restaurant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAssignChainRestaurant(int id)
        {
            var chainDetailsTable = _chainDetailsTableRepository.Table.Where(c => c.ChainId != id).Select(c => c.RestaurantId).ToArray();

            var restaurant = (from rest in _restaurantDetailsRepository.Table
                              where (!chainDetailsTable.Contains(rest.Id))
                              select (rest));

            return restaurant;
        }

        /// <summary>
        /// Gets the chain recent restaurants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetChainRecentRestaurants(int id)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == id).ChainId;
            var chainDetailsTable = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();

            var query = _restaurantOrderRepository.Table;
            var todayDate = DateTime.Now;
            return query.Where(o => DbFunctions.TruncateTime(o.ScheduledDate) == DbFunctions.TruncateTime(todayDate) && chainDetailsTable.Contains(o.RestaurantId)).Select(r => r.Restaurant).ToList();
        }

        /// <summary>
        /// Modifies the chain.
        /// </summary>
        /// <param name="chainTable">The chain table.</param>
        /// <returns>ChainTable.</returns>
        public ChainTable ModifyChain(ChainTable chainTable)
        {
            var existChain = _chainTableOperation.GetById(c => c.Id == chainTable.Id);
            existChain.Description = chainTable.Description;
            existChain.IsActive = chainTable.IsActive;

            foreach (var exChainDetails in existChain.ChainDetailsTables.ToList())
            {
                if (exChainDetails.RestaurantId != null)
                {
                    var existRestaurant = chainTable.ChainDetailsTables.Where(i => i.RestaurantId == exChainDetails.RestaurantId).FirstOrDefault();

                    if (existRestaurant == null)
                        _chainDetailsTableRepository.Delete(exChainDetails.Id);
                }
                if (exChainDetails.HotelId != null)
                {
                    var existHotel = chainTable.ChainDetailsTables.Where(i => i.HotelId == exChainDetails.HotelId).FirstOrDefault();

                    if (existHotel == null)
                        _chainDetailsTableRepository.Delete(exChainDetails.Id);
                }
            }

            foreach (var chainDetails in chainTable.ChainDetailsTables.ToList())
            {
                if (chainDetails.RestaurantId != null)
                {
                    var existRestaurant = existChain.ChainDetailsTables.Where(i => i.RestaurantId == chainDetails.RestaurantId).FirstOrDefault();

                    if (existRestaurant == null)
                        existChain.ChainDetailsTables.Add(chainDetails);
                }
                if (chainDetails.HotelId != null)
                {
                    var existHotel = existChain.ChainDetailsTables.Where(i => i.HotelId == chainDetails.HotelId).FirstOrDefault();

                    if (existHotel == null)
                        existChain.ChainDetailsTables.Add(chainDetails);
                }
            }

            _chainTableOperation.Update(existChain);
            var newRepo = new EfRepository<ChainTable>();
            var chain = newRepo.GetById(existChain.Id);

            return chain;
        }

        /// <summary>
        /// Deletes the chain.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteChain(int id)
        {
            _chainTableOperation.Delete(id);
        }

        /// <summary>
        /// Gets all recent orders of chain admin.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="restId">The rest identifier.</param>
        /// <param name="minAmount">The minimum amount.</param>
        /// <param name="maxAmount">The maximum amount.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <param name="flag">The flag.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetAllRecentOrdersOfChainAdmin(int customerId, string fromDate, string toDate, int restId, Double? minAmount, Double? maxAmount, int skip, int pageSize, out int total, int? flag)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            var restaurants = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();

            var query = _restaurantOrderRepository.Table;
            if (restId > 0)
            {
                query = query.Where(o => o.RestaurantId == restId);
            }
            else
            {
                query = query.Where(o => restaurants.Contains(o.RestaurantId));
            }

            if (flag == null)
            {
                if (string.IsNullOrWhiteSpace(fromDate) && string.IsNullOrWhiteSpace(toDate))
                {
                    var toDt = DateTime.Now;
                    var fromDt = toDt.AddDays(-30);
                    query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(fromDt)
                                                                        && DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(toDt));
                }
            }
            else if (flag == 1)
            {
                var currentDate = DateTime.Now;
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(currentDate) || DbFunctions.TruncateTime(o.ScheduledDate) >= DbFunctions.TruncateTime(currentDate));
            }
            if (!string.IsNullOrWhiteSpace(fromDate))
            {
                DateTime from_date = DateTime.ParseExact(fromDate, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) >= DbFunctions.TruncateTime(from_date));
            }
            if (!string.IsNullOrWhiteSpace(toDate))
            {
                DateTime to_date = DateTime.ParseExact(toDate, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                query = query.Where(o => DbFunctions.TruncateTime(o.CreationDate) <= DbFunctions.TruncateTime(to_date));
            }
            if (minAmount.HasValue && minAmount > 0)
            {
                query = query.Where(o => o.RestaurantOrderItems.Sum(i => i.Qty * i.RestaurantMenuItem.Price) > minAmount);
            }
            if (maxAmount.HasValue && maxAmount > 0)
            {
                query = query.Where(o => o.RestaurantOrderItems.Sum(i => i.Qty * i.RestaurantMenuItem.Price) < maxAmount);
            }

            var pages = query.OrderByDescending(o => o.Id)
          .Skip(skip)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }

        /// <summary>
        /// Gets all chain recent order.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable RestaurantOrder.</returns>
        public IEnumerable<RestaurantOrder> GetAllChainRecentOrder(int customerId, int skip, int pageSize, out int total)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            var restaurants = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();

            var query = _restaurantOrderRepository.Table.Where(r => restaurants.Contains(r.RestaurantId));

            var toDt = DateTime.Now;
            var fromDt = toDt.AddDays(-30);
            query = query.Where(o => DbFunctions.TruncateTime(o.ScheduledDate) >= DbFunctions.TruncateTime(fromDt)
                                                                && DbFunctions.TruncateTime(o.ScheduledDate) <= DbFunctions.TruncateTime(toDt));
            var pages = query.OrderByDescending(o => o.Id)
          .Skip(skip)
          .Take(pageSize)
          .GroupBy(p => new { Total = query.Count() })
          .FirstOrDefault();
            total = 0;
            if (pages == null)
                return new List<RestaurantOrder>();

            total = pages.Key.Total;
            var entities = pages.Select(p => p);
            return entities.ToList();
        }
        /// <summary>
        /// Gets all rest of chain admin by search text.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="restaurant_name">Name of the restaurant.</param>
        /// <returns>IEnumerable Restaurant.</returns>
        public IEnumerable<Restaurant> GetAllRestOfChainAdminBySearchText(int customerId, string restaurant_name)
        {
            var chainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            var chainDetailsTable = _chainDetailsTableRepository.Table.Where(c => c.ChainId == chainId && c.RestaurantId != null).Select(c => c.RestaurantId).ToList();

            var query = _restaurantDetailsRepository.Table;
            return query.Where(r => r.Name.Contains(restaurant_name) && chainDetailsTable.Contains(r.Id)).ToList();
        }

        /// <summary>
        /// Gets the name of all chains by.
        /// </summary>
        /// <param name="chain_name">Name of the chain.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        public IEnumerable<ChainTable> GetAllChainsByName(string chain_name)
        {
            var chainIds = _customerRoleRepository.Table.Where(r => r.ChainId != null).Select(r => r.ChainId).ToArray();

            var chains = (from chain in _chainTableRepository.Table
                          where (!chainIds.Contains(chain.Id) && chain.ChainName.Contains(chain_name) && chain.IsActive == true)
                          select (chain));
            return chains;
        }

        /// <summary>
        /// Gets the name of all default chains by.
        /// </summary>
        /// <param name="chainName">Name of the chain.</param>
        /// <returns>IEnumerable ChainTable.</returns>
        public IEnumerable<ChainTable> GetAllDefaultChainsByName(string chainName)
        {
            var query = _chainTableRepository.Table;
            return query.Where(c => c.ChainName.Contains(chainName)).ToList();
        }

        /// <summary>
        /// Gets all rest chain reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable RestaurantReview.</returns>
        public IEnumerable<RestaurantReview> GetAllRestChainReviews(int customerId)
        {
            var customerChainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            return _chainTableDetailsOperations.GetAll(cd => cd.ChainId == customerChainId && cd.RestaurantId != null).SelectMany(r => r.Restaurant.RestaurantReviews).ToList();
        }
        /// <summary>
        /// Gets all hotel chain reviews.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>IEnumerable HotelReview.</returns>
        public IEnumerable<HotelReview> GetAllHotelChainReviews(int customerId)
        {
            var customerChainId = _customerRoleOperations.GetById(c => c.CustomerId == customerId).ChainId;
            return _chainTableDetailsOperations.GetAll(cd => cd.ChainId == customerChainId && cd.HotelId != null).SelectMany(r => r.Hotel.HotelReviews).ToList();
        }

        #endregion ChainTable

        #region MenuItem Suggestion

        /// <summary>
        /// Gets the restaurant menu item suggestions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable MenuItemSuggestion.</returns>
        public IEnumerable<MenuItemSuggestion> GetRestaurantMenuItemSuggestions(int menuItemId, int skip, int pageSize, out int total)
        {
            return _menuItemSuggestionOperations.GetAllWithServerSidePagging(i => i.RestaurantMenuItemId == menuItemId, i => i.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Gets the unassign menu item suggetions.
        /// </summary>
        /// <param name="menuItemId">The menu item identifier.</param>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns>IEnumerable RestaurantMenuItem.</returns>
        public IEnumerable<RestaurantMenuItem> GetUnassignMenuItemSuggetions(int menuItemId, int restaurantId)
        {
            var assignMenuItemIds = _menuItemSuggestionOperations.GetAll(i => i.RestaurantMenuItemId == menuItemId).Select(mi => mi.menuItemSuggestionId).Distinct();
            var unassignedResMenuItems = _restaurantMenuItemsRepository.Table.Where(r => !assignMenuItemIds.Contains(r.Id) && r.RestaurantMenuGroup.RestaurantMenu.RestaurantId == restaurantId && r.IsActive == true);
            return unassignedResMenuItems;
        }

        /// <summary>
        /// Creates the menu item suggestion.
        /// </summary>
        /// <param name="menuItemSuggestion">The menu item suggestion.</param>
        public void CreateMenuItemSuggestion(List<MenuItemSuggestion> menuItemSuggestion)
        {
            foreach (var eachMenuItemSuggestion in menuItemSuggestion)
            {
                _menuItemSuggestionOperations.AddNew(eachMenuItemSuggestion);
            }
        }

        /// <summary>
        /// Deletes the menu item suggestion.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteMenuItemSuggestion(int id)
        {
            _menuItemSuggestionOperations.Delete(id);
        }

        #endregion MenuItem Suggestion

        #region Nutrition

        /// <summary>
        /// Gets all nutritions.
        /// </summary>
        /// <returns>IEnumerable Nutrition.</returns>
        public IEnumerable<Nutrition> GetAllNutritions()
        {
            return _nutritionRepository.Table;
        }
        /// <summary>
        /// Gets the un assigned nutritions.
        /// </summary>
        /// <param name="IngredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable Nutrition.</returns>
        public IEnumerable<Nutrition> GetUnAssignedNutritions(int IngredientId)
        {
            var assignedNutrition = _ingredientNutritionValuesOperation.GetAll(i => i.IngredientId == IngredientId).Select(n => n.NutritionId).ToList();
            var unassignedNutrition = _nutritionOperations.GetAll(f => !assignedNutrition.Contains(f.Id));
            return unassignedNutrition;
        }
        /// <summary>
        /// Gets the nutritions.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable Nutrition.</returns>
        public IEnumerable<Nutrition> GetNutritions(int skip, int pageSize, out int total)
        {
            return _nutritionOperations.GetAllWithServerSidePagging(_ => true, o => o.Id, skip, pageSize, out total);
        }
        /// <summary>
        /// Creates the nutrition.
        /// </summary>
        /// <param name="nutrition">The nutrition.</param>
        /// <returns>Nutrition.</returns>
        public Nutrition CreateNutrition(Nutrition nutrition)
        {
            return _nutritionOperations.AddNew(nutrition);
        }
        /// <summary>
        /// Deletes the nutrition.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteNutrition(int id)
        {
            _nutritionOperations.Delete(id);
        }

        #endregion Nutrition

        #region Ingredient Nutrition Values

        /// <summary>
        /// Gets the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <returns>IEnumerable IngredientNutritionValues.</returns>
        public IEnumerable<IngredientNutritionValues> GetIngredientNutritionValues(int ingredientId)
        {
            return _ingredientNutritionValuesRepository.Table.Where(i => i.IngredientId == ingredientId);
        }
        /// <summary>
        /// Gets the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientId">The ingredient identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable IngredientNutritionValues.</returns>
        public IEnumerable<IngredientNutritionValues> GetIngredientNutritionValues(int ingredientId, int skip, int pageSize, out int total)
        {
            return _ingredientNutritionValuesOperation.GetAllWithServerSidePagging(i => i.IngredientId == ingredientId, o => o.Id, skip, pageSize, out total);
        }

        /// <summary>
        /// Creates the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientNutritionValues">The ingredient nutrition values.</param>
        /// <returns>IngredientNutritionValues.</returns>
        public IngredientNutritionValues CreateIngredientNutritionValues(IngredientNutritionValues ingredientNutritionValues)
        {
            ingredientNutritionValues.CreationDate = DateTime.Now;
            return _ingredientNutritionValuesOperation.AddNew(ingredientNutritionValues);
        }

        /// <summary>
        /// Gets the ingredient nutrition values by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IngredientNutritionValues.</returns>
        public IngredientNutritionValues GetIngredientNutritionValuesById(int id)
        {
            return _ingredientNutritionValuesOperation.GetById(n => n.Id == id);
        }
        /// <summary>
        /// Modifies the ingredient nutrition values.
        /// </summary>
        /// <param name="ingredientNutritionValues">The ingredient nutrition values.</param>
        /// <returns>IngredientNutritionValues.</returns>
        public IngredientNutritionValues ModifyIngredientNutritionValues(IngredientNutritionValues ingredientNutritionValues)
        {
            _ingredientNutritionValuesOperation.Update(ingredientNutritionValues.Id, ingredientNutritionValues);
            return ingredientNutritionValues;
        }
        /// <summary>
        /// Deletes the ingredient nutrition values.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteIngredientNutritionValues(int id)
        {
            _ingredientNutritionValuesOperation.Delete(id);
        }

        #endregion Ingredient Nutrition Values
    }
}
