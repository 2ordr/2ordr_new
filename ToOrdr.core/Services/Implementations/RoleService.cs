﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 10-25-2018
// ***********************************************************************
// <copyright file="RoleService.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;
using ToOrdr.Core.Services.Interfaces;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class RoleService.
    /// Implements the <see cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Role},ToOrdr.Core.Entities.Role}" />
    /// Implements the <see cref="ToOrdr.Core.Services.Interfaces.IRoleService" />
    /// </summary>
    /// <seealso cref="ToOrdr.Core.Services.Implementations.ImplementationBase{ToOrdr.Core.Data.IRepository{ToOrdr.Core.Entities.Role},ToOrdr.Core.Entities.Role}" />
    /// <seealso cref="ToOrdr.Core.Services.Interfaces.IRoleService" />
    public class RoleService : ImplementationBase<IRepository<Role>, Role>, IRoleService
    {
        /// <summary>
        /// The role repository
        /// </summary>
        private readonly IRepository<Role> _roleRepository;
        /// <summary>
        /// The customer details repository
        /// </summary>
        private readonly IRepository<Customer> _customerDetailsRepository;
        /// <summary>
        /// The customer role repository
        /// </summary>
        private readonly IRepository<CustomerRole> _customerRoleRepository;


        /// <summary>
        /// Initializes a new instance of the <see cref="RoleService"/> class.
        /// </summary>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="customerDetailsRepository">The customer details repository.</param>
        /// <param name="customerRoleRepository">The customer role repository.</param>
        public RoleService(IRepository<Role> roleRepository,
            IRepository<Customer> customerDetailsRepository,
            IRepository<CustomerRole> customerRoleRepository)
            : base(roleRepository)
        {
            _roleRepository = roleRepository;
            _customerDetailsRepository = customerDetailsRepository;
            _customerRoleRepository = customerRoleRepository;
        }

        /// <summary>
        /// Gets the roles for user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>IList Role.</returns>
        public IList<Role> GetRolesForUser(string username)
        {
            List<Role> roles = new List<Role>();
            var customer = _customerDetailsRepository.Table.FirstOrDefault(c => c.Email == username);
            if (customer == null)
                return roles;

            return _customerRoleRepository.Table.Where(c => c.CustomerId == customer.Id).Select(c => c.Role).ToList();

        }


        /// <summary>
        /// Determines whether [is user in role] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="role">The role.</param>
        /// <returns><c>true</c> if [is user in role] [the specified username]; otherwise, <c>false</c>.</returns>
        public bool IsUserInRole(string username, string role)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.FirstOrDefault(r => r.Name == role) != null;
        }

        /// <summary>
        /// Gets the restaurant by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Restaurant.</returns>
        public Restaurant GetRestaurantByCustomerId(int customerId)
        {
            return _customerRoleRepository.Table.Where(r => r.CustomerId == customerId).FirstOrDefault().Restaurant;
        }
        /// <summary>
        /// Gets the hotel by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Hotel.</returns>
        public Hotel GetHotelByCustomerId(int customerId)
        {
            return _customerRoleRepository.Table.Where(r => r.CustomerId == customerId).FirstOrDefault().Hotel;
        }
    }


}
