﻿// ***********************************************************************
// Assembly         : ToOrdr.core
// Author           : Manoj & Suraj
// Created          : 10-25-2018
//
// Last Modified By : Manoj
// Last Modified On : 12-04-2018
// ***********************************************************************
// <copyright file="CRUDOperation.cs" company="Microsoft">
//     Copyright © Microsoft 2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrdr.Core.Data;
using ToOrdr.Core.Entities;

namespace ToOrdr.Core.Services.Implementations
{
    /// <summary>
    /// Class CRUDOperation.
    /// </summary>
    /// <typeparam name="TRepository">The type of the t repository.</typeparam>
    /// <typeparam name="TEntity">The type of the t entity.</typeparam>
    public class CRUDOperation<TRepository, TEntity>
        where TRepository : IRepository<TEntity>
        where TEntity : EntityBase
    {
        /// <summary>
        /// The repository
        /// </summary>
        TRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CRUDOperation{TRepository, TEntity}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public CRUDOperation(TRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TEntity.</returns>
        public TEntity GetById(int id)
        {
            return _repository.GetById(id);
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>TEntity.</returns>
        public TEntity GetById(Func<TEntity, bool> predicate)
        {
            return _repository.Table.Where(predicate).FirstOrDefault();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate)
        {
            return _repository.Table.Where(predicate).AsEnumerable();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAll()
        {
            return GetAll(x => true);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate, string orderby, int pageNumber, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderBy(_ => true)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<TEntity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate, Func<TEntity, Object> orderByPredicate, int pageNumber, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderBy(orderByPredicate)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<TEntity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }

        /// <summary>
        /// Gets all by desc.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAllByDesc(Func<TEntity, bool> predicate, Func<TEntity, Object> orderByPredicate, int pageNumber, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderByDescending(orderByPredicate)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<TEntity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }

        /// <summary>
        /// Gets all with server side pagging.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAllWithServerSidePagging(Func<TEntity, bool> predicate, Func<TEntity, Object> orderByPredicate, int skip, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderBy(orderByPredicate)
                .Skip(skip)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<TEntity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }
        /// <summary>
        /// Gets all with server side pagging desc.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="orderByPredicate">The order by predicate.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAllWithServerSidePaggingDesc(Func<TEntity, bool> predicate, Func<TEntity, Object> orderByPredicate, int skip, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderByDescending(orderByPredicate)
                .Skip(skip)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<TEntity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="orderby">The orderby.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="total">The total.</param>
        /// <returns>IEnumerable TEntity.</returns>
        public IEnumerable<TEntity> GetAll(string orderby, int pageNumber, int pageSize, out int total)
        {
            return this.GetAll(p => true, orderby, pageNumber, pageSize, out total);
        }


        /// <summary>
        /// Adds the new.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>TEntity.</returns>
        public virtual TEntity AddNew(TEntity entity)
        {

            try
            {
                return _repository.Insert(entity);


            }

            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public virtual void Update(TEntity entity)
        {
            _repository.Update(entity);
            return;
        }


        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="anyObject">Any object.</param>
        public virtual void Update(int id, Object anyObject)
        {
            _repository.Update(id, anyObject);
        }


        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="excludePropertyNames">The exclude property names.</param>
        public virtual void Update(int id, TEntity entity, string[] excludePropertyNames)
        {
            _repository.Update(id, entity, excludePropertyNames);

        }


        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(int id)
        {
            _repository.Delete(id);
        }

    }
}
