﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrdr.Core.Extensions
{
    public static class DbContextExtensions
    {
        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="id">Id of the entity to be modified</param>
        /// <returns></returns>
        public static bool Modify<TEntity>(this DbSet<TEntity> set, int id) where TEntity : class
        {
            return set.Find(id) != null;
        }
    }
}
